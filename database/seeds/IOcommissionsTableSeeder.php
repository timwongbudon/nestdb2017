<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Property;
use App\User;
use App\IOCommission;

class IOcommissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $coms = DB::table('commission')
        ->whereBetween('created_at', ['2020-06-01', '2021-03-01'])			
        ->get();
        $count = 0;
        foreach($coms as $com){
            $p = Property::find($com->propertyid);
            $check = $this->checkchangesproperty($com->propertyid);

            if($check['status'] == 1 && $check['information_officer']==44){
            //if($p->user_id == 44 ){			
                $count++;
                $data = array(
                    'user_id' => $check['information_officer'],
                    'property_id' => $com->propertyid,
                    'status' => 'Pending',
                    'commission' => '500',
                    'owner_id' => $p->owner_id,
                    'invoiceId' => $com->id,
                    'salarydate' => $com->commissiondate,
                );
                print_r($data);
                IOCommission::insert($data);
            //}
            }
            
        }
        //print_r($data);
        $this->command->info($count.' Entires are created');
        
    }


    public function checkchangesproperty($id){
		//$user = $request->user();
	
        $property = Property::findOrFail($id);
        if (empty($property)) {
            return '0';
        }
		
	
		$rows = $property->getLog();

		$propertyUserId = $property->user_id;
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		
		$r = null;

		$returnVal = 0;

		$createdby = json_decode($u[$propertyUserId]->roles);
		$io = "";
	
		
		if($createdby->Informationofficer == true && $u[$propertyUserId]->status == 0){
			$returnVal = 1;
			$io = $propertyUserId;
		}
	

		for ($i = 0; $i < $rows->count()-1; $i++){
            $r = $rows[$i];
			$n = array();		
		
            if (($i+1) < $rows->count()){
                $old = $rows[$i+1];

                $roles = json_decode($u[$r->userid]->roles);
				
				///check if IO Officer
                if($roles->Informationofficer == true ){
					$io = $r->userid;
                    //check if there are changes in the logs for the owner
                    if ($r['owner_id'] != $old['owner_id']){
                        if ($r['owner_id'] == 0){
                        
                        }else{						

                            $returnVal = 1;
                        }
                    }
                }
            }
        }

        return array('status'=>$returnVal, 'information_officer'=> $io );

	}
}
