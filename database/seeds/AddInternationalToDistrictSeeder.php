<?php

use Illuminate\Database\Seeder;

use App\District;

class AddInternationalToDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = date('Y-m-d H:i:s');

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $countries = array(
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Portugal',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Spain',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Mexico',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'USA',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Thailand',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Australia',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'New Zealand',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'South Africa',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'UK',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'France',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Greece',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Croatia',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
            array(
                'user_id'       => 1,
                'parent_id'     => 0,
                'name'          => 'Indonesia',
                'web'           => 1,
                'order'         => 0,
                'international' => 1,
                'created_at'    => $timestamp,
                'updated_at'    => $timestamp
            ),
        );

        DB::table('districts')->insert($countries);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
