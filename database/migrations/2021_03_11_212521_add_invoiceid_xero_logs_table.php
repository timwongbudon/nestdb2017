<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceidXeroLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('xero_logs', function (Blueprint $table) {
            $table->integer('invoiceid')->nullable()->default(NULL)->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xero_logs', function (Blueprint $table) {
            $table->dropColumn('invoiceid');
        });
    }
}
