<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersdocumentEnum2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE userdocuments MODIFY doctype ENUM('contract','hkid','eaa','dismissal','resignation')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE userdocuments MODIFY doctype ENUM('contract','hkid','eaa')");
    }
}
