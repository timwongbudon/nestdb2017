<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseCarInClientViewingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_viewing', function (Blueprint $table) {
            $table->integer('use_car')->nullable()->default(NULL)->after('driver');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_viewing', function (Blueprint $table) {
            $table->dropColumn('use_car');
        });
    }
}
