<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerIdToIoCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('io_commissions', function (Blueprint $table) {
            $table->integer('owner_id')->nullable()->default(NULL)->after('commission');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('io_commissions', function (Blueprint $table) {
            $table->dropColumn('owner_id');
        });
        
    }
}
