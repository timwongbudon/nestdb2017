<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('tel', 255);
            $table->string('email', 255);
            $table->string('workplace', 255);
            $table->string('title', 255);

            $table->string('firstname2', 255);
            $table->string('lastname2', 255);
            $table->string('tel2', 255);
            $table->string('email2', 255);
            $table->string('workplace2', 255);
            $table->string('title2', 255);
            
            $table->decimal('budget_min',11,2)->unsigned()->default('0');
            $table->decimal('budget_max',11,2)->unsigned()->default('0');
            $table->enum('tempature', array('', 'Hot', 'Cold', 'Warm'));
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('client_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->string('meta_key', 255);
            $table->longText('meta_value');
        });

        Schema::create('client_shortlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->longText('preference');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('client_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('shortlist_id')->unsigned();
            $table->bigInteger('property_id')->unsigned();
            $table->longText('remarks');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('client_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('property_id')->unsigned();
            $table->tinyInteger('type_id')->unsigned();// lease, sale
            $table->date('history_start');
            $table->date('history_end');
            $table->decimal('asking_rent',12,2)->unsigned()->default('0');
            $table->decimal('asking_sale',20,2)->unsigned()->default('0');
            $table->boolean('inclusive');
            $table->decimal('management_fee',8,2)->unsigned()->default('0');
            $table->decimal('goverment_rate',8,2)->unsigned()->default('0');
            $table->string('break_clause', 50);
            $table->string('rent_free_period', 50);
            $table->longText('remarks');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
        Schema::drop('client_metas');
        Schema::drop('client_histories');
    }
}
