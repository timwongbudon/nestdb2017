<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExpensesSetChequeToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function (Blueprint $table) {
            DB::statement("ALTER TABLE `expenses` CHANGE `chequenumber` `chequenumber` VARCHAR(191) DEFAULT '0'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            DB::statement("ALTER TABLE `expenses` CHANGE `chequenumber` `chequenumber` INT(11) DEFAULT '0'");
        });
    }
}
