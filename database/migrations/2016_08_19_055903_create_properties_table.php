<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string('name');
            $table->string('display_name');
            $table->mediumInteger('building_id')->unsigned()->default('0');
            $table->string('unit', 50);
            $table->tinyInteger('type_id')->unsigned();//1:rent;2:sale,3:both
            $table->string('address1', 255);
            $table->string('address2', 255);
            $table->smallInteger('district_id')->unsigned()->default('0');
            $table->string('country_code', 2);//HK,CN,FR

            $table->enum('floor_zone', array('', 'High', 'Middle', 'Low', 'Ground Floor'));
            $table->enum('layout', array('' ,'Duplex', 'Penthouse', 'Simplex', 'Triplex'));
            $table->longText('description');

            $table->tinyInteger('bedroom')->unsigned()->default('0');
            $table->tinyInteger('bathroom')->unsigned()->default('0');
            $table->tinyInteger('maidroom')->unsigned()->default('0');
            $table->smallInteger('year_built')->unsigned()->default('0');

            $table->mediumInteger('gross_area')->unsigned()->default('0');
            $table->mediumInteger('saleable_area')->unsigned()->default('0');

            $table->decimal('asking_rent',12,0)->unsigned()->default('0');
            $table->decimal('asking_sale',20,0)->unsigned()->default('0');
            $table->boolean('inclusive');
            $table->decimal('management_fee',8,2)->unsigned()->default('0');
            $table->decimal('government_rate',8,2)->unsigned()->default('0');

            $table->tinyInteger('poc_id')->unsigned();
            $table->bigInteger('owner_id')->unsigned()->default('0');
            $table->bigInteger('agent_id')->unsigned()->default('0');
            $table->string('rep_ids', 255);
            $table->string('agent_ids', 255);

            $table->tinyInteger('key_loc_id')->unsigned();
            $table->string('key_loc_other', 255);

            $table->string('door_code', 20);
            $table->string('agency_fee', 10);
            $table->longText('comments');
            $table->longText('contact_info');
            $table->date('available_date');

            $table->string('external_id', 50);
            $table->string('source', 20);

            $table->boolean('web')->default(0);
            $table->tinyInteger('car_park')->default(0);

            $table->tinyInteger('outdoor')->default(0);//web - override count

            $table->string('hkh_url', 255);
            $table->boolean('leased');
            $table->boolean('featured')->default(0);
            $table->string('slug', 100);//web - seo redirection use.

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('property_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->string('meta_key', 255);
            $table->longText('meta_value');
        });

        Schema::create('propertymigration_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->string('meta_key', 255);
            $table->longText('meta_value');
        });

        Schema::create('property_outdoors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->boolean('f1')->default(0);
            $table->boolean('f2')->default(0);
            $table->boolean('f3')->default(0);
            $table->boolean('f4')->default(0);
            $table->boolean('f5')->default(0);
        });

        Schema::create('property_features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->boolean('f1')->default(0);
            $table->boolean('f2')->default(0);
            $table->boolean('f3')->default(0);
            $table->boolean('f4')->default(0);
            $table->boolean('f5')->default(0);
            $table->boolean('f6')->default(0);
            $table->boolean('f7')->default(0);
            $table->boolean('f8')->default(0);
        });

        Schema::create('property_medias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->string('group', 50);//media source
            $table->string('url', 255);
            $table->string('filename', 255);
            $table->string('caption', 255);
            $table->string('alt', 255);
            $table->text('description');
            $table->tinyInteger('order');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('buildings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string('name');
            $table->string('display_name');//override name for printing card.
            $table->string('address1', 255);
            $table->string('address2', 255);
            $table->smallInteger('district_id')->unsigned()->default('0');
            $table->string('country_code', 2);//HK,CN,FR
            $table->smallInteger('year_built')->unsigned()->default('0');
            $table->string('lat', 50);
            $table->string('lng', 50);
            $table->string('tel', 50);
            $table->softDeletes();
            $table->timestamps();
            $table->unique('name');
        });

        Schema::create('building_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('building_id')->unsigned();
            $table->string('meta_key', 255);
            $table->longText('meta_value');
        });

        Schema::create('building_medias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('building_id')->unsigned();
            $table->string('group', 50);//media source
            $table->string('url', 255);
            $table->string('filename', 255);
            $table->string('caption', 255);
            $table->string('alt', 255);
            $table->text('description');
            $table->tinyInteger('order');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
        Schema::drop('property_metas');
        Schema::drop('buildings');
        Schema::drop('building_metas');
    }
}
