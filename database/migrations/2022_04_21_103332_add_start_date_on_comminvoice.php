<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartDateOnComminvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comminvoice', function (Blueprint $table) {            
            $table->date('start_date')->after('month_break_clause')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comminvoice', function (Blueprint $table) {
            $table->dropColumn('start_date');            
        });
    }
}
