<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreeMoreFlagsForProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function($table){
            $table->integer('feat_order')->after('leased');
        });
        Schema::table('properties', function($table){
            $table->integer('sale_order')->after('leased');
        });
        Schema::table('properties', function($table){
            $table->integer('rent_order')->after('leased');
        });
        Schema::table('properties', function($table){
            $table->integer('sold')->after('leased');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
