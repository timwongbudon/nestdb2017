<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoColOnComminvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comminvoice', function (Blueprint $table) {
            $table->text('lease_term')->after('terms')->nullable();
            $table->text('month_break_clause')->after('lease_term')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comminvoice', function (Blueprint $table) {
            $table->dropColumn('lease_term');
            $table->dropColumn('month_breaK_clause');
        });
    }
}
