<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComminvoiceAddWork extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comminvoice', function (Blueprint $table) {
            $table->string('work_title')->nullable();
            $table->text('work_place')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comminvoice', function (Blueprint $table) {
            $table->dropColumn('work_title');
            $table->dropColumn('work_place');
        });
    }
}
