<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyDeleteLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_delete_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');            
            $table->integer('deleted_by');            
            $table->integer('confirmed_by');
            $table->string('status');
            $table->softDeletes();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_delete_logs');
    }
}
