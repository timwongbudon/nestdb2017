<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPropertyRoomCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function($table){
            DB::statement("ALTER TABLE `properties` CHANGE `bedroom` `bedroom` DECIMAL(5,1) UNSIGNED NOT NULL DEFAULT '0'");
            DB::statement("ALTER TABLE `properties` CHANGE `bathroom` `bathroom` DECIMAL(5,1) UNSIGNED NOT NULL DEFAULT '0'");
            DB::statement("ALTER TABLE `properties` CHANGE `maidroom` `maidroom` DECIMAL(5,1) UNSIGNED NOT NULL DEFAULT '0'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
