<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_image_variations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_media_id')->unsigned()->index();
            $table->string('url', 255);
            $table->string('dimension', 100);
            $table->string('format', 50);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_image_variations');
    }
}
