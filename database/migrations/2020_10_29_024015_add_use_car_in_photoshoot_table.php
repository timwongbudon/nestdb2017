<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseCarInPhotoshootTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photoshoot', function (Blueprint $table) {
            $table->integer('use_car')->nullable()->default(NULL)->after('driver');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photoshoot', function (Blueprint $table) {
            $table->dropColumn('use_car');
        });
    }
}
