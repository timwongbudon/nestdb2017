<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->tinyInteger('type_id');//1:owner;2:rep,3:agent
            $table->bigInteger('parent_id')->unsigned()->default('0');
            $table->string('company', 255);// company name, agent name
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('tel', 255);
            $table->string('email', 255);
            $table->string('firstname2', 255);
            $table->string('lastname2', 255);
            $table->string('tel2', 255);
            $table->string('email2', 255);
            $table->string('address1', 255);
            $table->string('address2', 255);
            $table->smallInteger('district_id')->unsigned()->default('0');
            $table->string('country_code', 2);//HK,CN,FR
            $table->string('bri', 20);
            $table->string('hkid', 20);
            $table->string('agency_fee', 10);
            $table->longText('comments');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('vendor_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vendor_id')->unsigned();
            $table->string('meta_key', 255);
            $table->longText('meta_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendors');
        Schema::drop('vendor_metas');
    }
}
