<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsColumnToBonusDividen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonusdividend', function (Blueprint $table) {
            $table->text('comments')->default(null)->nullable()->after('paymentdate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bonusdividend', function (Blueprint $table) {
            $table->dropColumn('comments');
        });
    }
}
