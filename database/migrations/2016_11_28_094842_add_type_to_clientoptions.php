<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToClientoptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_options', function($table){
            $table->tinyInteger('type_id')->unsigned()->default('1')->after('property_id');//1:rent;2:sale,3:both
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_options', function($table){
            $table->dropColumn('type_id');
        });
    }
}
