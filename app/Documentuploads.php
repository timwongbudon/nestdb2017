<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;


class Documentuploads extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	
    protected $table = 'documentuploads';

    protected $fillable = ['id', 'doctype', 'shortlistid', 'consultantid', 'docpath'];
	
	
	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
}