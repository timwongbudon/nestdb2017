<?php

namespace App;

class PropertymigrationMeta extends BaseMetaModel 
{
	protected $table = 'propertymigration_metas';
    protected $fillable = ['property_id', 'meta_key', 'meta_value'];
    public $timestamps  = false;
    public $key_id = 'property_id';

    public static $rules = array(
            'property_id' => 'required|numeric',
            'meta_key'    => 'required|max:255',
    	);

    public static function get_metas($post_id) {
    	$metas = self::where('property_id', $post_id)->orderBy('id', 'ASC')->get();
    	foreach ($metas as $key => $meta) {
    		if (in_array($meta->meta_key, array('outdoors', 'features'))) {
    			$string = @json_decode($meta->meta_value);
    			$metas[$key]->meta_value = $string;
    		}
    	}
    	return $metas;
    }

    public function get_fixed_post_content() {
        if ($this->meta_key == 'post_content') {
            $html = str_get_html($this->meta_value);
            foreach ($html->find('img') as $img) {
                $img->width = '200';
                $img->height = '';
                $img->style = 'padding-bottom: 10px;';
            }
            foreach ($html->find('a') as $atag) {
                $atag->target = '_enlarge';
            }
            return $html;
        }
    }
}
