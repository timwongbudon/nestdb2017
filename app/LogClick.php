<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogClick extends BaseMediaModel 
{
    public $timestamps = false;

    protected $table = 'zlog_click';
    
    protected $fillable = ['userid', 'source'];
    public $key_id = 'logid';
	
	
	public static function logClick($source = 0){
		if (auth()->check()){
			$log = new LogClick();
			
			$log->userid = auth()->user()->id;
			$log->source = $source;
			
			$log->save();
		}
	}
	
}
