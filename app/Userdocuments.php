<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;


class Userdocuments extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	
    protected $table = 'userdocuments';

    protected $fillable = ['id', 'doctype', 'docuserid', 'userid', 'docpath'];
	
	public function created_at(){
		return $this->created_at;
	}
	
	public function created_at_nice(){
		return \NestDate::nest_datetime_format($this->created_at);
	}
}