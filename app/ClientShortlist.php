<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class ClientShortlist extends BaseModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'client_id', 'preference', 'status', 'comments', 'typeid'];

    public static $rules = array(
            'client_id' => 'required',
        );

    public static $messages = array(
        );

    protected $searchable = ['user_id', 'budget', 'client_name', 'tempature','typeid'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client() {
       return $this->belongsTo(Client::class);
    }

    public function options()
    {
        return $this->hasMany(ClientOption::class, 'shortlist_id', 'id')->where('client_id', $this->client_id)->with('property');
    }

	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}

    public function scopeIndexSearch($query, $request)
    {
        $input = $request->all();
        $query->select('client_shortlists.*')->leftJoin('clients', 'clients.id', '=', 'client_shortlists.client_id');
        foreach ($input as $key => $value) {
            if(in_array($key, $this->searchable) && !empty($value)) {
                if(is_string($value)){
					$value = trim($value);
				}
                switch ($key) {
                    case 'budget':
                        if (is_numeric($value) && $value>0):
                            $query->where(function($query) use ($value){
                                $query->where('budget_min', '<=', $value)
                                    ->where('budget_max', '>=', $value);
                            });
                        endif;
                        break;
                    case 'client_name':
                        $query->where(function($query) use ($value){
                                $query->orWhere('clients.firstname', 'like', "%{$value}%")
                                    ->orWhere('clients.lastname', 'like', "%{$value}%");
                            });
                        break;
                    case 'user_id':
                        $query->where("client_shortlists.{$key}", '=', $value);
                        break;

                    case 'typeid':
                            $query->where("{$key}", '=', $value);
                    break;
                    case 'tempature':
                        $query->where("clients.{$key}", '=', $value);
                        break;
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        return $query;
    }

    public function scopeNewIndexSearch($query, $input){
        $query->select('client_shortlists.*')->leftJoin('clients', 'clients.id', '=', 'client_shortlists.client_id');
        foreach ($input as $key => $value) {
            if(in_array($key, $this->searchable) && !empty($value)) {
                if(is_string($value)){
					$value = trim($value);
				}
                switch ($key) {
                    case 'budget':
                        if (is_numeric($value) && $value>0):
                            $query->where(function($query) use ($value){
                                $query->where('budget_min', '<=', $value)
                                    ->where('budget_max', '>=', $value);
                            });
                        endif;
                        break;
                    case 'client_name':
                        $query->where(function($query) use ($value){
                                $query->orWhere('clients.firstname', 'like', "%{$value}%")
                                    ->orWhere('clients.lastname', 'like', "%{$value}%");
                            });
                        break;
                    case 'user_id':
                        $query->where("client_shortlists.{$key}", '=', $value);
                        break;

                    case 'typeid':
                                $query->where("{$key}", '=', $value);
                    break;
                    case 'tempature':
                        $query->where("clients.{$key}", '=', $value);
                        break;
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        return $query;
    }

    public function scopeApiKeywordSearch($query, $keywords) {
        $keys = explode(' ', $keywords);
        $query->leftJoin('clients', 'clients.id', '=', 'client_shortlists.client_id');
        $query->where(function($query) use ($keys){
                            foreach ($keys as $value) {
                                if (preg_match("/\w+/", $value)) {
                                    $query->orWhere('clients.firstname', 'like', "%{$value}%")
                                        ->orWhere('clients.lastname', 'like', "%{$value}%");
                                }
                            }
                        });
        $query->select(\DB::raw('client_shortlists.*, clients.firstname, clients.lastname, clients.tel'));
        $query->orderBy('client_shortlists.id', 'desc');
        return $query;
    }

}
