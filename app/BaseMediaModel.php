<?php

namespace App;

class BaseMediaModel extends BaseModel 
{ 
    public $key_id = 'OBJID';

    /**
     * Add new media
     * @param array $data ('property_id', 'group', 'url', 'filename', 'caption', 'alt', 'description', 'order')
     */
    public function add_media($data=array()) {
    	if (empty($data) || !isset($data[$this->key_id]) || !isset($data['group']) || !isset($data['url']) ) {
            return false;
    	}
        if (!$this->is_duplicate($data[$this->key_id], $data['url'])) {
            return $this->create($data);
        }
    }

    public function is_duplicate($id, $url) {
        $count = $this->where($this->key_id, $id)->where('url', $url)->count();
        return $count > 0;
    }
}