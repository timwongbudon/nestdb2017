<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

	CONST NAOMI_ID = 6;
	CONST WILLIAM_ID = 7;
	const ACTIVE = 0;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'firstname', 'lastname', 'agentid', 'address', 'offtitle', 'picpath', 'gender', 'roles', 'status', 'sigpath', 'commstruct', 'color','allowed_vacation','vacation_tier','remaining_vacation', 'startdate', 'enddate', 'hkid', 'expensifyid', 'managing', 'sales_leaders'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all of the properties for the user.
     */
    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    /**
     * Get all of the buildings for the user.
     */
    public function buildings()
    {
        return $this->hasMany(Building::class);
    }

    /**
     * Get all of the vendors for the user.
     */
    public function vendors()
    {
        return $this->hasMany(Vendor::class);
    }

    /**
     * Get all of the vendors for the user.
     */
    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    public function imports()
    {
        return $this->hasMany(Import::class);
    }
	
	
	//Rights management
	public function getEmptyRights(){
		return array(
			'Superadmin' => false,
			'Director' => false,
			'Admin' => false,
			'Accountant' => false,
			'Informationofficer' => false,
			'Consultant' => false,
			'Officemanager' => false,
			'Designer' => false,
			'Photographer' => false,
			'Driver' => false,
			'Salesteamleader' => false
		);
	}
	
	public function getUserRights(){
		if ($this->roles == null || trim($this->roles) == ''){
			$this->roles = json_encode($this->getEmptyRights());
			$this->save();
		}
		
		$rights = json_decode($this->roles, true);
		
		//Admins should be able to see everything except the company section
		if (isset($rights['Admin']) && $rights['Admin']){
			$rights['Superadmin'] = true;
		}else{
			$rights['Admin'] = false;
		}
		
		if ($this->email == 'admin@nest-property.com'){
			$rights['Superadmin'] = true;
		}
		
		return $rights;
	}
	
	public function getUserManaging(){
		if ($this->managing == null || trim($this->managing) == ''){
			$this->managing = json_encode(array());
			$this->save();
		}
		
		$managing = json_decode($this->managing, true);
		
		return $managing;
	}

	public function getSalesLeaders()
	{
		if ($this->sales_leaders == null || trim($this->sales_leaders) == ''){
			$this->sales_leaders = json_encode(array());
			$this->save();
		}
		
		return json_decode($this->sales_leaders, true);
	}
	
	public function setUserRights($rights){
		$this->roles = json_encode($rights);
		$this->save();
	}
	
	public function getUserRightsString(){
		$rights = $this->getUserRights();
		
		$str = '';
		if ($rights['Superadmin'] && !$rights['Admin']){
			if ($str == '') $str = 'Superadmin'; else $str .= ', Superadmin';
		}
		if ($rights['Director']){
			if ($str == '') $str = 'Director'; else $str .= ', Director';
		}
		if ($rights['Admin']){
			if ($str == '') $str = 'Admin'; else $str .= ', Admin';
		}
		if ($rights['Accountant']){
			if ($str == '') $str = 'Accountant'; else $str .= ', Accountant';
		}
		if ($rights['Informationofficer']){
			if ($str == '') $str = 'Information Officer'; else $str .= ', Information Officer';
		}
		if ($rights['Consultant']){
			if ($str == '') $str = 'Consultant'; else $str .= ', Consultant';
		}
		if ($rights['Officemanager']){
			if ($str == '') $str = 'Office Manager'; else $str .= ', Office Manager';
		}
		if (isset($rights['Designer']) && $rights['Designer']){
			if ($str == '') $str = 'Designer'; else $str .= ', UI Designer';
		}
		if (isset($rights['Photographer']) && $rights['Photographer']){
			if ($str == '') $str = 'Photographer'; else $str .= ', Photographer';
		}
		if (isset($rights['Driver']) && $rights['Driver']){
			if ($str == '') $str = 'Driver'; else $str .= ', Driver';
		}
		
		return $str;
	}
	
	public function getUserRightsNameArr(){
		$ret = array(
			'Superadmin' => 'Superadmin',
			'Director' => 'Director',
			'Admin' => 'Admin',
			'Accountant' => 'Accountant',
			'Informationofficer' => 'Information Officer',
			'Consultant' => 'Consultant',
			'Officemanager' => 'Office Manager',
			'Designer' => 'UI Designer',
			'Photographer' => 'Photographer',
			'Driver' => 'Driver',
			'Salesteamleader' => 'Sales Team Leader'
		);
		
		return $ret;
	}
	
	public function isConsultant(){
		$rights = $this->getUserRights();
		
		if (isset($rights['Consultant']) && $rights['Consultant']){
			return true;
		}else{
			return false;
		}
	}
	
	public function isOfficeManager(){
		$rights = $this->getUserRights();
		
		if (isset($rights['Officemanager']) && $rights['Officemanager']){
			return true;
		}else{
			return false;
		}
	}
	
	public function isInformationofficer(){
		$rights = $this->getUserRights();
		
		if (isset($rights['Informationofficer']) && $rights['Informationofficer']){
			return true;
		}else{
			return false;
		}
	}
	
	public function isOnlyDriver(){
		$rights = $this->getUserRights();
		
		$count = 0;
		foreach ($rights as $x){
			if ($x){
				$count++;
			}
		}
		
		if (isset($rights['Driver']) && $rights['Driver'] && $count == 1){
			return true;
		}else{
			return false;
		}
	}
	
	public function isOnlyPhotographer(){
		$rights = $this->getUserRights();
		
		$count = 0;
		foreach ($rights as $x){
			if ($x){
				$count++;
			}
		}
		
		if (isset($rights['Photographer']) && $rights['Photographer'] && $count == 1){
			return true;
		}else{
			return false;
		}
	}
	
	

	public function property_deleted_by()
    {
        return $this->belongsTo('App\PropertyDeleteLogs', 'deleted_by');
    }
	public function property_deleted_confirmed_by()
    {
        return $this->belongsTo('App\PropertyDeleteLogs', 'confirmed_by');
    }

	public function getIsManagementAttribute(){
		return $this->getUserRights()['Superadmin'] || $this->getUserRights()['Admin'] || $this->getUserRights()['Director'];
	}
	
	
}
