<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogBasedocuments extends BaseMediaModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'zlog_basedocuments';
    
    protected $fillable = ['id', 'doctype', 'docversion', 'consultantid', 'fieldcontents', 'sectionshidden', 'replace', 'revision'];
	
    public $key_id = 'logid';
	
	
	
}
