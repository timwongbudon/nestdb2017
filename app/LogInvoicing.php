<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LogInvoicing extends Model
{
    use SoftDeletes;
    

    protected $table = 'invoicing_logs';

    protected $dates = ['deleted_at'];
	
    protected $fillable = ['id', 'user_id', 'consultantid', 'log'];

}
