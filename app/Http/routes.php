<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/migration', 'MigrationController@index');
// Route::get('/migration', 'Migration2Controller@index');
Route::auth();

Route::get('/testtt', 'TestController@index');

Route::get('/image/{id}/{format}/{filename}', 'ImageController@read');
Route::get('/whatsnew', 'ContentController@index');
Route::get('/imports', 'ImportController@index');
// Route::get('/import/create', 'ImportController@create');
Route::post('/import/store', 'ImportController@store');
Route::get('/import/edit/{import}', 'ImportController@edit');
Route::post('/import/update', 'ImportController@update');

Route::get('/user/settings', 'UserController@getsettings');
Route::post('/user/settings', 'UserController@changesettings');
Route::get('/user/profile', 'UserController@getprofile');
Route::post('/user/profile', 'UserController@changeprofile');
Route::get('/user/profile', 'UserController@getprofile');
Route::post('/user/profile', 'UserController@changeprofile');
Route::get('/user/admusers', 'UserController@admusers');
Route::get('/user/admuser/{id}', 'UserController@admuser');
Route::post('/user/admuser/{id}', 'UserController@changeadmuser');
Route::get('/user/newuser', 'UserController@newuser');
Route::post('/user/newuser', 'UserController@createuser');
Route::get('/user/list', 'UserController@userlist');
Route::get('/user/show/{id}', 'UserController@showuser');
Route::post('/user/show/{id}', 'UserController@savefromto');
Route::post('/user/docupload/{commissionid}', 'UserController@docstoreupload');
Route::get('/user/showdoc/{docid}', 'UserController@showdoc');
Route::get('/user/removedoc/{docid}/{invoiceid}', 'UserController@removedoc');

Route::get('/user/update_vacation/{id}/{number_vacation}/{vacation_tier}', 'UserController@update_vacation');

Route::get('/user/pagination/allchanges/{id}', 'UserController@fetch_data');
Route::get('/user/pagination/majorchanges/{id}', 'UserController@fetch_major_changes');
Route::get('/user/pagination/newproperties/{id}', 'UserController@fetch_newproperties');

Route::get('/showimage/{folder}/{image}', 'ImageAccessController@showimage');
Route::get('/showsig/{image}', 'ImageAccessController@showsig');

//Route::get('/', 'PropertyController@dashboard')->name('dashboard');
Route::get('/', 'LeadsController@home')->name('dashboard');
Route::get('/properties', 'PropertyController@index');
Route::get('/properties/new', 'PropertyController@newproperties');
Route::get('/property/connect-hongkonghomesphotos', 'PropertyMediaController@api_connect_hongkonghomesphotos');
Route::get('/property/connect-newhongkonghomesphotos', 'PropertyMediaController@api_connect_newhongkonghomesphotos');
Route::get('/property/hongkonghomesphotostest', 'PropertyMediaController@hongkonghomesphotostest');
Route::get('/property/newhongkonghomesphotostest', 'PropertyMediaController@newhongkonghomesphotostest');
Route::get('/incomplete/properties', 'PropertyController@incomplete_index');
Route::get('/trash/properties', 'PropertyController@trash_index');
Route::get('/frontend/properties', 'PropertyController@frontend_index');
Route::any('/frontend/select', 'PropertyController@frontend_select');
Route::any('/frontend/photoshoot', 'PropertyController@addphotoshoot');
Route::get('/property/create', 'PropertyController@create');
Route::get('/property/photoshoot', 'PropertyController@showphotoshoot');
Route::get('/property/createbob/{id}', 'PropertyController@createbob');
Route::get('/property/show/{property}', 'PropertyController@show');
Route::get('/property/edit/{property}', 'PropertyController@edit');
Route::get('/property/featrent/{property}', 'PropertyController@featrent');
Route::get('/property/featsale/{property}', 'PropertyController@featsale');
Route::get('/property/feathome/{property}', 'PropertyController@feathome');
Route::post('/property/delete', 'PropertyController@delete');
Route::post('/property/restore', 'PropertyController@restore');
Route::post('/property/store', 'PropertyController@store');
Route::post('/property/update', 'PropertyController@update');
Route::post('/property/option/print', 'PropertyController@option_print');
Route::post('/property/option/printagent', 'PropertyController@option_print_agent');
Route::post('/property/option/set-as-hot', 'PropertyController@option_set');
Route::post('/property/upload', 'PropertyMediaController@upload');
Route::get('/property/buildingpics/{propertyid}/{buildingid}', 'PropertyController@buildingpics');
Route::get('/property/addbuildingpics/{propertyid}/{photopropertyid}', 'PropertyController@addbuildingpics');
Route::get('/propertyimages/{property}/{group}/{type}/', 'PropertyMediaController@propertyimages');
Route::get('/property/media/edit-reorder/{property}/{group}/{type}/', 'PropertyMediaController@edit_reorder');
Route::get('/property/media/edit-remove/{property}/{group}/{type}/', 'PropertyMediaController@edit_remove');
Route::post('/property/media/reorder', 'PropertyMediaController@reorder')->middleware('auth');
Route::post('/property/media/remove', 'PropertyMediaController@remove')->middleware('auth');
Route::post('/property/media/restore', 'PropertyMediaController@restore')->middleware('auth');
Route::post('/property/media/permanent-remove', 'PropertyMediaController@permanent_remove')->middleware('auth');
Route::get('/property/flushmedia/{propertyid}', 'PropertyController@flushmedia');
Route::get('/upcomingleases', 'PropertyController@upcleases');
Route::get('/hotproperties', 'PropertyController@hotprops');
Route::get('/newproperties', 'PropertyController@newprops');
Route::get('/newcreatedprops', 'PropertyController@newcreatedprops');
Route::get('/clpropertiesowner/{ownerid}/{buildingid}', 'PropertyController@clpropertiesowner');
Route::get('/clpropertiesagency/{agencyid}/{buildingid}', 'PropertyController@clpropertiesagency');
Route::get('/bonuscomm', 'PropertyController@bonuscomm');
Route::get('/property/csvimport', 'PropertyController@csvimport');
Route::post('/property/docupload/{propertyid}', 'PropertyController@docstoreupload');
Route::get('/property/showdoc/{docid}', 'PropertyController@showdoc');
Route::get('/property/removedoc/{docid}/{propertyid}', 'PropertyController@removedoc');
Route::get('/property/{property}/{group}/{type}/', 'PropertyMediaController@api_index');
//Route::get('/property/zipimages/{id}', 'PropertyController@zipimages');
Route::get('/property/tobedeleted', 'PropertyController@deleted');
Route::post('/property/delete/confirm', 'PropertyController@confirm_delete');
Route::post('/property/delete/decline', 'PropertyController@decline_delete');
Route::get('/property/deletedproperties', 'PropertyController@deletedproperties');
Route::post('/property/restoreproperty', 'PropertyController@restore_property');
Route::get('/property/property_exists', 'PropertyController@property_exists');
Route::get('/property/property_exists_update', 'PropertyController@property_exists_update');


Route::get('/api/properties/', 'ApiPropertyController@index');
Route::get('/api/property/{property}', 'ApiPropertyController@show');
Route::get('/api/propertyquick/{property}', 'ApiPropertyController@showquick');
Route::get('/api/sitemap/', 'ApiPropertyController@sitemap');
Route::get('/api/full/', 'ApiPropertyController@full');
Route::get('/api/fullext/', 'ApiPropertyController@fullext');
Route::get('/api/propertiesquerycount/', 'ApiPropertyController@PropertyResultsCounter');

Route::get('/buildings/api/index', 'BuildingController@api_index');
Route::get('/buildings', 'BuildingController@index');
Route::get('/building/create', 'BuildingController@create');
Route::get('/building/show/{property}', 'BuildingController@show');
Route::get('/building/edit/{building}', 'BuildingController@edit');
Route::post('/building/store', 'BuildingController@store');
Route::post('/building/update', 'BuildingController@update');
Route::post('/building/remove', 'BuildingController@remove');
Route::post('/building/updatenames', 'BuildingController@updatenames');
Route::post('/building/updateaddress', 'BuildingController@updateaddress');
Route::post('/building/updatefacilities', 'BuildingController@updatefacilities');
Route::delete('/building/{building}', 'BuildingController@destroy');
Route::get('/building/geoapitest', 'BuildingController@geoapitest');

Route::get('/districts', 'DistrictController@index');
Route::any('/districts/option/reorder', 'DistrictController@option_reorder');
Route::any('/districts/select', 'DistrictController@frontend_select');
Route::any('/districts/export', 'DistrictController@frontend_export');

Route::get('/vendors/api/{type}/index', 'VendorController@api_index');
Route::get('/vendors/api/child/{parent_id}', 'VendorController@api_child_index');
Route::get('/vendors', 'VendorController@index');
Route::get('/vendor/create', 'VendorController@create');
Route::get('/vendor/show/{property}', 'VendorController@show');
Route::get('/vendor/edit/{vendor}', 'VendorController@edit');
Route::post('/vendor/store', 'VendorController@store');
Route::post('/vendor/update', 'VendorController@update');
Route::post('/vendor/remove', 'VendorController@remove');
Route::delete('/vendor/{vendor}', 'VendorController@destroy');
Route::get('/vendor/create-rep/{owner}', 'VendorController@create_rep');
Route::get('/vendor/create-agent/{agent}', 'VendorController@create_agent');
Route::get('/vendor/phonesearch/{phone}', 'VendorController@phonesearch');

Route::get('/clients', 'ClientController@index');
Route::get('/client/create', 'ClientController@create');
Route::get('/client/show/{property}', 'ClientController@show');
Route::get('/client/edit/{client}', 'ClientController@edit');
Route::post('/client/store', 'ClientController@store');
Route::post('/client/api/store', 'ClientController@api_store');
Route::post('/client/update', 'ClientController@update');
Route::delete('/client/{client}', 'ClientController@destroy');
Route::post('/client/docupload/{clientid}', 'ClientController@docstoreupload');
Route::get('/client/showdoc/{docid}', 'ClientController@showdoc');
Route::get('/client/showpic/{docid}', 'ClientController@showpic');
Route::get('/client/removedoc/{docid}/{clientid}', 'ClientController@removedoc');
Route::get('/client/check_client', 'ClientController@check_client');
Route::get('/client/api/lookup', 'ClientController@client_lookup');

Route::any('/client/{client}/store-shortlist', 'ClientController@store_shortlist');

/* todo fix controller */
Route::any('/shortlist/manage/{shortlist}', 'ClientController@manage_shortlist_start');
Route::any('/shortlist/manage-end', 'ClientController@manage_shortlist_end');
Route::any('/shortlist/update/{shortlist}', 'ClientController@manage_shortlist_update');
Route::any('/shortlist/show/{shortlist}', 'ClientController@shortlist_index');
Route::any('/shortlist/print/{shortlist}', 'ClientController@shortlist_print');
Route::any('/shortlist/store/{shortlist}', 'ClientShortlistController@store');
Route::any('/shortlist/show-order/{shortlist}', 'ClientShortlistController@show_order');
// Route::any('/shortlist/show-order/{shortlist}', 'ClientController@shortlist_index');
Route::any('/shortlists', 'ClientShortlistController@index');
Route::any('/allshortlists', 'ClientShortlistController@indexall');
Route::get('/shortlists/api/index', 'ClientShortlistController@api_index');
Route::any('/shortlist/option/toggle-type', 'ClientShortlistController@api_toggle_type');
Route::any('/shortlist/option/print', 'ClientShortlistController@option_print');
Route::any('/shortlist/option/printagent', 'ClientShortlistController@option_print_agent');
Route::any('/shortlist/option/delete', 'ClientShortlistController@option_select');
Route::any('/shortlist/option/reorder', 'ClientShortlistController@option_reorder');
Route::any('/shortlist/addtoview/{shortlist}', 'ClientShortlistController@addtoview');
Route::any('/shortlist/addtoview/{shortlist}/{vid}', 'ClientShortlistController@addtoviewid');

Route::any('/viewing/create/{shortlist}', 'ClientViewingController@create');
Route::any('/viewing/store', 'ClientViewingController@store');
Route::any('/viewing/update/{viewing}', 'ClientViewingController@update');
Route::any('/viewing/remove/{viewing}', 'ClientViewingController@remove');
Route::any('/viewing/show/{viewing}', 'ClientViewingController@show');
Route::any('/viewing/show-order/{viewingid}', 'ClientViewingController@show_order');
Route::any('/viewing/option/toggle-type/{viewing}', 'ClientViewingController@api_toggle_type');
Route::any('/viewing/option/print/{viewing}', 'ClientViewingController@option_print');
Route::any('/viewing/option/printagent/{viewing}', 'ClientViewingController@option_print_agent');
Route::any('/viewing/option/delete/{viewing}', 'ClientViewingController@option_remove');
Route::any('/viewing/option/reorder', 'ClientViewingController@option_reorder');

Route::any('/photoshoot/create', 'PhotoshootController@create');
Route::any('/photoshoot/store', 'PhotoshootController@store');
Route::any('/photoshoot/update/{photoshoot}', 'PhotoshootController@update');
Route::any('/photoshoot/remove/{photoshoot}', 'PhotoshootController@remove');
Route::any('/photoshoot/show/{photoshoot}', 'PhotoshootController@show');
Route::any('/photoshoot/show-order/{photoshootid}', 'PhotoshootController@show_order');
Route::any('/photoshoot/option/toggle-type/{photoshoot}', 'PhotoshootController@api_toggle_type');
Route::any('/photoshoot/option/delete/{photoshoot}', 'PhotoshootController@option_remove');
Route::any('/photoshoot/option/reorder', 'PhotoshootController@option_reorder');
Route::any('/photoshoot/addtoshoot/{vid}', 'PhotoshootController@addtoshoot');
Route::any('/photoshoot/main/delete', 'PhotoshootController@main_remove');
Route::any('/photoshoot/option/print', 'PhotoshootController@option_print');
Route::any('/photoshoot/option/printagent', 'PhotoshootController@option_print_agent');


Route::get('/resources', 'ResourceController@index');
Route::get('/resources/pdf/edit', 'ResourceController@editPdfTutorials');
Route::post('/resources/pdf/save', 'ResourceController@savePdfTutorial');
Route::get('/resources/pdf/remove/{id}', 'ResourceController@removePdfTutorial');
Route::get('/resources/{key}/edit', 'ResourceController@editPersonal');
Route::post('/resources/{key}/save', 'ResourceController@saveResource');
Route::any('/pdfresource/{pdf}', 'ResourceController@loadpdf');


Route::get('/home', 'HomeController@index');
//Route::get('/dbtest', 'DbController@index');



//new routes
Route::get('/comments/{id}', 'PropertyController@getcomments');
Route::post('/commentadd/{id}', 'PropertyController@addcomment');
Route::post('/commentremove/{id}', 'PropertyController@removecomment');

Route::get('/list', 'PropertyController@propertylist');
Route::any('/listsearch', 'PropertyController@propertylistsearch');
Route::any('/listadvsearch', 'PropertyController@propertylistadvsearch');

Route::get('/showcontent/{id}', 'PropertyController@getshowcontent');
Route::get('/showinformation/{id}', 'PropertyController@getshowinformation');
Route::get('/showpics/{id}', 'PropertyController@getshowpics');
Route::get('/showpicsoriginal/{id}', 'PropertyController@getshowpicsoriginal');
Route::get('/showcontact/{id}', 'PropertyController@getshowcontact');
Route::get('/showpropertyclients/{id}', 'PropertyController@getpropertyclients');
Route::get('/getpiclinks/{id}', 'PropertyController@getpiclinks');

Route::get('/vendor/comments/{id}', 'VendorController@getcomments');
Route::post('/vendor/commentadd/{id}', 'VendorController@addcomment');
Route::post('/vendor/commentremove/{id}', 'VendorController@removecomment');

Route::get('/property/changes/{id}', 'PropertyController@getchanges');
Route::get('/property/leasesales/{id}', 'PropertyController@getleasesales');

Route::get('/buildings/comments/{id}', 'BuildingController@getcomments');
Route::post('/buildings/commentadd/{id}', 'BuildingController@addcomment');
Route::post('/buildings/commentremove/{id}', 'BuildingController@removecomment');

Route::get('/client/comments/{id}', 'ClientController@getcomments');
Route::post('/client/commentadd/{id}', 'ClientController@addcomment');
Route::post('/client/commentremove/{id}', 'ClientController@removecomment');

Route::get('/documents', 'DocumentsController@index');
Route::get('/documents/{shortlistid}/offerletter/{propertyid}/{revision}', 'DocumentsController@offerletteredit');
Route::post('/documents/offerletter', 'DocumentsController@offerletterupdate');
Route::get('/documents/{shortlistid}/offerletternewversion/{propertyid}', 'DocumentsController@offerletternewversion');
Route::get('/documents/{shortlistid}/offerletterpdf/{propertyid}/{revision}', 'DocumentsController@offerletterpdf');
Route::get('/documents/{shortlistid}/tenancyagreement/{propertyid}', 'DocumentsController@tenancyagreementedit');
Route::post('/documents/tenancyagreement', 'DocumentsController@tenancyagreementupdate');
Route::get('/documents/{shortlistid}/tenancyagreementpdf/{propertyid}', 'DocumentsController@tenancyagreementpdf');
Route::get('/documents/{shortlistid}/landlordfeeletter/{propertyid}', 'DocumentsController@landlordfeeletteredit');
Route::post('/documents/landlordfeeletter', 'DocumentsController@landlordfeeletterupdate');
Route::get('/documents/{shortlistid}/landlordfeeletterpdf/{propertyid}', 'DocumentsController@landlordfeeletterpdf');
Route::get('/documents/{shortlistid}/landlordfeeletterpdf/{propertyid}/{logo}', 'DocumentsController@landlordfeeletterpdf');
Route::get('/documents/{shortlistid}/tenantfeeletter/{propertyid}', 'DocumentsController@tenantfeeletteredit');
Route::post('/documents/tenantfeeletter', 'DocumentsController@tenantfeeletterupdate');
Route::get('/documents/{shortlistid}/tenantfeeletterpdf/{propertyid}', 'DocumentsController@tenantfeeletterpdf');
Route::get('/documents/{shortlistid}/tenantfeeletterpdf/{propertyid}/{logo}', 'DocumentsController@tenantfeeletterpdf');
Route::get('/documents/{shortlistid}/letterofundertaking/{propertyid}', 'DocumentsController@letterofundertakingedit');
Route::post('/documents/letterofundertaking', 'DocumentsController@letterofundertakingupdate');
Route::get('/documents/{shortlistid}/letterofundertakingpdf/{propertyid}', 'DocumentsController@letterofundertakingpdf');
Route::get('/documents/{shortlistid}/letterofundertakingpdf/{propertyid}/{logo}', 'DocumentsController@letterofundertakingpdf');
Route::get('/documents/{shortlistid}/holdingdepositletter/{propertyid}', 'DocumentsController@holdingdepositletteredit');
Route::post('/documents/holdingdepositletter', 'DocumentsController@holdingdepositletterupdate');
Route::get('/documents/{shortlistid}/holdingdepositletterpdf/{propertyid}', 'DocumentsController@holdingdepositletterpdf');
Route::get('/documents/{shortlistid}/holdingdepositletterpdf/{propertyid}/{logo}', 'DocumentsController@holdingdepositletterpdf');
Route::get('/documents/{shortlistid}/utilityaccounts/{propertyid}', 'DocumentsController@utilityaccountsedit');
Route::post('/documents/utilityaccounts', 'DocumentsController@utilityaccountsupdate');
Route::get('/documents/{shortlistid}/utilityaccountspdf/{propertyid}', 'DocumentsController@utilityaccountspdf');
Route::get('/documents/{shortlistid}/handoverreport/{propertyid}', 'DocumentsController@handoverreportedit');
Route::post('/documents/handoverreport', 'DocumentsController@handoverreportupdate');
Route::get('/documents/{shortlistid}/handoverreportpdf/{propertyid}', 'DocumentsController@handoverreportpdf');
Route::get('/documents/{shortlistid}/handoverreportleasepdf/{propertyid}', 'DocumentsController@handoverreportleasepdf');
Route::get('/documents/{shortlistid}/handoverreportext/{propertyid}', 'DocumentsController@handoverreportextedit');
Route::post('/documents/handoverreportext', 'DocumentsController@handoverreportextupdate');
Route::get('/documents/{shortlistid}/handoverreportextpdf/{propertyid}', 'DocumentsController@handoverreportextpdf');
Route::get('/documents/{shortlistid}/handoverreportextleasepdf/{propertyid}', 'DocumentsController@handoverreportextleasepdf');
Route::get('/documents/index-lease/{shortlistid}', 'DocumentsController@indexlease');
Route::get('/documents/index-sale/{shortlistid}', 'DocumentsController@indexsale');
Route::get('/documents/{shortlistid}/docaddproperty/{propertyid}', 'DocumentsController@docaddproperty');
Route::get('/documents/{shortlistid}/docdelproperty/{propertyid}', 'DocumentsController@docdelproperty');
Route::post('/documents/{shortlistid}/docsavecomments', 'DocumentsController@docsavecomments');
Route::post('/documents/{shortlistid}/docstoreupload', 'DocumentsController@docstoreupload');
Route::get('/documents/showdoc/{docid}', 'DocumentsController@showdoc');
Route::get('/documents/removedoclease/{docid}/{documentid}', 'DocumentsController@removedoclease');
Route::get('/documents/removedocsale/{docid}/{documentid}', 'DocumentsController@removedocsale');
Route::get('/documents/{shortlistid}/coverletter/{propertyid}', 'DocumentsController@coverletteredit');
Route::post('/documents/coverletter', 'DocumentsController@coverletterupdate');
Route::get('/documents/{shortlistid}/coverletterpdf/{propertyid}', 'DocumentsController@coverletterpdf');
Route::get('/documents/{shortlistid}/coverletterpdf/{propertyid}/{logo}', 'DocumentsController@coverletterpdf');
Route::get('/documents/{shortlistid}/sideletter/{propertyid}', 'DocumentsController@sideletteredit');
Route::post('/documents/sideletter', 'DocumentsController@sideletterupdate');
Route::get('/documents/{shortlistid}/sideletterpdf/{propertyid}', 'DocumentsController@sideletterpdf');
Route::get('/documents/{shortlistid}/sideletterpdf/{propertyid}/{logo}', 'DocumentsController@sideletterpdf');
Route::get('/documents/{shortlistid}/customletter/{propertyid}', 'DocumentsController@customletteredit');
Route::post('/documents/customletter', 'DocumentsController@customletterupdate');
Route::get('/documents/{shortlistid}/customletterpdf/{propertyid}', 'DocumentsController@customletterpdf');
Route::get('/documents/{shortlistid}/customletterpdf/{propertyid}/{logo}', 'DocumentsController@customletterpdf');

Route::get('/commission/readyforinvoicing/{shortlistid}', 'CommissionController@readyforinvoicing');
Route::post('/commission/readyforinvoicing', 'CommissionController@storereadyforinvoicing');
Route::get('/commission/invoicing', 'CommissionController@invoicing');
Route::get('/commission/invoiceedit/{id}', 'CommissionController@invoiceedit');
Route::post('/commission/invoiceedit/{id}', 'CommissionController@invoicestore');
Route::get('/commission/comments/{id}', 'CommissionController@getcomments');
Route::post('/commission/commentadd/{id}', 'CommissionController@addcomment');
Route::post('/commission/commentremove/{id}', 'CommissionController@removecomment');
Route::post('/commission/docupload/{commissionid}', 'CommissionController@docstoreupload');
Route::get('/commission/showdoc/{docid}', 'CommissionController@showdoc');
Route::get('/commission/removedoc/{docid}/{invoiceid}', 'CommissionController@removedoc');
Route::post('/commission/commconsedit/{id}', 'CommissionController@commconsedit');
Route::get('/commission/commconsnew/{id}', 'CommissionController@commconsnew');
Route::get('/commission/commconsrem/{id}', 'CommissionController@commconsrem');
Route::get('/commission/targetusers', 'CommissionController@targetusers');
Route::get('/commission/targetuser/{id}', 'CommissionController@targetuser');
Route::get('/commission/edittargets/{id}/{year}', 'CommissionController@edittargets');
Route::post('/commission/edittargets/{id}/{year}', 'CommissionController@storetargets');
Route::post('/commission/notifyaccountant', 'CommissionController@notifyaccountant');
Route::get('/commission/createinvoices/{commissionid}', 'CommissionController@createinvoices');
Route::post('/commission/createinvoices/{commissionid}', 'CommissionController@createinvoicesstore');
Route::get('/commission/approval', 'CommissionController@approval');
Route::get('/commission/approve/{invoiceid}', 'CommissionController@approveinvoice');
Route::get('/commission/invoicenote', 'CommissionController@invoicenote');
Route::get('/commission/invoicenoted/{invoiceid}', 'CommissionController@invoicenoted');
Route::get('/commission/invoice/{invoiceid}', 'CommissionController@downloadinvoice');
Route::get('/commission/invoice/{invoiceid}/{logo}', 'CommissionController@downloadinvoice');
Route::get('/commission/editinvoice/{invoiceid}', 'CommissionController@editinvoice');
Route::post('/commission/editinvoice/{invoiceid}', 'CommissionController@editinvoicestore');
Route::post('/commission/editinvoicecomment', 'CommissionController@editinvoicecommentstore');
Route::get('/commission/geninvoicing', 'CommissionController@geninvoicing');
Route::get('/commission/creategeninvoices', 'CommissionController@creategeninvoices');
Route::post('/commission/creategeninvoices', 'CommissionController@creategeninvoicesstore');
Route::get('/commission/editgeninvoices/{invoiceid}', 'CommissionController@editgeninvoices');
Route::post('/commission/editgeninvoices/{invoiceid}', 'CommissionController@editgeninvoicesstore');
Route::get('/commission/geninvoice/{invoiceid}', 'CommissionController@downloadgeninvoice');
Route::get('/commission/invoicescsv', 'CommissionController@invoicescsv');
Route::get('/commission/csvinvoicedownload/{year}/{month}', 'CommissionController@csvinvoicedownload');
Route::get('/commission/propertyavailabledate', 'CommissionController@leased_properties');

Route::get('/commission/salaryusers', 'CommissionController@salaryusers');
Route::get('/commission/salaries/{userid}', 'CommissionController@salaries');
Route::get('/commission/salariescsv', 'CommissionController@salariescsv');
Route::get('/commission/csvsalarydownload/{year}/{month}', 'CommissionController@csvsalarydownload');
Route::get('/commission/createsalary/{userid}/{year}/{month}', 'CommissionController@createsalary');
Route::get('/commission/editsalary/{userid}/{year}/{month}', 'CommissionController@editsalary');
Route::post('/commission/editsalary/{userid}/{year}/{month}', 'CommissionController@editsalarystore');
Route::get('/commission/pdfsalary/{userid}/{year}/{month}', 'CommissionController@pdfsalary');
Route::get('/commission/pdfcommissionlist/{userid}/{year}/{month}', 'CommissionController@pdfcommissionlist');
Route::get('/commission/mysalaries', 'CommissionController@mysalaries');
Route::get('/commission/bonusdividends', 'CommissionController@bonusdividends');
Route::get('/commission/createbonusdividend', 'CommissionController@createbonusdividend');
Route::post('/commission/createbonusdividend', 'CommissionController@createbonusdividendstore');
Route::get('/commission/editbonusdividend/{id}', 'CommissionController@editbonusdividend');
Route::post('/commission/editbonusdividend/{id}', 'CommissionController@editbonusdividendstore');
Route::get('/commission/bonusdividendpdf/{id}', 'CommissionController@downloadbonusdividend');

Route::get('/commission/expensesusers', 'CommissionController@expensesusers');
Route::get('/commission/expenses/{userid}', 'CommissionController@expenses');
Route::get('/commission/createexpenses/{userid}/{year}/{month}', 'CommissionController@createexpenses');
Route::get('/commission/editexpenses/{userid}/{year}/{month}', 'CommissionController@editexpenses');
Route::post('/commission/editexpenses/{userid}/{year}/{month}', 'CommissionController@editexpensesstore');
Route::get('/commission/pdfexpenses/{userid}/{year}/{month}', 'CommissionController@pdfexpenses');
Route::get('/commission/myexpenses', 'CommissionController@myexpenses');

//Route::get('/commission/checkchanges/{id}', 'CommissionController@checkchanges');
Route::get('/commission/checkinvoices', 'CommissionController@checkinvoices');

Route::get('/dashboardnew', 'LeadsController@dashboard');
Route::get('/dashboardnew/{consultantid}', 'LeadsController@dashboard');
Route::get('/dashboardnew/{consultantid}/{year}', 'LeadsController@dashboard');
Route::get('/companyoverview', 'LeadsController@companyoverview');
Route::get('/companyoverview_ajx', 'LeadsController@companyoverview_ajx');
Route::get('/companyoverview_ajx/{year}', 'LeadsController@companyoverview_ajx');
Route::get('/companyoverview/outstanding-payments', 'LeadsController@outstandingpayments');
Route::get('/companyoverview/outstanding-payments/{year}', 'LeadsController@outstandingpayments');
Route::get('/companyoverview/{year}', 'LeadsController@companyoverview');
Route::get('/companydbusage', 'LeadsController@companydbusage');
Route::get('/companydbusage/{yearmonth}', 'LeadsController@companydbusage');
Route::get('/companydbusage/{yearmonth}/{userid}', 'LeadsController@companydbusage');
Route::get('/companydbtime', 'LeadsController@companydbtime');
Route::get('/companydbtime/{yearmonth}', 'LeadsController@companydbtime');
Route::get('/companydbtime/{yearmonth}/{userid}', 'LeadsController@companydbtime');
Route::get('/companyoverviewuser/{consultantid}/{year}', 'LeadsController@companyoverviewuser');
Route::get('/companyoverview/offersfetch/{consultantid}/{year}/{yearlist}', 'LeadsController@offersfetch');
Route::get('/companyoverview/offers/{consultantid}/{year}/{yearlist}', 'LeadsController@offers');
Route::get('/market-insight', 'LeadsController@offersbrackets');
Route::get('/market-insight/{year}', 'LeadsController@offersbrackets');
Route::get('/market-insight/{year}/{month}', 'LeadsController@offersbrackets');
Route::get('/companyoverview/consultantdeals/{consultantid}/{year}', 'LeadsController@consultantdeals');
Route::get('/companyoverview/consultantdeals/{consultantid}/{month}/{year}', 'LeadsController@consultantdeals');
Route::get('/companyoverview/{from}/{to}', 'LeadsController@companyoverview');


Route::get('/lead/newlead/{clientid}', 'LeadsController@newlead');
Route::post('/lead/store', 'LeadsController@createlead');
Route::get('/lead/edit/{id}', 'LeadsController@edit');
Route::post('/lead/edit/{id}', 'LeadsController@update');
Route::get('/lead/addshortlist/{id}/{shortlistid}', 'LeadsController@addshortlist');
Route::get('/lead/removeshortlist/{id}', 'LeadsController@removeshortlist');
Route::get('/lead/createshortlist/{id}', 'LeadsController@createshortlist');
Route::get('/lead/comments/{id}', 'LeadsController@getcomments');
Route::post('/lead/commentadd/{id}', 'LeadsController@addcomment');
Route::post('/lead/commentremove/{id}', 'LeadsController@removecomment');
Route::get('/lead/changes/{id}', 'LeadsController@getchanges');
Route::get('/lead/completed/{consultantid}', 'LeadsController@completed');
Route::get('/lead/dead/{consultantid}', 'LeadsController@dead');
Route::get('/lead/deleted/{consultantid}', 'LeadsController@deleted');
Route::post('/lead/updategooglelink/{id}', 'LeadsController@updateGoogleLink');
Route::get('/lead/removegooglelink/{id}', 'LeadsController@removeGoogleLink');

Route::get('/csvindex', 'CsvimportController@csvindex');
Route::post('/csvupload', 'CsvimportController@csvupload');
Route::get('/csvuploadshow/{cid}', 'CsvimportController@csvuploadshow');
Route::get('/csvpshow/{id}', 'CsvimportController@csvpshow');
Route::post('/csvstatusupdate/{id}', 'CsvimportController@csvstatusupdate');
Route::post('/csvnewproperty/{id}', 'CsvimportController@csvnewproperty');
Route::post('/csvupdateproperty/{id}', 'CsvimportController@csvupdateproperty');

Route::get('/websitenquiries', 'LeadsController@websitenquiries');
Route::get('/websitenquiriesall', 'LeadsController@websitenquiriesall');
Route::get('/websitenquiriescreatecontact/{id}', 'LeadsController@websitenquiriescreatecontact');
Route::get('/websitenquiriescreateproperty/{id}', 'LeadsController@websitenquiriescreateproperty');
Route::get('/websitenquiriesshowcontact/{id}', 'LeadsController@websitenquiriesshowcontact');
Route::get('/websitenquiriesshowproperty/{id}', 'LeadsController@websitenquiriesshowproperty');
Route::post('/websitenquiriescreatecontact/{id}', 'LeadsController@websitenquiriespassoncontact');
Route::post('/websitenquiriescreateproperty/{id}', 'LeadsController@websitenquiriespassonproperty');
Route::get('/websitenquiriesremovecontact/{id}', 'LeadsController@websitenquiriesremovecontact');
Route::get('/websitenquiriesfinishcontact/{id}', 'LeadsController@websitenquiriesfinishcontact');
Route::get('/websitenquiriesfinishproperty/{id}', 'LeadsController@websitenquiriesfinishproperty');
Route::get('/websitenquiriesremoveproperty/{id}', 'LeadsController@websitenquiriesremoveproperty');
Route::get('/websitenquiriesderemovecontact/{id}', 'LeadsController@websitenquiriesderemovecontact');
Route::get('/websitenquiriesderemoveproperty/{id}', 'LeadsController@websitenquiriesderemoveproperty');
Route::get('/websitenquiriesinfo', 'LeadsController@websitenquiriesinfo');
Route::get('/websitenquiriesforwarded', 'LeadsController@websitenquiriesforwarded');
Route::get('/lead/enterclient/{id}', 'LeadsController@enterclient');
Route::post('/lead/enterclient/{id}', 'LeadsController@enterclientsave');
Route::get('/iopropertycreate', 'LeadsController@iopropertycreate');
Route::post('/iopropertycreate', 'LeadsController@storeiopropertycreate');
Route::get('/iopropertyshow/{id}', 'LeadsController@iopropertyshow');
Route::post('/iopropertycopyimgs', 'LeadsController@iopropertycopyimgs');
Route::get('/iopropertyfinish/{id}', 'LeadsController@iopropertyfinish');



Route::get('/websiteenquiryproperty/comments/{id}', 'LeadsController@getcommentsproperty');
Route::post('/websiteenquiryproperty/commentadd/{id}', 'LeadsController@addcommentproperty');
Route::post('/websiteenquiryproperty/commentremove/{id}', 'LeadsController@removecommentproperty');
Route::get('/websiteenquirycontact/comments/{id}', 'LeadsController@getcommentscontact');
Route::post('/websiteenquirycontact/commentadd/{id}', 'LeadsController@addcommentcontact');
Route::post('/websiteenquirycontact/commentremove/{id}', 'LeadsController@removecommentcontact');

Route::get('/calendar', 'CalendarController@currentmonth');
Route::get('/carbooking', 'CalendarController@carbookings');
Route::post('/calendar', 'CalendarController@addcurrentmonth');
Route::get('/calendar/edit/{id}/{mode}', 'CalendarController@editcalendar');
Route::post('/calendar/edit/{id}/{mode}', 'CalendarController@changecalendar');
Route::post('/calendar/addhandover', 'CalendarController@addhandover');
Route::get('/calendar/remove/{id}', 'CalendarController@removecalendar');
Route::get('/calendar/remove/{id}/{mode}', 'CalendarController@removecalendar');
Route::get('/calendar/approve/{id}', 'CalendarController@approvecalendar');
Route::get('/calendar/approve/{id}/{mode}', 'CalendarController@approvecalendar');
Route::get('/calendar/noted/{id}', 'CalendarController@notedcalendar');
Route::get('/calendar/carbooking/{date}/{id}', 'CalendarController@carbooking');
Route::get('/calendar/carbooking_check/{date}/{id}/{timefrom}/{timeto}/{car}', 'CalendarController@carbooking_check');
Route::get('/calendar/viewing_duration_check/{date}/{numOfProp}/{timefrom}/{timeto}', 'CalendarController@viewing_duration_check');
Route::get('/calendar/carbookingphoto/{date}/{id}', 'CalendarController@carbookingphoto');
Route::get('/calendar/{month}/{year}', 'CalendarController@showthemonth');
Route::post('/calendar/{month}/{year}', 'CalendarController@addshowthemonth');
Route::get('/vacationlist', 'CalendarController@vacationlist');
Route::get('/vacationlists', 'CalendarController@vacationlists');
Route::get('/directorcalendar', 'CalendarController@currentmonthdirectorcalendar');
Route::post('/directorcalendar', 'CalendarController@addcurrentmonth');
Route::get('/directorcalendar/{month}/{year}', 'CalendarController@showthemonthdirectorcalendar');
Route::post('/directorcalendar/{month}/{year}', 'CalendarController@addshowthemonth');

Route::get('/basedocuments', 'BasedocumentsController@index');
Route::get('/basedocuments/blankletter/{docid}', 'BasedocumentsController@blankletteredit');
Route::post('/basedocuments/blankletter', 'BasedocumentsController@blankletterupdate');
Route::get('/basedocuments/blankletterpdf/{docid}', 'BasedocumentsController@blankletterpdf');
Route::get('/basedocuments/blankletterpdf/{docid}/{logo}', 'BasedocumentsController@blankletterpdf');
Route::get('/basedocuments/employmentletter/{docid}', 'BasedocumentsController@employmentletteredit');
Route::post('/basedocuments/employmentletter', 'BasedocumentsController@employmentletterupdate');
Route::get('/basedocuments/employmentletterpdf/{docid}', 'BasedocumentsController@employmentletterpdf');
Route::get('/basedocuments/employmentletterpdf/{docid}/{logo}', 'BasedocumentsController@employmentletterpdf');
Route::get('/basedocuments/dismissalletter/{docid}', 'BasedocumentsController@dismissalletteredit');
Route::post('/basedocuments/dismissalletter', 'BasedocumentsController@dismissalletterupdate');
Route::get('/basedocuments/dismissalletterpdf/{docid}', 'BasedocumentsController@dismissalletterpdf');
Route::get('/basedocuments/dismissalletterpdf/{docid}/{logo}', 'BasedocumentsController@dismissalletterpdf');
Route::get('/basedocuments/letterofemployment/{docid}', 'BasedocumentsController@letterofemploymentedit');
Route::post('/basedocuments/letterofemployment', 'BasedocumentsController@letterofemploymentupdate');
Route::get('/basedocuments/letterofemploymentpdf/{docid}', 'BasedocumentsController@letterofemploymentpdf');
Route::get('/basedocuments/letterofemploymentpdf/{docid}/{logo}', 'BasedocumentsController@letterofemploymentpdf');

Route::get('/informationofficer/commission', 'IOCommissionController@index');
Route::post('/informationofficer/commission/update/{id}', 'IOCommissionController@update');
//Route::get('/informationofficer/properties', 'IOCommissionController@properties');

Route::get('/xerologs', 'XeroLogsController@index');
