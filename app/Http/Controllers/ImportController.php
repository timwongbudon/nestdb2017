<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyMeta;
use App\PropertyMedia;
use App\PropertyFeature;
use App\Import;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;
use App\Repositories\PropertyRepository;

use Log;
use Plupload;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
		}
    }

    public function index()
    {
        $imports = Import::orderBy('id', 'desc')->paginate(20);
        $formats = $this->get_saved_formats();
        return view('imports.index', [
            'imports' => $imports,
        ])->with('formats', $formats);
    }

    private function get_saved_formats() {
        $formats = Import::select('id', 'name', 'filename')->orderBy('id', 'desc')->limit(100)->get();
        $res = array();
        if (!empty($formats)) {
            foreach ($formats as $format) {
                $res[$format->id] = $format->name . ' - ' . $format->filename;
            }
        }
        return $res;
    }

    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
	        $file = Input::file('file');
            $results = \Excel::load($file, function($reader) {})->get();
	        $fields = \NestImportFileHelper::nest_excel_get_fields($results);
	        \NestImportFileHelper::nest_excel_remove_empty_line($results, $fields[0]);

            $preference = '';
            if(!empty($request->format)) {
                $import = Import::find($request->format);
                $preference = $import->preference;
            }

            if (!empty($results)) {
				$file_json_encode = json_encode($results);
				$filename = sprintf("%d-%s", \NestDate::now(), $file->getClientOriginalName());
				$file->move(storage_path('excel') , $filename);
				$import = $request->user()->imports()->create([
                        'name'           => $request->name,
                        'filename'       => $filename,
                        'encoded_string' => $file_json_encode,
                        'comments'       => isset($request->comments)?$request->comments:'',
                        'preference'     => $preference,
                        'web'            => $request->web,
					]);
			} else {
				die('no result');
			}
        }
        if (!empty($import->id)) {
        	return redirect('/import/edit/' . $import->id);
        } else {
        	return redirect('/import');
        }
    }

    public function edit($id)
    {
        $import = Import::findOrFail($id);
        $results = json_decode($import->encoded_string);
        $fields = \NestImportFileHelper::nest_excel_get_fields($results);
        $options = Import::available_options();
        $preference = json_decode($import->preference);
        return view('imports.edit', compact('import'))
            ->with('fields', $fields)
            ->with('results', $results)
            ->with('options', $options)
            ->with('preference', $preference)
            ->with('previous_url', \NestRedirect::previous_url());
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $preference = !empty($data['fields'])?json_encode($data['fields']):json_encode(array());
        $import = Import::findOrFail($request->id);
        $results = json_decode($import->encoded_string);
        $fields = \NestImportFileHelper::nest_excel_get_fields($results);
        $settings = $data['fields'];
        if(!empty($results))foreach ($results as $key => $row) {
            $ImportObject = $this->prepare_import_object($row, $fields, $settings, $import->name, $import->web);
            $this->insert_import_object($ImportObject);
        }
        $import->preference = $preference;
        $import->save();
        return redirect('/imports/');
    }

    private function prepare_import_object($row, $fields, $settings, $import_name, $is_web=0) {
        $ImportObject = new ImportObject;
        foreach ($settings as $key => $field) {
            if (!empty($field) && !is_null($row->$fields[$key])) {
                if (!ImportObject::is_multiple_field($field)) {
                    if (!ImportObject::is_exception_field($field)) {
                        $ImportObject->{$field} = $row->$fields[$key];
                    } else {
                        $func = "exception_" . $field;
                        $ImportObject->{$field} = ImportObject::$func($row->$fields[$key]);
                    }
                } else {
                    $_field = ImportObject::multiple_field_filter($field);
                    $ImportObject->{$_field}[] = $row->$fields[$key];
                }
            }
        }
        $ImportObject->user_id = 1;//admin
        $ImportObject->source = $import_name;
        $ImportObject->display_name = $ImportObject->name;
        if (!isset($ImportObject->country_code)) {
            $ImportObject->country_code = 'hk';//default hk.
        }
        $ImportObject->web = $is_web?1:0;
        return $ImportObject;
    }

    private function insert_import_object($ImportObject)
    {
        $Property = new Property;
        $_property = (array)$ImportObject;
        $property = $Property->create($_property);

        $feature = new PropertyFeature();
        $meta = new PropertyMeta();
        if (isset($_property['features'])) {
            $encoded_features = json_encode($_property['features']);
            $meta->add_post_meta($property->id, 'features', $encoded_features, true); 
            $feature->add_options($property->id, $_property['features']);
        }
        if (isset($_property['coordinates'])) {
            $meta->add_post_meta($property->id, 'coordinates', $_property['coordinates'], true); 
        }
        if (isset($_property['featured_image'])) {
            $meta->add_post_meta($property->id, 'featured_image', $_property['featured_image'], true); 
        }
        if (isset($_property['nest_photos'])) {
            foreach ($_property['nest_photos'] as $photo_url) {
                $media = new PropertyMedia;
                $media->add_media(array(
                        'property_id' => $property->id,
                        'group'       => 'nest-photo',
                        'url'         => $photo_url,
                        'filename'    => '',
                    ));
            }
        }
        if (isset($_property['other_photos'])) {
            foreach ($_property['other_photos'] as $photo_url) {
                $media = new PropertyMedia;
                $media->add_media(array(
                        'property_id' => $property->id,
                        'group'       => 'other-photo',
                        'url'         => $photo_url,
                        'filename'    => '',
                    ));
            }
        }
        unset($ImportObject);
    }
}

class ImportObject
{
    public static function is_multiple_field($string) {
        return preg_match("/.+\[\]/", $string);
    }
    public static function multiple_field_filter($string) {
        return preg_match("/(.+)\[\]/", $string, $matches)?$matches[1]:'';
    }
    public static function is_exception_field($string) {
        return in_array($string, array('features'));
    }
    public static function exception_features($value) {
        return !empty($value)?explode(',',$value):array();
    }
}