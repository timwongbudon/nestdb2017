<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

use App\ClientShortlist;
use App\Property;
use App\User;
use App\Client;
use App\Document;
use App\Documentinfo;
use App\Commission;
use App\Commcons;
use App\Comminvoice;
use App\Invoicedocuments;
use App\Targets;
use App\Salary;
use App\Expenses;
use App\Bonusdividend;
use App\IOCommission;
use App\XeroLogs;
use App\LogInvoicing;


use View;
use App\Lead;
use Carbon\Carbon;
use App\LogClick;

class CommissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');

		$user = \Auth::user();

		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(11);

			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}

			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		return redirect('/shortlists');
    }

	//add amounts for landlord/tenant automatically if only 1 property is available
    public function readyforinvoicing($shortlistid){
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$consultant = User::where('id', $shortlist->user_id)->first();

		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}

		$doc = Documentinfo::where('shortlist_id', $shortlistid)->where('type_id', $typeid)->first();
		if (!$doc){
			return redirect('/shortlists');
		}
		$props = explode('|', $doc->properties);
		$properties = Property::whereIn('id', $props)->get();

		$rows = Document::where('shortlistid', '=', $shortlistid)->get();

		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		$lead = Lead::where('shortlistid','=',$shortlistid)->first();		

		return view('commission.readyforinvoicing', [
			'shortlist' => $shortlist,
			'properties' => $properties,
			'consultants' => $consultants,
			'typeid' => $typeid,
			'lead'=>$lead
		]);
	}

	public function storereadyforinvoicing(Request $request){
		$input = $request->all();
		$user = $request->user();

		$checkShortlist = Commission::where('shortlistid','=',$input['shortlistid'])->first();
		
		if( !isset($checkShortlist->id) ){

			if (isset($input['shortlistid'])){
				$data = array();

				$data['shortlistid'] = intval($input['shortlistid']);
				$data['status'] = 1;
				$data['finishdate'] = date('Y-m-d');

				if (isset($input['consultantid'])){
					$data['consultantid'] = intval($input['consultantid']);
				}
				if (isset($input['clientid'])){
					$data['clientid'] = intval($input['clientid']);
				}
				if (isset($input['propertyid'])){
					$data['propertyid'] = intval($input['propertyid']);
				}
				if (isset($input['salelease'])){
					$data['salelease'] = intval($input['salelease']);
				}
				if (isset($input['stamping'])){
					$data['stamping'] = intval($input['stamping']);
				}
				if (isset($input['fullamount'])){
					$data['fullamount'] = floatval($input['fullamount']);
				}
				if (isset($input['landlordamount'])){
					$data['landlordamount'] = floatval($input['landlordamount']);
				}
				if (isset($input['tenantamount'])){
					$data['tenantamount'] = floatval($input['tenantamount']);
				}
				if (isset($input['type'])){
					$data['type'] = trim($input['type']);
				}
				if (isset($input['commissiondate'])){
					$data['commissiondate'] = trim($input['commissiondate']);
				}
				if (isset($input['source'])){
					$data['source'] = trim($input['source']);
				}
				if (isset($input['sd_term'])){
					$data['sd_term'] = intval($input['sd_term']);
				}
				if (isset($input['sd_rent'])){
					$data['sd_rent'] = floatval($input['sd_rent']);
				}
				if (isset($input['sd_keymoney'])){
					$data['sd_keymoney'] = floatval($input['sd_keymoney']);
				}
				if (isset($input['sd_amount'])){
					$data['sd_amount'] = intval($input['sd_amount']);
				}

				if (isset($input['invoicedate'])){
					$data['invoicedate'] = trim($input['invoicedate']);
				}
				$comments = array(
					array(
						'userid' => $user->id,
						'datetime' => time(),
						'content' => trim($input['comment'])
					)
				);
				$data['comment'] = json_encode($comments);

				$comm = Commission::create($data);
				$comm->save();

				$constultantname = '';
				$users = User::all();
				foreach ($users as $u){
					if ($u->id == intval($input['consultantid'])){
						$constultantname = $u->name;
					}
				}
				$client = Client::where('id', intval($input['clientid']))->first();

				//Email to Accountant
				$subject = 'New invoice has been requested';
				$message = 'A consultant has requested a new invoice. Please log into https://db.nest-property.com and create the invoice(s). Thanks!';
				if ($constultantname != ''){
					$subject = 'New invoice has been requested from '.$constultantname;
					$message = $constultantname.' has requested a new invoice. Please log into https://db.nest-property.com and create the invoice(s)';
					if ($client){
						$message .= ' for '.$client->name();
					}
				}
				
				
				$lead = Lead::where('shortlistid', '=', $input['shortlistid'])->first();
				if ($lead){
					$lead->status = 6;
					$lead->save();
				}

				/*///for io commission
				$io_com = IOCommission::where(['property_id'=>$input['propertyid'], 'status'=>'draft'])->first();
				
				//echo "-yes-";
					if($io_com != null){
					//	$io_com = IOCommission::find($io_com->id);
						$io_com->status = 'Pending';
						$io_com->salarydate=$input['commissiondate'];
						$io_com->invoiceId=$comm->id;
						$io_com->save();
						//echo "-yesss-";
					} */
					
				
				//check property if eligible to io commission
				$property = Property::find($input['propertyid']);
				$check = $this->checkchanges($input['propertyid']);			

				$checkIOdata = IOCommission::where(['property_id'=>$input['propertyid'], 'user_id'=>$check['information_officer'], 'owner_id'=>$property->owner_id])->first();
			
				if($check['status'] == 1 && $checkIOdata == null){
					$io_com = new IOCommission;
				
					$io_com->user_id = $check['information_officer'];
					$io_com->property_id = $input['propertyid'];
					$io_com->status = 'Pending';
					$io_com->commission = '500';
					$io_com->owner_id = $property->owner_id;
					$io_com->invoiceId=$comm->id;
					$io_com->salarydate=$input['commissiondate'];				
					$io_com->save();
				}


				\Mail::send('emails.generic', ['body' => $message, 'title' => 'New invoice'], function ($mail) use ($subject) {
					$mail->from('noreply@db.nest-property.com', 'Nest Database');
					$mail->to('accounts@nest-property.com', 'Office Manager')->subject($subject);
				});


			}

			return redirect('/');

		} else {

			return redirect('/commission/invoiceedit/'.$checkShortlist->id);
			
		}
	}

	public function checkchanges($id){
		//$user = $request->user();
	
        $property = Property::findOrFail($id);
        if (empty($property)) {
            return '0';
        }
		
	
		$rows = $property->getLog();

		$propertyUserId = $property->user_id;
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		
		$r = null;

		$returnVal = 0;

		$createdby = json_decode($u[$propertyUserId]->roles);
		$io = "";
	
		
		if($createdby->Informationofficer == true && $u[$propertyUserId]->status == 0){
			$returnVal = 1;
			$io = $propertyUserId;
		}	

		for ($i = 0; $i < $rows->count()-1; $i++){
            $r = $rows[$i];
			$n = array();		
		
            if (($i+1) < $rows->count()){
                $old = $rows[$i+1];

                $roles = json_decode($u[$r->userid]->roles);
				
				///check if IO Officer
                if($roles->Informationofficer == true && $u[$r->userid]->status == 0 ){
					
					if($io==""){
						$io = $r->userid;
					}
					
                    //check if there are changes in the logs for the owner
                    if ($r['owner_id'] != $old['owner_id']){
                        if ($r['owner_id'] == 0){
                        
                        }else{
                            $returnVal = 1;
                        }
                    }
                }
            }
        }

        return array('status'=>$returnVal, 'information_officer'=> $io );

	}
	
	public function checkinvoices(){

		if(isset($_GET['user']) && $_GET['user']==44){

			$coms = DB::table('commission')
			->whereBetween('created_at', ['2020-06-01', '2021-03-01'])			
			->get();

			echo "<table border='1'>";

			echo "<tr>";
				
				echo "<td>Property created by</td>";
				echo "<td>Information Officer</td>";
				echo "<td>Property Name</td>";

				echo "<td>Invoice Created</td>";

				echo "</tr>";


			foreach($coms as $com){
				$p = Property::find($com->propertyid);
				$style="";
				if($p->user_id == 44 ) $style="background: #b1f4b1"; 
				$check = $this->checkchanges($com->propertyid);
				if($check['status'] == 1 && $check['information_officer']==44)  $style="background: #b1f4b1"; 

				echo "<tr  style='$style'>";			
				$test[$com->propertyid] = $p->user_id;

				
				echo "<td>".$p->user_id."</td>";
				echo "<td>".$check['information_officer']."</td>";
				echo "<td><a href='/property/edit/".$com->propertyid."'>".$p->name."</a> </td>";

				echo "<td>".$com->created_at."</td>";

				echo "</tr>";

				//if($p->user_id == 44 ){
					if($check['status'] == 1 && $check['information_officer']==44){
						
						$data[] = array(
							'user_id' => $p->user_id,
							'property_id' => $com->propertyid,
							'status' => 'Pending',
							'commission' => '500',
							'owner_id' => $p->owner_id,
							'invoiceId' => $com->id,
							'salarydate' => $com->commissiondate,
						);
					}
				//}
			}

			echo "</table>";
			

		
		} else {
			$coms = DB::table('commission')
			->where('created_at','like', '%2021%')
			->get();

			echo "<table border='1'>";

			echo "<tr>";
				
				echo "<td>Property created by</td>";
				echo "<td>Property Name</td>";

				echo "<td>Invoice Created</td>";

				echo "</tr>";
				

			foreach($coms as $com){
				$p = Property::find($com->propertyid);
				$style="";
				if($p->user_id == 44 ) $style="background: #b1f4b1"; 

				echo "<tr  style='$style'>";			
				$test[$com->propertyid] = $p->user_id;
				
				echo "<td>".$p->user_id."</td>";
				echo "<td><a href='/property/edit/".$com->propertyid."'>".$p->name."</a> </td>";

				echo "<td>".$com->created_at."</td>";

				echo "</tr>";
			}

			echo "</table>";
		}
		

		

	}

	//link Commission class up with consultant, client, property and shortlist
	//then make the invoicing list
	// x for consultants only show their own invoices
	// x for me, director and office manager/accountant show all invoices
	// x add filters on the top similar to client
	// x standard only show open invoices (then have filter to show others as well)

	//Will R. info:
	// x basically sends the tenancy agreement with some additional information over to OM
	// x then OM makes the invoice and sends it to him (consultant)
	// x he then sends it out and also chases not paid invoices
	// x the OM is the only one who can see the accounts, so she would have to tick things off
	// x it would be good if consultants could see their own invoices so they know who to chase
	// x add another date field for when the commission would be calculated, for commissions on the same day, ask Kathy how to handle those
    public function invoicing(Request $request){
        $input = $request->all();
		$cu = $request->user();

		$query = Commission::where('commission.id', '>', 0);

			$query->leftjoin('commcons','commission.id', '=', 'commcons.commissionid');
			$query->selectraw('commission.*');
			$query->groupby('commission.id');
			//$query->leftjoin('commcons','commission.consultantid', '=', 'commcons.consultantid');
			//$query->orWhere('commcons.consultantid', '=', $input['user_id']);
			//$query->where('commcons.deleted_at', '=', NULL);

		//DB::enableQueryLog();
		if (isset($input['year']) && isset($input['month'])){
			$query->whereRaw('YEAR(commission.commissiondate) = '.intval($input['year']))->whereRaw('MONTH(commission.commissiondate) = '.intval($input['month']));
		}else if (isset($input['status']) && is_numeric($input['status']) != ''){
			if ($input['status'] == 0){
				$query->where('status', '<', 10);
			}else if ($input['status'] == 1){
				$query->where('status', '=', 1);
			}else if ($input['status'] == 2){
				$query->where('status', '=', 2);
			}else if ($input['status'] == 3){
				$query->where('status', '=', 3);
			}else if ($input['status'] == 4){
				$query->where('status', '>', 4)->where('status', '<', 10);
			}else if ($input['status'] == 10){
				$query->where('status', '=', 10);
			}else if ($input['status'] == 11){
				$query->where('status', '=', 11);
			}else{
				$query->where('status', '<', 10);
			}
		}else{
			//$query->where('status', '<', 10);
		}

		if (isset($input['client_name']) && trim($input['client_name']) != ''){			
			
			//$query->whereRaw('commission.clientid in (select id from clients where firstname like ? or lastname like ?)', ['%'.$input['client_name'].'%', '%'.$input['client_name'].'%']);
			$query->whereRaw('commission.clientid in (select id from clients where CONCAT(firstname, " " ,lastname) LIKE  ?)', ['%'.$input['client_name'].'%']);			
		}

		if (isset($input['invoice_number']) && trim($input['invoice_number']) != ''){		
			$query->where('commission.invoicenumbers', 'like', '%'.trim($input['invoice_number']).'%');
			//$query->leftjoin('comminvoice','comminvoice.id', '=', trim($input['invoice_number']));
		}

		if (isset($input['invoice_amount']) && trim($input['invoice_amount']) > 0){			
			$query->whereRaw('(commission.landlordamount = '.doubleval($input['invoice_amount']).' or tenantamount = '.doubleval($input['invoice_amount']).' or (landlordamount+sd_amount/2) = '.doubleval($input['invoice_amount']).' or (tenantamount+sd_amount/2) = '.doubleval($input['invoice_amount']).')');
		}

		if (isset($input['property_name']) && trim($input['property_name']) != ''){			
			$query->whereRaw('commission.propertyid in (select id from properties where name like ?)', ['%'.$input['property_name'].'%']);
		}

		if ($cu->getUserRights()['Superadmin'] || $cu->getUserRights()['Accountant'] || $cu->getUserRights()['Director']){
			if (isset($input['user_id']) && is_numeric($input['user_id'])){
				//$query->where('commission.consultantid', '=', $input['user_id']);
				$query->Where('commcons.consultantid', '=', $input['user_id']);
			}
		}else{
			$query->where('commission.consultantid', '=', $cu->id);
		}

		if (isset($input['orderby']) && $input['orderby'] == 'statusdesc'){
			$query->orderBy('status', 'desc')->orderBy('id', 'desc');
		}else if (isset($input['orderby']) && $input['orderby'] == 'invnrasc'){
			$query->orderBy('invoicenumbers', 'asc')->orderBy('id', 'desc');
		}else if (isset($input['orderby']) && $input['orderby'] == 'invnrdesc'){
			$query->orderBy('invoicenumbers', 'desc')->orderBy('id', 'desc');
		}else if (isset($input['orderby']) && $input['orderby'] == 'offerdateasc'){
			$query->orderBy('commission.commissiondate', 'asc')->orderBy('id', 'desc');
		}else if (isset($input['orderby']) && $input['orderby'] == 'offerdatedesc'){
			$query->orderBy('commission.commissiondate', 'desc')->orderBy('id', 'desc');
		}else if (isset($input['orderby']) && $input['orderby'] == 'invoicedateasc'){
			$query->orderBy('invoicedate', 'asc')->orderBy('id', 'desc');
		}else if (isset($input['orderby']) && $input['orderby'] == 'invoicedatedesc'){
			$query->orderBy('invoicedate', 'desc')->orderBy('id', 'desc');
		}else{
			$query->orderBy('status', 'asc')->orderBy('commission.id', 'desc');
		}

    	$query->with('client')->with('property')->with('shortlist');

		/*if(isset($input['user_id'])){
			$query->leftjoin('commcons','commission.id', '=', 'commcons.commissionid');
			//$query->leftjoin('commcons','commission.consultantid', '=', 'commcons.consultantid');
			$query->selectraw('commission.*');
			$query->groupby('commission.id');
			//$query->leftjoin('commcons','commission.consultantid', '=', 'commcons.consultantid');
			$query->orWhere('commcons.consultantid', '=', $input['user_id']);
			$query->where('commcons.deleted_at', '=', NULL);

			if (isset($input['year']) && isset($input['month'])){
				$query->whereRaw('YEAR(commcons.commissiondate) = '.intval($input['year']))->whereRaw('MONTH(commcons.commissiondate) = '.intval($input['month']));
			}			
		}*/

		
        $commissions = $query->paginate(20);

		$curuser = 0;
		if (!isset($input['user_id'])){
			$currole = json_decode($request->user()->roles);
			if ($currole->Consultant && $request->user()->id != 6){
				$curuser = $request->user()->id;
				$input['user_id'] = $curuser;
			}
		}else{
			$curuser = $input['user_id'];
		}

		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

        $users = User::where('status', '=', 0)->select('id', 'name', 'roles')->orderBy('id', 'desc')->get();
		$user_ids = array();
		foreach ($users as $u){
			$roles = json_decode($u->roles);
			if ($roles->Consultant){
				$user_ids[$u->id] = $u->name;
			}
		}

		$statuslist = array(
			0 => 'Open',
			1 => 'Submitted',
			2 => 'Invoice Created',
			3 => 'Invoice Sent',
			4 => 'Waiting for Payment',
			10 => 'Fully Paid',
			11 => 'Cancelled'
		);

		$commids = array();
		$payouts = array();
		if ($cu->getUserRights()['Superadmin'] || $cu->getUserRights()['Accountant'] || $cu->getUserRights()['Director']){
			if (count($commissions) > 0){
				foreach ($commissions as $comm){
					$payouts[$comm->id] = array();
					$commids[] = $comm->id;
				}
			}
			$commcons = Commcons::whereIn('commissionid', $commids)->get();
			if (count($commcons) > 0){
				foreach ($commcons as $cc){
					$payouts[$cc->commissionid][] = $cc;
				}
			}
		}


		if (!empty($input->client_name)) $input->client_name = '';
		if (!empty($input->property_name)) $input->property_name = '';
		if (!empty($input->status)) $input->status = '';
		

		$querystringArray = Input::only(['user_id','client_name','property_name','status','invoice_number','invoice_amount','orderby']);

		return view('commission.invoicing', [
			'input' => $request,
			'commissions' => $commissions,
			'consultants' => $consultants,
			'user_ids' => $user_ids,
			'curuser' => $curuser,
			'statuslist' => $statuslist,
			'payouts' => $payouts,
			'querystringArray' => $querystringArray
		]);
	}

	public function invoiceedit($id, Request $request){
        $input = $request->all();
        $commission = Commission::where('id', '=', $id)->with('client')->with('property')->with('shortlist')->with('consultant')->withTrashed()->first();
		$curconsultant = User::where('id', $commission->consultantid)->first();

		$users = User::all();

		if ($commission){
			
			$consultants = array();
			foreach ($users as $uu){
				$consultants[$uu->id] = $uu;
			}

			$doc = Documentinfo::where('shortlist_id', $commission->shortlistid)->first();
			if (!$doc){
				return redirect('/shortlists');
			}
			$props = explode('|', $doc->properties);
			$properties = Property::whereIn('id', $props)->withTrashed()->get();

			$docuploads = Invoicedocuments::where('commissionid', '=', $id)->get();

			$commcons = Commcons::where('commissionid', $commission->id)->get();
			if (empty($commcons) || count($commcons) < 1){

				
				$lead = Lead::where('shortlistid','=',$commission->shortlistid)->first();

				$sc = json_decode($lead->shared_with);
				

				if(count($sc) > 0){

				
					$commissionconsultant = intval($commission->landlordamount) +  intval($commission->tenantamount);
					
					$divide = count($sc)+1;		

					$commissionconsultant = $commissionconsultant / $divide;

					
						$commconsOld = Commcons::where('commissionid', $commission->id)->first();
	
						$commcons = Commcons::where('commissionid', $commission->id)->delete();
	
						$data = array();
	
						$data['commissionid'] = $commission->id;
						$data['shortlistid'] = $commission->shortlistid;
						$data['consultantid'] = $commission->consultantid;
						$data['propertyid'] = $commission->propertyid;				
						$data['commissiondate'] = $commission->commissiondate;
						$data['invoicedate'] = $commission->invoicedate;
						$data['salelease'] = $commission->salelease;
						if(isset($commission->tenantpaiddate)){
							$data['tenantpaiddate'] = $commission->tenantpaiddate;
						}
						if(isset($commission->landlordpaiddate)){
							$data['landlordpaiddate'] = $commission->landlordpaiddate;
						}
						$data['commissiontotal'] = $commissionconsultant;
						$data['commissionconsultant'] = $commissionconsultant;
						//$data['payout'] = $payout;
	
						$comm = Commcons::create($data);
						$comm->save();
	
				
						
					

					foreach($sc as $consultant_sc){
						//echo $consultant_sc;					
					
	
								$data = array();
			
								$data['commissionid'] = $commission->id;
								$data['shortlistid'] = $commission->shortlistid;
								$data['consultantid'] = $consultant_sc;
								$data['propertyid'] = $commission->propertyid;				
								$data['commissiondate'] = $commission->commissiondate;
								$data['invoicedate'] = $commission->invoicedate;
								$data['salelease'] = $commission->salelease;
								if(isset($commission->tenantpaiddate)){
									$data['tenantpaiddate'] = $commission->tenantpaiddate;
								}
								if(isset($commission->landlordpaiddate)){
									$data['landlordpaiddate'] = $commission->landlordpaiddate;
								}
								$data['commissiontotal'] = $commissionconsultant;
								$data['commissionconsultant'] = $commissionconsultant;
								//$data['payout'] = $payout;
			
								$comm = Commcons::create($data);
								$comm->save();
			
					}

				} else { 
				
					$data = array();

					$data['commissionid'] = $commission->id;
					$data['shortlistid'] = $commission->shortlistid;
					$data['consultantid'] = $commission->consultantid;
					$data['propertyid'] = $commission->propertyid;
					$data['leadid'] = $commission->leadid;
					$data['commissiondate'] = $commission->commissiondate;
					$data['invoicedate'] = $commission->invoicedate;
					$data['salelease'] = $commission->salelease;
					$data['tenantpaiddate'] = $commission->tenantpaiddate;
					$data['landlordpaiddate'] = $commission->landlordpaiddate;
					$data['commissiontotal'] = $commission->landlordamount + $commission->tenantamount;
					$data['commissionconsultant'] = $commission->landlordamount + $commission->tenantamount;

					if (!empty($curconsultant)){
						$data['commstruct'] = $curconsultant->commstruct;
					}

					$comm = Commcons::create($data);
					$comm->save();

					//$commcons = Commcons::where('commissionid', $commission->id)->get();

				}
			}

			$commcons = Commcons::where('commissionid', $commission->id)->get();

			$invoices = Comminvoice::where('commissionid', $commission->id)->get();


			$io_commission = IOCommission::where('io_commissions.status', '!=', 'draft')->where('io_commissions.property_id', '=', $commission->propertyid)
			->leftJoin('users', 'io_commissions.user_id', '=', 'users.id')
			->leftJoin('properties', 'io_commissions.property_id', '=', 'properties.id')
			->leftJoin('vendors', 'properties.owner_id', '=', 'vendors.id')
			->select(
				'io_commissions.id as id',
				'io_commissions.status as status',
				'io_commissions.commission as commission',
				'io_commissions.salarydate as salarydate',
				'users.firstname as firstname',
				'users.lastname as lastname',
				'properties.name as name',
				'vendors.company as company',
				'vendors.firstname as owner_firstname',
				'vendors.lastname as owner_lastname')
			->orderBy('io_commissions.id','desc')
			->get();
			
			$lead = Lead::where('shortlistid','=',$commission->shortlistid)->first();


			$logs = LogInvoicing::where('commission_id','=',$id)->orderBy('created_at', 'desc')->get();

			$u = array();
			foreach ($users as $uu){
				$u[$uu->id] = $uu;
			}
			
			

			return view('commission.edit', [
				'input' => $input,				
				'commission' => $commission,
				'consultants' => $consultants,
				'properties' => $properties,
				'docuploads' => $docuploads,
				'curconsultant' => $curconsultant,
				'commcons' => $commcons,
				'invoices' => $invoices,
				'io_commission' => $io_commission,
				'lead' => $lead,
				'logs' => $logs,
				'users' => $u
			]);
		}else{
			return redirect('/');
		}
	}

	public function invoicestore($id, Request $request){
		$input = $request->all();
		$user = $request->user();
	
		$commission = Commission::where('id', '=', $id)->with('consultant')->first();	

		$consultantComm = User::where('id','=',$input['consultantid'])->first();

		$emailNoti= false;
		$compensationAutoUpdate = false;


		if ($commission){
			$data = array();
			$log = "";
			
			

			if (isset($input['consultantid']) && $input['consultantid'] != $commission->consultantid){
				$new = User::where('id','=',$input['consultantid'])->first();
				$newtxt = $new->firstname.' '. $new->lastname;

				$old = User::where('id','=', $commission->consultantid)->first();
				$oldtxt = $old->firstname.' '. $old->lastname;

				$log .= "<strong>Counsultant</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';

				$compensationAutoUpdate = true;
				$emailNoti = true;
			}

			if (isset($input['propertyid']) && $input['propertyid'] != $commission->propertyid){
				$new = Property::where('id','=',$input['propertyid'])->first();
				$newtxt = $new->name;

				$old = Property::where('id','=', $commission->propertyid)->first();
				$oldtxt = $old->name;

				$log .= "<strong>Property</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';
			}

			if (isset($input['salelease']) && $input['salelease'] != $commission->salelease){
				
				if($input['salelease'] == 1){
					$newtxt = "Lease";
					$oldtxt = "Sale";
				}

				if($input['salelease'] == 2){
					$newtxt = "Sale";
					$oldtxt = "Lease";
				}
				$log .= "<strong>Lease/Sale</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';

				$compensationAutoUpdate = true;
				$emailNoti = true;
			}

			if (isset($input['stamping']) && $input['stamping'] != $commission->stamping){
				
				if($input['stamping'] == 0) $oldtxt = "N/A";
				if($input['stamping'] == 1) $oldtxt = "Yes";
				if($input['stamping'] == 2) $oldtxt = "No";		

				if($commission->stamping == 0) $newtxt = "N/A";
				if($commission->stamping == 1) $newtxt = "Yes";
				if($commission->stamping == 2) $newtxt = "No";
								
				$log .= "<strong>Stamp Duty</strong> changed from ".$newtxt. " to ".$oldtxt.'<br>';
			}

			if (isset($input['fullamount']) && $input['fullamount'] != $commission->fullamount){
				
				$newtxt = $input['fullamount'];
				$oldtxt =  $commission->fullamount;
				$log .= "<strong>Full Lease per Month</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';


			}

			if (isset($input['landlordamount']) && $input['landlordamount'] != intval($commission->landlordamount)){				
				$newtxt = $input['landlordamount'];
				$oldtxt =  $commission->landlordamount;
				$log .= "<strong>Amount to Invoice Landlord/Vendor</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';

				$compensationAutoUpdate = true;
				$emailNoti = true;
			}

			if (isset($input['tenantamount'])  && $input['tenantamount'] != intval($commission->tenantamount)){
				$newtxt = $input['tenantamount'];
				$oldtxt =  $commission->tenantamount;
				$log .= "<strong>Amount to Invoice Tenant/Purchaser</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';
				//echo $log;

				$compensationAutoUpdate = true;
				$emailNoti = true;
			}

			
			if (isset($input['invoicedate'])  && $input['invoicedate'] != $commission->invoicedate  ){
				if($input['invoicedate']!="")
					$newtxt = date_format(date_create($input['invoicedate']),'d-m-Y');
				else
					$newtxt = "00-00-0000";

				if($commission->invoicedate!="0000-00-00")
					$oldtxt =  date_format(date_create($commission->invoicedate),'d-m-Y');
				else
					$oldtxt =  "00-00-0000";


				$log .= "<strong>Invoice Date</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';
				//die('changes detected 2');
			}

			if (isset($input['invoicenumbers'])  && $input['invoicenumbers'] != $commission->invoicenumbers){
				$newtxt = $input['invoicenumbers'];
				$oldtxt =  $commission->invoicenumbers;
				$log .= "<strong>Invoice Numbers</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';
				//die('changes detected 2');
			}

			
			if (isset($input['commissiondate'])  && $input['commissiondate'] != $commission->commissiondate ){
				
				if($input['commissiondate']!="")
					$newtxt = date_format(date_create($input['commissiondate']),'d-m-Y');
				else
					$newtxt = "00-00-0000";

				if($commission->commissiondate!="0000-00-00")
					$oldtxt =  date_format(date_create($commission->commissiondate),'d-m-Y');
				else
					$oldtxt =  "00-00-0000";

				$log .= "<strong>Offer Signing Date</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';				
			}


			if (isset($input['tenantpaiddate'])  && $input['tenantpaiddate'] != $commission->tenantpaiddate && $input['tenantpaiddate']!=""){
				if($input['tenantpaiddate']!="")
					$newtxt = date_format(date_create($input['tenantpaiddate']),'d-m-Y');
				else
					$newtxt = "00-00-0000";

				if($commission->tenantpaiddate!="0000-00-00")
					$oldtxt =  date_format(date_create($commission->tenantpaiddate),'d-m-Y');
				else
					$oldtxt =  "00-00-0000";

				$log .= "<strong>Date Landlord/Vendor Paid</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';				
			}

			if (isset($input['landlordpaiddate'])  && $input['landlordpaiddate'] != $commission->landlordpaiddate && $input['landlordpaiddate']!=""){
				if($input['landlordpaiddate']!="")
					$newtxt = date_format(date_create($input['landlordpaiddate']),'d-m-Y');
				else
					$newtxt = "00-00-0000";

				if($commission->landlordpaiddate!="0000-00-00")
					$oldtxt =  date_format(date_create($commission->landlordpaiddate),'d-m-Y');
				else
					$oldtxt =  "00-00-0000";

				$log .= "<strong>Date Tenant/Purchaser Paid</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';				
			}

			if (isset($input['type'])  && $input['type'] != $commission->type){
				if($input['type'] == 'direct') $newtxt = "Direct";			
				if($input['type'] == 'coop') $newtxt = "Co-operation";		
				if($input['type'] == 'reduced') $newtxt = "Reduced commission";		
				
				if($commission->type == 'direct') $oldtxt = "Direct";			
				if($commission->type == 'coop') $oldtxt = "Co-operation";		
				if($commission->type == 'reduced') $oldtxt = "Reduced commission";	

				$log .= "<strong>Type of Deal</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';				
			}

			if (isset($input['source'])  && $input['source'] != $commission->source){
				if($input['source'] == 0) $newtxt = "Nest";			
				if($input['source'] == 1) $newtxt = "Previous Client / Direct";						
				
				if($commission->source == 0) $oldtxt = "Nest";			
				if($commission->source == 1) $oldtxt = "Previous Client / Direct";						

				$log .= "<strong>Source of Client</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';	
				
				$compensationAutoUpdate = true;
				$emailNoti = true;
			}

			if (isset($input['status'])  && $input['status'] != $commission->status){
				if($input['status'] == 1) $newtxt = "Submitted";			
				if($input['status'] == 2) $newtxt = "Invoice Created";			
				if($input['status'] == 3) $newtxt = "Invoice Sent";			
				if($input['status'] == 5) $newtxt = "Waiting for Tenant Payment";			
				if($input['status'] == 6) $newtxt = "Waiting for Landlord Payment";						
				if($input['status'] == 7) $newtxt = "Waiting for Other Payment";						
				if($input['status'] == 8) $newtxt = "Waiting for Several Payment";						
				if($input['status'] == 10) $newtxt = "Fully Paid";						
				if($input['status'] == 11) $newtxt = "Cancelled";														
						
				if($commission->status  == 1) $oldtxt = "Submitted";			
				if($commission->status  == 2) $oldtxt = "Invoice Created";			
				if($commission->status  == 3) $oldtxt = "Invoice Sent";			
				if($commission->status  == 5) $oldtxt = "Waiting for Tenant Payment";			
				if($commission->status  == 6) $oldtxt = "Waiting for Landlord Payment";						
				if($commission->status  == 7) $oldtxt = "Waiting for Other Payment";						
				if($commission->status  == 8) $oldtxt = "Waiting for Several Payment";						
				if($commission->status  == 10) $oldtxt = "Fully Paid";						
				if($commission->status  == 11) $oldtxt = "Cancelled";											

				$log .= "<strong>Status</strong> changed from ".$oldtxt. " to ".$newtxt.'<br>';				

			}


			if($compensationAutoUpdate == true ){

				
				
				$commissionconsultant = intval($input['landlordamount']) +  intval($input['tenantamount']);

				$payout = 0;

				$update = false;

				if($consultantComm->commstruct == 2){
					if( $input['salelease'] == 1){
						$percentage = 0.40;
					}else if( $input['salelease'] == 2 ){
						if($input['source'] == 0){
							$percentage = 0.45;
						} else {
							$percentage = 0.50;
						}
					}

					$payout = $commissionconsultant * $percentage;
					$update = true;
				} else if ($consultantComm->commstruct == 3){
					$percentage = 0.20;
					$payout = $commissionconsultant * $percentage;
					$update = true;
				}
			
				$lead = Lead::where('shortlistid','=',$commission->shortlistid)->first();

				$sc = json_decode($lead->shared_with);
				

				if(count($sc) > 0){

					$divide = count($sc)+1;
					$payoutt = $payout / $divide;

					$commissionconsultantt = $commissionconsultant / $divide;
				

					if($update == true){
						$commconsOld = Commcons::where('commissionid', $commission->id)->where('consultantid','=',  $input['consultantid'])->first();
						$commconsOld_otherConsultant = Commcons::where('commissionid', $commission->id)->where('consultantid','!=',  $input['consultantid'])->with('consultant')->get();
	
						$commcons = Commcons::where('commissionid', $commission->id)->delete();
	
						$data = array();
	
						$data['commissionid'] = $commission->id;
						$data['shortlistid'] = $commission->shortlistid;
						$data['consultantid'] = $input['consultantid'];
						$data['propertyid'] = $input['propertyid'];				
						$data['commissiondate'] = $input['commissiondate'];
						$data['invoicedate'] = $input['invoicedate'];
						$data['salelease'] = $input['salelease'];
						if(isset($input['tenantpaiddate'])){
							$data['tenantpaiddate'] = $input['tenantpaiddate'];
						}
						if(isset($input['landlordpaiddate'])){
							$data['landlordpaiddate'] = $input['landlordpaiddate'];
						}
						$data['commissiontotal'] = $commissionconsultantt;
						$data['commissionconsultant'] = $commissionconsultantt;
						$data['payout'] = $payoutt;
	
						$comm = Commcons::create($data);
						$comm->save();
	
				
						$new = User::where('id','=',$input['consultantid'])->first();
						$con = $new->firstname.' '. $new->lastname;
	
						$conold = $commission->consultant->firstname.' '.$commission->consultant->lastname;
	
						$log .= "<br><strong>Consultant Compensation Previous Value</strong> Consultant: ".$conold. ", Agency Fee: ".doubleval($commconsOld->commissionconsultant).', Commission: '.doubleval($commconsOld->payout).'<br>';			
						
						
						$log .= "<strong>Consultant Compensation New Value</strong> Consultant: ".$con. ", Agency Fee: ".doubleval($commissionconsultantt).', Commission: '.doubleval($payoutt).'<br>';			
						
						
					}

					foreach($sc as $key=>$consultant_sc){
						//echo $consultant_sc;

						$consultantCommnew = User::where('id','=',$consultant_sc)->first();

						if($consultantCommnew->commstruct == 2){
							if( $input['salelease'] == 1){
								$percentage = 0.40;
							}else if( $input['salelease'] == 2 ){
								if($input['source'] == 0){
									$percentage = 0.45;
								} else {
									$percentage = 0.50;
								}
							}
		
							$payout = $commissionconsultantt * $percentage;
							$update = true;
						} else if ($consultantCommnew->commstruct == 3){
							$percentage = 0.20;
							$payout = $commissionconsultantt * $percentage;
							$update = true;
						}

						if($update == true){
							//$commconsOld = Commcons::where('commissionid', $commission->id)->where('consultantid',$consultant_sc)->first();
						

							$conold=$commconsOld_otherConsultant[$key]->consultant->firstname.' '.$commconsOld_otherConsultant[$key]->consultant->lastname;
									
							$data = array();		
												
							$con = $consultantCommnew->firstname.' '. $consultantCommnew->lastname;
		
							//$conold = $commission->consultant->firstname.' '.$commission->consultant->lastname;
		
							$log .= "<br><strong>Consultant Compensation Previous Value</strong> Consultant: ".$conold. ", Agency Fee: ".doubleval($commconsOld->commissionconsultant).', Commission: '.doubleval($commconsOld->payout).'<br>';			
							
							
							$log .= "<strong>Consultant Compensation New Value</strong> Consultant: ".$con. ", Agency Fee: ".doubleval($commissionconsultantt).', Commission: '.doubleval($payout).'<br>';			

							$data['commissionid'] = $commission->id;
							$data['shortlistid'] = $commission->shortlistid;
							$data['consultantid'] = $consultant_sc;
							$data['propertyid'] = $input['propertyid'];				
							$data['commissiondate'] = $input['commissiondate'];
							$data['invoicedate'] = $input['invoicedate'];
							$data['salelease'] = $input['salelease'];
							if(isset($input['tenantpaiddate'])){
								$data['tenantpaiddate'] = $input['tenantpaiddate'];
							}
							if(isset($input['landlordpaiddate'])){
								$data['landlordpaiddate'] = $input['landlordpaiddate'];
							}
							$data['commissiontotal'] = $commissionconsultantt;
							$data['commissionconsultant'] = $commissionconsultantt;
							$data['payout'] = $payout;
		
							$comm = Commcons::create($data);
							$comm->save();

						}
					}

				} else {
			
				
					if($update == true){
						$commconsOld = Commcons::where('commissionid', $commission->id)->first();

						$commcons = Commcons::where('commissionid', $commission->id)->delete();

						$data = array();

						$data['commissionid'] = $commission->id;
						$data['shortlistid'] = $commission->shortlistid;
						$data['consultantid'] = $input['consultantid'];
						$data['propertyid'] = $input['propertyid'];				
						$data['commissiondate'] = $input['commissiondate'];
						$data['invoicedate'] = $input['invoicedate'];
						$data['salelease'] = $input['salelease'];
						if(isset($input['tenantpaiddate'])){
							$data['tenantpaiddate'] = $input['tenantpaiddate'];
						}
						if(isset($input['landlordpaiddate'])){
							$data['landlordpaiddate'] = $input['landlordpaiddate'];
						}
						$data['commissiontotal'] = $commissionconsultant;
						$data['commissionconsultant'] = $commissionconsultant;
						$data['payout'] = $payout;

						$comm = Commcons::create($data);
						$comm->save();

				
						$new = User::where('id','=',$input['consultantid'])->first();
						$con = $new->firstname.' '. $new->lastname;

						$conold = $commission->consultant->firstname.' '.$commission->consultant->lastname;

						$log .= "<br><strong>Consultant Compensation Previous Value</strong> Consultant: ".$conold. ", Agency Fee: ".doubleval($commconsOld->commissionconsultant).', Commission: '.doubleval($commconsOld->payout).'<br>';			
						
						
						$log .= "<strong>Consultant Compensation New Value</strong> Consultant: ".$con. ", Agency Fee: ".doubleval($commissionconsultant).', Commission: '.doubleval($payout).'<br>';			
						
						
					}

				}
				
			}

			

			if($log !=""){
				$invoicelog = new LogInvoicing();
				$invoicelog->user_id = $user->id;
				$invoicelog->commission_id = $id;
			
				$invoicelog->log = $log;
				$invoicelog->save();
			}

				

			

			if (isset($input['consultantid'])){
				$data['consultantid'] = intval($input['consultantid']);
			}
			if (isset($input['propertyid'])){
				$data['propertyid'] = intval($input['propertyid']);
			}
			if (isset($input['salelease'])){
				$data['salelease'] = intval($input['salelease']);
			}
			if (isset($input['stamping'])){
				$data['stamping'] = intval($input['stamping']);
			}
			if (isset($input['fullamount'])){
				$data['fullamount'] = floatval($input['fullamount']);
			}
			if (isset($input['landlordamount'])){
				$data['landlordamount'] = floatval($input['landlordamount']);
			}
			if (isset($input['tenantamount'])){
				$data['tenantamount'] = floatval($input['tenantamount']);
			}
			if (isset($input['invoicedate'])){
				$data['invoicedate'] = trim($input['invoicedate']);
			}
			if (isset($input['invoicenumbers'])){
				$data['invoicenumbers'] = trim($input['invoicenumbers']);
			}
			if (isset($input['commissiondate'])){
				$data['commissiondate'] = trim($input['commissiondate']);
			}
			if (isset($input['tenantpaiddate'])){
				$data['tenantpaiddate'] = trim($input['tenantpaiddate']);
			}
			if (isset($input['landlordpaiddate'])){
				$data['landlordpaiddate'] = trim($input['landlordpaiddate']);
			}
			if (isset($input['status'])){
				$data['status'] = floatval($input['status']);
			}
			if (isset($input['type'])){
				$data['type'] = trim($input['type']);
			}
			if (isset($input['source'])){
				$data['source'] = trim($input['source']);
			}
			if (isset($input['sd_term'])){
				$data['sd_term'] = intval($input['sd_term']);
			}
			if (isset($input['sd_rent'])){
				$data['sd_rent'] = floatval($input['sd_rent']);
			}
			if (isset($input['sd_keymoney'])){
				$data['sd_keymoney'] = floatval($input['sd_keymoney']);
			}
			if (isset($input['sd_amount'])){
				$data['sd_amount'] = intval($input['sd_amount']);
			}

			$commission->fill($data);
			$commission->save();

			$commcons = Commcons::where('commissionid', $commission->id)->get();
			if (!empty($commcons) && count($commcons) > 0){
				foreach ($commcons as $comm){
					$data = array();

					$data['commissiondate'] = $commission->commissiondate;
					$data['invoicedate'] = $commission->invoicedate;
					$data['salelease'] = $commission->salelease;
					$data['tenantpaiddate'] = $commission->tenantpaiddate;
					$data['landlordpaiddate'] = $commission->landlordpaiddate;
					$data['commissiontotal'] = $commission->landlordamount + $commission->tenantamount;

					$comm->fill($data);
					$comm->save();
				}
			}


			if($emailNoti == true){
				$this->invoincingUpdateNotification($id,$log,$user->firstname.' '.$user->lastname,$consultantComm->email);
			}


		}




		return redirect('/commission/invoiceedit/'.$id);
	}


	public function invoincingUpdateNotification($id,$log,$updatedBy,$consultantEmail){
				//send email to Office Manager//Email to Accountant
				/*$subject = 'Invoicing Update';
				$message = $updatedBy.' has updated invoice https://https://db.nest-property.com//commission/invoiceedit/'.$id.':';
				$message .=	'<br>'.$log;				
				
				
				$headers[] = 'From: Nest Database <noreply@db.nest-property.com>';
				//$headers[] = 'Bcc: contact@wohok-solutions.com';

				mail($to, $subject, $message, implode("\r\n", $headers));*/

		$subject = 'Invoicing Update by '.$updatedBy;
		$message = $updatedBy.' has updated invoice <a href="https://db.nest-property.com/commission/invoiceedit/'.intval($id).'">https://db.nest-property.com/commission/invoiceedit/'.intval($id).'</a>:';
		$message .=	'<br>'.$log;		

		\Mail::send('emails.generic', ['body' => $message, 'title' => 'Invoicing Update'], function ($mail) use ($message,$consultantEmail) {
			$tos = ['Andrew@freshaccounting.biz','naomi.budden@nest-property.com','william.budden@nest-property.com'];
			$mail->from('noreply@db.nest-property.com', 'Nest Database');
			$mail->to($tos, 'Office Accounts')
			->subject('Invoicing Update')
			->cc($consultantEmail)
			->setBody($message, 'text/html');
		});

		

		
	}




    public function getcomments(Request $request, $id){
		$user = $request->user();

        $commission = Commission::findOrFail($id);
        if (empty($commission)) {
            return '0';
        }

		if (trim($commission->comment) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $commission);
		}else{
			$short = substr(trim($commission->comment), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($commission->comment, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($commission->comment)
					)
				);
				$this->storecomments($id, $comments, $commission);
			}
		}

		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}

		$datetime = date_create();

		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';

			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];

				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				//$comments[$i]['datum'] = date_format($datetime, 'Y-m-d G:i');
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('commission.commentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }

    public function addcomment(Request $request, $id){
        $commission = Commission::findOrFail($id);
		$property = Property::findOrFail($commission->propertyid);
		$user = $request->user();
		$input = $request->all();
        if (empty($commission)) {
            return '0';
        }

		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}

		if (trim($commission->comment) == ''){
			$comments = array();
		}else{
			$short = substr(trim($commission->comment), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($commission->comment, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($commission->comment)
					)
				);
			}
		}

		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;



		$commission->fill([
			'comment' => json_encode($comments)
		])->save();

                		\Mail::send('emails.commissions.comment', [
			'user' => $user,
			'id' => $id,
			'property' => $property,
			'comment' => $comment,
			'commission' => $commission
		], function ($mail) use ($user, $commission) {
			$mail->from('noreply@db.nest-property.com', 'Nest Database')
				->to('russmuza@gmail.com', 'Naomi Budden')
				->subject('Nest Property -- Comment added to an invoice by '.$user->name);

		});
                
		/*\Mail::send('emails.commissions.comment', [
			'user' => $user,
			'id' => $id,
			'property' => $property,
			'comment' => $comment,
			'commission' => $commission
		], function ($mail) use ($user, $commission) {
			$mail->from('noreply@db.nest-property.com', 'Nest Database')
				->to('naomi.budden@nest-property.com', 'Naomi Budden')
				->subject('Nest Property -- Comment added to an invoice by '.$user->name);
			
			if($commission->consultant && $commission->consultantid != $user->id){
				$mail->cc($commission->consultant->email, $commission->consultant->name);
			}

		});*/

		return 'ok';
    }

	public function removecomment(Request $request, $id){
        $commission = Commission::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($commission)) {
            return '0';
        }

		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}

		if (trim($commission->comment) == ''){
			$comments = array();
		}else{
			$short = substr(trim($commission->comment), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($commission->comment, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($commission->comment)
					)
				);
			}
		}

		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}

		$commission->fill([
			'comment' => json_encode($comments)
		])->save();

		return 'ok';
	}


	public function storecomments($id, $comments, $commission){
		$commission->fill([
			'comment' => json_encode($comments)
		])->save();
	}


    public function docstoreupload($commissionid, Request $request){
		$input = $request->all();
		$commissionid = intval($commissionid);

		$rules = array(
			'upfile' =>  'mimes:pdf,jpeg,jpg,png'
		);
		$validator = Validator::make($request->all(), $rules);

		$saved = false;
		if ($validator->errors()->count() == 0){
			if (Input::hasFile('upfile') && Input::file('upfile')->isValid() && $validator->errors()->count() == 0){
				$path = '../storage/invoicedocs/';
				$ext = strtolower(Input::file('upfile')->getClientOriginalExtension());
				$name = $commissionid.'_'.time().'_'.$input['doctype'].'.'.$ext;
				Input::file('upfile')->move($path, $name);

				$doc = new Invoicedocuments();
				$doc->doctype = $input['doctype'];
				$doc->commissionid = $commissionid;
				$doc->userid = $request->user()->id;
				$doc->docpath = $path.$name;
				$doc->save();
			}
		}

		return redirect('/commission/invoiceedit/'.$commissionid);
	}

	public function showdoc($docid){
		$docupload = Invoicedocuments::where('id', '=', $docid)->first();

		if ($docupload){
			if (file_exists($docupload->docpath)){
				return response()->download($docupload->docpath);
			}
		}else{
			return redirect('/');
		}
	}

	public function removedoc(Request $request, $docid, $invoiceid){
		$user = $request->user();

		$doc = Invoicedocuments::where('id', '=', $docid)->first();

		if ($doc && ($doc->userid == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant'])){
			$doc->delete();

			return redirect('/commission/invoiceedit/'.$invoiceid);
		}else{
			return redirect('/commission/invoicing');
		}
	}


	public function commconsedit($id, Request $request){
		$input = $request->all();
		$user = $request->user();

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Accountant'] || $user->getUserRights()['Director']){
			$comm = Commcons::where('id', $id)->first();

			if ($comm){
				$data = array();

				if (isset($input['consultantid']) && is_numeric($input['consultantid'])){
					$data['consultantid'] = intval($input['consultantid']);
				}
				if (isset($input['commissionconsultant']) && is_numeric($input['commissionconsultant'])){
					$data['commissionconsultant'] = doubleval($input['commissionconsultant']);
				}
				if (isset($input['payout']) && is_numeric($input['payout'])){
					$data['payout'] = doubleval($input['payout']);
				}


				

				$curconsultant_old = User::where('id', $comm->consultantid)->first();
				$con_old = $curconsultant_old->firstname.' '.$curconsultant_old->lastname;
				$log="";
				$log .= "<strong>Consultant Compensation Previous Value</strong> Consultant: ".$con_old. ", Agency Fee: ".doubleval($comm->commissionconsultant).', Commission: '.doubleval($comm->payout).'<br>';							


				$curconsultant = User::where('id', $input['consultantid'])->first();
				$con = $curconsultant->firstname.' '.$curconsultant->lastname;

				$log .= "<strong>Consultant Compensation New Value</strong> Consultant: ".$con. ", Agency Fee: ".doubleval($input['commissionconsultant']).', Commission: '.doubleval($input['payout']).'<br>';			
				$invoicelog = new LogInvoicing();
				$invoicelog->user_id = $user->id;
				$invoicelog->commission_id = $comm->commissionid;
				$invoicelog->log = $log;
				$invoicelog->save();

				$comm->fill($data);
				$comm->save();

				return redirect('/commission/invoiceedit/'.$comm->commissionid);
			}
		}

		return redirect('/commission/invoicing');
	}

	public function commconsnew($id, Request $request){
		$input = $request->all();
		$user = $request->user();

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Accountant'] || $user->getUserRights()['Director']){
			$commission = Commission::where('id', '=', $id)->first();

			if ($commission){
				$data = array();

				$data['commissionid'] = $commission->id;
				$data['shortlistid'] = $commission->shortlistid;
				$data['consultantid'] = $commission->consultantid;
				$data['propertyid'] = $commission->propertyid;
				$data['leadid'] = $commission->leadid;
				$data['commissiondate'] = $commission->commissiondate;
				$data['invoicedate'] = $commission->invoicedate;
				$data['salelease'] = $commission->salelease;
				$data['tenantpaiddate'] = $commission->tenantpaiddate;
				$data['landlordpaiddate'] = $commission->landlordpaiddate;
				$data['commissiontotal'] = $commission->landlordamount + $commission->tenantamount;


				$curconsultant = User::where('id', $commission->consultantid)->first();
				if (!empty($curconsultant)){
					$data['commstruct'] = $curconsultant->commstruct;
				}

				$comm = Commcons::create($data);
				$comm->save();
				
			}
		}

		return redirect('/commission/invoiceedit/'.$id);
	}

	public function commconsrem($id, Request $request){
		$input = $request->all();
		$user = $request->user();

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Accountant'] || $user->getUserRights()['Director']){
			$comm = Commcons::where('id', $id)->first();

			if ($comm){
				$comm->delete();

				$curconsultant = User::where('id', $comm->consultantid)->first();

				$con = $curconsultant->firstname.' '.$curconsultant->lastname;

				$log = "<strong>Removed Consultant Compensation</strong> Consultant: ".$con. ", Agency Fee: ".doubleval($comm->commissionconsultant).', Commission: '.doubleval($comm->payout).'<br>';			
				$invoicelog = new LogInvoicing();
				$invoicelog->user_id = $user->id;
				$invoicelog->commission_id = $comm->commissionid;
				$invoicelog->log = $log;
				$invoicelog->save();

				return redirect('/commission/invoiceedit/'.$comm->commissionid);
			}
		}

		return redirect('/commission/invoicing');
	}

	public function targetusers(Request $request)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboardnew');
		}

		//load all users
		$usersObj = User::all();
		$users = array();

		foreach ($usersObj as $u){
			//if ($u->id != $user->id){
				if ($u->isConsultant()){
					$uarr = $u->toArray();
					$uarr['rightsstr'] = $u->getUserRightsString();

					$users[] = $uarr;
				}
			//}
		}

		for ($i = 0; $i < count($users)-1; $i++){
			for ($a = $i+1; $a < count($users); $a++){
				if ($users[$i]['name'] > $users[$a]['name']){
					$help = $users[$i];
					$users[$i] = $users[$a];
					$users[$a] = $help;
				}
			}
		}

        return view('commission.targetusers', [
			'users' => $users
		]);
    }

	public function targetuser(Request $request, $id)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboardnew');
		}

		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$targets = Targets::where('consultantid', $id)->orderBy('year', 'desc')->get();
		$curyear = intval(date('Y'));
		$highestyear = 0;

		if (count($targets) > 0){
			foreach ($targets as $target){
				if ($target->year > $highestyear){
					$highestyear = $target->year;
				}
			}
		}

        return view('commission.targetuser', [
			'user' => $userObj,
			'targets' => $targets,
			'curyear' => $curyear,
			'highestyear' => $highestyear
		]);
    }

	public function edittargets(Request $request, $id, $year){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboardnew');
		}

		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$target = Targets::where('consultantid', $id)->where('year', $year)->first();

		if (!$target){
			$data = array(
				'year' => $year,
				'consultantid' => $id,
				'currentlease' => 0,
				'monthlylease' => json_encode(array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)),
				'currentleasedeals' => 0,
				'monthlyleasedeals' => json_encode(array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)),
				'currentsale' => 0,
				'currentsaledeals' => 0
			);

			$target = Targets::create($data);
			$target->save();
		}
		if (!$target){
			return redirect('dashboardnew');
		}

		$months = Targets::getmonths();
		$monthlylease = json_decode($target->monthlylease, true);
		$monthlyleasedeals = json_decode($target->monthlyleasedeals, true);

        return view('commission.edittargets', [
			'user' => $userObj,
			'target' => $target,
			'year' => $year,
			'months' => $months,
			'monthlylease' => $monthlylease,
			'monthlyleasedeals' => $monthlyleasedeals
		]);
    }

	public function storetargets(Request $request, $id, $year){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboardnew');
		}

		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$target = Targets::where('consultantid', $id)->where('year', $year)->first();
		if (!$target){
			return redirect('commission/targetuser/'.$id);
		}

		$data = array();

		if (isset($input['currentlease'])){
			$data['currentlease'] = intval($input['currentlease']);
		}
		if (isset($input['currentleasedeals'])){
			$data['currentleasedeals'] = intval($input['currentleasedeals']);
		}
		if (isset($input['currentsale'])){
			$data['currentsale'] = intval($input['currentsale']);
		}
		if (isset($input['currentsaledeals'])){
			$data['currentsaledeals'] = intval($input['currentsaledeals']);
		}
		$monthlylease = array();
		for ($i = 0; $i <= 11; $i++){
			if (isset($input['monthlylease'.$i])){
				$monthlylease[$i] = intval($input['monthlylease'.$i]);
			}else{
				$monthlylease[$i] = 0;
			}
		}
		$data['monthlylease'] = json_encode($monthlylease);
		$monthlyleasedeals = array();
		for ($i = 0; $i <= 11; $i++){
			if (isset($input['monthlyleasedeals'.$i])){
				$monthlyleasedeals[$i] = intval($input['monthlyleasedeals'.$i]);
			}else{
				$monthlyleasedeals[$i] = 0;
			}
		}
		$data['monthlyleasedeals'] = json_encode($monthlyleasedeals);

		$target->fill($data);
		$target->save();

		return redirect('commission/targetuser/'.$id);
    }



	public function notifyaccountant(Request $request){
        $input = $request->all();
		$user = $request->user();

		$subject = 'Change to existing invoice by '.$user->name;
		$message = $user->name.' has made a change to invoice https://db.nest-property.com/commission/invoiceedit/'.intval($input['commid']).', please take a look. Thanks!';

		\Mail::send('emails.generic', ['body' => $message, 'title' => 'Change to existing invoice'], function ($mail) use ($subject) {
			$mail->from('noreply@db.nest-property.com', 'Nest Database');
			$mail->to('accounts@nest-property.com', 'Office Accounts')->subject($subject);
		});

		return redirect('/commission/invoiceedit/'.intval($input['commid'].''));
	}


	public function createinvoices(Request $request, $commid){
        $input = $request->all();
		$user = $request->user();

		$commission = Commission::where('id', '=', $commid)->with('client')->with('property')->with('shortlist')->with('consultant')->first();

		if (empty($commission)){
			return redirect('dashboardnew');
			exit;
		}

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $commission->consultantid)){
			return redirect('dashboardnew');
			exit;
		}

		$basicfields = array(
			'invoicedate' => date('Y-m-d'),
			'invtype' => $commission->salelease,
			'buyername' => '',
			'buyeraddress' => '',
			'sellername' => '',
			'selleraddress' => '',
			'propertyaddress' => '',
			'purchasetext' => '',
			'terms' => '',
			'work_title' => '',
			'work_place' => ''
		);
		$seller = array(
			'feecalculation' => '',
			'price' => $commission->landlordamount,
			'addcosttext' => '',
			'addcost' => '',
			'duedate' => date('Y-m-d')
		);
		$buyer = array(
			'feecalculation' => '',
			'price' => $commission->tenantamount,
			'addcosttext' => '',
			'addcost' => '',
			'duedate' => date('Y-m-d')
		);

		//Trying to fill fields based on information we already have

		if ($commission->salelease == 1){
			//Lease
			//Stamp Duty
			if ($commission->sd_amount > 0 && $commission->stamping == 1){
				$seller['addcosttext'] = '50% of Government Stamp Duty Fee [HK$'.number_format($commission->sd_amount).' ÷ 2]';
				$buyer['addcosttext'] = '50% of Government Stamp Duty Fee [HK$'.number_format($commission->sd_amount).' ÷ 2]';
				$seller['addcost'] = $commission->sd_amount/2;
				$buyer['addcost'] = $commission->sd_amount/2;
			}
			$seller['feecalculation'] = '50% of One Month’s Total Rent';
			$buyer['feecalculation'] = '50% of One Month’s Total Rent';

			$offerletter = Document::where('shortlistid', '=', $commission->shortlistid)->where('propertyid', '=', $commission->propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Property Address
				if (isset($offerletterfieldcontents['f6'])){
					$basicfields['propertyaddress'] = $offerletterfieldcontents['f6'];
				}

				//Tenant Name
				if (isset($offerletterfieldcontents['f7'])){
					$basicfields['buyername'] = $offerletterfieldcontents['f7'];
				}

				//Tenant Address
				if (isset($offerletterfieldcontents['f9']) && isset($offerletterfieldcontents['f10']) && isset($offerletterfieldcontents['f11'])){
					$basicfields['buyeraddress'] = trim(trim($offerletterfieldcontents['f9']).PHP_EOL.trim($offerletterfieldcontents['f10']).PHP_EOL.trim($offerletterfieldcontents['f11']));
				}

				//Landlord Name
				if (isset($offerletterfieldcontents['f36'])){
					$basicfields['sellername'] = $offerletterfieldcontents['f36'];
				}

				// Workplace
				$basicfields['work_place'] = $commission->client->workplace;

				//Landlord Address
				if (isset($offerletterfieldcontents['f50']) && isset($offerletterfieldcontents['f51']) && isset($offerletterfieldcontents['f52'])){
					$basicfields['selleraddress'] = trim(trim($offerletterfieldcontents['f50']).PHP_EOL.trim($offerletterfieldcontents['f51']).PHP_EOL.trim($offerletterfieldcontents['f52']));
				}

				$monthlyrent = 0;
				if (isset($offerletterfieldcontents['f13'])){
					$monthlyrent = $offerletterfieldcontents['f13'];
				}
				$inclexcl = 'inclusive';
				if (isset($offerletterfieldcontents['f35'])){
					$inclexcl = $offerletterfieldcontents['f35'];
				}
				if ($inclexcl == 'inclusive'){
					$basicfields['purchasetext'] = 'HK$'.number_format($monthlyrent, 2).' per month inclusive of Management Fees, Government Rates, Government Rent and Property Tax';
				}else{
					$basicfields['purchasetext'] = 'HK$'.number_format($monthlyrent, 2).' per month exclusive of Management Fees, Government Rates, Government Rent and Property Tax';
				}
				if (isset($offerletterfieldcontents['f16']) && strlen($offerletterfieldcontents['f16']) > 4){
					$commencementdate = date_create($offerletterfieldcontents['f16']);
					$dateto = date_create($offerletterfieldcontents['f16']);
					$dateto->modify('+2 year')->modify('-1 day');

					$basicfields['terms'] = $commencementdate->format('jS F Y').' – '.$dateto->format('jS F Y').' (both days incl.)'.PHP_EOL.'[Two Year Lease, 12+2 month Break Clause]';
				}
			}else{
				$basicfields['buyername'] = $commission->client->firstname . ' ' . $commission->client->lastname;
				$basicfields['work_place'] = $commission->client->workplace;
				$basicfields['buyeraddress'] .=  $commission->client->address1;
				if($commission->client->address1){
					$basicfields['buyeraddress'] .= "\n";
				}
				$basicfields['buyeraddress'] .= $commission->client->address2;
				if($commission->client->address2){
					$basicfields['buyeraddress'] .= "\n";
				}
				$basicfields['buyeraddress'] .= $commission->client->address3;
			}
		}else{
			//Sale
			$basicfields['buyername'] = $commission->client->firstname . ' ' . $commission->client->lastname;
			$basicfields['buyeraddress'] .=  $commission->client->address1;
			if($commission->client->address1){
				$basicfields['buyeraddress'] .= "\n";
			}
			$basicfields['buyeraddress'] .= $commission->client->address2;
			if($commission->client->address2){
				$basicfields['buyeraddress'] .= "\n";
			}
			$basicfields['buyeraddress'] .= $commission->client->address3;
			$basicfields['work_place'] = $commission->client->workplace;

			$seller['feecalculation'] = '1% of the Purchase Price';
			$buyer['feecalculation'] = '1% of the Purchase Price';
		}


        return view('commission.createinvoices', [
			'user' => $user,
			'basicfields' => $basicfields,
			'seller' => $seller,
			'buyer' => $buyer,
			'lease_term' => Comminvoice::lease_term(),
			'month_break_clause' => Comminvoice::month_break_clause()
		]);
    }

	public function createinvoicesstore(Request $request, $commid){
        $input = $request->all();
		$user = $request->user();

		$commission = Commission::where('id', '=', $commid)->with('client')->with('property')->with('shortlist')->with('consultant')->first();

		if (empty($commission)){
			return redirect('dashboardnew');
			exit;
		}

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $commission->consultantid)){
			return redirect('dashboardnew');
			exit;
		}

		if ( (isset($input['bprice']) && $input['bprice'] > 0 ) || (isset($input['baddcost']) && $input['baddcost'] > 0 )  ){
			$data = array();

			$data['commissionid'] = $commid;
			$data['consultantid'] = $user->id;
			$data['invtype'] = $commission->salelease;
			$data['whofor'] = 2;
			$data['status'] = 0;

			if (isset($input['invoicedate'])){
				$data['invoicedate'] = $input['invoicedate'];

				
			}
			if (isset($input['buyername'])){
				$data['buyername'] = $input['buyername'];
			}
			if (isset($input['buyeraddress'])){
				$data['buyeraddress'] = $input['buyeraddress'];
			}
			if (isset($input['sellername'])){
				$data['sellername'] = $input['sellername'];
			}
			if (isset($input['selleraddress'])){
				$data['selleraddress'] = $input['selleraddress'];
			}
			if (isset($input['propertyaddress'])){
				$data['propertyaddress'] = $input['propertyaddress'];
			}
			if (isset($input['purchasetext'])){
				$data['purchasetext'] = $input['purchasetext'];
			}
			if (isset($input['terms'])){
				$data['terms'] = $input['terms'];
			}
			if (isset($input['bfeecalculation'])){
				$data['feecalculation'] = $input['bfeecalculation'];
			}
			if (isset($input['bprice'])){
				$data['price'] = $input['bprice'];
			}
			if (isset($input['baddcosttext'])){
				$data['addcosttext'] = $input['baddcosttext'];
			}
			if (isset($input['baddcost'])){
				$data['addcost'] = $input['baddcost'];
			}
			if (isset($input['bduedate'])){
				$data['duedate'] = $input['bduedate'];
			}
			if (isset($input['work_title'])){
				$data['work_title'] = $input['work_title'];
			}
			if (isset($input['work_place'])){
				$data['work_place'] = $input['work_place'];
			}

			if (isset($input['start_date'])){
				$data['start_date'] = $input['start_date'];
			}
			

			if (isset($input['lease_term'])){
				$data['lease_term'] = $input['lease_term'];

				$date_exp = date('Y-m-d', strtotime($data['start_date']. ' +'. Comminvoice::lease_term()[$input['lease_term']]));

				$data['expiration_date'] = $date_exp;

			}

			if (isset($input['month_break_clause'])){
				$data['month_break_clause'] = $input['month_break_clause'];
			}


			$comm1 = Comminvoice::create($data);
			$comm1->save();
		}

		if ( ( isset($input['sprice']) && $input['sprice'] > 0 ) || (isset($input['saddcost']) && $input['saddcost'] > 0 ) ){
			$data = array();

			$data['commissionid'] = $commid;
			$data['consultantid'] = $user->id;
			$data['invtype'] = $commission->salelease;
			$data['whofor'] = 1;
			$data['status'] = 0;

			if (isset($input['invoicedate'])){
				$data['invoicedate'] = $input['invoicedate'];
			}
			if (isset($input['buyername'])){
				$data['buyername'] = $input['buyername'];
			}
			if (isset($input['buyeraddress'])){
				$data['buyeraddress'] = $input['buyeraddress'];
			}
			if (isset($input['sellername'])){
				$data['sellername'] = $input['sellername'];
			}
			if (isset($input['selleraddress'])){
				$data['selleraddress'] = $input['selleraddress'];
			}
			if (isset($input['propertyaddress'])){
				$data['propertyaddress'] = $input['propertyaddress'];
			}
			if (isset($input['purchasetext'])){
				$data['purchasetext'] = $input['purchasetext'];
			}
			if (isset($input['terms'])){
				$data['terms'] = $input['terms'];
			}
			if (isset($input['sfeecalculation'])){
				$data['feecalculation'] = $input['sfeecalculation'];
			}
			if (isset($input['sprice'])){
				$data['price'] = $input['sprice'];
			}
			if (isset($input['saddcosttext'])){
				$data['addcosttext'] = $input['saddcosttext'];
			}
			if (isset($input['saddcost'])){
				$data['addcost'] = $input['saddcost'];
			}
			if (isset($input['sduedate'])){
				$data['duedate'] = $input['sduedate'];
			}

			if (isset($input['start_date'])){
				$data['start_date'] = $input['start_date'];
			}
			

			if (isset($input['lease_term'])){
				$data['lease_term'] = $input['lease_term'];

				$date_exp = date('Y-m-d', strtotime($data['start_date']. ' +'. Comminvoice::lease_term()[$input['lease_term']]));

				$data['expiration_date'] = $date_exp;

			}

			if (isset($input['month_break_clause'])){
				$data['month_break_clause'] = $input['month_break_clause'];
			}


			$comm2 = Comminvoice::create($data);
			$comm2->save();
		}

		if (!empty($comm1) && !empty($comm2)){
			$commission->invoicenumbers = ''.($comm1->id).', '.($comm2->id);
		}else if (!empty($comm1)){
			$commission->invoicenumbers = ''.($comm1->id);
		}else if (!empty($comm2)){
			$commission->invoicenumbers = ''.($comm2->id);
		}
		$commission->invoicedate = $input['invoicedate'];;
		$commission->save();

		return redirect('/commission/invoiceedit/'.$commid);
	}

	public function approval(Request $request){
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$invoices = Comminvoice::whereIn('status', [0, 2])->get();

		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

        return view('commission.approval', [
			'invoices' => $invoices,
			'consultants' => $consultants
		]);
	}

	public function approveinvoice(Request $request, $invoiceid){
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$invoice = Comminvoice::where('id', $invoiceid)->with('consultant')->first();

		if (!empty($invoice)){
			$invoice->status = 1;
			$invoice->save();

			$commission = Commission::where('id', '=', $invoice->commissionid)->first();
			if (!empty($commission)){
				$commission->status = 2;
				$commission->save();
			}


			//Create PDF invoice and send it via email
			// $html = view('commission.pdfinvoice', [
			// 	'inv' => $invoice,
			// 	'logo' => 'yes'
			// ]);
			$html = view('commission.pdfinvoice_v2', [
				'inv' => $invoice,
				'logo' => 'yes'
			]);

			if ($invoice->whofor == 1){
				$name = '#'.($invoice->id).' LL '.time().'.pdf';
			}else{
				$name = '#'.($invoice->id).' TT '.time().'.pdf';
			}
			$path = '../storage/pdfmail/'.$name;

			\PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->save($path);

			Mail::send('commission.invoiceemail', ['invoice' => $invoice], function ($m) use ($invoice, $path) {
				$m->from('noreply@nest-property.com', 'Nest Property');
				$m->to('xero.inbox.kkw3c.fzhvged4bzqeslea@xerofiles.com');
				$m->subject('Invoice #'.$invoice->id);
				$m->attach(storage_path($path));
			});

			
			if(count(Mail::failures()) > 0){
				$mail = "Email to Xero failed";
			} else {
				$mail = "Successfully sent to Xero";
			}
			

			$XeroLogs = new XeroLogs();
			$XeroLogs->user_id = $user->id;
			$XeroLogs->invoiceid = $invoiceid;
			$XeroLogs->message = $mail;
			$XeroLogs->save();

			$consultantEmail = $invoice->consultant->email;		

			$fromEmail = $user->email;
			$fromname = $user->firstname.' '. $user->lastname;	
		
			$message = 'Your invoice request has been approved: <a href="https://db.nest-property.com/commission/editinvoice/'.$invoice->id.'">https://db.nest-property.com/commission/editinvoice/'.$invoice->id.' </a><br />See attached files.<br />';	

			$subject = 'Nest Property — Invoice '.$invoice->id.' has been approved';		
		

			\Mail::send('emails.generic', ['body' => $message, 'title' => 'Invoicing Update'], function ($mail) use ($consultantEmail, $subject, $fromEmail, $message,$path) {
				$tos = [$consultantEmail];
				$mail->from($fromEmail, 'Nest Database');
				$mail->to($tos, 'Office Accounts')
				->subject($subject)			
				->setBody($message, 'text/html');
				$mail->attach(storage_path($path));
			});

		}

		return redirect('/commission/approval');
	}

	public function invoicenote(Request $request){
		$user = $request->user();

		$invoices = Comminvoice::whereIn('status', [1])->where('consultantid', $user->id)->get();

        return view('commission.invoicenote', [
			'invoices' => $invoices
		]);
	}

	public function invoicenoted(Request $request, $invoiceid){
		$user = $request->user();

		$invoice = Comminvoice::where('id', $invoiceid)->where('consultantid', $user->id)->first();

		if (!empty($invoice)){
			$invoice->status = 4;
			$invoice->save();
		}

		return redirect('/commission/invoicenote');
	}


	public function downloadinvoice(Request $request, $invoiceid, $logo = 'yes'){
		$user = $request->user();

		$invoice = Comminvoice::where('id', $invoiceid)->first();

		if (empty($invoice)){
			return redirect('dashboardnew');
			exit;
		}

		//check if user has the rights
		$rights = $user->getUserRights();

		if ($invoice->status == 1 || $invoice->status == 4){
			/*if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $invoice->consultantid)){
				return redirect('dashboardnew');
				exit;
			}*/
		}else{
			if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
				return redirect('dashboardnew');
				exit;
			}
		}

		if ($logo == 'no') {
			$template = 'commission.pdfinvoice';
		} else {
			switch ($request->version) {
				case 1: $template = 'commission.pdfinvoice'; break;
				case 3: $template = 'commission.pdfinvoice_v3'; break;
				case 2: default: $template = 'commission.pdfinvoice_v2'; break;
			}
		}

		$html = view($template, [
			'inv' => $invoice,
			'logo' => $logo
		]);
        // return $html;

		if ($invoice->whofor == 1){
			$name = '#'.($invoice->id).' LL '.trim(preg_replace('/\s\s+/', ' ', $invoice->sellername)).'.pdf';
		}else{
			$name = '#'.($invoice->id).' TT '.trim(preg_replace('/\s\s+/', ' ', $invoice->buyername)).'.pdf';
		}

        return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);

	}


	public function editinvoice(Request $request, $invoiceid){
		$user = $request->user();

		$invoice = Comminvoice::where('id', $invoiceid)->first();

		if (empty($invoice)){
			return redirect('dashboardnew');
			exit;
		}

		//check if user has the rights
		$rights = $user->getUserRights();

		//if ($invoice->status == 1 || $invoice->status == 4){
			if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $invoice->consultantid)){
				return redirect('dashboardnew');
				exit;
			}
		/*}else{
			if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
				return redirect('dashboardnew');
				exit;
			}
		}*/

		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}

		$comments = array();

		if(count(json_decode($invoice->comments)) > 0){

			foreach(json_decode($invoice->comments) as $comment){
				
				$uu = $u[$comment->userid];
				$comments[] = array(
					'user_pic'=>  $uu->picpath,
					'user_name'=>  $uu->firstname.' '.$uu->lastname,
					'date'=> $comment->datetime,
					'comment'=>$comment->content,

				);
							
			}
		}

		rsort($comments);	

		return view('commission.editinvoices', [
			'inv' => $invoice,
			'comments' => $comments,
			'lease_term' => Comminvoice::lease_term(),
			'month_break_clause' => Comminvoice::month_break_clause()
		]);
	}

	public function editinvoicestore(Request $request, $invoiceid){
        $input = $request->all();
		$user = $request->user();

		$invoice = Comminvoice::where('id', $invoiceid)->with('consultant')->first();

		if (empty($invoice)){
			return redirect('dashboardnew');
			exit;
		}

		//check if user has the rights
		$rights = $user->getUserRights();

		//if ($invoice->status == 1 || $invoice->status == 4){
			if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $invoice->consultantid)){
				return redirect('dashboardnew');
				exit;
			}
		/*}else{
			if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
				return redirect('dashboardnew');
				exit;
			}
		}*/

		

		if (isset($input['price']) && $input['price'] > 0){
			$data = array();

			$data['status'] = 2;

			if (isset($input['invoicedate'])){
				$data['invoicedate'] = $input['invoicedate'];			
			}
			if (isset($input['buyername'])){
				$data['buyername'] = $input['buyername'];
			}
			if (isset($input['buyeraddress'])){
				$data['buyeraddress'] = $input['buyeraddress'];
			}
			if (isset($input['sellername'])){
				$data['sellername'] = $input['sellername'];
			}
			if (isset($input['selleraddress'])){
				$data['selleraddress'] = $input['selleraddress'];
			}
			if (isset($input['propertyaddress'])){
				$data['propertyaddress'] = $input['propertyaddress'];
			}
			if (isset($input['purchasetext'])){
				$data['purchasetext'] = $input['purchasetext'];
			}
			if (isset($input['terms'])){
				$data['terms'] = $input['terms'];
			}
			if (isset($input['feecalculation'])){
				$data['feecalculation'] = $input['feecalculation'];
			}
			if (isset($input['price'])){
				$data['price'] = $input['price'];
			}
			if (isset($input['addcosttext'])){
				$data['addcosttext'] = $input['addcosttext'];
			}
			if (isset($input['addcost'])){
				$data['addcost'] = $input['addcost'];
			}
			if (isset($input['duedate'])){
				$data['duedate'] = $input['duedate'];
			}
			if (isset($input['work_title'])){
				$data['work_title'] = $input['work_title'];
			}
			if (isset($input['work_place'])){
				$data['work_place'] = $input['work_place'];
			}
			if (isset($input['start_date'])){
				$data['start_date'] = $input['start_date'];
			}
			

			if (isset($input['lease_term'])){
				$data['lease_term'] = $input['lease_term'];

				$date_exp = date('Y-m-d', strtotime($data['start_date']. ' +'. Comminvoice::lease_term()[$input['lease_term']]));

				$data['expiration_date'] = $date_exp;

			}

			if (isset($input['month_break_clause'])){
				$data['month_break_clause'] = $input['month_break_clause'];
			}


			

			$invoice->fill($data);
			$invoice->save();
		}


		return redirect('/commission/invoiceedit/'.$invoice->commissionid);
	}

	public function editinvoicecommentstore(Request $request){
        $input = $request->all();
		$user = $request->user();

	
		$invoice = Comminvoice::where('id', $input['id'])->with('consultant')->first();

		//check if user has the rights
		$rights = $user->getUserRights();
		
		if (empty($invoice)){
			return redirect('dashboardnew');
			exit;
		}

		$comment = '';
		if (isset($input['comments'])){
			$comment = trim(filter_var($input['comments'], FILTER_SANITIZE_STRING));
		}

		if (trim($invoice->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($invoice->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($invoice->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($invoice->comments)
					)
				);
			}
		}

		$newcomment = array(
			'userid' => $user->id,
			'datetime' => Carbon::now()->toDateTimeString(),
			'content' => $comment
		);

	
		$comments[] = $newcomment;

		$data['comments'] = json_encode($comments);

		$invoice->fill($data);
		$invoice->save();


		if($rights['Superadmin']  || $rights['Director'] || $rights['Accountant']){			
		
			$consultantEmail = $invoice->consultant->email;		

			$fromEmail = $user->email;
			$fromname = $user->firstname.' '. $user->lastname;	
		
			$message = 'Your invoice request has been rejected. <br/> Please check the comments on invoice and resubmit for approval: <a href="https://db.nest-property.com/commission/editinvoice/'.$invoice->id.'">https://db.nest-property.com/commission/editinvoice/'.$invoice->id.' </a><br /><br />';	

			$subject = 'Nest Property — Invoice '.$invoice->id.' has been rejected';		
		

			\Mail::send('emails.generic', ['body' => $message, 'title' => 'Invoicing Update'], function ($mail) use ($consultantEmail, $subject, $fromEmail, $message) {
				$tos = [$consultantEmail];
				$mail->from($fromEmail, 'Nest Database');
				$mail->to($tos, 'Office Accounts')
				->subject($subject)			
				->setBody($message, 'text/html');
			});

		} else {
			

			$fromEmail = $user->email;
			$fromname = $user->firstname.' '. $user->lastname;	
		
			$message = 'Please check the comments on this invoice: <a href="https://db.nest-property.com/commission/editinvoice/'.$invoice->id.'">https://db.nest-property.com/commission/editinvoice/'.$invoice->id.' </a><br /><br />';	

			$subject = 'Nest Property — Invoice '.$invoice->id.' has new comment';		
		

			\Mail::send('emails.generic', ['body' => $message, 'title' => 'Invoicing Update'], function ($mail) use ($subject, $fromEmail, $message) {
				$tos = ['naomi.budden@nest-property.com','william.budden@nest-property.com'];
				$mail->from($fromEmail, 'Nest Database');
				$mail->to($tos, 'Office Accounts')
				->subject($subject)			
				->setBody($message, 'text/html');
			});
		}


		return redirect('/commission/editinvoice/'.$invoice->id);
	}

	public function creategeninvoices(Request $request){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$inv = array(
			'invoicedate' => date('Y-m-d'),
			'invtype' => 3,
			'buyername' => '',
			'buyeraddress' => '',
			'feecalculation' => '',
			'price' => 0,
			'addcosttext' => '',
			'addcost' => 0,
			'duedate' => date('Y-m-d')
		);


        return view('commission.creategeninvoices', [
			'user' => $user,
			'inv' => $inv
		]);
    }


	public function creategeninvoicesstore(Request $request){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		if (isset($input['price']) && $input['price'] > 0){
			$data = array();

			$data['commissionid'] = 0;
			$data['consultantid'] = $user->id;
			$data['invtype'] = 3;
			$data['whofor'] = 0;
			$data['status'] = 1;

			if (isset($input['invoicedate'])){
				$data['invoicedate'] = $input['invoicedate'];
			}
			if (isset($input['buyername'])){
				$data['buyername'] = $input['buyername'];
			}
			if (isset($input['buyeraddress'])){
				$data['buyeraddress'] = $input['buyeraddress'];
			}
			if (isset($input['feecalculation'])){
				$data['feecalculation'] = $input['feecalculation'];
			}
			if (isset($input['price'])){
				$data['price'] = $input['price'];
			}
			if (isset($input['addcosttext'])){
				$data['addcosttext'] = $input['addcosttext'];
			}
			if (isset($input['addcost'])){
				$data['addcost'] = $input['addcost'];
			}
			if (isset($input['duedate'])){
				$data['duedate'] = $input['duedate'];
			}


			$comm = Comminvoice::create($data);
			$comm->save();
		}

		return redirect('/commission/geninvoicing');
	}

	public function geninvoicing(Request $request){
        $input = $request->all();
		$cu = $request->user();

		if (!($cu->getUserRights()['Superadmin'] || $cu->getUserRights()['Accountant'] || $cu->getUserRights()['Director'])){
			return redirect('dashboardnew');
			exit;
		}

		$invoices = Comminvoice::where('commissionid', 0)->orderBy('id', 'desc')->paginate(20);

		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		return view('commission.geninvoicing', [
			'input' => $request,
			'consultants' => $consultants,
			'invoices' => $invoices
		]);
	}

	public function editgeninvoices(Request $request, $invoiceid){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$inv = Comminvoice::where('id', $invoiceid)->first();

        return view('commission.editgeninvoices', [
			'user' => $user,
			'inv' => $inv
		]);
    }

	public function editgeninvoicesstore(Request $request, $invoiceid){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$inv = Comminvoice::where('id', $invoiceid)->first();

		if (!empty($inv) && isset($input['price']) && $input['price'] > 0){
			$data = array();

			if (isset($input['invoicedate'])){
				$data['invoicedate'] = $input['invoicedate'];
			}
			if (isset($input['buyername'])){
				$data['buyername'] = $input['buyername'];
			}
			if (isset($input['buyeraddress'])){
				$data['buyeraddress'] = $input['buyeraddress'];
			}
			if (isset($input['feecalculation'])){
				$data['feecalculation'] = $input['feecalculation'];
			}
			if (isset($input['price'])){
				$data['price'] = $input['price'];
			}
			if (isset($input['addcosttext'])){
				$data['addcosttext'] = $input['addcosttext'];
			}
			if (isset($input['addcost'])){
				$data['addcost'] = $input['addcost'];
			}
			if (isset($input['duedate'])){
				$data['duedate'] = $input['duedate'];
			}

			$inv->fill($data);
			$inv->save();
		}





		return redirect('/commission/geninvoicing');
    }

	public function downloadgeninvoice(Request $request, $invoiceid, $logo = 'yes'){
		$user = $request->user();

		$invoice = Comminvoice::where('id', $invoiceid)->first();

		if (empty($invoice)){
			return redirect('dashboardnew');
			exit;
		}

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$html = view('commission.pdfgeninvoice', [
			'inv' => $invoice,
			'logo' => $logo
		]);
        //return $html;

		$name = 'Invoice #'.($invoice->id).'.pdf';

        return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);

	}


	public function salaryusers(Request $request)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		//load all users
		$usersObj = User::all();
		$users = array();

		foreach ($usersObj as $u){
			//if ($u->id != $user->id){
				$uarr = $u->toArray();
				$uarr['rightsstr'] = $u->getUserRightsString();

				$users[] = $uarr;
			//}
		}

		for ($i = 0; $i < count($users)-1; $i++){
			for ($a = $i+1; $a < count($users); $a++){
				if ($users[$i]['name'] > $users[$a]['name']){
					$help = $users[$i];
					$users[$i] = $users[$a];
					$users[$a] = $help;
				}
			}
		}

        return view('commission.salaryusers', [
			'users' => $users
		]);
    }


	public function salaries(Request $request, $id)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$salaries = array();

		$ratesAPI = $this->getRateAPI();			
	

		$srs = Salary::where('userid', $id)->get();
		if (count($srs) > 0){
			foreach ($srs as $s){
				if (!isset($salaries[$s->year])){
					$salaries[$s->year] = array();
				}
				$salaries[$s->year][$s->month] = $s;
				
				//echo $s->fixedfees + $s->commissionfees + $s->otherfees - $s->mpfemployee - $s->otherdeductions."<br>";
				$salaries[$s->year][$s->month]['total'] = $s->fixedfees + $s->commissionfees + $s->otherfees - $s->mpfemployee - $s->otherdeductions;

				if(isset($ratesAPI->rates)){
					$eur = $salaries[$s->year][$s->month]['total'] / $ratesAPI->rates->HKD;

					$salaries[$s->year][$s->month]['total_USD'] = $eur * $ratesAPI->rates->USD;
					$salaries[$s->year][$s->month]['total_EUR'] = $eur;
					$salaries[$s->year][$s->month]['total_ZAR'] = $eur * $ratesAPI->rates->ZAR;
				} else {
					$salaries[$s->year][$s->month]['total_USD'] = 0;
					$salaries[$s->year][$s->month]['total_EUR'] = 0;
					$salaries[$s->year][$s->month]['total_ZAR'] = 0;
				}
				
			}
		}

		$curyear = intval(date('Y'));
		$curmonth = intval(date('m'));

		$months = Salary::getMonthNames();


        return view('commission.salaries', [
			'user' => $userObj,
			'salaries' => $salaries,
			'curyear' => $curyear,
			'curmonth' => $curmonth,
			'months' => $months
		]);

    }

	public function getRateAPI(){	
	
		$response = $this->curl_get_contents('http://api.exchangeratesapi.io/v1/latest?access_key=b6688eea4b230d4cb4ee7e6cd8113fa7&base=EUR');
		
		if( !isset( json_decode($response)->rates ) ){			
			$response2 = $this->curl_get_contents('https://v6.exchangerate-api.com/v6/7945342c1ff69df1cbe732fd/latest/EUR');
			$val = json_decode($response2);
			$response3 = array();
			$response3['rates'] = $val->conversion_rates;			
		
			return json_decode(json_encode($response3));
		} else {
		
			return json_decode($response);
		}
		
	
	}

	function curl_get_contents($url)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}


	public function createsalary(Request $request, $userid, $year, $month)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$salary = Salary::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$salary || empty($salary)){
			$salary = Salary::create();

			$lastyear = $year;
			$lastmonth = $month - 1;
			if ($lastmonth == 0){
				$lastmonth = 12;
				$lastyear--;
			}
			$lastsalary = Salary::where('userid', $userid)->where('year', $lastyear)->where('month', $lastmonth)->first();

			$data = array();
			$data['userid'] = $userid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['paymode'] = 'Cheque';
			$data['chequenumber'] = '';

			$data['employeename'] = $userObj->firstname.' '.$userObj->lastname;
			$data['hkid'] = $userObj->hkid;
			$data['employeeaddress'] = $userObj->address;

			if (!empty($lastsalary) && $lastsalary->fixedfees > 0){
				$data['fixedfees'] = $lastsalary->fixedfees;
			}else{
				$data['fixedfees'] = 0;
			}
			$data['otherdeductions'] = 0;
			$data['otherfees'] = 0;

			$data['otherfeestext'] = '';
			$data['otherdeductionstext'] = '';
			$data['mpfsixty'] = 0;

			$dd = date_create($year.'-'.$month.'-01');
			$data['payperiodfrom'] = $dd->format('Y-m-d');
			$dd = $dd->modify('+1 month')->modify('-1 day');
			$data['payperiodto'] = $dd->format('Y-m-d');

			//Commissions
			$dateto = date_create($year.'-'.$month.'-25');
			$datefrom = date_create($year.'-'.$month.'-25');
			$datefrom = $datefrom->modify('-1 month')->modify('+1 day');

			$invoicesll = Commission::where('landlordpaiddate', '>=', $datefrom)->where('landlordpaiddate', '<=', $dateto)->where('landlordamount', '>', 0)->with('property')->with('client')->get();
			$invoicestt = Commission::where('tenantpaiddate', '>=', $datefrom)->where('tenantpaiddate', '<=', $dateto)->where('tenantamount', '>', 0)->with('property')->with('client')->get();

			$ids = array();
			$invll = array();
			if (count($invoicesll) > 0){
				foreach ($invoicesll as $i){
					$ids[] = $i->id;
					$invll[$i->id] = $i;
				}
			}
			$invtt = array();
			if (count($invoicestt) > 0){
				foreach ($invoicestt as $i){
					$ids[] = $i->id;
					$invtt[$i->id] = $i;
				}
			}

			$commcons = Commcons::whereIn('commissionid', $ids)->where('consultantid', $userid)->get();

			$invoiceoutput = array();
			$commissionfees = 0;
			if (count($commcons) > 0){
				foreach ($commcons as $cc){
					$landlord = false;
					$tenant = false;
					$invoice = null;

					if (isset($invll[$cc->commissionid])){
						$invoice = $invll[$cc->commissionid];
						$landlord = true;
					}
					if (isset($invtt[$cc->commissionid])){
						$invoice = $invtt[$cc->commissionid];
						$tenant = true;
					}

					if ($landlord && $tenant){
						$invoiceoutput[] = array(
							'invoice' => $invoice,
							'amount' => $cc->payout,
							'type' => 'Landlord & Tenant'
						);
						$commissionfees += $cc->payout;
					}else if ($landlord){
						$invoiceoutput[] = array(
							'invoice' => $invoice,
							'amount' => round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
							'type' => 'Landlord'
						);
						$commissionfees += round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
					}else if ($tenant){
						$invoiceoutput[] = array(
							'invoice' => $invoice,
							'amount' => $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
							'type' => 'Tenant'
						);
						$commissionfees += $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
					}
				}
			}

			$data['commissionfees'] = $commissionfees;
			$data['mpfemployee'] = round($commissionfees*0.05, 2);
			$data['mpfemployer'] = round($commissionfees*0.05, 2);
			if ($commissionfees < 7100){
				$data['mpfemployee'] = 0;
			}
			if ($commissionfees > 30000){
				$data['mpfemployee'] = 1500;
				$data['mpfemployer'] = 1500;
			}
			$data['netincome'] = $commissionfees-$data['mpfemployee'];

			$salary->fill($data);
			$salary->save();
		}

		return redirect('/commission/editsalary/'.$userid.'/'.$year.'/'.$month);
    }

	public function editsalary(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$salary = Salary::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$salary || empty($salary)){
			return redirect('/commission/salaries/'.$userid);
		}

		$months = Salary::getMonthNames();

		//Commissions
		$dateto = date_create($year.'-'.$month.'-25');
		$datefrom = date_create($year.'-'.$month.'-25');
		$datefrom = $datefrom->modify('-1 month')->modify('+1 day');

		$invoicesll = Commission::where('landlordpaiddate', '>=', $datefrom)->where('landlordpaiddate', '<=', $dateto)->where('landlordamount', '>', 0)->with('property')->with('client')->with('consultant')->get();
		$invoicestt = Commission::where('tenantpaiddate', '>=', $datefrom)->where('tenantpaiddate', '<=', $dateto)->where('tenantamount', '>', 0)->with('property')->with('client')->with('consultant')->get();

		$ids = array();
		$invll = array();
		if (count($invoicesll) > 0){
			foreach ($invoicesll as $i){
				$ids[] = $i->id;
				$invll[$i->id] = $i;
			}
		}
		$invtt = array();
		if (count($invoicestt) > 0){
			foreach ($invoicestt as $i){
				$ids[] = $i->id;
				$invtt[$i->id] = $i;
			}
		}

		$commcons = Commcons::whereIn('commissionid', $ids)->where('consultantid', $userid)->get();

		$invoiceoutput = array();
		$commissionfees = 0;
		if (count($commcons) > 0){
			foreach ($commcons as $cc){
				$landlord = false;
				$tenant = false;
				$invoice = null;

				if (isset($invll[$cc->commissionid])){
					$invoice = $invll[$cc->commissionid];
					$landlord = true;
				}
				if (isset($invtt[$cc->commissionid])){
					$invoice = $invtt[$cc->commissionid];
					$tenant = true;
				}

				if ($landlord && $tenant){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => $cc->payout,
						'type' => 'Landlord & Tenant'
					);
					$commissionfees += $cc->payout;
				}else if ($landlord){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
						'type' => 'Landlord'
					);
					$commissionfees += round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
				}else if ($tenant){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
						'type' => 'Tenant'
					);
					$commissionfees += $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
				}
			}
		}

		$managingbonus = array();
		$managingcommissionfees = 0;
		$managing = $userObj->getUserManaging();
		if (count($managing) > 0){
			//run through the invoice results again and do everything for "managing" ids
			$commcons = Commcons::whereIn('commissionid', $ids)->whereIn('consultantid', array_keys($managing))->get();

			$managingbonus = array();
			if (count($commcons) > 0){
				foreach ($commcons as $cc){
					$landlord = false;
					$tenant = false;
					$invoice = null;

					if (isset($invll[$cc->commissionid])){
						$invoice = $invll[$cc->commissionid];
						$landlord = true;
					}
					if (isset($invtt[$cc->commissionid])){
						$invoice = $invtt[$cc->commissionid];
						$tenant = true;
					}

					if ($landlord && $tenant){
						$managingbonus[] = array(
							'invoice' => $invoice,
							'amount' => round($cc->commissionconsultant*0.05, 2),
							'type' => 'Landlord & Tenant'
						);
						$managingcommissionfees += round($cc->commissionconsultant*0.05, 2);
					}else if ($landlord){
						$managingbonus[] = array(
							'invoice' => $invoice,
							'amount' => round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->commissionconsultant*0.05, 2),
							'type' => 'Landlord'
						);
						$managingcommissionfees += round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->commissionconsultant*0.05, 2);
					}else if ($tenant){
						$managingbonus[] = array(
							'invoice' => $invoice,
							'amount' => round($cc->commissionconsultant*0.05, 2)-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->commissionconsultant*0.05, 2),
							'type' => 'Tenant'
						);
						$managingcommissionfees += round($cc->commissionconsultant*0.05, 2)-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->commissionconsultant*0.05, 2);
					}
				}
			}


		}

		$leadingBonus = array();
		$leadingCommissionFees = 0;
		$leading = $userObj->getSalesLeaders();
		if (count($leading) > 0){
			//run through the invoice results again and do everything for "leading" ids
			$commconsquery = Commcons::whereIn('commissionid', $ids)->whereIn('consultantid', array_keys($leading))->where('commissiondate', '!=', '0000-00-00');
			if ($userObj->id == 4) {
				// for month of May, show all commissions for Will Robertson
				if($datefrom->format('Y-m-d') == "2020-04-26" && $dateto->format('Y-m-d') == "2020-05-25"){
					$commconsquery->where(function($query) use ($datefrom, $dateto){
						return $query->orWhere(function($query) use ($datefrom, $dateto){
							return $query->where('landlordpaiddate', '>=', $datefrom)
							->where('landlordpaiddate', '<=', $dateto);
						})->orWhere(function($query) use ($datefrom, $dateto){
							return $query->where('tenantpaiddate', '>=', $datefrom)
							->where('tenantpaiddate', '<=', $dateto);
						});
					});
				}else{
					$commconsquery->where('commissiondate', '>=', '2020-05-01');					
				}
			} else {
				$commconsquery->where('commissiondate', '>=', '2020-08-12');
			}
			$commcons = $commconsquery->get();

			$leadingBonus = array();
			if (count($commcons) > 0){
				foreach ($commcons as $cc){
					$landlord = false;
					$tenant = false;
					$invoice = null;

					if (isset($invll[$cc->commissionid])){
						$invoice = $invll[$cc->commissionid];
						$landlord = true;
					}
					if (isset($invtt[$cc->commissionid])){
						$invoice = $invtt[$cc->commissionid];
						$tenant = true;
					}

					$type = '';
					if ($landlord && $tenant){
						$type = 'Landlord & Tenant';
					}else if ($landlord){
						$type = 'Landlord';
					}else if ($tenant){
						$type = 'Tenant';
					}

					$leadingBonus[] = array(
						'invoice' => $invoice,
						'amount' => round($cc->payout*Commcons::LEADING_COMMISION, 2),
						'type' => $type
					);
					$leadingCommissionFees += round($cc->payout*Commcons::LEADING_COMMISION, 2);
				}
			}
		}		
		
				
		$io_commission = IOCommission::where('io_commissions.status', '=', 'Approve')
			->where('io_commissions.user_id', '=', $userid)
			->whereBetween('io_commissions.updated_at', [$datefrom, $dateto])
			//->whereYear('io_commissions.salarydate', $year)
			->leftJoin('users', 'io_commissions.user_id', '=', 'users.id')
			->leftJoin('properties', 'io_commissions.property_id', '=', 'properties.id')
			->leftJoin('vendors', 'properties.owner_id', '=', 'vendors.id')
			->leftJoin('commission', 'io_commissions.invoiceId', '=', 'commission.id')
            ->leftJoin('clients', 'commission.clientid', '=', 'clients.id')
			->select(
				'io_commissions.id as id',
				'io_commissions.status as status',
				'io_commissions.commission as commission',
				'io_commissions.salarydate as salarydate',
				'io_commissions.invoiceId as invoiceId',
				'users.firstname as firstname',
				'users.lastname as lastname',
				'properties.name as name',
				'vendors.company as company',
				'vendors.firstname as owner_firstname',
				'vendors.lastname as owner_lastname',
				'clients.firstname as client_firstname',
                'clients.lastname as client_lastname')
			->orderBy('io_commissions.id','desc')
			->get();
		
        return view('commission.salary', [
			'user' => $userObj,
			'salary' => $salary,
			'year' => $year,
			'month' => $month,
			'months' => $months,
			'invoiceoutput' => $invoiceoutput,
			'commissionfees' => $commissionfees,
			'managingbonus' => $managingbonus,
			'managing' => $managing,
			'managingcommissionfees' => $managingcommissionfees,
			'leading' => $leading,
			'leadingBonus' => $leadingBonus,
			'leadingCommissionFees' => $leadingCommissionFees,
			'io_commission' => $io_commission
		]);
	}


	public function editsalarystore(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$salary = Salary::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$salary || empty($salary)){
			return redirect('/commission/salaries/'.$userid);
		}

		$data = array(
			'paymode' => $input['paymode'],
			'chequenumber' => $input['chequenumber'],
			'fixedfees' => $input['fixedfees'],
			'commissionfees' => $input['commissionfees'],
			'otherfees' => $input['otherfees'],
			'mpfemployee' => $input['mpfemployee'],
			'mpfemployer' => $input['mpfemployer'],
			'otherdeductions' => $input['otherdeductions'],
			'netincome' => $input['fixedfees']+$input['commissionfees']+$input['otherfees']-$input['mpfemployee']-$input['otherdeductions'],
			'employeename' => $input['employeename'],
			'hkid' => $input['hkid'],
			'employeeaddress' => $input['employeeaddress'],
			'otherfeestext' => $input['otherfeestext'],
			'otherdeductionstext' => $input['otherdeductionstext'],
			'payperiodfrom' => $input['payperiodfrom'],
			'payperiodto' => $input['payperiodto'],
			'paymentdate' => $input['paymentdate']
		);
		if (isset($input['mpfsixty'])){
			$data['mpfsixty'] = 1;
			$data['mpfemployee'] = 0;
			$data['mpfemployer'] = 0;
		}else{
			$data['mpfsixty'] = 0;
		}
		$salary->fill($data);
		$salary->save();

		return redirect('/commission/editsalary/'.$userid.'/'.$year.'/'.$month);
	}



	public function pdfsalary(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $userid)){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$salary = Salary::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$salary || empty($salary)){
			return redirect('/commission/salaries/'.$userid);
		}

		$leadingCommissionFees = 0;
		$leading = $userObj->getSalesLeaders();
		if (count($leading) > 0){

			$dateto = date_create($year.'-'.$month.'-25');
			$datefrom = date_create($year.'-'.$month.'-25');
			$datefrom = $datefrom->modify('-1 month')->modify('+1 day');

			$invoicesll = Commission::where('landlordpaiddate', '>=', $datefrom)->where('landlordpaiddate', '<=', $dateto)->where('landlordamount', '>', 0)->with('property')->with('client')->get()->pluck('id')->toArray();
			$invoicestt = Commission::where('tenantpaiddate', '>=', $datefrom)->where('tenantpaiddate', '<=', $dateto)->where('tenantamount', '>', 0)->with('property')->with('client')->get()->pluck('id')->toArray();

			$ids = array_merge($invoicesll, $invoicestt);

			//run through the invoice results again and do everything for "leading" ids
			$commconsquery = Commcons::whereIn('commissionid', $ids)->whereIn('consultantid', array_keys($leading))->where('commissiondate', '!=', '0000-00-00');
			if ($userObj->id == 4) {
				if($datefrom->format('Y-m-d') == "2020-04-26" && $dateto->format('Y-m-d') == "2020-05-25"){
					$commconsquery->where(function($query) use ($datefrom, $dateto){
						return $query->orWhere(function($query) use ($datefrom, $dateto){
							return $query->where('landlordpaiddate', '>=', $datefrom)
							->where('landlordpaiddate', '<=', $dateto);
						})->orWhere(function($query) use ($datefrom, $dateto){
							return $query->where('tenantpaiddate', '>=', $datefrom)
							->where('tenantpaiddate', '<=', $dateto);
						});
					});
				}else{
					$commconsquery->where('commissiondate', '>=', '2020-05-01');					
				}
			} else {
				$commconsquery->where('commissiondate', '>=', '2020-08-12');
			}
			$commcons = $commconsquery->get();

			if (count($commcons) > 0){
				foreach ($commcons as $cc){
					$leadingCommissionFees += round($cc->payout*Commcons::LEADING_COMMISION, 2);
				}
			}
		}

		$months = Salary::getMonthNames();

		$dateto = date_create($year.'-'.$month.'-25');
		$datefrom = date_create($year.'-'.$month.'-25');
		$datefrom = $datefrom->modify('-1 month')->modify('+1 day');
		$io_commission = 0;

		$io_commission = IOCommission::where('io_commissions.status', '=', 'Approve')
			->where('io_commissions.user_id', '=', $userid)
			->whereBetween('io_commissions.updated_at', [$datefrom, $dateto])								
			->sum('io_commissions.commission');
			
			 

        $html = view('commission.pdfsalary', [
			'user' => $userObj,
			'salary' => $salary,
			'year' => $year,
			'month' => $month,
			'months' => $months,
			'leadingCommissionFees' => $leadingCommissionFees,
			'io_commission' => $io_commission
		]);
        // return $html;

		$name = 'Salary '.$userObj->firstname.' '.$userObj->lastname.' '.$months[$month].' '.$year.'.pdf';

		return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
	}


	public function pdfcommissionlist(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $userid)){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$salary = Salary::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$salary || empty($salary)){
			return redirect('/commission/salaries/'.$userid);
		}

		$months = Salary::getMonthNames();

		//Commissions
		$dateto = date_create($year.'-'.$month.'-25');
		$datefrom = date_create($year.'-'.$month.'-25');
		$datefrom = $datefrom->modify('-1 month')->modify('+1 day');

		$invoicesll = Commission::where('landlordpaiddate', '>=', $datefrom)->where('landlordpaiddate', '<=', $dateto)->where('landlordamount', '>', 0)->with('property')->with('client')->get();
		$invoicestt = Commission::where('tenantpaiddate', '>=', $datefrom)->where('tenantpaiddate', '<=', $dateto)->where('tenantamount', '>', 0)->with('property')->with('client')->get();

		$ids = array();
		$invll = array();
		if (count($invoicesll) > 0){
			foreach ($invoicesll as $i){
				$ids[] = $i->id;
				$invll[$i->id] = $i;
			}
		}
		$invtt = array();
		if (count($invoicestt) > 0){
			foreach ($invoicestt as $i){
				$ids[] = $i->id;
				$invtt[$i->id] = $i;
			}
		}

		$commcons = Commcons::whereIn('commissionid', $ids)->where('consultantid', $userid)->get();

		$invoiceoutput = array();
		$commissionfees = 0;
		if (count($commcons) > 0){
			foreach ($commcons as $cc){
				$landlord = false;
				$tenant = false;
				$invoice = null;

				if (isset($invll[$cc->commissionid])){
					$invoice = $invll[$cc->commissionid];
					$landlord = true;
				}
				if (isset($invtt[$cc->commissionid])){
					$invoice = $invtt[$cc->commissionid];
					$tenant = true;
				}

				if ($landlord && $tenant){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => $cc->payout,
						'type' => 'Landlord & Tenant'
					);
					$commissionfees += $cc->payout;
				}else if ($landlord){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
						'type' => 'Landlord'
					);
					$commissionfees += round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
				}else if ($tenant){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
						'type' => 'Tenant'
					);
					$commissionfees += $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
				}
			}
		}

		$leadingBonus = array();
		$leadingCommissionFees = 0;
		$leading = $userObj->getSalesLeaders();
		if (count($leading) > 0){
			//run through the invoice results again and do everything for "leading" ids
			$commconsquery = Commcons::whereIn('commissionid', $ids)->whereIn('consultantid', array_keys($leading))->where('commissiondate', '!=', '0000-00-00');
			if ($userObj->id == 4) {
				if($datefrom->format('Y-m-d') == "2020-04-26" && $dateto->format('Y-m-d') == "2020-05-25"){
					$commconsquery->where(function($query) use ($datefrom, $dateto){
						return $query->orWhere(function($query) use ($datefrom, $dateto){
							return $query->where('landlordpaiddate', '>=', $datefrom)
							->where('landlordpaiddate', '<=', $dateto);
						})->orWhere(function($query) use ($datefrom, $dateto){
							return $query->where('tenantpaiddate', '>=', $datefrom)
							->where('tenantpaiddate', '<=', $dateto);
						});
					});
				}else{
					$commconsquery->where('commissiondate', '>=', '2020-05-01');					
				}
			} else {
				$commconsquery->where('commissiondate', '>=', '2020-08-12');
			}
			$commcons = $commconsquery->get();

			if (count($commcons) > 0){
				foreach ($commcons as $cc){
					$landlord = false;
					$tenant = false;
					$invoice = null;

					if (isset($invll[$cc->commissionid])){
						$invoice = $invll[$cc->commissionid];
						$landlord = true;
					}
					if (isset($invtt[$cc->commissionid])){
						$invoice = $invtt[$cc->commissionid];
						$tenant = true;
					}

					$type = '';
					if ($landlord && $tenant){
						$type = 'Landlord & Tenant';
					}else if ($landlord){
						$type = 'Landlord';
					}else if ($tenant){
						$type = 'Tenant';
					}

					$leadingBonus[] = array(
						'invoice' => $invoice,
						'amount' => round($cc->payout*Commcons::LEADING_COMMISION, 2),
						'type' => $type
					);
					$leadingCommissionFees += round($cc->payout*Commcons::LEADING_COMMISION, 2);
				}
			}
		}

		$dateto = date_create($year.'-'.$month.'-25');
		$datefrom = date_create($year.'-'.$month.'-25');
		$datefrom = $datefrom->modify('-1 month')->modify('+1 day');
		$io_commission = array();

		$io_commission = IOCommission::where('io_commissions.status', '=', 'Approve')
			->where('io_commissions.user_id', '=', $userid)
			->whereBetween('io_commissions.updated_at', [$datefrom, $dateto])								
			->leftJoin('users', 'io_commissions.user_id', '=', 'users.id')
			->leftJoin('properties', 'io_commissions.property_id', '=', 'properties.id')
			->leftJoin('vendors', 'properties.owner_id', '=', 'vendors.id')
			->leftJoin('commission', 'io_commissions.invoiceId', '=', 'commission.id')
			->leftJoin('clients', 'commission.clientid', '=', 'clients.id')
			->select(
				'io_commissions.id as id',
				'io_commissions.status as status',
				'io_commissions.commission as commission',
				'io_commissions.salarydate as salarydate',
				'io_commissions.invoiceId as invoiceId',
				'users.firstname as firstname',
				'users.lastname as lastname',
				'properties.name as name',
				'vendors.company as company',
				'vendors.firstname as owner_firstname',
				'vendors.lastname as owner_lastname',
				'clients.firstname as client_firstname',
                'clients.lastname as client_lastname')
			->orderBy('io_commissions.id','desc')
			->get();
		
		$ioCom=0;
		if(count($io_commission) > 0){
			foreach($io_commission as $index=> $iocom){
				$ioCom = $iocom->commission + $ioCom;
			}
		}
		

        $html = view('commission.pdfcommissionlist', [
			'user' => $userObj,
			'salary' => $salary,
			'year' => $year,
			'month' => $month,
			'months' => $months,
			'invoiceoutput' => $invoiceoutput,
			'commissionfees' => $commissionfees,
			'leadingBonus' => $leadingBonus,
			'leadingCommissionFees' => $leadingCommissionFees,
			'io_commission' => $io_commission,			
			'ioComTotal' => $ioCom
		]);
        // return $html;

		$name = 'Commission Breakdown '.$userObj->firstname.' '.$userObj->lastname.' '.$months[$month].' '.$year.'.pdf';

		return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
	}



	//Expenses start

	public function expensesusers(Request $request)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		//load all users
		$usersObj = User::all();
		$users = array();

		foreach ($usersObj as $u){
			//if ($u->id != $user->id){
				$uarr = $u->toArray();
				$uarr['rightsstr'] = $u->getUserRightsString();

				$users[] = $uarr;
			//}
		}

		for ($i = 0; $i < count($users)-1; $i++){
			for ($a = $i+1; $a < count($users); $a++){
				if ($users[$i]['name'] > $users[$a]['name']){
					$help = $users[$i];
					$users[$i] = $users[$a];
					$users[$a] = $help;
				}
			}
		}

        return view('commission.expensesusers', [
			'users' => $users
		]);
    }


	public function expenses(Request $request, $id)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$expenses = array();

		$srs = Expenses::where('userid', $id)->get();
		if (count($srs) > 0){
			foreach ($srs as $s){
				if (!isset($expenses[$s->year])){
					$expenses[$s->year] = array();
				}
				$expenses[$s->year][$s->month] = $s;
			}
		}

		$curyear = intval(date('Y'));
		$curmonth = intval(date('m'));

		$months = Expenses::getMonthNames();


        return view('commission.expenses', [
			'user' => $userObj,
			'expenses' => $expenses,
			'curyear' => $curyear,
			'curmonth' => $curmonth,
			'months' => $months
		]);

    }


	public function createexpenses(Request $request, $userid, $year, $month)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$expenses = Expenses::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$expenses || empty($expenses)){
			$expenses = Expenses::create();

			$data = array();
			$data['userid'] = $userid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['paymode'] = 'Cheque';
			$data['chequenumber'] = '';

			$data['employeename'] = $userObj->firstname.' '.$userObj->lastname;
			$data['expensifyid'] = $userObj->expensifyid;
			$data['employeeaddress'] = $userObj->address;

			$data['amount'] = 0;

			$dd = date_create($year.'-'.$month.'-01');
			$data['payperiodfrom'] = $dd->format('Y-m-d');
			$dd = $dd->modify('+1 month')->modify('-1 day');
			$data['payperiodto'] = $dd->format('Y-m-d');

			$expenses->fill($data);
			$expenses->save();
		}

		return redirect('/commission/editexpenses/'.$userid.'/'.$year.'/'.$month);
    }

	public function editexpenses(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$expenses = Expenses::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$expenses || empty($expenses)){
			return redirect('/commission/expenses/'.$userid);
		}

		$months = Expenses::getMonthNames();


        return view('commission.expense', [
			'user' => $userObj,
			'expenses' => $expenses,
			'year' => $year,
			'month' => $month,
			'months' => $months
		]);
	}


	public function editexpensesstore(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$expenses = Expenses::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$expenses || empty($expenses)){
			return redirect('/commission/expenses/'.$userid);
		}

		$data = array(
			'paymode' => $input['paymode'],
			'chequenumber' => $input['chequenumber'],
			'amount' => $input['amount'],
			'employeename' => $input['employeename'],
			'employeeaddress' => $input['employeeaddress'],
			'payperiodfrom' => $input['payperiodfrom'],
			'payperiodto' => $input['payperiodto'],
			'expensifyid' => $input['expensifyid'],
			'paymentdate' => $input['paymentdate']
		);
		$expenses->fill($data);
		$expenses->save();

		return redirect('/commission/editexpenses/'.$userid.'/'.$year.'/'.$month);
	}



	public function pdfexpenses(Request $request, $userid, $year, $month){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !($user->id == $userid)){
			return redirect('dashboardnew');
		}

		$userObj = User::where('id', $userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$expenses = Expenses::where('userid', $userid)->where('year', $year)->where('month', $month)->first();
		if (!$expenses || empty($expenses)){
			return redirect('/commission/expenses/'.$userid);
		}

		$months = Expenses::getMonthNames();

        $html = view('commission.pdfexpenses', [
			'user' => $userObj,
			'expenses' => $expenses,
			'year' => $year,
			'month' => $month,
			'months' => $months
		]);
        //return $html;

		$name = 'Expenses '.$userObj->firstname.' '.$userObj->lastname.' '.$months[$month].' '.$year.'.pdf';

		return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
	}




	public function mysalaries(Request $request){
        $input = $request->all();
		$user = $request->user();

		$userid = $user->id;

		$salaries = array();

		$salaries = Salary::where('userid', $user->id)->whereNotNull('paymentdate')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();

		$months = Salary::getMonthNames();

		//Commissions
		$today = date_create();
		if (intval($today->format('d')) > 25){
			$today = $today->modify('+1 month');
		}
		$year = intval($today->format('Y'));
		$month = intval($today->format('m'));

		$dateto = date_create($year.'-'.$month.'-25');
		$datefrom = date_create($year.'-'.$month.'-25');
		$datefrom = $datefrom->modify('-1 month')->modify('+1 day');

		$invoicesll = Commission::where('landlordpaiddate', '>=', $datefrom)->where('landlordpaiddate', '<=', $dateto)->where('landlordamount', '>', 0)->with('property')->with('client')->get();
		$invoicestt = Commission::where('tenantpaiddate', '>=', $datefrom)->where('tenantpaiddate', '<=', $dateto)->where('tenantamount', '>', 0)->with('property')->with('client')->get();

		$ids = array();
		$invll = array();
		if (count($invoicesll) > 0){
			foreach ($invoicesll as $i){
				$ids[] = $i->id;
				$invll[$i->id] = $i;
			}
		}
		$invtt = array();
		if (count($invoicestt) > 0){
			foreach ($invoicestt as $i){
				$ids[] = $i->id;
				$invtt[$i->id] = $i;
			}
		}

		$commcons = Commcons::whereIn('commissionid', $ids)->where('consultantid', $userid)->get();

		$invoiceoutput = array();
		$commissionfees = 0;
		if (count($commcons) > 0){
			foreach ($commcons as $cc){
				$landlord = false;
				$tenant = false;
				$invoice = null;

				if (isset($invll[$cc->commissionid])){
					$invoice = $invll[$cc->commissionid];
					$landlord = true;
				}
				if (isset($invtt[$cc->commissionid])){
					$invoice = $invtt[$cc->commissionid];
					$tenant = true;
				}

				if ($landlord && $tenant){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => $cc->payout,
						'type' => 'Landlord & Tenant'
					);
					$commissionfees += $cc->payout;
				}else if ($landlord){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
						'type' => 'Landlord'
					);
					$commissionfees += round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
				}else if ($tenant){
					$invoiceoutput[] = array(
						'invoice' => $invoice,
						'amount' => $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2),
						'type' => 'Tenant'
					);
					$commissionfees += $cc->payout-round(($invoice->landlordamount/($invoice->landlordamount+$invoice->tenantamount))*$cc->payout, 2);
				}
			}
		}

        return view('commission.mysalaries', [
			'user' => $user,
			'salaries' => $salaries,
			'months' => $months,
			'invoiceoutput' => $invoiceoutput,
			'commissionfees' => $commissionfees,
			'year' => $year,
			'month' => $month
		]);

    }

	public function myexpenses(Request $request){
        $input = $request->all();
		$user = $request->user();

		$expenses = array();

		$expenses = Expenses::where('userid', $user->id)->whereNotNull('paymentdate')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();

		$months = Expenses::getMonthNames();

        return view('commission.myexpenses', [
			'user' => $user,
			'expenses' => $expenses,
			'months' => $months
		]);

    }


	public function salariescsv(Request $request)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$curyear = intval(date('Y'));
		$curmonth = intval(date('m'));

		$months = Salary::getMonthNames();

        return view('commission.salariescsv', [
			'curyear' => $curyear,
			'curmonth' => $curmonth,
			'months' => $months
		]);

    }


	public function csvsalarydownload(Request $request, $year, $month)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}


		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=nest-salary-".$month."-".$year.".csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		$salaries = Salary::where('month', $month)->where('year', $year)->get();
		$columns = array('EmployeeName', 'FixedSalary', 'Commission', 'OtherFees', 'OtherDeductions', 'NetIncome', 'EEMPF', 'ERMPF', 'Paymode', 'PaymentNumber', 'PaymentDate', 'OtherFeesText', 'OtherDeductionsText');

		$callback = function() use ($salaries, $columns)
		{
			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);

			foreach($salaries as $s) {
				fputcsv($file, array($s->employeename, $s->fixedfees, $s->commissionfees, $s->otherfees, $s->otherdeductions, $s->netincome, $s->mpfemployee, $s->mpfemployer, $s->paymode, $s->chequenumber, $s->paymentdate, $s->otherfeestext, $s->otherdeductionstext));
			}
			fclose($file);
		};

		return response()->stream($callback, 200, $headers);
    }



	public function invoicescsv(Request $request)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}

		$curyear = intval(date('Y'));
		$curmonth = intval(date('m'));

		$months = Salary::getMonthNames();

        return view('commission.invoicescsv', [
			'curyear' => $curyear,
			'curmonth' => $curmonth,
			'months' => $months
		]);

    }


	public function csvinvoicedownload(Request $request, $year, $month)
    {
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if ((!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']) || $rights['Admin']){
			return redirect('dashboardnew');
		}


		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=nest-invoices-".$month."-".$year.".csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		$invoices = Comminvoice::whereRaw('MONTH(invoicedate) = '.intval($month))->whereRaw('YEAR(invoicedate) = '.intval($year))->get();
		$columns = array('ContactName', 'InvoiceNumber', 'InvoiceDate', 'DueDate', 'Description', 'Quantity', 'UnitAmount', 'TrackingName1', 'TrackingOption1');

		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		$callback = function() use ($invoices, $columns, $consultants)
		{
			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);

			foreach($invoices as $i) {
				if ($i->whofor == 1){
					fputcsv($file, array($i->sellername, $i->id, $i->invoicedate, $i->duedate, $i->feecalculation, 1, ($i->price+$i->addcost), $i->consultantid, $consultants[$i->consultantid]->name));
				}else{
					fputcsv($file, array($i->buyername, $i->id, $i->invoicedate, $i->duedate, $i->feecalculation, 1, ($i->price+$i->addcost), $i->consultantid, $consultants[$i->consultantid]->name));
				}
			}
			fclose($file);
		};

		return response()->stream($callback, 200, $headers);
    }




	public function bonusdividends(Request $request){
        $input = $request->all();
		$cu = $request->user();

		if (!($cu->getUserRights()['Superadmin'] || $cu->getUserRights()['Accountant'] || $cu->getUserRights()['Director'])){
			return redirect('dashboardnew');
			exit;
		}

		$records = Bonusdividend::orderBy('id', 'desc')->paginate(20);

		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		return view('commission.bonusdividends', [
			'input' => $request,
			'consultants' => $consultants,
			'records' => $records
		]);
	}

	public function createbonusdividend(Request $request){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$record = array(
			'id' => 0,
			'userid' => 0,
			'type' => 0,
			'paymode' => '',
			'chequenumber' => '',
			'otherfees' => 0,
			'otherdeductions' => 0,
			'netincome' => 0,
			'employeename' => '',
			'employeeaddress' => '',
			'otherfeestext' => '',
			'otherdeductionstext' => '',
			'hkid' => '',
			'paymentdate' => date('Y-m-d'),
			'comments' => ''
		);
		$record = (object) $record;

		$users = User::where('status', User::ACTIVE)->orderBy('name')->get();
		$consultants = array();
		$directors = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		$users = User::whereIn('id', array(User::NAOMI_ID,User::WILLIAM_ID))->get();
		foreach ($users as $uu){
			$directors[$uu->id] = $uu;
		}

        return view('commission.bonusdividend', [
			'user' => $user,
			'record' => $record,
			'consultants' => $consultants,
			'directors' => $directors
		]);
    }

	public function createbonusdividendstore(Request $request){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		if (isset($input['type'])){
			$data = array();

			if (isset($input['userid_dividend']) || isset($input['userid_bonus'])){
				$data['userid'] = $input['type'] ? $input['userid_dividend'] : $input['userid_bonus'];
			}
			if (isset($input['type'])){
				$data['type'] = $input['type'];
			}
			if (isset($input['paymode'])){
				$data['paymode'] = $input['paymode'];
			}
			if (isset($input['chequenumber'])){
				$data['chequenumber'] = $input['chequenumber'];
			}
			if (isset($input['otherfees'])){
				$data['otherfees'] = $input['otherfees'];
			}
			if (isset($input['otherdeductions'])){
				$data['otherdeductions'] = $input['otherdeductions'];
			}
			$data['netincome'] = $input['otherfees'] - $input['otherdeductions'];
			if (isset($input['employeename'])){
				$data['employeename'] = $input['employeename'];
			}
			if (isset($input['employeeaddress'])){
				$data['employeeaddress'] = $input['employeeaddress'];
			}
			if (isset($input['otherfeestext'])){
				$data['otherfeestext'] = $input['otherfeestext'];
			}
			if (isset($input['otherdeductionstext'])){
				$data['otherdeductionstext'] = $input['otherdeductionstext'];
			}
			if (isset($input['paymentdate'])){
				$data['paymentdate'] = $input['paymentdate'];
			}

			if (!empty($input['comments'])) {
				$data['comments'] = $input['comments'];
			}

			$bd = Bonusdividend::create($data);
			$bd->save();
		}

		return redirect('/commission/bonusdividends');
	}


	public function editbonusdividend(Request $request, $id){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$record = Bonusdividend::where('id', $id)->first();
		if (!($record)){
			return redirect('dashboardnew');
			exit;
		}

		$users = User::where('status', User::ACTIVE)
			->orWhere('id', $record->userid)
			->orderBy('name')
			->get();
		$consultants = array();
		$directors = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		$users = User::whereIn('id', array(User::NAOMI_ID,User::WILLIAM_ID))->get();
		foreach ($users as $uu){
			$directors[$uu->id] = $uu;
		}

        return view('commission.bonusdividend', [
			'user' => $user,
			'record' => $record,
			'consultants' => $consultants,
			'directors' => $directors
		]);
    }

	public function editbonusdividendstore(Request $request, $id){
        $input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
			exit;
		}

		$record = Bonusdividend::where('id', $id)->first();

		if (isset($input['type'])){
			$data = array();

			if (isset($input['userid_dividend']) || isset($input['userid_bonus'])){
				$data['userid'] = $input['type'] ? $input['userid_dividend'] : $input['userid_bonus'];
			}
			if (isset($input['type'])){
				$data['type'] = $input['type'];
			}
			if (isset($input['paymode'])){
				$data['paymode'] = $input['paymode'];
			}
			if (isset($input['chequenumber'])){
				$data['chequenumber'] = $input['chequenumber'];
			}
			if (isset($input['otherfees'])){
				$data['otherfees'] = $input['otherfees'];
			}
			if (isset($input['otherdeductions'])){
				$data['otherdeductions'] = $input['otherdeductions'];
			}
			$data['netincome'] = $input['otherfees'] - $input['otherdeductions'];
			if (isset($input['employeename'])){
				$data['employeename'] = $input['employeename'];
			}
			if (isset($input['employeeaddress'])){
				$data['employeeaddress'] = $input['employeeaddress'];
			}
			if (isset($input['otherfeestext'])){
				$data['otherfeestext'] = $input['otherfeestext'];
			}
			if (isset($input['otherdeductionstext'])){
				$data['otherdeductionstext'] = $input['otherdeductionstext'];
			}
			if (isset($input['paymentdate'])){
				$data['paymentdate'] = $input['paymentdate'];
			}
			if (!empty($input['comments'])) {
				$data['comments'] = $input['comments'];
			}

			$record->fill($data);
			$record->save();
		}

		return redirect('/commission/editbonusdividend/'.$id);
	}

	public function downloadbonusdividend(Request $request, $id){
		$input = $request->all();
		$user = $request->user();

		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboardnew');
		}

		$record = Bonusdividend::where('id', $id)->first();
		if (!$record || empty($record)){
			return redirect('/commission/bonusdividends');
		}

		$userObj = User::where('id', $record->userid)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboardnew');
		}

		$months = Salary::getMonthNames();

        $html = view('commission.bonusdividendpdf', [
			'user' => $userObj,
			'record' => $record,
			'userObj' => $userObj
		]);
        //return $html;

		if ($record->type == 0){
			$name = 'Bonus '.$userObj->firstname.' '.$userObj->lastname.' #'.$record->id.'.pdf';
		}else{
			$name = 'Dividend #'.$record->id.'.pdf';
		}

		return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
	}


	public function leased_properties(Request $request){
		$sql = Comminvoice::groupby('commissionid')
				->whereNotNull('expiration_date')
				->where('comminvoice.status','=',1)
				->leftjoin('commission','comminvoice.commissionid', '=', 'commission.id')
				->leftjoin('properties','commission.propertyid', '=', 'properties.id')
				->orderby('comminvoice.expiration_date','asc');
			
		$now = Carbon::now()->format('Y-m-d');

		if( $request->get('filter') !== null && $request->get('filter') !=='' ){
			$filter = $request->get('filter');
			
			
			if($filter=='0'){
				$adddate = Carbon::now()->addMonths(6)->format('Y-m-d');			
				$sql->whereBetween('expiration_date',[$now,$adddate]);
			} else if($filter==1){
				$adddate = Carbon::now()->addMonths(3)->format('Y-m-d');
				$sql->whereBetween('expiration_date',[$now,$adddate]);
			} else if($filter==2){
				$sql->where('expiration_date','<=',$now);
			} 
		} else if($request->get('available_date_from') !== null) {

			$available_date_from = $request->get('available_date_from');
			$available_date_to = $request->get('available_date_to');

			
			$available_date_from_ = Carbon::parse($available_date_from)->format('Y-m-d');
			$available_date_to_ = Carbon::parse($available_date_to)->format('Y-m-d');
			$sql->whereBetween('expiration_date',[$available_date_from_,$available_date_to_]);

		} else {
			$sql->where('expiration_date','>=',$now);
		}

		$list = $sql->get();

		/*foreach($list as $l){
			echo $l->commissionid .' - '.$l->name.' - '. $l->expiration_date.'<br>';
		}*/


		return view('commission.leasedproperties', [
			'list' => $list,
			'filter' => isset($filter) ? $filter : null,
			'from' => $request->get('available_date_from') !== null ?  $request->get('available_date_from') : null,
			'to' => $request->get('available_date_to') !== null ?  $request->get('available_date_to') : null,
		]);


	}





}
