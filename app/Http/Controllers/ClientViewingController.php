<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientMeta;
use App\ClientShortlist;
use App\ClientOption;
use App\ClientViewing;
use App\ClientViewingOption;
use App\Property;
use App\Building;
use App\User;
use App\PropertyPhoto;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\ClientRepository;

use View;
use App\Lead;
use App\Calendar;
use App\LogClick;



class ClientViewingController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClientRepository $clients, Redirector $redirect){
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(12);
			
			if ($user->isOnlyDriver() || $user->isOnlyPhotographer()){
				$redirect->to('/calendar')->send();
			}
			
			$this->clients = $clients;
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($shortlist){
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlist);
        $options = $shortlist->options()->lists('property_id')->toArray();

        $properties = Property::ShortlistSearch($shortlist->id)
                        ->with('owner')->with('agent')
                        ->with('featured_photo')
                        ->with('indexed_photo')
                        ->whereIn('properties.id', $options)
                        ->orderBy('properties.id','desc')->get();
		
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }
        
        $viewings = ClientViewing::where('shortlist_id', '=', $shortlist->id)->get();
		$viewingproperties = ClientViewingOption::where('shortlist_id', '=', $shortlist->id)->orderBy('viewing_id', 'asc')->get();
		$vps = array();
		foreach ($viewingproperties as $vp){
			if (isset($vps[$vp->viewing_id])){
				$vps[$vp->viewing_id][$vp->property_id] = $vp;
			}else{
				$vps[$vp->viewing_id] = array();
				$vps[$vp->viewing_id][$vp->property_id] = $vp;
			}
		}
       
		
        return view('viewings.create')
            ->with( 'properties',  $properties)
			->with('shortlist', $shortlist)
            ->with('vps', $vps)
			->with('times', Calendar::getTimesArray());
    }
	
	/**
     * Create a new property.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request){
		$data = $request->all();
      //  app('App\Http\Controllers\ClientShortlistController')->addtoview($data['shortlist_id'],$request);
        //$t->addtoview($data['shortlist_id'],$data['options_ids']);
		//dd($data['options_ids']);
		if (isset($data['shortlist_id'])){
			$cv = new ClientViewing;
			$cv->fill($data);
			$cv->save();

            if(isset($data['options_ids']))
                $this->addPropertytoView($data['shortlist_id'], 0, $request, $cv->id);
			
			return redirect('/shortlist/show/'.$data['shortlist_id']);
		}
		return redirect('/shortlists');
    }

    public function addPropertytoView($shortlist_id, $vid, Request $request, $viewing_id){
		$input = $request->all();

		$shortlist = ClientShortlist::findOrFail($shortlist_id);

		if ($vid > 0){
			$openview = ClientViewing::where('id', $vid)->first();
		}else{
			$viewings = ClientViewing::where('shortlist_id', '=', $shortlist_id)->get();
			$openview = null;
			if (count($viewings) > 0){
				foreach ($viewings as $v){
					if ($v->status == 0){
						$openview = $v;
					}
				}
			}
		}
		if ($openview != null){
			$existing = ClientViewingOption::where('viewing_id', '=', $openview->id)->withTrashed()->get();
			$ex = array();
			$trashed = array();
			if (count($existing) > 0){
				foreach ($existing as $e){
					$ex[intval($e->property_id)] = 1;
					if ($e->trashed()){
						$trashed[intval($e->property_id)] = $e;
					}
				}
			}

			if (is_array($input['options_ids']) && count($input['options_ids']) > 0){
				foreach ($input['options_ids'] as $option_id) {
					if (!isset($ex[intval($option_id)])){
						$tid = 1;
						if ($shortlist->typeid == 2){
							$tid = 2;
						}
						$data = array(
							'client_id' => $openview->client_id,
							'shortlist_id' => $openview->shortlist_id,
							'viewing_id' => $viewing_id,
							'property_id' => $option_id,
							'order' => 0,
							'type_id' => $tid
						);

                  

						$voption = new ClientViewingOption;
						$voption->fill($data);
						$voption->save();
					}else if (isset($trashed[intval($option_id)])){
						$trashed[intval($option_id)]->restore();
					}
				}
			}
			return redirect('/viewing/show/'.$openview->id);
		}

		return redirect('/shortlist/show/'.$shortlist_id);
	}
	
    public function update($viewing, Request $request){
		$data = $request->all();
		$cv = ClientViewing::findOrFail($viewing);
		
		if ($data['status'] == 10){
			if (isset($data['tempature'])){
				$client = Client::findOrFail($cv->client_id);
				$client->fill(array('tempature' => $data['tempature']));
				$client->save();
			}
		}else{
			unset($data['commentsafter']);
		}
		if (isset($data['tempature'])){
			unset($data['tempature']);
		}
		
		$cv->fill($data);
		$cv->save();
		
		return redirect('/shortlist/show/'.$cv->shortlist_id);
    }
	
    public function remove($viewing, Request $request){
		$cv = ClientViewing::findOrFail($viewing);
		$cv->delete();
		
		return redirect('/shortlist/show/'.$cv->shortlist_id);
    }
	
    public function show($viewing, Request $request) {
        $viewing = ClientViewing::findOrFail($viewing);
		$shortlist = ClientShortlist::with('client')->findOrFail($viewing->shortlist_id);
		
		$ex = ClientViewingOption::where('viewing_id', '=', $viewing->id)->select('property_id')->orderBy('order', 'asc')->get()->toArray();
		$existing = array();
		foreach ($ex as $e){
			$existing[] = $e['property_id'];
		}
		$ids_ordered = implode(',', $existing);
		
		$properties = Property::whereIn('properties.id', $existing)
			->with('owner')
			->with('agent')
			->with('featured_photo')
			->with('indexed_photo')
			->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
			->paginate(36);
		
		$properties_post = '';
		foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
			$properties_post .= $property->shorten_building_name().PHP_EOL.PHP_EOL;
        }
		
		$commentaftercontent = $viewing->commentsafter;
		if (trim($commentaftercontent) == ''){
			$commentaftercontent = $properties_post;
		}
		
		$oldfrom = $viewing->getNiceTimeFrom();
		
        return view('viewings.show', [
            'properties' => $properties,
			'shortlist' => $shortlist,
			'input' => $request,
			'viewing' => $viewing,
			'commentaftercontent' => $commentaftercontent,
			'times' => Calendar::getTimesArray(),
			'oldfrom' => $oldfrom
        ]);
    }
	
	function option_remove($viewing, Request $request){
        $input = $request->all();
		
        if (empty($input['options_ids'])) {
            return redirect('/viewing/show/' . $viewing);
        }
        foreach ($input['options_ids'] as $option_id) {
            $cvo = ClientViewingOption::where('property_id', $option_id)->where('viewing_id', $viewing)->orderby('order', 'asc')->first();
            if (!empty($cvo)) {
                $cvo->delete();
            }
        }
        return redirect('/viewing/show/' . $viewing);
	}


    /**
     * API
     */
    public function api_toggle_type($viewing, Request $request)
    {
        $input = $request->all();
        if (!empty($input['property_id'])) {
            $option = ClientViewingOption::where('viewing_id', $viewing)->where('property_id', $input['property_id'])->orderby('order', 'asc')->first();
			if (empty($option)) {
				return redirect('/viewing/show/'.$viewing);
			}
			$option->type_id = $option->type_id==1?2:1;
			$option->save();
        }
        return redirect('/viewing/show/'.$viewing);
    }


    public function option_print($viewing, Request $request) {
        $input = $request->all();
		
        if (empty($input['options_ids'])) {
            return redirect('/viewing/show/'.$viewing);
        }
        $v = ClientViewing::with('client')->findOrFail($viewing);
        $options = ClientViewingOption::where('viewing_id', $viewing)->select('property_id')->orderby('order', 'asc')->get()->toArray();
        $res = array();
		
        foreach ($options as $key => $value) {
            if (in_array($value['property_id'], $input['options_ids'])) {
                $res[] = $value['property_id'];
            }
        }
        if (empty($res)) {
            return redirect('/viewing/show/'.$viewing);
        }
		$ids_ordered = implode(',', $res);

        $properties = Property::IndexSearch($request)
                        ->with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $res)
						->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
						->paginate(20);
        $html = view('viewings.printpdf', [
            'properties' => $properties,
            'viewing' => $v
        ]);
		
		$name = '';
		if (trim($v['client']->firstname) != ''){
			$name = trim($v['client']->firstname).' ';
		}else{
			$name = trim($v['client']->lastname).' ';
		}
		$name .= 'Property Listings - '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a5', 'landscape')->setWarnings(false)->stream($name);
    }

    public function option_print_agent($viewing, Request $request) {
        $input = $request->all();
		
        if (empty($input['options_ids'])) {
            return redirect('/viewing/show/'.$viewing);
        }
        $v = ClientViewing::with('client')->findOrFail($viewing);
		$shortlist = ClientShortlist::with('client')->findOrFail($v->shortlist_id);
        $options = ClientViewingOption::where('viewing_id', $viewing)->select('property_id')->orderby('order', 'asc')->get()->toArray();
        $res = array();
		
        foreach ($options as $key => $value) {
            if (in_array($value['property_id'], $input['options_ids'])) {
                $res[] = $value['property_id'];
            }
        }
        if (empty($res)) {
            return redirect('/viewing/show/'.$viewing);
        }
		$ids_ordered = implode(',', $res);

        $properties = Property::IndexSearch($request)
                        ->with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $res)
						->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
						->paginate(20);
        $html = view('viewings.printpdfagent', [
            'properties' => $properties,
            'viewing' => $v,
			'shortlist' => $shortlist
        ]);
		
		$name = '';
		if (trim($v['client']->firstname) != ''){
			$name = trim($v['client']->firstname).' ';
		}else{
			$name = trim($v['client']->lastname).' ';
		}
		$name .= 'Property Listings Agent - '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
    }

    public function option_reorder(Request $request){
        $input = $request->all();
        if (empty($input['viewing_id'])) {
            return redirect('/shortlists/');
        }
        $viewing_id = $input['viewing_id'];
        if (empty($input['options_ids'])) {
            return redirect('/viewing/show/'.$viewing_id);
        }
        // dex($input['options_ids']);
        foreach ($input['options_ids'] as $key => $option_id) {
            $cvo = ClientViewingOption::where('property_id', $option_id)->where('viewing_id', $viewing_id)->orderby('order', 'asc')->first();
            if (!empty($cvo)) {
                $cvo->fill(array('order'=>$key));
                $cvo->save();
            }
            unset($cvo);
        }
        return redirect('/viewing/show/'.$viewing_id);
    }
	
    public function show_order($viewingid, Request $request){
        $viewing = ClientViewing::with('client')->findOrFail($viewingid);
		$ex = ClientViewingOption::where('viewing_id', $viewingid)->select('property_id')->orderby('order', 'asc')->get()->toArray();
		$options = array();
		foreach ($ex as $e){
			$options[] = $e['property_id'];
		}
		$ids_ordered = implode(',', $options);
        $properties = Property::whereIn('properties.id', $options)
                        ->with('owner')->with('agent')->with('allreps')
                        ->with('featured_photo')
                        ->with('indexed_photo')
						->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
						->paginate(40);

        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
        }
        
        return view('viewings.show-order', [
            'properties' => $properties,
        ])->with('viewing', $viewing)->with('input', $request);

    }
	
	
}
