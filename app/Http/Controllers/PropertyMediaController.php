<?php

namespace App\Http\Controllers;

use App\Property;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;

use App\PropertyMeta;
use App\PropertyMedia;
use App\PropertyPhoto;
use Log;
use Plupload;

class PropertyMediaController extends Controller
{
    /**
     * API
     */
    public function api_index($property_id, $group, $type, Request $request){
        $files = array();
        $field = $type=='website'?'web_order':'order';
        if (!empty($property_id) && !empty($group)) {
            $files = PropertyMedia::where('property_id', $property_id)->where('group', $group)->orderby($field, 'asc')->get();
        }
        if (empty($files)) {
            return response()->json();
        }
		$arr = $files->toArray();
		
		for ($i = 0; $i < count($arr); $i++){
			if (strstr($arr[$i]['url'], '192.168.1.115:8082')){
				$arr[$i]['url'] = str_replace('http://192.168.1.115:8082', 'https://db.nest-property.com/cms', $arr[$i]['url']);
			}
		}
		
        return response()->json($arr);
    }
	
    public function propertyimages($property_id, $group, $type){
		if ($type == 'website'){
			$files = PropertyMedia::where('property_id', $property_id)->where('group', $group)->orderby('web_order', 'asc')->get();
		}else{
			$files = PropertyMedia::where('property_id', $property_id)->where('group', $group)->orderby('order', 'asc')->get();
		}
				
		if (empty($files)){
			if (empty($files)) {
				return response()->json();
			}
		}
		
		$arr = array();
		foreach ($files as $file){
			$x = array();
			$x['url'] = $file->getShowUrlModal();
			$arr[] = $x;
		}

        return response()->json($arr);
    }

    public function api_connect_hongkonghomesphotos(Request $request) {
        $data = Input::all();
        $id = $data['id'];
        $url = $data['url'];
        $PropertyPhoto = new PropertyPhoto;
        $photos = $PropertyPhoto->insert_property_hongkonghome_photos($id, $url);
        return response()->json($photos);
    }
    public function api_connect_newhongkonghomesphotos(Request $request) {
        $data = Input::all();
        $id = $data['id'];
        $url = $data['url'];
        $PropertyPhoto = new PropertyPhoto;
        $photos = $PropertyPhoto->insert_property_newhongkonghome_photos($id, $url);
        return response()->json($photos);
    }

    public function hongkonghomesphotostest(Request $request) {
        $data = Input::all();
        $id = 0;
        $url = 'https://proway.com.hk/en/property/rent/repulse_bay/repulse_bay_road_84/3885?trigger=new&recommend_from_agent=Monita.Wong';
        $PropertyPhoto = new PropertyPhoto;
        $photos = $PropertyPhoto->insert_property_hongkonghome_photos($id, $url);
        echo response()->json($photos);
    }
	
    public function newhongkonghomesphotostest(Request $request) {
        $data = Input::all();
        $id = 0;
        $url = 'https://www.hongkonghomes.com/en/hong-kong-property/for-rent/mid-levels-west/the-summa-tower-1/129945?agent-recommendation=carmen-wong';
        $PropertyPhoto = new PropertyPhoto;
        $photos = $PropertyPhoto->insert_property_newhongkonghome_photos($id, $url);
        echo response()->json($photos);
    }
	
	

    /**
     * Upload media in storage
     */
    public function upload() {
        $accepted_groups = array('nest-photo', 'nest-doc', 'other-photo');
        if (!isset($_REQUEST['id'])) {
            return '';
        }
        if (!isset($_REQUEST['group']) || !in_array($_REQUEST['group'], $accepted_groups)) {
            return '';
        }
        
        $id = $_REQUEST['id'];
        $directory = $_REQUEST['group'];

        return Plupload::receive('file', function ($file) use ($id, $directory)
        {
            $storage_path = public_path();
            $file_path = 'images' .'/'. $directory .'/';
            $media = new PropertyMedia;
            $filename = sprintf("p%d-%s", $id, $file->getClientOriginalName());
            $media->add_media(array(
                    'property_id' => $id,
                    'group'       => $directory,
                    'url'         => url($file_path . $filename),
                    'filename'    => $filename,
                ));

            $file->move($storage_path .'/'. $file_path, $filename);

            $PropertyPhoto = new PropertyPhoto;
            $PropertyPhoto->set_featured_photo($id);
            
            return 'ready';
        });
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit_reorder($property_id, $group, $type, Request $request)
    {
        $property = Property::findOrFail($property_id);
        $field = $type=='website'?'web_order':'order';
        $files = PropertyMedia::where('property_id', $property_id)->where('group', $group)->orderby($field, 'asc')->get();		
        return view('properties.media.edit-reorder')
                ->with('files', $files)
                ->with('property', $property)
                ->with('type', $type)
                ->with('field', $field)
                ->with('group', $group);
    }

    public function edit_remove($property_id, $group, $type, Request $request)
    {
        $property = Property::findOrFail($property_id);
        $field = $type=='website'?'web_order':'order';
        $files = PropertyMedia::where('property_id', $property_id)->where('group', $group)->orderby($field, 'asc')->get();
        $trashed_files = PropertyMedia::onlyTrashed()->where('property_id', $property_id)->where('group', $group)->orderby($field, 'asc')->get();

        return view('properties.media.edit-remove')
                ->with('files', $files)
                ->with('trashed_files', $trashed_files)
                ->with('property', $property)
                ->with('type', $type)
                ->with('field', $field)
                ->with('group', $group);
    }

    /**
     * Re-order photos
     */
    public function reorder(Request $request)
    {
        $data = Input::all();
        if (!isset($data['field']) || !in_array($data['field'], ['order', 'web_order'])) {
            $data['field'] = 'order';
        }
        $group = !empty($data['group'])?$data['group']:'nest-photo';
        $type = $data['field'] != 'web_order'?'database':'website';
        if (!empty($data['orders']) && !empty($data['property_id'])) {
            foreach ($data['orders'] as $key => $value) {
                $file = PropertyMedia::findOrFail($value);
                if ($file) {
                    $file->fill(array($data['field']=>($key+1)));
                    $file->save();
                } 
            }
            $PropertyPhoto = new PropertyPhoto;
            $PropertyPhoto->set_featured_photo($data['property_id']);
        }
        if (!empty($data['property_id'])) {
            return redirect('/property/media/edit-reorder/' . $data['property_id'] . '/' . $group . '/' . $type);
        } else {
            return redirect('/list');
        }
    }

    public function remove(Request $request)
    {
        $data = Input::all();
        $group = !empty($data['group'])?$data['group']:'nest-photo';
        $type = $data['field'] != 'web_order'?'database':'website';
        if (!empty($data['property_id'])) {
			if (!empty($data['options_ids'])){
				foreach ($data['options_ids'] as $key => $value) {
					$file = PropertyMedia::findOrFail($value);
					if ($file) {
						$file->delete();
					}
				}
			}
			
			$PropertyPhoto = new PropertyPhoto;
			$PropertyPhoto->set_featured_photo($data['property_id']);
        }
        if (!empty($data['property_id'])) {
            return redirect('/property/media/edit-remove/' . $data['property_id'] . '/' . $group . '/' . $type);
        } else {
            return redirect('/list');
        }
    }

    public function restore(Request $request)
    {
        $data = Input::all();
        $group = !empty($data['group'])?$data['group']:'nest-photo';
        $type = $data['field'] != 'web_order'?'database':'website';
        if (!empty($data['options_ids']) && !empty($data['property_id'])) {
            foreach ($data['options_ids'] as $key => $value) {
                $file = PropertyMedia::onlyTrashed()->findOrFail($value);
                if ($file) {
                    $file->restore();
                }
            }
			
			$PropertyPhoto = new PropertyPhoto;
			$PropertyPhoto->set_featured_photo($data['property_id']);
        }
        if (!empty($data['property_id'])) {
            return redirect('/property/media/edit-remove/' . $data['property_id'] . '/' . $group . '/' . $type);
        } else {
            return redirect('/list');
        }
    }

    public function permanent_remove(Request $request)
    {
        $data = Input::all();
        $group = !empty($data['group'])?$data['group']:'nest-photo';
        $type = $data['field'] != 'web_order'?'database':'website';
        if (!empty($data['options_ids']) && !empty($data['property_id'])) {
            foreach ($data['options_ids'] as $key => $value) {
                $file = PropertyMedia::onlyTrashed()->findOrFail($value);
                if ($file) {
                    $file->property_id = 0; // unlink to this property. - todo remove all unlink photos
                    $file->save();
                }
            }
			
			$PropertyPhoto = new PropertyPhoto;
			$PropertyPhoto->set_featured_photo($data['property_id']);
        }
        if (!empty($data['property_id'])) {
            return redirect('/property/media/edit-remove/' . $data['property_id'] . '/' . $group . '/' . $type);
        } else {
            return redirect('/list');
        }
    }

}
