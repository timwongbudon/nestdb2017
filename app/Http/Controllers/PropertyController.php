<?php

namespace App\Http\Controllers;

use App\Property;
use App\Building;
use App\Vendor;
use App\Client;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;
use App\Repositories\PropertyRepository;

use App\PropertyMeta;
use App\PropertyMedia;
use App\PropertyPhoto;
use App\PropertymigrationMeta;
use App\PropertyFeature;
use App\PropertyImageVariation;
use App\Propertydocuments;
use App\User;
use App\LogProperties;
use App\Csvimportproperty;
use App\Commission;
use App\Photoshoot;
use App\PhotoshootOption;
use App\District;
use Log;
use Plupload;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\IOCommission;
use App\PropertyDeleteLogs;

use View;
use App\Lead;
use ZipArchive;
use App\LogClick;
use Psy\Command\WhereamiCommand;

class PropertyController extends Controller
{
	/**
     * The property repository instance.
     *
     * @var Property Repository
     */
    protected $properties;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PropertyRepository $properties, Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(6);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
			
			$this->properties = $properties;
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
	 * Display a list of all of the user's property.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request){
        $input = $request->all();
        $district_ids = Building::district_ids();
        $custom_listing_options = Property::custom_listing_options();
        // $_query = Property::IndexSearch($request)
        //                 ->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id!="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority, IF(available_date!="0000-00-00", 1, 0) as date_priority2'))
        //                 ->IndexSearchAdvancedSort($request)
        //                 ->with('owner')->with('agent')//->with('allreps')
        //                 ->with('building')
        //                 ->with('indexed_photo');
        // die($_query->toSql());
        // $_properties = $_query->paginate(10);
        // dex($_properties->toArray());

        $properties = Property::IndexSearch($request)
                        ->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id!="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority, IF(available_date!="0000-00-00", 1, 0) as date_priority2'))
                        ->with('owner')->with('agent')//->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->PropertyBackendSort($request)->paginate(10)->appends($input);
        // dex($properties[0]['indexed_photo']);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

        return view('properties.index', [
            'properties' => $properties,
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids())
                        ->with('current_url', \NestRedirect::current_url())
                        ->with('input', $request);
    }
	
	
	public function dashboard(Request $request){
		$input = $request->all();

        $hotprops = Property::DashboardHot()
			->select(DB::raw('properties.*'))
			->with('owner')
			->orderBy('available_date', 'desc')
			->paginate(10)->appends($input);
        
        $availableprops = Property::AvailableProps()
			->select(DB::raw('properties.*'))
			->with('owner')
			->orderBy('available_date', 'desc')
			->paginate(10)->appends($input);
        
        $leasedprops = Property::LeaseComingUp()
			->select(DB::raw('properties.*'))
			->with('owner')
			->LeaseSort($request)
			->paginate(10)->appends($input);
        
		
		return view('dashboard', [
			'hotprops' => $hotprops,
			'availableprops' => $availableprops,
			'leasedprops' => $leasedprops
		]);
	}
	
	public function upcleases(Request $request){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
						->whereraw('properties.available_date > "'.date('Y-m-d').'" and properties.leased = 1')
                        ->with('owner')
						->with('agent')
						->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.available_date','asc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.propertylist', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request)
						->with('showtype', 'upcleases');
		
	}
	
	public function hotprops(Request $request){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
						->whereraw('properties.hot = 1')
                        ->with('owner')
						->with('agent')
						->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.available_date','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.propertylist', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request);
		
		
	}
	
	public function newprops(Request $request){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
						->whereraw('properties.available_date < DATE_ADD(NOW(), INTERVAL 1 DAY)')
						->whereraw('properties.available_date > DATE_ADD(NOW(), INTERVAL -4 WEEK)')
                        ->with('owner')
						->with('agent')
						->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.available_date','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.latestproperties', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request);
		
		
	}
	
	public function newcreatedprops(Request $request){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
			->with('owner')
			->with('agent')
			->with('allreps')
			->with('building')
			->with('indexed_photo')
			->orderBy('properties.created_at','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }
		
	    return view('properties.propertylist', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
		->with('hot_option_ids', Property::$hot_option_ids)
		->with('photo_option_ids', Property::$photo_option_ids)
		->with('web_option_ids', Property::$web_option_ids)
		->with('custom_listing_options', $custom_listing_options)
		->with('outdoor_ids', Property::outdoor_ids())
		->with('feature_ids', Building::feature_ids_search())
		->with('input', $request);
	}
	
	public function clpropertiesowner(Request $request, $ownerid, $buildingid){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
						//->where('building_id', '=', $buildingid)
						->where('owner_id', '=', $ownerid)
                        ->with('owner')
						->with('agent')
						->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.leased','asc')
                        ->orderBy('properties.sold','asc')
                        ->orderBy('properties.available_date','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.clpropertiesowner', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request);
		
		
	}
	
	public function clpropertiesagency(Request $request, $agencyid, $buildingid){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
						//->where('building_id', '=', $buildingid)
						->where('agent_id', '=', $agencyid)
                        ->with('owner')
						->with('agent')
						->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.leased','asc')
                        ->orderBy('properties.sold','asc')
                        ->orderBy('properties.available_date','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.clpropertiesagency', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request);
		
		
	}
	
	public function bonuscomm(Request $request){
        $input = $request->all();
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
						->whereraw('properties.bonuscomm = 1')
                        ->with('owner')
						->with('agent')
						->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.available_date','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.propertylist', [
            'properties' => $properties,
			'showadvsearch' => false,
			'hidesearch' => true,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request);
	}
	

    public function incomplete_index(Request $request) {
        $input = $request->all();
        $district_ids = Building::district_ids();
        $custom_listing_options = Property::custom_listing_options();
        $properties = Property::IncompleteSearch($request)
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*'))
                        ->with('owner')->with('agent')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.id','desc')->paginate(10)->appends($input);

        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

        return view('properties.incomplete-index', [
            'properties' => $properties,
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids())
                        ->with('input', $request);
    }

    public function trash_index(Request $request) {
        $input = $request->all();
        $district_ids = Building::district_ids();
        $custom_listing_options = Property::custom_listing_options(); 
        $properties = Property::onlyTrashed()
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*'))
                        ->with('owner')->with('agent')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.id','desc')->paginate(10)->appends($input);

        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

        return view('properties.trash-index', [
            'properties' => $properties,
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids())
                        ->with('input', $request);
    }

    public function frontend_index(Request $request)
    {
        $input = $request->all();
        $district_ids = Building::district_ids();
        $custom_listing_options = Property::custom_listing_options();
        $properties_query = Property::FrontendSearch($request)
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*'))
                        ->with('owner')->with('agent')->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.id','desc');
        $properties = $properties_query->paginate(10)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.frontend-index', [
            'properties' => $properties,
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids())
                        ->with('input', $request);
    }

    public function addphotoshoot(Request $request)
    {
        $input = $request->all();
		
		if (is_array($input['options_ids']) && count($input['options_ids']) > 0){
			foreach ($input['options_ids'] as $property_id) {
				$property = Property::find($property_id);
				
				if (!empty($property)){
					$property->photoshoot = 1;
					$property->save();
				}
			}
		}
		
		$prevurl = \NestRedirect::previous_url();
		
        if (!empty($prevurl)){
            return redirect($prevurl);
        } else {
            return redirect('/list');
        }
    }
	
    public function frontend_select(Request $request)
    {
        $input = $request->all();
        $is_select = (!empty($input['action']) && $input['action']=='select');
		if (isset($input['options_ids'])){
			$this->_frontend_select($input['options_ids'], $is_select);
		}
		
		$prevurl = \NestRedirect::previous_url();
		
        if (!empty($prevurl)){
            return redirect($prevurl);
        } else {
            return redirect('/list');
        }
    }

    private function _frontend_select($property_ids, $is_select = true) 
    {
        foreach ($property_ids as $property_id) {
            $property = Property::find($property_id);
            if (!empty($property)) {
                $property->web = $is_select?1:0;
				$property->tst = time();
				$property->webtst = time();
                $property->save();
            }
        }
    }

    public function option_print(Request $request) {
        $input = $request->all();
        if (empty($input['options_ids'])) {
            return redirect('/');
        }
        $res = $input['options_ids'];
        $default_type = !empty($input['default_type'])?$input['default_type']:'lease';

        $properties = Property::with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $res)
                        ->orderBy('id','desc')->paginate(20);

        $html = view('shortlists.printpdf', [
            'properties' => $properties,
            'default_type' => $default_type,
        ]);
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a5', 'landscape')->setWarnings(false)->stream();
    }

    public function option_print_agent(Request $request) {
        $input = $request->all();
        if (empty($input['options_ids'])) {
            return redirect('/');
        }
        $res = $input['options_ids'];
        $default_type = !empty($input['default_type'])?$input['default_type']:'lease';

        $properties = Property::with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $res)
                        ->orderBy('id','desc')->paginate(20);

        $html = view('shortlists.printpdfagent', [
            'properties' => $properties,
            'default_type' => $default_type,
        ]);
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream();
    }

    public function option_set(Request $request) {
        $input = $request->all();
        if (empty($input['options_ids'])) {
            return redirect('/');
        }
        $res = $input['options_ids'];
        $is_hot = !empty($input['default_type'])?$input['default_type']:0;

        $properties = Property::whereIn('id', $res)->orderBy('id','desc')->get();

        if (!empty($properties)) {
            foreach ($properties as $property) {
                $property->hot = $is_hot;
				$property->tst = time();
                $property->save();
            }
        }
        return !empty($data['current_url'])?redirect($data['current_url']):redirect('/list');
    }

	/**
     * Create a new property.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Property::$rules, Property::$messages);
        $data = Input::all();
        // dex($data);
        if (!empty($data['rep_ids'])) {
            $data['rep_ids'] = join(',',$data['rep_ids']);
        }else{
			$data['rep_ids'] = '';
		}
        if (!empty($data['agent_ids'])) {
            $data['agent_ids'] = join(',',$data['agent_ids']);
        }else{
			$data['agent_ids'] = '';
		}
		$property = $request->user()->properties()->create($data);
		//for triiger IO commission 
		$user = $request->user();		
		$role = json_decode($user->roles);
		
		if( $role->Informationofficer==1){
			//echo $prev_prop_id .'-'. $request->owner_id;		
			$io = new IOCommission();
			$io_data=array(
				'user_id'=>$user->id,
				'property_id'=>$property->id,
				'commission'=>500,
				'status'=>'draft',
				);
			$io->fill($io_data);
			$io->save();
			//DB::table('io_commissions')->insert($io_data);
		}

        return redirect('/listsearch?s='.$property->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$featurelist = Property::getPropertyFeaturesList();
		$featurelistold = Property::getOtherOldFeatures();
		$facilitylist = Property::getBuildingFacilitiesList();

		$ios = User::where('status','=', 0)->get();
		//$ios = User::where('roles', 'like', '%"Informationofficer": true%')->where('status','=', 0)->get();
		$io_user=array();
		foreach($ios as $io){
			$user_rights = $io->getUserRights();
			if($user_rights['Informationofficer'] == 1){
				$io_user[] = $io;
			}
		}
		
        return view('properties.create')->with('outdoor_ids', Property::outdoor_ids())
            ->with('feature_ids', Building::feature_ids())
            ->with('country_ids', Building::country_ids())
            ->with('district_ids', Building::district_ids())
			->with('featurelist', $featurelist)
			->with('featurelistold', $featurelistold)
			->with('io_users', $io_user)
			->with('facilitylist', $facilitylist);
    }
	
    public function createbob(Request $request, $id)
    {
		$bid = intval($id);
		$building = Building::findOrFail($bid);
		
		if ($building){
			$data = array(
				'user_id' => $request->user()->id,
				'name' => $building->name,
				'display_name' => $building->display_name,
				'building_id' => $building->id,
				'type_id' => 1,
				'address1' => $building->address1,
				'address2' => $building->address2,
				'district_id' => $building->district_id,
				'country_code' => $building->country_code,
				'year_built' => $building->year_built
			);
			$property = $request->user()->properties()->create($data);
			
			if ($building->facilities != null && trim($building->facilities) != ''){
				$meta = new PropertyMeta();
				$feature = new PropertyFeature();
				
				$nf = explode(',', $building->facilities);
				
				$encoded_features = json_encode($nf);
				$meta->add_post_meta($property->id, 'features', $encoded_features, true); 
				$feature->set_options($property->id, $nf);
			}
			
			return redirect('/property/edit/'.$property->id);
		}
		return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
        $owner = null;
        $agent = null;
        $reps = null;
        $agents = null;
        if ($property->owner_id) {
            // dex($property->owner_id);
            $owner = Vendor::findOrFail($property->owner_id);
        }
        if ($property->agent_id) {
            // dex($property->agent_id);
            $agent = Vendor::findOrFail($property->agent_id);
        }
        if (!empty($property->rep_ids)) {
            $reps = Vendor::whereIn('id', explode(',',$property->rep_ids))->get();
        }
        if (!empty($property->agent_ids)) {
            $agents = Vendor::whereIn('id', explode(',',$property->agent_ids))->get();
        }

        foreach(array('nest-photo','other-photo') as $group) {
            $photos[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }
        foreach(array('nest-doc') as $group) {
            $docs[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }

        $PropertyMeta = new PropertyMeta; 
        $outdoors = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoors', true));
        $features = json_decode($PropertyMeta->get_post_meta($property->id, 'features', true));
        $outdoorareas = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoorareas', true), true);

        return view('properties.show')
            ->with('property', $property)
            ->with('owner', $owner)
            ->with('agent', $agent)
            ->with('reps', $reps)
            ->with('agents', $agents)
            ->with('photos', $photos)
            ->with('docs', $docs)
            ->with('outdoor_ids', Property::outdoor_ids())
            ->with('feature_ids', Building::feature_ids())
            ->with('district_ids', Building::district_ids())
            ->with('country_ids', Building::country_ids())
            ->with('media_ids', Property::$media_ids)
            ->with('outdoorareas', $outdoorareas)
            ->with('outdoors', $outdoors)
            ->with('features', $features);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
		$input = $request->all();
		$searchterm = '';
		if (isset($input['searchterm'])){
			$searchterm = trim(filter_var($input['searchterm'], FILTER_SANITIZE_STRING));
		}
		
        $property = Property::with('agent')->with('owner')->with('allreps')->with('allagents')->withTrashed()->findOrFail($id);
       
        $PropertyMeta = new PropertyMeta; 
        $outdoors = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoors', true));
        $features = json_decode($PropertyMeta->get_post_meta($property->id, 'features', true));
        $outdoorareas = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoorareas', true), true);
        if (!$outdoors) $outdoors = array();
        if (!$features) $features = array();
        if (!$outdoorareas) $outdoorareas = array();
		
		$featurelist = $property->getPropertyFeaturesList();
		$featurelistold = $property->getOtherOldFeatures();
		$facilitylist = $property->getBuildingFacilitiesList();
		
		$docuploads = Propertydocuments::where('propertyid', '=', $property->id)->get();
		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}
		
		$invoices = Commission::where('propertyid', $id)->orderBy('id', 'desc')->get();
		$salelease = array();
		foreach ($invoices as $invoice){
			$sl = array();
			$sl['name'] = 'Nest Property';
			$sl['pic'] = '/showimage/users/dummy.jpg';
			$sl['datum'] = 'ongoing';
			$sl['content'] = 'test';
			$sl['type'] = '';
			
			if ($invoice->commissiondate != null){
				$sl['datum'] = Carbon::createFromFormat('Y-m-d', $invoice->commissiondate)->format('d/m/Y');
			}
			
			if (isset($consultants[$invoice->consultantid])){
				$uu = $consultants[$invoice->consultantid];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$sl['name'] = $uu->name;
					if ($uu->picpath != ''){
						$sl['pic'] = $uu->picpath;
					}
				}
			}
			
			if ($invoice->salelease == 1){
				//Lease
				$sl['type'] = 'Lease';
				if ($invoice->commissiondate != null){
					$sl['content'] = 'The property has been leased out on '.$sl['datum'];
				}else{
					$sl['content'] = 'The property is currently in the process of being leased out';
				}
				if (is_numeric($invoice->fullamount) && $invoice->fullamount > 0){
					$sl['content'] .= ' for HK$ '.number_format($invoice->fullamount, 0);
				}
				$sl['content'] .= '<br />';
				$sl['content'] .= '<a href="'.url('/shortlist/show/'.$invoice->shortlistid).'" target="_blank">Shortlist</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/documents/index-lease/'.$invoice->shortlistid).'" target="_blank">Documents</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/client/show/'.$invoice->clientid).'" target="_blank">Client</a> &nbsp; ';
				
			}else{
				//Sale
				$sl['type'] = 'Sale';
				if ($invoice->commissiondate != null){
					$sl['content'] = 'The property has been sold on '.$sl['datum'];
				}else{
					$sl['content'] = 'The property is currently in the process of being sold';
				}
				if (is_numeric($invoice->fullamount) && $invoice->fullamount > 0){
					$sl['content'] .= ' for HK$ '.number_format($invoice->fullamount, 0);
				}
				$sl['content'] .= '<br />';
				$sl['content'] .= '<a href="'.url('/shortlist/show/'.$invoice->shortlistid).'" target="_blank">Shortlist</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/documents/index-sale/'.$invoice->shortlistid).'" target="_blank">Documents</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/client/show/'.$invoice->clientid).'" target="_blank">Client</a> &nbsp; ';
			}
			
			
			$salelease[]= $sl;
		}

		$propertyDeleteLogs = PropertyDeleteLogs::where('property_id','=',$property->id)
		->with('deletedby')
		->with('confirmedby')
		->orderby('id','DESC')
		->get();

	
		$ios = User::where('status','=', 0)->get();
		//$ios = User::where('roles', 'like', '%"Informationofficer": true%')->where('status','=', 0)->get();
		$io_user=array();
		foreach($ios as $io){
			$user_rights = $io->getUserRights();
			if($user_rights['Informationofficer'] == 1){
				$io_user[] = $io;
			}
		}
		
        $migration_metas = PropertymigrationMeta::get_metas($property->id);
        return view('properties.repair', compact('property'))
			->with('outdoor_ids', Property::outdoor_ids())
            ->with('feature_ids', Building::feature_ids())
            ->with('country_ids', Building::country_ids())
            ->with('district_ids', Building::district_ids())
            ->with('migration_metas', $migration_metas)
            ->with('previous_url', \NestRedirect::previous_url())
            ->with('outdoorareas', $outdoorareas)->with('outdoors', $outdoors)
			->with('features', $features)
			->with('searchterm', $searchterm)
			->with('featurelist', $featurelist)
			->with('featurelistold', $featurelistold)
			->with('docuploads', $docuploads)
			->with('consultants', $consultants)
			->with('salelease', $salelease)
			->with('facilitylist', $facilitylist)
			->with('propertyDeleteLogs', $propertyDeleteLogs)
			->with('io_users', $io_user)
			->with('isTrashed', $property->trashed()
			);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$user = $request->user();
		
		$role = json_decode($user->roles);

        $data = $request->all();
        $this->validate($request, Property::$rules, Property::$messages);
        $validator = Validator::make($request->all(), Property::$rules, Property::$messages);
        if ( !$validator->fails() ) {
			$searchterm = '';
			if (isset($data['oldsearchterm'])){
				$searchterm = trim(filter_var($data['oldsearchterm'], FILTER_SANITIZE_STRING));
				unset($data['oldsearchterm']);
			}
			
            $property = Property::findOrFail($request->id);
            $data = Input::all();
            if (!empty($data['rep_ids'])) {
                $data['rep_ids'] = join(',',$data['rep_ids']);
            }else{
				$data['rep_ids'] = '';				
			}
            if (!empty($data['agent_ids'])) {
                $data['agent_ids'] = join(',',$data['agent_ids']);
            }else{
				$data['agent_ids'] = '';
			}
			
			if ($data['type_id'] == 1 && $data['leased'] == 1){
				$data['hot'] = 0;
			}else if ($data['type_id'] == 2 && $data['sold'] == 1){
				$data['hot'] = 0;
			}else if ($data['type_id'] == 3 && $data['sold'] == 1 && $data['leased'] == 1){
				$data['hot'] = 0;
			}

			/*
			//$property->owner_id,
			//print_r($user->id);
			///check owner changes
			$prop_id =intval( $request->id);
			$property = Property::findOrFail($prop_id);
			$prev_owner_id = $property['owner_id'];

			$leased = intval($request->leased);
			$sold = intval($request->sold);

			$io = new IOCommission();
		
			if($prev_owner_id !=  $request->owner_id && $role->Informationofficer==1){
				//echo $prev_prop_id .'-'. $request->owner_id;
				$io_data=array(
					'user_id'=>$user->id,
					'property_id'=>$prop_id,
					'commission'=>500,
					'status'=>'draft',
					);
				$io->fill($io_data);
				$io->save();
				//DB::table('io_commissions')->insert($io_data);				
			}
			
			//check if for commision			
			//echo $leased.$sold.$leased.$property['leased'].$sold.$property['sold'];
			
			/*if( ($leased==1 || $sold==1) && ( $leased !=$property['leased'] || $sold != $property['sold']) ){
				
				$io_com = IOCommission::where(['user_id'=>$user->id,'property_id'=>$prop_id])->pluck('id');
			//echo "-yes-";
				if(isset($io_com)){
					$thisCom = IOCommission::find($io_com);
					$thisCom->status = 'Pending';
					$thisCom->save();
					//echo "-yesss-";
				}
			}*/
		

			
            $property->fill($data);
			$property->tst = time();
            $property->save();
			
			if (empty($data['previous_url'])){
				return redirect('/listsearch?id='.$property->id);
			}else{
				return redirect($data['previous_url']);
			}
        }
        return redirect()->back()->withErrors($validator)->withInput();
    }

	public function deleted(){

		$propertyDeleteLogs = PropertyDeleteLogs::orderby('id', 'DESC')
		->with('deletedby')		
		->with('deleted_property')
		->where('confirmed_by','=', 0)		
		->where('status','!=', 'Restored')		
		->get();


		return view('properties.deletedproperties', [
			'propertyDeleteLogs' => $propertyDeleteLogs
		]);


	}

	public function deletedproperties( Request $request){

		$deletedProperties = Property::orderby('deleted_at', 'DESC');

		if($request->get('search') !== null ){
			$deletedProperties->Where('name', 'like', '%' .$request->get('search') . '%');
			$deletedProperties->orWhere('id', 'like', '%' .$request->get('search') . '%');
		}
		
		$deletedProperties = $deletedProperties->onlyTrashed()->paginate(25);

		$data = array();

		foreach($deletedProperties  as $pdl){
			$data[] = array(
				'id' => $pdl->id,
				'name' => $pdl->name,
				'address1' => $pdl->address1,
				'deleted_at' => $pdl->deleted_at,
				'deleted_by' => $this->deleted_by_($pdl->id),			

			);

			
		}

		
		
		return view('properties.confirmeddeleted', [
			'propertyDeleteLogs' => $deletedProperties,
			'data' => $data,
			'search' => $request->get('search') !== null ? $request->get('search') : null,
		]);
		

	}

	public function deleted_by_($property_id){
		$data =  PropertyDeleteLogs::orderby('id', 'DESC')
		->with('deletedby')		
		->where('confirmed_by','!=', 0)		
		->where('status','=', 'Confirmed')
		->where('property_id','=', $property_id)
		->first();
		
		return isset($data->deletedby->name) ? $data->deletedby->name : "-";

	}
	
	public function confirm_delete(Request $request){
		$data = $request->all();
		$data['deleted_property_id'];

		$property = Property::findOrFail($data['deleted_property_id']);
		if (!empty($property)) {
			$property->delete();
			
			$deleted_by = auth()->user()->id;	
			$delete_log = PropertyDeleteLogs::findOrFail($data['id']);
			$delete_log->confirmed_by = $deleted_by;
			$delete_log->status = "Confirmed";
			$delete_log->save();

			return redirect()->back()->with('success', 'Successfully Deleted');   

		}
		return redirect()->back();   
	}

	public function decline_delete(Request $request){
		$data = $request->all();
		$data['deleted_property_id'];

			$deleted_by = auth()->user()->id;	
			$delete_log = PropertyDeleteLogs::findOrFail($data['id']);		
			$delete_log->confirmed_by =  $deleted_by;
			$delete_log->status = "Declined";
			$delete_log->save();

			return redirect()->back()->with('success', 'Successfully Declined');   

		
	}

	public function restore_property(Request $request){
		$data = $request->all();
		

		/*$property = Property::findOrFail($data['id']);
		if (!empty($property)) {
			$property->delete();
			
			$deleted_by = auth()->user()->id;	
			$delete_log = PropertyDeleteLogs::findOrFail($data['id']);
			$delete_log->confirmed_by = $deleted_by;
			$delete_log->status = "Confirmed";
			$delete_log->save();

			return redirect()->back()->with('success', 'Successfully Deleted');   

		}*/

		$property = Property::onlyTrashed()->findOrFail($data['id']);
        if (!empty($property)) {
            $property->restore();

			$deleted_by = auth()->user()->id;	

			$PropertyDeleteLogs = new PropertyDeleteLogs;
			$PropertyDeleteLogs->property_id = $data['id'];
			$PropertyDeleteLogs->deleted_by = $deleted_by;
			$PropertyDeleteLogs->status = "Restored";
			$PropertyDeleteLogs->save();
        }

		return redirect()->back();   
	}

    public function delete(Request $request)
    {
        $data = $request->all();
        if (empty($data['property_id'])) die('invalid input'); 
        $property = Property::findOrFail($data['property_id']);
        if (!empty($property)) {
            
			$deleted_by = auth()->user()->id;			
			if( $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director'] || $request->user()->getUserRights()['Admin'] ){
				$property->PropertyDeleteLogs($property->id,$deleted_by,$deleted_by);
				$property->delete();
			} else {
				$property->PropertyDeleteLogs($property->id,$deleted_by,0);
			}
			

        }
        return !empty($data['previous_url'])?redirect($data['previous_url']):redirect('/list');
    }

    public function restore(Request $request)
    {
        $data = $request->all();
        if (empty($data['property_id'])) die('invalid input'); 
        $property = Property::onlyTrashed()->findOrFail($data['property_id']);
        if (!empty($property)) {
            $property->restore();
        }
        return redirect('/trash/list');
    }
	
    public function featrent($id){
        Property::where('id', $id)->update(array('rent_order' => time()));
		return 'The property has been put to the top of the rental property list. Please go to the <a href="http://nest-property.com/cms/wp-admin/">homepage backend</a>, click on Update Properties, search for the property ID and click on "update". Once that is done, please clear the cache of the Nest website, then you should see this property appear first.';
    }

    public function featsale($id){
        Property::where('id', $id)->update(array('sale_order' => time()));
		return 'The property has been put to the top of the rental property list. Please go to the <a href="http://nest-property.com/cms/wp-admin/">homepage backend</a>, click on Update Properties, search for the property ID and click on "update". Once that is done, please clear the cache of the Nest website, then you should see this property appear first (in the sales list).';
    }

    public function feathome($id){
        Property::where('id', $id)->update(array('feat_order' => time(), 'tst' => time()));
		return 'The property has been put to the top of the rental property list. Please go to the <a href="http://nest-property.com/cms/wp-admin/">homepage backend</a>, click on Update Properties, search for the property ID and click on "update". Once that is done, please clear the cache of the Nest website, then you should see this property appear first';
    }
	
	
	
	
    public function getcomments(Request $request, $id){
		$user = $request->user();
		
        $property = Property::findOrFail($id);
        if (empty($property)) {
            return '0';
        }
		
		if (trim($property->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $property);
		}else{
			$short = substr(trim($property->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($property->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($property->comments)
					)
				);
				$this->storecomments($id, $comments, $property);
			}
		}
		
		if (trim($property->contact_info) != ''){
			array_unshift($comments, array(
				'userid' => 0,
				'datetime' => 0,
				'content' => 'Original contact info from old DB (before 2015): '.trim($property->contact_info)
			));
		}
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		$datetime = date_create();
		
		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';
			
			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				//$comments[$i]['datum'] = date_format($datetime, 'Y-m-d G:i');
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('properties.commentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }
	
    public function addcomment(Request $request, $id){
        $property = Property::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($property)) {
            return '0';
        }
		
		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}
		
		if (trim($property->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($property->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($property->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($property->comments)
					)
				);
			}
		}
		
		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;
		
		$property->fill([
			'comments' => json_encode($comments),
			'tst' => time()
		])->save();
		
		return 'ok';
    }
	
	public function removecomment(Request $request, $id){
        $property = Property::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($property)) {
            return '0';
        }
		
		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}
		
		if (trim($property->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($property->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($property->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($property->comments)
					)
				);
			}
		}
		
		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}
		
		$property->fill([
			'comments' => json_encode($comments),
			'tst' => time()
		])->save();
		
		return 'ok';
	}
	
	
	public function storecomments($id, $comments, $property){
		$property->fill([
			'comments' => json_encode($comments),
			'tst' => time()
		])->save();
	}
	
    public function propertylist(Request $request)
    {		
        $input = $request->all();	
        $district_ids = Building::district_ids_search_main();
        $custom_listing_options = Property::custom_listing_options();
        //$properties_query = Property::PropertylistSearch($request)
		$properties_query = Property::select(DB::raw('properties.*'))
                        ->with('owner')->with('agent')->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
						->orderBy('properties.hot','desc')
                        ->orderBy('properties.leased','asc')
                        ->orderBy('properties.sold','asc')
                        ->orderBy('properties.available_date','desc')
                        ->orderBy('properties.tst','desc');
        $properties = $properties_query->paginate(36)->appends($input);
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.propertylist', [
            'properties' => $properties,
			'showadvsearch' => false,
			'searchterm' => ''
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request)
						->with('showtype', 'main');
    }
	
    public function propertylistsearch(Request $request){
		
        $input = $request->all();
		
		$searchterm = '';
		$orisearchterm = '';
		if (isset($input['s'])){
			$orisearchterm = $input['s'];
			$searchterm = str_replace('<br />', '|', $input['s']);
			$searchterm = trim(filter_var($searchterm, FILTER_SANITIZE_STRING));
		}
		
		$address = '';
		if (strstr($searchterm, '|')){
			$e = explode('|', $searchterm);
			$searchterm = trim($e[0]);
			$input['s'] = $searchterm;
			$address = trim($e[1]);
		}
		
        $district_ids = Building::district_ids_search();
        $custom_listing_options = Property::custom_listing_options();
		
		$propid = null;
		$phonesearch = array();
		
		if (is_numeric($orisearchterm)){
			$propid = intval($orisearchterm);
			$searchterm = '';
			$properties = Property::where('properties.id', '=', $orisearchterm)
							->select(DB::raw('properties.*'))
							->with('owner')->with('agent')->with('allreps')
							->with('building')
							->with('indexed_photo')
							->orderBy('properties.hot','desc')
							->orderBy('properties.leased','asc')
							->orderBy('properties.sold','asc')
							->orderBy('properties.available_date','desc')
							->orderBy('properties.tst','desc')->paginate(36);
			
			//Search for phone numbers
			//Client
			$clients = Client::whereRaw('tel like "%'.$propid.'%" or tel2 like "%'.$propid.'%"')->limit(5)->get();
			if ($clients->count() > 0){
				foreach ($clients as $c){
					$phonesearch[] = array(
						'type' => 'Client',
						'name' => $c->name(),
						'phone' => $c->tel(),
						'link' => '/client/show/'.$c->id
					);
				}
				if ($clients->count() == 5){
					$phonesearch[] = array(
						'type' => '...',
						'name' => 'For More Clients Click Here',
						'phone' => '',
						'link' => '/clients?sstring=&tel='.$propid.'&id=&budget_min=&budget_max=&tempature=&action=search'
					);
					$phonesearch[] = array(
						'type' => '&nbsp;',
						'name' => '',
						'phone' => '',
						'link' => ''
					);
				}
			}
			
			//Vendor
			$vendors = Vendor::whereRaw('tel like "%'.$propid.'%" or tel2 like "%'.$propid.'%"')->limit(5)->get();
			if ($vendors->count() > 0){
				foreach ($vendors as $c){
					$phonesearch[] = array(
						'type' => 'Vendor',
						'name' => $c->name_company(),
						'phone' => $c->tel(),
						'link' => '/vendor/show/'.$c->id
					);
				}
				if ($vendors->count() == 5){
					$phonesearch[] = array(
						'type' => '...',
						'name' => 'For More Vendors Click Here',
						'phone' => '',
						'link' => '/vendors?sstring=&company=&tel='.$propid.'&id=&action=search'
					);
				}
			}
			
			
			
			
			
			
		}else if ($address == ''){
			$properties = Property::where('properties.name', 'like', '%'.html_entity_decode($searchterm, ENT_QUOTES).'%')
							->select(DB::raw('properties.*'))
							->with('owner')
							->with('agent')
							->with('allreps')
							->with('building')
							->with('indexed_photo')
							->orderBy('properties.hot','desc')
							->orderBy('properties.leased','asc')
							->orderBy('properties.sold','asc')
							->orderBy('properties.available_date','desc')
							->orderBy('properties.tst','desc')->paginate(36);
		}else{
			$properties = Property::where('properties.name', 'like', '%'.html_entity_decode($searchterm, ENT_QUOTES).'%')->where('properties.address1', 'like', '%'.html_entity_decode($address, ENT_QUOTES).'%')
							->select(DB::raw('properties.*'))
							->with('owner')->with('agent')->with('allreps')
							->with('building')
							->with('indexed_photo')
							->orderBy('properties.hot','desc')
							->orderBy('properties.leased','asc')
							->orderBy('properties.sold','asc')
							->orderBy('properties.available_date','desc')
							->orderBy('properties.tst','desc')->paginate(36);
		}
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }
		
		if ($orisearchterm != ''){
			$properties->appends(array('s' => $orisearchterm));
		}
		
	    return view('properties.propertylist', [
            'properties' => $properties,
			'showadvsearch' => false,
			'searchterm' => $searchterm,
			'address' => $address,
			'propid' => $propid,
			'phonesearch' => $phonesearch
        ])->with('district_ids', $district_ids)
                        ->with('hot_option_ids', Property::$hot_option_ids)
                        ->with('photo_option_ids', Property::$photo_option_ids)
                        ->with('web_option_ids', Property::$web_option_ids)
                        ->with('custom_listing_options', $custom_listing_options)
                        ->with('outdoor_ids', Property::outdoor_ids())
                        ->with('feature_ids', Building::feature_ids_search())
                        ->with('input', $request);
    }
	
    public function propertylistadvsearch(Request $request){
		$input = $request->all();
		
		$district999 = false;
		$district998 = false;
		$district997 = false;
		$district996 = false;
		$input['country_code'] = [];
		$countryCodesList = Building::country_ids();
		
		if (isset($input['district_id']) && count($input['district_id']) > 0){
			foreach ($input['district_id'] as $d){
				if (intval($d) == 999){
					$district999 = true;
				}else if (intval($d) == 998){
					$district998 = true;
				}else if (intval($d) == 997){
					$district997 = true;
				}else if (intval($d) == 996){
					$district996 = true;
				}else {
					if ($districtInt = District::where('id', $d)->where('international', 1)->first()) {
						if ( ($code = array_search($districtInt->name, $countryCodesList)) !== FALSE) {
							$input['country_code'][] = $code;
						}
					}
				}
			}
		}
		
		if ($district999){
			foreach ($input['district_id'] as $ind => $d){
				if (intval($d) == 999){
					unset($input['district_id'][$ind]);
				}
			}
			$alldistricts = Building::district_ids_search();
			foreach ($alldistricts['Hong Kong Island'] as $ind => $$d){
				$input['district_id'][] = $ind;
			}
		}
		if ($district998){
			foreach ($input['district_id'] as $ind => $d){
				if (intval($d) == 998){
					unset($input['district_id'][$ind]);
				}
			}
			$alldistricts = Building::district_ids_search();
			foreach ($alldistricts['Island South'] as $ind => $$d){
				$input['district_id'][] = $ind;
			}
		}
		if ($district997){
			foreach ($input['district_id'] as $ind => $d){
				if (intval($d) == 997){
					unset($input['district_id'][$ind]);
				}
			}
			$alldistricts = Building::district_ids_search();
			foreach ($alldistricts['New Territories'] as $ind => $d){
				$input['district_id'][] = $ind;
			}
		}
		if ($district996){
			foreach ($input['district_id'] as $ind => $d){
				if (intval($d) == 996){
					unset($input['district_id'][$ind]);
				}
			}
			$alldistricts = Building::district_ids_search();
			foreach ($alldistricts['Kowloon'] as $ind => $d){
				$input['district_id'][] = $ind;
			}
		}
		
		$request->merge($input);
		
		DB::enableQueryLog();
		
        $district_ids = Building::district_ids_search_main();
        $custom_listing_options = Property::custom_listing_options();
        $properties_query = Property::PropertylistadvsearchSearch($request)
                        ->select(DB::raw('properties.*'))
                        ->with('owner')->with('agent')->with('allreps')
                        ->with('building')
                        ->with('indexed_photo');
						
		if($request->without_building_id){
			$properties_query->where(function($q){
				return $q->whereNull('building_id')
					->orWhere('building_id', '')
					->orWhere('building_id', 0);
			});
		}	

		if(isset($request->no_available_date) && $request->no_available_date == '1'){
			$properties_query->where('properties.available_date', '=','0000-00-00');
			$properties_query->orwhere('properties.available_date', '=', null);

		}

		if ($input['sort_by_id'] == 2){
			$properties_query->orderBy('properties.tst','desc');
		}else if ($input['sort_by_id'] == 13){			
			$properties_query->orderBy('properties.available_date','asc');
		}else if ($input['sort_by_id'] == 3){
			if ($input['type_id'] == 2){
				$properties_query->where('properties.asking_sale', '>','0');
				$properties_query->orderBy('properties.asking_sale','asc');
			}else{
				$properties_query->where('properties.asking_rent', '>','0');
				$properties_query->orderBy('properties.asking_rent','asc');
			}
		}else if ($input['sort_by_id'] == 4){
			if ($input['type_id'] == 2){
				$properties_query->where('properties.asking_sale', '>','0');
				$properties_query->orderBy('properties.asking_sale','desc');
			}else{
				$properties_query->where('properties.asking_rent', '>','0');
				$properties_query->orderBy('properties.asking_rent','desc');
			}
		}else if ($input['sort_by_id'] == 5){
			$properties_query->orderBy('properties.floornr','asc');
		}else if ($input['sort_by_id'] == 6){
			$properties_query->orderBy('properties.floornr','desc');
		}else if ($input['sort_by_id'] == 7){
			$properties_query->orderBy('properties.saleable_area','asc');
		}else if ($input['sort_by_id'] == 8){
			$properties_query->orderBy('properties.saleable_area','desc');
		}else if ($input['sort_by_id'] == 9){
			$properties_query->orderBy('properties.building_id','desc');
		}else if ($input['sort_by_id'] == 12){
			$properties_query->orderBy('properties.building_id','asc');
		}else if ($input['sort_by_id'] == 10){			
			$properties_query->orderBy('properties.available_date','desc');
		}else if ($input['sort_by_id'] == 11){			
			$properties_query->orderBy('properties.available_date','asc');
			
		}else{
			$properties_query->orderBy('properties.hot','desc')
							->orderBy('properties.leased','asc')
							->orderBy('properties.sold','asc')
							->orderBy('properties.available_date','desc')
							->orderBy('properties.tst','desc');
		}
		
		//var_dump($properties_query->toSql());
		
        $properties = $properties_query->paginate(36)->appends($input);
		
		
		//var_dump(count($properties));
		//var_dump(DB::getQueryLog());
		//exit;
        
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }

	    return view('properties.propertylist', [
            'properties' => $properties,
			'district_ids' => $district_ids,
			'hot_option_ids' => Property::$hot_option_ids,
			'photo_option_ids' => Property::$photo_option_ids,
			'web_option_ids' => Property::$web_option_ids,
			'custom_listing_options' => $custom_listing_options,
			'outdoor_ids' => Property::outdoor_ids(),
			'feature_ids' => Building::feature_ids_search(),
			'input' => $request,
			'showadvsearch' => true,
			'searchterm' => '',
			'showtype' => 'main'
        ]);
    }
	
    public function getshowcontent($id)
    {
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
        $owner = null;
        $agent = null;
        $reps = null;
        $agents = null;
        if ($property->owner_id) {
            // dex($property->owner_id);
            $owner = Vendor::findOrFail($property->owner_id);
        }
        if ($property->agent_id) {
            // dex($property->agent_id);
            $agent = Vendor::findOrFail($property->agent_id);
        }
        if (!empty($property->rep_ids)) {
            $reps = Vendor::whereIn('id', explode(',',$property->rep_ids))->get();
        }
        if (!empty($property->agent_ids)) {
            $agents = Vendor::whereIn('id', explode(',',$property->agent_ids))->get();
        }

        foreach(array('nest-photo','other-photo') as $group) {
            $photos[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }
        foreach(array('nest-doc') as $group) {
            $docs[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }

        $PropertyMeta = new PropertyMeta; 
        $outdoors = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoors', true));
        $features = json_decode($PropertyMeta->get_post_meta($property->id, 'features', true));
        $outdoorareas = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoorareas', true), true);
		
		$featurelist = $property->getPropertyFeaturesList();
		$facilitylist = $property->getBuildingFacilitiesList();
		
		$featurestring = '';
		$facilitystring = '';
		
		foreach ($featurelist as $feature_id => $feature){
			if ($feature_id != 10 && $feature_id != 12 && $feature_id != 9 && $feature_id != 26){
				if (is_array($features)){
					if (in_array($feature_id, $features)){
						if ($featurestring == ''){
							$featurestring = $feature;
						}else{
							$featurestring .= ' | '.$feature;
						}
					}
				}
			}
		}
		
		foreach ($facilitylist as $feature_id => $feature){
			if (is_array($features)){
				if (in_array($feature_id, $features)){
					if ($facilitystring == ''){
						$facilitystring = $feature;
					}else{
						$facilitystring .= ' | '.$feature;
					}
				}
			}
		}
		
        return view('properties.newshowcontent')
            ->with('property', $property)
            ->with('owner', $owner)
            ->with('agent', $agent)
            ->with('reps', $reps)
            ->with('agents', $agents)
            ->with('photos', $photos)
            ->with('docs', $docs)
            ->with('outdoor_ids', Property::outdoor_ids())
            ->with('feature_ids', Building::feature_ids())
            ->with('district_ids', Building::district_ids())
            ->with('country_ids', Building::country_ids())
            ->with('media_ids', Property::$media_ids)
            ->with('outdoorareas', $outdoorareas)
            ->with('outdoors', $outdoors)
            ->with('features', $features)
            ->with('featurestring', $featurestring)
            ->with('facilitystring', $facilitystring);
    }
	
    public function getshowinformation($id)
    {
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
        $owner = null;
        $agent = null;
        $reps = null;
        $agents = null;
        if ($property->owner_id) {
            // dex($property->owner_id);
            $owner = Vendor::findOrFail($property->owner_id);
        }
        if ($property->agent_id) {
            // dex($property->agent_id);
            $agent = Vendor::findOrFail($property->agent_id);
        }
        if (!empty($property->rep_ids)) {
            $reps = Vendor::whereIn('id', explode(',',$property->rep_ids))->get();
        }
        if (!empty($property->agent_ids)) {
            $agents = Vendor::whereIn('id', explode(',',$property->agent_ids))->get();
        }

        foreach(array('nest-photo','other-photo') as $group) {
            $photos[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }
        foreach(array('nest-doc') as $group) {
            $docs[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }

        $PropertyMeta = new PropertyMeta; 
        $outdoors = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoors', true));
        $features = json_decode($PropertyMeta->get_post_meta($property->id, 'features', true));
        $outdoorareas = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoorareas', true), true);
		
		$featurelist = $property->getPropertyFeaturesList();
		$facilitylist = $property->getBuildingFacilitiesList();
		
		$featurestring = '';
		$facilitystring = '';
		
		foreach ($featurelist as $feature_id => $feature){
			if ($feature_id != 10 && $feature_id != 12 && $feature_id != 9 && $feature_id != 26){
				if (is_array($features)){
					if (in_array($feature_id, $features)){
						if ($featurestring == ''){
							$featurestring = $feature;
						}else{
							$featurestring .= ' | '.$feature;
						}
					}
				}
			}
		}
		
		foreach ($facilitylist as $feature_id => $feature){
			if (is_array($features)){
				if (in_array($feature_id, $features)){
					if ($facilitystring == ''){
						$facilitystring = $feature;
					}else{
						$facilitystring .= ' | '.$feature;
					}
				}
			}
		}
		
        return view('properties.newshowinformation')
            ->with('property', $property)
            ->with('owner', $owner)
            ->with('agent', $agent)
            ->with('reps', $reps)
            ->with('agents', $agents)
            ->with('photos', $photos)
            ->with('docs', $docs)
            ->with('outdoor_ids', Property::outdoor_ids())
            ->with('feature_ids', Building::feature_ids())
            ->with('district_ids', Building::district_ids())
            ->with('country_ids', Building::country_ids())
            ->with('media_ids', Property::$media_ids)
            ->with('outdoorareas', $outdoorareas)
            ->with('outdoors', $outdoors)
            ->with('features', $features)
            ->with('featurestring', $featurestring)
            ->with('facilitystring', $facilitystring);
    }
	
    public function getpropertyclients($id)
    {
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
		
		$clients = array();
		
		$r = Client::PropertyClientSearch()->where('client_options.property_id', '=', $id)->get();
		foreach ($r as $c){
			$clients[] = $c;
		}
		
        return view('properties.newshowpropertyclients')
            ->with('clients', $clients);
    }
	
    public function getshowpics($id){
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
		
		$piccnt = 0;
        foreach(array('nest-photo','other-photo') as $group) {
            $photos[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->orderby('order', 'asc')->get();
			$piccnt += $photos[$group]->count();
        }
		
		if ($piccnt == 0){
			return '0';
		}

        return view('properties.newshowpics')
            ->with('photos', $photos);
    }
	
    public function getpiclinks($id){
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
		
		$ret = array();
		
        foreach(array('nest-photo','other-photo') as $group) {
            $photos = PropertyMedia::where('property_id', $property->id)->where('group', $group)->orderby('order', 'asc')->get();
			
			foreach ($photos as $photo){
				$ret[] = $photo->getShowUrlDownload();
			}
        }
		
		return json_encode($ret);
    }
	
    public function getshowpicsoriginal($id){
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
		
		$piccnt = 0;
        foreach(array('nest-photo','other-photo') as $group) {
            $photos[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->orderby('order', 'asc')->get();
			$piccnt += $photos[$group]->count();
        }
		
		if ($piccnt == 0){
			return '0';
		}

        return view('properties.showpicsoriginal')
            ->with('photos', $photos);
    }
	
    public function getshowcontact($id)
    {
        $property = Property::findOrFail($id);
        if (empty($property)) {
            die('property not found');
        }
        $owner = null;
        $agent = null;
        $reps = null;
        $agents = null;
        if ($property->owner_id) {
            // dex($property->owner_id);
            $owner = Vendor::findOrFail($property->owner_id);
        }
        if ($property->agent_id) {
            // dex($property->agent_id);
            $agent = Vendor::findOrFail($property->agent_id);
        }
        if (!empty($property->rep_ids)) {
            $reps = Vendor::whereIn('id', explode(',',$property->rep_ids))->get();
        }
        if (!empty($property->agent_ids)) {
            $agents = Vendor::whereIn('id', explode(',',$property->agent_ids))->get();
        }

        foreach(array('nest-photo','other-photo') as $group) {
            $photos[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }
        foreach(array('nest-doc') as $group) {
            $docs[$group] = PropertyMedia::where('property_id', $property->id)->where('group', $group)->get();
        }

        $PropertyMeta = new PropertyMeta; 
        $outdoors = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoors', true));
        $features = json_decode($PropertyMeta->get_post_meta($property->id, 'features', true));
        $outdoorareas = json_decode($PropertyMeta->get_post_meta($property->id, 'outdoorareas', true), true);

        return view('properties.newshowcontact')
            ->with('property', $property)
            ->with('owner', $owner)
            ->with('agent', $agent)
            ->with('reps', $reps)
            ->with('agents', $agents)
            ->with('photos', $photos)
            ->with('docs', $docs)
            ->with('outdoor_ids', Property::outdoor_ids())
            ->with('feature_ids', Building::feature_ids())
            ->with('district_ids', Building::district_ids())
            ->with('country_ids', Building::country_ids())
            ->with('media_ids', Property::$media_ids)
            ->with('outdoorareas', $outdoorareas)
            ->with('outdoors', $outdoors)
            ->with('features', $features);
    }
	
	private function showNiceNumbers($nr){
		$ret = '0';
		
		if (floor($nr) == $nr){
			$ret = floor($nr);
		}else{
			$ret = $nr;
		}
		
		return $ret;
	}
	
    public function getchanges(Request $request, $id){
		$user = $request->user();
		
        $property = Property::findOrFail($id);
        if (empty($property)) {
            return '0';
        }
		
		$changes = array();
		$rows = $property->getLog();
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		$o_types = array(1 => 'Rent', 2 => 'Sale', 3 => 'Rent & Sale');
		$o_poc = array(1 => 'Owner', 2 => 'Rep', 3 => 'Agency');
		$o_key = array(1 => 'Managment Office', 2 => 'Door Code', 3 => 'Agency', 4 => 'Other');
		$o_district_ids = Building::district_ids();
		$o_country_ids = Building::country_ids();
		
		$r = null;
		
		for ($i = 0; $i < $rows->count()-1; $i++){
			$r = $rows[$i];
			$n = array();
			
			$n['name'] = 'Nest Property';
			$n['pic'] = '/showimage/users/dummy.jpg';
			
			if ($r->userid == 0 || !isset($u[$r->userid])){
				$n['name'] = 'Nest Property';
				$n['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$r->userid];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$n['name'] = $uu->name;
					if ($uu->picpath != ''){
						$n['pic'] = $uu->picpath;
					}else{
						$n['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			
			//$n['datum'] = \NestDate::nest_datetime_format($r->logtime, 'Y-m-d H:i:s');
			$n['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', $r->logtime, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y H:i');
			
			$content = '';
			if (($i+1) < $rows->count()){
				$old = $rows[$i+1];
				
				if ($r['type_id'] != $old['type_id']){
					if (isset($o_types[$r['type_id']]) && isset($o_types[$old['type_id']])){
						$content .= '<b>Type</b> changed from '.$o_types[$old['type_id']].' to '.$o_types[$r['type_id']].'<br />';
					}
				}			
				if ($r['sold'] != $old['sold']){
					if ($r['sold'] == 1){
						$content .= 'The property has been <b>sold</b><br />';
					}else{
						$content .= 'The property is now on <b>sale</b><br />';
					}
				}
				if ($r['leased'] != $old['leased']){
					if ($r['leased'] == 1){
						$content .= 'The property is now <b>leased</b><br />';
					}else{
						$content .= 'The property is not <b>leased</b> anymore<br />';
					}
				}
				if ($r['asking_rent'] != $old['asking_rent']){
					$content .= '<b>Rental Price</b> changed from HK$ '.$old['asking_rent'].' to HK$ '.$r['asking_rent'].'<br />';
				}
				if ($r['asking_sale'] != $old['asking_sale']){
					$content .= '<b>Sale Price</b> changed from HK$ '.$old['asking_sale'].' to HK$ '.$r['asking_sale'].'<br />';
				}
				if ($r['available_date'] != $old['available_date'] && $r['available_date'] != null){
					$content .= 'New <b>Available Date</b>: '.Carbon::createFromFormat('Y-m-d', $r['available_date'])->format('d/m/Y').'<br />';
				}
				if ($r['name'] != $old['name']){
					$content .= 'New <b>Property Name</b>: '.$r['name'].'<br />';
				}
				if ($r['display_name'] != $old['display_name']){
					$content .= 'New <b>Display Name</b>: '.$r['display_name'].'<br />';
				}
				if ($r['building_id'] != $old['building_id']){
					$content .= 'New <b>Building ID</b>: '.$r['building_id'].'<br />';
				}
				if ($r['unit'] != $old['unit']){
					$content .= '<b>Unit</b> changed: '.$r['unit'].'<br />';
				}
				if ($r['floornr'] != $old['floornr']){
					$content .= '<b>Floor</b> changed: '.$r['floornr'].'<br />';
				}
				if ($r['address1'] != $old['address1']){
					$content .= 'New <b>Address</b>: '.$r['address1'].'<br />';
				}
				if ($r['district_id'] != $old['district_id'] && $old['district_id'] != 0){
					if (isset($o_district_ids[$r['district_id']]) && isset($o_district_ids[$old['district_id']])){
						$content .= '<b>District</b> changed from '.$o_district_ids[$old['district_id']].' to '.$o_district_ids[$r['district_id']].'<br />';
					}else if (isset($o_district_ids[$r['district_id']])){
						$content .= 'New <b>District</b>: '.$o_district_ids[$r['district_id']].'<br />';
					}
				}
				if ($r['country_code'] != $old['country_code']){
					if (isset($o_country_ids[$r['country_code']]) && isset($o_country_ids[$old['country_code']])){
						$content .= '<b>Country</b> changed from '.$o_country_ids[$old['country_code']].' to '.$o_country_ids[$r['country_code']].'<br />';
					}else if (isset($o_country_ids[$r['country_code']])){
						$content .= 'New <b>Country</b>: '.$o_country_ids[$r['country_code']].'<br />';
					}
				}
				if ($r['floor_zone'] != $old['floor_zone']){
					$content .= '<b>Floor Zone</b> changed: '.$r['floor_zone'].'<br />';
				}
				if ($r['layout'] != $old['layout']){
					$content .= '<b>Layout</b> changed: '.$r['layout'].'<br />';
				}
				if ($r['description'] != $old['description']){
					$content .= 'New <b>Description</b>: '.$r['description'].'<br />';
				}
				if ($r['bedroom'] != $old['bedroom']){
					$content .= '<b>Bedrooms</b> changed from '.$this->showNiceNumbers($old['bedroom']).' to '.$this->showNiceNumbers($r['bedroom']).'<br />';
				}
				if ($r['bathroom'] != $old['bathroom']){
					$content .= '<b>Bathrooms</b> changed from '.$this->showNiceNumbers($old['bathroom']).' to '.$this->showNiceNumbers($r['bathroom']).'<br />';
				}
				if ($r['maidroom'] != $old['maidroom']){
					$content .= '<b>Maidsrooms</b> changed from '.$this->showNiceNumbers($old['maidroom']).' to '.$this->showNiceNumbers($r['maidroom']).'<br />';
				}
				if ($r['year_built'] != $old['year_built']){
					$content .= '<b>Year Built</b> changed to '.$r['year_built'].'<br />';
				}
				if ($r['gross_area'] != $old['gross_area']){
					$content .= '<b>Gross Area</b> changed from '.$old['gross_area'].' to '.$r['gross_area'].'<br />';
				}
				if ($r['saleable_area'] != $old['saleable_area']){
					$content .= '<b>Saleable Area</b> changed from '.$old['saleable_area'].' to '.$r['saleable_area'].'<br />';
				}
				if ($r['inclusive'] != $old['inclusive']){
					if ($r['inclusive'] == 1){
						$content .= 'Fees are now <b>inclusive</b><br />';
					}else{
						$content .= 'Fees are not <b>inclusive</b> anymore<br />';
					}
				}
				if ($r['management_fee'] != $old['management_fee']){
					$content .= '<b>Management Fee</b> changed from '.$old['management_fee'].' to '.$r['management_fee'].'<br />';
				}
				if ($r['government_rate'] != $old['government_rate']){
					$content .= '<b>Government Rate</b> changed from '.$old['government_rate'].' to '.$r['government_rate'].'<br />';
				}
				if ($r['web'] != $old['web']){
					if ($r['web'] == 1){
						$content .= 'This property is now shown on the <b>website</b><br />';
					}else{
						$content .= 'This property is not shown on the <b>website</b> anymore<br />';						
					}
				}
				if ($r['hot'] != $old['hot']){
					if ($r['hot'] == 1){
						$content .= 'This property is now <b>hot</b><br />';
					}else{
						$content .= 'This property is not <b>hot</b> anymore<br />';						
					}
				}
				if ($r['bonuscomm'] != $old['bonuscomm']){
					if ($r['bonuscomm'] == 1){
						$content .= 'This property now has <b>bonus commission</b><br />';
					}else{
						$content .= 'This property has no <b>bonus commission</b> anymore<br />';						
					}
				}
				if ($r['pictst'] != $old['pictst']){
					//$content .= '<b>Pictures</b> were updated<br />';
				}
				if ($r['poc_id'] != $old['poc_id']){					
					if (isset($o_poc[$r['poc_id']]) && isset($o_poc[$old['poc_id']])){
						$content .= '<b>Point of Contact</b> changed from '.$o_poc[$old['poc_id']].' to '.$o_poc[$r['poc_id']].'<br />';
					}
				}
				if ($r['owner_id'] != $old['owner_id']){
					if ($r['owner_id'] == 0){
						$content .= '<b>Owner</b> was removed</a>';
						if ($old['owner_id'] != 0){
							$vendor = Vendor::find($old['owner_id']);
							if ($vendor){
								$content .= ', Removed Owner: <a href="'.url('/vendor/show/'.trim($old['owner_id'])).'" target="_blank">'.$vendor->name_company().'</a>';
							}
						}
						$content .= '<br />';
					}else{
						$vendor = Vendor::find($r['owner_id']);
						if ($vendor){
							$content .= 'New <b>Owner</b>: <a href="'.url('/vendor/show/'.trim($r['owner_id'])).'" target="_blank">'.$vendor->name_company().'</a>';
						}else{
							$content .= 'New <b>Owner</b>: <a href="'.url('/vendor/show/'.trim($r['owner_id'])).'" target="_blank">ID '.$r['owner_id'].'</a>';
						}
						if ($old['owner_id'] != 0){
							$vendor = Vendor::find($old['owner_id']);
							if ($vendor){
								$content .= ', Old Owner: <a href="'.url('/vendor/show/'.trim($old['owner_id'])).'" target="_blank">'.$vendor->name_company().'</a>';
							}
						}
						$content .= '<br />';
					}
				}
				if ($r['rep_ids'] != $old['rep_ids']){
					if (trim($r['rep_ids']) == ''){
						$content .= '<b>Reps</b> were removed<br />';
					}else{
						$e = explode(',', $r['rep_ids']);
						$content .= 'Updated <b>Reps</b> list: ';
						foreach ($e as $index => $rep){
							if ($index > 0){
								$content .= ', ';
							}
							$vendor = Vendor::find($rep);
							if ($vendor){
								$content .= '<a href="'.url('/vendor/show/'.trim($rep)).'" target="_blank">'.$vendor->name().'</a>';
							}else{
								$content .= '<a href="'.url('/vendor/show/'.trim($rep)).'" target="_blank">ID '.trim($rep).'</a>';
							}
						}
						$content .= '<br />';
					}
				}
				if ($r['agent_id'] != $old['agent_id']){
					if ($r['agent_id'] == 0){
						$content .= '<b>Agency</b> was removed</a>';
						if ($old['agent_id'] != 0){
							$vendor = Vendor::find($old['agent_id']);
							if ($vendor){
								$content .= ', Removed Agency: <a href="'.url('/vendor/show/'.trim($old['agent_id'])).'" target="_blank">'.$vendor->name_company().'</a>';
							}
						}
						$content .= '<br />';
					}else{
						$vendor = Vendor::find($r['agent_id']);
						if ($vendor){
							$content .= 'New <b>Agency</b>: <a href="'.url('/vendor/show/'.trim($r['agent_id'])).'" target="_blank">'.$vendor->name_company().'</a>';
						}else{
							$content .= 'New <b>Agency</b>: <a href="'.url('/vendor/show/'.trim($r['agent_id'])).'" target="_blank">ID '.$r['agent_id'].'</a>';
						}
						if ($old['agent_id'] != 0){
							$vendor = Vendor::find($old['agent_id']);
							if ($vendor){
								$content .= ', Old Agency: <a href="'.url('/vendor/show/'.trim($old['agent_id'])).'" target="_blank">'.$vendor->name_company().'</a>';
							}
						}
						$content .= '<br />';
					}
				}
				if ($r['agent_ids'] != $old['agent_ids']){
					if (trim($r['agent_ids']) == ''){
						$content .= '<b>Agents</b> were removed<br />';
					}else{
						$e = explode(',', $r['agent_ids']);
						$content .= 'Updated <b>Agents</b> list: ';
						foreach ($e as $index => $age){
							if ($index > 0){
								$content .= ', ';
							}
							$vendor = Vendor::find($age);
							if ($vendor){
								$content .= '<a href="'.url('/vendor/show/'.trim($age)).'" target="_blank">'.$vendor->name().'</a>';
							}else{
								$content .= '<a href="'.url('/vendor/show/'.trim($age)).'" target="_blank">ID '.trim($age).'</a>';
							}
						}
						$content .= '<br />';
					}
				}
				if ($r['key_loc_id'] != $old['key_loc_id']){
					if (isset($o_key[$r['key_loc_id']]) && isset($o_key[$old['key_loc_id']])){
						$content .= '<b>Key Location</b> changed from '.$o_key[$old['key_loc_id']].' to '.$o_key[$r['key_loc_id']].'<br />';
					}
				}
				if ($r['key_loc_other'] != $old['key_loc_other']){
					$content .= 'New <b>Other Key Location</b>: '.$r['key_loc_other'].'<br />';
				}
				if ($r['door_code'] != $old['door_code']){
					$content .= 'New <b>Door Code</b>: '.$r['door_code'].'<br />';
				}
				if ($r['agency_fee'] != $old['agency_fee']){
					$content .= '<b>Agency Fee</b> changed from '.$old['agency_fee'].' to '.$r['agency_fee'].'<br />';
				}
				if ($r['commission'] != $old['commission']){
					$content .= '<b>Commission</b> changed to '.$r['commission'].'<br />';
				}
			}else{
				$content = '1st entry';
			}
			$n['content'] = $content;
			
			if ($content != ''){
				$changes[] = $n;
			}
		}
		
		$n = array();
		
		$n['name'] = 'Nest Property';
		$n['pic'] = '/showimage/users/dummy.jpg';
		$n['content'] = 'Created by Nest Property';
		
		if ($property->user_id == 0 || !isset($u[$property->user_id])){
			$n['name'] = 'Nest Property';
			$n['pic'] = '/showimage/users/dummy.jpg';
			$n['content'] = 'Created by Nest Property';
		}else{
			$uu = $u[$property->user_id];
			
			if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
				$n['name'] = $uu->name;
				$n['content'] = 'Created by '.$uu->name;
				if ($uu->picpath != ''){
					$n['pic'] = $uu->picpath;
				}else{
					$n['pic'] = '/showimage/users/dummy.jpg';
				}
			}
		}
		//$n['datum'] = \NestDate::nest_datetime_format($property->created_at, 'Y-m-d H:i:s');
		//$n['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', $property->created_at, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y H:i');
		$n['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', $property->created_at)->format('d/m/Y H:i');
		$changes[] = $n;

        return view('properties.changesshow', [
			'changes' => $changes,
			'user' => $user
		]);
    }
	
	
	
	public function newproperties(Request $request){
		$input = $request->all();
		
		//Get all changes for that user within last 7 days
		$changesfull = LogProperties::whereRaw('logtime >= DATE_SUB(NOW(), INTERVAL 7 DAY)')->orderBy('logtime', 'desc')->paginate(20);

		$allids = array();
		foreach ($changesfull as $change){
			$allids[$change->id] = 1;
		}
		$ids = array_keys($allids);
		
		//Get all changes for all ids
		$changes = LogProperties::whereIn('id', $ids)->orderBy('logtime', 'desc')->get();
		$date = date_create();
		date_modify($date, '-7 day');
		$d = date_format($date, 'Y-m-d H:i:s');
		$changelist = array();
		foreach ($changes as $change){
			if (isset($changelist[$change->id])){
				$changelist[$change->id][] = $change;
			}else{
				$changelist[$change->id] = array();
				$changelist[$change->id][] = $change;
			}
		}
		
		$changelistout = array();
		foreach ($changelist as $change){
			$changelistout[] = $change;
		}
		$changelist = $changelistout;
		
		for ($i = 0; $i < count($changelist)-1; $i++){
			for ($a = $i+1; $a < count($changelist); $a++){
				if ($changelist[$i][0]->logtime < $changelist[$a][0]->logtime){
					$help = $changelist[$i];
					$changelist[$i] = $changelist[$a];
					$changelist[$a] = $help;
				}
			}
		}
		
        return view('properties.newproperties', [
			'd' => $d,
			'changelist' => $changelist,
			'changesfull' => $changesfull
		]);
	}
	
	public function flushmedia($propertyid){
		$base = env('HOST_ROOT');
		
		$pics = PropertyPhoto::where('property_id', $propertyid)->get();
		if ($pics->count() > 0){
			foreach ($pics as $pic){
				if (trim($pic['nest_photo'] != '')){
					$rem = trim(str_replace($base, '', $pic['nest_photo']));
					if (substr($rem, 0, 4) != "http"){
						if (file_exists($rem) && strstr($rem, 'images/system/')){
							unlink($rem);
						}
					}
				}
				if (trim($pic['other_photo'] != '')){
					$rem = trim(str_replace($base, '', $pic['other_photo']));
					if (substr($rem, 0, 4) != "http"){
						if (file_exists($rem) && strstr($rem, 'images/system/')){
							unlink($rem);
						}
					}
				}
				if (trim($pic['old_photo'] != '')){
					$rem = trim(str_replace($base, '', $pic['old_photo']));
					if (substr($rem, 0, 4) != "http"){
						if (file_exists($rem) && strstr($rem, 'images/system/')){
							unlink($rem);
						}
					}
				}
				$pic->delete();
			}
		}
		
		$medias = PropertyMedia::where('property_id', $propertyid)->get();
		if ($medias->count() > 0){
			foreach ($medias as $media){
				$pics = PropertyImageVariation::where('property_media_id', $media['id'])->get();
				if ($pics->count() > 0){
					foreach ($pics as $pic){
						if (trim($pic['url'] != '')){
							$rem = trim(str_replace($base, '', $pic['url']));
							if (substr($rem, 0, 4) != "http"){
								if (file_exists($rem)){
									unlink($rem);
								}
							}
						}
						$pic->forceDelete();
					}
				}
			}
		}
		
		$property = Property::findOrFail($propertyid);
		$property->pictst = time();
		$property->save();		
		
        return redirect('/property/edit/'.$propertyid);
	}
	
	private function custom_ucwords($str){
		$e = explode(' ', trim($str));
		$ret = '';
		foreach ($e as $s){
			$s = strtolower($s);
			$s = trim($s);
			$s = strtoupper(substr($s, 0, 1)).substr($s, 1);
			$ret .= $s.' ';
		}
		return trim($ret);
	}
	
	
	public function csvimport(){
		$file = file('../storage/csv.csv');
		
		for ($i = 1; $i < count($file); $i++){
			$e = explode('	', trim($file[$i]));
			$out = '';
			if (count($e) > 64){
				$data = array();
				$data['cid'] = 1;
				
				//5...Address
				$lcaddress = $e[5];
				$lcaddress = $this->custom_ucwords($lcaddress);
				$out .= 'Address: '.$lcaddress.'<br />';
				$data['address'] = $lcaddress;
				//6...Building Name
				$lcbuilding = $e[6];
				$lcbuilding = $this->custom_ucwords($lcbuilding);
				$out .= 'Building: '.$lcbuilding.'<br />';
				$data['building'] = $lcbuilding;
				//7...District
				$out .= 'District: '.$e[7].'<br />';
				$data['district'] = trim($e[7]);
				//9...Unit
				$out .= 'Unit: '.$e[9].'<br />';
				$data['unit'] = trim($e[9]);
				//10/11...area
				$out .= 'Size: '.$e[10].'<br />';
				$iex = unpack("C*", trim($e[10]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['size'] = trim($nint);
				//13/15...lease rate
				$out .= 'Lease price: '.$e[13].'<br />';
				$iex = unpack("C*", trim($e[13]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['lease'] = $nint;
				//14...lease details
				//$out .= 'Details: '.$e[14].'<br />';
				$info = '';
				$info .= 'Details: '.trim($e[14]).PHP_EOL;
				$iex = unpack("C*", trim($e[14]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['details'] = trim($nint);
				//19...inclusive
				$out .= 'Inclusive: '.$e[19].'<br />';
				$iex = unpack("C*", trim($e[19]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['inclusive'] = trim($nint);
				//20...available date
				//$out .= 'Available date: '.$e[20].'-'.trim($e[21]).'<br />';
				if (strlen($e[20]) > 3){
					$ex = explode(' ', trim($e[20]));
					$mex = unpack("C*", trim($ex[0]));
					$month = chr($mex[1]).chr($mex[3]).chr($mex[5]);				
					$dex = unpack("C*", trim($ex[1]));
					$day = '';
					for ($ii = 1; $ii <= count($dex); $ii = $ii+2){
						$day .= chr($dex[$ii]);
					}
					$yex = unpack("C*", trim($e[21]));
					$year = '';
					for ($ii = 1; $ii <= count($yex); $ii = $ii+2){
						$year .= chr($yex[$ii]);
					}
					$d = date_create_from_format('M d Y', $month.' '.$day.' '.$year);
					$out .= 'Available date: '.$month.' '.$day.' '.$year.'<br />';
					$data['available'] = $d->format('Y-m-d');
					$out .= 'Available date: '.$data['available'].'<br />';
				}
				//22...Management fee
				$out .= 'Management fee: '.$e[22].'<br />';
				$iex = unpack("C*", trim($e[22]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['mfee'] = $nint;
				//23...Government rates
				$out .= 'Government rates: '.$e[23].'<br />';
				$iex = unpack("C*", trim($e[23]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['govrates'] = $nint;
				//24...Bedrooms
				$out .= 'Bedrooms: '.$e[24].'<br />';
				$iex = unpack("C*", trim($e[24]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['bedrooms'] = $nint;
				//25...Bathrooms
				$out .= 'Bathrooms: '.$e[25].'<br />';
				$iex = unpack("C*", trim($e[25]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['bathrooms'] = $nint;
				//27...Parking spots
				$out .= 'Parking spots: '.$e[27].'<br />';
				$iex = unpack("C*", trim($e[27]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['parking'] = $nint;
				//26...Key location
				$info .= 'Key location: '.trim($e[26]).PHP_EOL;
				$iex = unpack("C*", trim($e[26]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($nint);
				}
				$data['keyloc'] = trim($e[26]);
				//28...Contact info
				$info .= 'Contact info: '.trim($e[28]).PHP_EOL;
				$iex = unpack("C*", trim($e[28]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['contactinfo'] = trim($nint);
				//33...Further info
				$info .= 'Further info: '.trim($e[33]).PHP_EOL;
				$iex = unpack("C*", trim($e[33]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['finfo1'] = trim($nint);
				//34...Further info
				$info .= 'Further info: '.trim($e[34]).PHP_EOL;
				$iex = unpack("C*", trim($e[34]));
				$nint = '';
				for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
					$nint .= chr($iex[$ii]);
				}
				$data['finfo2'] = trim($nint);
				$base = 35;
				while (strlen($e[$base]) > 4 && $base < 65){
					$base++;
				}
				if (!isset($e[$base+29])){
					echo '--- Not properly formated start ---<br />'.trim($file[$i]).'<br />--- Not properly formated end ---<br />';
				}else{
					//36...Year built
					$out .= 'Year built: '.$e[$base+1].'<br />';
					$iex = unpack("C*", trim($e[$base+1]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['yearbuilt'] = $nint;
					//42...Street name
					//$out .= 'Street name: '.$e[$base+7].'<br />';
					//43...City
					//$out .= 'City: '.$e[$base+9].'<br />';
					$info .= 'City: '.trim($e[$base+9]).PHP_EOL;
					$data['city'] = trim($e[$base+9]);
					$out .= nl2br($info);
					$data['info'] = trim($info);
					if (trim($data['district']) == ''){
						$data['district'] = trim($e[$base+9]);
					}
					//48...Usage
					//$out .= 'Usage: '.$e[$base+14].'<br />';
					//56...Floor Nr
					$out .= 'Floor Nr.: '.$e[$base+22].'<br />';
					$iex = unpack("C*", trim($e[$base+22]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['floor'] = trim($nint);
					//64...Type of Availability
					$out .= 'Type of Availability: ';
					if (trim($e[$base+28]) == 'Sale or Lease'){
						$out .= '3';
						$data['type'] = 3;
					}else if (trim($e[$base+28]) == 'Sale'){
						$out .= '2';
						$data['type'] = 2;
					}else{
						$out .= '1';
						$data['type'] = 1;
					}
					$out .= '<br />';
					//65...Sale price
					$out .= 'Sale price: '.$e[$base+29].'<br />';
					$iex = unpack("C*", trim($e[$base+29]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['sale'] = $nint;
					
					echo $out;
					
					//$imp = new Csvimportproperty();
					//$imp->create($data);
				}
				//echo $out;
				echo '<br />';
				
			}
		}
		
		
        return '';
	}
	
	
	
    public function docstoreupload($propertyid, Request $request){
		$input = $request->all();
		$propertyid = intval($propertyid);
		
		$rules = array(
			'upfile' =>  'mimes:pdf'
		);
		$validator = Validator::make($request->all(), $rules);
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			if (Input::hasFile('upfile') && Input::file('upfile')->isValid() && $validator->errors()->count() == 0){
				$path = '../storage/propertydocs/';
				$name = $propertyid.'_'.time().'_'.$input['doctype'].'.pdf';
				Input::file('upfile')->move($path, $name);
				
				$doc = new Propertydocuments();
				$doc->doctype = $input['doctype'];
				$doc->propertyid = $propertyid;
				$doc->consultantid = $request->user()->id;
				$doc->docpath = $path.$name;
				$doc->save();
			}
		}
		
		return redirect('/property/edit/'.$propertyid);
	}
	
	public function showdoc($docid){
		$docupload = Propertydocuments::where('id', '=', $docid)->first();
		
		if ($docupload){
			if (file_exists($docupload->docpath)){
				return response()->download($docupload->docpath);
			}else{
				return redirect('/');
			}
		}else{
			return redirect('/');
		}
	}
	
	//not working because the images are on various servers
	
	/*public function zipimages(Request $request, $id){
		$path = '../storage/zips/'.intval($id).'_'.time().'.zip';
		
		$nestphoto = PropertyMedia::where('property_id', $id)->where('group', 'nest-photo')->get();
		$otherphoto = PropertyMedia::where('property_id', $id)->where('group', 'other-photo')->get();
        
		$zip = new ZipArchive;
		if ($zip->open($path, ZipArchive::CREATE) === TRUE){
			foreach ($nestphoto as $index => $pic){
				$url = $pic->url;
				$ext = pathinfo($url, PATHINFO_EXTENSION);
				
				if (strstr($url, 'db.nest-property.com') || strstr($url, '192.168.1.115:8082')){
					$url = str_replace('https://', '', $url);
					$url = str_replace('http://', '', $url);
					$url = str_replace('db.nest-property.com/cms', '', $url);
					$url = str_replace('db.nest-property.com', '', $url);
					$url = str_replace('192.168.1.115:8082', '', $url);
					
					$zip->addFile('../public'.$url, 'image-nest-'.($index+1).'.'.$ext);
				}
			}
			foreach ($otherphoto as $index => $pic){
				$url = $pic->url;
				$ext = pathinfo($url, PATHINFO_EXTENSION);
				
				if (strstr($url, 'db.nest-property.com') || strstr($url, '192.168.1.115:8082')){
					$url = str_replace('https://', '', $url);
					$url = str_replace('http://', '', $url);
					$url = str_replace('db.nest-property.com/cms', '', $url);
					$url = str_replace('db.nest-property.com', '', $url);
					$url = str_replace('192.168.1.115:8082', '', $url);
					
					$zip->addFile('../public'.$url, 'image-nest-'.($index+1).'.'.$ext);
				}
			}
			$zip->close();
		}
		
		$headers = array(
			'Content-Type' => 'application/octet-stream',
		);
		
		if (file_exists($path)){
			return response()->download($path, 'pictures-raw.zip', $headers);
		}
	}*/
	
	
	public function buildingpics($propertyid, $buildingid){
		$properties = Property::where('building_id', $buildingid)->where('id', '!=', $propertyid)->get();
		$pictures = array();
		
		foreach ($properties as $p){
			$pics = PropertyMedia::where('property_id', $p->id)->whereIn('group', ['nest-photo','other-photo'])->orderby('order', 'asc')->get();
			if ($pics->count() > 0){
				$pictures[$p->id] = $pics;
			}
		}
		
		return view('properties.buildingpics', [
			'propertyid' => $propertyid,
			'buildingid' => $buildingid,
			'properties' =>  $properties,
			'pictures' => $pictures
		]);
	}
	
	public function addbuildingpics($propertyid, $photopropertyid){
		$pics = PropertyMedia::where('property_id', $photopropertyid)->whereIn('group', ['nest-photo','other-photo'])->orderby('order', 'asc')->get();
		
		foreach ($pics as $p){
			$data = $p->toArray();
			
			$data['property_id'] = $propertyid;
			$data['order'] = 0;
			$data['web_order'] = 0;
			unset($data['id']);
			unset($data['updated_at']);
			unset($data['created_at']);
			unset($data['deleted_at']);
			
			$new = PropertyMedia::create($data);
			$new->save();
		}
		
		return $this->flushmedia($propertyid);
	}
	
	
    public function getleasesales(Request $request, $id){
		$user = $request->user();
		
        $property = Property::findOrFail($id);
        if (empty($property)) {
            return '0';
        }
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		$invoices = Commission::where('propertyid', $id)->orderBy('id', 'desc')->get();
		$salelease = array();
		foreach ($invoices as $invoice){
			$sl = array();
			$sl['name'] = 'Nest Property';
			$sl['pic'] = '/showimage/users/dummy.jpg';
			$sl['datum'] = 'ongoing';
			$sl['content'] = 'test';
			$sl['type'] = '';
			
			if ($invoice->commissiondate != null){
				$sl['datum'] = Carbon::createFromFormat('Y-m-d', $invoice->commissiondate)->format('d/m/Y');
			}
			
			if (isset($u[$invoice->consultantid])){
				$uu = $u[$invoice->consultantid];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$sl['name'] = $uu->name;
					if ($uu->picpath != ''){
						$sl['pic'] = $uu->picpath;
					}
				}
			}
			
			if ($invoice->salelease == 1){
				//Lease
				$sl['type'] = 'Lease';
				if ($invoice->commissiondate != null){
					$sl['content'] = 'The property has been leased out on '.$sl['datum'];
				}else{
					$sl['content'] = 'The property is currently in the process of being leased out';
				}
				if (is_numeric($invoice->fullamount) && $invoice->fullamount > 0){
					$sl['content'] .= ' for HK$&nbsp;'.number_format($invoice->fullamount, 0);
				}
				$sl['content'] .= '<br />';
				$sl['content'] .= '<a href="'.url('/shortlist/show/'.$invoice->shortlistid).'" target="_blank">Shortlist</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/documents/index-lease/'.$invoice->shortlistid).'" target="_blank">Documents</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/client/show/'.$invoice->clientid).'" target="_blank">Client</a> &nbsp; ';
				
			}else{
				//Sale
				$sl['type'] = 'Sale';
				if ($invoice->commissiondate != null){
					$sl['content'] = 'The property has been sold on '.$sl['datum'];
				}else{
					$sl['content'] = 'The property is currently in the process of being sold';
				}
				if (is_numeric($invoice->fullamount) && $invoice->fullamount > 0){
					$sl['content'] .= ' for HK$&nbsp;'.number_format($invoice->fullamount, 0);
				}
				$sl['content'] .= '<br />';
				$sl['content'] .= '<a href="'.url('/shortlist/show/'.$invoice->shortlistid).'" target="_blank">Shortlist</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/documents/index-sale/'.$invoice->shortlistid).'" target="_blank">Documents</a> &nbsp; ';
				$sl['content'] .= '<a href="'.url('/client/show/'.$invoice->clientid).'" target="_blank">Client</a> &nbsp; ';
			}
			
			$salelease[]= $sl;
		}
		
		

        return view('properties.leasesalesshow', [
			'user' => $user,
			'salelease' => $salelease
		]);
    }
	
	public function removedoc(Request $request, $docid, $propertyid){
		$user = $request->user();	
		
		$doc = Propertydocuments::where('id', '=', $docid)->first();
		
		if ($doc && ($doc->consultantid == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			$doc->delete();
			
			return redirect('/property/edit/'.$propertyid);
		}else{
			return redirect('/');
		}
	}
	
	public function showphotoshoot(Request $request){
		$user = $request->user();
		
		$properties = Property::where('photoshoot', '=', 1)->with('owner')->with('agent')->with('allreps')->with('building')->with('indexed_photo')->get();
				
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }
		
		$unfinishedshootings = Photoshoot::where('status', '!=', 10)->orderBy('v_date', 'desc')->get();
		$finishedshootings = Photoshoot::where('status', '=', 10)->orderBy('v_date', 'desc')->get();
		
        return view('properties.photoshoot', [
			'user' => $user,
			'properties' => $properties,
			'unfinishedshootings' => $unfinishedshootings,
			'finishedshootings' => $finishedshootings
		]);
	}


	public function property_exists(Request $request){
		$user = $request->user();

		$property = Property::where('name','=',$request->name);

		//if(isset($request->unit)){
			$property->where('unit','=',$request->unit);
	//	}
		
	//	if(isset($request->display_name)){
		//$property->where('display_name','=',$request->display_name);
	//	}
		
	//	if(isset($request->floornr)){
		$property->where('floornr','=',$request->floornr);
	//	}

	//	if(isset($request->address1)){
		$property->where('address1','=',$request->address1);
	//	}

	//	if(isset($request->building_id)){
		$property->where('building_id','=',$request->building_id);
	//	}

		$count = $property->count();

		if($count > 0){

			$results = $property->get();

			$html = "<strong>Similar Properties</strong><br><br><ul>";

			foreach($results as $result){
				$html .= '<li> <a target="_blank" style="color: #FFF;" href="/property/edit/'.$result->id.'">'.$result->name.' ('.$result->id.')</a></li>';
			}
			$html .= "</ul>";
			$html .= "<br><p>Would you like to proceed adding this new property? Please click Confirm to proceed.</p>";

			$return = array('count' => $count, 'results'=>$html );
		} else {
			$return = array('count' => $count  );
		}

echo json_encode($return);
		
	}

	public function property_exists_update(Request $request){
		$user = $request->user();

		$property = Property::where('name','=',$request->name);

		//if(isset($request->unit)){
			$property->where('unit','=',$request->unit);
	//	}
		
	//	if(isset($request->display_name)){
		//$property->where('display_name','=',$request->display_name);
	//	}
		
	//	if(isset($request->floornr)){
		$property->where('floornr','=',$request->floornr);
	//	}

	//	if(isset($request->address1)){
		$property->where('address1','=',$request->address1);
	//	}

	//	if(isset($request->building_id)){
		$property->where('building_id','=',$request->building_id);
	//	}

	$property->where('id','!=',$request->id);

		$count = $property->count();

		if($count > 0){

			$results = $property->get();

			$html = "<strong>Similar Properties</strong><br><br><ul>";

			foreach($results as $result){
				$html .= '<li> <a target="_blank" style="color: #FFF;" href="/property/edit/'.$result->id.'">'.$result->name.' ('.$result->id.')</a></li>';
			}
			$html .= "</ul>";
			$html .= "<br><p>Would you like to proceed updating this property? Please click Confirm to proceed.</p>";

			$return = array('count' => $count, 'results'=>$html );
		} else {
			$return = array('count' => $count  );
		}

echo json_encode($return);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
