<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

use App\BasedocumentFields;
use App\Basedocument;
use App\User;
use App\Commission;

use View;
use App\Lead;
use App\LogClick;

class BasedocumentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(8);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
					
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$docs = Basedocument::orderBy('id', 'desc')->paginate(25);
		
		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}
		
		return view('basedocuments.index', [
			'docs' => $docs,
			'consultants' => $consultants
		]);
    }
	
	
	
	//Blank Letter
    public function blankletteredit(Request $request, $docid){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$docversion = BasedocumentFields::getCurDocVersions('bl');
		
		$data = array(
			'doctype' => 'blank',
			'docversion' => $docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		if ($docid > 0){
			$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'blank')->orderBy('id', 'desc')->first();
			if ($row){
				$new = false;
			}
		}
		
		$fieldslist = BasedocumentFields::getBlankLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getBlankLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['id'] = 0;
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			$data['consultantid'] = $row->consultantid;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = BasedocumentFields::getBlankLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('basedocuments.blank-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields
		]);
    }
	
	public function blankletterupdate(Request $request){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = BasedocumentFields::getBlankLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'blank',
				'docversion' => $docversion,
				'consultantid' => intval($input['consultantid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			$docid = intval($input['id']);
			if ($docid == 0){
				//New
				$doc = Basedocument::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Basedocument::findOrFail($docid);
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if ($docid > 0){
			return redirect('/basedocuments/blankletter/'.$docid);
		}else{
			return redirect('/basedocuments');
		}
	}
	
	
    public function blankletterpdf(Request $request, $docid, $logo = 'yes'){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'blank')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/basedocuments');
		}
		
		$data = array(
			'doctype' => 'blank',
			'docversion' => $row->docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => $docid
		);
		
		$fieldslist = BasedocumentFields::getBlankLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getBlankLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = BasedocumentFields::getBlankLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}

		$template = 'basedocuments.blank-letter-pdf-v2';
		if ($logo == 'no') {
			$template = 'basedocuments.blank-letter-pdf-v1';
		}
		
		$html = view($template, [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'logo' => $logo
		]);
		
		$name = 'Blank Letter '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
	//Employment Letter
    public function employmentletteredit(Request $request, $docid){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$docversion = BasedocumentFields::getCurDocVersions('el');
		
		$data = array(
			'doctype' => 'employmentletter',
			'docversion' => $docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		if ($docid > 0){
			$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'employmentletter')->orderBy('id', 'desc')->first();
			if ($row){
				$new = false;
			}
		}
		
		$fieldslist = BasedocumentFields::getEmploymentLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getEmploymentLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'Will Robertson';
			$data['fieldcontents']['f3'] = 'R123456(7)';
			$data['fieldcontents']['f4'] = 'December 2016';
			$data['fieldcontents']['f5'] = 'To Whom It May Concern:';
			$data['fieldcontents']['f6'] = 'Mr. Robertson holds the position of Senior Consultant of the company. Her basic salary is HKD360,000 per annum plus commission and company bonuses — performance specific. ';
			$data['fieldcontents']['f7'] = 'Naomi Budden';
			$data['fieldcontents']['f8'] = 'Director | Residential Search & Investment';
			$data['fieldcontents']['f9'] = 'E-278433';
			$data['id'] = 0;
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			$data['consultantid'] = $row->consultantid;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = BasedocumentFields::getEmploymentLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('basedocuments.employment-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields
		]);
    }
	
	public function employmentletterupdate(Request $request){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = BasedocumentFields::getEmploymentLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'employmentletter',
				'docversion' => $docversion,
				'consultantid' => intval($input['consultantid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			$docid = intval($input['id']);
			if ($docid == 0){
				//New
				$doc = Basedocument::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Basedocument::findOrFail($docid);
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if ($docid > 0){
			return redirect('/basedocuments/employmentletter/'.$docid);
		}else{
			return redirect('/basedocuments');
		}
	}
	
	
    public function employmentletterpdf(Request $request, $docid, $logo = 'yes'){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'employmentletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/basedocuments');
		}
		
		$data = array(
			'doctype' => 'employmentletter',
			'docversion' => $row->docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => $docid
		);
		
		$fieldslist = BasedocumentFields::getEmploymentLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getEmploymentLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = BasedocumentFields::getEmploymentLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('basedocuments.employment-letter-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'logo' => $logo
		]);
		
		$name = 'Employment Letter '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
	
	
	
	
	//Letter of Dismissal
    public function dismissalletteredit(Request $request, $docid){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$docversion = BasedocumentFields::getCurDocVersions('lod');
		
		$data = array(
			'doctype' => 'letterofdismissal',
			'docversion' => $docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		if ($docid > 0){
			$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'letterofdismissal')->orderBy('id', 'desc')->first();
			if ($row){
				$new = false;
			}
		}
		
		$fieldslist = BasedocumentFields::getDismissalLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getDismissalLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		if ($new){
			$dd = date_create();
			$dd = $dd->modify('+1 month');
			
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'Mr. Firstname Lastname';
			$data['fieldcontents']['f3'] = 'By Email';
			$data['fieldcontents']['f4'] = 'Dear xxx';
			$data['fieldcontents']['f5'] = 'RE: Employment at Nest Property as Accounts & Human Resources Associate Director [Full Job Tittle]';
			$data['fieldcontents']['f6'] = date('Y-m-d');
			$data['fieldcontents']['f7'] = 'one month\'s notice';
			$data['fieldcontents']['f8'] = $dd->format('Y-m-d');
			$data['fieldcontents']['f9'] = 'During this period you are expected to continue to fulfil your job obligations and carry out your role at the company as set out in the Offer of Employment. OR However, during this period, you do not need to return back to work. Nevertheless, your salary and other benefits to which you are entitled under your contract will continue to be paid during this notice period.';
			$data['fieldcontents']['f10'] = 'January 2015 Salary';
			$data['fieldcontents']['f11'] = '= HK$XX,XXX.XX less MPF (Payable on 31st Jan 2015)';
			$data['fieldcontents']['f12'] = 'February 2015 Salary (1-22 Feb)';
			$data['fieldcontents']['f13'] = '= HK$XXX,XXX.XX less MPF (Payable on 28th Feb 2015)';
			$data['fieldcontents']['f14'] = '';
			$data['fieldcontents']['f15'] = '';
			$data['fieldcontents']['f16'] = '[Full Name of Employee]';
			$data['fieldcontents']['f17'] = 'Naomi Budden';
			$data['fieldcontents']['f18'] = 'Managing Director';
			$data['id'] = 0;
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			$data['consultantid'] = $row->consultantid;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = BasedocumentFields::getDismissalLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('basedocuments.dismissal-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields
		]);
    }
	
	public function dismissalletterupdate(Request $request){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = BasedocumentFields::getDismissalLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'letterofdismissal',
				'docversion' => $docversion,
				'consultantid' => intval($input['consultantid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			$docid = intval($input['id']);
			if ($docid == 0){
				//New
				$doc = Basedocument::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Basedocument::findOrFail($docid);
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if ($docid > 0){
			return redirect('/basedocuments/dismissalletter/'.$docid);
		}else{
			return redirect('/basedocuments');
		}
	}
	
	
    public function dismissalletterpdf(Request $request, $docid, $logo = 'yes'){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'letterofdismissal')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/basedocuments');
		}
		
		$data = array(
			'doctype' => 'letterofdismissal',
			'docversion' => $row->docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => $docid
		);
		
		$fieldslist = BasedocumentFields::getDismissalLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getDismissalLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = BasedocumentFields::getDismissalLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('basedocuments.dismissal-letter-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'logo' => $logo
		]);
		
		$name = 'Letter of Dismissal '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
	
	//Letter of Employment
    public function letterofemploymentedit(Request $request, $docid){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$docversion = BasedocumentFields::getCurDocVersions('ooe');
		
		$data = array(
			'doctype' => 'offerofemployment',
			'docversion' => $docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		if ($docid > 0){
			$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'offerofemployment')->orderBy('id', 'desc')->first();
			if ($row){
				$new = false;
			}
		}
		
		$fieldslist = BasedocumentFields::getLetterOfEmploymentFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getLetterOfEmploymentShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		if ($new){		
			$data['fieldcontents']['f0'] = '';
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'XXX XXX';
			$data['fieldcontents']['f3'] = 'XXXXXX (X)';
			$data['fieldcontents']['f4'] = 'Naomi Budden';
			$data['fieldcontents']['f5'] = 'Director';
			$data['fieldcontents']['f6'] = 'naomi.budden@nest-property.com';
			$data['fieldcontents']['f7'] = 'XXX';
			$data['fieldcontents']['f8'] = 'e.g. Monday 22nd July, 2019';
			$data['fieldcontents']['f9'] = 'Consultant — Residential Search & Investments';
			$data['fieldcontents']['f10'] = '10am to 6pm';
			$data['fieldcontents']['f11'] = 'You are free to choose your own holidays and take days off as you see fit. The Company asks only that you inform management of any holiday leave at least two weeks in advance. We also ask that where possible you base your holidays around your client work-flow, and the natural ebb and flow of your client pipeline.';
			$data['fieldcontents']['f12'] = 'Consultant';
			$data['fieldcontents']['f13'] = 'her';
			$data['fieldcontents']['f14'] = "To provide full home-finding services to clients, as assigned by the Company, including orientations, school search advice, selection and viewing of properties during office and non-office hours, weekends and holidays, as requested by clients.  \r\nTo support corporate client accounts in the relocation of new staff from overseas to Hong Kong.  \r\nTo bring in new client accounts and generate business leads through marketing and networking.  \r\nTo carry out lease / sales negotiations, prepare offer letters, Tenancy Agreements / Provisional S&P Agreements and relevant documentation, working hand-in-hand with landlords / vendors and clients alike.  \r\nTo file all documents and paperwork [as required by law for three years from completion] in an orderly and appropriate manner for record filing \u2014 according to company policy.  \r\nTo assist clients with handover services, such as pre-handover inspections, utilities accounts set-up, handover of premises and follow-ups for defect rectifications, etc.  \r\nTo actively promote Nest Property and its services, and as such generate client leads and bring prospective new business to the Company, at all times maintaining and developing client relationships.  \r\nTo gather email addresses and details of prospective client leads in order to send out Electronic Data Messages for marketing purposes.  \r\nTo use, update and maintain the Nest Property website and database conscientiously and with accuracy, maintaining current records of vacant property listings, and uploading comments and corresponding property photos.  \r\nWhen in the office to handle phone enquiries from landlords / tenants and new client enquiries.  \r\nTo assist with photo shoots.  \r\n";
			$data['fieldcontents']['f15'] = "Use of Apple Mac Computer  \r\nUse of printer and fax facilities  \r\nA Nest Property e-mail account accessible through the Internet  \r\nUse of Online Agency CRM system & database  \r\nDocumentation / Forms / Marketing collateral and other Nest Property stationery  \r\nHot desk, and telephone line for local calls  \r\nInsurance for accident or injury within the office  \r\nCompany Professional Photographer & Graphic Designer to facilitate property shoots and electronic direct marketing (EDMs)  \r\nUse of Branded Company Car and Driver  \r\n";
			$data['fieldcontents']['f16'] = "Mobile phone and related expenses, e.g. SMS charges.  \r\nMedical and dental schemes.  \r\nOther insurance e.g. life, personal accident insurance etc.  \r\n";
			$data['fieldcontents']['f17'] = "Client referred by Nest Property - 35%  \r\nClient brought on of your own accord - 40%  \r\n";
			$data['id'] = 0;
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			$data['consultantid'] = $row->consultantid;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = BasedocumentFields::getLetterOfEmploymentReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('basedocuments.offer-of-employment', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields
		]);
    }
	
	public function letterofemploymentupdate(Request $request){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = BasedocumentFields::getLetterOfEmploymentFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'offerofemployment',
				'docversion' => $docversion,
				'consultantid' => intval($input['consultantid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			$docid = intval($input['id']);
			if ($docid == 0){
				//New
				$doc = Basedocument::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Basedocument::findOrFail($docid);
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if ($docid > 0){
			return redirect('/basedocuments/letterofemployment/'.$docid);
		}else{
			return redirect('/basedocuments');
		}
	}
	
	
    public function letterofemploymentpdf(Request $request, $docid, $logo = 'yes'){
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant'] && !$rights['Accountant'] && !$rights['Informationofficer']){
			return redirect('resources');
		}
		
		$row = Basedocument::where('id', '=', $docid)->where('doctype', '=', 'offerofemployment')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/basedocuments');
		}
		
		$data = array(
			'doctype' => 'offerofemployment',
			'docversion' => $row->docversion,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => $docid
		);
		
		$fieldslist = BasedocumentFields::getLetterOfEmploymentFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = BasedocumentFields::getLetterOfEmploymentShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = BasedocumentFields::getLetterOfEmploymentReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$consultant = User::where('id', $row->consultantid)->first();
		if ($logo == 'no') {
			$template = 'offer-of-employment-pdf-v1';
		} else {
			$template = 'offer-of-employment-pdf-v2';
		}
		
		$html = view('basedocuments.'.$template, [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'logo' => $logo,
			'consultant' => $consultant
		]);
		
		$name = 'Offer of Employment '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function getabc(){
		return array(
			0 => 'a',
			1 => 'b',
			2 => 'c',
			3 => 'd',
			4 => 'e',
			5 => 'f',
			6 => 'g',
			7 => 'h',
			8 => 'i',
			9 => 'j',
			10 => 'k',
			11 => 'l',
			12 => 'm',
			13 => 'n',
			14 => 'o',
			15 => 'p',
			16 => 'q',
			17 => 'r',
			18 => 's',
			19 => 't',
			20 => 'u',
			21 => 'v',
			22 => 'w',
			23 => 'x',
			24 => 'y',
			25 => 'z'
		);
	}
	
	
}
