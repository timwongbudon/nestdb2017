<?php

namespace App\Http\Controllers;

use App\Test;
use App\Property;
use App\District;
use Input;
use Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Building;
use App\PropertyMedia;
use App\PropertyPhoto;
use App\PropertyImageVariation;
use App\LogClick;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');

		$user = \Auth::user();

		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(4);

			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
		}
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$this->image_variation_for_featured();die;
        // $this->image_variation_for_print_test();die;
        // $this->image_variation_read_image_test();die;
        // $this->image_variation_watermark_test();die;
        // $this->image_display_test();die;
        // $this->download_image_from_url_test();die;
        // $this->image_download_get_local_first_url_test();die;
        // $this->save_image_variation();die;
        // $this->fix_districts();die;
        // $this->get_coordinates_test();die;
        // $this->fix_countries();die;
        // $this->excel_import_test();die;
        // $this->gen_images_test($request);die;
        // $this->watermark_test();die;
        // return $this->popup_test3();
        // return $this->popup_test2();
        // return $this->popup_test1();
        // $this->excel_test1();
        // return $this->dompdf_test3();die;
        // return $this->multiconnection();die;
        // $this->hongkonghome_photos_test();
        // $this->fix_district();
        // return $this->html_dom_test();

		/*$hex = array(
			0 => '0',
			1 => '1',
			2 => '2',
			3 => '3',
			4 => '4',
			5 => '5',
			6 => '6',
			7 => '7',
			8 => '8',
			9 => '9',
			10 => 'A',
			11 => 'B',
			12 => 'C',
			13 => 'D',
			14 => 'E',
			15 => 'F',
		);

		$html = '';

		for ($x = 0; $x < 16; $x=$x+5){
			for ($y = 0; $y < 16; $y=$y+5){
				for ($z = 0; $z < 16; $z=$z+5){
					$html .= '<div style="float:left;width:80px;height:20px;background:#B'.$hex[$x].'B'.$hex[$y].'B'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#B'.$hex[$x].'B'.$hex[$y].'B'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#B'.$hex[$x].'B'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#B'.$hex[$x].'B'.$hex[$y].'C'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#B'.$hex[$x].'C'.$hex[$y].'B'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#B'.$hex[$x].'C'.$hex[$y].'B'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#B'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#B'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'B'.$hex[$y].'B'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'B'.$hex[$y].'B'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'B'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'B'.$hex[$y].'C'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'C'.$hex[$y].'B'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'C'.$hex[$y].'B'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].'</div>';

					$html .= '<div style="float:left;width:80px;height:20px;background:#D'.$hex[$x].'D'.$hex[$y].'D'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#D'.$hex[$x].'D'.$hex[$y].'D'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#D'.$hex[$x].'D'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#D'.$hex[$x].'D'.$hex[$y].'C'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#D'.$hex[$x].'C'.$hex[$y].'D'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#D'.$hex[$x].'C'.$hex[$y].'D'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#D'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#D'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'D'.$hex[$y].'D'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'D'.$hex[$y].'D'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'D'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'D'.$hex[$y].'C'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'C'.$hex[$y].'D'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'C'.$hex[$y].'D'.$hex[$z].'</div>';
					$html .= '<div style="float:left;width:80px;height:20px;background:#C'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].';border:1px solid #000000;text-align:center;padding:10px;">#C'.$hex[$x].'C'.$hex[$y].'C'.$hex[$z].'</div>';

				}
			}
		}*/

    echo "Hello World";
  	//phpinfo('');
		exit;


		//return $html;

        /*return view('tests.index', [
            'tests' => $html,
        ]);*/
    }

    private function image_variation_for_featured() {
        $property_id = '4173 ';
        $type = 'other_photo';
        $download_url = 'http://192.168.1.115:8082/images/other-photo/p4173-52016-kam-fai-mansion-lr.jpg';
        // $download_url = 'http://localnestdb2016:8888/images/system/4173-featured.jpg';
        // $download_url = 'http://db.nest-property.com/cms/images/system/4173-featured.jpg';
        $photo_url = PropertyImageVariation::read_download_image($property_id, $type, $download_url);
        die($photo_url);
    }

    private function image_variation_for_print_test() {
        $property_id = '4173 ';
        $photo = Property::where('id', '4174')->first();
        pdump($photo->first_print_image());
        pdump($photo->second_print_image());
        pdump($photo->third_print_image());
        die;
    }

    private function image_variation_read_image_test() {
        $property_media_id = '9992';
        // $img = PropertyImageVariation::read_image($property_media_id, 'largexx');
        $img = PropertyImageVariation::read_image($property_media_id, 'large');
        // $img = PropertyImageVariation::read_image($property_media_id, 'thumbnail');
        header('Content-Type: image/jpg');
        echo $img->encode('jpg');die;
    }

    private function image_variation_watermark_test() {
        $property_media_id = '9992';
        $watermarked_image = PropertyImageVariation::watermark_image($property_media_id);
        header('Content-Type: image/jpg');
        echo $watermarked_image->encode('jpg');die;
        dex('end - ' . __FUNCTION__);
    }
    private function image_display_test() {
        // ref: http://image.intervention.io/use/http
        // create a new image resource
        // $img = Image::canvas(800, 600, '#ff00ff');
        // // send HTTP header and output image data
        // header('Content-Type: image/png');
        // echo $img->encode('png');die;

        $type = 'download';
        $storage_path = public_path();
        $directory = $type;
        $file_path = 'images' .'/'. $directory .'/';
        $img = Image::make($file_path . '15385-9992-kam-fai-mansion.jpg');
        // dex($img->width());
        header('Content-Type: image/jpg');
        echo $img->encode('jpg');die;
        dex('end - ' . __FUNCTION__);
    }

    private function image_download_get_local_first_url_test() {
        $property_id = '15385';
        $property_media_id = '9992';
        $image = PropertyMedia::where('id', $property_media_id)->with('original')->first();
        pdump([
                'url'        => $image->url,
                'original'   => $image->original?$image->original['url']:'',
                'test func.' => $image->get_local_first_url(),
            ]);
        pdump($image->toArray());
        dex('end - ' . __FUNCTION__);
    }

    private function download_image_from_url_test() {
        // $url = 'http://nest-property.com/wp-content/uploads/2016/08/1-1.jpg';
        $property_id = '15385';
        $property_media_id = '9992';
        // $status = PropertyImageVariation::download_from_url($property_id, $property_media_id ,true);
        $status = PropertyImageVariation::download_from_url($property_id, $property_media_id);
        die($status);
        // $property = Property::where('id', $property_id)->first();
        // $image = PropertyMedia::where('property_id', $property_id)->orderBy('order', 'asc')->first();
        // if (!empty($image) && !empty($property)) {
        //     $storage_path = public_path();
        //     $directory = 'download';
        //     $file_path = 'images' .'/'. $directory .'/';
        //     $url = $image->url;
        //     $img_ext = pathinfo($url, PATHINFO_EXTENSION);
        //     $file_name = sprintf('%d-%d-%s.%s', $image->property_id, $image->id, $property->property_slug(), $img_ext);
        //     $img = $file_path . $file_name;
        //     $status = file_put_contents($img, file_get_contents($url));

        //     $_img = Image::make($img);
        //     $_dimension = !empty($_img)?$_img->width() . 'x' . $_img->height():'';
        //     if (!empty($status)) {
        //         $PropertyImageVariation = new PropertyImageVariation;
        //         $image_variation = $PropertyImageVariation->create(array(
        //                 'property_media_id' => $property_id,
        //                 'url'               => url($file_path . $file_name),
        //                 'dimension'         => $_dimension,
        //                 'format'            => 'download',
        //             ));
        //     }
        // }
        // dex('end - ' . __FUNCTION__);
    }

    private function save_image_variation() {
        $PropertyImageVariation = new PropertyImageVariation;
        $image_variation = $PropertyImageVariation->create(array(
                'property_media_id' => '15101',
                'url'               => 'http://nest-property.com/wp-content/uploads/2016/08/1-1.jpg',
                'dimension'         => '800x600',
                'format'            => 'large',
            ));
        dex($image_variation->toArray());
    }

    private function fix_districts() {
        // $districts = District::get();
        $districts = Building::district_ids();
        // dex($districts);
        $District = new District();
        // $building = $District->createDistrict('1', 'abc');
        foreach ($districts as $key => $value) {
            $district = $District->createDistrict($key, $value);
            pdump($district);
        }
        die;
    }

    private function fix_countries() {
        $country_ids = Building::country_ids();
        $res = array();
        foreach ($country_ids as $key => $value) {
            $res[strtolower($key)] = $value;
        }
        foreach ($res as $key => $value) {
            printf("\"%s\"=>\"%s\",\n", $key, $value);
        }
        die;
    }

    private function get_coordinates_test() {
        pdump(__FUNCTION__);
        $buildings = Building::where('lng','')->orderBy('id', 'desc')->get();
        foreach ($buildings as $key => $building) {
            // $building->fix_address1();
            $_address = $building->shorten_address();
            if (!preg_match('/hong kong/', $_address)) {
                $_address1 = $_address . ', Hong Kong';
            } else {
                $_address1 = $_address;
            }

            // pdump([$building->shorten_name(),
            //     $building->address1,
            //     $_address1,
            //     $building->district(),
            //     $building->toArray()]);
            // die;

            $map_info = Building::get_coordinates($_address1);
            if (!empty($map_info)) {
                pdump([$building->id, $map_info]);
                $building->lat = $map_info['lat'];
                $building->lng = $map_info['lng'];
                $status = $building->save();
            }
        }
        die(microtime(true) - LARAVEL_START);

        // pdump(Building::get_coordinates('Peak Road 99-103, The Peak, Hong Kong')); //22.2656211,114.1525413
        // pdump(Building::get_coordinates('233 Hollywood Road, Sheung Wan, Hong Kong'));
        die;
    }

    private function excel_import_test() {
        // die(__FUNCTION__);
        $file = storage_path('excel') . '/sample-import-excel.csv';
        \Excel::load($file, function($reader) {
            // Getting all results
            $results = $reader->all();
            $fields = $this->excel_import_test_get_fields($results);
            $this->excel_import_test_remove_empty_line($results, $fields[0]);
            // get first line.
            pdump($results[1]);

            // dex(count($results));
            // dex($results);
            echo (microtime(true) - LARAVEL_START);
            die;
        });
    }

    private function excel_import_test_get_fields($results) {
        if (empty($results)) return array();
        $res = array();
        foreach ($results[0] as $key => $value) {
            $res[] = $key;
        }
        return $res;
    }

    private function excel_import_test_remove_empty_line(&$results, $field) {
        if (empty($results)) return array();
        foreach ($results as $key => $row) {
            if(empty($row->$field)) {
                unset($results[$key]);
            }
        }
    }

    private function gen_images_test(Request $request) {
        pdump('gen image test start.');
        $storage_path = public_path();
        $directory = 'nest-photo';
        $file_path = 'images' .'/'. $directory .'/';

        $properties_query = Property::FrontendSearch($request)
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*'))
                        ->with('owner')->with('agent')//->with('allreps')
                        ->with('building')
                        ->with('indexed_photo')
                        ->orderBy('properties.id','desc');
        $properties = $properties_query->paginate(2);

        // dex($properties->toArray());
        foreach($properties as $property):
            foreach ($property->nest_photos as $key => $photo) {
                $this->gen_single_image($photo->url, $storage_path, $file_path);
            }
        endforeach;
        die;
        // $property = Property::where('id', '3265')->with('nest_photos')->first();
        // if (empty($property)) {
        //     die('empty property');
        // }
        // foreach ($property->nest_photos as $key => $photo) {
        //     $this->gen_single_image($photo->url, $storage_path, $file_path);
        // }
        // die;
    }

    private function gen_single_image($url, $storage_path, $file_path) {
        $info = pathinfo($url);
        $filename = $info['basename'];
        $watermark_ratio = 0.50;
        // dex($filename);

        // save thumb
        Image::make($url)->fit(300,300)->save($storage_path .'/'. $file_path . 'test/' . 'thumb-'.$filename);

        // add watermark
        $img = Image::make($url);
        $logo_width = $img->width() * $watermark_ratio;
        $watermark = Image::make($storage_path.'/nestproperty-logo-white.png')->resize($logo_width, null, function ($constraint) {$constraint->aspectRatio();});
        $img->insert($watermark, 'center');
        $img->save($storage_path .'/'. $file_path . 'test/' . $filename);
    }

    private function watermark_test() {
        pdump('water mark test start.');

        $storage_path = public_path();
        $directory = 'nest-photo';
        $file_path = 'images' .'/'. $directory .'/';

        $filename = 'the-summa-sample.jpg';
        $img = Image::make(sprintf($storage_path .'/'. $file_path . '%s', $filename));

        // save thumb
        Image::make(sprintf($storage_path .'/'. $file_path . '%s', $filename))->fit(300,300)->save($storage_path .'/'. $file_path .'test-thumb.jpg');

        // $size = $img->filesize();
        pdump([$img->width(), $img->height()]);

        // $logo_width = $img->width() * 0.65;
        $logo_width = $img->width() * 0.50;
        pdump($logo_width);

        $watermark = Image::make($storage_path.'/nestproperty-logo-white.png')->resize($logo_width, null, function ($constraint) {$constraint->aspectRatio();});
        // $watermark = Image::make($storage_path.'/watermark.png')->resize($logo_width, null, function ($constraint) {$constraint->aspectRatio();});
        $img->insert($watermark, 'center');
        // $img->insert($storage_path.'/watermark.png', 'center');
        $img->save($storage_path .'/'. $file_path .'test.jpg');

        dex('water mark test end.');
        // $img = Image::make(sprintf($storage_path .'/'. $file_path . '%s', $filename));
    }

    private function hongkonghome_photos_test() {
        $url = 'http://hongkonghomes.com/en/property/rent/sai_kung/che_keng_tuk_road/80522/info';
        $PropertyPhoto = new PropertyPhoto;
        $PropertyPhoto->insert_property_hongkonghome_photos(14963, $url);
        dex('done');
    }

    public function fix_district() {
        echo "<PRE>";
        $building = Building::district_ids();
        ksort($building);
        // $building['201'] = 'Ma On Shan';
        // $building['202'] = 'Sham Tseng';
        $building['203'] = 'Tuen Mun';
        $building['204'] = 'Kowloon City';
        die(json_encode($building));
        pdump($building);
        die;
    }

    private function html_dom_test() {
        $html = str_get_html('<div id="hello">Hello</div><div id="world">World</div>');
        $html->find('div', 1)->class = 'bar';
        $html->find('div[id=hello]', 0)->innertext = 'foo';
        echo $html; // Output: <div id="hello">foo</div><div id="world" class="bar">World</div>
        die;
    }

    private function multiconnection() {
        $data = DB::connection('mysql2')->table('wp_posts')->limit(10)->get();
        dex($data);
    }

    private function popup_test3() {
        return view('tests.popup3');
    }

    public function popup_test3_ajax() {
        die('This is some text in a paragraph.');
    }

    private function popup_test2() {
        return view('tests.popup2');
    }

    private function popup_test1() {
        return view('tests.popup1');
    }

    private function excel_test1() {

        // http://www.maatwebsite.nl/laravel-excel/docs/import
        $data = Test::get()->toArray();
        // dex($data);
        \Excel::create('Laravel Excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
        // \Excel::create('Laravel Excel', function($excel) {
        //     $excel->sheet('Excel sheet', function($sheet) {
        //         $sheet->setOrientation('landscape');
        //     });
        // })->export('xls');
    }

    private function dompdf_test3() {
        $html = view('tests.dompdf');
        return \PDF::loadHTML($html)->setPaper('a5', 'landscape')->setWarnings(false)->stream();
    }

    private function dompdf_test2() {
        $data = array();
        $pdf = \PDF::loadView('pdf.invoice', $data);
        return $pdf->download('invoice.pdf');
    }

    private function dompdf_test1() {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->stream();
    }

    private function db_query_test() {
        DB::enableQueryLog();
        $tests = Test::get();
        echo "<PRE>";
        var_dump($tests->toArray());
        var_dump(DB::getQueryLog());
        die;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "<PRE>";
        // var_dump($request->toArray());
        // dex($request->toArray());

        // if ($request->hasFile('image')) {
        //     $img = Image::make($request->file('image'));
        //     $file = \NestImage::saveImage($img, 'building-photo');
        // }
        // dex($path);

        //Display File Name
        $file = $request->file('image');

        //Display File Name
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';

        //Display File Extension
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';
        //Display File Real Path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';
        //Display File Size
        echo 'File Size: '.$file->getSize();
        echo '<br>';
        //Display File Mime Type
        echo 'File Mime Type: '.$file->getMimeType();
        //Move Uploaded File
        $destinationPath = 'images';
        $file->move($destinationPath,$file->getClientOriginalName());

        die;

        $this->validate($request, [
            'name'   => 'required|max:255',
            'amount' => 'numeric',
        ]);

        $request->user()->tests()->create([
            'name' => $request->name,
        ]);

        return redirect('/tests');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
