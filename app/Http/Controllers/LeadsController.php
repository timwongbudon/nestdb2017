<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Mail;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\ClientShortlist;
use App\Property;
use App\User;
use App\Document;
use App\Documentinfo;
use App\Lead;
use App\ClientViewing;
use App\Commission;
use App\Client;
use App\FormContact;
use App\FormProperty;
use App\Calendar;
use App\Clientdocuments;
use App\Targets;
use App\Commcons;
use App\Infoofficermessage;
use App\PropertyMedia;
use App\PropertyPhoto;

use App\LogLogin;
use App\LogProperties;
use App\LogLead;
use App\LogDocuments;
use App\LogClick;


use Carbon\Carbon;

use View;

class LeadsController extends Controller
{

    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');

		if(!\Auth::check()){
			$redirect->to('/login')->send();
		}
		$headerdata = Lead::headerdata();
		View::share('headerdata', $headerdata);

		LogClick::logClick(1);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		$user = \Auth::user();
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		return redirect('/dashboardnew');
    }

	public function home(Request $request){
		$user = $request->user();

		if ($user->getUserRights()['Accountant'] && !$user->getUserRights()['Consultant']){
			return redirect('/commission/invoicing');
		}else if (isset($user->getUserRights()['Driver']) && $user->getUserRights()['Driver']){
			return redirect('/calendar');
		}else if (isset($user->getUserRights()['Photographer']) && $user->getUserRights()['Photographer']){
			return redirect('/calendar');
		}else if (isset($user->getUserRights()['Informationofficer']) && $user->getUserRights()['Informationofficer']){
			return redirect('/calendar');
		}else if ($user->getUserRights()['Superadmin']){
			return redirect('/companyoverview');
		}/*else if ($user->getUserRights()['Director'] && !$user->getUserRights()['Consultant']){
			return redirect('/companyoverview');
		}*/
		else if ($user->getUserRights()['Director']){
			return redirect('/companyoverview');
		}else{
			return redirect('/dashboardnew');
		}
	}

    public function dashboard(Request $request, $consultantid = 0, $year = 0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if ($year == 0){
			$year = intval(date('Y'));
		}

		if ($consultantid == 0){
			$consultantid = $user->id;
		}
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}else if (isset($user->getUserRights()['Photographer']) && $user->getUserRights()['Photographer']){
			return redirect('/calendar');
		}

		if (!$user->getUserRights()['Superadmin'] && !$user->getUserRights()['Director'] && !$user->getUserRights()['Accountant']){
			if ($consultantid != $user->id){
				return redirect('/dashboardnew');
			}
		}

	
		$leads = Lead::where('status', '!=', 7)->where('status', '!=', 8)->where('status', '!=', 11)
		->where('consultantid', '=', $consultantid)
		//->orwhereRaw("( JSON_CONTAINS(CONVERT( shared_with,  JSON), '[".'"'.$consultantid.'"'."]') )")		
		->orwhereRaw("( shared_with like '%".'"'.$consultantid.'"'."%' )")		
		->with('client')
		->orderBy('created_at', 'DESC')
		->get();
		//->toSql();
		//dd($leads);


		//$sql = "SELECT * FROM `leads` WHERE (`status` != 7 and `status` != 8 and `status` != 11 and `consultantid` = 41 or JSON_CONTAINS(CONVERT( shared_with,  JSON), '[]') ) and `leads`.`deleted_at` is null order by `created_at` desc";
		//$leads = DB::select(DB::raw($sql));
		

		$offerings = Lead::where('status', 5)
		->where('consultantid', '=', $consultantid)->with('client')
		//->orwhereRaw("( JSON_CONTAINS(CONVERT( shared_with,  JSON), '[".'"'.$consultantid.'"'."]') and status = 5 )")		
		->orwhereRaw("( shared_with like '%".'"'.$consultantid.'"'."%' and status = 5 )")		
		->groupBy('clientid')
		->get();
		
		$completed = Lead::where('status', '=', 7)
		->where('consultantid', '=', $consultantid)
		//->orwhereRaw("( JSON_CONTAINS(CONVERT( shared_with,  JSON), '[".'"'.$consultantid.'"'."]') and status = 7 )")		
		->orwhereRaw("( shared_with like '%".'"'.$consultantid.'"'."%' and status = 7 )")		
		->with('client')
		->orderBy('updated_at', 'desc')->limit(10)->get();

		$dead = Lead::where('status', '=', 8)
		->where('consultantid', '=', $consultantid)
		//->orwhereRaw("( JSON_CONTAINS(CONVERT( shared_with,  JSON), '[".'"'.$consultantid.'"'."]') and status = 8 )")		
		->orwhereRaw("( shared_with like '%".'"'.$consultantid.'"'."%' and status = 8 )")		
		->with('client')
		->orderBy('updated_at', 'desc')
		->limit(10)->get();

		$clientids = array();

		$shortlists = array();
		$commshortids = array();
		$offhortids = array();
		foreach ($leads as $l){
			$shortlists[] = $l->shortlistid;
			if ($l->status == 6){
				$commshortids[] = $l->shortlistid;
			}
			if ($l->status == 5 && $l->type == 1){
				$offhortids[] = $l->shortlistid;
			}
			if (isset($l['client']) && !empty($l['client']->id)){
				$clientids[] = $l['client']->id;
			}
		}
		foreach ($completed as $l){
			$commshortids[] = $l->shortlistid;
		}

		$clientpics = array();
		$pics = Clientdocuments::wherein('clientid', $clientids)->where('doctype', 'picture')->get();
		if (!empty($pics) && count($pics) > 0){
			foreach ($pics as $p){
				$clientpics[$p->clientid] = $p->id;
			}
		}

		$viewings = array();
		$handoverdates = array();
		if ($consultantid > 0){
			$dateto = strtotime("+28 day", time());
			$viewings = ClientViewing::where('v_date', '>=', date('Y-m-d'))->wherein('shortlist_id', $shortlists)->where('status', '=', 0)->where('v_date', '<=', date('Y-m-d', $dateto))
				->with('client')->with('shortlist')
				->orderBy('v_date')->orderBy('ampm')->orderBy('hour')->orderBy('minute')->limit(10)->get();
			$hod = Calendar::where('type', 1)->where('leadid', '!=', 'null')->where('datefrom', '>', date('Y-m-d', (time()-1210000)))->get();
			foreach ($hod as $h){
				$handoverdates[$h->leadid] = $h->datefrom;
			}
		}
		$times = Calendar::getTimesArray();
		$tarr = array_flip($times);
		for ($i = 0; $i < count($viewings); $i++){
			for ($a = $i+1; $a < count($viewings); $a++){
				if ($viewings[$i]->v_date == $viewings[$a]->v_date){
					if (isset($tarr[$viewings[$i]->timefrom]) && isset($tarr[$viewings[$a]->timefrom]) && $tarr[$viewings[$i]->timefrom] > $tarr[$viewings[$a]->timefrom]){
						$help = $viewings[$i];
						$viewings[$i] = $viewings[$a];
						$viewings[$a] = $help;
					}
				}
			}
		}

		$calendar = Calendar::where('user_id', $consultantid)->where('datefrom', '>=', date('Y-m-d'))->orderBy('datefrom', 'asc')->get();
		$calendarleadids = array();
		for ($i = 0; $i < count($calendar); $i++){
			if ($calendar[$i]->type == 1 && $calendar[$i]->leadid > 0){
				$calendarleadids[] = $calendar[$i]->leadid;
			}
			for ($a = $i+1; $a < count($calendar); $a++){
				if ($calendar[$i]->datefrom == $calendar[$a]->datefrom){
					if ($calendar[$i]->timefrom == null || (isset($tarr[$calendar[$i]->timefrom]) && isset($tarr[$calendar[$a]->timefrom]) && $tarr[$calendar[$i]->timefrom] > $tarr[$calendar[$a]->timefrom])){
						$help = $calendar[$i];
						$calendar[$i] = $calendar[$a];
						$calendar[$a] = $help;
					}
				}
			}
		}
		$calendarleadsres = Lead::wherein('id', $calendarleadids)->with('client')->get();
		$calendarleads = array();
		if (count($calendarleadsres) > 0){
			foreach ($calendarleadsres as $cl){
				$calendarleads[$cl->id] = $cl;
			}
		}

		$commissions = array();
		if (count($commshortids) > 0){
			$commresult = Commission::wherein('shortlistid', $commshortids)->get();
			if (count($commresult) > 0){
				foreach ($commresult as $cr){
					$commissions[$cr->shortlistid] = $cr;
				}
			}
		}

		$offers = array();
		if (count($offhortids) > 0){
			foreach ($offhortids as $oid){
				$offresults = Document::where('shortlistid', '=', $oid)->where('doctype', '=', 'offerletter')->orderBy('updated_at', 'desc')->orderBy('revision', 'desc')->first();
				if ($offresults){
					$offers[$oid] = $offresults;
				}
			}
		}

		$consultant = $user;
		if ($consultantid != $user->id){
			foreach ($users as $u){
				if ($u->id == $consultantid){
					$consultant = $u;
				}
			}
		}
		$isconsultant = false;
		if ($consultant->getUserRights()['Consultant']){
			$isconsultant = true;
		}

		$fulltargets = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'quarterdollar' => 0,
			'quarterdeals' => 0,
			'bianndollar' => 0,
			'bianndeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);
		$fulldone = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'quarterdollar' => 0,
			'quarterdeals' => 0,
			'bianndollar' => 0,
			'bianndeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);
		$fullpercent = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'quarterdollar' => 0,
			'quarterdeals' => 0,
			'bianndollar' => 0,
			'bianndeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);

		$monthlyvalues = array(
			'dollardone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollartarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealsdone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealstarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		);

		$target = Targets::where('year', $year)->where('consultantid', $consultantid)->first();

		$month = intval(date('m'))-1;
		$quarter = array(0, 1, 2);
		$biann = array(0, 1, 2, 3, 4, 5);
		if ($month > 8){
			$quarter = array(9, 10, 11);
		}else if ($month > 5){
			$quarter = array(6, 7, 8);
			$biann = array(6, 7, 8, 9, 10, 11);
		}else if ($month > 2){
			$quarter = array(3, 4, 5);
			$biann = array(6, 7, 8, 9, 10, 11);
		}

		if ($target){
			$monthlylease = json_decode($target->monthlylease, true);
			$monthlyleasedeals = json_decode($target->monthlyleasedeals, true);

			//Month
			if (isset($monthlylease[$month])){
				$fulltargets['monthdollar'] = $monthlylease[$month];
			}
			if (isset($monthlyleasedeals[$month])){
				$fulltargets['monthdeals'] = $monthlyleasedeals[$month];
			}

			//Quarter
			foreach ($quarter as $m){
				if (isset($monthlylease[$m])){
					$fulltargets['quarterdollar'] += $monthlylease[$m];
				}
				if (isset($monthlyleasedeals[$m])){
					$fulltargets['quarterdeals'] += $monthlyleasedeals[$m];
				}
			}

			//Biann
			foreach ($biann as $m){
				if (isset($monthlylease[$m])){
					$fulltargets['bianndollar'] += $monthlylease[$m];
				}
				if (isset($monthlyleasedeals[$m])){
					$fulltargets['bianndeals'] += $monthlyleasedeals[$m];
				}
			}

			//Annual
			for ($m = 0; $m < 12; $m++){
				if (isset($monthlylease[$m])){
					$fulltargets['anndollar'] += $monthlylease[$m];
				}
				if (isset($monthlyleasedeals[$m])){
					$fulltargets['anndeals'] += $monthlyleasedeals[$m];
				}
				$monthlyvalues['dollartarget'][$m] += $monthlylease[$m];
				$monthlyvalues['dealstarget'][$m] += $monthlyleasedeals[$m];
			}

			//Sales
			$fulltargets['saledollar'] = $target->currentsale;
			$fulltargets['saledeals'] = $target->currentsaledeals;
		}

		//Commission
		$comms = Commcons::select(array('*', DB::raw('MONTH(commissiondate) as commmonth')))->where('consultantid', $consultantid)->whereRaw('YEAR(commissiondate) = '.$year)->get();

		if (count($comms) > 0){
			foreach ($comms as $comm){
				$commmonth = $comm->commmonth-1;

				if ($comm->salelease == 1){
					if ($month == $commmonth){
						$fulldone['monthdollar'] += $comm->commissionconsultant;
						$fulldone['monthdeals']++;
					}
					if (in_array($commmonth, $quarter)){
						$fulldone['quarterdollar'] += $comm->commissionconsultant;
						$fulldone['quarterdeals']++;
					}
					if (in_array($commmonth, $biann)){
						$fulldone['bianndollar'] += $comm->commissionconsultant;
						$fulldone['bianndeals']++;
					}

					$fulldone['anndollar'] += $comm->commissionconsultant;
					$fulldone['anndeals']++;
				}else{
					$fulldone['saledollar'] += $comm->commissionconsultant;
					$fulldone['saledeals']++;
				}

				$monthlyvalues['dollardone'][$commmonth] += $comm->commissionconsultant;
				
				//check if the deal is split comm
				/*$t = Commcons::where('commissionid','=', $comm->commissionid);
				if($t->count() > 1 ){
					$val = 1 / $t->count();
					$monthlyvalues['dealsdone'][$commmonth] = number_format($monthlyvalues['dealsdone'][$commmonth] + $val,2);
				} else {
					$monthlyvalues['dealsdone'][$commmonth]++;
				}*/

				$NumComm = 1;
				$commCount = $this->getCommissionCount($comm->propertyid,$comm->shortlistid,$comm->commissiondate);
				
				if($commCount > 1){
					$NumComm = 1 / $commCount;

					$NumComm = 1 / $commCount;
					$ttal = $monthlyvalues['dealsdone'][$commmonth]  + $NumComm;					
					
					$monthlyvalues['dealsdone'][$commmonth] = strpos($ttal,".")  ? number_format($ttal,1) : $ttal;
				} else {
					$monthlyvalues['dealsdone'][$commmonth]++;
				}

				
			}
		}
		$monthcount = 0;
		$dollarsum = 0;
		$dealsum = 0;
		for ($m = 0; $m < 12 && $m < (intval(date('m'))-1); $m++){
			if ($monthlyvalues['dollartarget'][$m] > 0){
				$monthcount++;
			}
		}
		for ($m = 0; $m < 12; $m++){
			$monthlyvalues['dollartarget'][$m] = $monthlyvalues['dollartarget'][$m] - $monthlyvalues['dollardone'][$m];
			if ($m < (intval(date('m'))-1)){
				$dollarsum += $monthlyvalues['dollardone'][$m];
			}
			if ($monthlyvalues['dollartarget'][$m] < 0){
				$monthlyvalues['dollartarget'][$m] = 0;
			}
			$monthlyvalues['dealstarget'][$m] = $monthlyvalues['dealstarget'][$m] - $monthlyvalues['dealsdone'][$m];
			if ($m < (intval(date('m'))-1)){
				$dealsum += $monthlyvalues['dealsdone'][$m];
			}
			if ($monthlyvalues['dealstarget'][$m] < 0){
				$monthlyvalues['dealstarget'][$m] = 0;
			}
		}
		if ($monthcount > 0){
			$dollarsum = round($dollarsum / $monthcount);
			$dealsum = round($dealsum / $monthcount, 2);
		}else{
			$dealsum = 0;
			$dollarsum = 0;
		}

		//Percent
		foreach ($fullpercent as $key => $value){
			if ($fulltargets[$key] <= $fulldone[$key]){
				$fullpercent[$key] = 100;
			}else if ($fulldone[$key] <= 0){
				$fullpercent[$key] = 0;
			}else{
				$fullpercent[$key] = floor($fulldone[$key]/$fulltargets[$key]*100);
			}
		}

		$query = Commission::with('client')->with('property')->with('shortlist');
		$query->where('status', '<', 10);
		$query->where('consultantid', '=', $consultantid);
        $opencommissions = $query->orderBy('status', 'asc')->orderBy('id', 'desc')->paginate(20);

		///for outstanding payments
		if ($year == 0){
			$year = intval(date('Y'));
		}

		$years = array();
		for ($i = intval(date('Y')); $i >= 2017; $i--){
			$years[] = $i;
		}

		//Current Year
		$invoices = Commission::select('commission.id as cid','shortlistid','clientid','consultantid','propertyid', 'tenantpaiddate','landlordpaiddate', 'landlordamount', 'tenantamount',
		'properties.name as property',
		'users.firstname as cnfirstname', 
							'users.lastname as cnlastname',
							'clients.firstname as cfirstname', 
							'clients.lastname as clastname',
		 DB::raw('MONTH(commissiondate) as cmonth'), 
		 DB::raw('MONTH(landlordpaiddate) as lmonth'), 
		 DB::raw('MONTH(tenantpaiddate) as tmonth'))->whereRaw('YEAR(commissiondate) = '.intval($year))
		 ->where('consultantid','=', $consultantid)
		->leftjoin('users','commission.consultantid', '=', 'users.id')
		->leftjoin('clients','commission.clientid', '=', 'clients.id')
		->leftjoin('properties','commission.propertyid', '=', 'properties.id')
		 ->get();

		$outstandingpayment = 0;
		$outstandingpaymentcnt = 0;

		$data = array();

		foreach ($invoices as $inv){
			
			//Landlord
			if ($inv->lmonth > 0){
			
				
			}else{			

				$offerLetter = $this->getLatestOffers($inv->shortlistid, $inv->propertyid);
				//print_r($offerLetter->fieldcontents);

				if(isset($offerLetter->fieldcontents)){
					$inv->startdate = json_decode($offerLetter->fieldcontents)->f16;					
				}
			
				if($inv->landlordamount  > 0){
				//if($inv->landlordamount > 0 &&  !empty($inv->landlordpaiddate) && $inv->landlordpaiddate !='0000-00-00'){
					$data[$inv->cid] = $inv;				
				}
			
			}

			//Tenant
			if ($inv->tmonth > 0){
			
			}else{			

				$offerLetter = $this->getLatestOffers($inv->shortlistid, $inv->propertyid);
				//print_r($offerLetter->fieldcontents);

				if(isset($offerLetter->fieldcontents)){
					$inv->startdate = json_decode($offerLetter->fieldcontents)->f16;					
				}				
				
				if($inv->tenantamount > 0){
				//if($inv->tenantamount > 0 && (empty($inv->tenantpaiddate) || $inv->tenantpaiddate !='0000-00-00' )){				
					$data[$inv->cid] = $inv;					
				}
			
				
			}
		}

		$data1 = collect($data)->sortBy('startdate',false)->toArray();
		$data1 = collect($data1)->sortBy('startdate')->reverse()->toArray();
		///END for outstanding payments

		return view('dashboardnew', [
			'offerings' => $offerings,
			'leads' => $leads,
			'completed' => $completed,
			'dead' => $dead,
			'users' => $users,
			'consultantid' => $consultantid,
			'viewings' => $viewings,
			'commissions' => $commissions,
			'offers' => $offers,
			'clientpics' => $clientpics,
			'isconsultant' => $isconsultant,
			'fulltargets' => $fulltargets,
			'fulldone' => $fulldone,
			'fullpercent' => $fullpercent,
			'monthlyvalues' => $monthlyvalues,
			'dollarsum' => $dollarsum,
			'dealsum' => $dealsum,
			'monthcount' => $monthcount,
			'year' => $year,
			'opencommissions' => $opencommissions,
			'handoverdates' => $handoverdates,
			'calendar' => $calendar,
			'calendarleads' => $calendarleads,
			'outstandingpayments' => $data1
		]);
	}

	public function companyoverview(Request $request, $year = 0){
		if( $request->get('from') !== null && $request->get('to') !== null ){
			$year = "?from=".$request->get('from').'&to='.$request->get('to');
		}
	
		return view('companyoverview',[
			'year' => $year,
		]);

	}
    public function companyoverview_ajx(Request $request, $year = 0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();
		
		
		
		if ($year == 0){
			$year = intval(date('Y'));
		}

		if (!($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant']) || $user->getUserRights()['Admin']){
			//return redirect('/dashboardnew');
		}

		$dealscounted = array();

		$conscurmonth = array();
		$conscuryear = array();

		$fulltargets = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);
		$fulldone = array(
			'monthdollar' => 0,
			'monthdollarcomp' => 0,
			'monthdeals' => 0,
			'anndollar' => 0,
			'anndollarcomp' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledollarcomp' => 0,
			'saledeals' => 0,
		);
		$fullpercent = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);

		$monthlyvalues = array(
			'dollardone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollardonecons' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollardonecomp' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollartarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealsdone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealstarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'payments' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'paymentsoutstanding' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
		);

		if($year >= intval(2021)){
			$targets = Targets::where('year', $year)
			->where('consultantid', '!=', 7) //exclude william		
			->where('consultantid', '!=', 6) //exclude naomi		
			->get();
			
		} else {			
			$targets = Targets::where('year', $year)	
			->get();
		}
		

		$month = intval(date('m'))-1;
		$maxmonth = $month;
		if ($year < intval(date('Y'))){
			$maxmonth = 12;
		}

		$consultant_count = 0;
		foreach ($targets as $target){
			if ($target){
				$consultantid = $target->consultantid;
				
				$monthlylease = json_decode($target->monthlylease, true);
				$monthlyleasedeals = json_decode($target->monthlyleasedeals, true);

				//Month
				if (isset($monthlylease[$month])){
					$fulltargets['monthdollar'] += $monthlylease[$month];
				}
				if (isset($monthlyleasedeals[$month])){
					$fulltargets['monthdeals'] += $monthlyleasedeals[$month];
				}

				//Annual
				for ($m = 0; $m < 12; $m++){
					if (isset($monthlylease[$m])){
						$fulltargets['anndollar'] += $monthlylease[$m];
					}
					if (isset($monthlyleasedeals[$m])){
						$fulltargets['anndeals'] += $monthlyleasedeals[$m];
					}
					$monthlyvalues['dollartarget'][$m] += $monthlylease[$m];
					$monthlyvalues['dealstarget'][$m] += $monthlyleasedeals[$m];
				}

				//Sales
				$fulltargets['saledollar'] += $target->currentsale;
				$fulltargets['saledeals'] += $target->currentsaledeals;
			}

			//Commission
			$comms = Commcons::select(array('*', DB::raw('MONTH(commissiondate) as commmonth')))->where('consultantid', $consultantid)->whereRaw('YEAR(commissiondate) = '.$year)->get();

			if (count($comms) > 0){
				foreach ($comms as $comm){
					$commmonth = $comm->commmonth-1;

					if (!isset($dealscounted[$comm->commissionid])){
						$dealscounted[$comm->commissionid] = 1;

						if ($comm->salelease == 1){
							if ($month == $commmonth){
								$fulldone['monthdollar'] += $comm->commissionconsultant;
								$fulldone['monthdollarcomp'] += $comm->payout;
								$fulldone['monthdeals']++;
							}

							$fulldone['anndollar'] += $comm->commissionconsultant;
							$fulldone['anndollarcomp'] += $comm->payout;
							$fulldone['anndeals']++;
						}else{
							$fulldone['saledollar'] += $comm->commissionconsultant;
							$fulldone['saledollarcomp'] += $comm->payout;
							$fulldone['saledeals']++;
						}

						$monthlyvalues['dollardone'][$commmonth] += $comm->commissionconsultant;
						$monthlyvalues['dollardonecons'][$commmonth] += $comm->payout;
						$monthlyvalues['dollardonecomp'][$commmonth] += ($comm->commissionconsultant - $comm->payout);
						$monthlyvalues['dealsdone'][$commmonth]++;
					}else{
						if ($comm->salelease == 1){
							if ($month == $commmonth){
								$fulldone['monthdollar'] += $comm->commissionconsultant;
								$fulldone['monthdollarcomp'] += $comm->payout;
							}

							$fulldone['anndollar'] += $comm->commissionconsultant;
							$fulldone['anndollarcomp'] += $comm->payout;
						}else{
							$fulldone['saledollar'] += $comm->commissionconsultant;
							$fulldone['saledollarcomp'] += $comm->payout;
						}

						$monthlyvalues['dollardone'][$commmonth] += $comm->commissionconsultant;
						$monthlyvalues['dollardonecons'][$commmonth] += $comm->payout;
						$monthlyvalues['dollardonecomp'][$commmonth] += ($comm->commissionconsultant - $comm->payout);
					}

					if ($month == $commmonth){
						if (isset($conscurmonth[$comm->consultantid])){
							$conscurmonth[$comm->consultantid] += $comm->commissionconsultant;
						}else{
							$conscurmonth[$comm->consultantid] = $comm->commissionconsultant;
						}
					}
					if (isset($conscuryear[$comm->consultantid])){
						$conscuryear[$comm->consultantid] += $comm->commissionconsultant;
					}else{
						$conscuryear[$comm->consultantid] = $comm->commissionconsultant;
					}
				}
			}

			$consultant_count++;
		}

		$dollarsum = 0;
		$dealsum = 0;
		for ($m = 0; $m < 12; $m++){
			$monthlyvalues['dollartarget'][$m] = $monthlyvalues['dollartarget'][$m] - $monthlyvalues['dollardone'][$m];
			if ($m < $maxmonth){
				$dollarsum += $monthlyvalues['dollardone'][$m];
			}
			if ($monthlyvalues['dollartarget'][$m] < 0){
				$monthlyvalues['dollartarget'][$m] = 0;
			}
			$monthlyvalues['dealstarget'][$m] = $monthlyvalues['dealstarget'][$m] - $monthlyvalues['dealsdone'][$m];
			if ($m < $maxmonth){
				$dealsum += $monthlyvalues['dealsdone'][$m];
			}
			if ($monthlyvalues['dealstarget'][$m] < 0){
				$monthlyvalues['dealstarget'][$m] = 0;
			}
		}
		if ($maxmonth > 0){
			$dollarsum = round($dollarsum / $maxmonth);
			$dealsum = round($dealsum / $maxmonth, 2);
		}else{
			$dealsum = 0;
			$dollarsum = 0;
		}

		//Percent
		foreach ($fullpercent as $key => $value){
			if ($fulltargets[$key] <= $fulldone[$key]){
				$fullpercent[$key] = 100;
			}else if ($fulldone[$key] <= 0){
				$fullpercent[$key] = 0;
			}else{
				$fullpercent[$key] = floor($fulldone[$key]/$fulltargets[$key]*100);
			}
		}

		$years = array();
		for ($i = intval(date('Y')); $i >= 2017; $i--){
			$years[] = $i;
		}

		//Current Year
		$invoices = Commission::select('landlordpaiddate', 'landlordpaiddate', 'landlordamount', 'tenantamount', DB::raw('MONTH(commissiondate) as cmonth'), DB::raw('MONTH(landlordpaiddate) as lmonth'), DB::raw('MONTH(tenantpaiddate) as tmonth'))->whereRaw('YEAR(commissiondate) = '.intval($year))->get();

		$outstandingpayment = 0;
		$outstandingpaymentcnt = 0;

		$outstandingpaymentcntCurYear = 0;
		$outstandingpaymentCurYear = 0;

		foreach ($invoices as $inv){
			//Landlord
			if ($inv->lmonth > 0){
				$monthlyvalues['payments'][$inv->lmonth-1] += $inv->landlordamount;
			}else{
				$monthlyvalues['paymentsoutstanding'][$inv->cmonth-1] += $inv->landlordamount;
				$outstandingpayment += $inv->landlordamount;
				$outstandingpaymentcnt++;

				if($inv->landlordamount > 0){
					$outstandingpaymentCurYear += $inv->landlordamount;
					$outstandingpaymentcntCurYear++;
				}
			}
			//Tenant
			if ($inv->tmonth > 0){
				$monthlyvalues['payments'][$inv->tmonth-1] += $inv->tenantamount;
			}else{
				$monthlyvalues['paymentsoutstanding'][$inv->cmonth-1] += $inv->tenantamount;
				$outstandingpayment += $inv->tenantamount;
				$outstandingpaymentcnt++;

				if($inv->tenantamount > 0){
					$outstandingpaymentCurYear += $inv->tenantamount;
					$outstandingpaymentcntCurYear++;
				}
			}
		}

		
		//Past Year
		$pastyear = $year-1;
		$invoices = Commission::select('landlordpaiddate', 'landlordpaiddate', 'landlordamount', 'tenantamount',
		 DB::raw('MONTH(commissiondate) as cmonth'), 
		 DB::raw('MONTH(landlordpaiddate) as lmonth'),
		 DB::raw('YEAR(tenantpaiddate) as tyear'),
		 DB::raw('YEAR(landlordpaiddate) as lyear'),
		 DB::raw('MONTH(tenantpaiddate) as tmonth'))
		 ->whereRaw('YEAR(commissiondate) = '.intval($pastyear))->get();

		$lastoutstandingpayment = 0;
		$lastoutstandingpaymentcnt = 0;

		foreach ($invoices as $inv){
			//Landlord
			if ($inv->lmonth > 0){
				if ($inv->lyear == $year){
					$monthlyvalues['payments'][$inv->lmonth-1] += $inv->landlordamount;
				}
			}else{
				$lastoutstandingpayment += $inv->landlordamount;
				$lastoutstandingpaymentcnt++;
				$outstandingpayment += $inv->landlordamount;
				$outstandingpaymentcnt++;
			}
			//Tenant
			if ($inv->tmonth > 0){
				if ($inv->tyear == $year){
					$monthlyvalues['payments'][$inv->tmonth-1] += $inv->tenantamount;
				}
			}else{
				$lastoutstandingpayment += $inv->tenantamount;
				$lastoutstandingpaymentcnt++;
				$outstandingpayment += $inv->tenantamount;
				$outstandingpayment++;
			}
		}

		foreach($monthlyvalues['paymentsoutstanding'] as $key=>$d){
			
			$val = $monthlyvalues['payments'][$key] - $d;			
			$monthlyvalues['chartlivepayments'][$key] = $val < 0 ? 0 : $val;
			//$monthlyvalues['payments'][$key] = $monthlyvalues['payments'][$key] - 131750  - 131750;

		}

		//OVerride outstandingpayment and outstandingpayment. Current year only
		$outstandingpayment = $outstandingpaymentCurYear;
		$outstandingpaymentcnt = $outstandingpaymentcntCurYear;

		//Consultants
		$usersbyid = array();
		foreach ($users as $u){
			$usersbyid[$u->id] = $u;
		}

		//Consultants per Year
		if (intval(date('Y')) == $year){
			$daytoday = intval(date('z'))+1;
		}else{
			$daytoday = 365;
		}
		$daycounter = 0;
		$consultantdays = 0;

		echo '<!-- '.PHP_EOL;
		foreach ($users as $u){
			if ($u->isConsultant() && $u->id != 6){
				if ($u->startdate != null && $u->startdate != '0000-00-00'){
					$startdate = date_create_from_format('Y-m-d', $u->startdate);
					$consultantdays = 0;

					if (intval($startdate->format('Y')) < $year){
						if ($u->enddate == null || $u->enddate == '0000-00-00'){
							$daycounter += $daytoday;
							$consultantdays = $daytoday;
						}else if ($u->enddate > $year.'-12-31'){
							$daycounter += 365;
							$consultantdays = 365;
						}else{
							$enddate = date_create_from_format('Y-m-d', $u->enddate);
							if ($enddate->format('Y') == $year){
								$daycounter += intval($enddate->format('z')) + 1;
								$consultantdays = intval($enddate->format('z')) + 1;
							}
						}
					}else if (intval($startdate->format('Y')) == $year){
						if ($u->enddate == null || $u->enddate == '0000-00-00'){
							$daycounter += $daytoday - intval($startdate->format('z')) - 1;
							$consultantdays = $daytoday - intval($startdate->format('z')) - 1;
						}else if ($u->enddate > $year.'-12-31'){
							$daycounter += 365 - intval($startdate->format('z')) - 1;
							$consultantdays = 365 - intval($startdate->format('z')) - 1;
						}else{
							$enddate = date_create_from_format('Y-m-d', $u->enddate);
							if ($enddate->format('Y') == $year){
								$daycounter += intval($enddate->format('z')) - intval($startdate->format('z'));
								$consultantdays = intval($enddate->format('z')) - intval($startdate->format('z'));
							}
						}
					}

					echo $u->name.' '.$consultantdays.PHP_EOL;

				}
			}
		}
		echo ' -->'.PHP_EOL;

		//$colors = array('#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000');

		$conscurmonthout = array();
		$cnt = 0;
		foreach ($conscurmonth as $uid => $c){
			$conscurmonthout[] = array(
				'uid' => $uid,
				'username' => $usersbyid[$uid]->name,
				'amount' => $c,
				'color' => '#'.$usersbyid[$uid]->color,
			);
			$cnt++;
		}
		for ($i = 0; $i < count($conscurmonthout)-1; $i++){
			for ($a = $i+1; $a <  count($conscurmonthout); $a++){
				if ($conscurmonthout[$i]['amount'] < $conscurmonthout[$a]['amount']){
					$help = $conscurmonthout[$i];
					$conscurmonthout[$i] = $conscurmonthout[$a];
					$conscurmonthout[$a] = $help;
				}
			}
		}

		$conscuryearout = array();
		$cnt = 0;
		foreach ($conscuryear as $uid => $c){
			$conscuryearout[] = array(
				'uid' => $uid,
				'username' => $usersbyid[$uid]->name,
				'amount' => $c,
				'color' => '#'.$usersbyid[$uid]->color,
			);
			$cnt++;
		}
		for ($i = 0; $i < count($conscuryearout)-1; $i++){
			for ($a = $i+1; $a <  count($conscuryearout); $a++){
				if ($conscuryearout[$i]['amount'] < $conscuryearout[$a]['amount']){
					$help = $conscuryearout[$i];
					$conscuryearout[$i] = $conscuryearout[$a];
					$conscuryearout[$a] = $help;
				}
			}
		}

		$leads = Lead::select(array('*', DB::raw('MONTH(created_at) as leadmonth')))->whereRaw('YEAR(created_at) = '.$year)->get();

		$leadstotal = array(
			'new' => 0,
			'offering' => 0,
			'invoicing' => 0,
			'hot' => 0,
			'active' => 0,
			'stored' => 0,
			'total' => 0
		);

		$leadsdeadvscomplete = array(
			'dead' => 0,
			'complete' => 0,
			'total' => 0
		);
		$leadsdeadvscompletebysource = array();
		$leadstotalbysource = array();
		foreach (Lead::allsources() as $key => $source){
			$leadstotalbysource[$key] = array(
				'new' => 0,
				'offering' => 0,
				'invoicing' => 0,
				'hot' => 0,
				'active' => 0,
				'stored' => 0,
				'total' => 0
			);
			$leadsdeadvscompletebysource[$key] = array(
				'dead' => 0,
				'complete' => 0,
				'total' => 0
			);
		}
		$leadstotalbysource[0] = array(
			'new' => 0,
			'offering' => 0,
			'invoicing' => 0,
			'hot' => 0,
			'active' => 0,
			'stored' => 0,
			'total' => 0
		);
		$leadsdeadvscompletebysource[0] = array(
			'dead' => 0,
			'complete' => 0,
			'total' => 0
		);

		if (count($leads) > 0){
			foreach ($leads as $lead){
				if ($lead->status == 0){
					$leadstotal['new']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['new']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 1){
					$leadstotal['hot']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['hot']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 2 || $lead->status == 3){
					$leadstotal['active']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['active']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 5){
					$leadstotal['offering']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['offering']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 6){
					$leadstotal['invoicing']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['invoicing']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 7){
					$leadsdeadvscomplete['complete']++;
					$leadsdeadvscomplete['total']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['complete']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 8){
					$leadsdeadvscomplete['dead']++;
					$leadsdeadvscomplete['total']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['dead']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 9 || $lead->status == 10 || $lead->status == 12){
					$leadstotal['stored']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['stored']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}
			}
		}

		//Bubble charts
		$bubble1 = array();
		$bubble2 = array();
		$lead1 = array();
		$lead2 = array();
		$b1offer = array();
		$b2offer = array();
		$maxy = 0;
		$maxx1 = 0;
		$maxx2 = 0;
		
		if( $request->get('from') !== null && $request->get('to') !== null ){
			
			$vr = ClientViewing::whereBetween('v_date', [$request->get('from'), $request->get('to')])->get();
		} else {
			$vr = ClientViewing::whereYear('v_date', '=', $year)->whereNotIn('user_id', [31,28,8,15,13,14])->get();
			//[31,28,8,15,13,14] exclude former information offer
		}

		if (count($vr) > 0){
			foreach ($vr as $v){
				if (!isset($bubble1[$v->user_id])){
					$bubble1[$v->user_id] = array(
						'viewings' => 1,
						'offers' => 0,
						'invoices' => 0,
						'commission' => 0
					);
				}else{
					$bubble1[$v->user_id]['viewings']++;
				}
			}
		}

		if( $request->get('from') !== null && $request->get('to') !== null ){
			$or = Document::where('doctype', '=', 'offerletter')->whereNotIn('consultantid', [31,28,8,15,13,14])->whereBetween('created_at', [$request->get('from'), $request->get('to')])->get();
			//[31,28,8,15,13,14] exclude former information offer
		} else {			
			$or = Document::where('doctype', '=', 'offerletter')->whereNotIn('consultantid', [31,28,8,15,13,14])->whereYear('created_at', '=', $year)->get();
		}		
		
		if (count($or) > 0){
			foreach ($or as $o){				
				if (!isset($b1offer[$o->shortlistid])){
					if (!isset($bubble1[$o->consultantid])){
						$bubble1[$o->consultantid] = array(
							'viewings' => 0,
							'offers' => 1,
							'invoices' => 0,
							'commission' => 0
						);
					}else{
						$bubble1[$o->consultantid]['offers']++;
					}
					if ($bubble1[$o->consultantid]['offers'] > $maxy){
						$maxy = $bubble1[$o->consultantid]['offers'];
					}
					if ($bubble1[$o->consultantid]['viewings'] > $maxx1){
						$maxx1 = $bubble1[$o->consultantid]['viewings'];
					}
					$b1offer[$o->shortlistid] = 1;
				}
			}
		}

		if( $request->get('from') !== null && $request->get('to') !== null ){
			$ir = Commcons::whereBetween('commissiondate', [$request->get('from'), $request->get('to')])->get();
		} else {
			$ir = Commcons::whereYear('commissiondate', '=', $year)->get();
		}
		
		if (count($ir) > 0){
			foreach ($ir as $i){
				if (!isset($bubble1[$i->consultantid])){
					$NumComm = 1;
					$commCount = $this->getCommissionCount($i->propertyid,$i->shortlistid,$i->commissiondate);
					
					if($commCount > 1){
						$NumComm = 1 / $commCount;
					}

					$bubble1[$i->consultantid] = array(
						'viewings' => 0,
						'offers' => 0,
						'invoices' => number_format($NumComm,1),
						'commission' => $i->commissionconsultant
					);
				}else{

					$commCount = $this->getCommissionCount($i->propertyid,$i->shortlistid,$i->commissiondate);
					if($commCount > 1){
						$NumComm = 1 / $commCount;
						$ttal = $bubble1[$i->consultantid]['invoices'] + $NumComm;
						$bubble1[$i->consultantid]['invoices'] = strpos($ttal,".")  ? number_format($ttal,1) : $ttal;
					} else {
						$bubble1[$i->consultantid]['invoices']++;
					}				

					$bubble1[$i->consultantid]['commission'] += $i->commissionconsultant;
				}
			}
		}

		//2
		$mindate = date('Y-m-d', strtotime('-90 days'));

		$vr = ClientViewing::where('v_date', '>=', $mindate)->get();
		if (count($vr) > 0){
			foreach ($vr as $v){
				if (!isset($bubble2[$v->user_id])){
					$bubble2[$v->user_id] = array(
						'viewings' => 1,
						'offers' => 0,
						'invoices' => 0,
						'commission' => 0
					);
				}else{
					$bubble2[$v->user_id]['viewings']++;
				}
			}
		}

		$or = Document::where('doctype', '=', 'offerletter')->where('created_at', '>=', $mindate)->get();
		if (count($or) > 0){
			foreach ($or as $o){
				if (!isset($b2offer[$o->shortlistid])){
					if (!isset($bubble2[$o->consultantid])){
						$bubble2[$o->consultantid] = array(
							'viewings' => 0,
							'offers' => 1,
							'invoices' => 0,
							'commission' => 0
						);
					}else{
						$bubble2[$o->consultantid]['offers']++;
					}
					if ($bubble2[$o->consultantid]['offers'] > $maxy){
						$maxy = $bubble2[$o->consultantid]['offers'];
					}
					if ($bubble2[$o->consultantid]['viewings'] > $maxx2){
						$maxx2 = $bubble2[$o->consultantid]['viewings'];
					}
					$b2offer[$o->shortlistid] = 1;
				}
			}
		}

		$ir = Commcons::where('commissiondate', '>=', $mindate)->get();
		if (count($ir) > 0){
			foreach ($ir as $i){
				if (!isset($bubble2[$i->consultantid])){
					$bubble2[$i->consultantid] = array(
						'viewings' => 0,
						'offers' => 0,
						'invoices' => 1,
						'commission' => $i->commissionconsultant
					);
				}else{
					
					$commCount = $this->getCommissionCount($i->propertyid,$i->shortlistid,$i->commissiondate);
					if($commCount > 1){
						$NumComm = 1 / $commCount;
						$ttal = $bubble2[$i->consultantid]['invoices'] + $NumComm;						

						$bubble2[$i->consultantid]['invoices'] = strpos($ttal,".")  ? number_format($ttal,1) : $ttal;
					} else {
						$bubble2[$i->consultantid]['invoices']++;
					}
					
					$bubble2[$i->consultantid]['commission'] += $i->commissionconsultant;
				}
			}
		}

		$maxy += 15;
		$maxy = round($maxy/10)*10;

		if (count($bubble1) > 0){
			foreach ($bubble1 as $id => $b){
				$b['id'] = $id;
				//$lead1[] = $b;
			}
		}
		if (count($bubble2) > 0){
			foreach ($bubble2 as $id => $b){
				$b['id'] = $id;
				//$lead2[] = $b;
			}
		}
		for ($i = 0; $i < count($lead1)-1; $i++){
			for ($a = $i+1; $a < count($lead1); $a++){
				if ($lead1[$i]['commission'] < $lead1[$a]['commission']){
					$help = $lead1[$i];
					$lead1[$i] = $lead1[$a];
					$lead1[$a] = $help;
				}
			}
		}
		for ($i = 0; $i < count($lead2)-1; $i++){
			for ($a = $i+1; $a < count($lead2); $a++){
				if ($lead2[$i]['commission'] < $lead2[$a]['commission']){
					$help = $lead2[$i];
					$lead2[$i] = $lead2[$a];
					$lead2[$a] = $help;
				}
			}
		}
		
		$maxx1 = floor($maxx1/10+1)*10+10;
		$maxx2 = floor($maxx2/10+1)*10+10;

		//override lead2
		foreach ($bubble2 as $id => $b){
			$b['id'] = $id;
			 $b['offers'] =$this->thisconsultant($id,$year,false);
			 
			$lead2[] = $b;
		}

		//override lead1
		$tempbubble1 = $bubble1;
		foreach ($tempbubble1 as $id => $b){
			$b['id'] = $id;
			if( $request->get('from') !== null && $request->get('to') !== null ){	
				
				$aveRental_offers_count = $this->aveRental_offers_count($id,$year,true);
				//$b['offers'] =$this->thisconsultant($id,$year,true);
				//$daterange = date_format( $request->get('from'),'YYYY-mm-dd')." - ".$request->get('to');
				$b['offers'] =$aveRental_offers_count['offers_count'];
				$daterange = Carbon::createFromFormat('Y-m-d', $request->get('from'))->format('d.m.y')." - ".Carbon::createFromFormat('Y-m-d', $request->get('to'))->format('d.m.y');
				//$b['averental'] =$this->getAveRental($id,$request->get('from')."/".$request->get('to'),true);
				$b['averental'] =$aveRental_offers_count['ave_rental'];

				$bubble1[$id]['offers'] = $b['offers'];
			} else {
				$aveRental_offers_count = $this->aveRental_offers_count($id,$year,true);
				$b['offers'] =$aveRental_offers_count['offers_count'];
				$b['averental'] =$aveRental_offers_count['ave_rental'];
				//$b['offers'] =$this->thisconsultant($id,$year,true);
				//$b['averental'] =$this->getAveRental($id,$year,false);
				
				$bubble1[$id]['offers'] = $b['offers'];
			}
			if($id != 52){
				$lead1[] = $b;
			}
		}		
		
		
		$lead1 = collect($lead1)->sortBy('commission')->reverse()->toArray();
		$lead2 = collect($lead2)->sortBy('commission')->reverse()->toArray();

		//remove naomi and william in the bubble chart for 2021 52=shelley					
		if( $request->get('from') == null && $request->get('to') == null  && $year==2021){				
			unset($bubble1[7]);
			unset($bubble1[6]);
			unset($bubble1[52]);
		}

		if( $request->get('from') !== null && $request->get('to') !== null ){
			if(  date('Y',strtotime($request->get('from')) ) == 2021 && date('Y',strtotime($request->get('to'))) == 2021 ) {
				$templead1 = $lead1;
				unset($lead1);
				foreach($templead1 as $lead){
					if($lead['id'] !== 7 && $lead['id'] !== 6 ){
						$lead1[] = $lead;
					}
				}			
				unset($bubble1[7]);
				unset($bubble1[6]);		
				unset($bubble1[52]);						
			}
		}

		
		

		return view('leads.companyoverview_ajx', [
			'users' => $users,
			'fulltargets' => $fulltargets,
			'fulldone' => $fulldone,
			'fullpercent' => $fullpercent,
			'monthlyvalues' => $monthlyvalues,
			'dollarsum' => $dollarsum,
			'dealsum' => $dealsum,
			'monthcount' => $maxmonth,
			'year' => $year,
			'years' => $years,
			'lastoutstandingpaymentcnt' => $lastoutstandingpaymentcnt,
			'lastoutstandingpayment' => $lastoutstandingpayment,
			'outstandingpaymentcnt' => $outstandingpaymentcnt,
			'outstandingpayment' => $outstandingpayment,
			'usersbyid' => $usersbyid,
			'conscurmonthout' => $conscurmonthout,
			'conscuryearout' => $conscuryearout,
			'leadstotal' => $leadstotal,
			'leadsdeadvscomplete' => $leadsdeadvscomplete,
			'leadsdeadvscompletebysource' => $leadsdeadvscompletebysource,
			'leadstotalbysource' => $leadstotalbysource,
			'leadsources' => Lead::allsources(),
			'daytoday' => $daytoday,
			'daycounter' => $daycounter,
			'bubble1' => $bubble1,
			'bubble2' => $bubble2,
			'maxy' => $maxy,
			'maxx1' => $maxx1,
			'maxx2' => $maxx2,
			'lead1' => isset($lead1) ? $lead1 : array(),
			'lead2' => $lead2,	
			'from' =>  $request->get('from') !== null ? $request->get('from') : "",
			'to' => $request->get('to') !== null ? $request->get('to') : "",
			'daterange' => isset($daterange) ? $daterange : null,
			'consultant_count' => $consultant_count,
		
			
		]);
	}

	
	public function getCommissionCount($propid, $shortlistid, $commissiondate){
		$ir = Commcons::where('shortlistid','=',$shortlistid)				
		->where('propertyid','=',$propid)
		->where('commissiondate','=',$commissiondate)
		->count();

			return $ir;
	}

	public function getAveRental($consultantid,$year = 2021,$daterange){
		
		/*
		//$consultantid =4;

				$offers = Document::select(
					'users.firstname as cnfirstname', 
						'users.lastname as cnlastname',
						'properties.name as property',
						'properties.id as propertyid',
						'properties.asking_rent',
						'documents.created_at',
						'documents.revision',
						'documents.shortlistid',
						'documents.fieldcontents',
						'leads.id as leadsid',
						'commission.id as commissionid'
						)
			->leftjoin('users','documents.consultantid', '=', 'users.id')
			->leftjoin('properties','documents.propertyid', '=', 'properties.id')
			//->leftjoin('commission','documents.shortlistid', '=', 'commission.shortlistid')
			//->leftjoin('leads','documents.shortlistid', '=', 'leads.shortlistid')
			->leftjoin('leads', function($query){
				$query->on('leads.shortlistid','=','documents.shortlistid')				
				->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
			})
			->leftjoin('commission', function($query){
				$query->on('commission.shortlistid','=','documents.shortlistid')				
				->on('commission.propertyid', '=', 'documents.propertyid' );					
			})
		//	->groupBy('documents.shortlistid')								
			->orderBy('documents.created_at', 'desc');
			

		if($daterange == true){
		//$offers->where("documents.created_at",">",date("Y-m-d",time()-3600*24*90));
			$date = explode("/",$year);
			$from = $date[0];
			$to = $date[1];
			
			$days90 = date("Y-m-d",time()-3600*24*90);
			$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at BETWEEN "'.$from.'" AND "'.$to.'"  GROUP BY documents.shortlistid)');
			$daterange = false;
		} else {
			//$offers->whereYear('documents.created_at', '=', $year);	
			$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at like "%'.$year.'%"  GROUP BY documents.shortlistid)');			
			$daterange = true;
		}

		

		$offers = $offers->paginate(25);

		
		$o=0;
		foreach($offers as $offer){

			
			$offersv2 = Document::select('documents.*',
										'properties.name as property',										
										'properties.asking_rent',										
										'users.firstname as cnfirstname', 
								'users.lastname as cnlastname',
								'clients.firstname as cfirstname', 
								'clients.lastname as clastname',
								'commission.commissiondate as commissiondate',
								'leads.id as leadsid',
								'leads.status as status'
								)
						->where('doctype','=',"offerletter")
						->where('documents.shortlistid','=',$offer->shortlistid)
						//->where('documents.consultantid','=',$consultantid)				
					//	->where('documents.id','=',$consultantid)		
					//	->where(DB::raw('YEAR(documents.created_at)'), '=', $year)
						->leftjoin('properties','documents.propertyid', '=', 'properties.id')
						->leftjoin('users','documents.consultantid', '=', 'users.id')
						->leftjoin('clients','documents.clientid', '=', 'clients.id')
						//->leftjoin('commission','documents.shortlistid', '=', 'commission.shortlistid')
						->leftjoin('leads', function($query){
							$query->on('leads.shortlistid','=','documents.shortlistid')				
							->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
						})
						->leftjoin('commission', function($query){
							$query->on('commission.shortlistid','=','documents.shortlistid')				
							->on('commission.propertyid', '=', 'documents.propertyid' );
							//->on('commission.id', '=', DB::raw("(select max(id) from commission WHERE commission.shortlistid = documents.shortlistid)"));
						})
						
						->orderBy('documents.revision', 'desc')
						->orderBy('clients.firstname', 'ASC')
						->groupby('propertyid')
						->get();

			$data = array();

			//get check and get invoice data
			$invc = Commission::where('shortlistid','=', $offer->shortlistid);
			if($invc->exists()){
				$fa = $invc->first();
				$fullamount = $fa->fullamount;
			}


			
						
			$count = 0;
			foreach($offersv2 as $offerv2){

				$getlatestOffer = Document::selectRaw('fieldcontents')
					->where('documents.shortlistid','=',$offer->shortlistid)
					->where('documents.propertyid','=',$offerv2->propertyid)
					->where('documents.doctype','=','offerletter')
					->orderBy('documents.revision', 'desc')
					->skip(0)->take(1)
					->get();
					
				foreach($getlatestOffer as $testt){					
					$latestOffer = $testt->fieldcontents;
				}

				//check if this the one with invoice
				if(!$count==0 AND !isset($fullamount)){
					
				} else {
					$fullamount = json_decode($latestOffer)->f13;
				}
				$count++;

					
				//$data[$offerv2->propertyid] = array(
					$data[] = array(
					'cnfirstname' => $offerv2->cnfirstname,
					'cnlastname' => $offerv2->cnlastname,
					'cfirstname' => $offerv2->cfirstname,
					'clastname' => $offerv2->clastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $latestOffer,
					'asking_rent'=> $offerv2->asking_rent,
					'offerprice' => $fullamount,
					'leads'=> $offerv2->leadsid,
					'commissiondate'=> $offerv2->commissiondate,
					'propertyid'=> $offerv2->propertyid,
					'property'=> $offerv2->property,
					'leadsid'=> $offerv2->leadsid,
					'status'=> $offerv2->status,
					
				);
			}
		
			//no data on $offersv2 get original data
			if(count($offersv2) == 0){
				$fullamount = json_decode($offer->fieldcontents)->f13;
				$data[] = array(
					'cnfirstname' => $offer->cnfirstname,
					'cnlastname' => $offer->cnlastname,
					'cfirstname' => $offer->cfirstname,
					'clastname' => $offer->clastname,
					'fieldcontents' => $offer->fieldcontents,
					'offerprice' => $fullamount,
					'asking_rent'=> $offer->asking_rent,
					'leads'=> $offer->leadsid,
					'commissiondate'=> $offer->commissiondate,
					'propertyid'=> $offer->propertyid,
					'property'=> $offer->property,
					'leadsid'=> $offer->leadsid,
					'status'=> $offer->status,
				);
			}
		

			$data2[$offer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

			

		}


		$totaloffer = 0;
		$aveRental = 0;
		$x = 0;
		if(isset($data2)){
			foreach ($data2 as  $key=> $offerrs){
				foreach ($offerrs as $offer){
					if(!empty($offer['commissiondate']) ){
						$totaloffer = $totaloffer +  $offer['offerprice'];
						$x++;
					}
				}
			}
			
			if($totaloffer != 0 ){
				$aveRental = $totaloffer / $x;
			} 
			

			//return $aveRental;
		} else {
			//return 0;
		}
		*/

		$totaloffer = 0;
		$aveRental = 0;
		$x = 0;

		$from = request('from', $default = null);
		$to = request('to', $default = null);
		$to = date('Y-m-d',strtotime($to . ' +1 day'));

		$docuffers = Document::groupBy('shortlistid')
		->groupBy('clientid')
		->groupBy('propertyid')				
		->orderBy('documents.created_at', 'desc');
	

	
			if($from !== null && $to !== null ){								
				//$docuffers->whereBetween('created_at', [$request->get('from') , $request->get('to') ]);
				$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and `created_at` between '".$from."' and '".$to."'");
			} else {			
				//$docuffers->whereRaw("(shared_with like '%"4"%' or consultantid=4) and (doctype='offerletter' and YEAR(created_at) = 2021)");
				$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and YEAR(created_at) = ".$year." ");
			
				//$docuffers->whereYear('created_at', '=',$year);
			}
	
		

		$docuffers = $docuffers->get();
		
	
		$data2 = array();
		$count = 0;
		$data = array();

		foreach($docuffers as $docoffer){

			$docufferss2 = Document::where('doctype','=','offerletter')
			->where('documents.shortlistid','=',$docoffer->shortlistid)
			->groupBy('shortlistid')
			->groupBy('clientid')
			->groupBy('propertyid')				
			->orderBy('documents.created_at', 'desc');
			

		

			$docufferss2 = $docufferss2->get();

			$data = array();

			foreach($docufferss2 as $docofferr){

				$client = Client::where('id','=',$docofferr->clientid)->first();		
				$user = USER::where('id','=',$docofferr->consultantid)->first();		
				$property = Property::where('id','=',$docofferr->propertyid)->first();	
				$comm = Commission::where('shortlistid','=',$docofferr->shortlistid)->where('propertyid','=',$docofferr->propertyid)->first();
				$commissiondate = !empty($comm->commissiondate) && $comm->commissiondate!=='0000-00-00' && $comm->commissiondate!=='' ? date('d.m.Y',strtotime($comm->commissiondate)) : "-";
				

				$fieldcontents = $docofferr->fieldcontents;

				//$lead = DB::select(DB::raw("(select max(id) as id from leads WHERE leads.shortlistid = ".$docofferr->shortlistid.")"))->first();
				$lead = Lead::where('shortlistid','=',$docofferr->shortlistid)->first();

			

		
				$data[] = array(
					'cnfirstname' => $user->firstname,
					'cnlastname' => $user->lastname,
					'cfirstname' => $client->firstname,
					'clastname' => $client->lastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $fieldcontents,
					'asking_rent'=>  !empty($property->asking_rent) ? $property->asking_rent : 0,
					'offerprice' => !empty(json_decode($fieldcontents)->f13) ? json_decode($fieldcontents)->f13 : 0,
					//'leads'=> $offerv2->leadsid,
					'commissiondate'=> $commissiondate,
					'propertyid'=> $docofferr->propertyid,
					'property'=> !empty($property->name) ? $property->name : '-',
					'leadsid'=> !empty($lead->id) ? $lead->id : '#',
					'status'=>  !empty($lead ->status) ? $lead ->status : '#',
				);

				
			}

			

			$data2[$docoffer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();



			
		}

		$prevKey=0;
		foreach($data2 as  $key=> $offerrs ){

			foreach ($offerrs as $offer){
				if($key != $prevKey){
					$prevKey  = $key;
					if(!empty($offer['commissiondate']) && $offer['commissiondate']!=='-' && $offer['status'] !== 8){
						$totaloffer = $totaloffer +  $offer['offerprice'];
						$x++;
					}
				}
			}

		}

		if($totaloffer != 0 ){
			$aveRental = $totaloffer / $x;
		} 

		return $aveRental;
		

	
	}




	public function thisconsultant($consultantid,$year,$yearlist){

	/*	$days90 = date("Y-m-d",time()-3600*24*90);

		$offers = Document::where('doctype', '=', 'offerletter')
				->where('documents.consultantid', '=', $consultantid)
				->select(
				'users.firstname as cnfirstname', 
				'users.lastname as cnlastname',
				'properties.name as property',
				'properties.id as propertyid',
				'properties.asking_rent',
				'documents.created_at',
				'documents.revision',
				'documents.shortlistid',
				'documents.fieldcontents',
				'leads.id as leadsid'
				)
				->leftjoin('users','documents.consultantid', '=', 'users.id')
				->leftjoin('properties','documents.propertyid', '=', 'properties.id')				
				->leftjoin('leads', function($query){
					$query->on('leads.shortlistid','=','documents.shortlistid')				
					->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
				})			
				->leftjoin('commission', function($query){
					$query->on('commission.shortlistid','=','documents.shortlistid')				
					->on('commission.propertyid', '=', 'documents.propertyid' );					
				})					
				->orderBy('documents.created_at', 'desc');
			//	->where("documents.created_at",">",date("Y-m-d",time()-3600*24*90))			
			//	->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at > "'.$days90.'"  GROUP BY documents.shortlistid)')
			//	->get();
		
		if($yearlist == false){
			
			//$offers->where("documents.created_at",">",date("Y-m-d",time()-3600*24*90));		
						
			$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at >= "'.$days90.'"  GROUP BY documents.shortlistid)');
			
			
		} else {
			//$offers->whereYear('documents.created_at', '=', $year);
			//$offers->where("documents.created_at",">",date("Y-m-d",time()-3600*24*90));		
			$from = request('from', $default = null);
			$to = request('to', $default = null);
			
			
			if($from !== null){				
				$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at between "'.$from.'" AND "'.$to.'"   GROUP BY documents.shortlistid)');			
			} else {				
				$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at like "%'.$year.'%"  GROUP BY documents.shortlistid)');						
			}
			
			
		}
		$offers = $offers->get();


		return count($offers); */


		$from = request('from', $default = null);
		$to = request('to', $default = null);
		$to = date('Y-m-d',strtotime($to . ' +1 day'));

		$docuffers = Document::groupBy('shortlistid')
		->groupBy('clientid')
		->groupBy('propertyid')				
		->orderBy('documents.created_at', 'desc');
	

	
			if($from !== null && $to !== null ){								
				//$docuffers->whereBetween('created_at', [$request->get('from') , $request->get('to') ]);
				$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and `created_at` between '".$from."' and '".$to."'");
			} else {			
				//$docuffers->whereRaw("(shared_with like '%"4"%' or consultantid=4) and (doctype='offerletter' and YEAR(created_at) = 2021)");
				$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and YEAR(created_at) = ".$year." ");
			
				//$docuffers->whereYear('created_at', '=',$year);
			}
	
		

		$docuffers = $docuffers->get();
		
	
		$data2 = array();
		$count = 0;
		$data = array();

		foreach($docuffers as $docoffer){


			$docufferss = Document::whereYear('created_at', '=',$year)->where('doctype','=','offerletter')
			->where('documents.shortlistid','=',$docoffer->shortlistid)
			->groupBy('shortlistid')
			->groupBy('clientid')
			->groupBy('propertyid')				
			->orderBy('documents.created_at', 'desc')
			->get();

			$data = array();

			foreach($docufferss as $docofferr){

				$client = Client::where('id','=',$docofferr->clientid)->first();		
				$user = USER::where('id','=',$docofferr->consultantid)->first();		
				$property = Property::where('id','=',$docofferr->propertyid)->first();	
				$comm = Commission::where('shortlistid','=',$docofferr->shortlistid)->where('propertyid','=',$docofferr->propertyid)->first();
				$commissiondate = !empty($comm->commissiondate) ? date('d.m.Y',strtotime($comm->commissiondate)) : "-";

				$fieldcontents = $docofferr->fieldcontents;

				//$lead = DB::select(DB::raw("(select max(id) as id from leads WHERE leads.shortlistid = ".$docofferr->shortlistid.")"))->first();
				$lead = Lead::where('shortlistid','=',$docofferr->shortlistid)->first();

			

		
				$data[] = array(
					'cnfirstname' => $user->firstname,
					'cnlastname' => $user->lastname,
					'cfirstname' => $client->firstname,
					'clastname' => $client->lastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $fieldcontents,
					'asking_rent'=> isset($property->asking_rent) && !empty($property->asking_rent) ? $property->asking_rent : '',
					'offerprice' => json_decode($fieldcontents)->f13,
					//'leads'=> $offerv2->leadsid,
					'commissiondate'=> $commissiondate,
					'propertyid'=> $docofferr->propertyid,
					'property'=> isset($property->name) && !empty($property->name) ? $property->name : '',
					'leadsid'=> !empty($lead->id) ? $lead->id : '#',
					'status'=>  isset($lead ->status) && !empty($lead ->status) ? $lead ->status : '#',
					'shared_with' =>$docoffer->shared_with
				);

			
					
				$data2[$docoffer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();	
			}

			$prevKey=0;
			$count = 0;
			foreach($data2 as  $key=> $offerrs ){

				foreach ($offerrs as $offer){
					
					if($key != $prevKey){
						$prevKey  = $key;
						
						
						if( count(json_decode($offer['shared_with'])) <= 0){
							$count++;
						} else {
							 $other_consultant = count(json_decode($offer['shared_with']));
							 if($other_consultant == 1){
								$t = 0.5;
							} else {
								$t = round(1 / (1 + $other_consultant),2);
							}

							$count = $count  + $t;

						}
					}
				}
	
			}
		
			
			/*if(!isset($data2[$docoffer->shortlistid])){
				if( $docoffer->shared_with == ''){
					$count = $count  + 1;
				} else {
					$Y = date('Y',strtotime( $docoffer->created_at));
				
					$other_consultant = count(json_decode($docoffer->shared_with));
				
					$t =0 ;

					if($year == $Y){
						$t =1 ;

						if($other_consultant == 1){
							$t = 0.5;
						} else {
							$t = round(1 / (1 + $other_consultant),2);
						}
					}
					
				
					$count = $count  + $t;
				}
			}*/
			
			//$data2[$docoffer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

			
		
		}

		return $count;
	}


	public function aveRental_offers_count($consultantid,$year,$yearlist){ //identical to thisConsultant. this a optimize ver for both ave rentalt and offer counts
			
	
			$from = request('from', $default = null);
			$to = request('to', $default = null);
			$to = date('Y-m-d',strtotime($to . ' +1 day'));
	
			$docuffers = Document::groupBy('shortlistid')
			->groupBy('clientid')
			->groupBy('propertyid')				
			->orderBy('documents.created_at', 'desc');
		
	
		
				if($from !== null && $to !== null ){								
					//$docuffers->whereBetween('created_at', [$request->get('from') , $request->get('to') ]);
					$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and `created_at` between '".$from."' and '".$to."'");
				} else {			
					//$docuffers->whereRaw("(shared_with like '%"4"%' or consultantid=4) and (doctype='offerletter' and YEAR(created_at) = 2021)");
					$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and YEAR(created_at) = ".$year." ");
				
					//$docuffers->whereYear('created_at', '=',$year);
				}
		
			
	
			$docuffers = $docuffers->get();
			
		
			$data2 = array();
			$count = 0;
			$data = array();
	
			foreach($docuffers as $docoffer){
	
	
				$docufferss =Document::where('doctype','=','offerletter')
				->where('documents.shortlistid','=',$docoffer->shortlistid)
				//->groupBy('shortlistid')
				//->groupBy('clientid')
				//->groupBy('propertyid')				
				->whereIn('id', function($q) use ($docoffer){
					$q->select(DB::raw('MAX(id) FROM documents where shortlistid ="'.$docoffer->shortlistid.'" and doctype="offerletter" GROUP BY shortlistid,clientid,propertyid '));
				})->get();
	
				$data = array();
	
				foreach($docufferss as $docofferr){
	
					$client = Client::where('id','=',$docofferr->clientid)->first();		
					$user = USER::where('id','=',$docofferr->consultantid)->first();		
					$property = Property::where('id','=',$docofferr->propertyid)->first();	
					$comm = Commission::where('shortlistid','=',$docofferr->shortlistid)->where('propertyid','=',$docofferr->propertyid)->first();
					$commissiondate = !empty($comm->commissiondate) ? date('d.m.Y',strtotime($comm->commissiondate)) : "-";
	
					$fieldcontents = $docofferr->fieldcontents;
	
					//$lead = DB::select(DB::raw("(select max(id) as id from leads WHERE leads.shortlistid = ".$docofferr->shortlistid.")"))->first();
					$lead = Lead::where('shortlistid','=',$docofferr->shortlistid)->first();
	
				
	
			
					$data[] = array(
						'cnfirstname' => $user->firstname,
						'cnlastname' => $user->lastname,
						'cfirstname' => $client->firstname,
						'clastname' => $client->lastname,
						//'fieldcontents' => $offerv2->fieldcontents,
						'fieldcontents' => $fieldcontents,
						'asking_rent'=> isset($property->asking_rent) && !empty($property->asking_rent) ? $property->asking_rent : '',
						'offerprice' => json_decode($fieldcontents)->f13,
						//'leads'=> $offerv2->leadsid,
						'commissiondate'=> $commissiondate,
						'propertyid'=> $docofferr->propertyid,
						'property'=> isset($property->name) && !empty($property->name) ? $property->name : '',
						'leadsid'=> !empty($lead->id) ? $lead->id : '#',
						'status'=>  !empty($lead ->status) ? $lead ->status : '#',
						'shared_with' =>$docoffer->shared_with
					);
	
				
						
					$data2[$docoffer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();	
				}
				
				$return = array();
			
				
			
			}


		
			///Offers Count
			$prevKey=0;
			$count = 0;
			foreach($data2 as  $key=> $offerrs ){

				foreach ($offerrs as $offer){
					
					if($key != $prevKey){
						$prevKey  = $key;
						
						
						if( count(json_decode($offer['shared_with'])) <= 0){
							$count++;
						} else {
							 $other_consultant = count(json_decode($offer['shared_with']));
							 if($other_consultant == 1){
								$t = 0.5;
							} else {
								$t = round(1 / (1 + $other_consultant),2);
							}

							$count = $count  + $t;

						}
					}
				}
	
			}
	
			$return['offers_count'] =  $count;
			

			///Average rental
			$totaloffer = 0;
			$aveRental = 0;
			$x = 0;
			$prevKey=0;
			foreach($data2 as  $key=> $offerrs ){

				foreach ($offerrs as $offer){
					if($key != $prevKey){
						$prevKey  = $key;
						if(!empty($offer['commissiondate']) && $offer['commissiondate']!=='-' && $offer['status'] !== 8){
							$totaloffer = $totaloffer +  $offer['offerprice'];
							$x++;
						}
					}
				}

			}

			if($totaloffer != 0 ){
				$aveRental = $totaloffer / $x;
			} 

			$return['ave_rental'] =  $aveRental;


			return $return;

	}

	public function offersfetch(Request $request, $consultantid,$year = 0){
	

		/*

		$year = $request->year;
		$consultantid = $request->consultantid;

				$offers = Document::select(
					'users.firstname as cnfirstname', 
						'users.lastname as cnlastname',
						'properties.name as property',
						'properties.id as propertyid',
						'properties.asking_rent',
						'documents.created_at',
						'documents.revision',
						'documents.shortlistid',
						'documents.fieldcontents',
						'leads.id as leadsid',
						'commission.id as commissionid'
						)
			->leftjoin('users','documents.consultantid', '=', 'users.id')
			->leftjoin('properties','documents.propertyid', '=', 'properties.id')
			//->leftjoin('commission','documents.shortlistid', '=', 'commission.shortlistid')
			//->leftjoin('leads','documents.shortlistid', '=', 'leads.shortlistid')
			->leftjoin('leads', function($query){
				$query->on('leads.shortlistid','=','documents.shortlistid')				
				->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
			})
			->leftjoin('commission', function($query){
				$query->on('commission.shortlistid','=','documents.shortlistid')				
				->on('commission.propertyid', '=', 'documents.propertyid' );					
			})
		//	->groupBy('documents.shortlistid')								
			->orderBy('documents.created_at', 'desc');
			

		if($request->yearlist == 'false'){
		//$offers->where("documents.created_at",">",date("Y-m-d",time()-3600*24*90));
		$days90 = date("Y-m-d",time()-3600*24*90);
		$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at > "'.$days90.'"  GROUP BY documents.shortlistid)');
		$yearlist = "false";
		} else {
		
			if( $request->get('from') !== null && $request->get('to') !== null ){	
			
				$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at between "'.$request->get('from').'" AND "'.$request->get('to').'"   GROUP BY documents.shortlistid)');			
			} else {
				//$offers->whereYear('documents.created_at', '=', $year);	
				$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at like "%'.$year.'%"  GROUP BY documents.shortlistid)');			
			}
			$yearlist = "true";

		}

		$offers = $offers->paginate(25);

		$data2 = array();

		

		$o=0;
		foreach($offers as $offer){

			
			$offersv2 = Document::select('documents.*',
										'properties.name as property',										
										'properties.asking_rent',										
										'users.firstname as cnfirstname', 
								'users.lastname as cnlastname',
								'clients.firstname as cfirstname', 
								'clients.lastname as clastname',
								'commission.commissiondate as commissiondate',
								'leads.id as leadsid',
								'leads.status as status'
								)
						->where('doctype','=',"offerletter")
						->where('documents.shortlistid','=',$offer->shortlistid)
						//->where('documents.consultantid','=',$consultantid)				
					//	->where('documents.id','=',$consultantid)		
					//	->where(DB::raw('YEAR(documents.created_at)'), '=', $year)
						->leftjoin('properties','documents.propertyid', '=', 'properties.id')
						->leftjoin('users','documents.consultantid', '=', 'users.id')
						->leftjoin('clients','documents.clientid', '=', 'clients.id')
						//->leftjoin('commission','documents.shortlistid', '=', 'commission.shortlistid')
						->leftjoin('leads', function($query){
							$query->on('leads.shortlistid','=','documents.shortlistid')				
							->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
						})
						->leftjoin('commission', function($query){
							$query->on('commission.shortlistid','=','documents.shortlistid')				
							->on('commission.propertyid', '=', 'documents.propertyid' );
							//->on('commission.id', '=', DB::raw("(select max(id) from commission WHERE commission.shortlistid = documents.shortlistid)"));
						})
						
						->orderBy('documents.revision', 'desc')
						->orderBy('clients.firstname', 'ASC')
						->groupby('propertyid')
						->get();

			$data = array();

			//get check and get invoice data
			$invc = Commission::where('shortlistid','=', $offer->shortlistid);
			if($invc->exists()){
				$fa = $invc->first();
				$fullamount = $fa->fullamount;
			}


			
						
			$count = 0;
			foreach($offersv2 as $offerv2){

				$getlatestOffer = Document::selectRaw('fieldcontents')
					->where('documents.shortlistid','=',$offer->shortlistid)
					->where('documents.propertyid','=',$offerv2->propertyid)
					->where('documents.doctype','=','offerletter')
					->orderBy('documents.revision', 'desc')
					->skip(0)->take(1)
					->get();
					
				foreach($getlatestOffer as $testt){					
					$latestOffer = $testt->fieldcontents;
				}

				//check if this the one with invoice
				if(!$count==0 AND !isset($fullamount)){
					
				} else {
					$fullamount = json_decode($latestOffer)->f13;
				}
				$count++;

					
				//$data[$offerv2->propertyid] = array(
					$data[] = array(
					'cnfirstname' => $offerv2->cnfirstname,
					'cnlastname' => $offerv2->cnlastname,
					'cfirstname' => $offerv2->cfirstname,
					'clastname' => $offerv2->clastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $latestOffer,
					'asking_rent'=> $offerv2->asking_rent,
					'offerprice' => $fullamount,
					'leads'=> $offerv2->leadsid,
					'commissiondate'=> $offerv2->commissiondate,
					'propertyid'=> $offerv2->propertyid,
					'property'=> $offerv2->property,
					'leadsid'=> $offerv2->leadsid,
					'status'=> $offerv2->status,
					
				);
			}
		
			//no data on $offersv2 get original data
			if(count($offersv2) == 0){
				$fullamount = json_decode($offer->fieldcontents)->f13;
				$data[] = array(
					'cnfirstname' => $offer->cnfirstname,
					'cnlastname' => $offer->cnlastname,
					'cfirstname' => $offer->cfirstname,
					'clastname' => $offer->clastname,
					'fieldcontents' => $offer->fieldcontents,
					'offerprice' => $fullamount,
					'asking_rent'=> $offer->asking_rent,
					'leads'=> $offer->leadsid,
					'commissiondate'=> $offer->commissiondate,
					'propertyid'=> $offer->propertyid,
					'property'=> $offer->property,
					'leadsid'=> $offer->leadsid,
					'status'=> $offer->status,
				);
			}
		

			$data2[$offer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

			

		}

		//print_r($finalData);
		*/

		$page = intval($request->get('page'));
		$data2 = $request->session()->get('offerslist','default');
		$offers = $this->paginate($data2,25,$page);

		$yearlist = true;
	


		return view('offerspagination', [
			'offers' => $offers,
			'offers2' => $offers,
			'year'=> $year,
			'consultantid' => $consultantid,
			'yearlist' => $yearlist,
			'statuses' => Lead::allstatus(),
		]);
	}


    public function companyoverviewuser(Request $request, $consultantid, $year){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if (!($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant'])){
			return redirect('/dashboardnew');
		}

		$fulltargets = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);
		$fulldone = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);
		$fullpercent = array(
			'monthdollar' => 0,
			'monthdeals' => 0,
			'anndollar' => 0,
			'anndeals' => 0,
			'saledollar' => 0,
			'saledeals' => 0,
		);

		$monthlyvalues = array(
			'dollardone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollartarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealsdone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealstarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		);

		$target = Targets::where('year', $year)->where('consultantid', $consultantid)->first();

		$month = intval(date('m'))-1;
		$maxmonth = $month;
		if ($year < intval(date('Y'))){
			$maxmonth = 12;
		}

		if ($target){
			$monthlylease = json_decode($target->monthlylease, true);
			$monthlyleasedeals = json_decode($target->monthlyleasedeals, true);

			//Month
			if (isset($monthlylease[$month])){
				$fulltargets['monthdollar'] = $monthlylease[$month];
			}
			if (isset($monthlyleasedeals[$month])){
				$fulltargets['monthdeals'] = $monthlyleasedeals[$month];
			}

			//Annual
			for ($m = 0; $m < 12; $m++){
				if (isset($monthlylease[$m])){
					$fulltargets['anndollar'] += $monthlylease[$m];
				}
				if (isset($monthlyleasedeals[$m])){
					$fulltargets['anndeals'] += $monthlyleasedeals[$m];
				}
				$monthlyvalues['dollartarget'][$m] += $monthlylease[$m];
				$monthlyvalues['dealstarget'][$m] += $monthlyleasedeals[$m];
			}

			//Sales
			$fulltargets['saledollar'] = $target->currentsale;
			$fulltargets['saledeals'] = $target->currentsaledeals;
		}

		//Commission
		$comms = Commcons::select(array('*', DB::raw('MONTH(commissiondate) as commmonth')))->where('consultantid', $consultantid)->whereRaw('YEAR(commissiondate) = '.$year)->get();

		if (count($comms) > 0){
			foreach ($comms as $comm){
				$commmonth = $comm->commmonth-1;

				if ($comm->salelease == 1){
					if ($month == $commmonth){
						$fulldone['monthdollar'] += $comm->commissionconsultant;
						$fulldone['monthdeals']++;
					}

					$fulldone['anndollar'] += $comm->commissionconsultant;
					$fulldone['anndeals']++;
				}else{
					$fulldone['saledollar'] += $comm->commissionconsultant;
					$fulldone['saledeals']++;
				}

				$monthlyvalues['dollardone'][$commmonth] += $comm->commissionconsultant;
				//$monthlyvalues['dealsdone'][$commmonth]++;
				$NumComm = 1;
				$commCount = $this->getCommissionCount($comm->propertyid,$comm->shortlistid,$comm->commissiondate);
				
				if($commCount > 1){
					$NumComm = 1 / $commCount;

					$NumComm = 1 / $commCount;
					$ttal = $monthlyvalues['dealsdone'][$commmonth]  + $NumComm;					
					
					$monthlyvalues['dealsdone'][$commmonth] = strpos($ttal,".")  ? number_format($ttal,1) : $ttal;
				} else {
					$monthlyvalues['dealsdone'][$commmonth]++;
				}
			}
		}

		$dollarsum = 0;
		$dealsum = 0;
		for ($m = 0; $m < 12; $m++){
			$monthlyvalues['dollartarget'][$m] = $monthlyvalues['dollartarget'][$m] - $monthlyvalues['dollardone'][$m];
			if ($m < $maxmonth){
				$dollarsum += $monthlyvalues['dollardone'][$m];
			}
			if ($monthlyvalues['dollartarget'][$m] < 0){
				$monthlyvalues['dollartarget'][$m] = 0;
			}
			$monthlyvalues['dealstarget'][$m] = $monthlyvalues['dealstarget'][$m] - $monthlyvalues['dealsdone'][$m];
			if ($m < $maxmonth){
				$dealsum += $monthlyvalues['dealsdone'][$m];
			}
			if ($monthlyvalues['dealstarget'][$m] < 0){
				$monthlyvalues['dealstarget'][$m] = 0;
			}
		}
		if ($maxmonth > 0){
			$dollarsum = round($dollarsum / $maxmonth);
			$dealsum = round($dealsum / $maxmonth, 2);
		}else{
			$dealsum = 0;
			$dollarsum = 0;
		}

		//Percent
		foreach ($fullpercent as $key => $value){
			if ($fulltargets[$key] <= $fulldone[$key]){
				$fullpercent[$key] = 100;
			}else if ($fulldone[$key] <= 0){
				$fullpercent[$key] = 0;
			}else{
				$fullpercent[$key] = floor($fulldone[$key]/$fulltargets[$key]*100);
			}
		}

		$years = array();
		for ($i = intval(date('Y')); $i >= 2017; $i--){
			$years[] = $i;
		}

		$leads = Lead::select(array('*', DB::raw('MONTH(created_at) as leadmonth')))->where('consultantid', $consultantid)->whereRaw('YEAR(created_at) = '.$year)->get();

		$leadstotal = array(
			'new' => 0,
			'offering' => 0,
			'invoicing' => 0,
			'hot' => 0,
			'active' => 0,
			'stored' => 0,
			'total' => 0
		);

		$leadsdeadvscomplete = array(
			'dead' => 0,
			'complete' => 0,
			'total' => 0
		);
		$leadsdeadvscompletebysource = array();
		$leadstotalbysource = array();
		foreach (Lead::allsources() as $key => $source){
			$leadstotalbysource[$key] = array(
				'new' => 0,
				'offering' => 0,
				'invoicing' => 0,
				'hot' => 0,
				'active' => 0,
				'stored' => 0,
				'total' => 0
			);
			$leadsdeadvscompletebysource[$key] = array(
				'dead' => 0,
				'complete' => 0,
				'total' => 0
			);
		}
		$leadstotalbysource[0] = array(
			'new' => 0,
			'offering' => 0,
			'invoicing' => 0,
			'hot' => 0,
			'active' => 0,
			'stored' => 0,
			'total' => 0
		);
		$leadsdeadvscompletebysource[0] = array(
			'dead' => 0,
			'complete' => 0,
			'total' => 0
		);

		if (count($leads) > 0){
			foreach ($leads as $lead){
				if ($lead->status == 0){
					$leadstotal['new']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['new']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 1){
					$leadstotal['hot']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['hot']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 2 || $lead->status == 3){
					$leadstotal['active']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['active']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 5){
					$leadstotal['offering']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['offering']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 6){
					$leadstotal['invoicing']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['invoicing']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 7){
					$leadsdeadvscomplete['complete']++;
					$leadsdeadvscomplete['total']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['complete']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 8){
					$leadsdeadvscomplete['dead']++;
					$leadsdeadvscomplete['total']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['dead']++;
					$leadsdeadvscompletebysource[$lead->sourcehear]['total']++;
				}else if ($lead->status == 9 || $lead->status == 10 || $lead->status == 12){
					$leadstotal['stored']++;
					$leadstotal['total']++;
					$leadstotalbysource[$lead->sourcehear]['stored']++;
					$leadstotalbysource[$lead->sourcehear]['total']++;
				}
			}
		}

		$liveleads = Lead::where('consultantid', $consultantid)->where('status', '!=', 11)->where('status', '!=', 7)->where('status', '!=', 8)->orderBy('created_at', 'desc')->with('client')->get();
		$liveleadsids = array();
		if (count($liveleads) > 0){
			foreach ($liveleads as $l){
				$liveleadsids[] = $l->shortlistid;
			}
		}
		$liveviewingsr = ClientViewing::whereIn('shortlist_id', $liveleadsids)->get();
		$liveviewings = array();
		if (count($liveviewingsr) > 0){
			foreach ($liveviewingsr as $l){
				if (isset($liveviewings[$l->shortlist_id])){
					$liveviewings[$l->shortlist_id]++;
				}else{
					$liveviewings[$l->shortlist_id] = 1;
				}
			}
		}
		$liveofferr = Document::whereIn('shortlistid', $liveleadsids)->where('doctype', 'offerletter')->orderBy('created_at', 'asc')->get();
		$liveoffer = array();
		if (count($liveofferr) > 0){
			foreach ($liveofferr as $l){
				$liveoffer[$l->shortlistid] = $l;
			}
		}


		

		return view('companyoverviewuser', [
			'users' => $users,
			'fulltargets' => $fulltargets,
			'fulldone' => $fulldone,
			'fullpercent' => $fullpercent,
			'monthlyvalues' => $monthlyvalues,
			'dollarsum' => $dollarsum,
			'dealsum' => $dealsum,
			'monthcount' => $maxmonth,
			'year' => $year,
			'years' => $years,
			'consultantid' => $consultantid,
			'leadstotal' => $leadstotal,
			'leadsdeadvscomplete' => $leadsdeadvscomplete,
			'leadsdeadvscompletebysource' => $leadsdeadvscompletebysource,
			'leadstotalbysource' => $leadstotalbysource,
			'leadsources' => Lead::allsources(),
			'liveleads' => $liveleads,
			'allstatus' => Lead::allstatus(),
			'liveviewings' => $liveviewings,
			'liveoffer' => $liveoffer,
			'reminderdate' => date('Y-m-d', strtotime('-30 days')),
			'todaydate' => date('Y-m-d')		
		]);
	}


	public function offers(Request $request, $consultantid, $year){

		/*$offers = Document::select(
						'users.firstname as cnfirstname', 
							'users.lastname as cnlastname',
							'clients.firstname as cfirstname', 
							'clients.lastname as clastname',
							'properties.name as property',
							'properties.id as propertyid',
							'properties.asking_rent',
							'documents.created_at',
							'documents.revision',
							'documents.shortlistid',
							'documents.fieldcontents',
							'leads.id as leadsid',
							'commission.id as commissionid',
							'commission.commissiondate as commissiondate',
							'leads.status as status'
							)
				->leftjoin('users','documents.consultantid', '=', 'users.id')
				->leftjoin('properties','documents.propertyid', '=', 'properties.id')
				//->leftjoin('commission','documents.shortlistid', '=', 'commission.shortlistid')
				//->leftjoin('leads','documents.shortlistid', '=', 'leads.shortlistid')
				//->leftjoin('users','documents.consultantid', '=', 'users.id')
				->leftjoin('clients','documents.clientid', '=', 'clients.id')
				->leftjoin('leads', function($query){
					$query->on('leads.shortlistid','=','documents.shortlistid')				
					->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
				})
				->leftjoin('commission', function($query){
					$query->on('commission.shortlistid','=','documents.shortlistid')				
					->on('commission.propertyid', '=', 'documents.propertyid' );					
				})
			//	->groupBy('documents.shortlistid')								
				->orderBy('documents.created_at', 'desc');
			

		if($request->yearlist == 'false'){
			
			//$offers->where("documents.created_at",">",date("Y-m-d",time()-3600*24*90));
			$days90 = date("Y-m-d",time()-3600*24*90);
			$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at > "'.$days90.'"  GROUP BY documents.shortlistid)');
			$yearlist = "false";
		} else {
			//$offers->whereYear('documents.created_at', '=', $year);	
			if( $request->get('from') !== null && $request->get('to') !== null ){	
				
				//$or = Document::where('doctype', '=', 'offerletter')->whereBetween('created_at', [$request->get('from'), $request->get('to')])->get();
				$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at between "'.$request->get('from').'" AND "'.$request->get('to').'"   GROUP BY documents.shortlistid)');			
			} else {
				$offers->whereRaw('documents.id IN (select MAX(id) FROM documents WHERE doctype="offerletter" AND consultantid="'.$consultantid.'" AND created_at like "%'.$year.'%"  GROUP BY documents.shortlistid)');			
			}
			
			
			$yearlist = "true";
		}

		$offers = $offers->paginate(25);

		
		$o=0;
		foreach($offers as $offer){

			
			$offersv2 = Document::select('documents.*',
										'properties.name as property',										
										'properties.asking_rent',										
										'users.firstname as cnfirstname', 
								'users.lastname as cnlastname',
								'clients.firstname as cfirstname', 
								'clients.lastname as clastname',
								'commission.commissiondate as commissiondate',
								'leads.id as leadsid',
								'leads.status as status'
								)
						->where('doctype','=',"offerletter")
						->where('documents.shortlistid','=',$offer->shortlistid)
						//->where('documents.consultantid','=',$consultantid)				
					//	->where('documents.id','=',$consultantid)		
						->where(DB::raw('YEAR(documents.created_at)'), '=', $year)
						->leftjoin('properties','documents.propertyid', '=', 'properties.id')
						->leftjoin('users','documents.consultantid', '=', 'users.id')
						->leftjoin('clients','documents.clientid', '=', 'clients.id')
						//->leftjoin('commission','documents.shortlistid', '=', 'commission.shortlistid')
						->leftjoin('leads', function($query){
							$query->on('leads.shortlistid','=','documents.shortlistid')				
							->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
						})
						->leftjoin('commission', function($query){
							$query->on('commission.shortlistid','=','documents.shortlistid')				
							->on('commission.propertyid', '=', 'documents.propertyid' );
							//->on('commission.id', '=', DB::raw("(select max(id) from commission WHERE commission.shortlistid = documents.shortlistid)"));
						})
						
						->orderBy('documents.revision', 'desc')
						->orderBy('clients.firstname', 'ASC')
						->groupby('propertyid')
						->get();

			$data = array();

			//get check and get invoice data
			$invc = Commission::where('shortlistid','=', $offer->shortlistid);
			if($invc->exists()){
				$fa = $invc->first();
				$fullamount = $fa->fullamount;
			}


			
						
			$count = 0;
			foreach($offersv2 as $offerv2){

				$getlatestOffer = Document::selectRaw('fieldcontents')
					->where('documents.shortlistid','=',$offer->shortlistid)
					->where('documents.propertyid','=',$offerv2->propertyid)
					->where('documents.doctype','=','offerletter')
					->orderBy('documents.revision', 'desc')
					->skip(0)->take(1)
					->get();
					
				foreach($getlatestOffer as $testt){					
					$latestOffer = $testt->fieldcontents;
				}

				//check if this the one with invoice
				if(!$count==0 AND !isset($fullamount)){
					
				} else {
					$fullamount = json_decode($latestOffer)->f13;
				}
				$count++;

					
				//$data[$offerv2->propertyid] = array(
					$data[] = array(
					'cnfirstname' => $offerv2->cnfirstname,
					'cnlastname' => $offerv2->cnlastname,
					'cfirstname' => $offerv2->cfirstname,
					'clastname' => $offerv2->clastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $latestOffer,
					'asking_rent'=> $offerv2->asking_rent,
					'offerprice' => $fullamount,
					'leads'=> $offerv2->leadsid,
					'commissiondate'=> $offerv2->commissiondate,
					'propertyid'=> $offerv2->propertyid,
					'property'=> $offerv2->property,
					'leadsid'=> $offerv2->leadsid,
					'status'=> $offerv2->status,
					
				);
			}
		
			//no data on $offersv2 get original data
			if(count($offersv2) == 0){
				$fullamount = json_decode($offer->fieldcontents)->f13;
				$data[] = array(
					'cnfirstname' => $offer->cnfirstname,
					'cnlastname' => $offer->cnlastname,
					'cfirstname' => $offer->cfirstname,
					'clastname' => $offer->clastname,
					'fieldcontents' => $offer->fieldcontents,
					'offerprice' => $fullamount,
					'asking_rent'=> $offer->asking_rent,
					'leads'=> $offer->leadsid,
					'commissiondate'=> $offer->commissiondate,
					'propertyid'=> $offer->propertyid,
					'property'=> $offer->property,
					'leadsid'=> $offer->leadsid,
					'status'=> $offer->status,
				);
			}
		

			$data2[$offer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

			

		}*/

//		print_r($data2);

		//

		$docuffers = Document::groupBy('shortlistid')		
		->groupBy('shortlistid')
		->groupBy('clientid')		
		->groupBy('propertyid')				
		->orderBy('documents.created_at', 'desc');

		$yearlist = false;

		if($request->yearlist == true){
			
			if( $request->get('from') !== null && $request->get('to') !== null ){					
				$to = date('Y-m-d',strtotime($request->get('to') . ' +1 day'));
				
				//$docuffers->whereBetween('created_at', [$request->get('from') , $request->get('to') ]);
				$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and `created_at` between '".$request->get('from')."' and '".$to."'");
				$yearlist = true;
			} else {
				//$docuffers->whereRaw("(shared_with like '%"4"%' or consultantid=4) and (doctype='offerletter' and YEAR(created_at) = 2021)");
				$docuffers->orwhereRaw(" (shared_with like '%".'"'.$consultantid.'"'."%'  or consultantid=".$consultantid.") and doctype='offerletter' and YEAR(created_at) = ".$year." ");
			
				//$docuffers->whereYear('created_at', '=',$year);
				$yearlist = true;
			}
			
		}


	
		$docuffersRes = $docuffers->get();	
		$offers = $docuffersRes;
	
		$data2 = array();
		//echo count($docuffersRes);

		foreach($docuffersRes as $docoffer){
		
			$docufferss2 = Document::where('doctype','=','offerletter')
			->where('documents.shortlistid','=',$docoffer->shortlistid)
			//->groupBy('shortlistid')
			//->groupBy('clientid')
			//->groupBy('propertyid')				
			->whereIn('id', function($q) use ($docoffer){
				$q->select(DB::raw('MAX(id) FROM documents where shortlistid ="'.$docoffer->shortlistid.'" and doctype="offerletter" GROUP BY shortlistid,clientid,propertyid '));
			});
			//->orderBy('documents.created_at', 'desc');
			/// dd($docufferss2->toSql());

		

			$docufferss2 = $docufferss2->get();

			$data = array();

			foreach($docufferss2 as $docofferr){

				$client = Client::where('id','=',$docofferr->clientid)->first();		
				$user = USER::where('id','=',$docofferr->consultantid)->first();		
				$property = Property::where('id','=',$docofferr->propertyid)->first();	
				$comm = Commission::where('shortlistid','=',$docofferr->shortlistid)->where('propertyid','=',$docofferr->propertyid)->first();
				$commissiondate = !empty($comm->commissiondate) && $comm->commissiondate!=='0000-00-00' && $comm->commissiondate!=='' ? date('d.m.Y',strtotime($comm->commissiondate)) : "-";
				

				$fieldcontents = $docofferr->fieldcontents;

				//$lead = DB::select(DB::raw("(select max(id) as id from leads WHERE leads.shortlistid = ".$docofferr->shortlistid.")"))->first();
				$lead = Lead::where('shortlistid','=',$docofferr->shortlistid)
				//->where('consultantid','=',$consultantid)
				->first();

			

		
				$data[] = array(
					'cnfirstname' => $user->firstname,
					'cnlastname' => $user->lastname,
					'cfirstname' => $client->firstname,
					'clastname' => $client->lastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $fieldcontents,
					'asking_rent'=>  !empty($property->asking_rent) ? $property->asking_rent : 0,
					'offerprice' => !empty(json_decode($fieldcontents)->f13) ? json_decode($fieldcontents)->f13 : 0,
					//'leads'=> $offerv2->leadsid,
					'commissiondate'=> $commissiondate,
					'propertyid'=> $docofferr->propertyid,
					'property'=> !empty($property->name) ? $property->name : '-',
					'leadsid'=> !empty($lead->id) ? $lead->id : '#',
					'status'=>  !empty($lead ->status) ? $lead ->status : '#',
				);
			}

			$data2[$docoffer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

		
		}
		//echo count(	$data2);
		//dump($data2);
	//	dd($docuffersRes);
		$offers = $this->paginate($data2,25);
		$request->session()->put('offerslist', $data2);

			
		return view('offerslist', [				
			'year' => $year,		
			'offers' => $offers,
			'offers2' => $offers,
			'consultantid'=> $consultantid,
			'yearlist' => $yearlist,
			'statuses' => Lead::allstatus(),
		]);
	}

	public function paginate($items, $perPage = 25, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

	/*public function test(Request $request,$id){
		$docuffers = Document::whereYear('created_at', '=','2021')->where('consultantid','=',$id)->where('doctype','=','offerletter')
		->orwhereRaw("( shared_with like '%".'"'.$id.'"'."%' )")	
		->groupBy('shortlistid')
		->groupBy('clientid')
		->groupBy('propertyid')				
		->orderBy('documents.created_at', 'desc')
		->get();

		$i = 1;

		echo '<table class="table ">
		<tr>	
			<td  width="2%"  class="text-left">
			
			</td>							
			<td width="15%" class="text-left">
				<b>Consultant</b>
			</td>
			<td width="15%" class="text-left">
				<b>Client</b>
			</td>
			<td width="15%" class="text-center">
				<b>Offering Date</b>
			</td>
			<td width="15%" class="text-center">
				<b>Offering Signed</b>
			</td>
			<td width="10%" class="text-left">
				<b>Property</b>
			</td>
			<td width="10%" class="text-center">
				<b>Asking</b>
			</td>
			<td width="10%" class="text-center">
				<b>Offer</b>
			</td>	
			<td width="10%" class="text-center">
				<b>Status</b>
			</td>
			<td width="10%" class="text-center">
				<b></b>
			</td>
		</tr>';

		foreach($docuffers as $docoffer){

			$docufferss = Document::whereYear('created_at', '=','2021')->where('doctype','=','offerletter')
			->where('documents.shortlistid','=',$docoffer->shortlistid)
			->groupBy('shortlistid')
			->groupBy('clientid')
			->groupBy('propertyid')				
			->orderBy('documents.created_at', 'desc')
			->get();

			$data = array();

			foreach($docufferss as $docofferr){

				$client = Client::where('id','=',$docofferr->clientid)->first();		
				$user = USER::where('id','=',$docofferr->consultantid)->first();		
				$property = Property::where('id','=',$docofferr->propertyid)->first();	
				$comm = Commission::where('shortlistid','=',$docofferr->shortlistid)->where('propertyid','=',$docofferr->propertyid)->first();
				$commissiondate = !empty($comm->commissiondate) ? date('d.m.Y',strtotime($comm->commissiondate)) : "-";

				$fieldcontents = $docofferr->fieldcontents;

				//$lead = DB::select(DB::raw("(select max(id) as id from leads WHERE leads.shortlistid = ".$docofferr->shortlistid.")"))->first();
				$lead = Lead::where('shortlistid','=',$docofferr->shortlistid)->first();

			

			
				$data[] = array(
					'cnfirstname' => $client->firstname,
					'cnlastname' => $client->lastname,
					'cfirstname' => $user->firstname,
					'clastname' => $user->lastname,
					//'fieldcontents' => $offerv2->fieldcontents,
					'fieldcontents' => $fieldcontents,
					'asking_rent'=> $property->asking_rent,
					'offerprice' => json_decode($fieldcontents)->f13,
					//'leads'=> $offerv2->leadsid,
					'commissiondate'=> $commissiondate,
					'propertyid'=> $docofferr->propertyid,
					'property'=> $property->name,
					'leadsid'=> $lead->id,
					//'status'=> $offerv2->status,
				);
			}

			$finalData[$docoffer->shortlistid] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

		
		}

		$prevKey=0;
		foreach($finalData as $key=> $offers){

			foreach($offers as $offer){

		

				echo '<tr>';

					echo '<td>'.$i++.' '.$key.'</td>';				
					echo '<td>'.$offer['cfirstname'].' '.$offer['clastname'].'</td>';
					echo '<td>'.$offer['cnfirstname'].' '.$offer['cnlastname'].'</td>';
					echo '<td>'.$offer['property'].'</td>';
					echo '<td>'.date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)).'</td>';
					echo '<td>'.$offer['commissiondate'] .'</td>';
					echo '<td>'.$offer['asking_rent'].'</td>';
					echo '<td>'.$offer['offerprice'].'</td>';
					echo '<td>'.$offer['leadsid'].'</td>';
					
			

				echo '</td>';
			}

		}
	}*/


	public function completed(Request $request, $consultantid = 0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if ($consultantid == 0){
			return redirect('/');
		}
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		$completed = Lead::select('properties.*', 'commission.*', 'leads.*')->join('commission', 'leads.shortlistid', '=', 'commission.shortlistid')->join('properties', 'commission.propertyid', '=', 'properties.id')->distinct()->where('leads.status', '=', 7)->where('leads.consultantid', '=', $consultantid)->with('client')->orderBy('commission.commissiondate', 'desc')->get();

		$leadids = array();
		foreach ($completed as $c){
			$leadids[] = $c->id;
		}
		$handovers = Calendar::whereIn('leadid', $leadids)->where('type', 1)->get();
		$handover = array();
		foreach ($handovers as $h){
			$handover[$h->leadid] = $h;
		}

		return view('leads.completed', [
			'completed' => $completed,
			'users' => $users,
			'consultantid' => $consultantid,
			'handover' => $handover
		]);
	}

	public function dead(Request $request, $consultantid = 0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if ($consultantid == 0){
			return redirect('/');
		}
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		$dead = Lead::where('status', '=', 8)->where('consultantid', '=', $consultantid)->with('client')->orderBy('updated_at', 'desc')->get();

		return view('leads.dead', [
			'dead' => $dead,
			'users' => $users,
			'consultantid' => $consultantid,
		]);
	}

	public function deleted(Request $request, $consultantid = 0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if ($consultantid == 0){
			return redirect('/');
		}
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		$deleted = Lead::where('status', '=', 11)->where('consultantid', '=', $consultantid)->with('client')->orderBy('updated_at', 'desc')->paginate(30);

		return view('leads.deleted', [
			'deleted' => $deleted,
			'users' => $users,
			'consultantid' => $consultantid,
		]);
	}


	public function newlead($clientid){
        $users = User::orderBy('status', 'asc')->get();

		return view('leads.create', [
			'clientid' => $clientid,
			'users' => $users,
			'avstatus' => Lead::allstatus(),
			'avsources' => Lead::allsources(),


		]);
	}

	public function createlead(Request $request){
		$input = $request->all();
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if (isset($input['clientid'])){
			$data = array();

			$data['clientid'] = intval($input['clientid']);
			$data['createdby'] = $user->id;
			$data['userid'] = $user->id;

			$data['consultantid'] = $input['consultantid'];
			$data['status'] = $input['status'];
			$data['lastoutreach'] = $input['lastoutreach'];
			$data['remindme'] = $input['remindme'];
			$data['dateinitialcontact'] = $input['dateinitialcontact'];
			$data['sourcehear'] = $input['sourcehear'];
			$data['type'] = $input['type'];
			if (isset($input['clienttype'])){
				$data['clienttype'] = $input['clienttype'];
			}
			$data['req_beds'] = $input['req_beds'];
			$data['req_baths'] = $input['req_baths'];
			$data['req_maidrooms'] = $input['req_maidrooms'];
			$data['req_outdoor'] = $input['req_outdoor'];
			$data['req_carparks'] = $input['req_carparks'];
			$data['req_facilites'] = $input['req_facilites'];
			$data['budget'] = $input['budget'];
			$data['budgetto'] = $input['budgetto'];
			$data['likes'] = $input['likes'];
			$data['currentflat'] = $input['currentflat'];
			$data['reasonformove'] = $input['reasonformove'];
			$data['noticeperiod'] = $input['noticeperiod'];
			$data['preferredareas'] = $input['preferredareas'];

			if (trim($input['comments']) != ''){
				$comments = array(
					array(
						'userid' => $user->id,
						'datetime' => time(),
						'content' => trim($input['comments'])
					)
				);
			}else{
				$comments = array();
			}
			$data['comments'] = json_encode($comments);

			$lead = Lead::create($data);
			$lead->save();

			$client = Client::where('id', '=', $lead->clientid)->first();
			if ($client){
				$client->user_id = $lead->consultantid;
				$client->budget_min = $lead->budget;
				$client->budget_max = $lead->budgetto;
				$client->save();
			}

			return redirect('/lead/edit/'.$lead->id);
		}else{
			return redirect('/');
		}
	}

	public function addshortlist($id, $shortlistid){
		$lead = Lead::where('id', '=', $id)->first();

		if ($lead){
			$lead->shortlistid = $shortlistid;
			$lead->save();

			$shortlist = ClientShortlist::find($shortlistid);
			if ($shortlist){
				$shortlist->user_id = $lead->consultantid;
				$shortlist->typeid = $lead->type;
				$shortlist->save();
			}

			return redirect('/lead/edit/'.$lead->id);
		}else{
			return redirect('/');
		}
	}

	public function removeshortlist($id){
		$lead = Lead::where('id', '=', $id)->first();

		if ($lead){
			$lead->shortlistid = 0;
			$lead->save();

			return redirect('/lead/edit/'.$lead->id);
		}else{
			return redirect('/');
		}
	}

	public function createshortlist($id){
		$user = \Auth::user();
		$lead = Lead::where('id', '=', $id)->with('client')->first();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($lead){
			$newid = $lead['client']->shortlists()->create(array('user_id' => $user->id))->id;
			return $this->addshortlist($id, $newid);
		}else{
			return redirect('/');
		}
	}

	public function edit($id){
        $users = User::orderBy('status', 'asc')->get();
		$user = \Auth::user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$lead = Lead::where('id', '=', $id)->with('client')->first();

		if ($lead){
			$shortlist = null;
			$shortlists = null;
			$viewings = null;
			$comm = null;
			$doc = null;
			$properties = null;
			$docs = array();
			$docsprops = array();
			$offerletters = array();
			$lastofferletter = null;
			$lastofferlettercalendar = null;

			if ($lead->shortlistid > 0){
				// - there is a shortlist which is added (open) - show all 4 buttons like on the shortlist part
				// - there is a shortlist which is added (closed) - only show view & documents buttons

				$shortlist = ClientShortlist::find($lead->shortlistid);
				$viewings = ClientViewing::where('shortlist_id', '=', $lead->shortlistid)->get();
				$comm = Commission::where('shortlistid', '=', $lead->shortlistid)->first();
				$doc = Documentinfo::where('shortlist_id', $lead->shortlistid)->where('type_id', $lead->type)->first();

				if ($doc){
					$props = explode('|', $doc->properties);
					$properties = Property::whereIn('id', $props)->with('indexed_photo')->withTrashed()->get();

					foreach ($properties as $key => $property) {
						if (empty($property['indexed_photo'])) {
							$PropertyPhoto = new PropertyPhoto;
							$photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
							$properties[$key]->featured_photo_url = $photo->featured_photo();
						} else {
							$properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
						}
					}

					$rows = Document::where('shortlistid', '=', $lead->shortlistid)->get();
					foreach ($rows as $d){
						if (!isset($docs[$d->doctype])){
							$docs[$d->doctype] = array();
						}
						$docs[$d->doctype][$d->propertyid] = $d;
						if (!isset($docsprops[$d->propertyid])){
							$phelper = Property::where('id', $d->propertyid)->withTrashed()->first();
							if ($phelper){
								$docsprops[$d->propertyid] = $phelper;
							}
						}
						if (isset($docsprops[$d->propertyid])){
							if ($d->doctype == 'offerletter'){
								if (!isset($offerletters[$d->propertyid])){
									$offerletters[$d->propertyid] = array();
								}
								$offerletters[$d->propertyid][$d->revision] = $d;
							}
						}
					}
					if (count($offerletters) > 0){
						foreach ($offerletters as $oletters){
							if (count($oletters) > 0){
								foreach ($oletters as $ol){
									$lastofferletter = $ol;
								}
							}
						}
					}
					if ($lastofferletter){
						$lastofferletter['fieldcontents'] = json_decode($lastofferletter->fieldcontents, true);
					}
				}

			}else{
				// - no open shortlist for that client - create a new one / automatically added to that lead
				// - an existing open shortlist for that client / allow to view and ask if that should be added to the lead

				$shortlists = $lead['client']->shortlists()->with('options')->orderBy('status', 'asc')->get();

			}

			$form = null;
			if ($lead->websiteid > 0){
				$form = FormProperty::where('id', '=', $lead->websiteid)->first();
			}else if ($lead->contactid > 0){
				$form = FormContact::where('id', '=', $lead->contactid)->first();
			}

			$lastofferlettercalendar = Calendar::where('leadid', $lead->id)->first();

			$clientpic = null;
			if (isset($lead['client']) && !empty($lead['client']->id)){
				$clientpic = Clientdocuments::where('clientid', $lead['client']->id)->where('doctype', 'picture')->first();
			}

			

			


			return view('leads.edit', [
				'users' => $users,
				'avstatus' => Lead::allstatus(),
				'avsources' => Lead::allsources(),
				'lead' => $lead,
				'shortlist' => $shortlist,
				'shortlists' => $shortlists,
				'viewings' => $viewings,
				'commission' => $comm,
				'doc' => $doc,
				'properties' => $properties,
				'docs' => $docs,
				'docsprops' => $docsprops,
				'offerletters' => $offerletters,
				'form' => $form,
				'lastofferletter' => $lastofferletter,
				'lastofferlettercalendar' => $lastofferlettercalendar,
				'times' => Calendar::getTimesArray(),
				'clientpic' => $clientpic,
				'form_details' => !empty($form->form_details) ? unserialize($form->form_details) : null,
			]);
		}else{
			return redirect('/');
		}
	}


	public function update($id, Request $request){
		$input = $request->all();
		$user = $request->user();
		
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$lead = Lead::where('id', '=', $id)->first();

		if ($lead){
			$data = array();
			$data['userid'] = $user->id;

			$data['consultantid'] = $input['consultantid'];
			$data['status'] = $input['status'];
			$data['lastoutreach'] = $input['lastoutreach'];
			$data['remindme'] = $input['remindme'];
			$data['dateinitialcontact'] = $input['dateinitialcontact'];
			$data['sourcehear'] = $input['sourcehear'];
			$data['type'] = $input['type'];

			$data['shared_with'] = !empty($input['shared_with']) ? json_encode($input['shared_with']) : "";

			if (isset($input['clienttype'])){
				$data['clienttype'] = $input['clienttype'];
			}
			if (isset($input['req_beds'])){
				$data['req_beds'] = $input['req_beds'];
			}
			if (isset($input['req_baths'])){
				$data['req_baths'] = $input['req_baths'];
			}
			if (isset($input['req_maidrooms'])){
				$data['req_maidrooms'] = $input['req_maidrooms'];
			}
			if (isset($input['req_outdoor'])){
				$data['req_outdoor'] = $input['req_outdoor'];
			}
			if (isset($input['req_carparks'])){
				$data['req_carparks'] = $input['req_carparks'];
			}
			if (isset($input['req_facilites'])){
				$data['req_facilites'] = $input['req_facilites'];
			}
			if (isset($input['offerprice'])){
				$data['offerprice'] = $input['offerprice'];
			}
			if (isset($input['budget'])){
				$data['budget'] = $input['budget'];
			}
			if (isset($input['budgetto'])){
				$data['budgetto'] = $input['budgetto'];
			}
			if (isset($input['likes'])){
				$data['likes'] = $input['likes'];
			}
			if (isset($input['currentflat'])){
				$data['currentflat'] = $input['currentflat'];
			}
			if (isset($input['reasonformove'])){
				$data['reasonformove'] = $input['reasonformove'];
			}
			if (isset($input['noticeperiod'])){
				$data['noticeperiod'] = $input['noticeperiod'];
			}
			if (isset($input['preferredareas'])){
				$data['preferredareas'] = $input['preferredareas'];
			}
		
			$lead->fill($data);
			$lead->save();

			$client = Client::where('id', '=', $lead->clientid)->first();
			if ($client){
				$client->user_id = $lead->consultantid;
				$client->budget_min = $lead->budget;
				$client->budget_max = $lead->budgetto;
				$client->save();
			}

			if ($lead->shortlistid > 0){
				$shortlist = ClientShortlist::where('id', '=', $lead->shortlistid)->first();
				if ($shortlist){
					$shortlist->typeid = $lead->type;
					$shortlist->user_id = $lead->consultantid;
					$shortlist->save();

					DB::table('client_options')->where('shortlist_id', $shortlist->id)->update(['type_id' => $shortlist->typeid]);
					DB::table('client_viewing_options')->where('shortlist_id', $shortlist->id)->update(['type_id' => $shortlist->typeid]);
				}
			}
		}

		return redirect('/lead/edit/'.$id);
	}


	public function updateGoogleLink($id, Request $request){
		$input = $request->all();
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$lead = Lead::where('id', '=', $id)->first();

		if ($lead){
			if (isset($input['googledrivelink'])){
				$data['googledrivelink'] = $input['googledrivelink'];
			}

			$lead->fill($data);
			$lead->save();
		}

		return redirect('/lead/edit/'.$id);
	}

	public function removeGoogleLink($id, Request $request){
		$input = $request->all();
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$lead = Lead::where('id', '=', $id)->first();

		if ($lead){
			$data['googledrivelink'] = '';
			$lead->fill($data);
			$lead->save();
		}

		return redirect('/lead/edit/'.$id);
	}






    public function getcomments(Request $request, $id){
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

        $lead = Lead::findOrFail($id);
        if (empty($lead)) {
            return '0';
        }

		if (trim($lead->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $lead);
		}else{
			$short = substr(trim($lead->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($lead->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($lead->comments)
					)
				);
				$this->storecomments($id, $comments, $lead);
			}
		}

		$users = User::orderBy('status', 'asc')->get();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}

		$datetime = date_create();

		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';

			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];

				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				//$comments[$i]['datum'] = date_format($datetime, 'Y-m-d G:i');
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('leads.commentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }

    public function addcomment(Request $request, $id){
        $lead = Lead::findOrFail($id);
		$user = $request->user();
		$input = $request->all();

		$consultant = $input['consultant'];
		$client = $input['clientName'];

		$cn = User::where('id',$consultant)->first();
		
		$consultantName = $cn->firstname. " ".$cn->lastname;
		$consultantEmail = $cn->email;

		$userName = $user->firstname." ".$user->lastname;
		$userID = $user->id;
		$userEmail = $user->email;
		
		
        if (empty($lead)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$comment = '';
		if (isset($input['comments'])){
			$comment = trim(filter_var($input['comments'], FILTER_SANITIZE_STRING));
		}

		if (trim($lead->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($lead->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($lead->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($lead->comments)
					)
				);
			}
		}

		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;

		$lead->fill([
			'comments' => json_encode($comments)
		])->save();

		
		$subject = 'Nest Property — Comment Added to Client Enquiry';
		
		$message = 'Consultant:'.$consultantName."<br />";
		$message .= 'Client Name:'.$client."<br /><br />";
		$message .= 'The comment is: '.$comment."<br /><br />";

		$data = array(
			'name'=> $consultantName, 
			'consultantName' => $consultantName, 
			'client' => $client, 
			'comment' => $comment, 
			'title'=> $subject, 
			'username'=>$userName,
			'id'=>$id);
		
		if($consultant != $user->id){
			\Mail::send('leads.mailnoti', $data, function($mail) use ($subject, $consultantEmail, $consultantName,$userName,$userEmail){
			$mail->to($consultantEmail, $consultantName)->subject($subject);
			$mail->from('noreply@db.nest-property.com',$userName);
			$mail->cc($userEmail,$userName);
			});
		}
		

		return 'ok';
    }

	public function removecomment(Request $request, $id){
        $lead = Lead::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($lead)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}

		if (trim($lead->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($lead->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($lead->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($lead->comments)
					)
				);
			}
		}

		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}

		$lead->fill([
			'comments' => json_encode($comments)
		])->save();

		return 'ok';
	}


	public function storecomments($id, $comments, $lead){
		$lead->fill([
			'comments' => json_encode($comments)
		])->save();
	}

	public function getchanges(Request $request, $id){
		$user = $request->user();
        $lead = Lead::findOrFail($id);

        if (empty($lead)){
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$changes = array();
		$rows = $lead->getLog();

		$users = User::orderBy('status', 'asc')->get();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}

		$o_types = array(1 => 'Lease', 2 => 'Sale');
		$o_source = Lead::allsources();
		$o_status = Lead::allstatus();

		$r = null;

		for ($i = 0; $i < count($rows)-1; $i++){
			$r = $rows[$i];
			$n = array();

			$n['name'] = 'Nest Property';
			$n['pic'] = '/showimage/users/dummy.jpg';

			if ($r->userid == 0 || !isset($u[$r->userid])){
				$n['name'] = 'Nest Property';
				$n['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$r->userid];

				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$n['name'] = $uu->name;
					if ($uu->picpath != ''){
						$n['pic'] = $uu->picpath;
					}else{
						$n['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}

			//$n['datum'] = \NestDate::nest_datetime_format($r->logtime, 'Y-m-d H:i:s');
			$n['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', $r->logtime, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y H:i');

			$content = '';
			if (($i+1) < count($rows)){
				$old = $rows[$i+1];

				if ($r['type'] != $old['type']){
					if (isset($o_types[$r['type']]) && isset($o_types[$old['type']])){
						$content .= '<b>Type</br> changed from '.$o_types[$old['type']].' to '.$o_types[$r['type']].'<br />';
					}
				}
				if ($r['clienttype'] != $old['clienttype']){
					if (isset($o_types[$r['clienttype']]) && isset($o_types[$old['clienttype']])){
						$content .= '<b>Client Type</b> changed from '.$o_types[$old['clienttype']].' to '.$o_types[$r['clienttype']].'<br />';
					}
				}
				if ($r['status'] != $old['status']){
					if (isset($o_status[$old['status']]) && isset($o_status[$r['status']])){
						$content .= '<b>Status</b> changed from '.$o_status[$old['status']].' to '.$o_status[$r['status']].'<br />';
					}
				}
				if ($r['lastoutreach'] != $old['lastoutreach']){
					if ($r['lastoutreach'] != null && $r['lastoutreach'] != '0000-00-00' && $old['lastoutreach'] != null && $old['lastoutreach'] != '0000-00-00'){
						$content .= '<b>Last Outreach</b> changed from '.\NestDate::nest_date_format($old['lastoutreach'], 'Y-m-d').' to '.\NestDate::nest_date_format($r['lastoutreach'], 'Y-m-d').'<br />';
					}else if ($r['lastoutreach'] != null && $r['lastoutreach'] != '0000-00-00'){
						$content .= '<b>Last Outreach</b> is now '.\NestDate::nest_date_format($r['lastoutreach'], 'Y-m-d').'<br />';
					}
				}
				if ($r['remindme'] != $old['remindme']){
					if ($r['remindme'] != null && $r['remindme'] != '0000-00-00' && $old['remindme'] != null && $old['remindme'] != '0000-00-00'){
						$content .= '<b>Remind Me</b> changed from '.\NestDate::nest_date_format($old['remindme'], 'Y-m-d').' to '.\NestDate::nest_date_format($r['remindme'], 'Y-m-d').'<br />';
					}else if ($r['remindme'] != null && $r['remindme'] != '0000-00-00'){
						$content .= '<b>Remind Me</b> is now '.\NestDate::nest_date_format($r['remindme'], 'Y-m-d').'<br />';
					}
				}
				if ($r['dateinitialcontact'] != $old['dateinitialcontact']){
					if ($r['dateinitialcontact'] != null && $r['dateinitialcontact'] != '0000-00-00' && $old['dateinitialcontact'] != null && $old['dateinitialcontact'] != '0000-00-00'){
						$content .= '<b>First Contact</b> changed from '.\NestDate::nest_date_format($old['dateinitialcontact'], 'Y-m-d').' to '.\NestDate::nest_date_format($r['dateinitialcontact'], 'Y-m-d').'<br />';
					}else if ($r['dateinitialcontact'] != null && $r['dateinitialcontact'] != '0000-00-00'){
						$content .= '<b>First Contact</b> is now '.\NestDate::nest_date_format($r['dateinitialcontact'], 'Y-m-d').'<br />';
					}
				}
				if ($r['budget'] != $old['budget']){
					$content .= '<b>Budget From</b> changed from '.$old['budget'].' to '.$r['budget'].'<br />';
				}
				if ($r['budgetto'] != $old['budgetto']){
					$content .= '<b>Budget To</b> changed from '.$old['budgetto'].' to '.$r['budgetto'].'<br />';
				}
				if ($r['sourcehear'] != $old['sourcehear']){
					if (isset($o_source[$old['sourcehear']]) && isset($o_source[$r['sourcehear']])){
						$content .= '<b>Source</b> changed from '.$o_source[$old['sourcehear']].' to '.$o_source[$r['sourcehear']].'<br />';
					}else if (isset($o_source[$r['sourcehear']])){
						$content .= 'New <b>Source</b>: '.$o_source[$r['sourcehear']].'<br />';
					}
				}
				if ($r['req_beds'] != $old['req_beds']){
					$content .= 'New <b>Number of Required Bedrooms</b>: '.$r['req_beds'].'<br />';
				}
				if ($r['req_baths'] != $old['req_baths']){
					$content .= 'New <b>Number of Required Bathrooms</b>: '.$r['req_baths'].'<br />';
				}
				if ($r['req_maidrooms'] != $old['req_maidrooms']){
					$content .= 'New <b>Number of Required Maid\'s Room(s)</b>: '.$r['req_maidrooms'].'<br />';
				}
				if ($r['req_carparks'] != $old['req_carparks']){
					$content .= 'New <b>Number of Required Carpark(s)</b>: '.$r['req_carparks'].'<br />';
				}
				if ($r['req_outdoor'] != $old['req_outdoor']){
					$content .= '<b>Outdoor Space</b> changed to: '.$r['req_outdoor'].'<br />';
				}
				if ($r['req_facilites'] != $old['req_facilites']){
					$content .= '<b>Facilities</b> changed to: '.$r['req_facilites'].'<br />';
				}
				if ($r['likes'] != $old['likes']){
					$content .= '<b>Likes / Dislikes</b> changed to: '.$r['likes'].'<br />';
				}
				if ($r['currentflat'] != $old['currentflat']){
					$content .= '<b>Currently Living</b> changed to: '.$r['currentflat'].'<br />';
				}
				if ($r['reasonformove'] != $old['reasonformove']){
					$content .= '<b>Reason for Move</b> changed to: '.$r['reasonformove'].'<br />';
				}
				if ($r['noticeperiod'] != $old['noticeperiod']){
					$content .= '<b>Notice Period</b> changed to: '.$r['noticeperiod'].'<br />';
				}
				if ($r['preferredareas'] != $old['preferredareas']){
					$content .= '<b>Preferred Area(s)</b> changed to: '.$r['preferredareas'].'<br />';
				}
			}else{
				$content = '1st entry';
			}
			$n['content'] = $content;

			if ($content != ''){
				$changes[] = $n;
			}
		}

		$n = array();

		$n['name'] = 'Nest Property';
		$n['pic'] = '/showimage/users/dummy.jpg';
		$n['content'] = 'Created by Nest Property';

		if ($lead->createdby == 0 || !isset($u[$lead->createdby])){
			$n['name'] = 'Nest Property';
			$n['pic'] = '/showimage/users/dummy.jpg';
			$n['content'] = 'Created by Nest Property';
		}else{
			$uu = $u[$lead->createdby];

			if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
				$n['name'] = $uu->name;
				$n['content'] = 'Created by '.$uu->name;
				if ($uu->picpath != ''){
					$n['pic'] = $uu->picpath;
				}else{
					$n['pic'] = '/showimage/users/dummy.jpg';
				}
			}
		}
		$n['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', $lead->created_at, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y H:i');
		$changes[] = $n;

        return view('leads.changesshow', [
			'changes' => $changes,
			'user' => $user
		]);
    }

    public function websitenquiries(Request $request){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$contactforms = FormContact::where('status', '=', 0)->orderBy('id', 'DESC')->get();
			$propertyforms = FormProperty::where('status', '=', 0)->orderBy('id', 'DESC')->get();

			$landlordforms = FormContact::where('status', '=', 3)->where('landlorduser', '=', $user->id)->orderBy('id', 'DESC')->get();

			return view('leads.websitenquiries', [
				'users' => $users,
				'contactforms' => $contactforms,
				'propertyforms' => $propertyforms,
				'landlordforms' => $landlordforms
			]);
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesinfo(Request $request){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Informationofficer']){
			$landlordforms = FormContact::where('status', '=', 3)->where('landlorduser', '=', $user->id)->get();
			$landlordformsold = FormContact::where('status', '=', 4)->where('landlorduser', '=', $user->id)->get();

			return view('leads.websitenquiriesinfo', [
				'users' => $users,
				'landlordforms' => $landlordforms,
				'landlordformsold' => $landlordformsold
			]);
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesall(Request $request){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		$consultantid = $request->input('consultantid');

		$from = $request->input('from');
		$to = $request->input('to');

		$filter =false;

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$userarr = array();
		foreach ($users as $u){
			$userarr[$u->id] = $u;
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$contactforms = FormContact::select('contact_form_submissions.*','leads.consultantid')->orderBy('contact_form_submissions.id', 'desc')							
							->leftjoin('leads','contact_form_submissions.id', '=', 'leads.contactid');
			
			//dd($contactforms);
			$propertyforms = FormProperty::select('property_form_submissions.*','leads.consultantid')->orderBy('id', 'desc')
						->leftjoin('leads','property_form_submissions.id', '=', 'leads.websiteid');
			
			if($consultantid !=""){
				$contactforms->where('leads.consultantid','=', $consultantid);
				$propertyforms->where('leads.consultantid','=', $consultantid);
				$filter =true;
			}

			if($from !="" && $to !=""){
				$contactforms->whereBetween('contact_form_submissions.ts', [$from, $to]);
				$propertyforms->whereBetween('property_form_submissions.ts', [$from, $to]);
				$filter =true;
			}

			$contactforms = $contactforms->get();
			$propertyforms = $propertyforms->get();
			

			return view('leads.websitenquiriesall', [
				'users' => $users,
				'contactforms' => $contactforms,
				'propertyforms' => $propertyforms,
				'userarr' => $userarr,	
				'inputs' => array('consultantid'=>$consultantid,'from'=>$from, 'to'=>$to),
				'filter' => $filter
			]);
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriescreatecontact(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$form = FormContact::where('id', '=', $id)->first();
			$emailclients = array();
			$phoneclients = array();
			$nameclients = array();
			$userbyid = array();

			foreach ($users as $u){
				$userbyid[$u->id] = $u;
			}

			$email = trim($form->email);
			if ($email != ''){
				$emailclients = Client::where('email', 'like', $email)->orWhere('email', 'like', $email)->get();
			}

			$phone = trim(str_replace(' ', '', $form->phone));
			if (strlen($phone) > 3){
				$phone = substr($phone, -4);
			}
			if ($phone != ''){
				$phoneclients = Client::where('tel', 'like', '%'.$phone.'%')->orWhere('tel2', 'like', '%'.$phone.'%')->get();
			}

			$name = trim($form->name);
			$names = explode(' ', trim($form->name));
			if (count($names) > 1){
				foreach ($names as $n){
					if (strlen($n) > 3){
						$name = $n;
					}
				}
			}
			if ($name != ''){
				$nameclients = Client::where('firstname', 'like', '%'.$name.'%')->orWhere('firstname2', 'like', '%'.$name.'%')->orWhere('lastname', 'like', '%'.$name.'%')->orWhere('lastname2', 'like', '%'.$name.'%')->get();
			}

			return view('leads.websitenquiriescreatecontact', [
				'users' => $users,
				'form' => $form,
				'emailclients' => $emailclients,
				'phoneclients' => $phoneclients,
				'nameclients' => $nameclients,
				'userbyid' => $userbyid
			]);
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesshowcontact(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$form = FormContact::where('id', '=', $id)->first();

		$lead = null;
		if ($form->status == 10){
			$lead = Lead::where('contactid', '=', $form->id)->first();
		}

		return view('leads.websitenquiriesshowcontact', [
			'users' => $users,
			'form' => $form,
			'lead' => $lead
		]);
	}

    public function websitenquiriespassoncontact(Request $request, $id){
		$user = $request->user();
		$input = $request->all();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			if (isset($input['landlordregion'])){
				$form = FormContact::where('id', '=', $id)->first();

				$form->landlorduser = intval($input['userid']);
				$form->landlordregion = intval($input['landlordregion']);
				$form->status = 3;
				$form->save();
			}else if (isset($input['consultantid'])){
				$form = FormContact::where('id', '=', $id)->first();

				$form->status = 10;
				$form->save();

				$data = array();

				$data['clientid'] = 0;
				$data['createdby'] = $user->id;
				$data['userid'] = $user->id;

				$data['consultantid'] = $input['consultantid'];
				$data['status'] = 0;
				$data['type'] = $input['type'];
				if (isset($input['clienttype'])){
					$data['clienttype'] = $input['clienttype'];
				}
				$data['contactid'] = $input['websiteid'];
				$data['comments'] = $form->comments;

				$lead = Lead::create($data);
				$lead->save();

				//notification				
				$subject = 'Nest Property — New Client Enquiry';
				$consultant = User::findorFail( $input['consultantid']);
				$comment_url =  "https://db.nest-property.com/dashboardnew/".$input['consultantid'];
				
				$prop_url = $form->propertylink;
				//$prop = explode("/", $prop_url);
				//$propID = end($prop);

				//$prop_url = "https://db.nest-property.com/property/edit/".$propID;

				$client = $form->name;
				$consultantName = $consultant->name;
				$consultantEmail = $consultant->email;

				$user = $user->name;

				$data = array(
					'comment_url'=> $comment_url, 
					'prop_url'=>$prop_url,
					'client'=>$client,
					'consultantEmail'=> $consultantEmail,
					'consultantName'=> $consultantName,
					'subject'=>$subject
				);
				
				
					\Mail::send('leads.enquirymailnoti', $data, function($mail) use ($subject,$consultantEmail,$consultantName,$user){
					$mail->to($consultantEmail, $consultantName)->subject($subject);
					$mail->from('noreply@db.nest-property.com',$user);					
					});


			}else{
				
				$form = FormContact::where('id', '=', $id)->first();

				$form->forwarduser = intval($input['userid']);
				$form->status = 3;
				$form->save();

				$consultant = User::findorFail( $input['userid']);
				$client = $form->name;

				$toEmail = $consultant->email;
				$fromname = $consultant->firstname.' '. $consultant->lastname;	
			
				$message = 'Hello '.$fromname.',<br><br>A new client enquiry has been forwarded to you: <a href="https://db.nest-property.com/dashboardnew/'.$input['userid'].'">https://db.nest-property.com/dashboardnew/'.$input['userid'].'</a><br>';
				$message .= 'Client Name: '.$client;

				$subject = 'Nest Property — New Client Enquiry has been forwarded to you';				
			

				\Mail::send('emails.generic', ['body' => $message, 'title' => 'Invoicing Update'], function ($mail) use ($toEmail, $subject, $message) {
					$tos = $toEmail;
					$mail->from('noreply@db.nest-property.com', 'Nest Database');
					$mail->to($tos, 'Office Accounts')
					->subject($subject)			
					->setBody($message, 'text/html');
				});
			}

			

			return redirect('/websitenquiries');
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesremovecontact(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$form = FormContact::where('id', '=', $id)->first();

			$form->status = 1;
			$form->save();

			return redirect('/websitenquiries');
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesfinishcontact(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$form = FormContact::where('id', '=', $id)->first();

		$form->status = 4;
		$form->save();

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			return redirect('/websitenquiries');
		}else{
			if ($form->landlorduser > 0){
				return redirect('/websitenquiriesinfo');
			}else{
				return redirect('/websitenquiriesforwarded');
			}
		}
	}

    public function websitenquiriesfinishproperty(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$form = FormProperty::where('id', '=', $id)->first();

		$form->status = 4;
		$form->save();

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			return redirect('/websitenquiries');
		}else{
			return redirect('/websitenquiriesforwarded');
		}
	}


    public function websitenquiriesderemovecontact(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$form = FormContact::where('id', '=', $id)->first();

			$form->status = 0;
			$form->save();

			return redirect('/websitenquiries');
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriescreateproperty(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$form = FormProperty::where('id', '=', $id)->first();
			$emailclients = array();
			$phoneclients = array();
			$nameclients = array();
			$userbyid = array();

			foreach ($users as $u){
				$userbyid[$u->id] = $u;
			}

			$email = trim($form->email);
			if ($email != ''){
				$emailclients = Client::where('email', 'like', $email)->orWhere('email', 'like', $email)->get();
			}

			$phone = trim(str_replace(' ', '', $form->phone));
			if (strlen($phone) > 3){
				$phone = substr($phone, -4);
			}
			if ($phone != ''){
				$phoneclients = Client::where('tel', 'like', '%'.$phone.'%')->orWhere('tel2', 'like', '%'.$phone.'%')->get();
			}

			$name = trim($form->name);
			$names = explode(' ', trim($form->name));
			if (count($names) > 1){
				foreach ($names as $n){
					if (strlen($n) > 3){
						$name = $n;
					}
				}
			}
			if ($name != ''){
				$nameclients = Client::where('firstname', 'like', '%'.$name.'%')->orWhere('firstname2', 'like', '%'.$name.'%')->orWhere('lastname', 'like', '%'.$name.'%')->orWhere('lastname2', 'like', '%'.$name.'%')->get();
			}

			return view('leads.websitenquiriescreateproperty', [
				'users' => $users,
				'form' => $form,
				'emailclients' => $emailclients,
				'phoneclients' => $phoneclients,
				'nameclients' => $nameclients,
				'userbyid' => $userbyid
			]);
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesshowproperty(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$form = FormProperty::where('id', '=', $id)->first();

		$lead = null;
		if ($form->status == 10){
			$lead = Lead::where('websiteid', '=', $form->id)->first();
		}

		return view('leads.websitenquiriesshowproperty', [
			'users' => $users,
			'form' => $form,
			'lead' => $lead
		]);
	}

    public function websitenquiriespassonproperty(Request $request, $id){
		$user = $request->user();
		$input = $request->all();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			if (isset($input['consultantid'])){
				$form = FormProperty::where('id', '=', $id)->first();
				//$form->passon_consultantid = $input['consultantid'];
				$form->status = 10;
				$form->save();

				$data = array();

				$data['clientid'] = 0;
				$data['createdby'] = $user->id;
				$data['userid'] = $user->id;

				$data['consultantid'] = $input['consultantid'];
				$data['status'] = 0;
				$data['type'] = $input['type'];
				if (isset($input['clienttype'])){
					$data['clienttype'] = $input['clienttype'];
				}
				$data['websiteid'] = $input['websiteid'];
				$data['comments'] = $form->comments;

				$lead = Lead::create($data);
				$lead->save();

				//notification
				
				$subject = 'Nest Property — New Client Enquiry';

				$consultant = User::findorFail( $input['consultantid']);

				$comment_url =  "https://db.nest-property.com/dashboardnew/".$input['consultantid'];
				
				$prop_url = $form->propertylink;
				//$prop = explode("/", $prop_url);
				//$propID = end($prop);

				//$prop_url = "https://db.nest-property.com/property/edit/".$propID;

				$client = $form->name;
				$consultantName = $consultant->name;
				$consultantEmail = $consultant->email;

				$user = $user->name;

				$data = array(
					'comment_url'=> $comment_url, 
					'prop_url'=>$prop_url,
					'client'=>$client,
					'consultantEmail'=> $consultantEmail,
					'consultantName'=> $consultantName,
					'subject'=>$subject
				);
				
				
					\Mail::send('leads.enquirymailnoti', $data, function($mail) use ($subject,$consultantEmail,$consultantName,$user){
					$mail->to($consultantEmail, $consultantName)->subject($subject);
					$mail->from('noreply@db.nest-property.com',$user);					
					});
				

			}else{
				$form = FormProperty::where('id', '=', $id)->first();

				$form->forwarduser = intval($input['userid']);
				$form->status = 3;
				$form->save();


				$consultant = User::findorFail( $input['userid']);
				$client = $form->name;

				$toEmail = $consultant->email;
				$fromname = $consultant->firstname.' '. $consultant->lastname;	
			
				$message = 'Hello '.$fromname.',<br><br>A new client enquiry has been forwarded to you: <a href="https://db.nest-property.com/dashboardnew/'.$input['userid'].'">https://db.nest-property.com/dashboardnew/'.$input['userid'].'</a><br>';
				$message .="Client Name: ".$client.'<br>';
				$message .="Property: ".$form->propertylink.'<br>';

				$subject = 'Nest Property — New Client Enquiry has been forwarded to you';				
			

				\Mail::send('emails.generic', ['body' => $message, 'title' => 'Invoicing Update'], function ($mail) use ($toEmail, $subject, $message) {
					$tos = $toEmail;
					$mail->from('noreply@db.nest-property.com', 'Nest Database');
					$mail->to($tos, 'Office Accounts')
					->subject($subject)			
					->setBody($message, 'text/html');
				});
			}

			return redirect('/websitenquiries');
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesremoveproperty(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$form = FormProperty::where('id', '=', $id)->first();

			$form->status = 1;
			$form->save();

			return redirect('/websitenquiries');
		}else{
			return redirect('/');
		}
	}

    public function websitenquiriesderemoveproperty(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$form = FormProperty::where('id', '=', $id)->first();

			$form->status = 0;
			$form->save();

			return redirect('/websitenquiries');
		}else{
			return redirect('/');
		}
	}

    public function enterclient(Request $request, $id){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$lead = Lead::where('id', '=', $id)->first();
		if ($lead->websiteid > 0){
			$form = FormProperty::where('id', '=', $lead->websiteid)->first();
		}else if ($lead->contactid > 0){
			$form = FormContact::where('id', '=', $lead->contactid)->first();
		}

		return view('leads.enterclient', [
			'users' => $users,
			'form' => $form,
			'lead' => $lead
		]);
	}

    public function enterclientsave(Request $request, $id){
		$user = $request->user();
		$input = $request->all();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$client = Client::where('id', intval($input['clientid']))->first();

		if ($client){
			$lead = Lead::where('id', '=', $id)->first();

			$lead->clientid = intval($input['clientid']);
			$lead->save();

			return redirect('/lead/edit/'.$id);
		}else{
			return redirect('/lead/enterclient/'.$id);
		}
	}

    public function websitenquiriesforwarded(Request $request){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$infoofficermessages = Infoofficermessage::where('status', '=', 0)->orderBy('id', 'desc')->get();
		$infoofficermessagesold = Infoofficermessage::where('status', '=', 1)->orderBy('id', 'desc')->get();
		$propertyforms = FormProperty::where('status', '=', 3)->where('forwarduser', '=', $user->id)->orderBy('id', 'desc')->get();
		$propertyformsold = FormProperty::where('status', '=', 4)->where('forwarduser', '=', $user->id)->orderBy('id', 'desc')->get();
		$contactforms = FormContact::where('status', '=', 3)->where('forwarduser', '=', $user->id)->orderBy('id', 'desc')->get();
		$contactformsold = FormContact::where('status', '=', 4)->where('forwarduser', '=', $user->id)->orderBy('id', 'desc')->get();

		$userarr = array();
		foreach ($users as $u){
			$userarr[$u->id] = $u;
		}

		return view('leads.websitenquiriesforwarded', [
			'users' => $users,
			'user' => $user,
			'propertyforms' => $propertyforms,
			'propertyformsold' => $propertyformsold,
			'contactforms' => $contactforms,
			'contactformsold' => $contactformsold,
			'infoofficermessages' => $infoofficermessages,
			'infoofficermessagesold' => $infoofficermessagesold,
			'userarr' => $userarr
		]);
	}



	//Property Comments
    public function getcommentsproperty(Request $request, $id){
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

        $form = FormProperty::where('id', '=', $id)->first();
        if (empty($form)) {
            return '0';
        }

		if (trim($form->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $form);
		}else{
			$short = substr(trim($form->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($form->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($form->comments)
					)
				);
				$this->storecomments($id, $comments, $form);
			}
		}

		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}

		$datetime = date_create();

		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';

			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];

				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('leads.websiteenquirypropertycommentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }

    public function addcommentproperty(Request $request, $id){
        $form = FormProperty::where('id', '=', $id)->first();
		$user = $request->user();
		$input = $request->all();
        if (empty($form)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}

		if (trim($form->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($form->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($form->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($form->comments)
					)
				);
			}
		}

		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;

		$form->fill([
			'comments' => json_encode($comments)
		])->save();

		return 'ok';
    }

	public function removecommentproperty(Request $request, $id){
        $form = FormProperty::where('id', '=', $id)->first();
		$user = $request->user();
		$input = $request->all();
        if (empty($form)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}

		if (trim($form->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($form->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($form->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($form->comments)
					)
				);
			}
		}

		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}

		$form->fill([
			'comments' => json_encode($comments)
		])->save();

		return 'ok';
	}

	public function storecommentsproperty($id, $comments, $form){
		$form->fill([
			'comments' => json_encode($comments)
		])->save();
	}



	//Contact Comments
    public function getcommentscontact(Request $request, $id){
		$user = $request->user();

        $form = FormContact::where('id', '=', $id)->first();
        if (empty($form)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		if (trim($form->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $form);
		}else{
			$short = substr(trim($form->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($form->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($form->comments)
					)
				);
				$this->storecomments($id, $comments, $form);
			}
		}

		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}

		$datetime = date_create();

		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';

			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];

				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('leads.websiteenquirycontactcommentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }

    public function addcommentcontact(Request $request, $id){
        $form = FormContact::where('id', '=', $id)->first();
		$user = $request->user();
		$input = $request->all();
        if (empty($form)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}

		if (trim($form->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($form->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($form->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($form->comments)
					)
				);
			}
		}

		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;

		$form->fill([
			'comments' => json_encode($comments)
		])->save();

		return 'ok';
    }

	public function removecommentcontact(Request $request, $id){
        $form = FormContact::where('id', '=', $id)->first();
		$user = $request->user();
		$input = $request->all();
        if (empty($form)) {
            return '0';
        }
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}

		if (trim($form->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($form->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($form->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($form->comments)
					)
				);
			}
		}

		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}

		$form->fill([
			'comments' => json_encode($comments)
		])->save();

		return 'ok';
	}

	public function storecommentscontact($id, $comments, $form){
		$form->fill([
			'comments' => json_encode($comments)
		])->save();
	}


    public function iopropertycreate(Request $request){
		return view('leads.iopropertycreate', []);
	}

    public function storeiopropertycreate(Request $request){
		$input = $request->all();
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$data = array();

		$data['userid'] = $user->id;

		$data['comments'] = $input['comments'];
		$data['hkhomeslink'] = $input['hkhomeslink'];
		$data['propertysource'] = $input['propertysource'];
		$data['pocname'] = $input['pocname'];
		$data['pocemail'] = $input['pocemail'];
		$data['poctel'] = $input['poctel'];
		$data['dcheckdb'] = $input['dcheckdb'];

		$imgs = $_FILES['nestpictureupload'];

		if(!empty($imgs)){
			$storage_path = public_path();
			$filename = 'io'.time().'_'.$user->id.'_';
			$file_path = $storage_path .'/images/nest-photo/';
			$d = array();

			for ($i = 0; $i < count($imgs['name']); $i++){
				$ext = trim(strtolower(end((explode(".", $imgs['name'][$i])))));
				if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'tiff'){
					$n = $filename.$i.'.'.$ext;
					move_uploaded_file($imgs['tmp_name'][$i] ,$file_path.$n);
					$media = PropertyMedia::create(array(
						'property_id' => 0,
						'group'       => 'nest-photo',
						'url'         => url('/images/nest-photo/'.$n),
						'filename'    => $n,
					));
					$media->save();
					$d[] = $media->id;
				}
			}
			$data['nestpictureupload'] = json_encode($d);
		}

		$imgs = $_FILES['otherpictureupload'];

		if(!empty($imgs)){
			$storage_path = public_path();
			$filename = 'io'.time().'_'.$user->id.'_';
			$file_path = $storage_path .'/images/other-photo/';
			$d = array();

			for ($i = 0; $i < count($imgs['name']); $i++){
				$ext = trim(strtolower(end((explode(".", $imgs['name'][$i])))));
				if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'tiff'){
					$n = $filename.$i.'.'.$ext;
					move_uploaded_file($imgs['tmp_name'][$i] ,$file_path.$n);
					$media = PropertyMedia::create(array(
						'property_id' => 0,
						'group'       => 'other-photo',
						'url'         => url('/images/other-photo/'.$n),
						'filename'    => $n,
					));
					$media->save();
					$d[] = $media->id;
				}
			}
			$data['otherpictureupload'] = json_encode($d);
		}

		$iomessage = Infoofficermessage::create($data);
		$iomessage->save();

		return redirect('/dashboardnew');
	}

    public function iopropertyshow(Request $request, $id){
		$iomessage = Infoofficermessage::where('id', $id)->first();
		$users = User::orderBy('status', 'asc')->get();

		$userarr = array();
		foreach ($users as $u){
			$userarr[$u->id] = $u;
		}

		if ($iomessage){
			$nestpics = PropertyMedia::whereIn('id', json_decode($iomessage->nestpictureupload, true))->get();
			$otherpics = PropertyMedia::whereIn('id', json_decode($iomessage->otherpictureupload, true))->get();

			return view('leads.iopropertyshow', [
				'iomessage' => $iomessage,
				'userarr' => $userarr,
				'nestpics' => $nestpics,
				'otherpics' => $otherpics,
				'id' => $id
			]);
		}else{
			return redirect('/dashboardnew');
		}
	}

	public function iopropertycopyimgs(Request $request){
		$input = $request->all();
		$user = $request->user();

		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}

		$property = Property::where('id', $input['propertyid'])->first();
		$io = Infoofficermessage::where('id', $input['id'])->first();

		if (!empty($property) && !empty($io)){
			$nestpics = PropertyMedia::whereIn('id', json_decode($io->nestpictureupload, true))->get();
			$otherpics = PropertyMedia::whereIn('id', json_decode($io->otherpictureupload, true))->get();

			if (count($nestpics) > 0){
				foreach ($nestpics as $pic){
					if ($pic->property_id == 0){
						$pic->property_id = $property->id;
						$pic->save();
					}else{
						$data = $pic->toArray();

						$data['property_id'] = $property->id;
						$data['order'] = 0;
						$data['web_order'] = 0;
						unset($data['id']);
						unset($data['updated_at']);
						unset($data['created_at']);
						unset($data['deleted_at']);

						$new = PropertyMedia::create($data);
						$new->save();
					}
				}
			}

			if (count($otherpics) > 0){
				foreach ($otherpics as $pic){
					if ($pic->property_id == 0){
						$pic->property_id = $property->id;
						$pic->save();
					}else{
						$data = $pic->toArray();

						$data['property_id'] = $property->id;
						$data['order'] = 0;
						$data['web_order'] = 0;
						unset($data['id']);
						unset($data['updated_at']);
						unset($data['created_at']);
						unset($data['deleted_at']);

						$new = PropertyMedia::create($data);
						$new->save();
					}
				}
			}

			return redirect('/property/flushmedia/'.$property->id);
		}else{
			return redirect('/iopropertyshow/'.$input['id']);
		}
	}

    public function iopropertyfinish(Request $request, $id){
		$iomessage = Infoofficermessage::where('id', $id)->first();

		if ($iomessage){
			$iomessage->status = 1;
			$iomessage->save();
		}

		return redirect('/websitenquiriesforwarded');
	}















    public function companydbusage(Request $request, $yearmonth = 0, $userid=0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();
		$users2 = User::where('status',0)->orderBy('status', 'asc')->get();

		$usersbyid = array();
		foreach ($users as $u){
			$usersbyid[$u->id] = $u;
		}

		if ($yearmonth == 0){
			$year = intval(date('Y'));
			$month = intval(date('m'));
		}else{
			$e = explode('-', $yearmonth);

			if (count($e) > 1){
				$year = intval($e[0]);
				$month = intval($e[1]);
			}else{
				$year = intval(date('Y'));
				$month = intval(date('m'));
			}
		}

		if (!(isset($user->getUserRights()['Superadmin']) || isset($user->getUserRights()['Director']))){
			return redirect('/dashboardnew');
		}

		$yearmonths = array();
		for ($y = 2017; $y <= intval(date('Y')); $y++){
			for ($m = 1; $m <= 12 && ($y < intval(date('Y')) || $m <= intval(date('m'))); $m++){
				$yearmonths[] = array('y' => $y, 'm' => $m);
			}
		}
		$months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');

		$metrics = array();
		$lastdayofmonth = 31;

		if($userid == 0){
			$userid = $user->id;
		} 


		$logins = LogLead::whereRaw('YEAR(logtime) = '.$year.' and MONTH(logtime) = '.$month)->where('userid', $userid)->get();
		foreach ($logins as $log){
			if (!isset($metrics[$log->userid])){
				$metrics[$log->userid] = array(
					'click' => array(),
					'login' => array(),
					'properties' => array(),
					'lead' => array(),
					'documents' => array()
				);
			}
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $log->logtime, 'GMT')->setTimezone('Asia/Hong_Kong');
			$metrics[$log->userid]['lead'][] = array(
				'x' => (intval($datetime->format('H'))*60 + intval($datetime->format('i')))/60,
				'y' => $datetime->format('d'),
				'show' => 'Enquiry: '.$datetime->format('d/m/Y H:i')
			);
			$lastdayofmonth = $datetime->format('t');
		}

		$logins = LogDocuments::whereRaw('YEAR(logtime) = '.$year.' and MONTH(logtime) = '.$month)->where('userid', $userid)->get();
		foreach ($logins as $log){
			if (!isset($metrics[$log->userid])){
				$metrics[$log->userid] = array(
					'click' => array(),
					'login' => array(),
					'properties' => array(),
					'lead' => array(),
					'documents' => array()
				);
			}
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $log->logtime, 'GMT')->setTimezone('Asia/Hong_Kong');
			$metrics[$log->userid]['documents'][] = array(
				'x' => (intval($datetime->format('H'))*60 + intval($datetime->format('i')))/60,
				'y' => $datetime->format('d'),
				'show' => 'Document: '.$datetime->format('d/m/Y H:i')
			);
			$lastdayofmonth = $datetime->format('t');
		}

		$logins = LogClick::whereRaw('YEAR(logtime) = '.$year.' and MONTH(logtime) = '.$month)->where('userid', $userid)->get();
		foreach ($logins as $log){
			if (!isset($metrics[$log->userid])){
				$metrics[$log->userid] = array(
					'click' => array(),
					'login' => array(),
					'properties' => array(),
					'lead' => array(),
					'documents' => array()
				);
			}
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $log->logtime, 'GMT')->setTimezone('Asia/Hong_Kong');
			$metrics[$log->userid]['click'][] = array(
				'x' => (intval($datetime->format('H'))*60 + intval($datetime->format('i')))/60,
				'y' => $datetime->format('d'),
				'show' => 'Click: '.$datetime->format('d/m/Y H:i')
			);
			$lastdayofmonth = $datetime->format('t');
		}

		$logins = LogLogin::whereRaw('YEAR(logtime) = '.$year.' and MONTH(logtime) = '.$month)->where('userid', $userid)->get();
		foreach ($logins as $log){
			if (!isset($metrics[$log->userid])){
				$metrics[$log->userid] = array(
					'click' => array(),
					'login' => array(),
					'properties' => array(),
					'lead' => array(),
					'documents' => array()
				);
			}
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $log->logtime, 'GMT')->setTimezone('Asia/Hong_Kong');
			$metrics[$log->userid]['login'][] = array(
				'x' => (intval($datetime->format('H'))*60 + intval($datetime->format('i')))/60,
				'y' => $datetime->format('d'),
				'show' => 'Login: '.$datetime->format('d/m/Y H:i')
			);
			$lastdayofmonth = $datetime->format('t');
		}

		$logins = LogProperties::whereRaw('YEAR(logtime) = '.$year.' and MONTH(logtime) = '.$month)->where('userid', $userid)->get();
		foreach ($logins as $log){
			if (!isset($metrics[$log->userid])){
				$metrics[$log->userid] = array(
					'click' => array(),
					'login' => array(),
					'properties' => array(),
					'lead' => array(),
					'documents' => array()
				);
			}
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $log->logtime, 'GMT')->setTimezone('Asia/Hong_Kong');
			$metrics[$log->userid]['properties'][] = array(
				'x' => (intval($datetime->format('H'))*60 + intval($datetime->format('i')))/60,
				'y' => $datetime->format('d'),
				'show' => 'Property: '.$datetime->format('d/m/Y H:i')
			);
			$lastdayofmonth = $datetime->format('t');
		}

		if ($year == intval(date('Y')) && $month == intval(date('m'))){
			$lastdayofmonth = date('d');
		}




		return view('companydbusage', [
			'users' => $users,
			'yearmonths' => $yearmonths,
			'year' => $year,
			'month' => $month,
			'months' => $months,
			'metrics' => $metrics,
			'usersbyid' => $usersbyid,
			'lastdayofmonth' => $lastdayofmonth,
			'users2'=> $users2,
			'selected_user' => $userid
		]);
	}

    public function companydbtime(Request $request, $yearmonth = 0, $userid=0){
		$user = $request->user();
		$users = User::orderBy('status', 'asc')->get();

		$users2 = User::where('status',0)->orderBy('status', 'asc')->get();

		$usersbyid = array();
		foreach ($users as $u){
			$usersbyid[$u->id] = $u;
		}

		if ($yearmonth == 0){
			$year = intval(date('Y'));
			$month = intval(date('m'));
		}else{
			$e = explode('-', $yearmonth);

			if (count($e) > 1){
				$year = intval($e[0]);
				$month = intval($e[1]);
			}else{
				$year = intval(date('Y'));
				$month = intval(date('m'));
			}
		}

		if (!(isset($user->getUserRights()['Superadmin']) || isset($user->getUserRights()['Director']))){
			return redirect('/dashboardnew');
		}

		$yearmonths = array();
		for ($y = 2019; $y <= intval(date('Y')); $y++){
			if ($y == 2019){
				for ($m = 5; $m <= 12 && ($y < intval(date('Y')) || $m <= intval(date('m'))); $m++){
					$yearmonths[] = array('y' => $y, 'm' => $m);
				}
			}else{
				for ($m = 1; $m <= 12 && ($y < intval(date('Y')) || $m <= intval(date('m'))); $m++){
					$yearmonths[] = array('y' => $y, 'm' => $m);
				}
			}
		}
		$months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');

		$metrics = array();
		$lastdayofmonth = 31;


		if($userid == 0){
			$userid = $user->id;
		} 


		$logins = LogClick::where('userid',$userid)->whereRaw('YEAR(logtime) = '.$year.' and MONTH(logtime) = '.$month)->orderBy('logid', 'asc')->get();
		foreach ($logins as $log){
			if (!isset($metrics[$log->userid])){
				$metrics[$log->userid] = array();
				for ($i = 1; $i <= 31; $i++){
					$metrics[$log->userid][$i] = array(
						'time' => 0,
						'clicks' => array()
					);
				}
			}
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $log->logtime, 'GMT')->setTimezone('Asia/Hong_Kong');
			$metrics[$log->userid][intval($datetime->format('d'))]['clicks'][] = intval($datetime->format('H'))*60 + intval($datetime->format('i'));
		}

		if (!empty($datetime)){
			$lastdayofmonth = $datetime->format('t');
		}
		if ($year == intval(date('Y')) && $month == intval(date('m'))){
			$lastdayofmonth = date('d');
		}

		if (count($metrics) > 0){
			foreach ($metrics as $userid => $ar){
				foreach ($ar as $day => $arr){
					$time = 0;
					$last = 0;
					$from = 0;

					foreach ($arr['clicks'] as $click){
						if ($last == 0){
							$from = $click;
							$last = $click;
						}else if ($click-$last > 15){
							$time += ($last-$from);
							$from = $click;
							$last = $click;
						}else{
							$last = $click;
						}
					}

					if ($last > $from){
						$time += ($last-$from);
					}

					$metrics[$userid][$day]['time'] = $time;
				}
			}
		}

		return view('companydbtime', [
			'users' => $users,
			'yearmonths' => $yearmonths,
			'year' => $year,
			'month' => $month,
			'months' => $months,
			'metrics' => $metrics,
			'usersbyid' => $usersbyid,
			'lastdayofmonth' => $lastdayofmonth,
			'users2' => $users2,
			'selected_user' => $userid
		]);
	}

	public function outstandingpayments(Request $request, $year = 0){

			$monthlyvalues = array(
			'dollardone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollardonecons' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollardonecomp' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dollartarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealsdone' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'dealstarget' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'payments' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			'paymentsoutstanding' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
		);

		if ($year == 0){
			$year = intval(date('Y'));
		}

		$years = array();
		for ($i = intval(date('Y')); $i >= 2017; $i--){
			$years[] = $i;
		}

		//Current Year
		$invoices = Commission::select('commission.id as cid','shortlistid','clientid','consultantid','propertyid', 'tenantpaiddate','landlordpaiddate', 'landlordamount', 'tenantamount',
		'properties.name as property',
		'users.firstname as cnfirstname', 
							'users.lastname as cnlastname',
							'clients.firstname as cfirstname', 
							'clients.lastname as clastname',
		 DB::raw('MONTH(commissiondate) as cmonth'), 
		 DB::raw('MONTH(landlordpaiddate) as lmonth'), 
		 DB::raw('MONTH(tenantpaiddate) as tmonth'))->whereRaw('YEAR(commissiondate) = '.intval($year))
		->leftjoin('users','commission.consultantid', '=', 'users.id')
		->leftjoin('clients','commission.clientid', '=', 'clients.id')
		->leftjoin('properties','commission.propertyid', '=', 'properties.id')
		 ->get();

		$outstandingpayment = 0;
		$outstandingpaymentcnt = 0;

		$data = array();

		foreach ($invoices as $inv){
			
			//Landlord
			if ($inv->lmonth > 0){
				$monthlyvalues['payments'][$inv->lmonth-1] += $inv->landlordamount;
				
			}else{
				$monthlyvalues['paymentsoutstanding'][$inv->cmonth-1] += $inv->landlordamount;
				$outstandingpayment += $inv->landlordamount;
				$outstandingpaymentcnt++;

				$offerLetter = $this->getLatestOffers($inv->shortlistid, $inv->propertyid);
				//print_r($offerLetter->fieldcontents);

				if(isset($offerLetter->fieldcontents)){
					$inv->startdate = json_decode($offerLetter->fieldcontents)->f16;					
				}
			
				if($inv->landlordamount  > 0){
				//if($inv->landlordamount > 0 &&  !empty($inv->landlordpaiddate) && $inv->landlordpaiddate !='0000-00-00'){
					$data[$inv->cid] = $inv;				
				}
			
			}

			//Tenant
			if ($inv->tmonth > 0){
				$monthlyvalues['payments'][$inv->tmonth-1] += $inv->tenantamount;
			}else{
				$monthlyvalues['paymentsoutstanding'][$inv->cmonth-1] += $inv->tenantamount;
				$outstandingpayment += $inv->tenantamount;
				$outstandingpaymentcnt++;
				

				$offerLetter = $this->getLatestOffers($inv->shortlistid, $inv->propertyid);
				//print_r($offerLetter->fieldcontents);

				if(isset($offerLetter->fieldcontents)){
					$inv->startdate = json_decode($offerLetter->fieldcontents)->f16;					
				}				
				
				if($inv->tenantamount > 0){
				//if($inv->tenantamount > 0 && (empty($inv->tenantpaiddate) || $inv->tenantpaiddate !='0000-00-00' )){				
					$data[$inv->cid] = $inv;					
				}
			
				
			}
		}

		/*//Past Year
		$pastyear = $year-1;
		$invoices = Commission::select('commission.id as cid','shortlistid','clientid','consultantid','propertyid', 'tenantpaiddate','landlordpaiddate', 'landlordamount', 'tenantamount',
		'properties.name as property',
		'users.firstname as cnfirstname', 
							'users.lastname as cnlastname',
							'clients.firstname as cfirstname', 
							'clients.lastname as clastname',
		 DB::raw('MONTH(commissiondate) as cmonth'), 
		 DB::raw('MONTH(landlordpaiddate) as lmonth'),
		 DB::raw('YEAR(tenantpaiddate) as tyear'),
		 DB::raw('YEAR(landlordpaiddate) as lyear'),
		 DB::raw('MONTH(tenantpaiddate) as tmonth'))
		 ->leftjoin('users','commission.consultantid', '=', 'users.id')
		->leftjoin('clients','commission.clientid', '=', 'clients.id')
		->leftjoin('properties','commission.propertyid', '=', 'properties.id')
		 ->whereRaw('YEAR(commissiondate) = '.intval($pastyear))->get();

		$lastoutstandingpayment = 0;
		$lastoutstandingpaymentcnt = 0;

		foreach ($invoices as $inv){
			//Landlord
			if ($inv->lmonth > 0){
				if ($inv->lyear == $year){
					$monthlyvalues['payments'][$inv->lmonth-1] += $inv->landlordamount;
				}
			}else{
				$lastoutstandingpayment += $inv->landlordamount;
				$lastoutstandingpaymentcnt++;
				$outstandingpayment += $inv->landlordamount;
				$outstandingpaymentcnt++;

				$offerLetter = $this->getLatestOffers($inv->shortlistid, $inv->propertyid);
				//print_r($offerLetter->fieldcontents);

				if(isset($offerLetter->fieldcontents)){
					$inv->startdate = json_decode($offerLetter->fieldcontents)->f16;					
				}
			
				if($inv->landlordamount > 0){
					$data[$inv->cid] = $inv;
				}
				
			}

			//Tenant
			if ($inv->tmonth > 0){
				if ($inv->tyear == $year){
					$monthlyvalues['payments'][$inv->tmonth-1] += $inv->tenantamount;
				}
			}else{
				$lastoutstandingpayment += $inv->tenantamount;
				$lastoutstandingpaymentcnt++;
				$outstandingpayment += $inv->tenantamount;
				$outstandingpaymentcnt++;

				$offerLetter = $this->getLatestOffers($inv->shortlistid, $inv->propertyid);
				//print_r($offerLetter->fieldcontents);

				if(isset($offerLetter->fieldcontents)){
					$inv->startdate = json_decode($offerLetter->fieldcontents)->f16;					
				}				

				if($inv->tenantamount > 0){
					$data[$inv->cid] = $inv;
				}
				
			}
		}*/

		$data1 = collect($data)->sortBy('startdate',false)->toArray();
		$data1 = collect($data1)->sortBy('startdate')->reverse()->toArray();		
	
	

		return view('leads.outstandingpayments', [
			'data'=> $data1,
			'outstandingpayment' => $outstandingpayment,
			'outstandingpaymentcnt' => $outstandingpaymentcnt,
			'year'=>$year
		]);
	}

	public function getLatestOffers($shortlistid,$propertyid){

		return Document::where('shortlistid','=',$shortlistid)
		->where('doctype','=','offerletter')
		->where('propertyid','=',$propertyid)
		->orderBy('revision', 'DESC')
		->first();

	}


	public function offersbrackets(Request $request, $year = 0,  $month = 0){

		
		if($year == 0){
			$year = date('Y');
		}	
		
		$offers = Document::select(
					'users.firstname as cnfirstname', 
						'users.lastname as cnlastname',
						'clients.firstname as cfirstname', 
						'clients.lastname as clastname',
						'properties.name as property',
						'properties.id as propertyid',
						'properties.asking_rent',
						'documents.created_at',
						'documents.revision',
						'documents.shortlistid',
						'documents.fieldcontents',
						'documents.id as docId',
						'leads.id as leadsid',
						'commission.id as commissionid',
						'commission.commissiondate as commissiondate',
						'leads.status as status'
						)
			->leftjoin('users','documents.consultantid', '=', 'users.id')
			->leftjoin('properties','documents.propertyid', '=', 'properties.id')		
			->leftjoin('clients','documents.clientid', '=', 'clients.id')
			->leftjoin('leads', function($query){
				$query->on('leads.shortlistid','=','documents.shortlistid')				
				->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = documents.shortlistid)"));
			})
			->leftjoin('commission', function($query){
				$query->on('commission.shortlistid','=','documents.shortlistid')				
				->on('commission.propertyid', '=', 'documents.propertyid' );					
			})					
			->orderBy('documents.created_at', 'desc');		
	
		$offers->whereRaw('documents.id IN (select MAX(id) 
				FROM documents WHERE doctype="offerletter" AND created_at like "%'.$year.'%"  GROUP BY documents.shortlistid)');	
		
		if($month != 0){
			$offers->whereRaw('MONTH(documents.created_at) = '. intval($month));			
		}
		

		

		//$offers = $offers->paginate(25);
		$offers = $offers->get();

		$data = array();
		$data2 = array();

		$bracket1 = array();
		$bracket2 = array();
		$bracket3 = array();
		$bracket4 = array();
		$bracket5 = array();

		$percentage = 0;
		$percentageCount = 0;

		$brackets_data = array(
			'bracket1'=> 0,
			'bracket1_count'=> 0,
			'bracket2'=> 0,
			'bracket2_count'=> 0,
			'bracket3'=> 0,
			'bracket3_count'=> 0,
			'bracket4'=> 0,
			'bracket4_count'=> 0,
			'bracket5'=> 0,
			'bracket5_count'=> 0			
		);

		$Totalpercentage = 0;

		foreach($offers as $offer){
			$fullamount = json_decode($offer->fieldcontents)->f13;
			
		
			
			$val = $this->getCommission($offer->shortlistid);		
			if(!empty($val)){
				$offer->commissiondate = $val->commissiondate;
				$fullamount = $val->fullamount ;

				$prop = $this->getProperty($val->propertyid);

				if(!empty($prop)){

				$offer->propertyid = $prop->id;
				$offer->property = $prop->name;
				$offer->asking_rent = $prop->asking_rent;
				}

				$thisoffer = $this->getOffer($offer->shortlistid, $val->propertyid);	
				
				if(!empty($thisoffer)){
					//$fullamount = json_decode($thisoffer->fieldcontents)->f13;
					$offer->fieldcontents = $thisoffer->fieldcontents;
				}
			}
		

			$asking_rent = $offer->asking_rent;
		
		
			
			if($offer->commissiondate !=='0000-00-00 00:00:00' AND !empty($offer->commissiondate) AND $fullamount!=0 ){
				

				if($asking_rent >=20000 AND $asking_rent <= 39000){
			
					$bracket1[] = array(
						'cnfirstname' => $offer->cnfirstname,
						'cnlastname' => $offer->cnlastname,
						'cfirstname' => $offer->cfirstname,
						'clastname' => $offer->clastname,
						'fieldcontents' => $offer->fieldcontents,
						'offerprice' => $fullamount,
						'asking_rent'=> $offer->asking_rent,
						'leads'=> $offer->leadsid,
						'commissiondate'=> $offer->commissiondate,
						'propertyid'=> $offer->propertyid,
						'property'=> $offer->property,
						'leadsid'=> $offer->leadsid,
						'status'=> $offer->status,
						'percentage' => $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount)
					);

					$percentage = $percentage + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
					$percentageCount++;					
					
					$brackets_data['bracket1'] = $brackets_data['bracket1'] + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
					$brackets_data['bracket1_count'] = $brackets_data['bracket1_count'] + 1;

				}

				
				if($asking_rent >=40000 AND $asking_rent <= 79000){
				
						$bracket2[] = array(
							'cnfirstname' => $offer->cnfirstname,
							'cnlastname' => $offer->cnlastname,
							'cfirstname' => $offer->cfirstname,
							'clastname' => $offer->clastname,
							'fieldcontents' => $offer->fieldcontents,
							'offerprice' => $fullamount,
							'asking_rent'=> $offer->asking_rent,
							'leads'=> $offer->leadsid,
							'commissiondate'=> $offer->commissiondate,
							'propertyid'=> $offer->propertyid,
							'property'=> $offer->property,
							'leadsid'=> $offer->leadsid,
							'status'=> $offer->status,
							'percentage' => $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount)
						);

						$percentage = $percentage + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
						$percentageCount++;

						$brackets_data['bracket2'] = $brackets_data['bracket2'] + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
						$brackets_data['bracket2_count'] = $brackets_data['bracket2_count'] + 1;
	
				}

				
				if($asking_rent >=80000 AND $asking_rent <= 129000){
					
							$bracket3[] = array(
								'cnfirstname' => $offer->cnfirstname,
								'cnlastname' => $offer->cnlastname,
								'cfirstname' => $offer->cfirstname,
								'clastname' => $offer->clastname,
								'fieldcontents' => $offer->fieldcontents,
								'offerprice' => $fullamount,
								'asking_rent'=> $offer->asking_rent,
								'leads'=> $offer->leadsid,
								'commissiondate'=> $offer->commissiondate,
								'propertyid'=> $offer->propertyid,
								'property'=> $offer->property,
								'leadsid'=> $offer->leadsid,
								'status'=> $offer->status,
								'percentage' => $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount)
							);

						$percentage = $percentage + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
						$percentageCount++;

						$brackets_data['bracket3'] = $brackets_data['bracket3'] + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
						$brackets_data['bracket3_count'] = $brackets_data['bracket3_count'] + 1;
						
		
				}

				if($asking_rent >=130000 AND $asking_rent <= 199000){
					
						$bracket4[] = array(
							'cnfirstname' => $offer->cnfirstname,
							'cnlastname' => $offer->cnlastname,
							'cfirstname' => $offer->cfirstname,
							'clastname' => $offer->clastname,
							'fieldcontents' => $offer->fieldcontents,
							'offerprice' => $fullamount,
							'asking_rent'=> $offer->asking_rent,
							'leads'=> $offer->leadsid,
							'commissiondate'=> $offer->commissiondate,
							'propertyid'=> $offer->propertyid,
							'property'=> $offer->property,
							'leadsid'=> $offer->leadsid,
							'status'=> $offer->status,
							'percentage' => $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount)
						);
					
					$percentage = $percentage + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
					$percentageCount++;
					
					$brackets_data['bracket4'] = $brackets_data['bracket4'] + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
					$brackets_data['bracket4_count'] = $brackets_data['bracket4_count'] + 1;
	
				}
				
				if($asking_rent >=200000 ){
	
						$bracket5[] = array(
							'cnfirstname' => $offer->cnfirstname,
							'cnlastname' => $offer->cnlastname,
							'cfirstname' => $offer->cfirstname,
							'clastname' => $offer->clastname,
							'fieldcontents' => $offer->fieldcontents,
							'offerprice' => $fullamount,
							'asking_rent'=> $offer->asking_rent,
							'leads'=> $offer->leadsid,
							'commissiondate'=> $offer->commissiondate,
							'propertyid'=> $offer->propertyid,
							'property'=> $offer->property,
							'leadsid'=> $offer->leadsid,
							'status'=> $offer->status,
							'percentage' => $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount)
						);
					
						$percentage = $percentage + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
						$percentageCount++;

						$brackets_data['bracket5'] = $brackets_data['bracket5'] + $this->getPercentageDiff($offer->asking_rent ? : 1, $fullamount);
						$brackets_data['bracket5_count'] = $brackets_data['bracket5_count'] + 1;
	
				}

				
			}
				
			
		}
		
		//$Totalpercentage = $percentage;
		if($percentage < 0){
			$Totalpercentage = number_format($percentage / $percentageCount, 2, '.', ',');
		}

		$Totalpercentage = $percentageCount !==0 ? number_format($percentage / $percentageCount, 2, '.', ',') : 0;

		

		$bracket1_percentage =  $brackets_data['bracket1_count'] !==0 ? number_format($brackets_data['bracket1'] / $brackets_data['bracket1_count'], 2, '.', ',') : 0;
		$bracket2_percentage =  $brackets_data['bracket2_count'] !==0 ? number_format($brackets_data['bracket2'] / $brackets_data['bracket2_count'], 2, '.', ',') : 0;
		$bracket3_percentage =  $brackets_data['bracket3_count'] !==0 ? number_format($brackets_data['bracket3'] / $brackets_data['bracket3_count'], 2, '.', ',') : 0;
		$bracket4_percentage =  $brackets_data['bracket4_count'] !==0 ? number_format($brackets_data['bracket4'] / $brackets_data['bracket4_count'], 2, '.', ',') : 0;
		$bracket5_percentage =  $brackets_data['bracket5_count'] !==0 ? number_format($brackets_data['bracket5'] / $brackets_data['bracket5_count'], 2, '.', ',') : 0;

		//$bracket1[] = collect($data)->sortBy('commissiondate')->reverse()->toArray();

		$years = array();
		for ($i = intval(date('Y')); $i >= 2017; $i--){
			$years[] = $i;
		}

		$months = array(
			0 => 'ALL', 
			1 => 'January', 
			2 => 'Febuary',
			3 => 'March',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December',
		);
		
		
		return view('leads.offersbrackets', [
			'year' => $year,		
			'years' => $years,		
			'months' => $months,
			'month' => $month,
			'offers' => $data,
			'bracket1' => $bracket1,
			'bracket2' => $bracket2,
			'bracket3' => $bracket3,
			'bracket4' => $bracket4,
			'bracket5' => $bracket5,
			'offers2' => $offers,		
			'yearlist' => "",
			'statuses' => Lead::allstatus(),
			'Totalpercentage' => $Totalpercentage,
			'bracket1_percentage'=> $bracket1_percentage,
			'bracket2_percentage'=> $bracket2_percentage,
			'bracket3_percentage'=> $bracket3_percentage,
			'bracket4_percentage'=> $bracket4_percentage,
			'bracket5_percentage'=> $bracket5_percentage,
			
		]);

	}

	public function getPercentageDiff($asking, $offer){

		//$val = $asking - $offer;
		$val = $offer - $asking;
		$vall = $val / $asking;
		$percentage = $vall * 100;

		if($asking == 1){
			return "-";		
		} else {
			return number_format($percentage, 2)."%";		
		}

		

	}


	public function getCommission($shortlistid){
		$com = Commission::where('shortlistid','=',$shortlistid)
		->orderBy('id',"DESC")
		->first();

		return $com;

	}


	public function getProperty($id){
		$prop = Property::where('id','=',$id)	
		->first();

		return $prop;
	}


	public function getOffer($shortlistid,$propId){
		$doc = Document::where('shortlistid','=',$shortlistid)
		->where('propertyid','=', $propId)
		->where('doctype','=', 'offerletter')
		->orderBy('id',"DESC")
		->first();

		return $doc;

	}

	public function getClient($clientid){
		$client = Client::find($clientid);		

		return $client;

	}

	public function consultantdeals(Request $request, $consultantid, $month = 0,$year){

	
		$data2 = Commcons::select(
				'users.firstname as cnfirstname', 
				'users.lastname as cnlastname',			
				'users.firstname as cnfirstname', 
				'users.lastname as cnlastname',
				'commcons.*',
				'leads.id as leadsid'
		)		
		->whereYear('commcons.created_at','=',$year)
		->leftjoin('properties','commcons.propertyid', '=', 'properties.id')
		->leftjoin('users','commcons.consultantid', '=', 'users.id')
		->leftjoin('leads', function($query){
			$query->on('leads.shortlistid','=','commcons.shortlistid')				
			->on('leads.id', '=', DB::raw("(select max(id) from leads WHERE leads.shortlistid = commcons.shortlistid)"));
		})
		->where('commcons.consultantid','=',$consultantid);
		
		if($month!=0){
			$data2->whereMonth('commcons.created_at','=',$month);
		
		}
		
		$data2 = $data2->paginate(20);

		

		foreach($data2 as $data){

			$val = $this->getCommission($data->shortlistid);			
			$fullamount = $val->fullamount ;

			$prop = $this->getProperty($val->propertyid);
			$thisoffer = $this->getOffer($data->shortlistid, $val->propertyid);		
			
			
			$client = $this->getClient($val->clientid);

			
			$offers[] = array(
				'c_name' => $client->firstname. ' ' .$client->lastname,
				'cn_name' => $data->cnfirstname . ' ' . $data->cnlastname,
				'property' => $prop->name,
				'propertyid' => $prop->id,
				'fieldcontents' => isset($thisoffer->fieldcontents) ? $thisoffer->fieldcontents : null,
				'commissiondate' => $val->commissiondate,
				'asking_rent' => $prop->asking_rent,
				'offerprice'=> $fullamount,
				'commissionconsultant' => $data->commissionconsultant,	
				'leadsid'=> $data->leadsid

			);

		}

		
			
		return view('leads.consultantdeals', [				
			'year' => $year,		
			'offers' => !empty($offers) ? $offers: null,
			//'offers2' => $offers,
			'consultantid'=> $consultantid,		
			'month' => $month	
		]);
	}




}

