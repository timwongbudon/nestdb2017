<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

use View;
use App\Lead;
use App\LogClick;
use App\PdfTutorial;
use App\ResourceContent;

class ResourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');     
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(5);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pdfs = PdfTutorial::all();
        $personal = ResourceContent::where('key', ResourceContent::KEY_PERSONAL)->first();
        $usefulLinks = ResourceContent::where('key', ResourceContent::KEY_USEFUL_LINKS)->first();
        return view('resources.index', compact('pdfs', 'personal', 'usefulLinks'));
    }
	
    public function loadpdf($pdf){
		if (file_exists(storage_path('pdfs/'.$pdf.'.pdf'))){
			return response()->download(storage_path('pdfs/'.$pdf.'.pdf'));
        }

        return Redirect::route('home')->withMessage('file not found');
    }

    public function editPdfTutorials ()
    {   
        $pdfs = PdfTutorial::all();
        return view('resources.editpdftutorials', compact('pdfs'));
    }

    public function savePdfTutorial (Request $request)
    {
        if ($request->hasFile('pdfFile')) {

            $file = $request->file('pdfFile');
            $storage_path = storage_path();
            $file_path = 'pdfs/';
            $filename = str_replace(' ', '-', trim($file->getClientOriginalName()));
            
            $file->move($storage_path .'/'. $file_path, $filename);

            PdfTutorial::create([
                'display_name' => trim($file->getClientOriginalName()),
                'filename' => $filename,
                'path' => $storage_path .'/'. $file_path . $filename
            ]);            
        }
        return redirect( url('/resources/pdf/edit') );
    }

    public function removePdfTutorial (Request $request) 
    {
        if ($pdf = PdfTutorial::find($request->id)) {
            $pdf->delete();
        }
        return redirect( url('/resources/pdf/edit') );
    }
	
    public function editPersonal (Request $request)
    {
        $key = $request->key;
        $resource = ResourceContent::where('key', $key)->first();
        return view('resources.editresource', compact('resource', 'key'));
    }

    public function saveResource (Request $request)
    {
        $key = $request->key;

        $resource = ResourceContent::where('key', $key)->first();
        if (empty($resource)) {
            ResourceContent::create([
                'name' => str_replace('_', ' ', $key),
                'key' => $key,
                'content' => $request->quillContent
            ]);
        } else {
            $resource->content = $request->quillContent;
            $resource->save();
        }
        return redirect('/resources/'.$key.'/edit');
    }
	
}
