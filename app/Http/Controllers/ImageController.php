<?php

namespace App\Http\Controllers;

use App\Property;
use Input;
use Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;

use App\PropertyMedia;
use App\PropertyPhoto;
use App\PropertyImageVariation;

class ImageController extends Controller
{
	public function read($property_media_id, $format, $filename, Request $request) {	
			$overwrite = $request->input('overwrite', 0);
			$img = PropertyImageVariation::read_image($property_media_id, $format, $overwrite);
			header('Content-Type: image/jpeg');
			header('Cache-Control: max-age=2592000, public');
			if( $img !== null ){
				//dd( $img->encode('jpg'));
				echo  $img->encode('jpg');
			} else {				
				$img =  Image::make("https://db.nest-property.com/images/tools/dummy-featured-small.jpg");
				echo $img->encode('jpg');
			}			
	
		
	}
}