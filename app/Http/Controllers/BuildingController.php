<?php

namespace App\Http\Controllers;

use App\Building;
use App\BuildingMeta;
use App\Property;
use App\User;
use App\PropertyMeta;
use App\PropertyFeature;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Repositories\BuildingRepository;

use Illuminate\Support\Facades\DB;

use View;
use App\Lead;
use Carbon\Carbon;
use App\LogClick;

class BuildingController extends Controller
{
	/**
     * The building repository instance.
     *
     * @var Building Repository
     */
    protected $buildings;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BuildingRepository $buildings, Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(16);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
			
			$this->buildings = $buildings;
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * API
     */
    public function api_index(Request $request)
    {
        $keywords = $request->input('keywords', '');
        $buildings = array();
        if (!empty($keywords)) {
            $buildings = Building::ApiKeywordSearch($keywords)->orderBy('name', 'asc')->limit(30)->get();
        }
        $error = !empty($buildings);
        $action_default = array(
                array('name' => $keywords, 'id' => ''),
            );
        $action_array = array(
                array('name' => 'Manage Buildings', 'id' => '', 'action' => 'manage-item'),
                // array('name' => 'Create new', 'id' => '', 'action' => 'create-new-item')
            );
        if (empty($buildings)) {
            return response()->json(array());
        }
        return response()->json(array_merge($action_default, $buildings->toArray(), $action_array));
    }

    /**
	 * Display a list of all of the user's building.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
    {
        $input = $request->all();
		
        $buildings = Building::IndexSearch($request)->orderBy('id', 'desc')->paginate(20)->appends($input);
        $district_ids = Building::district_ids();
        return view('buildings.index', [
            'buildings' => $buildings,
        ])->with('district_ids', $district_ids)->with('input', $request);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$facilitieslist = Property::getBuildingFacilitiesList();
		
        return view('buildings.create')
            ->with('district_ids', Building::district_ids())
            ->with('country_ids', Building::country_ids())
			->with('facilitieslist', $facilitieslist)
            ->with('features', Building::feature_ids());
    }

	/**
     * Create a new building.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Building::$rules, Building::$messages);
        $data = Input::all();
			
		if (isset($data['features'])){
			if (is_array($data['features'])){
				$data['facilities'] = implode(',', $data['features']);
			}else{
				$data['facilities'] = '';
			}
			unset($data['features']);
		}else{
			$data['facilities'] = '';
		}
			
        $building = $request->user()->buildings()->create($data);
        return redirect('/buildings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $building = Building::findOrFail($id);
		$features = array();
		$facilitieslist = Property::getBuildingFacilitiesList();
		$facilities = '';
		if ($building->facilities != null && trim($building->facilities) != ''){
			$e = explode(',', $building->facilities);
			foreach ($e as $ee){
				$idd = intval($ee);
				if (isset($facilitieslist[$idd])){
					$features[] = $facilitieslist[$idd];
				}
			}
			$facilities = implode(', ', $features);
		}
		
		$properties1 = Property::where('building_id', '=', $id)->where('floornr', '>', '0')->orderBy('floornr', 'asc')->get();
		$properties2 = Property::where('building_id', '=', $id)->where('floornr', '<=', '0')->orderBy('available_date', 'desc')->get();
		
        return view('buildings.show', compact('building'))
            ->with('country_ids', Building::country_ids())
            ->with('district_ids', Building::district_ids())
			->with('facilities', $facilities)
			->with('properties1', $properties1)
			->with('properties2', $properties2);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $building = Building::findOrFail($id);
		$features = array();
		if ($building->facilities != null && trim($building->facilities) != ''){
			$features = explode(',', $building->facilities);
		}
		
		$properties1 = Property::where('building_id', '=', $id)->where('floornr', '>', '0')->orderBy('floornr', 'asc')->get();
		$properties2 = Property::where('building_id', '=', $id)->where('floornr', '<=', '0')->orderBy('available_date', 'desc')->get();
		
		$facilitieslist = Property::getBuildingFacilitiesList();
		
        return view('buildings.edit', compact('building'))
            ->with('district_ids', Building::district_ids())
            ->with('country_ids', Building::country_ids())
            ->with('previous_url', \NestRedirect::previous_url())
            ->with('features', Building::feature_ids())
			->with('facilitieslist', $facilitieslist)
			->with('facilities', $features)
			->with('properties1', $properties1)
			->with('properties2', $properties2);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
		
        $validator = Validator::make($request->all(), Building::$rules, Building::$messages);
        if( !$validator->fails() ) {
            $building = Building::findOrFail($request->id);
			
			$data = $request->all();
			
			if (isset($data['features'])){
				if (is_array($data['features'])){
					$data['facilities'] = implode(',', $data['features']);
				}else{
					$data['facilities'] = '';
				}
				unset($data['features']);
			}else{
				$data['facilities'] = '';
			}
			
            $building->fill($data);
            $building->save();
			
            return !empty($data['previous_url'])?redirect($data['previous_url']):redirect('/buildings');
        }
        return redirect()->back()->withErrors($validator)->withInput();
    }

    /**
     * Destroy the given building.
     *
     * @param  Request  $request
     * @param  Building  $building
     * @return Response
     */
    public function destroy(Request $request, Building $building)
    {
        // $this->authorize('destroy', $building);
        $building->delete();
        return redirect('/buildings');
    }
	
	
	public function remove(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$id = intval($input['id']);
			if ($id > 0){
				$properties = Property::where('building_id', '=', $id)->get();
				
				if (count($properties) == 0){
					$building = Building::findOrFail($id);
					$building->fill([
						'deleted' => 1
					]);
					$building->save();
				}					
			}
		}
		return redirect(url('/buildings'));
	}
	
	public function updatenames(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$id = intval($input['id']);
			if ($id > 0){
				$properties = Property::where('building_id', '=', $id)->get();
				$building = Building::findOrFail($id);
				
				foreach ($properties as $p){
					$p->name = $building->name;
					$p->save();
				}
				return redirect(url('/building/edit/'.$id));
			}
		}
		return redirect(url('/buildings'));
	}
	
	public function updateaddress(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$id = intval($input['id']);
			if ($id > 0){
				$properties = Property::where('building_id', '=', $id)->get();
				$building = Building::findOrFail($id);
				
				foreach ($properties as $p){
					$p->address1 = $building->address1;
					$p->address2 = $building->address2;
					$p->save();
				}
				return redirect(url('/building/edit/'.$id));
			}
		}
		return redirect(url('/buildings'));
	}
	
	public function updatefacilities(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$id = intval($input['id']);
			if ($id > 0){
				$properties = Property::where('building_id', '=', $id)->get();
				$building = Building::findOrFail($id);
				
				$meta = new PropertyMeta; 
				$feature = new PropertyFeature();
				
				foreach ($properties as $p){
					$featuresMeta = json_decode($meta->get_post_meta($p->id, 'features', true));
					$facilitylist = $p->getBuildingFacilitiesList();
					
					$features = array();
					
					if (count($featuresMeta) > 0){
						foreach ($featuresMeta as $idd){
							if (!isset($facilitylist[intval($idd)])){
								$features[] = intval($idd);
							}
						}
					}
					
					$nf = explode(',', $building->facilities);
					
					if (count($nf) > 0){
						foreach ($nf as $idd){
							$features[] = intval($idd);
						}
					}
				
					$encoded_features = json_encode($features);
					$meta->add_post_meta($p->id, 'features', $encoded_features, true); 
					$feature->set_options($p->id, $features);
				}
				
				return redirect(url('/building/edit/'.$id));
			}
		}
		return redirect(url('/buildings'));
	}
	
	
	
	
    public function getcomments(Request $request, $id){
		$user = $request->user();
		
        $building = Building::findOrFail($id);
        if (empty($building)) {
            return '0';
        }
		
		if ($building->comments == null || trim($building->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $building);
		}else{
			$short = substr(trim($building->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($building->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($building->comments)
					)
				);
				$this->storecomments($id, $comments, $building);
			}
		}
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		$datetime = date_create();
		
		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';
			
			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				//$comments[$i]['datum'] = date_format($datetime, 'Y-m-d G:i');
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('buildings.commentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }
	
    public function addcomment(Request $request, $id){
        $building = Building::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($building)) {
            return '0';
        }
		
		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}
		
		if (trim($building->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($building->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($building->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($building->comments)
					)
				);
			}
		}
		
		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;
		
		$building->fill([
			'comments' => json_encode($comments)
		])->save();
		
		return 'ok';
    }
	
	public function removecomment(Request $request, $id){
        $building = Building::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($building)) {
            return '0';
        }
		
		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}
		
		if (trim($building->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($building->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($building->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($building->comments)
					)
				);
			}
		}
		
		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}
		
		$building->fill([
			'comments' => json_encode($comments)
		])->save();
		
		return 'ok';
	}
	
	
	public function storecomments($id, $comments, $building){
		$building->fill([
			'comments' => json_encode($comments)
		])->save();
	}
	
	public function geoapitest(){
		return Building::get_coordinates('22-24 Consort Rise, Hong Kong');
	}
	
	
	
	
}
