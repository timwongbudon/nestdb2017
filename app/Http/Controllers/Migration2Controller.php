<?php

/**
 * migration for old website.
 */

namespace App\Http\Controllers;

use App\Property;
use App\PropertyMeta;
use App\PropertyFeature;
use App\PropertyPhoto;
use App\PropertyOutdoor;
use App\PropertymigrationMeta;
use App\Building;
use Input;
use Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

class Migration2Controller extends Controller
{
    public $field_whitelist = array(
                                'ID',
                                'post_title',
                                'post_name',
                                'post_date',
                            );
    public $meta_whitelist = array(
                                'ref_key',
                                'address', 
                                'maids_room',
                                'outdoor',
                                'car_park',
                                'reference_number',
                                'net_size',
                                'estate_purpose', 
                                'estate_price', 
                                'estate_bedrooms', 
                                'estate_bathrooms', 
                                'estate_area', 
                                'estate_coordinates', 
                                'estate_status', 
                                'estate_featured', 
                                '_thumbnail_id', 
                                '_estate_image_gallery',
                            );
    public $meta_array_whitelist = array('nestweb_image');

    public function translate_features($old_id) {
        $feature_ids = array(
                "43" => "26",//"Garden",
                "48" => "1",//"Gym",
                "49" => "2",//"Swimming Pool",
                "50" => "3",//"Tennis Court",
                "51" => "4",//"Squash Court",
                "52" => "6",//"Shuttle Bus",
                "53" => "7",//"Children's Play Area",
                "56" => "9",//"Balcony",
                "57" => "10",//"Rooftop",
                "58" => "11",//"Helper's Room",
                "59" => "12",//"Terrace",
                "93" => "13",//"Store Room",
                "94" => "14",//"Concierge Service",
                "95" => "15",//"Sauna",
                "96" => "16",//"Duplex",
                "97" => "17",//"Walk-In Closet",
                "98" => "18",//"Laundry Room",
                "99" => "19",//"Children's Play Room",
                "100" => "20",//"Play Room",
                "101" => "21",//"Brand New Renovation",
                "102" => "22",//"Lift",
                "103" => "23",//"Stand-Alone House",
                "104" => "24",//"Jacuzzi",
                "105" => "25",//"Internal Staircase:",
                "106" => "27",//"Kitchen Pantry",
                "107" => "8",//"Pet Friendly",
                "108" => "28",//"Barbeque Area",
                "109" => "29",//"Mahjong rooms",
                "110" => "30",//"Family Room",
            );
        if (isset($feature_ids[$old_id])) {
            return $feature_ids[$old_id];
        } else {
            return '0';
        }
    }

    public function translate_locations($old_id) {
        $location_ids = array(
                "17"  => "4",//"Central",
                "18"  => "6",//"Chung Hom Kok",
                "19"  => "85",//"Clearwater Bay",
                "20"  => "7",//"Deep Water Bay",
                "21"  => "64",//"Discovery Bay",
                "22"  => "8",//"Happy Valley",
                "23"  => "27",//"Kennedy Town",
                "24"  => "3",//"Mid Levels Central",
                "25"  => "10",//"Mid Levels East",
                "26"  => "11",//"Mid Levels West",
                "27"  => "25",//"Peak",
                "28"  => "14",//"Pok Fu Lam",
                "29"  => "16",//"Repulse Bay",
                "30"  => "55",//"Sai Kung",
                "31"  => "100",//"Sai Ying Pun",
                "32"  => "20",//"Sheung Wan",
                "33"  => "205",//"Shek O",
                "34"  => "19",//"Shouson Hill",
                "35"  => "206",//"Silverstrand",
                "36"  => "207",//"South Bay",
                "37"  => "22",//"Stanley",
                "38"  => "24",//"Tai Tam",
                "39"  => "26",//"Wanchai",
                "61"  => "1",//"Causeway Bay",
                "83"  => "23",//"Tai Hang",
                "88"  => "0",//"International",
                "89"  => "0",//"French Riviera",
                "90"  => "0",//"Monaco",
                "91"  => "0",//"Hong Kong",
                "92"  => "0",//"Thailand",
                "111" => "28",//"Wong Chuk Hang",
            );
        if (isset($location_ids[$old_id])) {
            return $location_ids[$old_id];
        } else {
            return '0';
        }
    }

    public function translate_estate_purpose($str) {
        return $str=='rent'?1:2; 
    }

    public function index()
    {
        $data = $this->get_properties();
        \Excel::create('export-oldnestweb', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->export('csv');
    }

    private function get_properties() {
        $properties = DB::connection('mysql3')->table('wp_posts')->select($this->field_whitelist)
                        ->where('post_type', 'estate')
                        ->where('post_status', 'publish')
                        // ->where('ID', '9142') // new image field structure
                        // ->where('ID', '470') // old image field structure
                        ->get();
        $res =array();
        // dex($properties);
        foreach ($properties as $key => $property) {
            list($properties[$key]->feature_ids, $properties[$key]->feature_strings) = $this->get_features($property->ID);
            list($properties[$key]->location_id, $properties[$key]->location_str) = $this->get_locations($property->ID);
            $properties[$key]->property_meta = $this->get_property_meta($property->ID);
            
            $export_format = $this->get_export_format($properties[$key]);
            if (isset($export_format['location_id']) && $export_format['location_id']!=0) {
                $res[$property->ID] = $export_format;
            }
        }
        // dex(count($res));
        // dex($res);
        return $res;
    }

    private function get_export_format($property) {
        foreach ($this->meta_whitelist as $key => $value) {
            if (!isset($property->property_meta[$value])) {
                $property->property_meta[$value] = '';
            }
        }
        $feature_ids = !empty($property->feature_ids)?join(',', $property->feature_ids):'';
        // dex($property);
        $asking_rent = isset($property->property_meta['estate_purpose'])&&$property->property_meta['estate_purpose']==1?$property->property_meta['estate_price']:'0';
        $asking_sale = isset($property->property_meta['estate_purpose'])&&$property->property_meta['estate_purpose']!=1?$property->property_meta['estate_price']:'0';
        $res = array(
                "id"               => $property->ID,
                "name"             => $property->post_title,
                "slug"             => $property->post_name,
                "created"          => $property->post_date,
                
                "address"          => $property->property_meta['address'],                
                "location_id"      => $property->location_id,
                "location_str"     => $property->location_str,
                
                "feature_ids"      => $feature_ids,
                "feature_strings"  => $property->feature_strings,
                
                "gross_area"       => $property->property_meta['estate_area'],
                "saleable_area"    => $property->property_meta['net_size'],
                "type_id"          => $property->property_meta['estate_purpose'],
                "asking_rent"      => $asking_rent,
                "asking_sale"      => $asking_sale,
                
                "bedrooms"         => $property->property_meta['estate_bedrooms'],
                "bathrooms"        => $property->property_meta['estate_bathrooms'],
                "maids_room"       => $property->property_meta['maids_room'],
                
                "outdoor"          => $property->property_meta['outdoor'],
                "car_park"         => $property->property_meta['car_park'],
                "status"           => !empty($property->property_meta['estate_status'])?1:0,
                "featured"         => !empty($property->property_meta['estate_featured'])?1:0,
                
                "featured_image"   => $property->property_meta['_thumbnail_id'],
                
                "ref_key"          => $property->property_meta['ref_key'],
                "coordinates"      => $property->property_meta['estate_coordinates'],
                "reference_number" => $property->property_meta['reference_number'],
            );
        for($i=0; $i<30; $i++) {//max number of images.
            $res['image'.$i] = '';
        }
        if(!empty($property->property_meta['images'])) {
            foreach ($property->property_meta['images'] as $key => $image) {
                $res['image'.$key] = $image;
            }
        }
        $res['image_num'] = !empty($property->property_meta['images'])?count($property->property_meta['images']):0;
        // dex($res);
        return $res;
    }

    private function get_locations($property_id) {
        $sql_str = 'SELECT `wp_terms`.* FROM `wp_term_relationships` LEFT JOIN `wp_term_taxonomy` ON `wp_term_relationships`.`term_taxonomy_id` = `wp_term_taxonomy`.`term_taxonomy_id` LEFT JOIN `wp_terms` ON `wp_term_taxonomy`.`term_id` = `wp_terms`.`term_id` WHERE `object_id` = ? and `taxonomy` = ?';
        $locations = DB::connection('mysql3')->select($sql_str, [$property_id, 'estate-location']);
        if (empty($locations)) return array('','');
        $location = current($locations); 
        return array($this->translate_locations($location->term_id), $location->name);
    }

    private function get_features($property_id) {
        $sql_str = 'SELECT `wp_terms`.* FROM `wp_term_relationships` LEFT JOIN `wp_term_taxonomy` ON `wp_term_relationships`.`term_taxonomy_id` = `wp_term_taxonomy`.`term_taxonomy_id` LEFT JOIN `wp_terms` ON `wp_term_taxonomy`.`term_id` = `wp_terms`.`term_id` WHERE `object_id` = ? and `taxonomy` = ?';
        $features = DB::connection('mysql3')->select($sql_str, [$property_id, 'estate-type']);
        if (empty($features)) return array('','');
        $feature_ids = array();
        $feature_strings = array();
        foreach ($features as $key => $feature) {
            $feature_ids[] = $this->translate_features($feature->term_id);
            $feature_strings[] = $feature->name;
        }
        $feature_str = !empty($feature_strings)?join(', ', $feature_strings):'';
        return array($feature_ids, $feature_str);
    }

    private function get_property_meta($property_id) {
        $metas = DB::connection('mysql3')->table('wp_postmeta')
                        ->where('post_id', $property_id)->get();
        $_metas = array();
        foreach ($metas as $key => $meta) {
            if (in_array($meta->meta_key, $this->meta_whitelist)) {
                if ($meta->meta_key == '_estate_image_gallery') {
                    $_metas[$meta->meta_key] = $this->exception_estate_image_gallery($meta->meta_value);
                } else {
                    switch($meta->meta_key) {
                        case "estate_purpose":
                            $_metas[$meta->meta_key] = $this->translate_estate_purpose($meta->meta_value);
                            break;
                        case "_thumbnail_id":
                            $_metas[$meta->meta_key] = $this->translate_thumbnail_id($meta->meta_value);
                            break;
                        default:
                            $_metas[$meta->meta_key] = $meta->meta_value;
                            break;
                    }
                }
            }
            if (in_array($meta->meta_key, $this->meta_array_whitelist)) {
                $_metas[$meta->meta_key][] = $meta->meta_value;
            }
        }

        if (!empty($_metas['nestweb_image'])) {
            $_metas['images'] = $this->exception_nestweb_images($_metas['nestweb_image']);
            unset($_metas['nestweb_image']);
            if (isset($_metas['_estate_image_gallery'])) {
                unset($_metas['_estate_image_gallery']); 
            }
        }
        if (empty($_metas['images'])) {
            $_metas['images'] =$_metas['_estate_image_gallery'];
            unset($_metas['_estate_image_gallery']);
        }
        return $_metas;
        // dex($_metas);
        // dex($metas);
    }

    private function exception_estate_image_gallery($string) {
        $images = json_decode($string);
        if (empty($images))return array();
        $_images = array();
        foreach ($images as $key => $image) {
            $_images[] = $image->thumbnail;
        }
        return $_images;
    }

    private function exception_nestweb_images($image_ids) {
        if (empty($image_ids)) return array();
        $images = array();
        foreach ($image_ids as $key => $image_id) {
            $images[] = $this->exception_nestweb_image($image_id);
        }
        return $images;
    }

    private function translate_thumbnail_id($image_id) {
        return $this->exception_nestweb_image($image_id);
    }

    private function exception_nestweb_image($image_id) {
        if (empty($image_id)) return '';
        $image = DB::connection('mysql3')->table('wp_posts')
                ->where('post_type', 'attachment')
                ->where('ID', $image_id)->first();
        if (empty($image)) return '';
        return $image->guid;
    }
}
