<?php

namespace App\Http\Controllers;

use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;
use Log;
use Plupload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\LogProperties;
use App\Commission;
use App\Property;
use App\Userdocuments;

use View;
use App\Lead;
use App\LogClick;
use App\Calendar;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;


use Carbon\Carbon;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		if(!\Auth::check()){
			$redirect->to('/login')->send();
		}else{
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
			
			LogClick::logClick(3);
		}
    }


    /**
	 * Show form for User Settings
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function getsettings(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		$same = 'no';
		if (Hash::check('tim.com', $user->password)) {
			$same = 'yes';
		}
		
		
		
		
        return view('user.settings', [
			'same' => $same
		]);
    }
	
	
    /**
	 * Change password / check if old and new passwords are fine
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function changesettings(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		$rules = array(
			'old_password' => 'required',
			'new_password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
			'password_confirmation' => 'required|same:new_password',
		);
		$validator = Validator::make($request->all(), $rules);
		
		if (!Hash::check($request->old_password, $user->password)) {
			$validator->getMessageBag()->add('old_password', 'The provided old password is not correct.');
		}
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			$user->fill([
				'password' => Hash::make($request->new_password)
			])->save();
			$saved = true;
		}
		
        return view('user.settings', [
			'saved' => $saved
		])->withErrors($validator);
    }
	
	

    /**
	 * Show form for User Profile
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function getprofile(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		
		$year = intval(date('Y'));
		$userVacations = Calendar::where('user_id','=',$user->id)->where('type','=',3)->whereRaw('YEAR(datefrom) = '.$year)->get();			
		$vacation = 0;		
		foreach($userVacations as $userVacation){
			
			$from = new Carbon($userVacation->datefrom);
			$to =  new Carbon($userVacation->dateto);		
			
			$vacation = $vacation + $from->diff($to)->days + 1;

		}
		
		
        return view('user.profile', [
			'user' => $user,
			'vacation_used'=>$vacation
		]);
    }
	
	
    /**
	 * Change profile information
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function changeprofile(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		$rules = array(
			'name' => 'required|max:255',
			'firstname' => 'required|max:200',
			'lastname' => 'required|max:200',
			'gender' =>  'required|numeric',
			'phone' =>  'max:100',
			'agentid' =>  'max:50',
			'offtitle' =>  'max:200',
			'picture' =>  'image|mimes:jpg,jpeg,png',
			'signature' =>  'image|mimes:jpg,jpeg,png',
		);
		$validator = Validator::make($request->all(), $rules);
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			$user->fill([
				'name' => filter_var($input['name'], FILTER_SANITIZE_STRING),
				'firstname' => filter_var($input['firstname'], FILTER_SANITIZE_STRING),
				'lastname' => filter_var($input['lastname'], FILTER_SANITIZE_STRING),
				'gender' => filter_var($input['gender'], FILTER_SANITIZE_NUMBER_INT),
				'phone' => filter_var($input['phone'], FILTER_SANITIZE_STRING),
				'agentid' => filter_var($input['agentid'], FILTER_SANITIZE_STRING),
				'offtitle' => filter_var($input['offtitle'], FILTER_SANITIZE_STRING),
				'address' => filter_var($input['address'], FILTER_SANITIZE_STRING),
				'hkid' => filter_var($input['hkid'], FILTER_SANITIZE_STRING),
				'expensifyid' => filter_var($input['expensifyid'], FILTER_SANITIZE_STRING),
			])->save();
			$saved = true;
		}
		
		if (Input::hasFile('picture') && Input::file('picture')->isValid() && $validator->errors()->count() == 0){
			$img = Image::make(Input::file('picture')->getRealPath());
			$path = 'images/users/';
			$nameori = $user->id.'_'.time().'_ori.'.Input::file('picture')->getClientOriginalExtension();
			$name = $user->id.'_'.time().'.'.Input::file('picture')->getClientOriginalExtension();
			
			$img->save($path.$nameori);
			$img->resize(300, 300)->save($path.$name);
			
			$user->fill([
				'picpath' => filter_var('showimage/users/'.$name, FILTER_SANITIZE_STRING)
			])->save();
		}
		
		if (Input::hasFile('signature') && Input::file('signature')->isValid() && $validator->errors()->count() == 0){
			$img = Image::make(Input::file('signature')->getRealPath());
			$path = '../storage/signatures/';
			$nameori = $user->id.'_'.time().'_ori.'.Input::file('signature')->getClientOriginalExtension();
			
			$img->save($path.$nameori);
			
			$user->fill([
				'sigpath' => filter_var('showsig/'.$nameori, FILTER_SANITIZE_STRING)
			])->save();
		}
		
        return view('user.profile', [
			'user' => $user,			
			'saved' => $saved
		])->withErrors($validator);;
    }
	
	
    /**
	 * Show All Users
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function admusers(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboard');
		}
		
		//load all users
		$usersObj = User::all();
		$users = array();
		
		foreach ($usersObj as $u){
			//if ($u->id != $user->id){
				$uarr = $u->toArray();
				$uarr['rightsstr'] = $u->getUserRightsString();
				
				$users[] = $uarr;
			//}
		}
		
		for ($i = 0; $i < count($users)-1; $i++){
			for ($a = $i+1; $a < count($users); $a++){
				if ($users[$i]['name'] > $users[$a]['name']){
					$help = $users[$i];
					$users[$i] = $users[$a];
					$users[$a] = $help;
				}
			}
		}
		
        return view('user.admusers', [
			'users' => $users
		]);
    }
	
    /**
	 * Show particular Users
	 *
	 * @param  Request  $request
	 * @param  User ID $id
	 * @return Response
	 */
	public function admuser(Request $request, $id)
    {
        $input = $request->all();
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] || $rights['Admin']){
			return redirect('dashboard');
		}
		
		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboard');			
		}
		$rights = $userObj->getUserRights();
		if ($rights['Admin']){
			$rights['Superadmin'] = false;
		}
		$rolenames = $userObj->getUserRightsNameArr();
		$roles = array_keys($rolenames);
		
		
		$usersObj = User::all();
		$users = array();
		foreach ($usersObj as $u){
			if ($u->id != $id){
				$uarr = $u->toArray();
				$users[] = $uarr;
			}
		}
		
		$managing = $userObj->getUserManaging();

		$salesLeaders = $userObj->getSalesLeaders();

		$year = intval(date('Y'));

		$userVacations = Calendar::where('user_id','=',$id)->where('type','=',3)->whereRaw('YEAR(datefrom) = '.$year)->get();
		
		$vacation = 0;
	
		foreach($userVacations as $userVacation){
			
			$from = new Carbon($userVacation->datefrom);
			$to =  new Carbon($userVacation->dateto);		
			
			$vacation = $vacation + $from->diff($to)->days + 1;

		}

		
        return view('user.admuser', [
			'user' => $userObj,
			'roles' => $roles,
			'rights' => $rights,
			'rolenames' => $rolenames,
			'users' => $users,
			'managing' => $managing,
			'salesLeaders' => $salesLeaders,
			'vacation_used' => $vacation
		]);
    }
	
	
    /**
	 * Change & show particular Users
	 *
	 * @param  Request  $request
	 * @param  User ID $id
	 * @return Response
	 */
	public function changeadmuser(Request $request, $id)
    {
        $input = $request->all();
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboard');
		}
		
		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboard');			
		}
	
		//Save things
		$rules = array(
			'name' => 'required|max:255',
			'firstname' => 'required|max:200',
			'lastname' => 'required|max:200',
			'gender' =>  'required|numeric',
			'status' =>  'required|numeric',
			'phone' =>  'max:100',
			'agentid' =>  'max:50',
			'offtitle' =>  'max:200',
			'picture' =>  'image|mimes:jpg,jpeg,png',
		);
		$validator = Validator::make($request->all(), $rules);

	
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			
			if($userObj->allowed_vacation != $input['allowed_vacation']){
				$userObj->fill(['remaining_vacation' => trim(filter_var($input['allowed_vacation'], FILTER_SANITIZE_STRING))])->save();
			}

			$userObj->fill([
				'name' => trim(filter_var($input['name'], FILTER_SANITIZE_STRING)),
				'firstname' => trim(filter_var($input['firstname'], FILTER_SANITIZE_STRING)),
				'lastname' => trim(filter_var($input['lastname'], FILTER_SANITIZE_STRING)),
				'gender' => filter_var($input['gender'], FILTER_SANITIZE_NUMBER_INT),
				'phone' => trim(filter_var($input['phone'], FILTER_SANITIZE_STRING)),
				'agentid' => trim(filter_var($input['agentid'], FILTER_SANITIZE_STRING)),
				'offtitle' => trim(filter_var($input['offtitle'], FILTER_SANITIZE_STRING)),
				'address' => filter_var($input['address'], FILTER_SANITIZE_STRING),
				'color' => trim(filter_var($input['color'], FILTER_SANITIZE_STRING)),
				//'allowed_vacation' => trim(filter_var($input['allowed_vacation'], FILTER_SANITIZE_STRING)),								
				//'vacation_tier' => trim(filter_var($input['vacation_tier'], FILTER_SANITIZE_STRING)),								
				'commstruct' => filter_var($input['commstruct'], FILTER_SANITIZE_NUMBER_INT),
				'status' => filter_var($input['status'], FILTER_SANITIZE_NUMBER_INT),
				'hkid' => filter_var($input['hkid'], FILTER_SANITIZE_STRING),
				'expensifyid' => filter_var($input['expensifyid'], FILTER_SANITIZE_STRING),
				'startdate' => $input['startdate'],
				'enddate' => $input['enddate']
			])->save();
				
                        if($input['commstruct']==4){
                            
                            $commstruct_customArray =array(
                                
                                "type"=>$input['commstruct_custom_type'],
                                "currency"=>$input['commstruct_custom_currency'],
                                "amount"=>$input['commstruct_custom_amount']
                                );
                            
                        }else{

                            $commstruct_customArray =array(
                                
                                "type"=>'',
                                "currency"=>'',
                                "amount"=>''
                                ); 
                            
                        }
                        
                        $commstruct_customJsonString=json_encode($commstruct_customArray, JSON_FORCE_OBJECT);
                        DB::table('users')->where('id',$id)->update(array(
                             'commstruct_custom'=>$commstruct_customJsonString,
                          ));
			
			$saved = true;
		}
		
		if (Input::hasFile('picture') && Input::file('picture')->isValid() && $validator->errors()->count() == 0){
			$img = Image::make(Input::file('picture')->getRealPath());
			$path = 'images/users/';
			$nameori = $userObj->id.'_'.time().'_ori.'.Input::file('picture')->getClientOriginalExtension();
			$name = $userObj->id.'_'.time().'.'.Input::file('picture')->getClientOriginalExtension();
			
			$img->save($path.$nameori);
			$img->resize(300, 300)->save($path.$name);
			
			$userObj->fill([
				'picpath' => filter_var('showimage/users/'.$name, FILTER_SANITIZE_STRING)
			])->save();
		}
		
		//Rights
		$rights = $userObj->getEmptyRights();
		if (isset($input['rights'])){
			$inprights = $input['rights'];
			$inp = array();
			foreach ($inprights as $i){
				$inp[$i] = 1;
			}
			if (!empty($rights)){
				foreach ($rights as $key => $val){
					if (isset($inp[$key])){
						$rights[$key] = true;
					}
				}
			}
		}
		$userObj->setUserRights($rights);
		
		$roles = array_keys($rights);
		$rolenames = $userObj->getUserRightsNameArr();
		
		//Managing
		if (isset($input['managing'])){
			$inprights = $input['managing'];
			$inp = array();
			foreach ($inprights as $i){
				$inp[$i] = 1;
			}
			$userObj->fill([
				'managing' => json_encode($inp)
			])->save();
		}

		//sales Leaders 
		$leaders = array();
		if (!empty($input['sales_leaders'])) {
			foreach ($input['sales_leaders'] as $l) {
				$leaders[$l] = 1;
			}
		}

		$userObj->fill([
			'sales_leaders' => json_encode($leaders)
		])->save();

		//Password
		if (trim($request->new_password) != ''){
			$rules = array(
				'new_password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
				'password_confirmation' => 'required|same:new_password',
			);
			$validator2 = Validator::make($request->all(), $rules);
			
			$saved = false;		
			if ($validator2->errors()->count() == 0){
				$userObj->fill([
					'password' => Hash::make($request->new_password)
				])->save();
				$saved = true;
			}else{
				$errors = $validator2->errors();
				if ($errors->get('new_password')){
					foreach ($errors->get('new_password') as $msg){
						$validator->getMessageBag()->add('new_password', $msg);
					}
				}
				if ($errors->get('password_confirmation')){
					foreach ($errors->get('password_confirmation') as $msg){
						$validator->getMessageBag()->add('password_confirmation', $msg);
					}
				}
			}
		}
		
		
		$usersObj = User::all();
		$users = array();
		foreach ($usersObj as $u){
			if ($u->id != $id){
				$uarr = $u->toArray();
				$users[] = $uarr;
			}
		}
		
		$managing = $userObj->getUserManaging();

		$salesLeaders = $userObj->getSalesLeaders();


		$year = intval(date('Y'));

		$userVacations = Calendar::where('user_id','=',$id)->where('type','=',3)->whereRaw('YEAR(datefrom) = '.$year)->get();
		
		$vacation = 0;
	
		foreach($userVacations as $userVacation){
			
			$from = new Carbon($userVacation->datefrom);
			$to =  new Carbon($userVacation->dateto);		
			
			$vacation = $vacation + $from->diff($to)->days + 1;

		}
		
        return view('user.admuser', [
			'user' => $userObj,
			'roles' => $roles,
			'rights' => $rights,
			'rolenames' => $rolenames,		
			'saved' => $saved,
                        'commstruct_customArray' =>  $commstruct_customArray,
			'users' => $users,
			'managing' => $managing,
			'salesLeaders' => $salesLeaders,
			'vacation_used' => $vacation
		])->withErrors($validator);
    }
	
	
	
    /**
	 * Show new User form
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function newuser(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboard');
		}
		
		$emptyusr = array(
			'email' => '',
			'name' => '',
			'gender' => 0,
			'firstname' => '',
			'lastname' => ''
		);
		
        return view('user.newuser', [
			'user' => $emptyusr
		]);
    }
	
    /**
	 * Show new User form
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function createuser(Request $request)
    {
        $input = $request->all();
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director']){
			return redirect('dashboard');
		}
		
		//Save things
		$rules = array(
			'email' => 'required|max:255|unique:users',
			'name' => 'required|max:255',
			'firstname' => 'required|max:200',
			'lastname' => 'required|max:200',
			'gender' =>  'required|numeric',
			'new_password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
			'password_confirmation' => 'required|same:new_password',
		);
		$validator = Validator::make($request->all(), $rules);
		
		$emptyusr = array(
			'email' => trim(filter_var($input['email'], FILTER_SANITIZE_EMAIL)),
			'name' => trim(filter_var($input['name'], FILTER_SANITIZE_STRING)),
			'firstname' => trim(filter_var($input['firstname'], FILTER_SANITIZE_STRING)),
			'lastname' => trim(filter_var($input['lastname'], FILTER_SANITIZE_STRING)),
			'gender' => filter_var($input['gender'], FILTER_SANITIZE_NUMBER_INT),
		);
		
		if ($validator->errors()->count() == 0){
			//create user
			
			$emptyusr['password'] = Hash::make($request->new_password);
			
			$user = User::create($emptyusr);
			
			return redirect('/user/admuser/'.$user->id);
		}else{
			return view('user.newuser', [
				'user' => $emptyusr
			])->withErrors($validator);
		}
    }
	
	public function userlist(Request $request){
        $input = $request->all();
		$user = $request->user();
		
		$usersObj = User::all();
		$users = array();
		
		foreach ($usersObj as $u){
			$uarr = $u->toArray();
			$uarr['rightsstr'] = $u->getUserRightsString();
			
			$users[] = $uarr;
		}
		
		for ($i = 0; $i < count($users)-1; $i++){
			for ($a = $i+1; $a < count($users); $a++){
				if ($users[$i]['name'] > $users[$a]['name']){
					$help = $users[$i];
					$users[$i] = $users[$a];
					$users[$a] = $help;
				}
			}
		}
		
        return view('user.list', [
			'users' => $users
		]);
    }
	
	public function showuser(Request $request, $id){
        $input = $request->all();
		$user = $request->user();
		
		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboard');			
		}
		$rights = $userObj->getUserRights();
		$roles = array_keys($rights);
		$rolenames = $userObj->getUserRightsNameArr();
		
		//Get all changes for that user within last 28 days
		$changesfull = LogProperties::where('userid', '=', $id)->whereRaw('logtime >= DATE_SUB(NOW(), INTERVAL 15 DAY)')->orderBy('logtime', 'desc')->get();
		$changesfull2 = LogProperties::where('userid', '=', $id)->whereRaw('logtime >= DATE_SUB(NOW(), INTERVAL 15 DAY)')->orderBy('logtime', 'desc')->paginate(25);

		$allids = array();
		foreach ($changesfull as $change){
			$allids[$change->id] = 1;
		}
		$ids = array_keys($allids);
		
		//Get all changes for all ids
		$changes = LogProperties::whereIn('id', $ids)->orderBy('logtime', 'desc')->get();
		$date = date_create();
		date_modify($date, '-15 day');
		$d = date_format($date, 'Y-m-d H:i:s');
		$changelist = array();
		foreach ($changes as $change){
			if (isset($changelist[$change->id])){
				$changelist[$change->id][] = $change;
			}else{
				if ($change->userid == $id){
					$changelist[$change->id] = array();
					$changelist[$change->id][] = $change;
				}
			}
		}
		
		$changelistout = array();
		foreach ($changelist as $change){
			$changelistout[] = $change;
		}
		$changelist = $changelistout;
		
		for ($i = 0; $i < count($changelist)-1; $i++){
			for ($a = $i+1; $a < count($changelist); $a++){
				if ($changelist[$i][0]->logtime < $changelist[$a][0]->logtime){
					$help = $changelist[$i];
					$changelist[$i] = $changelist[$a];
					$changelist[$a] = $help;
				}
			}
		}
		
		$invoices = Commission::where('consultantid', $id)->whereRaw('commissiondate > DATE_ADD(NOW(), INTERVAL -6 MONTH)')->orderBy('commissiondate', 'desc')->get();
		$propertyids = array();
		foreach ($invoices as $invoice){
			$propertyids[] = $invoice->propertyid;
		}
		$props = Property::whereIn('id', $propertyids)->get();
		$propertynames = array();
		foreach ($props as $p){
			$propertynames[$p->id] = $p;
		}
		
		$docuploads = Userdocuments::where('docuserid', '=', $id)->get();

		///for the major changes pagination, data preparation
		$arr = array();
		$newprop = array();
		
		foreach($changelist as $list){
			$first = $list[0];
			$last = null;
			if (isset($list[1])){

				$last = $list[1];
				
				for ($i = 2; $i < count($list); $i++){
					if ($list[$i]->logtime > $d){
						$last = $list[$i];
						$last = $list[$i];						
					}
				}
				
			} 		

			if ($last == null || $first->available_date != $last->available_date || $first->pictst != $last->pictst || $first->owner_id != $last->owner_id || $first->agent_id != $last->agent_id || $first->rep_ids != $last->rep_ids || $first->agent_ids != $last->agent_ids || $first->asking_rent != $last->asking_rent || $first->asking_sale != $last->asking_sale){
				$changelist2[] = array($first->name);
				
				$arr[] = array($first, $last);
			}

			//new prrop
			if  ($last == null || ($first->created_at == $last->updated_at)){								
				$newprop[] = array($first, $last);
			}


		}
		
		

		$arr2 = $this->paginate($arr,25);
		$request->session()->put('majorlogs', $arr);	

		$newprop2 = $this->paginate($newprop,25);
		$request->session()->put('newprop', $newprop);	

		
		
        return view('user.show', [
			'user' => $userObj,
			'roles' => $roles,
			'rights' => $rights,
			'rolenames' => $rolenames,
			'd' => $d,
			//'changelist' => $changelist,
			'changelist' => $arr2,
			'newpropertieslogs' => $newprop2,
			'id' => $id,
			'changesfull' => $changesfull2,
			'invoices' => $invoices,
			'propertynames' => $propertynames,
			'docuploads' => $docuploads
		]);
	}

	public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
	}
	

	public function fetch_major_changes(Request $request, $id){
		
		if($request->ajax())
		{
			$page = intval($request->get('page'));
			$arr = $request->session()->get('majorlogs','default');

			$date = date_create();
			date_modify($date, '-15 day');
			$d = date_format($date, 'Y-m-d H:i:s');
			
			$changelist = $this->paginate($arr,25,$page);
			
			return view('user.paginationmajorupdates', compact('changelist','d'))->render();
		}	
		
	}

	public function fetch_newproperties(Request $request, $id){
		
		if($request->ajax())
		{
			$page = intval($request->get('page'));
			$arr = $request->session()->get('newprop','default');

			$date = date_create();
			date_modify($date, '-15 day');
			$d = date_format($date, 'Y-m-d H:i:s');
			
			$newpropertieslogs = $this->paginate($arr,25,$page);
			
			return view('user.paginationnewproperties', compact('newpropertieslogs','d'))->render();
		}	
		
	}
	
	public function fetch_data(Request $request, $id){
		
		
		if($request->ajax())
		{			
		
			$changesfull = LogProperties::where('userid', '=', $id)->whereRaw('logtime >= DATE_SUB(NOW(), INTERVAL 15 DAY)')->orderBy('logtime', 'desc')->paginate(25);
		//	print_r($changesfull);
			return view('user.paginationallchanges', compact('changesfull'))->render();
		}
	}
	
	
    public function docstoreupload($docuserid, Request $request){
		$input = $request->all();
		$docuserid = intval($docuserid);		
		
		$rules = array(
			'upfile' =>  'mimes:pdf,jpeg,jpg,png'
		);
		$validator = Validator::make($request->all(), $rules);
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			if (Input::hasFile('upfile') && Input::file('upfile')->isValid() && $validator->errors()->count() == 0){
		
				$path = '../storage/userdocs/';
				$ext = strtolower(Input::file('upfile')->getClientOriginalExtension());
				$name = $docuserid.'_'.time().'_'.$input['doctype'].'.'.$ext;
				Input::file('upfile')->move($path, $name);
				$type =$input['doctype'];
				$doc = new Userdocuments();
				$doc->doctype = $type;
				$doc->docuserid = $docuserid;
				$doc->userid = $request->user()->id;
				$doc->docpath = $path.$name;
			
				$doc->save();
				
			}
		}
		return redirect('/user/show/'.$docuserid);					
		//return redirect('/user/show/'.$docuserid.'?error=true');
	}
	
	public function showdoc(Request $request, $docid){
		$user = $request->user();	
		
		$doc = Userdocuments::where('id', '=', $docid)->first();
		
		if ($doc){
			if ($doc && ($doc->userid == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant'])){
				if (file_exists($doc->docpath)){
					return response()->download($doc->docpath);
				}
			}
		}else{
			return redirect('/');
		}
	}
	
	public function removedoc(Request $request, $docid, $docuserid){	
		$user = $request->user();	
		
		$doc = Userdocuments::where('id', '=', $docid)->first();
		
		if ($doc && ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant'])){
			$doc->delete();
			
			return redirect('/user/show/'.$docuserid);
		}else{
			return redirect('/user/list');
		}
	}
	
	public function savefromto(Request $request, $id)
    {
        $input = $request->all();
		$user = $request->user();
		
		//check if user has the rights
		$rights = $user->getUserRights();
		if (!$rights['Superadmin'] && !$rights['Director'] && !$rights['Accountant']){
			return redirect('dashboard');
		}
		
		//load user
		$userObj = User::where('id', $id)->first();
		if (!$userObj || empty($userObj)){
			return redirect('dashboard');			
		}
		
		$userObj->fill([
			'startdate' => $input['startdate'],
			'enddate' => $input['enddate']
		])->save();
		
        return redirect('/user/show/'.$id);
    }


	public function update_vacation(Request $request, $id, $number_vacation, $vacation_tier)
    {
		$vacationUser = User::where('id', $id)->first();		
		$vacationUser->allowed_vacation = $number_vacation;
		$vacationUser->vacation_tier = $vacation_tier;
		$vacationUser->save();
	}
	
	
}
