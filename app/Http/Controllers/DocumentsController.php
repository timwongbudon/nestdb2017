<?php

namespace App\Http\Controllers;

use App\Client;
use Input;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

use App\DocumentFields;
use App\ClientShortlist;
use App\Property;
use App\Document;
use App\Documentinfo;
use App\Documentuploads;
use App\User;
use App\Commission;
use App\Propertydocuments;
use App\Clientdocuments;

use View;
use App\Lead;
use App\LogClick;

use Dompdf\Dompdf;
use Dompdf\Options;

class DocumentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(8);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
					
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		
		
		return view('documents.index');
    }
	
    public function indexlease($shortlistid){
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}
		
		$doc = Documentinfo::where('shortlist_id', $shortlistid)->where('type_id', $typeid)->first();
		if (!$doc){
			$doc = new Documentinfo();
			$doc->fill(array(
				'client_id' => $shortlist['client']->id,
				'shortlist_id' => $shortlistid,
				'type_id' => $typeid
			));
			$doc->save();
		}
		$props = explode('|', $doc->properties);
		$properties = Property::whereIn('id', $props)->withTrashed()->get();
		
		$rows = Document::where('shortlistid', '=', $shortlistid)->get();
		$docs = array();
		$docsprops = array();
		$offerletters = array();
		foreach ($rows as $d){
			if (!isset($docs[$d->doctype])){
				$docs[$d->doctype] = array();
			}
			$docs[$d->doctype][$d->propertyid] = $d;
			if (!isset($docsprops[$d->propertyid])){
				$phelper = Property::where('id', $d->propertyid)->withTrashed()->first();
				if ($phelper){
					$docsprops[$d->propertyid] = $phelper;
				}
			}
			if (isset($docsprops[$d->propertyid])){
				if ($d->doctype == 'offerletter'){
					if (!isset($offerletters[$d->propertyid])){
						$offerletters[$d->propertyid] = array();
					}
					$offerletters[$d->propertyid][$d->revision] = $d;
				}
			}
		}
		
		$docuploads = Documentuploads::where('shortlistid', '=', $shortlistid)->get();
		$comm = Commission::where('shortlistid', '=', $shortlist->id)->get();
		
		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}
		
		$propdocs = array();
		foreach ($properties as $p){
			$propdocs[$p->id] = Propertydocuments::where('propertyid', '=', $p->id)->withTrashed()->get();
		}
		$clientdocs = Clientdocuments::where('clientid', '=', $shortlist['client']->id)->get();
		
		
		return view('documents.index-lease', [
			'shortlist' => $shortlist,
			'docinfo' => $doc,
			'properties' => $properties,
			'docs' => $docs,
			'docsprops' => $docsprops,
			'docuploads' => $docuploads,
			'consultants' => $consultants,
			'commission' => $comm,
			'propdocs' => $propdocs,
			'clientdocs' => $clientdocs,
			'offerletters' => $offerletters
		]);
	}
	
    public function indexsale($shortlistid){
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}
		$doc = Documentinfo::where('shortlist_id', $shortlistid)->where('type_id', $typeid)->first();
		if (!$doc){
			$doc = new Documentinfo();
			$doc->fill(array(
				'client_id' => $shortlist['client']->id,
				'shortlist_id' => $shortlistid,
				'type_id' => $typeid
			));
			$doc->save();
		}
		$props = explode('|', $doc->properties);
		$properties = Property::whereIn('id', $props)->get();
		
		$rows = Document::where('shortlistid', '=', $shortlistid)->get();
		$docs = array();
		$docsprops = array();
		foreach ($rows as $d){
			if (!isset($docs[$d->doctype])){
				$docs[$d->doctype] = array();
			}
			$docs[$d->doctype][$d->propertyid] = $d;
			if (!isset($docsprops[$d->propertyid])){
				$docsprops[$d->propertyid] = Property::where('id', $d->propertyid)->first();
			}
		}
		
		$docuploads = Documentuploads::where('shortlistid', '=', $shortlistid)->get();
		$comm = Commission::where('shortlistid', '=', $shortlist->id)->get();
		
		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}
		
		$propdocs = array();
		foreach ($properties as $p){
			$propdocs[$p->id] = Propertydocuments::where('propertyid', '=', $p->id)->get();
		}
		$clientdocs = Clientdocuments::where('clientid', '=', $shortlist['client']->id)->get();
		
		return view('documents.index-sale', [
			'shortlist' => $shortlist,
			'docinfo' => $doc,
			'properties' => $properties,
			'docs' => $docs,
			'docsprops' => $docsprops,
			'docuploads' => $docuploads,
			'consultants' => $consultants,
			'commission' => $comm,
			'propdocs' => $propdocs,
			'clientdocs' => $clientdocs
		]);
	}
	
    public function offerletteredit($shortlistid, $propertyid, $revision){
		$docversion = DocumentFields::getCurDocVersions('ol');
	
		
		$data = array(
			'doctype' => 'offerletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0,
			'revision' => 1
		);
		$new = true;
		$clientId="";
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->where('revision', '=', $revision)->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
			$clientId=$row->clientid;
		}

	
		
		$fieldslist = DocumentFields::getOfferLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getOfferLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
				
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'The Landlord';
			$data['fieldcontents']['f4'] = '';
			$data['fieldcontents']['f5'] = 'SUBJECT TO CONTRACT';
			$data['fieldcontents']['f6'] = $property->name;
			$data['fieldcontents']['f7'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
			$data['fieldcontents']['f8'] = $shortlist['client']->title . ($shortlist['client']->title && $shortlist['client']->workplace ? ', ' : '') . $shortlist['client']->workplace;
			$data['fieldcontents']['f9'] = $shortlist['client']->address1;
			$data['fieldcontents']['f10'] = $shortlist['client']->address2;
			$data['fieldcontents']['f11'] = $shortlist['client']->address3;
			$data['fieldcontents']['f12'] = $shortlist['client']->hkidpassport;
			$data['fieldcontents']['f13'] = $property->asking_rent;
			if ($consultant){
				$data['fieldcontents']['f28'] = $consultant->email;
				$data['fieldcontents']['f29'] = $consultant->phone;
				$data['fieldcontents']['f30'] = $consultant->name;
				$data['fieldcontents']['f31'] = $consultant->offtitle;
				$data['fieldcontents']['f32'] = $consultant->agentid;
			}
			$data['fieldcontents']['f'] = '';
			$data['replace']['mngfees'] = 'For the account of the Landlord.';
			$data['replace']['govrates'] = 'For the account of the Landlord.';
			$data['replace']['govrent'] = 'For the account of the Landlord.';
			$data['fieldcontents']['f42'] = 'hide';
			$data['fieldcontents']['f49'] = 'Tenant';
			$data['fieldcontents']['f53'] = 'Two (2) years';
			$data['fieldcontents']['f54'] = 'Yes';
			$data['fieldcontents']['f55'] = 'two (2) months\' written notice';
			
			$data['fieldcontents']['f40']  = 'To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To revarnish all natural wood surfaces including doors.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To provide blinds or curtain rails and curtains throughout.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To provide one standard-sized washing machine and clothes dryer.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To repair any defects.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To clean the premises throughout including the interior of windows and cupboards.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.'.PHP_EOL;
			$data['fieldcontents']['f40'] .= 'A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties. ';

			$clientId = $shortlist['client']->id;

		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			
			if(Input::get('offering')!== null){
				$val = $row->fieldcontents;				

				$client = Client::find($row->clientid);
				

				$t = json_decode($val);
				$t->f8 = $client->title.", ".$client->workplace;
				$t->f9 = $client->address1;
				$t->f7 = $client->firstname." ".$client->lastname;
				$t->f12 = $client->hkidpassport;

				$row->fieldcontents = json_encode(($t));
			//print_r(json_decode($row->fieldcontents)->f8);
			//die;
			}
			

			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['fieldcontents']['f'] = '';
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			$data['revision'] = $row->revision;
			if (!isset($data['fieldcontents']['f42']) || $data['fieldcontents']['f42'] == ''){
				$data['fieldcontents']['f42'] = 'hide';
			}
			if (!isset($data['fieldcontents']['f49']) || $data['fieldcontents']['f49'] == ''){
				$data['fieldcontents']['f49'] = 'Tenant';
			}
			if (!isset($data['fieldcontents']['f53']) || $data['fieldcontents']['f53'] == ''){
				$data['fieldcontents']['f53'] = 'Two (2) years';
			}
			if (!isset($data['fieldcontents']['f54']) || $data['fieldcontents']['f54'] == ''){
				$data['fieldcontents']['f54'] = 'Yes';
			}
			if (!isset($data['fieldcontents']['f55']) || $data['fieldcontents']['f55'] == ''){
				$data['fieldcontents']['f55'] = 'two (2) months\' written notice';
			}
		}
		
		$replaceFields = DocumentFields::getOfferLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		$replaceFieldsDesc = DocumentFields::getOfferLetterReplaceAutoText($docversion);
		$helppics = DocumentFields::getOfferLetterHelpPics($docversion);
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}

		//get Lead to get other consultants
		//$lead = Lead::where('shortlistid', '=', $shortlistid)->orderBy('id', 'desc')->first();
		$other_consultant = !empty($row->shared_with) ? $row->shared_with : null;

		$sharedTo = array();
		
		if($other_consultant != null){
			$other_consultant = json_decode($other_consultant );
			
			foreach($other_consultant as $t){
				$sharedTo[] =  User::where('id',$t)->first();
			}

		}

	

		
		
		return view('documents.offer-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc,
			'helppics' => $helppics,
			'clientid' => $clientId,
			'sharedTo' => $sharedTo,
			
		]);
    }
	
	public function offerletterupdate(Request $request){
		$input = $request->all();

		$this->validate($request, DocumentFields::$offer_letter_validations, DocumentFields::$offer_letter_messages);
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getOfferLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}

			//get Lead to get other consultants
			$lead = Lead::where('shortlistid', '=', $input['shortlistid'])->orderBy('id', 'desc')->first();
			$other_consultant = !empty($lead->shared_with) ? $lead->shared_with : null;
			
			$data = array(
				'doctype' => 'offerletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace),
				'shared_with' => $other_consultant,
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		$revision = 1;
		if ($doc && is_numeric($doc->revision)){
			$revision = $doc->revision;
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/offerletter/'.intval($input['propertyid']).'/'.$revision);
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function offerletternewversion($shortlistid, $propertyid){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('revision', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		
		$data = array(
			'doctype' => 'offerletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => $row->sectionshidden,
			'fieldcontents' => $row->fieldcontents,
			'replace' => $row->replace,
			'consultantid' => $row->consultantid,
			'clientid' => $row->clientid,
			'id' => 0,
			'revision' => $row->revision+1
		);
		
		$doc = Document::create($data);
		$doc->save();
		
		return redirect('/documents/index-lease/'.$shortlistid);
	}
	
	
    public function offerletterpdf($shortlistid, $propertyid, $revision){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->where('revision', '=', $revision)->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'offerletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getOfferLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getOfferLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);

		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getOfferLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		if (!isset($data['fieldcontents']['f53']) || $data['fieldcontents']['f53'] == ''){
			$data['fieldcontents']['f53'] = 'Two (2) years';
		}
		if (!isset($data['fieldcontents']['f54']) || $data['fieldcontents']['f54'] == ''){
			$data['fieldcontents']['f54'] = 'Yes';
		}
		if (!isset($data['fieldcontents']['f55']) || $data['fieldcontents']['f55'] == ''){
			$data['fieldcontents']['f55'] = 'two (2) months\' written notice';
		}
		if (!isset($data['fieldcontents']['f58']) || $data['fieldcontents']['f58'] == ''){
			$data['fieldcontents']['f58'] = 'HKID';
		}
		if (!isset($data['fieldcontents']['f59']) || $data['fieldcontents']['f59'] == ''){
			$data['fieldcontents']['f59'] = 'HKID';
		}
		
		$html = view('documents.offer-letter-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Offer to Lease '.date('d-m-Y').'.pdf';
		
        // return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
    public function tenancyagreementedit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('ta');
		
		$data = array(
			'doctype' => 'tenancyagreement',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenancyagreement')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getTenancyAgreementFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getTenancyAgreementShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');

			$data['fieldcontents']['f7'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
			$data['fieldcontents']['f18'] = $shortlist['client']->hkidpassport;

			$data['fieldcontents']['f8'] = $shortlist['client']->title;
			if($shortlist['client']->title){
				$data['fieldcontents']['f8'] .= ", ";
			}
			$data['fieldcontents']['f8'] .= $shortlist['client']->workplace;
			if($shortlist['client']->title || $shortlist['client']->workplace){
				$data['fieldcontents']['f8'] .= "\n";
			}
			$data['fieldcontents']['f8'] .= $shortlist['client']->address1;
			if($shortlist['client']->address1){
				$data['fieldcontents']['f8'] .= "\n";
			}
			$data['fieldcontents']['f8'] .= $shortlist['client']->address2;
			if($shortlist['client']->address2){
				$data['fieldcontents']['f8'] .= "\n";
			}
			$data['fieldcontents']['f8'] .= $shortlist['client']->address3;

			/*$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f4'] = $shortlist['client']->firstname;
			$data['fieldcontents']['f6'] = $property->name;
			$data['fieldcontents']['f7'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
			$data['fieldcontents']['f8'] = $shortlist['client']->workplace;
			$data['fieldcontents']['f13'] = $property->asking_rent;
			if ($consultant){
				$data['fieldcontents']['f28'] = $consultant->email;
				$data['fieldcontents']['f29'] = $consultant->phone;
				$data['fieldcontents']['f30'] = $consultant->name;
				$data['fieldcontents']['f32'] = $consultant->agentid;
			}*/
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Landlord
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f36'];
				//Premises
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f6'];
				//minimum period
				$data['fieldcontents']['f3'] = $offerletterfieldcontents['f26'];
				//Tenant name
				if (isset($offerletterfieldcontents['f42']) && $offerletterfieldcontents['f42'] == 'show'){
					$data['fieldcontents']['f7'] = $offerletterfieldcontents['f7'].' & '.$offerletterfieldcontents['f43'];
				}else{
					$data['fieldcontents']['f7'] = $offerletterfieldcontents['f7'];
				}
				//Tenant further details
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f8'].PHP_EOL.$offerletterfieldcontents['f9'].PHP_EOL.$offerletterfieldcontents['f10'].PHP_EOL.$offerletterfieldcontents['f11'];
				//Break Clause
				if (isset($offerletterfieldcontents['f49'])){
					$data['fieldcontents']['f22'] = $offerletterfieldcontents['f49'];
				}
				$data['fieldcontents']['f18'] = $offerletterfieldcontents['f12'];
			}
			
			$holdingdepositletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'holdingdepositletter')->orderBy('id', 'desc')->first();
			if ($holdingdepositletter){
				$fieldcontents = json_decode($holdingdepositletter->fieldcontents, true);
				//Landlord name
				$data['fieldcontents']['f5'] = $fieldcontents['f2'];
				//Landlord address
				$data['fieldcontents']['f6'] = $fieldcontents['f3'];
			}
			
			$tenantfeeletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenantfeeletter')->orderBy('id', 'desc')->first();
			if ($tenantfeeletter){
				$fieldcontents = json_decode($tenantfeeletter->fieldcontents, true);
				//Tenant name
				$data['fieldcontents']['f7'] = $fieldcontents['f4'];
				//Tenant address
				$data['fieldcontents']['f8'] = $fieldcontents['f5'];
			}
			
			$landlordfeeletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'landlordfeeletter')->orderBy('id', 'desc')->first();
			if ($landlordfeeletter){
				$fieldcontents = json_decode($landlordfeeletter->fieldcontents, true);
				//Landlord name
				$data['fieldcontents']['f5'] = $fieldcontents['f4'];
				//Landlord address
				$data['fieldcontents']['f6'] = $fieldcontents['f5'];
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		if (!isset($data['fieldcontents']['f22'])){
			$data['fieldcontents']['f22'] = 'Both';
		}
		
		$replaceFields = DocumentFields::getTenancyAgreementReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}

		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getTenancyAgreementReplaceAutoText($docversion);

		//This will take the other conditions content set in the offer letter
		//and put it in the tenancy agreement for the user to edit or replace
		$rowOfferLetter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();

		if (!empty($rowOfferLetter)) {
			//Landlords Other conditions
			$offerLetter = json_decode($rowOfferLetter->fieldcontents, true);
			if (!empty( trim($offerLetter['f40']) )) {
				$replaceFieldsDesc['landLordOtherConditions'] = $offerLetter['f40'];
			}
			//Tenants Other Conditions
			if (!empty( trim($offerLetter['f34']) )) {
				$replaceFieldsDesc['tenantOtherConditions'] = $offerLetter['f34'];
			}

			if ( empty($data['fieldcontents']['f3']) && !empty($offerLetter['f26']) ) {
				$data['fieldcontents']['f3'] = $offerLetter['f26'];
			}

			if ( empty( $data['fieldcontents']['f24'] ) && !empty( $offerLetter['f58'] )  ) {
				$data['fieldcontents']['f24'] = $offerLetter['f58'];
			}

			if ( empty( $data['fieldcontents']['f4'] ) && !empty( $offerLetter['f55'] )  ) {
				//default value from offer letter is adjusted to match the sentence in the early termination (a) section
				$termination = explode(' ', $offerLetter['f55']);
				if ( count($termination) >= 2 ) {
					$data['fieldcontents']['f4'] = $termination[0].' '.$termination[1];
				} else {
					$data['fieldcontents']['f4'] = $termination[0];
				}
			}
		}

		$helppics = DocumentFields::getTenancyAgreementHelpPics($docversion);
		
		return view('documents.tenancy-agreement', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc,
			'helppics' => $helppics
		]);
    }
	
	public function tenancyagreementupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getTenancyAgreementFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'tenancyagreement',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/tenancyagreement/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function tenancyagreementpdf($shortlistid, $propertyid){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenancyagreement')->orderBy('id', 'desc')->first();

		$rowOfferLetter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();

		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'tenancyagreement',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getTenancyAgreementFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getTenancyAgreementShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
	
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
		
		if (!isset($data['fieldcontents']['f23'])){
			$data['fieldcontents']['f23'] = '';
		}
		if (!isset($data['fieldcontents']['f24'])){
			$data['fieldcontents']['f24'] = '';
		}
		if (!isset($data['fieldcontents']['f2'])){
			$data['fieldcontents']['f2'] = '15';
		}
				
		$replaceFields = DocumentFields::getTenancyAgreementReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}

		if ( !empty($rowOfferLetter->fieldcontents) ) {
			$offerLetter = json_decode($rowOfferLetter->fieldcontents, true);
			if (!empty( trim($offerLetter['f40']) )) {
				$data['fieldcontents']['landLordOtherConditions'] = $offerLetter['f40'];
			}

			if (!empty( trim($offerLetter['f34']) )) {
				$data['fieldcontents']['tenantOtherConditions'] = $offerLetter['f34'];
			}
		}

		$html = view('documents.tenancy-agreement-pdf-v1', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Tenancy Agreement - '.$n.''.date('d-m-Y').'.pdf';
		

		$domPdfOptions = new Options();
		$domPdfOptions->set("isPhpEnabled", true);		
		$dompdf = new Dompdf($domPdfOptions);
		$dompdf->loadHtml($html);
		$dompdf->setPaper('a4');
		$dompdf->render();
		return $dompdf->stream($name,array("Attachment" => false));
		
        // return $html;
        //return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->setOptions(['isPhpEnabled'=>true])->stream($name);
    }
	
	
	
    public function landlordfeeletteredit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('lfl');
		
		$data = array(
			'doctype' => 'landlordfeeletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'landlordfeeletter')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getLandlordFeeLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getLandlordFeeLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
				
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'Landlord';
			$data['fieldcontents']['f7'] = $property->name;
			if ($consultant){
				$data['fieldcontents']['f11'] = $consultant->name;
				$data['fieldcontents']['f12'] = $consultant->offtitle;
				$data['fieldcontents']['f13'] = $consultant->agentid;
			}
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Landlord
				$data['fieldcontents']['f4'] = $offerletterfieldcontents['f36'];
				//Premises
				$data['fieldcontents']['f7'] = $offerletterfieldcontents['f6'];
				//Consultant name
				$data['fieldcontents']['f11'] = $offerletterfieldcontents['f30'];
				//Consultant role/title
				$data['fieldcontents']['f12'] = $offerletterfieldcontents['f31'];
				//Consultant license number
				$data['fieldcontents']['f13'] = $offerletterfieldcontents['f32'];
			}
			
			$holdingdepositletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'holdingdepositletter')->orderBy('id', 'desc')->first();
			if ($holdingdepositletter){
				$fieldcontents = json_decode($holdingdepositletter->fieldcontents, true);
				//Landlord name
				$data['fieldcontents']['f4'] = $fieldcontents['f2'];
				//Landlord address
				$data['fieldcontents']['f5'] = $fieldcontents['f3'];
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getLandlordFeeLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getLandlordFeeReplaceAutoText($docversion);
		
		return view('documents.landlord-fee-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc
		]);
    }
	
	public function landlordfeeletterupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getLandlordFeeLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'landlordfeeletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/landlordfeeletter/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function landlordfeeletterpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'landlordfeeletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'landlordfeeletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getLandlordFeeLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getLandlordFeeLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getLandlordFeeLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$file = 'documents.landlord-fee-letter-pdf-v2';

		if($logo == 'no'){
			$file = 'documents.landlord-fee-letter-pdf-v1';
		}

		$html = view($file, [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Landlord Fee Letter - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
    public function tenantfeeletteredit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('tfl');
		
		$data = array(
			'doctype' => 'tenantfeeletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenantfeeletter')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getTenantFeeLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getTenantFeeLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
				
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'Tenant';
			$data['fieldcontents']['f7'] = $property->name;
			if ($consultant){
				$data['fieldcontents']['f11'] = $consultant->name;
				$data['fieldcontents']['f12'] = $consultant->offtitle;
				$data['fieldcontents']['f13'] = $consultant->agentid;
			}
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Tenant
				if (isset($offerletterfieldcontents['f42']) && $offerletterfieldcontents['f42'] == 'show'){
					$data['fieldcontents']['f4'] = $offerletterfieldcontents['f7'].' & '.$offerletterfieldcontents['f43'];
				}else{
					$data['fieldcontents']['f4'] = $offerletterfieldcontents['f7'];
				}
				//Premises
				$data['fieldcontents']['f7'] = $offerletterfieldcontents['f6'];
				//Consultant name
				$data['fieldcontents']['f11'] = $offerletterfieldcontents['f30'];
				//Consultant role/title
				$data['fieldcontents']['f12'] = $offerletterfieldcontents['f31'];
				//Consultant license number
				$data['fieldcontents']['f13'] = $offerletterfieldcontents['f32'];

				//Client address
				$data['fieldcontents']['f5'] = $shortlist['client']->workplace;
				if($shortlist['client']->workplace){
					$data['fieldcontents']['f5'] .= "\n";
				}
				$data['fieldcontents']['f5'] .= $offerletterfieldcontents['f9'];
				if($offerletterfieldcontents['f9']){
					$data['fieldcontents']['f5'] .= "\n";
				}
				$data['fieldcontents']['f5'] .= $offerletterfieldcontents['f10'];
				if($offerletterfieldcontents['f10']){
					$data['fieldcontents']['f5'] .= "\n";
				}
				$data['fieldcontents']['f5'] .= $offerletterfieldcontents['f11'];

			}else{
				//Client name
				$data['fieldcontents']['f4'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
				
				//Client address
				$data['fieldcontents']['f5'] = $shortlist['client']->workplace;
				if($shortlist['client']->workplace){
					$data['fieldcontents']['f5'] .= "\n";
				}
				
				$data['fieldcontents']['f5'] .= $shortlist['client']->address1;
				if($shortlist['client']->address1){
					$data['fieldcontents']['f5'] .= "\n";
				}
				$data['fieldcontents']['f5'] .= $shortlist['client']->address2;
				if($shortlist['client']->address2){
					$data['fieldcontents']['f5'] .= "\n";
				}
				$data['fieldcontents']['f5'] .= $shortlist['client']->address3;

			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getTenantFeeLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getTenantFeeReplaceAutoText($docversion);
		
		return view('documents.tenant-fee-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc
		]);
    }
	
	public function tenantfeeletterupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getTenantFeeLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'tenantfeeletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/tenantfeeletter/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function tenantfeeletterpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenantfeeletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'tenantfeeletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getTenantFeeLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getTenantFeeLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getTenantFeeLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$file = 'documents.tenant-fee-letter-pdf-v2';

		if($logo == 'no'){
			$file = 'documents.tenant-fee-letter-pdf-v1';
		}

		$html = view($file, [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Tenant Fee Letter - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
    public function letterofundertakingedit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('lou');
		
		$data = array(
			'doctype' => 'letterofundertaking',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'letterofundertaking')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getLetterOfUndertakingFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getLetterOfUndertakingShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
				
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = $property->name;
			$data['fieldcontents']['f3'] = '';
			$data['fieldcontents']['f4'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				$data['fieldcontents']['f2'] = $offerletterfieldcontents['f6'];
				$data['fieldcontents']['f3'] = $offerletterfieldcontents['f36'];
				if (isset($offerletterfieldcontents['f42']) && $offerletterfieldcontents['f42'] == 'show'){
					$data['fieldcontents']['f4'] = $offerletterfieldcontents['f7'].' & '.$offerletterfieldcontents['f43'];
				}else{
					$data['fieldcontents']['f4'] = $offerletterfieldcontents['f7'];
				}
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getLetterOfUndertakingReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getLetterOfUndertakingReplaceAutoText($docversion);
		
		return view('documents.letter-of-undertaking', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc
		]);
    }
	
	public function letterofundertakingupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getLetterOfUndertakingFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'letterofundertaking',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/letterofundertaking/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function letterofundertakingpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'letterofundertaking')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'letterofundertaking',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getLetterOfUndertakingFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getLetterOfUndertakingShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getLetterOfUndertakingReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		if ($logo == 'no') {
			$template = 'letter-of-undertaking-pdf-v1';
		} else {
			$template = 'letter-of-undertaking-pdf-v2';
		}
		
		$html = view('documents.'.$template, [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Letter of Undertaking - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
    public function holdingdepositletteredit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('hdl');
		
		$data = array(
			'doctype' => 'holdingdepositletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'holdingdepositletter')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getHoldingDepositLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHoldingDepositLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f5'] = $property->name;
			if ($consultant){
				$data['fieldcontents']['f7'] = $consultant->name;
				$data['fieldcontents']['f8'] = $consultant->offtitle;
				$data['fieldcontents']['f9'] = $consultant->agentid;
			}
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Landlord
				$data['fieldcontents']['f2'] = $offerletterfieldcontents['f36'];
				//Premises
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f6'];
				//Consultant name
				$data['fieldcontents']['f7'] = $offerletterfieldcontents['f30'];
				//Consultant role/title
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f31'];
				//Consultant license number
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f32'];
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getHoldingDepositLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getHoldingDepositLetterReplaceAutoText($docversion);
		
		return view('documents.holding-deposit-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc
		]);
    }
	
	public function holdingdepositletterupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getHoldingDepositLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'holdingdepositletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/holdingdepositletter/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function holdingdepositletterpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'holdingdepositletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'holdingdepositletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getHoldingDepositLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHoldingDepositLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getHoldingDepositLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('documents.holding-deposit-letter-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Holding Deposit Letter - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
    public function utilityaccountsedit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('ua');
		
		$data = array(
			'doctype' => 'utilityaccounts',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'utilityaccounts')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getUtilityAccountsFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getUtilityAccountsShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Tenant
				$data['fieldcontents']['f1'] = $offerletterfieldcontents['f7'];
				//HKID
				$data['fieldcontents']['f2'] = $offerletterfieldcontents['f12'];
				//Address
				$data['fieldcontents']['f3'] = $offerletterfieldcontents['f6'];
				//Lease Term
				$data['fieldcontents']['f4'] = $offerletterfieldcontents['f53'];
			}else{
				$data['fieldcontents']['f1'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
				$data['fieldcontents']['f2'] = $shortlist['client']->hkidpassport;
			}
			$tenancyagreement = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenancyagreement')->orderBy('id', 'desc')->first();
			if ($tenancyagreement){
				$tenancyagreementfieldcontents = json_decode($tenancyagreement->fieldcontents, true);
				$data['fieldcontents']['f1'] = $tenancyagreementfieldcontents['f7'];
				$data['fieldcontents']['f3'] = $tenancyagreementfieldcontents['f9'];
				$data['fieldcontents']['f2'] = $tenancyagreementfieldcontents['f18'];
				$data['fieldcontents']['f4'] = $tenancyagreementfieldcontents['f10'];
			}
			$data['fieldcontents']['f7'] = 'As Above';
			$data['fieldcontents']['f12'] = 'As Above';
			$data['fieldcontents']['f17'] = 'As Above';
			
			$data['fieldcontents']['f8'] = 'TBC [Reference No. xxx]';
			$data['fieldcontents']['f13'] = 'TBC [Reference No. xxx]';
			$data['fieldcontents']['f18'] = 'TBC [Reference No. xxx]';
			
			$data['fieldcontents']['f10'] = 'HK$400';
			$data['fieldcontents']['f15'] = 'TBC';
			$data['fieldcontents']['f20'] = 'HK$600';
			
			$data['fieldcontents']['f21'] = 'Tenant';
			$data['fieldcontents']['f22'] = 'HKID No.';
			
			if (isset($shortlist['client']) && trim($shortlist['client']->tel) != ''){
				$data['fieldcontents']['f5'] = trim($shortlist['client']->tel);
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		if (!isset($data['fieldcontents']['f21'])){
			$data['fieldcontents']['f21'] = 'Tenant';
		}
		if (!isset($data['fieldcontents']['f22'])){
			$data['fieldcontents']['f22'] = 'HKID No.';
		}
		
		$replaceFields = DocumentFields::getUtilityAccountsReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('documents.utility-accounts', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist
		]);
    }
	
	public function utilityaccountsupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getUtilityAccountsFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'utilityaccounts',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/utilityaccounts/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function utilityaccountspdf($shortlistid, $propertyid){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'utilityaccounts')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'utilityaccounts',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getUtilityAccountsFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getUtilityAccountsShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
		
		if (!isset($data['fieldcontents']['f21'])){
			$data['fieldcontents']['f21'] = 'Tenant';
		}
		if (!isset($data['fieldcontents']['f22'])){
			$data['fieldcontents']['f22'] = 'HKID No.';
		}		
		
		$replaceFields = DocumentFields::getUtilityAccountsReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('documents.utility-accounts-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Utility Accounts - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a5', 'portrait')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
    public function handoverreportedit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('hr');
		
		$data = array(
			'doctype' => 'handoverreport',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'handoverreport')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getHandoverReportFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHandoverReportShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				$data['fieldcontents']['f1'] = $offerletterfieldcontents['f6'];
				$data['fieldcontents']['f4'] = $offerletterfieldcontents['f30'];
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f25'];
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f13'];
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f18'];
				$data['fieldcontents']['f10'] = $offerletterfieldcontents['f20'];
				$data['fieldcontents']['f11'] = $offerletterfieldcontents['f22'];
				if (isset($offerletterfieldcontents['f42']) && $offerletterfieldcontents['f42'] == 'show'){
					$data['fieldcontents']['f16'] = $offerletterfieldcontents['f7'].' & '.$offerletterfieldcontents['f43'];
				}else{
					$data['fieldcontents']['f16'] = $offerletterfieldcontents['f7'];
				}
				$data['fieldcontents']['f17'] = $shortlist['client']->workplace.' '.$offerletterfieldcontents['f9'].' '.$offerletterfieldcontents['f10'].' '.$offerletterfieldcontents['f11'];
				
			}else{
				$data['fieldcontents']['f16'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
				$data['fieldcontents']['f17'] = $shortlist['client']->workplace.' '.$shortlist['client']->address1.' '.$shortlist['client']->address2 . ' ' . $shortlist['client']->address3;

			}

			$tenancyagreement = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenancyagreement')->orderBy('id', 'desc')->first();
			if ($tenancyagreement){
				$tenancyagreementfieldcontents = json_decode($tenancyagreement->fieldcontents, true);
				$data['fieldcontents']['f1'] = $tenancyagreementfieldcontents['f9'];
				$data['fieldcontents']['f6'] = $tenancyagreementfieldcontents['f19'];
				$data['fieldcontents']['f7'] = $tenancyagreementfieldcontents['f20'];
				$data['fieldcontents']['f13'] = $tenancyagreementfieldcontents['f14'];
				$data['fieldcontents']['f14'] = $tenancyagreementfieldcontents['f16'];
				$data['fieldcontents']['f15'] = $tenancyagreementfieldcontents['f15'];
				$data['fieldcontents']['f23'] = $tenancyagreementfieldcontents['f5'];
				$data['fieldcontents']['f24'] = $tenancyagreementfieldcontents['f6'];
				$data['fieldcontents']['f17'] = $tenancyagreementfieldcontents['f8'];
			}
			$data['fieldcontents']['f29'] = 'hide';
			$data['fieldcontents']['f30'] = 'person';
			$data['fieldcontents']['f31'] = 'person';
			$data['fieldcontents']['f32'] = 'big';
			
			if (isset($shortlist['client']) && trim($shortlist['client']->tel) != ''){
				$data['fieldcontents']['f19'] = trim($shortlist['client']->tel);
			}
			if (isset($shortlist['client']) && trim($shortlist['client']->email) != ''){
				$data['fieldcontents']['f20'] = trim($shortlist['client']->email);
			}
			
			$tenantfeeletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenantfeeletter')->orderBy('id', 'desc')->first();
			if ($tenantfeeletter){
				$fieldcontents = json_decode($tenantfeeletter->fieldcontents, true);
				if (trim($fieldcontents['f4']) != ''){
					$data['fieldcontents']['f16'] = $fieldcontents['f4'];
				}
				if (trim($fieldcontents['f5']) != ''){
					$data['fieldcontents']['f17'] = $fieldcontents['f5'];
				}
				if (trim($fieldcontents['f7']) != ''){
					$data['fieldcontents']['f1'] = $fieldcontents['f7'];
				}
			}
			
			$landlordfeeletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'landlordfeeletter')->orderBy('id', 'desc')->first();
			if ($landlordfeeletter){
				$fieldcontents = json_decode($landlordfeeletter->fieldcontents, true);
				if (trim($fieldcontents['f4']) != ''){
					$data['fieldcontents']['f23'] = $fieldcontents['f4'];
				}
				if (trim($fieldcontents['f5']) != ''){
					$data['fieldcontents']['f24'] = $fieldcontents['f5'];
				}
				if (trim($fieldcontents['f7']) != ''){
					$data['fieldcontents']['f1'] = $fieldcontents['f7'];
				}
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			if (!isset($data['fieldcontents']['f29']) || $data['fieldcontents']['f29'] == ''){
				$data['fieldcontents']['f29'] = 'person';
			}
			if (!isset($data['fieldcontents']['f30']) || $data['fieldcontents']['f30'] == ''){
				$data['fieldcontents']['f30'] = 'person';
			}
			if (!isset($data['fieldcontents']['f31']) || $data['fieldcontents']['f31'] == ''){
				$data['fieldcontents']['f31'] = 'person';
			}
			if (!isset($data['fieldcontents']['f32']) || $data['fieldcontents']['f31'] == ''){
				$data['fieldcontents']['f32'] = 'big';
			}
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getHandoverReportReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('documents.handover-report', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist
		]);
    }
	
	public function handoverreportupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getHandoverReportFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'handoverreport',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/handoverreport/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function handoverreportpdf($shortlistid, $propertyid){
		set_time_limit(100);
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'handoverreport')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'handoverreport',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getHandoverReportFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHandoverReportShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getHandoverReportReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('documents.handover-report-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Handover Report - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
    public function handoverreportleasepdf($shortlistid, $propertyid){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'handoverreport')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'handoverreport',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getHandoverReportFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHandoverReportShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getHandoverReportReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('documents.handover-report-lease-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Lease Details - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	//------------------------
	//Extended Handover Report
	//------------------------
	
    public function handoverreportextedit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('hre');
		
		$data = array(
			'doctype' => 'handoverreportext',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'handoverreportext')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getHandoverReportExtFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHandoverReportExtShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				$data['fieldcontents']['f1'] = $offerletterfieldcontents['f6'];
				$data['fieldcontents']['f4'] = $offerletterfieldcontents['f30'];
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f25'];
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f13'];
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f18'];
				$data['fieldcontents']['f10'] = $offerletterfieldcontents['f20'];
				$data['fieldcontents']['f11'] = $offerletterfieldcontents['f22'];
				if (isset($offerletterfieldcontents['f42']) && $offerletterfieldcontents['f42'] == 'show'){
					$data['fieldcontents']['f16'] = $offerletterfieldcontents['f7'].' & '.$offerletterfieldcontents['f43'];
				}else{
					$data['fieldcontents']['f16'] = $offerletterfieldcontents['f7'];
				}
				$data['fieldcontents']['f17'] = $shortlist['client']->workplace.' '.$offerletterfieldcontents['f9'].' '.$offerletterfieldcontents['f10'].' '.$offerletterfieldcontents['f11'];
				
			}else{
				$data['fieldcontents']['f16'] = $shortlist['client']->firstname.' '.$shortlist['client']->lastname;
				$data['fieldcontents']['f17'] = $shortlist['client']->workplace.' '.$shortlist['client']->address1.' '.$shortlist['client']->address2 . ' ' . $shortlist['client']->address3;
			}
			$tenancyagreement = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenancyagreement')->orderBy('id', 'desc')->first();
			if ($tenancyagreement){
				$tenancyagreementfieldcontents = json_decode($tenancyagreement->fieldcontents, true);
				$data['fieldcontents']['f1'] = $tenancyagreementfieldcontents['f9'];
				$data['fieldcontents']['f6'] = $tenancyagreementfieldcontents['f19'];
				$data['fieldcontents']['f7'] = $tenancyagreementfieldcontents['f20'];
				$data['fieldcontents']['f13'] = $tenancyagreementfieldcontents['f14'];
				$data['fieldcontents']['f14'] = $tenancyagreementfieldcontents['f16'];
				$data['fieldcontents']['f15'] = $tenancyagreementfieldcontents['f15'];
				$data['fieldcontents']['f23'] = $tenancyagreementfieldcontents['f5'];
				$data['fieldcontents']['f24'] = $tenancyagreementfieldcontents['f6'];
				$data['fieldcontents']['f17'] = $tenancyagreementfieldcontents['f8'];
			}
			$data['fieldcontents']['f29'] = 'hide';
			$data['fieldcontents']['f30'] = 'person';
			$data['fieldcontents']['f31'] = 'person';
			$data['fieldcontents']['f32'] = 'big';
			
			if (isset($shortlist['client']) && trim($shortlist['client']->tel) != ''){
				$data['fieldcontents']['f19'] = trim($shortlist['client']->tel);
			}
			if (isset($shortlist['client']) && trim($shortlist['client']->email) != ''){
				$data['fieldcontents']['f20'] = trim($shortlist['client']->email);
			}
			
			$tenantfeeletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenantfeeletter')->orderBy('id', 'desc')->first();
			if ($tenantfeeletter){
				$fieldcontents = json_decode($tenantfeeletter->fieldcontents, true);
				if (trim($fieldcontents['f4']) != ''){
					$data['fieldcontents']['f16'] = $fieldcontents['f4'];
				}
				if (trim($fieldcontents['f5']) != ''){
					$data['fieldcontents']['f17'] = $fieldcontents['f5'];
				}
				if (trim($fieldcontents['f7']) != ''){
					$data['fieldcontents']['f1'] = $fieldcontents['f7'];
				}
			}
			
			$landlordfeeletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'landlordfeeletter')->orderBy('id', 'desc')->first();
			if ($landlordfeeletter){
				$fieldcontents = json_decode($landlordfeeletter->fieldcontents, true);
				if (trim($fieldcontents['f4']) != ''){
					$data['fieldcontents']['f23'] = $fieldcontents['f4'];
				}
				if (trim($fieldcontents['f5']) != ''){
					$data['fieldcontents']['f24'] = $fieldcontents['f5'];
				}
				if (trim($fieldcontents['f7']) != ''){
					$data['fieldcontents']['f1'] = $fieldcontents['f7'];
				}
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
			if (!isset($data['fieldcontents']['f29']) || $data['fieldcontents']['f29'] == ''){
				$data['fieldcontents']['f29'] = 'person';
			}
			if (!isset($data['fieldcontents']['f30']) || $data['fieldcontents']['f30'] == ''){
				$data['fieldcontents']['f30'] = 'person';
			}
			if (!isset($data['fieldcontents']['f31']) || $data['fieldcontents']['f31'] == ''){
				$data['fieldcontents']['f31'] = 'person';
			}
			if (!isset($data['fieldcontents']['f32']) || $data['fieldcontents']['f31'] == ''){
				$data['fieldcontents']['f32'] = 'big';
			}
		}
		$data['fieldcontents']['f'] = '';
		
		$rooms = array();
		if (isset($data['fieldcontents']['f33']) && count($data['fieldcontents']['f33']) > 0){
			$rooms = $data['fieldcontents']['f33'];
		}
		
		$replaceFields = DocumentFields::getHandoverReportExtReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('documents.handover-report-extended', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'rooms' => $rooms
		]);
    }
	
	public function handoverreportextupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getHandoverReportExtFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$rooms = array();
			if (isset($input['roomscounter'])){
				for ($i = 0; $i <= intval($input['roomscounter']); $i++){
					if (isset($input['roomnewposition_'.$i]) && intval($input['roomnewposition_'.$i]) >= 0){
						if (isset($input['roomtype_'.$i]) && isset($input['roomname_'.$i])){
							$rooms[] = array(
								'roomtype' => $input['roomtype_'.$i],
								'roomname' => $input['roomname_'.$i],
								'position' => $input['roomnewposition_'.$i]
							);
						}
					}
				}
			}
			
			for ($i = 0; $i < count($rooms)-1; $i++){
				for ($a = $i+1; $a < count($rooms); $a++){
					if ($rooms[$i]['position'] > $rooms[$a]['position']){
						$help = $rooms[$i];
						$rooms[$i] = $rooms[$a];
						$rooms[$a] = $help;
					}
				}
			}
			
			$fields['f33'] = $rooms;
			
			$data = array(
				'doctype' => 'handoverreportext',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/handoverreportext/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function handoverreportextpdf($shortlistid, $propertyid){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'handoverreportext')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'handoverreportext',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getHandoverReportExtFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHandoverReportExtShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getHandoverReportExtReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$rooms = array();
		if (isset($data['fieldcontents']['f33']) && count($data['fieldcontents']['f33']) > 0){
			$rooms = $data['fieldcontents']['f33'];
		}
		
		$html = view('documents.handover-report-extended-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'rooms' => $rooms
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Handover Report - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
    public function handoverreportextleasepdf($shortlistid, $propertyid){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'handoverreportext')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'handoverreportext',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getHandoverReportExtFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getHandoverReportExtShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getHandoverReportExtReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$rooms = array();
		if (isset($data['fieldcontents']['f33']) && count($data['fieldcontents']['f33']) > 0){
			$rooms = $data['fieldcontents']['f33'];
		}
		
		$html = view('documents.handover-report-extended-lease-pdf-v2', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'rooms' => $rooms
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Lease Details - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
    public function docaddproperty($shortlistid, $propertyid){
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}
		
		$doc = Documentinfo::where('shortlist_id', $shortlistid)->where('type_id', $typeid)->first();
		if ($doc){
			$props = $doc->properties;
			if (trim($props) == ''){
				$doc->properties = $propertyid;
			}else{
				$ps = explode('|', $props);
				if (!in_array($propertyid, $ps)){
					$ps[] = $propertyid;
				}
				$doc->properties = implode('|', $ps);
			}
			$doc->save();
		}else{
			$doc = new Documentinfo();
			$doc->fill(array(
				'client_id' => $shortlist['client']->id,
				'shortlist_id' => $shortlistid,
				'type_id' => $typeid,
				'properties' => $propertyid
			));
			$doc->save();
		}
		
		if ($typeid == 1){
			return redirect('/documents/index-lease/'.$shortlistid);
		}else{
			return redirect('/documents/index-sale/'.$shortlistid);
		}
	}
	
    public function docdelproperty($shortlistid, $propertyid){
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}
		
		$doc = Documentinfo::where('shortlist_id', $shortlistid)->where('type_id', $typeid)->first();
		if ($doc){
			$props = $doc->properties;
			if (trim($props) == $propertyid){
				$doc->properties = '';
			}else{
				$ps = explode('|', $props);
				for ($i = 0; $i < count($ps); $i++){
					if ($ps[$i] == $propertyid){
						unset($ps[$i]);
					}
				}
				$doc->properties = implode('|', $ps);
			}
			$doc->save();
			
			if ($doc->type_id == 1){
				return redirect('/documents/index-lease/'.$shortlistid);
			}else{
				return redirect('/documents/index-sale/'.$shortlistid);
			}
		}
		
		return redirect('/shortlists');
	}
	
    public function docsavecomments($shortlistid, Request $request){
		$input = $request->all();
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		
		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}
		
		$doc = Documentinfo::where('shortlist_id', $shortlistid)->where('type_id', $typeid)->first();
		if ($doc){
			if (isset($input['remarks'])){
				$doc->remarks = $input['remarks'];
				$doc->save();
			}
			
			if ($doc->type_id == 1){
				return redirect('/documents/index-lease/'.$shortlistid);
			}else{
				return redirect('/documents/index-sale/'.$shortlistid);
			}
		}
		
		return redirect('/shortlists');
		
	}
	
    public function docstoreupload($shortlistid, Request $request){
		$input = $request->all();
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		
		$typeid = 1;
		if ($shortlist->typeid == 2){
			$typeid = 2;
		}
		
		$rules = array(
			'upfile' =>  'mimes:pdf'
		);
		$validator = Validator::make($request->all(), $rules);
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			if (Input::hasFile('upfile') && Input::file('upfile')->isValid() && $validator->errors()->count() == 0){
				$path = '../storage/documentuploads/';
				$name = $shortlistid.'_'.time().'_'.$input['doctype'].'.pdf';
				Input::file('upfile')->move($path, $name);
				
				$doc = new Documentuploads();
				$doc->doctype = $input['doctype'];
				$doc->shortlistid = $shortlistid;
				$doc->consultantid = $request->user()->id;
				$doc->docpath = $path.$name;
				$doc->save();
			}
		}
		
		if ($typeid == 1){
			return redirect('/documents/index-lease/'.$shortlistid);
		}else{
			return redirect('/documents/index-sale/'.$shortlistid);
		}
	}
	
	public function showdoc($docid){
		$docupload = Documentuploads::where('id', '=', $docid)->first();
		
		if ($docupload){
			if (file_exists($docupload->docpath)){
				return response()->download($docupload->docpath);
			}
		}else{
			return redirect('/documents');
		}
	}
	
	public function removedoclease(Request $request, $docid, $documentid){	
		$user = $request->user();	
		
		$doc = Documentuploads::where('id', '=', $docid)->first();
		
		if ($doc && ($doc->consultantid == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			$doc->delete();
			
			return redirect('/documents/index-lease/'.$documentid);
		}else{
			return redirect('/shortlists');
		}
	}
	
	public function removedocsale(Request $request, $docid, $documentid){	
		$user = $request->user();	
		
		$doc = Documentuploads::where('id', '=', $docid)->first();
		
		if ($doc && ($doc->consultantid == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			$doc->delete();
			
			return redirect('/documents/index-sale/'.$documentid);
		}else{
			return redirect('/shortlists');
		}
	}
	
	
	
    public function coverletteredit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('cl');
		
		$data = array(
			'doctype' => 'coverletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'coverletter')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getCoverLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getCoverLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'Landlord';
			$data['fieldcontents']['f5'] = $property->name;
			if ($consultant){
				$data['fieldcontents']['f11'] = $consultant->name;
				$data['fieldcontents']['f12'] = $consultant->offtitle;
				$data['fieldcontents']['f13'] = $consultant->agentid;
			}
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Landlord
				$data['fieldcontents']['f3'] = $offerletterfieldcontents['f36'];
				$data['fieldcontents']['f4'] = $offerletterfieldcontents['f36'];
				//Premises
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f6'];
				//Consultant name
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f30'];
				//Consultant role/title
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f31'];
				//Consultant license number
				$data['fieldcontents']['f10'] = $offerletterfieldcontents['f32'];
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getCoverLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getCoverLetterReplaceAutoText($docversion);
		
		return view('documents.cover-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc
		]);
    }
	
	public function coverletterupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getCoverLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'coverletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/coverletter/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function coverletterpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'coverletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'coverletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getCoverLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getCoverLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getCoverLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('documents.cover-letter-pdf-v1', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);

		if(isset($_GET['test_view']) AND $_GET['test_view']=='datetop'){
			$html = view('documents.cover-letter-pdf-v3', [
				'fields' => $fields,
				'hides' => $hides,
				'data' => $data,
				'abc' => $this->getabc(),
				'consultant' => $consultant,
				'logo' => $logo
			]);
		} else {
			$html = view('documents.cover-letter-pdf-v2', [
				'fields' => $fields,
				'hides' => $hides,
				'data' => $data,
				'abc' => $this->getabc(),
				'consultant' => $consultant,
				'logo' => $logo
			]);
		}
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Cover Letter - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	//Side Letter
    public function sideletteredit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('sl');
		
		$data = array(
			'doctype' => 'sideletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'sideletter')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getSideLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getSideLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f2'] = 'Tenant';
			$data['fieldcontents']['f5'] = $property->name;
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Premises
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f6'];
				//Landlord Name
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f30'];
				//Tenant Name
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f31'];
			}
			
			$tenancyagreement = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'tenancyagreement')->orderBy('id', 'desc')->first();
			if ($tenancyagreement){
				$tenancyagreementfieldcontents = json_decode($tenancyagreement->fieldcontents, true);
				//Date of Agreement
				$data['fieldcontents']['f6'] = $tenancyagreementfieldcontents['f1'];
				//Premises
				$data['fieldcontents']['f5'] = $tenancyagreementfieldcontents['f9'];
				//Landlord Name
				$data['fieldcontents']['f8'] = $tenancyagreementfieldcontents['f5'];
				//Tenant Name
				$data['fieldcontents']['f9'] = $tenancyagreementfieldcontents['f7'];
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getSideLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		$replaceFieldsDesc = DocumentFields::getSideLetterReplaceAutoText($docversion);
		
		return view('documents.side-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist,
			'replaceFieldsDesc' => $replaceFieldsDesc
		]);
    }
	
	public function sideletterupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getSideLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'sideletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/sideletter/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function sideletterpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'sideletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'sideletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getSideLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getSideLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getSideLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}

		$template = 'documents.side-letter-pdf-v2';
		if ($logo == 'no') {
			$template = 'documents.side-letter-pdf-v1';
		}
		
		$html = view($template, [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Side Letter - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	//Custom Letter
    public function customletteredit($shortlistid, $propertyid){
		$docversion = DocumentFields::getCurDocVersions('cul');
		
		$data = array(
			'doctype' => 'customletter',
			'docversion' => $docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		$new = true;
		
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'customletter')->orderBy('id', 'desc')->first();
		if ($row){
			$new = false;
		}
		
		$fieldslist = DocumentFields::getCustomLetterFields($docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getCustomLetterShowHide($docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		if ($new){
			$data['fieldcontents']['f1'] = date('Y-m-d');
			$data['fieldcontents']['f5'] = $property->name;
			$data['fieldcontents']['f7'] = 'No Signature';
			
			$offerletter = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'offerletter')->orderBy('id', 'desc')->first();
			if ($offerletter){
				$offerletterfieldcontents = json_decode($offerletter->fieldcontents, true);
				//Premises
				$data['fieldcontents']['f5'] = $offerletterfieldcontents['f6'];
				//Consultant name
				$data['fieldcontents']['f8'] = $offerletterfieldcontents['f30'];
				//Consultant role/title
				$data['fieldcontents']['f9'] = $offerletterfieldcontents['f31'];
				//Consultant license number
				$data['fieldcontents']['f10'] = $offerletterfieldcontents['f32'];
			}
		}else{
			$data['sectionshidden'] = json_decode($row->sectionshidden, true);
			$data['fieldcontents'] = json_decode($row->fieldcontents, true);
			$data['replace'] = json_decode($row->replace, true);
			$data['id'] = $row->id;
		}
		$data['fieldcontents']['f'] = '';
		
		$replaceFields = DocumentFields::getCustomLetterReplace($docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		//find sections hidden that used to be there but aren't anymore
		$hiddenhides = array();
		foreach ($data['sectionshidden'] as $sh){
			$found = false;
			foreach ($hides as $hide){
				if (count($hide) >= 4){
					if ($hide[0].'_'.$hide[1] == $sh){
						$found = true;
					}
				}
			}
			if (!$found){
				$hiddenhides[] = $sh;
			}
		}
		
		return view('documents.custom-letter', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'hiddenhides' => $hiddenhides,
			'new' => $new,
			'replaceFields' => $replaceFields,
			'shortlist' => $shortlist
		]);
    }
	
	public function customletterupdate(Request $request){
		$input = $request->all();
		
		if (isset($input['id'])){
			$docversion = intval($input['docversion']);
			
			//hidden sections
			if (isset($input['hidevals'])){
				$sectionshidden = $input['hidevals'];
			}else{
				$sectionshidden = array();
			}
			//replace
			if (isset($input['replace'])){
				$replace = $input['replace'];
			}else{
				$replace = array();
			}
			
			//fields
			$fieldslist = DocumentFields::getCustomLetterFields($docversion);
			$fieldlines = explode(PHP_EOL, $fieldslist);
			$fields = array();
			foreach ($fieldlines as $line){
				$e = explode('|', trim($line));
				if (count($e) >= 3){
					$x = $e[0];
					if (isset($input['f'.intval($x)])){
						$fields['f'.intval($x)] = $input['f'.intval($x)];
					}
				}
			}
			
			$data = array(
				'doctype' => 'customletter',
				'docversion' => $docversion,
				'shortlistid' => intval($input['shortlistid']),
				'consultantid' => intval($input['consultantid']),
				'clientid' => intval($input['clientid']),
				'propertyid' => intval($input['propertyid']),
				'fieldcontents' => json_encode($fields),
				'sectionshidden' => json_encode($sectionshidden),
				'replace' => json_encode($replace)
			);
			
			if (intval($input['id']) == 0){
				//New
				$doc = Document::create($data);
				$doc->save();
			}else{
				//Existing
				$doc = Document::findOrFail(intval($input['id']));
				$doc->fill($data);
				$doc->save();
			}
		}
		
		if (isset($input['shortlistid']) && isset($input['propertyid'])){
			return redirect('/documents/'.intval($input['shortlistid']).'/customletter/'.intval($input['propertyid']));
		}else{
			return redirect('/documents');
		}
	}
	
	
    public function customletterpdf($shortlistid, $propertyid, $logo = 'yes'){
		$row = Document::where('shortlistid', '=', $shortlistid)->where('propertyid', '=', $propertyid)->where('doctype', '=', 'customletter')->orderBy('id', 'desc')->first();
		if (!$row){
			return redirect('/documents');
		}
		
		$data = array(
			'doctype' => 'customletter',
			'docversion' => $row->docversion,
			'shortlistid' => $shortlistid,
			'propertyid' => $propertyid,
			'sectionshidden' => array(),
			'fieldcontents' => array(),
			'replace' => array(),
			'id' => 0
		);
		
		$fieldslist = DocumentFields::getCustomLetterFields($row->docversion);
		$fieldlines = explode(PHP_EOL, $fieldslist);
		$fields = array();
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				$data['fieldcontents']['f'.$e[0]] = '';
				$fields[] = $e;
			}
		}
		
		$hidelist = DocumentFields::getCustomLetterShowHide($row->docversion);
		$hidelines = explode(PHP_EOL, $hidelist);
		$hides = array();
		foreach ($hidelines as $line){
			$hides[] = explode('|', trim($line));
		}
		
		$shortlist = ClientShortlist::with('client')->findOrFail($shortlistid);
		$property = Property::withTrashed()->findOrFail($propertyid);
		$consultant = User::where('id', $shortlist->user_id)->first();
		
		$data['consultantid'] = $shortlist->user_id;
		$data['clientid'] = $shortlist['client']->id;
		
		$data['sectionshidden'] = json_decode($row->sectionshidden, true);
		$data['fieldcontents'] = json_decode($row->fieldcontents, true);
		$data['replace'] = json_decode($row->replace, true);
		$data['id'] = $row->id;
		$data['docversion'] = $row->docversion;
				
		$replaceFields = DocumentFields::getCustomLetterReplace($row->docversion);
		foreach ($replaceFields as $key => $value){
			if (!isset($data['replace'][$key])){
				$data['replace'][$key] = '';
			}
		}
		foreach ($fieldlines as $line){
			$e = explode('|', trim($line));
			if (count($e) >= 3){
				if (!isset($data['fieldcontents']['f'.$e[0]])){
					$data['fieldcontents']['f'.$e[0]] = '';
				}
			}
		}
		
		$html = view('documents.custom-letter-pdf-v1', [
			'fields' => $fields,
			'hides' => $hides,
			'data' => $data,
			'abc' => $this->getabc(),
			'consultant' => $consultant,
			'logo' => $logo
		]);

		if(isset($_GET['test_view']) AND $_GET['test_view']=='datetop'){
			$html = view('documents.custom-letter-pdf-v3', [
				'fields' => $fields,
				'hides' => $hides,
				'data' => $data,
				'abc' => $this->getabc(),
				'consultant' => $consultant,
				'logo' => $logo
			]);
		} else {
			$html = view('documents.custom-letter-pdf-v2', [
				'fields' => $fields,
				'hides' => $hides,
				'data' => $data,
				'abc' => $this->getabc(),
				'consultant' => $consultant,
				'logo' => $logo
			]);
		}
		
		$n = '';
		if ($property){
			$n = ucwords(strtolower($property->name)).' - ';
		}
		$name = 'Custom Letter - '.$n.''.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4')->setWarnings(false)->stream($name);
    }
	
	
	
	
	
	
	
	public function getabc(){
		return array(
			0 => 'a',
			1 => 'b',
			2 => 'c',
			3 => 'd',
			4 => 'e',
			5 => 'f',
			6 => 'g',
			7 => 'h',
			8 => 'i',
			9 => 'j',
			10 => 'k',
			11 => 'l',
			12 => 'm',
			13 => 'n',
			14 => 'o',
			15 => 'p',
			16 => 'q',
			17 => 'r',
			18 => 's',
			19 => 't',
			20 => 'u',
			21 => 'v',
			22 => 'w',
			23 => 'x',
			24 => 'y',
			25 => 'z'
		);
	}
	
	
}
