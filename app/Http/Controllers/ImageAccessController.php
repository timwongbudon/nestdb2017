<?php

namespace App\Http\Controllers;

use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Log;
use Plupload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Redirector;

class ImageAccessController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect){
        $this->middleware('auth');
    }

    /**
	 * Show Image from Images folder
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function showimage($folder, $image){
        $img = Image::make('images/'.$folder.'/'.$image);
        header('Content-Type: image/jpeg');
		header('Cache-Control: max-age=2592000, public');
        echo $img->encode('jpg');
    }
	
	public function showsig($image){
        $img = Image::make('../storage/signatures/'.$image);
        header('Content-Type: image/jpeg');
		header('Cache-Control: max-age=2592000, public');
        echo $img->encode('jpg');
    }
}















