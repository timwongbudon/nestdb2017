<?php

namespace App\Http\Controllers;

use App\District;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use View;
use App\Lead;
use App\LogClick;

class DistrictController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(9);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
					
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $districts_query = District::IndexSearch($request);
        if (isset($_GET['web']) && $_GET['web']==1) {
            $template = 'districts.index-web';
            $districts_query->orderBy('order', 'asc')->orderBy('id', 'desc');
        } else {
            $template = 'districts.index';
            $districts_query->orderBy('id', 'desc');
        }

        $districts = $districts_query->paginate(200)->appends($input);
        return view($template, [
            'districts' => $districts,
        ])->with('input', $request);
    }

    public function option_reorder(Request $request)
    {
        $input = $request->all();
        if (empty($input['options_ids'])) {
            return redirect('/districts?web=1');
        }
        foreach ($input['options_ids'] as $key => $option_id) {
            $District = District::where('id', $option_id)->first();
            if (!empty($District)) {
                $District->fill(array('order'=>$key));
                $District->save();
            }
            unset($District);
        }
        return redirect('/districts?web=1');
    }

    public function frontend_select(Request $request)
    {
        $input = $request->all();
        $is_select = (!empty($input['action']) && $input['action']=='select');
        $this->_frontend_select($input['options_ids'], $is_select);
        if (!empty($input['url'])) {
            return redirect($input['url']);
        } else {
            return redirect('/districts');
        }
    }

    private function _frontend_select($district_ids, $is_select = true) 
    {
        foreach ($district_ids as $district_id) {
            $district = District::find($district_id);
            if (!empty($district)) {
                $district->web = $is_select?1:0;
                $district->save();
            }
        }
    }

    public function frontend_export(Request $request)
    {
        $districts = District::where('web', 1)->orderBy('order', 'asc')->orderBy('id', 'desc')->paginate(200);
        foreach ($districts as $key => $district) {
            printf("\"%d\"=>\"%s\", <br/>", $district->id, $district->name);
        }
        die;
    }

}
