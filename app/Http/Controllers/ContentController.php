<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('whatsnew');
    }
}
