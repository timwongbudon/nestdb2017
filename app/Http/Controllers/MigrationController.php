<?php

/**
 * depreciated - migration for old database.
 */

namespace App\Http\Controllers;

use App\Property;
use App\PropertyMeta;
use App\PropertyFeature;
use App\PropertyPhoto;
use App\PropertyOutdoor;
use App\PropertymigrationMeta;
use App\Building;
use App\ClientShortlist;
use Input;
use Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

class MigrationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->fix_shortlist_user_id();die;
        // $this->gen_list();
        // return $this->gen_photo_index();die;
        // return $this->export_old_properties();die;
        // return $this->fix_carpark_num();die;
        // return $this->fix_carpark_num2();die;
        // return $this->fix_contact_info();die;
        // return $this->fix_building_address();die;
        // return $this->fix_carpark_v2();die;
    }

    private function fix_shortlist_user_id() {
        $shortlists = ClientShortlist::with('client')->get();
        foreach ($shortlists as $shortlist) {
            $shortlist->user_id = $shortlist->client->user_id;
            $shortlist->save();
        }
        die(__FUNCTION__);
    }

    private function fix_carpark_v2 () {
        // $properties = $this->list_old_properties();// with external
        $properties = Property::select('id','external_id', 'car_park')
            ->where('external_id', '!=', '')
            ->where('car_park', '>', '0')
            ->orderBy('id', 'ASC')
            ->paginate(20000);
        // dex(count($properties));
        
        $PropertyMeta = new PropertyMeta();
        foreach ($properties as $index => $property) {
            pdump($property->toArray());    
            // $features = json_decode($PropertyMeta->get_post_meta($property->id, 'features', true));
            $features = "";
            $feature = $PropertyMeta::where('property_id', $property->id)->first();
            if (!empty($feature)) {
                $features = json_decode($feature->meta_value);
                if (!in_array(5, $features)) {
                    $features[] = "5";
                    $encoded_string = json_encode($features);
                    $PropertyMeta->update_post_meta($property->id, 'features', $encoded_string); 
                }
            } else {
                $features[] = "5";
                $encoded_string = json_encode($features);
                $PropertyMeta->add_post_meta($property->id, 'features', $encoded_string, true); 
            }
            $PropertyFeature = new PropertyFeature();
            $PropertyFeature->add_options($property->id, $features);

            pdump($property->id);
            pdump($features);
        }
        echo microtime(true) - LARAVEL_START;
        die('END');
    }
    private function fix_building_address() {
        $buildings = Building::orderBy('id', 'DESC')
            // ->where('id', '3663')
            // ->limit(1000)
            // ->limit(10)
            ->get();
    

        foreach ($buildings as $index => $building) {
            $buildings[$index]->fix = $this->_building_address_shorten($building->address1, $building->district());
            if(!empty($buildings[$index]->fix)):
            $data[] = array(
                    'id'       => $buildings[$index]->id,
                    'address1' => $buildings[$index]->address1,
                    'guess'    => $buildings[$index]->fix,
                    'district' => $building->district(),
                    'building name'     => $buildings[$index]->name,
                );
            endif;

        }
        // dex($data);

        \Excel::create('Nest-Building-Checking', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
        die;
    }

    private function _building_address_shorten($address, $district) {
        list($_address) = explode(", " . $district . ",", $address);
        $shorten_address = $_address;
        if (preg_match("/(\d[^,]*),(.+)/", $_address, $m)) {
            $shorten_address = $m[1] . $m[2];
        }
        list($shorten_address) = explode(' / ', $shorten_address);
        return $shorten_address;
    }

    private function fix_carpark_num() {
        $properties = $this->list_old_properties();// with external
        foreach ($properties as $index => $property) {
            $old_carpark_num = $this->get_old_carpark_num($property->external_id);
            printf("%d,%d,%d,%d<br/>", $index, $property->id, $property->external_id, $old_carpark_num);
            if ($old_carpark_num > 0) {
                $migration_meta = new PropertymigrationMeta();
                $migration_meta->add_post_meta($property->id, 'car_park', $old_carpark_num, true); 
                unset($migration_meta);
            }
        }
        die;
    }

    private function fix_carpark_num2() {
        $metas = PropertymigrationMeta::where('meta_key', 'car_park')
                    ->orderBy('id', 'ASC')
                    ->paginate(20000);
        foreach ($metas as $index => $meta) {
            printf("%d,%d,%d<br/>", $index, $meta->property_id, $meta->meta_value);
            $property = Property::where('id', $meta->property_id)->first();
            $property->fill(array('car_park' => $meta->meta_value));
			$property->tst = time();
            $property->save();
            unset($property);
        }
        die;
    }

    private function fix_contact_info() {
        $metas = PropertymigrationMeta::where('meta_key', 'contact_information')
                    ->orderBy('id', 'ASC')
                    ->paginate(20000);
        foreach ($metas as $index => $meta) {
            printf("%d,%d,%s<br/>", $index, $meta->property_id, $meta->meta_value);
            $property = Property::where('id', $meta->property_id)->first();
            $property->fill(array('contact_info' => $meta->meta_value));
			$property->tst = time();
            $property->save();
            unset($property);
        }
        die;
    }

    private function get_old_carpark_num($external_id) {
        $post_meta = DB::connection('mysql2')->table('wp_postmeta')
                    ->where('meta_key', 'phone_number')
                    ->where('meta_value', '!=', '')
                    ->where('post_id', '=', $external_id)
                    ->first();
        return intval($post_meta?$post_meta->meta_value:0);
    }

    private function gen_photo_index() {
        $PropertyPhoto = new PropertyPhoto;
        $properties = $this->list_all_properties();
        foreach ($properties as $property) {
            // $PropertyPhoto->set_featured_photo_001($property->id, $property->external_id); 
            // $PropertyPhoto->set_featured_photo($property->id, $property->external_id); 
            $PropertyPhoto->insert_property_old_photos($property->id); 
        }
        echo (microtime(true) - LARAVEL_START);die;
    }

    private function gen_photo_index_001() {
        $PropertyPhoto = new PropertyPhoto;
        $properties = $this->list_old_properties();
        foreach ($properties as $property) {
            $PropertyPhoto->insert_property_old_photos($property->id); 
        }
        echo (microtime(true) - LARAVEL_START);die;
    }

    private function gen_list() {
        $pattern = "wget http://localnestdb2016:8888/export?page=%d\n";
        for($i=2; $i<=200; $i++) {
            printf($pattern, $i);
        }
        die;
    }

    /**
     * [list_old_properties description]
     * extract properties from new db with external id
     */
    private function list_all_properties() {
        $properties = Property::select('id','external_id')
            ->where('id', '>', '14663')
            ->orderBy('id', 'ASC')
            ->paginate(1000);
        return $properties;
    }

    /**
     * [list_old_properties description]
     * extract properties from new db with external id
     */
    private function list_old_properties() {
        $properties = Property::select('id','external_id')
            ->where('external_id', '!=', '')
            ->orderBy('id', 'ASC')
            ->paginate(20000);
        return $properties;
    }

    /**
     * [export_old_properties description]
     * extract properties from old db
     */
    private function export_old_properties() {
        $posts = DB::connection('mysql2')->table('wp_posts')
            ->select(DB::raw('ID, post_title, post_content, post_date'))
            ->where('post_status', 'publish')
            ->where('post_type', 'property')
            ->where('ID', '>', '26605')
            ->orderBy('ID', 'ASC')
            ->paginate(100);

        // dex($posts->toArray()); 

        if($posts)foreach ($posts as $key=>$post) {
            $posts[$key]->external_id = $post->ID;
            $posts[$key]->name = $post->post_title;
            $posts[$key]->user_id = 1; //super admin
            $posts[$key]->source = 'NESTOLDDB';
            $this->_set_meta_data($post);
            $posts[$key]->outdoors = $this->_get_outdoors_ids($post->ID);
            $posts[$key]->features = $this->_get_features_ids($post->ID);
        }

        $Property = new Property;
        foreach ($posts as $post) {
            $property = $Property->create_migration($post);
            if (!empty($property)) {
                $this->_store_meta($property->id, $post);
                pdump($property->id);
            }
        }
        dex('end');
    }

    private function _store_meta($property_id, $post) {
        $meta = new PropertyMeta();
        $outdoor = new PropertyOutdoor();
        $feature = new PropertyFeature();
        if (isset($post->features)) {
            $encoded_features = json_encode($post->features);
            $meta->add_post_meta($property_id, 'features', $encoded_features, true); 
            $feature->add_options($property_id, $post->features);
        }
        if (isset($post->outdoors)) {
            $encoded_outdoors = json_encode($post->outdoors);
            $meta->add_post_meta($property_id, 'outdoors', $encoded_outdoors, true); 
            $outdoor->add_options($property_id, $post->outdoors);
        }
        $migration_meta = new PropertymigrationMeta();
        $property = (array)$post;
        unset($property['ID']);
        unset($property['post_title']);
        foreach ($property as $key => $value) {
            if (is_array($value)) {
                $encoded_string = json_encode($value);
                $migration_meta->add_post_meta($property_id, $key, $encoded_string, true); 
            } else {
                $migration_meta->add_post_meta($property_id, $key, $value, true); 
            }
        }
    }

    private function _set_meta_data(&$post) {
        $fields = array(
                'dropbox'             => 'unit',
                'address'             => 'address1',
                'deposit'             => 'address2',//'location',
                'contact_information' => 'contact_information',
                'comments'            => 'comments',
                'rent_or_sale'        => 'type_id',
                'price'               => 'price',
                'bedrooms'            => 'bedroom',
                'bathrooms'           => 'bathroom',
                'helpers_quarter'     => 'maidroom',
                'checkbox'            => 'gross_area',
                'net'                 => 'saleable_area',
                'incusive'            => 'inclusive',
                'management_fee'      => 'management_fee',
                'government_rates'    => 'government_rate',
                'availlable_date'     => 'available_date',
            );
        $type_ids = array(
                'Rent' => 1,
                'Sale' => 2,
            );
        $metas = DB::connection('mysql2')->table('wp_postmeta')
                        ->select(DB::raw('post_id, meta_key, meta_value'))
                        ->where('post_id', $post->ID)
                        ->get(); 
        foreach ($metas as $meta) {
            if (isset($fields[$meta->meta_key])) {
                if($fields[$meta->meta_key] == 'type_id') {
                    if (isset($type_ids[$meta->meta_value])) {
                        $post->{$fields[$meta->meta_key]} = $type_ids[$meta->meta_value];
                    }
                } elseif($fields[$meta->meta_key] == 'available_date') {
                    if(!empty($meta->meta_value)) {
                        $_date = @date('Y-m-d', $meta->meta_value);
                        if (!empty($_date)) {
                            $post->{$fields[$meta->meta_key]} = $_date;
                        }
                    }
                } else {
                    $post->{$fields[$meta->meta_key]} = $meta->meta_value;
                }
            }
        }
        if (isset($post->price) && isset($post->type_id)) {
            if ($post->type_id == 1) {
                $post->asking_rent = $post->price;
            } elseif($post->type_id == 2) {
                $post->asking_sale = $post->price;
            }
        }
    }

    private function _get_outdoors_ids($post_id) {
        $fields = array(
                'outdoor_space__terrace' => 1,
                'outdoor_space__balcony' => 2,
                'outdoor_space__rooftop' => 3,
                'outdoor_space__garden'  => 4,
                'outdoor_space__patio'   => 5,
            ); 
        $metas = DB::connection('mysql2')->table('wp_postmeta')
                        ->select(DB::raw('post_id, meta_key, meta_value'))
                        ->where('post_id', $post_id)
                        ->where('meta_key', 'like', "outdoor_space_%")
                        ->get();
        if (empty($metas)) {
            return array();
        }
        $res = array();
        foreach ($metas as $meta) {
            if (isset($fields[$meta->meta_key])) {
                if ($meta->meta_value == 'true') {
                    $res[] = $fields[$meta->meta_key];
                }
            }
        }
        return $res;
    }

    private function _get_features_ids($post_id) {
        $fields = array(
                'features__gymnasium'            => 1,
                'features__swimming_pool'        => 2,
                'features__tennis_court'         => 3,
                'features__squash_court'         => 4,
                'features__car_park'             => 5,
                'features__shuttle_bus'          => 6,
                'features__childrens_playground' => 7,
            ); 
        $metas = DB::connection('mysql2')->table('wp_postmeta')
                        ->select(DB::raw('post_id, meta_key, meta_value'))
                        ->where('post_id', $post_id)
                        ->where('meta_key', 'like', "features__%")
                        ->get(); 
        if (empty($metas)) {
            return array();
        }
        $res = array();
        foreach ($metas as $meta) {
            if (isset($fields[$meta->meta_key])) {
                if ($meta->meta_value == 'true') {
                    $res[] = $fields[$meta->meta_key];
                }
            }
        }
        return $res;
    }
}
