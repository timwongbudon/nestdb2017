<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

use App\ClientShortlist;
use App\Property;
use App\User;
use App\Document;
use App\Documentinfo;
use App\Lead;
use App\Calendar;
use App\ClientViewing;
use App\Commission;
use App\Client;
use App\FormContact;
use App\FormProperty;
use App\Photoshoot;

use Carbon\Carbon;

use View;
use App\LogClick;

class CalendarController extends Controller
{	
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		if(!\Auth::check()){
			$redirect->to('/login')->send();
		}
		
		LogClick::logClick(15);
		
		$headerdata = Lead::headerdata();
		View::share('headerdata', $headerdata);
    }
	
	private function showmonth($user, $month, $year){
		$firstday = intval(date('N', mktime(0, 0, 0, $month, 1, $year)))-1;
		$days = intval(date('t', mktime(0, 0, 0, $month, 1, $year)));
		
		$usersrow = User::orderBy('status', 'asc')->get();
		$users = array();
		foreach ($usersrow as $u){
			$users[$u->id] = $u;
		}
		
		$d = new \DateTime();
		$d->setTimestamp(mktime(0, 0, 0, $month, 1, $year));
		$curmonth = intval($d->format('n'));
		$curyear = intval($d->format('Y'));
		$d->modify('+1 month');
		$nextmonth = intval($d->format('n'));
		$nextyear = intval($d->format('Y'));
		$d->modify('-2 month');
		$prevmonth = intval($d->format('n'));
		$prevyear = intval($d->format('Y'));
		
		$viewings = ClientViewing::whereRaw('MONTH(v_date) = '.$month)->whereRaw('YEAR(v_date) = '.$year)->whereRaw('(status = 0 or v_date < CURDATE())')->with('client')->with('shortlist')->get();
		$varr = array();
		
		foreach ($viewings as $v){
			$d = new \DateTime($v->v_date);
			$day = intval($d->format('j'));
			
			if (!isset($varr[$day])){
				$varr[$day] = array();
			}
			$varr[$day][] = $v;
		}
		
		$photoshoots = Photoshoot::whereRaw('MONTH(v_date) = '.$month)->whereRaw('YEAR(v_date) = '.$year)->whereRaw('(status = 0 or v_date < CURDATE())')->get();
		$parr = array();
		
		foreach ($photoshoots as $v){
			$d = new \DateTime($v->v_date);
			$day = intval($d->format('j'));
			
			if (!isset($parr[$day])){
				$parr[$day] = array();
			}
			$parr[$day][] = $v;
		}
		
		$calendars = Calendar::whereRaw('((MONTH(datefrom) = '.$month.' and YEAR(datefrom) = '.$year.') or (dateto is not null and MONTH(dateto) = '.$month.' and YEAR(dateto) = '.$year.'))')->get();
		$carr = array();
		$leads = array();
		
		foreach ($calendars as $c){
			if ($c->type == 3){
				$dfrom = new \DateTime($c->datefrom);
				$dto = new \DateTime($c->dateto);
				
				while ($dfrom->format('Y-m-d') <= $dto->format('Y-m-d')){
					if (intval($dfrom->format('m')) == $curmonth){
						$day = intval($dfrom->format('j'));
						
						if (!isset($carr[$day])){
							$carr[$day] = array();
						}
						$carr[$day][] = $c;
					}
					$dfrom->modify('+1 day');
				}
			}else{
				if ($c->type == 1 && $c->leadid != null){
					$lead = Lead::where('id', $c->leadid)->with('client')->first();
					$leads[$c->leadid] = $lead;
				}
				$d = new \DateTime($c->datefrom);
				$day = intval($d->format('j'));
				
				if (!isset($carr[$day])){
					$carr[$day] = array();
				}
				$carr[$day][] = $c;
			}
		}
		$times = Calendar::getTimesArray();
		$driveroptions = Calendar::driverOptions();
		$nondriveroptions = Calendar::nondriverOptions();
		$months = Calendar::getMonths();
		
		return view('calendar/show', [
			'firstday' => $firstday,
			'days' => $days,
			'nextmonth' => $nextmonth,
			'nextyear' => $nextyear,
			'prevmonth' => $prevmonth,
			'prevyear' => $prevyear,
			'curmonth' => $curmonth,
			'curyear' => $curyear,
			'users' => $users,
			'varr' => $varr,
			'parr' => $parr,
			'carr' => $carr,
			'leads' => $leads,
			'times' => $times,
			'driveroptions' => $driveroptions,
			'nondriveroptions' => $nondriveroptions,
			'months' => $months
		]);
	}
	
	private function showmonthdirector($user, $month, $year){
		$firstday = intval(date('N', mktime(0, 0, 0, $month, 1, $year)))-1;
		$days = intval(date('t', mktime(0, 0, 0, $month, 1, $year)));
		
		$usersrow = User::orderBy('status', 'asc')->get();
		$users = array();
		foreach ($usersrow as $u){
			$users[$u->id] = $u;
		}
		
		$d = new \DateTime();
		$d->setTimestamp(mktime(0, 0, 0, $month, 1, $year));
		$curmonth = intval($d->format('n'));
		$curyear = intval($d->format('Y'));
		$d->modify('+1 month');
		$nextmonth = intval($d->format('n'));
		$nextyear = intval($d->format('Y'));
		$d->modify('-2 month');
		$prevmonth = intval($d->format('n'));
		$prevyear = intval($d->format('Y'));
				
		$calendars = Calendar::whereRaw('((MONTH(datefrom) = '.$month.' and YEAR(datefrom) = '.$year.') or (dateto is not null and MONTH(dateto) = '.$month.' and YEAR(dateto) = '.$year.'))')->get();
		$carr = array();
		$leads = array();
		
		foreach ($calendars as $c){
			if ($c->type == 3){
				$dfrom = new \DateTime($c->datefrom);
				$dto = new \DateTime($c->dateto);
				
				while ($dfrom->format('Y-m-d') <= $dto->format('Y-m-d')){
					if (intval($dfrom->format('m')) == $curmonth){
						$day = intval($dfrom->format('j'));
						
						if (!isset($carr[$day])){
							$carr[$day] = array();
						}
						$carr[$day][] = $c;
					}
					$dfrom->modify('+1 day');
				}
			}else{
				if ($c->type == 1 && $c->leadid != null){
					$lead = Lead::where('id', $c->leadid)->with('client')->first();
					$leads[$c->leadid] = $lead;
				}
				$d = new \DateTime($c->datefrom);
				$day = intval($d->format('j'));
				
				if (!isset($carr[$day])){
					$carr[$day] = array();
				}
				$carr[$day][] = $c;
			}
		}
		$times = Calendar::getTimesArray();
		$driveroptions = Calendar::driverOptions();
		$nondriveroptions = Calendar::nondriverOptions();
		$months = Calendar::getMonths();
		
		return view('calendar/showdirector', [
			'firstday' => $firstday,
			'days' => $days,
			'nextmonth' => $nextmonth,
			'nextyear' => $nextyear,
			'prevmonth' => $prevmonth,
			'prevyear' => $prevyear,
			'curmonth' => $curmonth,
			'curyear' => $curyear,
			'users' => $users,
			'carr' => $carr,
			'leads' => $leads,
			'times' => $times,
			'driveroptions' => $driveroptions,
			'nondriveroptions' => $nondriveroptions,
			'months' => $months
		]);
	}
	

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
		$user = $request->user();
		
		return $this->showmonth($user, date('n'), date('Y'));
    }
	
    public function currentmonth(Request $request){
		$user = $request->user();
		
		return $this->showmonth($user, date('n'), date('Y'));
	}
	
	public function showthemonth(Request $request, $month, $year){
		$user = $request->user();
		
		return $this->showmonth($user, $month, $year);
	}
	
    public function currentmonthdirectorcalendar(Request $request){
		$user = $request->user();
		
		return $this->showmonthdirector($user, date('n'), date('Y'));
	}
	
	public function showthemonthdirectorcalendar(Request $request, $month, $year){
		$user = $request->user();
		
		return $this->showmonthdirector($user, $month, $year);
	}
	
    public function addcurrentmonth(Request $request){
		$user = $request->user();
		$input = $request->input();	
		
		$cal = new Calendar();
	
		if (isset($input['userid'])){
			$cal->user_id = $input['userid'];
		}else{
			$cal->user_id = $user->id;
		}
		$cal->type = $input['type'];
		if (isset($input['datefrom'])){
			$cal->datefrom = $input['datefrom'];
		}
		if (isset($input['dateto'])){
			$cal->dateto = $input['dateto'];
		}
		if (isset($input['timefrom'])){
			$cal->timefrom = $input['timefrom'];
		}
		if (isset($input['timeto'])){
			$cal->timeto = $input['timeto'];
		}
		if (isset($input['comments'])){
			$cal->comments = $input['comments'];
		}
		if (isset($input['driveroptions'])){
			$cal->driveroptions = $input['driveroptions'];
		}
		if (isset($input['directoronly'])){
			$cal->directoronly = $input['directoronly'];
		}
		if (!empty($input['use_car'])) {
			$cal->use_car = $input['use_car'];
		}

		if($input['type'] == 3){
		

			$year = intval(date('Y'));
			$userVacations = Calendar::where('user_id','=',$user->id)->where('type','=',3)->whereRaw('YEAR(datefrom) = '.$year)->get();			
			$vacation = 0;		
			foreach($userVacations as $userVacation){
				
				$from = new Carbon($userVacation->datefrom);
				$to =  new Carbon($userVacation->dateto);		
				
				$vacation = $vacation + $from->diff($to)->days + 1;

			}

			$fdate = $input['datefrom'];			
			$tdate = $input['dateto'];
			$from = new Carbon($fdate);
			$to =  new Carbon($tdate);		
			
			$thisvacation = $from->diff($to)->days + 1;

			$totalVacation = $vacation + $thisvacation;

			$remaining_vacation = $user->allowed_vacation -  $totalVacation;

			$cal->vacation_text = "Total number of days used: <strong>".$totalVacation." days.</strong><br> Remaining days left: <strong>".$remaining_vacation." days.</strong>";
		
		}

	
		

		$cal->save();
		
		if (isset($input['directoronly']) && $input['directoronly'] == 1){
			return $this->showmonthdirector($user, date('n'), date('Y'));
		}else{
			return $this->showmonth($user, date('n'), date('Y'));
		}
	}
	
	public function addshowthemonth(Request $request, $month, $year){
		$user = $request->user();
		$input = $request->input();
		
		$cal = new Calendar();
		
		if (isset($input['userid'])){
			$cal->user_id = $input['userid'];
		}else{
			$cal->user_id = $user->id;
		}
		$cal->type = $input['type'];
		if (isset($input['datefrom'])){
			$cal->datefrom = $input['datefrom'];
		}
		if (isset($input['dateto'])){
			$cal->dateto = $input['dateto'];
		}
		if (isset($input['timefrom'])){
			$cal->timefrom = $input['timefrom'];
		}
		if (isset($input['timeto'])){
			$cal->timeto = $input['timeto'];
		}
		if (isset($input['comments'])){
			$cal->comments = $input['comments'];
		}
		if (isset($input['driveroptions'])){
			$cal->driveroptions = $input['driveroptions'];
		}
		if (isset($input['directoronly'])){
			$cal->directoronly = $input['directoronly'];
		}
		if (!empty($input['use_car'])) {
			$cal->use_car = $input['use_car'];
		}

		if($input['type'] == 3){
			

			$year = intval(date('Y'));
			$userVacations = Calendar::where('user_id','=',$user->id)->where('type','=',3)->whereRaw('YEAR(datefrom) = '.$year)->get();			
			$vacation = 0;		
			foreach($userVacations as $userVacation){
				
				$from = new Carbon($userVacation->datefrom);
				$to =  new Carbon($userVacation->dateto);		
				
				$vacation = $vacation + $from->diff($to)->days + 1;

			}

			$fdate = $input['datefrom'];			
			$tdate = $input['dateto'];
			$from = new Carbon($fdate);
			$to =  new Carbon($tdate);		
			
			$thisvacation = $from->diff($to)->days + 1;

			$totalVacation = $vacation + $thisvacation;

			$remaining_vacation = $user->allowed_vacation -  $totalVacation;

			$cal->vacation_text = "Total number of days used: <strong>".$totalVacation." days.</strong><br> Remaining days left: <strong>".$remaining_vacation." days.</strong>";
		
		}


		$cal->save();
		
		if (isset($input['directoronly']) && $input['directoronly'] == 1){
			return $this->showmonthdirector($user, $month, $year);
		}else{
			return $this->showmonth($user, $month, $year);
		}
		
	}
	
    public function addhandover(Request $request){
		$user = $request->user();
		$input = $request->input();
		
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		
		$cal = new Calendar();
		$cal->user_id = $user->id;
		$cal->type = 1;
		if (isset($input['datefrom'])){
			$cal->datefrom = $input['datefrom'];
		}
		if (isset($input['leadid'])){
			$cal->leadid = $input['leadid'];
		}
		if (isset($input['timefrom'])){
			$cal->timefrom = $input['timefrom'];
		}
		$cal->save();
				
		return redirect('/lead/edit/'.$cal->leadid);
	}
	
	public function editcalendar(Request $request, $id, $mode = 0){
		$user = $request->user();
		$input = $request->input();
		
		if($user == null){
			return redirect('/login');
		}
		
		$cal = Calendar::where('id', $id)->first();
		$driveroptions = Calendar::driverOptions();
		$nondriveroptions = Calendar::nondriverOptions();

		if ($user->isOnlyDriver() && $cal->type != 3 ){
			return redirect('/calendar');
		}
		
		if ($cal && ($cal->user_id == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			return view('calendar/edit', [
				'cal' => $cal,
				'mode' => $mode,
				'times' => Calendar::getTimesArray(),
				'driveroptions' => $driveroptions,
				'nondriveroptions' => $nondriveroptions
			]);
		}else{
			return redirect('/');
		}
	}
	
	public function changecalendar(Request $request, $id, $mode = 0){
		$user = $request->user();
		$input = $request->input();
		
		if($user == null){
			return redirect('/login');
		}
		
		$cal = Calendar::where('id', $id)->first();		
		$origDateFrom = $cal->datefrom;
		$origDateTo = $cal->dateto;

		$origDateFrom = new Carbon($origDateFrom);
		$origDateTo =  new Carbon($origDateTo);		

		$origVacation = $origDateFrom->diff($origDateTo)->days + 1;

		
		
		if ($user->isOnlyDriver() && $cal->type != 3 ){
			return redirect('/calendar');
		}
		
		if ($cal && ($cal->user_id == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			if (isset($input['datefrom'])){
				$cal->datefrom = $input['datefrom'];
			}
			if (isset($input['dateto'])){
				$cal->dateto = $input['dateto'];
			}
			if (isset($input['timefrom'])){
				$cal->timefrom = $input['timefrom'];
			}
			if (isset($input['timeto'])){
				$cal->timeto = $input['timeto'];
			}
			if (isset($input['comments'])){
				$cal->comments = $input['comments'];
			}
			if (isset($input['driveroptions'])){
				$cal->driveroptions = $input['driveroptions'];
			}
			if (!$user->getUserRights()['Superadmin'] && !$user->getUserRights()['Director']){
				$cal->accepted = 0;
			}
			if (!empty($input['use_car'])) {
				$cal->use_car = $input['use_car'];
			}


			

			if($cal->type == 3){
				

				$year = intval(date('Y'));
				$userVacations = Calendar::where('user_id','=',$cal->user_id)->where('type','=',3)->where('id','!=',$id)->whereRaw('YEAR(datefrom) = '.$year)->get();			
				$vacation = 0;		
				foreach($userVacations as $userVacation){
					
					$from = new Carbon($userVacation->datefrom);
					$to =  new Carbon($userVacation->dateto);		
					
					$vacation = $vacation + $from->diff($to)->days + 1;

				}

				//$revert_remaining_vacation  = $vacation + $origVacation;

				$fdate = $input['datefrom'];			
				$tdate = $input['dateto'];
				$from = new Carbon($fdate);
				$to =  new Carbon($tdate);		
				
				$thisvacation = $from->diff($to)->days + 1;

				$totalVacation = $vacation + $thisvacation;

				$remaining_vacation = $user->allowed_vacation -  $totalVacation;

				

				$cal->vacation_text = "Total number of days used: <strong>".$totalVacation." days.</strong><br> Remaining days left: <strong>".$remaining_vacation." days.</strong>";
			
			}


			$cal->save();
			
			if ($mode == 1 && $cal->type == 1 && $cal->leadid != null){
				return redirect('/lead/edit/'.$cal->leadid);
			}else{
				if ($cal->directoronly == 1){
					return redirect('/directorcalendar');
				}else{
					return redirect('/calendar');
				}
			}
		}else{
			return redirect('/');
		}
	}
	
	public function removecalendar(Request $request, $id, $mode = 0){	
		$user = $request->user();	
		
		if($user == null){
			return redirect('/login');
		}
		
		$cal = Calendar::where('id', $id)->first();
		
		if ($user->isOnlyDriver() && $cal->type != 3 ){
			return redirect('/calendar');
		}

		if ($cal && ($cal->user_id == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			if ($cal){

				$vacationUser = User::where('id', $cal->user_id)->first();
				$fdate = $cal->datefrom;
				$tdate = $cal->dateto;
				$from = new Carbon($fdate);
				$to =  new Carbon($tdate);		
				
				$vacation = $from->diff($to)->days + 1;
				$remaining_vacation = $vacationUser->remaining_vacation + $vacation;
				
				$vacationUser->remaining_vacation = $remaining_vacation;
				$vacationUser->save();
				$cal->delete();
			}
			
			if ($mode == 1){
				return redirect('/vacationlist');
			}else{
				if ($cal->directoronly == 1){
					return redirect('/directorcalendar');
				}else{
					return redirect('/calendar');
				}
			}
		}else{
			return redirect('/calendar');
		}
	}
	
	public function vacationlist(Request $request){	
		$user = $request->user();	
		
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		
		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant']){
			$calendars = Calendar::where('type', 3)->orderBy('datefrom', 'desc')->paginate(30);
			
			$usersrow = User::orderBy('status', 'asc')->get();
			$users = array();
			foreach ($usersrow as $u){
				$users[$u->id] = $u;
			}

			$vac_remaining = array();
			$count = 0;
			$calculation = array();

			foreach ($calendars as $index => $vac){
				$thisyear = date('Y',strtotime($vac->datefrom));
				
				if( isset($users[$vac->user_id]->allowed_vacation)  && $thisyear >= 2021   ){						
					$vac_remaining[$vac->user_id][] = $vac->vacationCount();	
					$calculation[$vac->id][$vac->user_id] = $vac->vacationCount();	
				}
				
			}

			//$revcalculation = rsort($calculation);

			$Rcalculation = collect($calculation)->reverse()->toArray();
			$userRemaining = array();
			$temp = array();
			$temp1 = array();
			//print_r($Rcalculation);
			foreach( $Rcalculation as $key => $value){
				

				if( !isset($temp[key($value)]) ){
					$userRemaining[$key][key($value)]['remaining_days'] = $users[key($value)]->allowed_vacation - end($value);
					$temp[key($value)] = $users[key($value)]->allowed_vacation - end($value);

					$temp1[key($value)] =  end($value);
					$userRemaining[$key][key($value)]['used_days'] = $temp1[key($value)];
					
					

				} else {
					$temp[key($value)] = $temp[key($value)] - end($value);
					$userRemaining[$key][key($value)]['remaining_days'] = $temp[key($value)];

					$temp1[key($value)] = $temp1[key($value)] + end($value);
					$userRemaining[$key][key($value)]['used_days'] = $temp1[key($value)];

					
				}
				
				

			}
			
			return view('calendar/vacationlist', [
				'calendars' => $calendars,
				'users' => $users,
				'vacation_remaining' => $vac_remaining,
				'userRemaining' => $userRemaining
			]);
		}else{
			return redirect('/calendar');
		}
	}

	public function vacationlists(Request $request){	
		$user = $request->user();	
		
		if($user == null){
			return redirect('/login');
		}
		// if ($user->isOnlyDriver()){
		// 	return redirect('/calendar');
		// }
		
		// if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'] || $user->getUserRights()['Accountant']){
		//$q = Calendar::where('type', 3)->orderBy('datefrom', 'desc');
		$q = Calendar::where('type', 3)->orderBy('id', 'desc');

		if (!$user->getUserRights()['Superadmin'] && !$user->getUserRights()['Director'] && !$user->getUserRights()['Admin'] && !$user->getUserRights()['Accountant']){
			$q->where('user_id', $user->id);
		}
		
		$calendars = $q->paginate(30);

		$usersrow = User::orderBy('status', 'asc')->get();
		$users = array();
		foreach ($usersrow as $u){
			$users[$u->id] = $u;
		}



		$vac_remaining = array();
		$count = 0;
		$calculation = array();

		foreach ($calendars as $index => $vac){
			$thisyear = date('Y',strtotime($vac->datefrom));
			
			if(  $thisyear >= 2021   ){						
				$vac_remaining[$vac->user_id][] = $vac->vacationCount();	
				$calculation[$vac->id][$vac->user_id] = $vac->vacationCount();	
			}
			
		}

		//$revcalculation = rsort($calculation);

		$Rcalculation = collect($calculation)->reverse()->toArray();
		$userRemaining = array();
		$temp = array();
		$temp1 = array();
		//print_r($Rcalculation);
		foreach( $Rcalculation as $key => $value){
			

			if( !isset($temp[key($value)]) ){
				$userRemaining[$key][key($value)]['remaining_days'] = $users[key($value)]->allowed_vacation - end($value);
				$temp[key($value)] = $users[key($value)]->allowed_vacation - end($value);

				$temp1[key($value)] =  end($value);
				$userRemaining[$key][key($value)]['used_days'] = $temp1[key($value)];				


			} else {
				$temp[key($value)] = $temp[key($value)] - end($value);
				$userRemaining[$key][key($value)]['remaining_days'] = $temp[key($value)];

				$temp1[key($value)] = $temp1[key($value)] + end($value);
				$userRemaining[$key][key($value)]['used_days'] = $temp1[key($value)];

				
			}

			

		}

		
		
		return view('calendar/vacationlist', [
			'calendars' => $calendars,
			'users' => $users,
			'vacation_remaining' => $vac_remaining,
			'userRemaining' => $userRemaining
		]);
		// }else{
		// 	return redirect('/calendar');
		// }
	}
	
	public function approvecalendar(Request $request, $id, $mode = 0){
		$user = $request->user();
		
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		
		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Director']){
			$cal = Calendar::where('id', $id)->first();

			
			if ($cal){
				$cal->accepted = 1;
				$cal->save();

				
				$constultantname = 'xxx';			
				$users = User::all();
				foreach ($users as $u){
					if ($u->id == $cal->user_id){
						$constultantname = $u->name;
					}
				}
				
				//send email to Office Manager//Email to Accountant
				$subject = 'New holiday has been approved';
				$message = 'A new holiday has been approved for consultant '.$constultantname.' from ';
				$message .= Carbon::createFromFormat('Y-m-d', $cal->datefrom)->format('d/m/Y');
				$message .= ' to ';
				$message .= Carbon::createFromFormat('Y-m-d', $cal->dateto)->format('d/m/Y');
				
				$to = 'accounts@nest-property.com';
				
				$headers[] = 'From: Nest Database <noreply@db.nest-property.com>';
				//$headers[] = 'Bcc: contact@wohok-solutions.com';

				mail($to, $subject, $message, implode("\r\n", $headers));
			}
		}
		
		return redirect('/vacationlist');
	}
	
	public function notedcalendar(Request $request, $id){
		$user = $request->user();
		
		if($user == null){
			return redirect('/login');
		}
		if ($user->isOnlyDriver()){
			return redirect('/calendar');
		}
		
		if ($user->getUserRights()['Superadmin'] || $user->getUserRights()['Accountant']){
			$cal = Calendar::where('id', $id)->first();
			
			if ($cal){
				$cal->accepted = 2;
				$cal->save();
			}
		}
		
		return redirect('/vacationlist');
	}
	
	public function carbooking(Request $request, $date, $id = 0){
		$user = $request->user();
		
		if($user == null){
			return redirect('/login');
		}
		
		$arr = array();
		
		$viewings = ClientViewing::where('v_date', $date)->where('status', 0)->where('driver', 1)->where('id', '!=', $id)->get();
		foreach ($viewings as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Viewing',
				'car' => $v->driver == 1 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);
		}
		
		
		$photoshoots = Photoshoot::where('v_date', $date)->where('status', 0)->where('driver', 1)->get();
		foreach ($photoshoots as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Photoshoot',
				'car' => $v->driver == 1 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);
		}
		
		$calendars = Calendar::where('datefrom', $date)->where('type', 2)->get();
		foreach ($calendars as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Calendar Entry',
				'car' => $v->type == 2 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);
		}
		
		if (count($arr) > 0){
			for ($i = 0; $i < count($arr)-1; $i++){
				for ($a = $i+1; $a < count($arr); $a++){
					if ($arr[$i]['time'] > $arr[$a]['time']){
						$help = $arr[$i];
						$arr[$i] = $arr[$a];
						$arr[$a] = $help;
					}
				}
			}
			
			$str = '';
			foreach ($arr as $a){
				$str .= $a['str'].$a['car'].'<br />';
			}
			return $str;
		}else{
			return 'No Reservation Yet';
		}
	}

	public function carbooking_check(Request $request, $date, $id = 0, $timefrom, $timeto, $car){
		$user = $request->user();
		
		if($user == null){
			//return redirect('/login');
		}

		$timeFrom = strtotime($date. $timefrom);
		$timeTo = strtotime($date. $timeto);

		$return = false;
		
		$arr = array();
		
		$viewings = ClientViewing::where('v_date', $date)->where('status', 0)->where('driver', 1)->where('id', '!=', $id)->where('use_car','=',$car)->get();
		foreach ($viewings as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Viewing',
				'car' => $v->driver == 1 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);

			if((strtotime($date.$v->timefrom) <= $timeFrom) && ($timeFrom <= strtotime($date.$v->timeto))
			|| (strtotime($date.$v->timefrom) <= $timeTo) && ($timeTo <= strtotime($date.$v->timeto))
			){
				$return = true;
			}

			
		}

		
		//return  $v->timefrom .' - '.strtotime($date. $v->timeto).'- '. $timefrom.'- '. $timeto  ;
		
		
		$photoshoots = Photoshoot::where('v_date', $date)->where('status', 0)->where('driver', 1)->where('id', '!=', $id)->where('use_car', '=',$car)->get();
		foreach ($photoshoots as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Photoshoot',
				'car' => $v->driver == 1 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);

			if((strtotime($date.$v->timefrom) <= $timeFrom) && ($timeFrom <= strtotime($date.$v->timeto))
			|| (strtotime($date.$v->timefrom) <= $timeTo) && ($timeTo <= strtotime($date.$v->timeto))
			){
				$return = true;
			}
		}
		
		$calendars = Calendar::where('datefrom', $date)->where('type', 2)->where('id', '!=', $id)->where('use_car', '=',$car)->get();
		foreach ($calendars as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Calendar Entry',
				'car' => $v->type == 2 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);

			if((strtotime($date.$v->timefrom) <= $timeFrom) && ($timeFrom <= strtotime($date.$v->timeto))
			|| (strtotime($date.$v->timefrom) <= $timeTo) && ($timeTo <= strtotime($date.$v->timeto))
			){
				$return = true;
			}
		}

		return array($return);

		
	
	}

	public function viewing_duration_check(Request $request, $date, $numOfProp, $timefrom, $timeto){
		$timeFrom = strtotime($date.$timefrom);
		$timeTo = strtotime($date.$timeto);

		if($numOfProp > 1 ) {
			$allowedTotalMin = (($numOfProp - 1) * 35 ) + 60;
		} else {
			$allowedTotalMin = 60;
		}

		$endTime = strtotime("+".$allowedTotalMin." minutes", $timeFrom);

		if($endTime >= $timeTo){
			return array(0);
		} else {

			//return date('Y-m-d h:i:s a', $endTime);
			return array("Usage of time is over the allowed maximum time per property viewing. Select up to ".date('h:i a', $endTime)." only.");
		}
	}
	
	public function carbookingphoto(Request $request, $date, $id = 0){
		$user = $request->user();
		
		if($user == null){
			return redirect('/login');
		}
		
		$arr = array();
		
		$viewings = ClientViewing::where('v_date', $date)->where('status', 0)->where('driver', 1)->get();
		foreach ($viewings as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Viewing',
				'car' => $v->driver == 1 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);
		}
		
		
		$photoshoots = Photoshoot::where('v_date', $date)->where('status', 0)->where('driver', 1)->where('id', '!=', $id)->get();
		foreach ($photoshoots as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Photoshoot',
				'car' => $v->driver == 1 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);
		}
		
		$calendars = Calendar::where('datefrom', $date)->where('type', 2)->get();
		foreach ($calendars as $v){
			$time = 0;
			if (strstr($v->timefrom, 'am')){
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				$time = $e[0]*60+$e[1];
			}else{
				$t = str_replace('am', '', $v->timefrom);
				$e = explode(':', $t);
				if ($e[0] < 12){
					$time = 12*60+$e[0]*60+$e[1];
				}else{
					$time = $e[0]*60+$e[1];
				}
			}
			
			$arr[] = array(
				'time' => $time,
				'str' => $v->timefrom.'-'.$v->timeto.': Calendar Entry',
				'car' => $v->type == 2 ? ' - Car: <img src="'.$v->getUsedCar().'" width="30">' : ''
			);
		}
		
		if (count($arr) > 0){
			for ($i = 0; $i < count($arr)-1; $i++){
				for ($a = $i+1; $a < count($arr); $a++){
					if ($arr[$i]['time'] > $arr[$a]['time']){
						$help = $arr[$i];
						$arr[$i] = $arr[$a];
						$arr[$a] = $help;
					}
				}
			}
			
			$str = '';
			foreach ($arr as $a){
				$str .= $a['str'].$a['car'].'<br />';
			}
			return $str;
		}else{
			return 'No Reservation Yet';
		}
	}
	
	
	public function carbookings(){
		$usersrow = User::orderBy('status', 'asc')->get();
		$users = array();
		foreach ($usersrow as $u){
			$users[$u->id] = $u;
		}
		
		$d = new \DateTime();
		
		$dd = $d->format('N');
		if ($dd == 7){
			$d->modify('+1 day');
		}else if ($dd > 1){
			$d->modify('-'.($dd-1).' day');
		}
		
		$monday = $d->format('Y-m-d');
		$d->modify('+6 day');
		$saturday = $d->format('Y-m-d');
		
		$viewings = ClientViewing::where('driver', 1)->where('v_date', '>=', $monday)->where('v_date', '<=', $saturday)->whereRaw('status = 0')->with('client')->with('shortlist')->get();
		$varr = array();
		
		foreach ($viewings as $v){
			$d = new \DateTime($v->v_date);
			$day = intval($d->format('j'));
			
			$varr[] = $v;
		}
		
		$photoshoots = Photoshoot::where('driver', 1)->where('v_date', '>=', $monday)->where('v_date', '<=', $saturday)->whereRaw('status = 0')->get();
		$parr = array();
		
		foreach ($photoshoots as $v){
			$d = new \DateTime($v->v_date);
			$day = intval($d->format('j'));
			
			$parr[] = $v;
		}
		
		$calendars = Calendar::where('type', '=', 2)->where('datefrom', '>=', $monday)->where('datefrom', '<=', $saturday)->get();
		$carr = array();
		$leads = array();
		
		foreach ($calendars as $c){
			if ($c->type == 3){
				$dfrom = new \DateTime($c->datefrom);
				$dto = new \DateTime($c->dateto);
				
				while ($dfrom->format('Y-m-d') <= $dto->format('Y-m-d')){
					if (intval($dfrom->format('m')) == $curmonth){
						$day = intval($dfrom->format('j'));
						
						$carr[] = $c;
					}
					$dfrom->modify('+1 day');
				}
			}else{
				if ($c->type == 1 && $c->leadid != null){
					$lead = Lead::where('id', $c->leadid)->with('client')->first();
					$leads[$c->leadid] = $lead;
				}
				$d = new \DateTime($c->datefrom);
				$day = intval($d->format('j'));
				
				$carr[] = $c;
			}
		}
		
		$usersrow = User::orderBy('status', 'asc')->get();
		$users = array();
		foreach ($usersrow as $u){
			$users[$u->id] = $u;
		}
		
		return view('calendar/carbooking', [
			'varr' => $varr,
			'parr' => $parr,
			'carr' => $carr,
			'monday' => $monday,
			'saturday' => $saturday,
			'users' => $users
		]);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
