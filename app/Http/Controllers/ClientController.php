<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientMeta;
use App\ClientShortlist;
use App\ClientViewing;
use App\ClientViewingOption;
use App\Property;
use App\PropertyPhoto;
use App\User;
use App\Clientdocuments;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ClientRepository;

use View;
use App\Lead;
use Carbon\Carbon;
use App\LogClick;

class ClientController extends Controller
{
	/**
     * The client repository instance.
     *
     * @var Client Repository
     */
    protected $clients;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClientRepository $clients, Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(14);
			
			if ($user->isOnlyDriver() || $user->isOnlyPhotographer()){
				$redirect->to('/calendar')->send();
			}
			
			$this->clients = $clients;
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
	 * Display a list of all of the user's client.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
    {
        $input = $request->all();

		$q = Client::IndexSearch($request);
		
		if($request->sort){
			$sort = explode("_", $request->sort);
			if($sort[0] == 'consultant'){
				$q->leftJoin('users', function($join){
					return $join->on('users.id', '=', 'clients.user_id');
				})->orderBy('users.name', $sort[1]);
			}else{
				$q->orderBy($sort[0], $sort[1]);
			}
		}else{
			$q->orderBy('id', 'desc');
		}

		$clients = $q->paginate(20)->appends($input);

        return view('clients.index', [
            'clients' => $clients,
			'consultants' => User::where('roles', 'like', '%"Consultant":true%')
				->where('status', User::ACTIVE)
				->orderBy('name', 'asc')
				->get()
		])->with('input', $request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

	/**
     * Create a new client.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        $this->validate($request, Client::$rules, Client::$messages);
        $data = Input::all();
        $client = $request->user()->clients()->create($data);
        $client->shortlists()->create(array('user_id' => $user->id));
		
		if (isset($data['email']) && trim($data['email']) != ''){
			\NestEdmSubscription::addEmail($data['email'], 15);
		}
		if (isset($data['email2']) && trim($data['email2']) != ''){
			\NestEdmSubscription::addEmail($data['email2'], 15);
		}
		
        return redirect('/clients');
    }

    public function api_store(Request $request)
    {
		$user = \Auth::user();
        $this->validate($request, Client::$rules, Client::$messages);
        $data = Input::all();
        $client = $request->user()->clients()->create($data);
        $shortlist = $client->shortlists()->create(array('user_id' => $user->id));
        return response()->json(array('error'=>0, 'shortlist_id'=>$shortlist->id));
    }

    /**
     * Create a new client shortlist
     *
     * @param  Request  $request
     * @return Response
     */
    public function store_shortlist($client_id, Request $request)
    {
		$user = \Auth::user();
        $client = Client::findOrFail($client_id);
        $client->shortlists()->create(array('user_id' => $user->id));
        return redirect('/client/show/' . $client_id);
    }

    /**
     * Manage a client shortlist
     * - put client id in session
     *
     * @param  Request  $request
     * @return Response
     */
    public function manage_shortlist_start($shortlist_id, Request $request)
    {
        $request->session()->put('shortlist_id', $shortlist_id);
        return redirect('/list');
    }
    
    public function manage_shortlist_end(Request $request)
    {
        if($request->session()->has('shortlist_id')) {
            $request->session()->forget('shortlist_id');
        }
        return redirect('/clients');
    }

    public function manage_shortlist_update($shortlist_id, Request $request)
    {
        if (empty($request->property_ids)) {
            return Response::json(array('error'=>true));
        }
        $shortlist = ClientShortlist::with('client')->findOrFail($shortlist_id);
        $shortlist['client']->save_shortlist_options($shortlist_id, $request->property_ids);
        return response()->json(array('error'=>false));
    }

    public function shortlist_index($shortlist_id, Request $request) {
        $shortlist = ClientShortlist::with('client')->findOrFail($shortlist_id);
        $options = $shortlist->options()->lists('property_id')->toArray();
        $properties = Property::ShortlistSearch($shortlist_id)
                        ->with('owner')->with('agent')
                        ->with('featured_photo')
                        ->with('indexed_photo')
                        ->whereIn('properties.id', $options)
                        ->orderBy('properties.id','desc')->get();
		
        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
        }
		
		$viewings = ClientViewing::where('shortlist_id', '=', $shortlist_id)->get();
		$viewingproperties = ClientViewingOption::where('shortlist_id', '=', $shortlist_id)->orderBy('viewing_id', 'asc')->get();
		$vps = array();
		foreach ($viewingproperties as $vp){
			if (isset($vps[$vp->viewing_id])){
				$vps[$vp->viewing_id][$vp->property_id] = $vp;
			}else{
				$vps[$vp->viewing_id] = array();
				$vps[$vp->viewing_id][$vp->property_id] = $vp;
			}
		}
		
		$curuser = $request->user();
		$sluser = null;
		$usersObj = User::all();
		$users = array();
		
		foreach ($usersObj as $u){
			if ($u->status == 0 || $shortlist->user_id == $u->id){
				$users[$u->id] = $u->name;
			}
			if ($shortlist->user_id == $u->id){
				$sluser = $u;
			}
		}
		
		$userreadonly = true;
		$rights = $curuser->getUserRights();
		if ($rights['Superadmin'] || $rights['Director'] || $curuser->id == $shortlist->user_id){
			$userreadonly = false;
		}
		
        return view('shortlists.show', [
            'properties' => $properties,
			'shortlist' => $shortlist,
			'input' => $request,
			'vps' => $vps,
			'viewings' => $viewings,
			'viewingproperties'=>$viewingproperties,
            'previous_url' => \NestRedirect::previous_url(),
			'users' => $users,
			'curuser' => $curuser,
			'userreadonly' => $userreadonly,
			'sluser' => $sluser
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $client = Client::findOrFail($id);
		$docuploads = Clientdocuments::where('clientid', '=', $id)->get();
        $shortlists = $client->shortlists()->with('options')->orderBy('status', 'asc')->limit(20)->get();
		$leads = Lead::where('clientid', '=', $id)->get();
        return view('clients.show', compact('client'))
			->with('docuploads', $docuploads)
			->with('shortlists', $shortlists)
			->with('leads', $leads);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
		
		$docuploads = Clientdocuments::where('clientid', '=', $id)->get();
		$users = User::all();
		$consultants = array();
		foreach ($users as $uu){
			$consultants[$uu->id] = $uu;
		}

		$offering="";
		if(Input::get('offering')!== null){
			$offering="?offering=true";
		}
		
        return view('clients.edit', compact('client'))
			->with('docuploads', $docuploads)
			->with('consultants', $consultants)
			->with('previous_url', \NestRedirect::previous_url().$offering);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), Client::$rules, Client::$messages);
        if( !$validator->fails() ) {
            $client = Client::findOrFail($request->id);
            $client->fill($request->all());
            $client->save();
			
			if (isset($data['email']) && isset($data['emailold']) && trim($data['email']) != '' && trim($data['emailold']) != trim($data['email'])){
				\NestEdmSubscription::addEmail($data['email'], 15);
			}
			if (isset($data['email2']) && isset($data['email2old']) && trim($data['email2']) != '' && trim($data['email2old']) != trim($data['email2'])){
				\NestEdmSubscription::addEmail($data['email2'], 15);
			}
			
            return !empty($data['previous_url'])?redirect($data['previous_url']):redirect('/clients');
        }
        return redirect()->back()->withErrors($validator)->withInput();
    }

    /**
     * Destroy the given client.
     *
     * @param  Request  $request
     * @param  Client  $client
     * @return Response
     */
    public function destroy(Request $request, Client $client)
    {
        // $this->authorize('destroy', $client);
        $client->delete();
        return redirect('/clients');
    }
	
	
	
	
    public function getcomments(Request $request, $id){
		$user = $request->user();
		
        $client = Client::findOrFail($id);
        if (empty($client)) {
            return '0';
        }
		
		if ($client->comments == null || trim($client->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $client);
		}else{
			$short = substr(trim($client->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($client->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($client->comments)
					)
				);
				$this->storecomments($id, $comments, $client);
			}
		}
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		$datetime = date_create();
		
		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';
			
			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				//$comments[$i]['datum'] = date_format($datetime, 'Y-m-d G:i');
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('buildings.commentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }
	
    public function addcomment(Request $request, $id){
        $client = Client::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($client)) {
            return '0';
        }
		
		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}
		
		if (trim($client->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($client->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($client->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($client->comments)
					)
				);
			}
		}
		
		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;
		
		$client->fill([
			'comments' => json_encode($comments)
		])->save();
		
		return 'ok';
    }
	
	public function removecomment(Request $request, $id){
        $client = Client::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($client)) {
            return '0';
        }
		
		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}
		
		if (trim($client->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($client->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($client->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($client->comments)
					)
				);
			}
		}
		
		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}
		
		$client->fill([
			'comments' => json_encode($comments)
		])->save();
		
		return 'ok';
	}
	
	
	public function storecomments($id, $comments, $client){
		$client->fill([
			'comments' => json_encode($comments)
		])->save();
	}
	
	
	
    public function docstoreupload($clientid, Request $request){
		$input = $request->all();
		$clientid = intval($clientid);
		
		$rules = array(
			'upfile' =>  'mimes:pdf,jpeg,jpg,png'
		);
		$validator = Validator::make($request->all(), $rules);
		
		$saved = false;		
		if ($validator->errors()->count() == 0){
			if (Input::hasFile('upfile') && Input::file('upfile')->isValid() && $validator->errors()->count() == 0){
				$path = '../storage/clientdocs/';
				$ext = strtolower(Input::file('upfile')->getClientOriginalExtension());
				$name = $clientid.'_'.time().'_'.$input['doctype'].'.'.$ext;
				Input::file('upfile')->move($path, $name);
				
				$doc = new Clientdocuments();
				$doc->doctype = $input['doctype'];
				$doc->clientid = $clientid;
				$doc->consultantid = $request->user()->id;
				$doc->docpath = $path.$name;
				$doc->save();
			}
		}
		
		return redirect('/client/edit/'.$clientid);
	}
	
	public function showdoc($docid){
		$docupload = Clientdocuments::where('id', '=', $docid)->first();
		
		if ($docupload){
			if (file_exists($docupload->docpath)){
				return response()->download($docupload->docpath);
			}
		}else{
			return redirect('/');
		}
	}
	
	public function showpic($docid){
		$docupload = Clientdocuments::where('id', '=', $docid)->first();
		
		if ($docupload){
			if (file_exists($docupload->docpath)){
				return Image::make($docupload->docpath)->response();
			}
		}else{
			return redirect('/');
		}
	}
	
	public function removedoc(Request $request, $docid, $clientid){	
		$user = $request->user();	
		
		$doc = Clientdocuments::where('id', '=', $docid)->first();
		
		if ($doc && ($doc->consultantid == $user->id || $user->getUserRights()['Superadmin'] || $user->getUserRights()['Director'])){
			$doc->delete();
			
			return redirect('/client/edit/'.$clientid);
		}else{
			return redirect('/clients');
		}
	}


	public function check_client(Request $request){	

		$firstname = $request->clientfirstname;
		$lastname = $request->clientlastname;
		$email = $request->clientemail;


		$users = Client::where('firstname','like','%'.$firstname.'%');
		$users->where('lastname','like','%'.$lastname.'%');
		$users->where('email','like','%'.$email.'%');

		$msg = "<br><p>Would you like to proceed adding this client? Please click Confirm to proceed.</p>";
		if(isset($request->id) &&  $request->id !=""){
			$users->where('id','!=',$request->id);
			$msg = "<br><p>Would you like to proceed updating this client? Please click Confirm to proceed.</p>";
		}

		$count = $users->count();
		$html="";
		if($count > 0){
			$results = $users->get();
			$html = "<strong>Similar Clients</strong><br><br><ul>";

			foreach($results as $result){
				$html .= '<li> <a target="_blank" style="color: #FFF;" href="/client/show/'.$result->id.'">'.$result->firstname.' '.$result->lastname.' ('.$result->id.')</a></li>';
			}
			$html .= "</ul>";
			$html .= $msg;

			$return = array('count' => $count, 'results'=>$html );

		} else {
			$return = array('count' => $count, 'results'=>$html );
		}

		echo json_encode($return);
	}
	
	public function client_lookup(Request $request){
		$keyword = $request->keyword;

		$res = Client::where('firstname', 'like', "%{$keyword}%")
		->orWhere('lastname', 'like', "%{$keyword}%")
		->orWhere('id', 'like', "%{$keyword}%")
		->orWhere('email', 'like', "%{$keyword}%")
		->limit(20)
		->orderBy('id', 'desc')
		->get();

		return response()->json($res->toArray());

				
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
