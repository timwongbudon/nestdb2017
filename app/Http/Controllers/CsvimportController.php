<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

use App\ClientShortlist;
use App\Property;
use App\User;
use App\Document;
use App\Documentinfo;
use App\Csvimport;
use App\Csvimportproperty;
use App\Building;

use View;
use App\Lead;
use App\LogClick;

class CsvimportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(10);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
					
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		return redirect('/shortlists');
    }
	
	//Create 1 table that basically has all the information about the upload + one column as json with the ones that failed
	//Create another table with 1 to * and 1 entry per row in the CSV
	//Then have a UI where you can go in 1 by 1 and either create, update, ... buildings, properties, etc. (that might then be forwarded e.g. to Grace)
	//Create a mask for Naomi to upload things / also a menu point in the menu on the left
	
    public function csvindex(){
		$uploads = Csvimport::orderBy('tst', 'desc')->paginate(20);
		
		return view('csvimport.index', [
			'uploads' => $uploads
		]);
	}
	
    public function csvupload(Request $request){
		$input = $request->all();
				
		if (Input::hasFile('csvdoc') && Input::file('csvdoc')->isValid()){
			$path = '../storage/csvimport/';
			$t = time();
			$name = $t.'.csv';
			Input::file('csvdoc')->move($path, $name);
			
			$c = new Csvimport();
			$c->tst = $t;
			$c->save();
			
			$c->runImport();
		}
		
		return redirect('/csvindex');
	}
	
    public function csvuploadshow($cid){
		$show = Csvimportproperty::where('cid', '=', $cid)->orderBy('status', 'asc')->orderBy('available', 'asc')->paginate(20);
		$stats = Csvimportproperty::select(DB::raw('count(*) as statcount, status'))->where('cid', '=', $cid)->groupBy('status')->orderBy('status', 'asc')->get();
		$upload = Csvimport::where('id', '=', $cid)->first();
		
		if (!$upload){
			return redirect('/csvindex');
		}
		
		$processed = 0;
		$waiting = 0;
		
		foreach ($stats as $st){
			if ($st->status == 1){
				$waiting = $st->statcount;
			}else if ($st->status == 2){
				$processed = $st->statcount;
			}
		}
		
		return view('csvimport.show', [
			'show' => $show,
			'stats' => $stats,
			'upload' => $upload,
			'waiting' => $waiting,
			'processed' => $processed
		]);
	}
	
    public function csvpshow($id){
		$show = Csvimportproperty::where('id', '=', $id)->first();
		
		if (!$show){
			return redirect('/csvindex');
		}
		
		//search for buildings
		$search = trim($show->building);
		if (strstr($show->building, '-')){
			$search = trim(substr($show->building, 0, strpos($show->building, '-')-1));
		}
		
		$buildings = Building::where('name', 'like', $search.'%')->get();
		
		
		
		return view('csvimport.csvpshow', [
			'show' => $show,
			'buildings' => $buildings
		]);
	}
	
    public function csvstatusupdate($id, Request $request){
		$input = $request->all();
		
		$csv = Csvimportproperty::where('id', '=', $id)->first();
		
		if ($csv && isset($input['newstatus'])){
			$csv->status = intval($input['newstatus']);
			
			$csv->save();
			
			if ($csv->status == 2){
				return redirect('/csvuploadshow/'.$csv->cid);
			}
		}
		
		return redirect('/csvpshow/'.$id);
	}
	
	//New Property based on Building ID and data
    public function csvnewproperty($id, Request $request){
		$input = $request->all();
		
		if (!isset($input['bid']) || !is_numeric($input['bid'])){
			return redirect('/csvpshow/'.$id);
		}
		
		$csv = Csvimportproperty::where('id', '=', $id)->first();
		$building = Building::where('id', '=', intval($input['bid']))->first();
		
		if ($csv && $building){
			$p = new Property();
			
			$p->name = $building->name;
			$p->display_name = $building->display_name;
			$p->building_id = $building->id;
			$p->address1 = $building->address1;
			$p->address2 = $building->address2;
			$p->district_id = $building->district_id;
			$p->country_code = $building->country_code;
			$p->year_built = $building->year_built;
			
			$p->unit = $csv->unit;
			$p->floornr = $csv->floor;
			$p->type_id = $csv->type;
			$p->bedroom = $csv->bedrooms;
			$p->bathroom = $csv->bathrooms;
			$p->gross_area = $csv->size;
			$p->asking_rent = $csv->lease;
			$p->asking_sale = $csv->sale;
			$p->inclusive = $csv->inclusive;
			$p->management_fee = $csv->mfee;
			$p->government_rate = $csv->govrates;
			$p->car_park = $csv->parking;
			$p->available_date = $csv->available;
			
			$comment = 'Automatic comment based on CSV imported data | ';
			if (trim($csv->keyloc) != ''){
				$comment .= 'Key location: '.$csv->keyloc.PHP_EOL;
			}
			$comment .= ' | Additional info:';
			if (trim($csv->contactinfo) != ''){
				$comment .= ' '.$csv->contactinfo.PHP_EOL;
			}
			if (trim($csv->finfo1) != ''){
				$comment .= ' '.$csv->finfo1.PHP_EOL;
			}
			if (trim($csv->finfo2) != ''){
				$comment .= ' '.$csv->finfo2.PHP_EOL;
			}
			if (trim($csv->details) != ''){
				$comment .= ' '.$csv->details.PHP_EOL;
			}
			$comments = array();
			$newcomment = array(
				'userid' => $request->user()->id,
				'datetime' => time(),
				'content' => $comment
			);
			$comments[] = $newcomment;
			$p->comments = json_encode($comments);
			
			$p->tst = time();
			$p->save();
			
			$csv->pid = $p->id;
			$csv->save();
			
			return redirect('/property/edit/'.$p->id);
		}
		
		return redirect('/csvpshow/'.$id);
	}
	
	//Update existing Property based on Property ID and data
    public function csvupdateproperty($id, Request $request){
		$input = $request->all();
		
		if (!isset($input['pid']) || !is_numeric($input['pid'])){
			return redirect('/csvpshow/'.$id);
		}
		
		$csv = Csvimportproperty::where('id', '=', $id)->first();
		$p = Property::where('id', '=', intval($input['pid']))->first();
		
		if ($csv && $p){
			$p->unit = $csv->unit;
			$p->floornr = $csv->floor;
			$p->type_id = $csv->type;
			$p->bedroom = $csv->bedrooms;
			$p->bathroom = $csv->bathrooms;
			$p->gross_area = $csv->size;
			$p->asking_rent = $csv->lease;
			$p->asking_sale = $csv->sale;
			$p->inclusive = $csv->inclusive;
			$p->management_fee = $csv->mfee;
			$p->government_rate = $csv->govrates;
			$p->car_park = $csv->parking;
			$p->available_date = $csv->available;
			
			$comment = 'Automatic comment based on CSV imported data | ';
			if (trim($csv->keyloc) != ''){
				$comment .= 'Key location: '.$csv->keyloc.PHP_EOL;
			}
			$comment .= ' | Additional info:';
			if (trim($csv->contactinfo) != ''){
				$comment .= ' '.$csv->contactinfo.PHP_EOL;
			}
			if (trim($csv->finfo1) != ''){
				$comment .= ' '.$csv->finfo1.PHP_EOL;
			}
			if (trim($csv->finfo2) != ''){
				$comment .= ' '.$csv->finfo2.PHP_EOL;
			}
			if (trim($csv->details) != ''){
				$comment .= ' '.$csv->details.PHP_EOL;
			}
			$comments = array();
			if (trim($p->comments) != ''){
				$comments = json_decode($p->comments);
			}
			$newcomment = array(
				'userid' => $request->user()->id,
				'datetime' => time(),
				'content' => $comment
			);
			$comments[] = $newcomment;
			$p->comments = json_encode($comments);
			
			$p->tst = time();
			$p->save();
			
			$csv->pid = $p->id;
			$csv->save();
			
			return redirect('/property/edit/'.$p->id);
		}
		
		return redirect('/csvpshow/'.$id);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
