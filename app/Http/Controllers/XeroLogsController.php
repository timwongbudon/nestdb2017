<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\XeroLogs;

class XeroLogsController extends Controller
{
    public function index(){

        $XeroLogs = XeroLogs::select('xero_logs.message', 'xero_logs.invoiceid', 'xero_logs.created_at', 'users.name')
        ->leftJoin('users', 'xero_logs.user_id', '=', 'users.id')
        ->orderby('xero_logs.created_at','DESC')
        ->paginate(25);

        return view('xerologs.index', [
            'logs' => $XeroLogs
        ]);
    }
}
