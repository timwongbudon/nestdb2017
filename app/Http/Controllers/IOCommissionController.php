<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\IOCommission;
use App\Property;

class IOCommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice = array();
        

        $commission = IOCommission::where('io_commissions.status', '!=', 'draft')
                    ->leftJoin('users', 'io_commissions.user_id', '=', 'users.id')
                    ->leftJoin('properties', 'io_commissions.property_id', '=', 'properties.id')
                    ->leftJoin('vendors', 'properties.owner_id', '=', 'vendors.id')
                    ->leftJoin('commission', 'io_commissions.invoiceId', '=', 'commission.id')
                    ->leftJoin('clients', 'commission.clientid', '=', 'clients.id')
                    ->select(
                        'io_commissions.id as id',
                        'io_commissions.status as status',
                        'io_commissions.commission as commission',
                        'io_commissions.salarydate as salarydate',
                        'io_commissions.property_id as property_id',
                        'io_commissions.updated_at',
                        'users.firstname as firstname',
                        'users.lastname as lastname',
                        'properties.name as name',
                        'vendors.company as company',
                        'vendors.firstname as owner_firstname',
                        'vendors.lastname as owner_lastname',
                        'clients.firstname as client_firstname',
                        'clients.lastname as client_lastname',
                        'commission.commissiondate as commissiondate'
                        )
                    ->orderBy('io_commissions.id','desc')
                    ->paginate(30);
                    //->get();
        
                        

        return view('commission.iocommission', ['commission'=>$commission]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $IOCommission = IOCommission::find($id);

        $IOCommission->status = $request->status;

        $IOCommission->save();

        return redirect('/informationofficer/commission');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function properties()
    {
      
        $io_properties = Property::whereNotNull('information_officer')       
        ->leftJoin('users', 'properties.information_officer', '=', 'users.id') 
        ->select(
            'properties.*',
            'users.firstname as firstname',
            'users.lastname as lastname'
        )
        ->paginate(30);

        
        return view('properties.io_properties', ['io_properties'=>$io_properties]);
    }

}
