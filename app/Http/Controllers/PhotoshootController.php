<?php

namespace App\Http\Controllers;

use App\Photoshoot;
use App\PhotoshootOption;
use App\Property;
use App\Building;
use App\User;
use App\PropertyPhoto;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\ClientRepository;

use View;
use App\Lead;
use App\Calendar;
use App\LogClick;

class PhotoshootController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClientRepository $clients, Redirector $redirect){
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(7);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
			
			$this->clients = $clients;
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
		
        return view('photoshoot.create')
			->with('times', Calendar::getTimesArray());
    }
	
	/**
     * Create a new property.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request){
		$data = $request->all();
		
		$cv = new Photoshoot;
		$cv->fill($data);
		$cv->save();
		
		return redirect('/property/photoshoot');
    }
	
    public function update($photoshoot, Request $request){
		$data = $request->all();
		$cv = Photoshoot::findOrFail($photoshoot);
		
		if ($data['status'] != 10){
			unset($data['commentsafter']);
		}
		if (isset($data['tempature'])){
			unset($data['tempature']);
		}
		
		$cv->fill($data);
		$cv->save();
		
		return redirect('/property/photoshoot');
    }
	
    public function remove($photoshoot, Request $request){
		$cv = Photoshoot::findOrFail($photoshoot);
		$cv->delete();
		
		return redirect('/property/photoshoot');
    }
	
    public function show($photoshoot, Request $request) {
        $photoshoot = Photoshoot::findOrFail($photoshoot);
		
		$ex = PhotoshootOption::where('photoshoot_id', '=', $photoshoot->id)->select('property_id')->orderBy('order', 'asc')->get()->toArray();
		$existing = array();
		foreach ($ex as $e){
			$existing[] = $e['property_id'];
		}
		$ids_ordered = implode(',', $existing);
		
		$properties = Property::whereIn('properties.id', $existing)
			->with('owner')
			->with('agent')
			->with('featured_photo')
			->with('indexed_photo')
			->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
			->paginate(36);
		
		$properties_post = '';
		foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
            $properties[$key]->reps = $property->get_reps();
            $properties[$key]->agents = $property->get_agents();
			$properties_post .= $property->shorten_building_name().PHP_EOL.PHP_EOL;
        }
		
		$commentaftercontent = $photoshoot->commentsafter;
		if (trim($commentaftercontent) == ''){
			$commentaftercontent = $properties_post;
		}
		
		$oldfrom = $photoshoot->getNiceTimeFrom();
		
        return view('photoshoot.show', [
            'properties' => $properties,
			'input' => $request,
			'photoshoot' => $photoshoot,
			'commentaftercontent' => $commentaftercontent,
			'times' => Calendar::getTimesArray(),
			'oldfrom' => $oldfrom
        ]);
    }
	
	function option_remove($photoshoot, Request $request){
        $input = $request->all();
		
        if (empty($input['options_ids'])) {
            return redirect('/photoshoot/show/' . $photoshoot);
        }
        foreach ($input['options_ids'] as $option_id) {
            $cvo = PhotoshootOption::where('property_id', $option_id)->where('photoshoot_id', $photoshoot)->orderby('order', 'asc')->first();
            if (!empty($cvo)) {
                $cvo->delete();
            }
        }
        return redirect('/photoshoot/show/' . $photoshoot);
	}


    public function option_reorder(Request $request){
        $input = $request->all();
        if (empty($input['photoshoot_id'])) {
            return redirect('/property/photoshoot');
        }
        $photoshoot_id = $input['photoshoot_id'];
        if (empty($input['options_ids'])) {
            return redirect('/photoshoot/show/'.$photoshoot_id);
        }
        foreach ($input['options_ids'] as $key => $option_id) {
            $cvo = PhotoshootOption::where('property_id', $option_id)->where('photoshoot_id', $photoshoot_id)->orderby('order', 'asc')->first();
            if (!empty($cvo)) {
                $cvo->fill(array('order'=>$key));
                $cvo->save();
            }
            unset($cvo);
        }
        return redirect('/photoshoot/show/'.$photoshoot_id);
    }
	
    public function show_order($photoshoot_id, Request $request){
        $photoshoot = Photoshoot::findOrFail($photoshoot_id);
		$ex = PhotoshootOption::where('photoshoot_id', $photoshoot_id)->select('property_id')->orderby('order', 'asc')->get()->toArray();
		$options = array();
		foreach ($ex as $e){
			$options[] = $e['property_id'];
		}
		$ids_ordered = implode(',', $options);
        $properties = Property::whereIn('properties.id', $options)
                        ->with('owner')->with('agent')->with('allreps')
                        ->with('featured_photo')
                        ->with('indexed_photo')
						->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
						->paginate(40);

        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
        }
        
        return view('photoshoot.show-order', [
            'properties' => $properties,
        ])->with('photoshoot', $photoshoot)->with('input', $request);

    }
	
	public function api_toggle_type($photoshoot, Request $request)
    {
        $input = $request->all();
        if (!empty($input['property_id'])) {
            $option = PhotoshootOption::where('photoshoot_id', $photoshoot)->where('property_id', $input['property_id'])->orderby('order', 'asc')->first();
			if (empty($option)) {
				return redirect('/photoshoot/show/'.$photoshoot);
			}
			$option->type_id = $option->type_id==1?2:1;
			$option->save();
        }
        return redirect('/photoshoot/show/'.$photoshoot);
    }
	
	public function addtoshoot($photoshoot_id, Request $request){
		$input = $request->all();
		
		if ($photoshoot_id > 0){
			$openshoot = Photoshoot::where('id', $photoshoot_id)->first();
		}
		if ($openshoot != null){
			$existing = PhotoshootOption::where('photoshoot_id', '=', $openshoot->id)->withTrashed()->get();
			$ex = array();
			$trashed = array();
			if (count($existing) > 0){
				foreach ($existing as $e){
					$ex[intval($e->property_id)] = 1;
					if ($e->trashed()){
						$trashed[intval($e->property_id)] = $e;
					}
				}
			}
			
			if (is_array($input['options_ids']) && count($input['options_ids']) > 0){
				foreach ($input['options_ids'] as $option_id) {
					if (!isset($ex[intval($option_id)])){
						$tid = 1;
						$data = array(
							'photoshoot_id' => $openshoot->id,
							'property_id' => $option_id,
							'order' => 0,
							'type_id' => $tid
						);
						
						$voption = new PhotoshootOption;
						$voption->fill($data);
						$voption->save();
					}else if (isset($trashed[intval($option_id)])){
						$trashed[intval($option_id)]->restore();
					}
					$property = Property::where('id', '=', $option_id)->first();
					if ($property){
						$property->photoshoot = 0;
						$property->save();
					}
				}
			}
			return redirect('/photoshoot/show/'.$openshoot->id);
		}
		
		return redirect('/property/photoshoot');
	}
	
	public function main_remove(Request $request){
		$input = $request->all();
		
		if (is_array($input['options_ids']) && count($input['options_ids']) > 0){
			foreach ($input['options_ids'] as $option_id) {
				$property = Property::where('id', '=', $option_id)->first();
				if ($property){
					$property->photoshoot = 0;
					$property->save();
				}
			}
		}
		
		return redirect('/property/photoshoot');
	}
	

    public function option_print(Request $request) {
        $input = $request->all();
		
        $properties = Property::IndexSearch($request)
						->orderBy('properties.district_id', 'asc')
                        ->with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $input['options_ids'])
                        ->orderBy('id','desc')->paginate(20);
        $html = view('shortlists.printpdf', [
            'properties' => $properties
        ]);
		
		$name = 'Photoshoot Property Listings - '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a5', 'landscape')->setWarnings(false)->stream($name);
    }

    public function option_print_agent(Request $request) {
        $input = $request->all();

        $properties = Property::IndexSearch($request)
						->orderBy('properties.district_id', 'asc')
                        ->with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $input['options_ids'])
                        ->orderBy('id','desc')->paginate(20);
        $html = view('shortlists.printpdfagent', [
            'properties' => $properties
        ]);
		
		$name = 'Photoshoot Property Listings Agent - '.date('d-m-Y').'.pdf';
		
        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
    }
	
}
