<?php

namespace App\Http\Controllers;

use App\Vendor;
use App\VendorMeta;
use App\Building;
use App\Property;
use App\User;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;
use App\Repositories\VendorRepository;
use DB;

use View;
use App\Lead;
use Carbon\Carbon;
use App\LogClick;



class VendorController extends Controller
{
	/**
     * The vendor repository instance.
     *
     * @var Vendor Repository
     */
    protected $vendors;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VendorRepository $vendors, Redirector $redirect)
    {
        $this->middleware('auth');
		
		$user = \Auth::user();
		
		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(2);
			
			if ($user->isOnlyDriver()){
				$redirect->to('/calendar')->send();
			}
			
			$this->vendors = $vendors;
			
			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * API
     */
    public function api_child_index($parent_id, Request $request)
    {
        $vendors = array();
        $vendors = Vendor::select(array('id', 'firstname', 'lastname', 'company', 'tel'))->where('parent_id', $parent_id)
                ->orderBy('firstname', 'asc')->limit(100)->get();
        $error = empty($vendors);
        return response()->json([
                'error' => $error,
                'content' => $vendors
            ]);
    }

    public function api_index($type, Request $request)
    {
        switch ($type) {
            case 'owner':
                $type_id = [1, 3];
                break;
            case 'rep':
                $type_id = [2];
                break;
            case 'agency':
                $type_id = [3, 1];
                break;
            case 'agent':
                $type_id = [4];
                break;
            default:
                die;
                break;
        }
        $keywords = $request->input('keywords', '');
        $vendors = array();
        if (!empty($keywords)) {
            $vendors = Vendor::wherein('type_id', $type_id)
                ->where(function($query) use ($keywords){
                    $query->orWhere('company', 'like', "%{$keywords}%")
                        ->orWhere('firstname', 'like', "%{$keywords}%")
                        ->orWhere('lastname', 'like', "%{$keywords}%")
                        ->orWhereRaw('CONCAT(firstname, \' \', lastname) like ?', ['%'.$keywords.'%'])
                        ->orWhere('email', 'like', "%{$keywords}%")
                        ->orWhere('tel', 'like', "%{$keywords}%")
                        ->orWhere('firstname2', 'like', "%{$keywords}%")
                        ->orWhere('lastname2', 'like', "%{$keywords}%")
                        ->orWhereRaw('CONCAT(firstname2, \' \', lastname2) like ?', ['%'.$keywords.'%'])
                        ->orWhere('email2', 'like', "%{$keywords}%")
                        ->orWhere('tel2', 'like', "%{$keywords}%");
                })
                ->orderBy('firstname', 'asc')->limit(20)->get();
        }
        foreach ($vendors as $key => $vendor) {
            $vendors[$key]->fix_vendor_name();
        }
        $action_array = array(array('name' => 'Manage Vendor', 'id' => '', 'action' => 'manage-item'));
		if( !empty($vendors) ){			
			return response()->json(array_merge($vendors->toarray(), $action_array));
		} else {				
			return response()->json(array_merge($vendors, $action_array));
		}
    }

    /**
	 * Display a list of all of the user's vendor.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
    {
        $input = $request->all();
	    return view('vendors.index', [
            'vendors' => Vendor::IndexSearch($request)->orderBy('id','desc')->with('parent')->paginate(20)->appends($input),
        ])->with('input', $request);
    }

	/**
     * Create a new vendor.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        if (!empty($data['type_id'])) {
            $this->validate($request, Vendor::$_rules[$data['type_id']], Vendor::$_messages[$data['type_id']]);
        } else {
            $this->validate($request, Vendor::$rules, Vendor::$messages);
        }
		if ($data['type_id'] == 2 || $data['type_id'] == 4){
			$data['corporatelandlord'] = 0;
		}
        $vendor = $request->user()->vendors()->create($data);
		
		if (isset($data['email']) && trim($data['email']) != '' && isset($data['edm']) && $data['edm'] == 1){
			\NestEdmSubscription::addEmail($data['email'], 16);
		}

        return redirect('/vendors');
    }

    /**
     * Destroy the given vendor.
     *
     * @param  Request  $request
     * @param  Vendor  $vendor
     * @return Response
     */
    public function destroy(Request $request, Vendor $vendor)
    {
        // $this->authorize('destroy', $vendor);
        $vendor->delete();
        return redirect('/vendors');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendors.create')->with('district_ids', Building::district_ids())->with('country_ids', Building::country_ids());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendor = Vendor::with('parent')->with('children')->findOrFail($id);
		
		$properties = array();
		if ($vendor){
			if ($vendor->type_id == 1){
				//Owner
				$properties = Property::where('owner_id', '=', $vendor->id)->orderBy('available_date', 'desc')->get();
			}else if ($vendor->type_id == 2){
				//Rep
				$ps = Property::where('rep_ids', 'like', '%'.$vendor->id.'%')->orderBy('available_date', 'desc')->get();
				foreach ($ps as $p){
					$reps = explode(',', $p->rep_ids);
					foreach ($reps as $rep){
						if (intval($rep) == $vendor->id){
							$properties[] = $p;
						}
					}
				}
			}else if ($vendor->type_id == 3){
				//Agency
				$properties = Property::where('agent_id', '=', $vendor->id)->orderBy('available_date', 'desc')->get();
			}else if ($vendor->type_id == 4){
				//Agent
				$ps = Property::where('agent_ids', 'like', '%'.$vendor->id.'%')->orderBy('available_date', 'desc')->get();
				foreach ($ps as $p){
					$reps = explode(',', $p->agent_ids);
					foreach ($reps as $rep){
						if (intval($rep) == $vendor->id){
							$properties[] = $p;
						}
					}
				}
			}
		}
		
        return view('vendors.show', compact('vendor'))
			->with('district_ids', Building::district_ids())
			->with('properties', $properties)
			->with('country_ids', Building::country_ids());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::with('parent')->with('children')->findOrFail($id);
		
		$properties = array();
		if ($vendor){
			if ($vendor->type_id == 1){
				//Owner
				$properties = Property::where('owner_id', '=', $vendor->id)->orderBy('available_date', 'desc')->get();
			}else if ($vendor->type_id == 2){
				//Rep
				$ps = Property::where('rep_ids', 'like', '%'.$vendor->id.'%')->orderBy('available_date', 'desc')->get();
				foreach ($ps as $p){
					$reps = explode(',', $p->rep_ids);
					foreach ($reps as $rep){
						if (intval($rep) == $vendor->id){
							$properties[] = $p;
						}
					}
				}
			}else if ($vendor->type_id == 3){
				//Agency
				$properties = Property::where('agent_id', '=', $vendor->id)->orderBy('available_date', 'desc')->get();
			}else if ($vendor->type_id == 4){
				//Agent
				$ps = Property::where('agent_ids', 'like', '%'.$vendor->id.'%')->orderBy('available_date', 'desc')->get();
				foreach ($ps as $p){
					$reps = explode(',', $p->agent_ids);
					foreach ($reps as $rep){
						if (intval($rep) == $vendor->id){
							$properties[] = $p;
						}
					}
				}
			}
		}
		
        return view('vendors.edit', compact('vendor'))
            ->with('previous_url', \NestRedirect::previous_url())
            ->with('district_ids', Building::district_ids())
            ->with('properties', $properties)
			->with('country_ids', Building::country_ids());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        if (!empty($data['type_id'])) {
            $data['parent_id'] = null;
            $validator = Validator::make($request->all(), Vendor::$_rules[$data['type_id']], Vendor::$_messages[$data['type_id']]);
        } else {
            $validator = Validator::make($request->all(), Vendor::$rules, Vendor::$messages);
        }
        // $validator = Validator::make($request->all(), Vendor::$rules, Vendor::$messages);
        if( !$validator->fails() ) {
            $vendor = Vendor::findOrFail($request->id);
            $vendor->fill($request->all());
            $vendor->save();
			
			if (isset($data['email']) && trim($data['email']) != '' && isset($data['edm']) && $data['edm'] == 1 && isset($data['edmold']) && $data['edmold'] == 0){
				\NestEdmSubscription::addEmail($data['email'], 16);
			}else if (isset($data['email']) && trim($data['email']) != '' && isset($data['edm']) && $data['edm'] == 1 && isset($data['emailold']) && trim($data['email']) != trim($data['emailold'])){
				\NestEdmSubscription::addEmail($data['email'], 16);
			}
			
            return !empty($data['previous_url'])?redirect($data['previous_url']):redirect('/vendors');
        }
        return redirect()->back()->withErrors($validator)->withInput();
    }
	
    public function remove(Request $request)
    {
        $id = intval($request->id);
		
		if ($id > 0){
            $vendor = Vendor::findOrFail($id);
            $vendor->delete();
		}
        
        return redirect('/vendors');
    }
	
    public function getcomments(Request $request, $id){
		$user = $request->user();
		
        $vendor = Vendor::findOrFail($id);
        if (empty($vendor)) {
            return '0';
        }
		
		if (trim($vendor->comments) == ''){
			$comments = array();
			$this->storecomments($id, $comments, $vendor);
		}else{
			$short = substr(trim($vendor->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($vendor->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($vendor->comments)
					)
				);
				$this->storecomments($id, $comments, $vendor);
			}
		}
		
		$users = User::all();
		$u = array();
		foreach ($users as $uu){
			$u[$uu->id] = $uu;
		}
		
		$datetime = date_create();
		
		for ($i = 0; $i < count($comments); $i++){
			$comments[$i]['name'] = 'Nest Property';
			$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			$comments[$i]['datum'] = 'initial comments';
			
			if ($comments[$i]['userid'] == 0 || !isset($u[$comments[$i]['userid']])){
				$comments[$i]['name'] = 'Nest Property';
				$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
			}else{
				$uu = $u[$comments[$i]['userid']];
				
				if ($uu->status == 0 || $request->user()->getUserRights()['Superadmin'] || $request->user()->getUserRights()['Director']){
					$comments[$i]['name'] = $uu->name;
					if ($uu->picpath != ''){
						$comments[$i]['pic'] = $uu->picpath;
					}else{
						$comments[$i]['pic'] = '/showimage/users/dummy.jpg';
					}
				}
			}
			if ($comments[$i]['datetime'] == 0){
				$comments[$i]['datum'] = 'initial comments';
			}else{
				date_timestamp_set($datetime, $comments[$i]['datetime']);
				//$comments[$i]['datum'] = date_format($datetime, 'Y-m-d G:i');
				$comments[$i]['datum'] = Carbon::createFromFormat('Y-m-d H:i:s', date_format($datetime, 'Y-m-d G:i:s'))->format('d/m/Y H:i');
			}
		}
		for ($i = 0; $i < count($comments)/2; $i++){
			$help = $comments[$i];
			$comments[$i] = $comments[count($comments) - 1 - $i];
			$comments[count($comments) - 1 - $i] = $help;
		}

        return view('vendors.commentsshow', [
			'comments' => $comments,
			'user' => $user
		]);
    }
	
    public function addcomment(Request $request, $id){
        $vendor = Vendor::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($vendor)) {
            return '0';
        }
		
		$comment = '';
		if (isset($input['comment'])){
			$comment = trim(filter_var($input['comment'], FILTER_SANITIZE_STRING));
		}
		
		if (trim($vendor->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($vendor->comments), 0, 2);
			if ($short == '[]' || $short == '[{'){
				$comments = json_decode($vendor->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($vendor->comments)
					)
				);
			}
		}
		
		$newcomment = array(
			'userid' => $user->id,
			'datetime' => time(),
			'content' => $comment
		);
		$comments[] = $newcomment;
		
		$vendor->fill([
			'comments' => json_encode($comments)
		])->save();
		
		return 'ok';
    }
	
	public function removecomment(Request $request, $id){
        $vendor = Vendor::findOrFail($id);
		$user = $request->user();
		$input = $request->all();
        if (empty($vendor)) {
            return '0';
        }
		
		$time = -1;
		if (isset($input['t'])){
			$time = trim(filter_var($input['t'], FILTER_SANITIZE_NUMBER_INT));
		}
		
		if (trim($vendor->comments) == ''){
			$comments = array();
		}else{
			$short = substr(trim($vendor->comments), 0, 2);
			if ($short == '[]' || $short == '[{' || $short == '{"'){
				$comments = json_decode($vendor->comments, true);
			}else{
				$comments = array(
					array(
						'userid' => 0,
						'datetime' => 0,
						'content' => trim($vendor->comments)
					)
				);
			}
		}
		
		for ($i = 0; $i < count($comments); $i++){
			if ($comments[$i]['datetime'] == $time){
				array_splice($comments, $i, 1);
				break;
			}
		}
		
		$vendor->fill([
			'comments' => json_encode($comments)
		])->save();
		
		return 'ok';
	}
	
	public function storecomments($id, $comments, $vendor){
		$vendor->fill([
			'comments' => json_encode($comments)
		])->save();
	}
	
	
	public function phonesearch(Request $request, $phone){
        $input = $request->all();
		
		$vendors = array();
		if (trim($phone) != ''){
			$vendors = Vendor::whereRaw('(tel like "%'.intval($phone).'%" or tel2 like "%'.intval($phone).'%") and deleted_at is null')->limit(100)->get();
		}
		
	    return response()->json($vendors);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


}
