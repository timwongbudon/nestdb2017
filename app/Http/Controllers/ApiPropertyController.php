<?php

namespace App\Http\Controllers;

use App\Property;
use App\Building;
use App\Vendor;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Repositories\PropertyRepository;

use App\PropertyMeta;
use App\PropertyMedia;
use App\PropertyPhoto;
use App\PropertymigrationMeta;
use Log;
use Plupload;
use Illuminate\Support\Facades\DB;

class ApiPropertyController extends Controller
{
	/**
     * The property repository instance.
     *
     * @var Property Repository
     */
    protected $properties;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PropertyRepository $properties)
    {
        $this->properties = $properties;
    }

    /**
	 * Display a list of all of the user's property.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
    {
        $input = $request->all();
        $properties = Property::ApiFrontendSearch($request)
                        ->ApiKeywordSearch($request)
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id!="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority, IF(available_date!="0000-00-00", 1, 0) as date_priority2'))
                        ->with('building')
                        ->with('nest_photos')
                        ->IndexSearchAdvancedSort($request)
                        ->paginate(6)->appends($input);
                        // debug
                        // ->IndexSearchAdvancedSort($request);
                        //die($properties->toSql());
        $res = array();
        foreach ($properties as $key => $property) {
            $res[] = $property->api_output();
        }
		
		//debug
		/*foreach ($res as $r){
			echo $r['propertyId'].': '.$r['isLeased'].'<br />';
		}
		echo PHP_EOL.PHP_EOL.PHP_EOL;
		exit;*/
		//debug end
		
        $info = $properties->toArray();
        $info['perPage'] = $info['per_page'];
        $info['currentPage'] = $info['current_page'];
        $info['lastPage'] = $info['last_page'];
        unset($info['data']);
        unset($info['next_page_url']);
        unset($info['prev_page_url']);
        unset($info['from']);
        unset($info['to']);
        unset($info['per_page']);
        unset($info['current_page']);
        unset($info['last_page']);
        // dex($properties->toArray());
        return response()->json(array(
                    'error'      => 0,
                    'info'       => $info,
                    'properties' => $res,
                ));
    }
    
	public function PropertyResultsCounter(Request $request)
    {
       $input = $request->all();
        $date12monthsago = date('Y-m-01', strtotime('-455 days'));//15months ago
       
        $input['districts']= '0,'.$input['districts'];
        $districtsArray = explode(",",$input['districts']);
        
        $input['features']= '0,'.$input['features'];
        $featuresArray = explode(",",$input['features']);
        
       $properties = DB::table('properties')->select('properties.id','properties.name','properties.address1')
               ->leftjoin('property_features', 'property_features.property_id', '=', 'properties.id')
               ->where('web', 1);
       
       $properties->where('available_date', '>=', $date12monthsago);
       $properties->where('building_id', '>', 0);
   
       if(!(empty($districtsArray)) ){
            
           if($districtsArray[1]>0){
               $properties->whereIn('district_id', $districtsArray);
           }
           
       }
       
       
       if(!(empty($featuresArray))){//features not empty
           
            foreach($featuresArray as $feature){

                if($feature>0){
                    $properties->where('f'.$feature, '=', 1);
                }
                
            }
            
       }
       
       if($input['type_id']==1){//for rent
            $properties->whereIn('type_id', [1,3]); //2 is sale and 3 is rent/sale
       }
       
       if($input['type_id']==2){//for sale
            $properties->whereIn('type_id', [2,3]); //2 is sale and 3 is rent/sale
       }
       
       if($input['keywords']){//search text
            $properties->where('name', 'like', "%{$input['keywords']}%");
       }   
       
       if($input['bedroom_min']  && $input['bedroom_min']!='all'){//min bedrooms
            $properties->where('bedroom', '>=', $input['bedroom_min']);
       }
       
       if($input['bedroom_max'] && $input['bedroom_max']!='all'){//max bedrooms
            $properties->where('bedroom', '<=', $input['bedroom_max']);
       } 
       
       if($input['size_min'] && $input['size_min']!='all'){//min bedrooms
            $properties->where('gross_area', '>=', $input['size_min']);
       } 
       
       if($input['size_max'] && $input['size_max']!='all'){//max bedrooms
            $properties->where('gross_area', '<=', $input['size_max']);
       } 
       
       if($input['price_max'] && $input['type_id']==1  && $input['price_max']!='all'){//price_max
            $properties->where('asking_rent', '<=', $input['price_max']);
       } 
       
       if($input['price_min'] && $input['type_id']==1  && $input['price_min']!='all'){//price_min
            $properties->where('asking_rent', '>=', $input['price_min']);
       } 
       
       if($input['price_max'] && $input['type_id']==2  && $input['price_max']!='all'){//price_max
            $properties->where('asking_sale', '<=', $input['price_max']);
       } 
       
       if($input['price_min'] && $input['type_id']==2  && $input['price_min']!='all'){//price_min
            $properties->where('asking_sale', '>=', $input['price_min']);
       } 

       $propertiesLoadedArray=$properties->get();
       $propertiesLoadedCount=count($propertiesLoadedArray);
       
        return response()->json(array(
                    'error'      => 0,
                    'propertiesLoaded' => $propertiesLoadedArray,
                    'propertiesCount' => $propertiesLoadedCount,
                ));
    }    

    public function show($property_id, Request $request)
    {
        if (empty($property_id)) {
            return response()->json(array(
                    'error'    => 1,
                    'property' => array(),
                ));
        }
        if (is_numeric($property_id)) {
            $property = Property::where('id', $property_id)
                // ->where('web', 1)
                ->with('building')->first();
        }
        $res = array();
        if(!empty($property)) {
            $res = $property->api_output();
            $related_res = $this->_get_related_properties($property, $request);
        }
        return response()->json(array(
                    'error'             => 0,
                    'property'          => $res,
                    'relatedProperties' => $related_res,
                ));
    }
	
    public function showquick($property_id, Request $request)
    {
        if (empty($property_id)) {
            return response()->json(array(
                    'error'    => 1,
                    'property' => array(),
                ));
        }
        if (is_numeric($property_id)) {
            $property = Property::where('id', $property_id)
                // ->where('web', 1)
                ->with('building')->first();
        }
        $res = array();
        if(!empty($property)) {
            $res = $property->api_output();
        }
        return response()->json(array(
			'error'             => 0,
			'property'          => $res,
		));
    }

    private function _get_related_properties($property, Request $request) {
        if (in_array($property->type_id, array(1,3))) { // not - for sale
            if (empty($property->asking_rent)) return array();
            $price_min = $property->asking_rent * 0.8;
            $price_max = $property->asking_rent * 1.2;
        } elseif (in_array($property->type_id, array(2,3))) { // for sale
            if (empty($property->asking_sale)) return array();
            $price_min = $property->asking_sale * 0.8;
            $price_max = $property->asking_sale * 1.2;
        } 
        $_request = array(
			'price_min'   => $price_min,
			'price_max'   => $price_max,
			'district_id' => $property->district_id,
			'excluded_id' => $property->id,
		);
        if ($property->country_code != 'hk') {
            $_request['type_id'] = 4;
        }else{
			if ($property->type_id == 3 && $property->leased == 0){
				$_request['type_id'] = 1;
				$_request['leased'] = 0;
			}else if(in_array($property->type_id, array(2,3)) && $property->sold == 0){
				$_request['type_id'] = 2;
			}else if ($property->type_id == 2){
				$_request['type_id'] = 2;
			}else{
				$_request['type_id'] = 1;
				$_request['leased'] = 0;
			}
		}
        $request->replace($_request);

        $properties = Property::ApiFrontendSearch($request)
                        ->ApiKeywordSearch($request)
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority'))
                        ->with('building')
                        ->with('nest_photos')
                        ->orderBy('leased','asc')
                        ->orderBy('date_priority','asc')
                        ->orderBy('properties.id','desc')->paginate(4);
						
						
        $res = array();
		$added = array();
        foreach ($properties as $key => $property) {
            $res[] = $property->api_output();
			$added[$property->nest_id()] = 1;
        }
		
		if (count($res) < 4){
			$price_min = $price_min * 0.6;
			$price_max = $price_max * 1.4;
			$_request['price_min'] = $price_min;
			$_request['price_max'] = $price_max;
			$request->replace($_request);
			
			$properties = Property::ApiFrontendSearch($request)
				->ApiKeywordSearch($request)
				->IndexSearch($request)
				->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority'))
				->with('building')
				->with('nest_photos')
				->orderBy('leased','asc')
				->orderBy('date_priority','asc')
				->orderBy('properties.id','desc')->paginate(4);
			
			foreach ($properties as $key => $property) {
				if (!isset($added[$property->nest_id()]) && count($res) < 4){
					$res[] = $property->api_output();
					$added[$property->nest_id()] = 1;
				}
			}
		}
		
		if (count($res) < 4){
			$_request['price_min'] = $price_min / 0.6;
			$_request['price_max'] = $price_max / 1.4;
			unset($_request['district_id']);
			$request->replace($_request);
			
			$properties = Property::ApiFrontendSearch($request)
				->ApiKeywordSearch($request)
				->IndexSearch($request)
				->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority'))
				->with('building')
				->with('nest_photos')
				->orderBy('leased','asc')
				->orderBy('date_priority','asc')
				->orderBy('properties.id','desc')->paginate(4);
			
			foreach ($properties as $key => $property) {
				if (!isset($added[$property->nest_id()]) && count($res) < 4){
					$res[] = $property->api_output();
					$added[$property->nest_id()] = 1;
				}
			}
		}
		
		if (count($res) < 4){
			$price_min = 0;
			$price_max = 1000000000;
			$_request['price_min'] = $price_min;
			$_request['price_max'] = $price_max;
			$request->replace($_request);
			$properties = Property::ApiFrontendSearch($request)
				->ApiKeywordSearch($request)
				->IndexSearch($request)
				->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority'))
				->with('building')
				->with('nest_photos')
				->orderBy('leased','asc')
				->orderBy('date_priority','asc')
				->orderBy('properties.id','desc')->paginate(4);
			
			foreach ($properties as $key => $property) {
				if (!isset($added[$property->nest_id()]) && count($res) < 4){
					$res[] = $property->api_output();
					$added[$property->nest_id()] = 1;
				}
			}			
		}
		
        return $res;
    }
	
    /**
	 * Display a list of all of the user's property.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function sitemap(Request $request)
	{
        $input = $request->all();
        /*$properties = Property::ApiFrontendSearch($request)
                        ->ApiKeywordSearch($request)
                        ->IndexSearch($request)
                        ->select(DB::raw('properties.*, ABS(IF(available_date!="0000-00-00" and external_id!="", DATEDIFF(NOW(), available_date), DATEDIFF(NOW(), properties.created_at))) as date_priority, IF(available_date!="0000-00-00", 1, 0) as date_priority2'))
                        ->with('building')
                        ->with('nest_photos')
                        ->IndexSearchAdvancedSort($request)
                        ->paginate(6)->appends($input);*/
                        // debug
                        // ->IndexSearchAdvancedSort($request);
                        // die($properties->toSql());
		/*$properties = DB::table('properties')
			->where('web', 1)
			->get();*/
		//die($properties->toSql());
        $ret = array();
        foreach (Property::where('web', 1)->cursor() as $property) {
            $ret[] = $property->get_property_api_path();
        }
		
		//debug
		/*foreach ($ret as $r){
			echo $r.'<br />';
		}
		die;*/
		//debug end
		
        /*$info = $properties->toArray();
        $info['perPage'] = $info['per_page'];
        $info['currentPage'] = $info['current_page'];
        $info['lastPage'] = $info['last_page'];
        unset($info['data']);
        unset($info['next_page_url']);
        unset($info['prev_page_url']);
        unset($info['from']);
        unset($info['to']);
        unset($info['per_page']);
        unset($info['current_page']);
        unset($info['last_page']);*/
        // dex($properties->toArray());
        return response()->json(array(
                    'error'      => 0,
                    'urls' => $ret,
                ));
    }

    /**
	 * Display a list of all of the user's property.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function full(Request $request){
        $ret = array();
		$i = 0;
        foreach (Property::where('web', 1)->cursor() as $property) {
			if ($i < 100000){
				//$ret[] = $property->api_output();
				$ret[] = $property->id;
			}
			$i++;
        }
		
        return response()->json(array(
                    'error'      => 0,
                    'properties' => $ret,
                ));
    }
	
	public function fullext(Request $request){
        $ret = array();
		$i = 0;
        foreach (Property::where('web', 1)->cursor() as $property) {
			if ($i < 100000){
				$arr = array(
					'id' => $property->id,
					'tst' => $property->tst
				);
				$ret[] = $arr;
			}
			$i++;
        }
		
        return response()->json(array(
                    'error'      => 0,
                    'properties' => $ret,
                ));
    }
}
