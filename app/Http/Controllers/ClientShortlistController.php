<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientMeta;
use App\ClientShortlist;
use App\ClientOption;
use App\ClientViewing;
use App\ClientViewingOption;
use App\Property;
use App\Building;
use App\User;
use Input;
use Image;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;
use App\Repositories\ClientRepository;
use Illuminate\Support\Facades\DB;

use View;
use App\Lead;
use App\LogClick;

class ClientShortlistController extends Controller
{
	/**
     * The client repository instance.
     *
     * @var Client Repository
     */
    protected $clients;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClientRepository $clients, Redirector $redirect)
    {
        $this->middleware('auth');

		$user = \Auth::user();

		if(!\Auth::check() || $user == null){
			$redirect->to('/login')->send();
		}else{
			LogClick::logClick(13);

			if ($user->isOnlyDriver() || $user->isOnlyPhotographer()){
				$redirect->to('/calendar')->send();
			}

			$this->clients = $clients;

			$headerdata = Lead::headerdata();
			View::share('headerdata', $headerdata);
		}
    }

    /**
     * API
     */
    public function api_toggle_type(Request $request)
    {
        $input = $request->all();
        if (!empty($input['shortlist_id']) || !empty($input['property_id'])) {
            $option = ClientOption::where('shortlist_id', $input['shortlist_id'])->where('property_id', $input['property_id'])->first();
        }
        if (empty($option)) {
            return redirect('/shortlist/show/' . $input['shortlist_id']);
        }
        $option->type_id = $option->type_id==1?2:1;
        $option->save();
        return redirect('/shortlist/show/' . $input['shortlist_id']);
    }

    /**
	 * Display a list of all of the user's client.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
    {
        $input = $request->all();
		$curuser = 0;
		if (!isset($input['user_id'])){
			$currole = json_decode($request->user()->roles);
			if ($currole->Consultant){
				$curuser = $request->user()->id;
				$input['user_id'] = $curuser;
			}
		}else{
			$curuser = $input['user_id'];
		}
        $users = User::where('status', '=', 0)->select('id', 'name', 'roles')->orderBy('id', 'desc')->get();
		$user_ids = array();
		foreach ($users as $u){
			$roles = json_decode($u->roles);
			if ($roles->Consultant){
				$user_ids[$u->id] = $u->name;
			}
		}
        $shortlists = ClientShortlist::where('status', '<', 10)->with('client')
        ->with('user')->orderBy('client_shortlists.id', 'desc')
        ->NewIndexSearch($input)
        ->paginate(20)->appends($input);

        return view('shortlists.index', [
            'shortlists' => $shortlists,
			'curuser' => $curuser
        ])->with('tempatures', Client::$tempatures)->with('user_ids', $user_ids)->with('input', $request);
    }

	public function indexall(Request $request)
    {
        $input = $request->all();
		$curuser = 0;
		if (!isset($input['user_id'])){
			$currole = json_decode($request->user()->roles);
			if ($currole->Consultant){
				$curuser = $request->user()->id;
				$input['user_id'] = $curuser;
			}
		}else{
			$curuser = $input['user_id'];
		}
        $users = User::where('status', '=', 0)->select('id', 'name', 'roles')->orderBy('id', 'desc')->get();
		$user_ids = array();
		foreach ($users as $u){
			$roles = json_decode($u->roles);
			if ($roles->Consultant){
				$user_ids[$u->id] = $u->name;
			}
		}
        $shortlists = ClientShortlist::NewIndexSearch($input)->with('client')->with('user')->orderBy('client_shortlists.id', 'desc')->paginate(20);

        return view('shortlists.index', [
            'shortlists' => $shortlists,
			'curuser' => $curuser
        ])->with('tempatures', Client::$tempatures)->with('user_ids', $user_ids)->with('input', $request);
    }

    public function option_print(Request $request) {
        $input = $request->all();
        if (empty($input['shortlist_id'])) {
            return redirect('/shortlists/');
        }
        $shortlist_id = $input['shortlist_id'];
        if (empty($input['options_ids'])) {
            return redirect('/shortlist/show/' . $shortlist_id);
        }
        $shortlist = ClientShortlist::with('client')->findOrFail($shortlist_id);
        $options = $shortlist->options()->lists('property_id')->toArray();
        $res = $options;
        foreach ($options as $key => $value) {
            if (!in_array($value, $input['options_ids'])) {
                unset($res[$key]);
            }
        }
        if (empty($res)) {
            return redirect('/shortlist/show/' . $shortlist_id);
        }

        $properties = Property::IndexSearch($request)
						->orderBy('properties.district_id', 'asc')
                        ->with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $res)
                        ->orderBy('id','desc')->paginate(20);
        $html = view('shortlists.printpdf', [
            'properties' => $properties,
            'shortlist' => $shortlist
        ]);

		$name = '';
		if (trim($shortlist['client']->firstname) != ''){
			$name = trim($shortlist['client']->firstname).' ';
		}else{
			$name = trim($shortlist['client']->lastname).' ';
		}
		$name .= 'Property Listings - '.date('d-m-Y').'.pdf';

        //return $html;
        return \PDF::loadHTML($html)->setPaper('a5', 'landscape')->setWarnings(false)->stream($name);
    }

    public function option_print_agent(Request $request) {
        $input = $request->all();
        if (empty($input['shortlist_id'])) {
            return redirect('/shortlists/');
        }
        $shortlist_id = $input['shortlist_id'];
        if (empty($input['options_ids'])) {
            return redirect('/shortlist/show/' . $shortlist_id);
        }
        $shortlist = ClientShortlist::with('client')->findOrFail($shortlist_id);
        $options = $shortlist->options()->lists('property_id')->toArray();
        $res = $options;
        foreach ($options as $key => $value) {
            if (!in_array($value, $input['options_ids'])) {
                unset($res[$key]);
            }
        }
        if (empty($res)) {
            return redirect('/shortlist/show/' . $shortlist_id);
        }

        $properties = Property::IndexSearch($request)
						->orderBy('properties.district_id', 'asc')
                        ->with('meta')
                        ->with('nest_photos')
                        ->with('other_photos')
                        ->whereIn('id', $res)
                        ->orderBy('id','desc')->paginate(20);
        $html = view('shortlists.printpdfagent', [
            'properties' => $properties,
            'shortlist' => $shortlist
        ]);

		$name = '';
		if (trim($shortlist['client']->firstname) != ''){
			$name = trim($shortlist['client']->firstname).' ';
		}else{
			$name = trim($shortlist['client']->lastname).' ';
		}
		$name .= 'Property Listings Agent - '.date('d-m-Y').'.pdf';

        //return $html;
        return \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->stream($name);
    }

    public function option_select(Request $request)
    {
        $input = $request->all();
        if (empty($input['shortlist_id'])) {
            return redirect('/shortlists/');
        }
        $shortlist_id = $input['shortlist_id'];
        if (empty($input['options_ids'])) {
            return redirect('/shortlist/show/' . $shortlist_id);
        }
        foreach ($input['options_ids'] as $option_id) {
            $ClientOption = ClientOption::where('property_id', $option_id)->where('shortlist_id', $shortlist_id)->first();
            if (!empty($ClientOption)) {
                $ClientOption->delete();
            }
        }
        return redirect('/shortlist/show/' . $shortlist_id);
    }

    public function option_reorder(Request $request)
    {
        $input = $request->all();
        if (empty($input['shortlist_id'])) {
            return redirect('/shortlists/');
        }
        $shortlist_id = $input['shortlist_id'];
        if (empty($input['options_ids'])) {
            return redirect('/shortlist/show/' . $shortlist_id);
        }
        // dex($input['options_ids']);
        foreach ($input['options_ids'] as $key => $option_id) {
            $ClientOption = ClientOption::where('property_id', $option_id)->where('shortlist_id', $shortlist_id)->first();
            if (!empty($ClientOption)) {
                $ClientOption->fill(array('order'=>$key));
                $ClientOption->save();
            }
            unset($ClientOption);
        }
        return redirect('/shortlist/show/' . $shortlist_id);
    }

    public function api_index(Request $request)
    {
        $keywords = $request->input('keywords', '');
        $shortlists = array();
        $shortlists = ClientShortlist::ApiKeywordSearch($keywords)->where('status', '=', 0)->limit(60)->get();
        $error = !empty($shortlists);
        foreach ($shortlists as $key=>$shortlist) {
            unset($shortlists[$key]->preference);
            unset($shortlists[$key]->email);
            $shortlists[$key]->created = \NestDate::nest_datetime_format($shortlists[$key]->created_at);
            unset($shortlists[$key]->created_at);
            unset($shortlists[$key]->updated_at);
        }
        // dex($shortlists->toArray());
        $action_array = array(
                array('name' => 'Create new', 'id' => '', 'action' => 'create-item'),
                array('name' => 'Manage Shortlists', 'id' => '', 'action' => 'manage-item'),
            );
        if (empty($shortlists)) {
            return response()->json(array());
        }
        return response()->json(array_merge($shortlists->toArray(), $action_array));
    }

    public function show_order($shortlist_id, Request $request)
    {
        $shortlist = ClientShortlist::with('client')->findOrFail($shortlist_id);
        $options = $shortlist->options()->lists('property_id')->toArray();
        $properties = Property::ShortlistSearch($shortlist_id)
                        ->with('owner')->with('agent')->with('allreps')
                        ->with('featured_photo')
                        ->with('indexed_photo')
                        ->whereIn('properties.id', $options)
                        ->orderBy('properties.id','desc')->paginate(40);

        // dex($properties->toArray());

        foreach ($properties as $key => $property) {
            if (empty($property['indexed_photo'])) {
                $PropertyPhoto = new PropertyPhoto;
                $photo = $PropertyPhoto->set_featured_photo($property->id, $property->external_id);
                $properties[$key]->featured_photo_url = $photo->featured_photo();
            } else {
                $properties[$key]->featured_photo_url = $property['indexed_photo']->featured_photo();
            }
        }

        return view('shortlists.show-order', [
            'properties' => $properties,
        ])->with('shortlist', $shortlist)->with('input', $request);

    }

	public function store($shortlist_id, Request $request){
		$input = $request->all();

		$data = array();
		if (isset($input['comments'])){
			$data['comments'] = $input['comments'];
		}
		if (isset($input['status']) && is_numeric($input['status'])){
			$data['status'] = intval($input['status']);
		}
		if (isset($input['typeid']) && is_numeric($input['typeid'])){
			$data['typeid'] = intval($input['typeid']);
		}
		if (isset($input['user_id']) && is_numeric($input['user_id'])){
			$data['user_id'] = intval($input['user_id']);
		}

		$shortlist = ClientShortlist::findOrFail($shortlist_id);
		$shortlist->fill($data);
		$shortlist->save();

		if (isset($data['typeid']) && ($data['typeid'] == 1 || $data['typeid'] == 2) && $shortlist_id > 0){
			DB::table('client_options')->where('shortlist_id', $shortlist_id)->update(['type_id' => $data['typeid']]);
			DB::table('client_viewing_options')->where('shortlist_id', $shortlist_id)->update(['type_id' => $data['typeid']]);
		}

		if (isset($input['previous_url'])){
			return redirect($input['previous_url']);
		}else{
			return redirect('/shortlists');
		}

	}

	public function addtoview($shortlist_id, Request $request){
		return $this->addtoviewid($shortlist_id, 0, $request);
	}

	public function addtoviewid($shortlist_id, $vid, Request $request){
		$input = $request->all();

		$shortlist = ClientShortlist::findOrFail($shortlist_id);

		if ($vid > 0){
			$openview = ClientViewing::where('id', $vid)->first();
		}else{
			$viewings = ClientViewing::where('shortlist_id', '=', $shortlist_id)->get();
			$openview = null;
			if (count($viewings) > 0){
				foreach ($viewings as $v){
					if ($v->status == 0){
						$openview = $v;
					}
				}
			}
		}
		if ($openview != null){
			$existing = ClientViewingOption::where('viewing_id', '=', $openview->id)->withTrashed()->get();
			$ex = array();
			$trashed = array();
			if (count($existing) > 0){
				foreach ($existing as $e){
					$ex[intval($e->property_id)] = 1;
					if ($e->trashed()){
						$trashed[intval($e->property_id)] = $e;
					}
				}
			}

			if (is_array($input['options_ids']) && count($input['options_ids']) > 0){
				foreach ($input['options_ids'] as $option_id) {
					if (!isset($ex[intval($option_id)])){
						$tid = 1;
						if ($shortlist->typeid == 2){
							$tid = 2;
						}
						$data = array(
							'client_id' => $openview->client_id,
							'shortlist_id' => $openview->shortlist_id,
							'viewing_id' => $openview->id,
							'property_id' => $option_id,
							'order' => 0,
							'type_id' => $tid
						);

                  

						$voption = new ClientViewingOption;
						$voption->fill($data);
						$voption->save();
					}else if (isset($trashed[intval($option_id)])){
						$trashed[intval($option_id)]->restore();
					}
				}
			}
			return redirect('/viewing/show/'.$openview->id);
		}

		return redirect('/shortlist/show/'.$shortlist_id);
	}





    /**
     * Destroy the given shortlist.
     *
     * @param  Request  $request
     * @param  ClientShortlist  $shortlist
     * @return Response
     */
    // public function destroy(Request $request, ClientShortlist $shortlist)
    // {
    //     $shortlist->delete();
    //     return redirect('/shortlist');
    // }
}
