<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Csvimport extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'csvimport';
	
    protected $fillable = ['id', 'lastupdate', 'tst', 'pcnt', 'failedones'];
	
	
	private function custom_ucwords($str){
		$e = explode(' ', trim($str));
		$ret = '';
		foreach ($e as $s){
			$s = strtolower($s);
			$s = trim($s);
			$s = strtoupper(substr($s, 0, 1)).substr($s, 1);
			$ret .= $s.' ';
		}
		return trim($ret);
	}
	
	public function runImport(){
		if (file_exists('../storage/csvimport/'.$this->tst.'.csv')){
			$file = file('../storage/csvimport/'.$this->tst.'.csv');
			$cnt = 0;
			$failed = array();
			
			for ($i = 1; $i < count($file); $i++){
				$e = explode('	', trim($file[$i]));
				$out = '';
				if (count($e) > 64){
					$data = array();
					$data['cid'] = $this->id;
					
					//5...Address
					$lcaddress = $e[5];
					$lcaddress = $this->custom_ucwords($lcaddress);
					$iex = unpack("C*", trim($lcaddress));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
						if (isset($iex[$ii+1]) && chr($iex[$ii+1]) == ' '){
							$nint .= ' ';
						}
					}
					$lcaddress = $nint;
					$out .= 'Address: '.$lcaddress.'<br />';
					$data['address'] = $lcaddress;
					//6...Building Name
					$lcbuilding = $e[6];
					$iex = unpack("C*", trim($lcbuilding));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
						if (isset($iex[$ii+1]) && chr($iex[$ii+1]) == ' '){
							$nint .= ' ';
						}
					}
					$lcbuilding = $nint;
					$lcbuilding = $this->custom_ucwords($lcbuilding);
					$out .= 'Building: '.$lcbuilding.'<br />';
					$data['building'] = $lcbuilding;
					//7...District
					$iex = unpack("C*", trim($e[7]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
						if (isset($iex[$ii+1]) && chr($iex[$ii+1]) == ' '){
							$nint .= ' ';
						}
					}
					$out .= 'District: '.$nint.'<br />';
					$data['district'] = trim($nint);
					//9...Unit
					$iex = unpack("C*", trim($e[9]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
						if (isset($iex[$ii+1]) && chr($iex[$ii+1]) == ' '){
							$nint .= ' ';
						}
					}
					$out .= 'Unit: '.$nint.'<br />';
					$data['unit'] = trim($nint);
					//10/11...area
					$out .= 'Size: '.$e[10].'<br />';
					$iex = unpack("C*", trim($e[10]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['size'] = trim($nint);
					//13/15...lease rate
					$out .= 'Lease price: '.$e[13].'<br />';
					$iex = unpack("C*", trim($e[13]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['lease'] = $nint;
					//14...lease details
					//$out .= 'Details: '.$e[14].'<br />';
					$info = '';
					$info .= 'Details: '.trim($e[14]).PHP_EOL;
					$iex = unpack("C*", trim($e[14]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['details'] = trim($nint);
					//19...inclusive
					$out .= 'Inclusive: '.$e[19].'<br />';
					$iex = unpack("C*", trim($e[19]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['inclusive'] = trim($nint);
					//20...available date
					//$out .= 'Available date: '.$e[20].'-'.trim($e[21]).'<br />';
					if (strlen($e[20]) > 3){
						$ex = explode(' ', trim($e[20]));
						$mex = unpack("C*", trim($ex[0]));
						$month = chr($mex[1]).chr($mex[3]).chr($mex[5]);				
						$dex = unpack("C*", trim($ex[1]));
						$day = '';
						for ($ii = 1; $ii <= count($dex); $ii = $ii+2){
							$day .= chr($dex[$ii]);
						}
						$yex = unpack("C*", trim($e[21]));
						$year = '';
						for ($ii = 1; $ii <= count($yex); $ii = $ii+2){
							$year .= chr($yex[$ii]);
						}
						$d = date_create_from_format('M d Y', $month.' '.$day.' '.$year);
						$out .= 'Available date: '.$month.' '.$day.' '.$year.'<br />';
						$data['available'] = $d->format('Y-m-d');
					}
					//22...Management fee
					$out .= 'Management fee: '.$e[22].'<br />';
					$iex = unpack("C*", trim($e[22]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['mfee'] = $nint;
					//23...Government rates
					$out .= 'Government rates: '.$e[23].'<br />';
					$iex = unpack("C*", trim($e[23]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['govrates'] = $nint;
					//24...Bedrooms
					$out .= 'Bedrooms: '.$e[24].'<br />';
					$iex = unpack("C*", trim($e[24]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['bedrooms'] = $nint;
					//25...Bathrooms
					$out .= 'Bathrooms: '.$e[25].'<br />';
					$iex = unpack("C*", trim($e[25]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['bathrooms'] = $nint;
					//27...Parking spots
					$out .= 'Parking spots: '.$e[27].'<br />';
					$iex = unpack("C*", trim($e[27]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['parking'] = $nint;
					//26...Key location
					$info .= 'Key location: '.trim($e[26]).PHP_EOL;
					$iex = unpack("C*", trim($e[26]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($nint);
					}
					$data['keyloc'] = trim($e[26]);
					//28...Contact info
					$info .= 'Contact info: '.trim($e[28]).PHP_EOL;
					$iex = unpack("C*", trim($e[28]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['contactinfo'] = trim($nint);
					//33...Further info
					$info .= 'Further info: '.trim($e[33]).PHP_EOL;
					$iex = unpack("C*", trim($e[33]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['finfo1'] = trim($nint);
					//34...Further info
					$info .= 'Further info: '.trim($e[34]).PHP_EOL;
					$iex = unpack("C*", trim($e[34]));
					$nint = '';
					for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
						$nint .= chr($iex[$ii]);
					}
					$data['finfo2'] = trim($nint);
					$base = 35;
					while (strlen($e[$base]) > 4 && $base < 65){
						$base++;
					}
					if (!isset($e[$base+29])){
						//echo '--- Not properly formated start ---<br />'.trim($file[$i]).'<br />--- Not properly formated end ---<br />';
						$failed[] = trim($file[$i]);
					}else{
						//36...Year built
						$out .= 'Year built: '.$e[$base+1].'<br />';
						$iex = unpack("C*", trim($e[$base+1]));
						$nint = '';
						for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
							$nint .= chr($iex[$ii]);
						}
						$data['yearbuilt'] = $nint;
						//42...Street name
						//$out .= 'Street name: '.$e[$base+7].'<br />';
						//43...City
						//$out .= 'City: '.$e[$base+9].'<br />';
						$iex = unpack("C*", trim($e[$base+9]));
						$nint = '';
						for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
							$nint .= chr($iex[$ii]);
							if (isset($iex[$ii+1]) && chr($iex[$ii+1]) == ' '){
								$nint .= ' ';
							}
						}
						$info .= 'City: '.trim($nint).PHP_EOL;
						$data['city'] = trim($nint);
						$out .= nl2br($info);
						$data['info'] = trim($info);
						if (trim($data['district']) == ''){
							$data['district'] = trim($e[$base+9]);
						}
						//48...Usage
						//$out .= 'Usage: '.$e[$base+14].'<br />';
						//56...Floor Nr
						$out .= 'Floor Nr.: '.$e[$base+22].'<br />';
						$iex = unpack("C*", trim($e[$base+22]));
						$nint = '';
						for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
							$nint .= chr($iex[$ii]);
						}
						$data['floor'] = trim($nint);
						//64...Type of Availability
						$out .= 'Type of Availability: ';
						if (trim($e[$base+28]) == 'Sale or Lease'){
							$out .= '3';
							$data['type'] = 3;
						}else if (trim($e[$base+28]) == 'Sale'){
							$out .= '2';
							$data['type'] = 2;
						}else{
							$out .= '1';
							$data['type'] = 1;
						}
						$out .= '<br />';
						//65...Sale price
						$out .= 'Sale price: '.$e[$base+29].'<br />';
						$iex = unpack("C*", trim($e[$base+29]));
						$nint = '';
						for ($ii = 1; $ii <= count($iex); $ii = $ii+2){
							$nint .= chr($iex[$ii]);
						}
						$data['sale'] = $nint;
						
						$imp = new Csvimportproperty();
						$imp->create($data);
						$cnt++;
					}
				}else{
					if (trim($file[$i]) != ''){
						$failed[] = trim($file[$i]);
					}
				}
			}
		}
		
		$this->pcnt = $cnt;
		$this->failedones = json_encode($failed);
		$this->save();
	}
	
}