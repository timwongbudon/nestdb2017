<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Illuminate\Support\Facades\DB;

use App\LogLead;
use App\FormContact;
use App\FormProperty;


use Carbon\Carbon;

class Calendar extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'calendar';
	
    protected $fillable = ['id', 'user_id', 'type', 'datefrom', 'dateto', 'timefrom', 'timeto', 'accepted', 'leadid', 'driveroptions', 'use_car', 'comments','allowed_vacation', 'directoronly'];
	
	public function niceDateFrom(){
		$d = date_create_from_format('Y-m-d', $this->datefrom);
		return date_format($d, 'd/m/Y');
	}
	
	public function niceDayFrom(){
		$d = date_create_from_format('Y-m-d', $this->datefrom);
		return date_format($d, 'l');
	}
	
	public function niceDateTo(){
		$d = date_create_from_format('Y-m-d', $this->dateto);
		return date_format($d, 'd/m/Y');
	}
	
	public function niceDayTo(){
		$d = date_create_from_format('Y-m-d', $this->dateto);
		return date_format($d, 'l');
	}
	
	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
	public function updated_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->updated_at);
		return date_format($d, 'd/m/Y H:i');
	}

	public function vacationCount(){
		$from = new Carbon($this->datefrom);
		$to =  new Carbon($this->dateto);		
		return $from->diff($to)->days + 1;
	}
	
	public static function getTimesArray(){
		return array(
			'07:00 am',
			'7:15 am',
			'7:30 am',
			'7:45 am',
			'8:00 am',
			'8:15 am',
			'8:30 am',
			'8:45 am',
			'9:00 am',
			'9:15 am',
			'9:30 am',
			'9:45 am',
			'10:00 am',
			'10:15 am',
			'10:30 am',
			'10:45 am',
			'11:00 am',
			'11:15 am',
			'11:30 am',
			'11:45 am',
			'12:00 pm',
			'12:15 pm',
			'12:30 pm',
			'12:45 pm',
			'1:00 pm',
			'1:15 pm',
			'1:30 pm',
			'1:45 pm',
			'2:00 pm',
			'2:15 pm',
			'2:30 pm',
			'2:45 pm',
			'3:00 pm',
			'3:15 pm',
			'3:30 pm',
			'3:45 pm',
			'4:00 pm',
			'4:15 pm',
			'4:30 pm',
			'4:45 pm',
			'5:00 pm',
			'5:15 pm',
			'5:30 pm',
			'5:45 pm',
			'6:00 pm',
			'6:15 pm',
			'6:30 pm',
			'6:45 pm',
			'7:00 pm',
			'7:15 pm',
			'7:30 pm',
			'7:45 pm',
			'8:00 pm',
			'8:15 pm',
			'8:30 pm',
			'8:45 pm',
			'9:00 pm',
			'9:15 pm',
			'9:30 pm',
			'9:45 pm',
			'10:00 pm',
			'10:15 pm',
			'10:30 pm',
			'10:45 pm',
		);
	}
	
	public static function driverOptions(){
		return array(
			'Photoshoot', 
			'Key Pickup', 
			'Document Pickup', 
			'Personal',
			'Document Signing / Cheque Collection',
			'Property Reconnaissance',
			'Handover',
			'Client Meeting'
		);
	}
	
	public static function nondriverOptions(){
		return array(
			'Photoshoot', 
			'Key Pickup', 
			'Document Pickup', 
			'Personal', 
			'Interview', 
			'Meeting',
			'Document Signing / Cheque Collection',
			'Property Reconnaissance',
			'Handover',
			'Client Meeting'
		);
	}
	
	public static function getMonths(){
		return array(
			1 => 'January', 
			2 => 'February', 
			3 => 'March', 
			4 => 'April', 
			5 => 'May', 
			6 => 'June', 
			7 => 'July', 
			8 => 'August', 
			9 => 'September', 
			10 => 'October', 
			11 => 'November', 
			12 => 'December', 
		);
	}
	
	public function getNiceTime(){
		$ret = '';
		
		if (trim($this->timefrom) != ''){
			$ret = trim($this->timefrom);
		}
		if (trim($this->timeto) != ''){
			$ret .= '-'.trim($this->timeto);
		}
		
		return $ret;
	}

	public function getUsedCar ()
	{
		switch($this->use_car) {
			case 1: return asset('/alphard_logo.png'); break;
			case 2: return asset('/tesla.png'); break;
                        case 3: return asset('/tesla2.png'); break;
			default: return asset('/alphard_logo.png'); break;
		}
	}
	
	
	
	
	
	
	
	
}