<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Image;

class PropertyPhoto extends BaseModel 
{
	protected $table = 'property_photos';
    protected $fillable = ['property_id', 'nest_photo', 'other_photo', 'old_photo'];
    public $timestamps  = false;
    public $key_id = 'property_id';

    public static $rules = array(
            'property_id' => 'required|numeric',
    	);

	//this and the following function parse the image links from a HK Homes property page and then copy them over to the database
    public function insert_property_hongkonghome_photos($property_id, $url) {
        if (!empty($property_id)) {
            $property = Property::find($property_id);
            if (!empty($property)) {
                $property->fill(['hkh_url' => $url]);
				$property->tst = time();
				$property->pictst = time();
                $property->save();
            }
        }
        $photos = $this->gen_hongkonghome_photos($url);
        // dex([__FUNCTION__, $property_id, $photos]);
        foreach ($photos as $photo) {
            $photo_url = $photo['url'];
            $photo_caption = $photo['caption'];
            $media = new PropertyMedia;
            // check before add media
            $media->add_media(array(
                        'property_id' => $property_id,
                        'group'       => 'other-photo',
                        'url'         => $photo_url,
                        'filename'    => '',
                        'caption'     => $photo_caption,
						'order'		  => 999
                    ));
            unset($media);
        }
        $this->set_featured_photo($property_id);
        return $photos;
    }

    public function gen_hongkonghome_photos($url) {
        $content = file_get_contents($url);
        $html = str_get_html($content);

        $item = array();
		$item['images'] = array();
        // $item['title'] = $html->find('h1', 0)->innertext;
        foreach($html->find('.prop-content-right .tn') as $tn) {
            $image = array();
            $image['url'] = $tn->find('img', 0)->src;
            $image['url'] = str_replace('s=60', 's=600', $image['url']);
			$image['url'] = str_replace('s=80', 's=600', $image['url']);
            $image['caption'] = $tn->find('input', 0)->value;
            $item['images'][] = $image;
        }
        return $item['images'];
    }

	//this and the following function parse the image links from a Proway property page and then copy them over to the database (sorry for the bad naming of the functions)
    public function insert_property_newhongkonghome_photos($property_id, $url) {
        if (!empty($property_id)) {
            $property = Property::find($property_id);
            if (!empty($property)) {
                $property->fill(['hkhnew_url' => $url]);
				$property->tst = time();
				$property->pictst = time();
                $property->save();
            }
        }
        $photos = $this->gen_newhongkonghome_photos($url);
        // dex([__FUNCTION__, $property_id, $photos]);
		
		$time = time();
		$file_path = 'images' .'/'. 'system' .'/';
		
        foreach ($photos as $index => $photo) {
            $photo_url = $photo['url'];
            $media = new PropertyMedia;
			$img_ext = pathinfo($photo_url, PATHINFO_EXTENSION);
			
			$file_name = $property_id.'_'.$time.'_'.($index+1).'.'.$img_ext;
			
			$_img = Image::make($photo_url);
			$_img->save($file_path . $file_name);
			
            // check before add media
            $media->add_media(array(
				'property_id' => $property_id,
				'group'       => 'other-photo',
				'url'         => url($file_path . $file_name),
				'filename'    => '',
				'caption'     => '',
				'order'		  => 999
			));
			
            unset($media);
        }
        $this->set_featured_photo($property_id);
        return $photos;
    }

    public function gen_newhongkonghome_photos($url) {
        $content = file_get_contents($url);
        $html = str_get_html($content);

        $item = array();
		$item['images'] = array();
        // $item['title'] = $html->find('h1', 0)->innertext;
		
		$slides = $html->find('.property-photos .swiper-wrapper .swiper-slide .w-100');
		
        foreach($slides as $tn) {
            $image = array();
            $image['url'] = $tn->attr['data-src'];
            //$image['url'] = str_replace('s=60', 's=600', $image['url']);
			//$image['url'] = str_replace('s=80', 's=600', $image['url']);
            //$image['caption'] = $tn->find('input', 0)->value;
            $item['images'][] = $image;
        }
        return $item['images'];
    }
	
    public function featured_photo() {
        if (!empty($this->nest_photo)) {
            return PropertyImageVariation::read_download_image($this->property_id, 'nest_photo', $this->nest_photo);
        }else if(!empty($this->other_photo)) {
            return PropertyImageVariation::read_download_image($this->property_id, 'other_photo', $this->other_photo);
        }else if(!empty($this->old_photo)) {
            return PropertyImageVariation::read_download_image($this->property_id, 'old_photo', $this->old_photo);
        }
    }

    public function gen_one_photo_index($property_id) {
        $this->gen_property_old_photo($property_id);
        dex($property_id); 
    }

    public function insert_property_old_photos($property_id) 
    {
        $old_photos = $this->get_property_old_photos($property_id);
        if (empty($old_photos)) {
            return false;
        }
        pdump([$property_id => $old_photos]);
        // return;
        foreach ($old_photos as $old_photo) {
            $old_photo_url = $old_photo->url;
            $media = new PropertyMedia;
            // check before add media
            $media->add_media(array(
                        'property_id' => $property_id,
                        'group'       => 'other-photo',
                        'url'         => $old_photo_url,
                        'filename'    => '',
                    ));
            unset($media);
        }
		$property = Property::find($property_id);
		if (!empty($property)) {
			$property->tst = time();
			$property->pictst = time();
			$property->save();
		}
    }

    public function get_property_old_photos($property_id) 
    {
        $PropertymigrationMeta = new PropertymigrationMeta();
        $content = $PropertymigrationMeta->get_post_meta($property_id, 'post_content', true);
        $html = str_get_html($content);
        $photos = array();
        if(!empty($html))foreach ($html->find('img') as $img) {
            $image = new \stdClass();
            $image->url = $img->src;
            $photos[] = $image;
        }
        return $photos;
    }

    public function set_featured_photo($property_id, $property_external_id='') {
        $old_featured_url = '';
        $first_nest_photo_url = $this->get_first_nest_photo_url($property_id);
        $first_other_photo_url = $this->get_first_other_photo_url($property_id);
		
		$property = Property::find($property_id);
		if (!empty($property)) {
			$property->tst = time();
			$property->pictst = time();
			$property->save();
		}
		
        $data = array(
                'property_id' => $property_id,
                'nest_photo'  => $first_nest_photo_url,
                'other_photo' => $first_other_photo_url,
                'old_photo'   => $old_featured_url,
            );
        // pdump([$property_id, $property_external_id, 'data' => $data]);
        if ($this->is_duplicate($property_id)) {
            $photo = $this->where('property_id', $property_id)->first();
            $photo->nest_photo  = $first_nest_photo_url;
            $photo->other_photo = $first_other_photo_url;
            // $photo->old_photo   = $old_featured_url;
            // pdump([$property_id, $photo->nest_photo, $photo->other_photo]);
            return $photo->save();
        } else {
            return $this->create($data);
        }
    }

    // public function set_featured_photo_001($property_id, $property_external_id) {
    //     $old_featured_url = $this->get_property_featured_photo_url($property_external_id);
    //     $first_nest_photo_url = $this->get_first_nest_photo_url($property_id);
    //     $first_other_photo_url = $this->get_first_other_photo_url($property_id);
    //     $data = array(
    //             'property_id' => $property_id,
    //             'nest_photo'  => $first_nest_photo_url,
    //             'other_photo' => $first_other_photo_url,
    //             'old_photo'   => $old_featured_url,
    //         );
    //     pdump([$property_id, $property_external_id, 'data' => $data]);
    //     if ($this->is_duplicate($property_id)) {
    //         $photo = $this->where('property_id', $property_id)->first();
    //         $photo->nest_photo  = $first_nest_photo_url;
    //         $photo->other_photo = $first_other_photo_url;
    //         $photo->old_photo   = $old_featured_url;
    //         return $photo->save();
    //     } else {
    //         return $this->create($data);
    //     }
    // }

    public function get_first_nest_photo_url($property_id) {
        $photo = PropertyMedia::where('property_id', $property_id)->where('group', 'nest-photo')->orderBy('order', 'asc')->first();
        return !empty($photo)?$photo->url:'';
    }

    public function get_first_other_photo_url($property_id) {
        $photo = PropertyMedia::where('property_id', $property_id)->where('group', 'other-photo')->orderBy('order', 'asc')->first();
        return !empty($photo)?$photo->url:'';
    }

    public function get_property_featured_photo_url($property_external_id) {
        $post_id = $this->_get_property_old_featured_photo_id($property_external_id);
        return !empty($post_id)?$this->_get_property_featured_photo_url($post_id):'';
    }

    public function _get_property_featured_photo_url($post_id) {
        $photo = DB::connection('mysql2')->table('wp_posts')
            ->select('guid as url')
            ->where('ID', $post_id)
            ->where('post_type', 'attachment')
            ->first();
        return !empty($photo)?$photo->url:null;
    }

    public function _get_property_old_featured_photo_id($property_external_id) {
        $photo = DB::connection('mysql2')->table('wp_postmeta')
            ->select('meta_value as featured_id')
            ->where('post_id', $property_external_id)
            ->where('meta_key', '_thumbnail_id')
            ->first();
        return !empty($photo)?$photo->featured_id:null;
    }

    public function is_duplicate($id) {
        $count = $this->where($this->key_id, $id)->count();
        return $count > 0;
    }
}
