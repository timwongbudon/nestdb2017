<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyMedia extends BaseMediaModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'property_medias';
    
    protected $fillable = ['property_id', 'external_id', 'group', 'url', 'filename', 'caption', 'alt', 'description', 'order', 'web_order'];
    public $key_id = 'property_id';

    public function get_api_photo() {
        return array(
                'photoId' => $this->id,
                'url'     => $this->_get_url('large'),
                'urlmedium'     => $this->_get_url('medium'),
                'urlsmall'     => $this->_get_url('small'),
                'urlsocial'     => $this->_get_url('social'),
                'thumb'   => $this->_get_url('thumbnail'),
            );
    }
	
	//The problem here is, that some of the images are linked to either HK Homes or Proway, the copying over to our server has been done later on
	//Furthermore, some images (the ones with IP addresses) link to old images that either aren't available anymore or still do exist but on another server
	//Therefore, below code is all a bit of a mess. If not really needed, I wouldn't touch it
    public function _get_url($format) {
        $property = Property::where('id', $this->property_id)->first();
        if (empty($property)) return '';
		
		if (strstr($this->url, 'img.hongkonghomes.com')){
			return $this->url;
		}
		if (strstr($this->url, 'img.proway.com.hk')){
			return $this->url;
		}
		/*if (strstr($this->url, '192.168.1.115:8082')){
			return str_replace('http://192.168.1.115:8082', 'https://db.nest-property.com/cms', $this->url);
		}*/
		
        $file_path = sprintf("/image/%d/%s/%s.jpg", $this->id, $format, $property->property_slug());
        return url($file_path);
    }
	
    public function _get_print_url($format) {
        $property = Property::where('id', $this->property_id)->first();
        if (empty($property)) return '';
		
		if (strstr($this->url, 'img.hongkonghomes.com')){
			return $this->url;
		}
		if (strstr($this->url, 'img.proway.com.hk')){
			return $this->url;
		}
		
        $file_path = sprintf("/image/%d/%s/%s.jpg", $this->id, $format, $property->property_slug());
        return url($file_path);
    }
	
	public function getShowUrl(){
		if (strstr($this->url, '192.168.1.115:8082')){
			return str_replace('http://192.168.1.115:8082', 'https://db.nest-property.com', $this->url);
		}
		
		return $this->url;
	}
	
	public function getShowUrlModal(){
        $property = Property::where('id', $this->property_id)->first();
		$back = $this->_get_url('system-thumbnail').'?pictst='.$property->pictst;
		
		if (strstr($back, '192.168.1.115:8082')){
			return str_replace('http://192.168.1.115:8082', 'https://db.nest-property.com', $back);
		}
		
		if (strstr($back, 'https://db.nest-property.com/cms')){
			return str_replace('https://db.nest-property.com/cms', 'https://db.nest-property.com', $back);
		}
		if (strstr($back, 'http://db.nest-property.com/cms')){
			return str_replace('http://db.nest-property.com/cms', 'https://db.nest-property.com', $back);
		}
		
		return $back;
	}
	
	public function getShowUrlDownload(){
        $property = Property::where('id', $this->property_id)->first();
		$back = $this->_get_url('medium');
		
		if (strstr($back, '192.168.1.115:8082')){
			return str_replace('http://192.168.1.115:8082', 'https://db.nest-property.com', $back);
		}
		
		if (strstr($back, 'https://db.nest-property.com/cms')){
			return str_replace('https://db.nest-property.com/cms', 'https://db.nest-property.com', $back);
		}
		if (strstr($back, 'http://db.nest-property.com/cms')){
			return str_replace('http://db.nest-property.com/cms', 'https://db.nest-property.com', $back);
		}
		
		return $back;
	}

    private function _temp_image_base_url($url, $is_thumb = 0) {
    	$info = pathinfo($url);
        $filename = $info['basename'];
        $directory = 'nest-photo';
        $file_path = 'images' .'/'. $directory .'/' . 'test/';
        if ($is_thumb) {
        	$filename = 'thumb-' . $filename;
        }
        return sprintf(url($file_path) . '/%s', $filename);
    }

    public function Original() {
        return $this->hasOne(PropertyImageVariation::class, 'property_media_id', 'id')->where('format', 'download');
    }

    public function get_local_first_url() {
        return !empty($this->Original)?$this->Original->url:$this->url;
    } 
}
