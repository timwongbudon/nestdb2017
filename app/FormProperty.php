<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Carbon\Carbon;

class FormProperty extends BaseModel 
{
    use SoftDeletes;

    protected $table = 'property_form_submissions';
	
    protected $fillable = ['id', 'ts', 'name', 'phone', 'email', 'budgetfrom', 'budgetto', 'message', 'status', 'newsletter', 'forwarduser','passon_consultantid', 'comments', 'viewedids','form_fields'];
	public $key_id = 'id';
	
	public function getDateTime(){
		return Carbon::createFromFormat('Y-m-d H:i:s', $this->ts, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y H:i');
	}
	
}