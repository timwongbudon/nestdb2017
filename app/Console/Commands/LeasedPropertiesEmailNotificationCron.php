<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Property;
use App\Comminvoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class LeasedPropertiesEmailNotificationCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:leasedpropertiesemailnotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email notification that contains properties that will expire 3 months from current date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      
        $properties_query = Property::select(DB::raw('properties.id'));
        $newDateTime = Carbon::now()->subMonth(3)->format('Y-m-d');
        $newDateTimeEmail = Carbon::now()->addMonth(3)->format('d.m.Y');
        $properties_query->whereraw('properties.available_date = "'.$newDateTime.'" and properties.leased = 1');

        $properties = $properties_query->get();


        ///list for invoices with lease term more that 1 year
        $leased = [];
        $currentDate = Carbon::now()->format('Y-m-d');

        $list1 = Comminvoice::groupby('commissionid')
        ->whereNotNull('expiration_date')
        ->where('comminvoice.status','=',1)
        ->where('comminvoice.lease_term','>=',3)
        ->where('expiration_date','=',$currentDate)
        ->leftjoin('commission','comminvoice.commissionid', '=', 'commission.id')
        ->leftjoin('properties','commission.propertyid', '=', 'properties.id')
        ->orderby('comminvoice.expiration_date','desc')
        ->get();

        foreach($list1 as $prop){
            $leased[$prop->commissionid] = $prop;
        }

        $newDateTime = Carbon::now()->subMonth(2)->format('Y-m-d');
        $list2 = Comminvoice::groupby('commissionid')
        ->whereNotNull('expiration_date')
        ->where('comminvoice.status','=',1)
        ->where('comminvoice.lease_term','<=',2)
        ->where('expiration_date','=',$newDateTime)
        ->leftjoin('commission','comminvoice.commissionid', '=', 'commission.id')
        ->leftjoin('properties','commission.propertyid', '=', 'properties.id')
        ->orderby('comminvoice.expiration_date','desc')
        ->get();

        foreach($list2 as $prop){
            $leased[$prop->commissionid] = $prop;
        }


      
        if (!empty($leased)) {
         

            $msg = "Hi,<br><br>";
            $msg .= "The following lease is about to expire. <br><br>";
            foreach($leased as $prop){
                $msg  .=  "-<a href='https://db.nest-property.com/commission/invoiceedit/".$prop->commissionid."' target='_blank'>https://db.nest-property.com/property/edit/".$prop->commissionid.'</a><br>';
            }
            $msg .= "<br>Please login to your account.<br><br><br>";

            $msg .= "Thank you";
    

       
            Mail::send('emails.generic', ['body' => $msg, 'title' => 'Invoicing Update'], function ($mail) use ($msg) {
            
                $mail->from('noreply@db.nest-property.com', 'Nest Database');
                $mail->to('constantine.leung@nest-property.com', 'Office Accounts');
                $mail->cc('william.budden@nest-property.com', 'Office Accounts');
                $mail->cc('naomi.budden@nest-property.com', 'Office Accounts')
                ->subject("Leased properties expiration")
                ->setBody($msg, 'text/html');
            });

            //$this->info( $msg);
            echo  $msg;
        }
       
   

    }
}
