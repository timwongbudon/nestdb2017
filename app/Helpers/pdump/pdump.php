<?php
include_once "php-dump.php";
function phpdump($var,$exit=false) {
  if ( class_exists('phpDump') ) {
    static $dump = null;
    if (!$dump) $dump = new phpDump;
    $dump->dump($var);
  } else {
    var_export($var);
  }
  if($exit)exit;
}
function exdump($var){phpdump($var,true);}
function dex($var){exdump($var);}
function dsql($var){die('<pre>'.$var.'</pre>');}
?>