<?php
/**
 * Nest Image
 */
class NestImage {
	public static function saveImage($image, $folder="test") {
		if (!empty($image)) {
	    	$uuid4 = \Ramsey\Uuid\Uuid::uuid4();
			$filename = $uuid4->toString() . '.jpg';
			$path = public_path("images/{$folder}/{$filename}");
			$image->save($path);
			return $filename;
		}else{
			return false;
		}
	}
	public static function fixOldUrl($url){
		$MY_HOST = env('MY_HOST', '');
		$PRODUCTION_SERVER = env('PRODUCTION_SERVER', '0');
		return $PRODUCTION_SERVER?str_replace('http://192.168.1.115:8082', 'http://' . $MY_HOST, $url):$url;
	}
}

Class NestNumber {
	public static function room_num($num) {
		/*$mod_count =  ($num / 0.5) % 2;
		if ($num=='0.0') return 0;
        if ($mod_count==0) {
            return number_format($num, 0);
        }else{
            return number_format($num, 1);
        }*/
		if (round($num) == $num){
			return number_format($num, 0);
		}else{
			return number_format($num, 1);
		}
	}
	public static function sale_price_printing($num) {
		if (round($num) == $num){
			return number_format($num, 0);
		}else if (round($num, 1) == $num){
			return number_format($num, 1);
		}else{
			return number_format($num, 2);
		}
	}
}

Class NestPreference {
	public static function isShowPhoto() {
		// return !preg_match("/^(admin|chris)/", Auth::user()->email);
		return true;
	}
}

Class NestDate {
	public static function now() {
		return Carbon\Carbon::now()->format('YmdHis');
	}
	public static function nest_date_format($date_string, $format="Y-m-d H:i:s") {
		if ($date_string == null || $date_string == '0000-00-00'){
			return '';
		}else{
			return Carbon\Carbon::createFromFormat($format, $date_string)->format('d/m/Y'); 
		}
	}
	public static function nest_datetime_format($date_string, $format="Y-m-d H:i:s") {
		if ($date_string == null || $date_string == '0000-00-00'){
			return '';
		}else{
			return Carbon\Carbon::createFromFormat($format, $date_string)->format('d/m/Y H:i'); 
		}
	}
	public static function nest_contract_datetime_format($date_string) {
		if ($date_string == null || $date_string == '0000-00-00'){
			return '';
		}else{
			return Carbon\Carbon::createFromFormat('Y-m-d', $date_string)->format('jS F Y'); 
		}
	}
	public static function nest_dayofmonth_datetime_format($date_string) {
		if ($date_string == null || $date_string == '0000-00-00'){
			return '';
		}else{
			return 'the '.Carbon\Carbon::createFromFormat('Y-m-d', $date_string)->format('jS').' day of '.Carbon\Carbon::createFromFormat('Y-m-d', $date_string)->format('F Y'); 
		}
	}
}

Class NestRedirect {
	public static function current_url() {
		return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}
	public static function previous_url() {
		return isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
	}
}

Class NestString {

	public static function sanitize_title($title) {
		return self::sanitize_title_with_dashes($title);
	}

	/**
	 * Sanitizes a title, replacing whitespace and a few other characters with dashes.
	 *
	 * Limits the output to alphanumeric characters, underscore (_) and dash (-).
	 * Whitespace becomes a dash.
	 *
	 * @since 1.2.0
	 *
	 * @param string $title     The title to be sanitized.
	 * @param string $raw_title Optional. Not used.
	 * @param string $context   Optional. The operation for which the string is sanitized.
	 * @return string The sanitized title.
	 */
	public static function sanitize_title_with_dashes( $title, $raw_title = '', $context = 'display' ) {
		$title = strip_tags($title);
		// Preserve escaped octets.
		$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
		// Remove percent signs that are not part of an octet.
		$title = str_replace('%', '', $title);
		// Restore octets.
		$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

		// if (seems_utf8($title)) {
		// 	if (function_exists('mb_strtolower')) {
		// 		$title = mb_strtolower($title, 'UTF-8');
		// 	}
		// 	$title = utf8_uri_encode($title, 200);
		// }

		$title = strtolower($title);

		if ( 'save' == $context ) {
			// Convert nbsp, ndash and mdash to hyphens
			$title = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $title );
			// Convert nbsp, ndash and mdash HTML entities to hyphens
			$title = str_replace( array( '&nbsp;', '&#160;', '&ndash;', '&#8211;', '&mdash;', '&#8212;' ), '-', $title );

			// Strip these characters entirely
			$title = str_replace( array(
				// iexcl and iquest
				'%c2%a1', '%c2%bf',
				// angle quotes
				'%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
				// curly quotes
				'%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
				'%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
				// copy, reg, deg, hellip and trade
				'%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
				// acute accents
				'%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
				// grave accent, macron, caron
				'%cc%80', '%cc%84', '%cc%8c',
			), '', $title );

			// Convert times to x
			$title = str_replace( '%c3%97', 'x', $title );
		}

		$title = preg_replace('/&.+?;/', '', $title); // kill entities
		$title = str_replace('.', '-', $title);

		$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
		$title = preg_replace('/\s+/', '-', $title);
		$title = preg_replace('|-+|', '-', $title);
		$title = trim($title, '-');

		return $title;
	}
}

class NestImportFileHelper {
	public static function nest_excel_get_fields($results) {
        if (empty($results)) return array();
        $res = array();
        $sample = $results[0];
        foreach ($sample as $key => $value) {
            $res[] = $key;
        }
        return $res;
    }

    public static function nest_excel_remove_empty_line(&$results, $field) {
        if (empty($results)) return array();
        foreach ($results as $key => $row) {
            if(empty($row->$field)) {
                unset($results[$key]);
            }
        }
    }	
}

//Client ... 15
//Manual ... 17
//Vendor ... 16
class NestEdmSubscription {
	public static function addEmail($email, $list){
		$url = 'https://nest-property.api-us1.com';

		$params = array(
			'api_key'      => 'fb772ca7053219a836bc800e2bf6b4737c3faa0584ce3755601032db3ab9301fcf833140',
			'api_action'   => 'contact_add',
			'api_output'   => 'serialize',
		);

		// here we define the data we are posting in order to perform an update
		$post = array(
			'email'                    => $email,
			'tags'                     => 'nestdb',
			'p['.$list.']'                   => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
			'status['.$list.']'              => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
			'instantresponders['.$list.']' => 1 // set to 0 to if you don't want to sent instant autoresponders
		);

		// This section takes the input fields and converts them to the proper format
		$query = "";
		foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
		$query = rtrim($query, '& ');

		// This section takes the input data and converts it to the proper format
		$data = "";
		foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
		$data = rtrim($data, '& ');

		// clean up the url
		$url = rtrim($url, '/ ');

		// This sample code uses the CURL library for php to establish a connection,
		// submit your request, and show (print out) the response.
		if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

		// If JSON is used, check if json_decode is present (PHP 5.2.0+)
		if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
			die('JSON not supported. (introduced in PHP 5.2.0)');
		}

		// define a final API request - GET
		$api = $url . '/admin/api.php?' . $query;

		$request = curl_init($api); // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
		//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
		curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

		$response = (string)curl_exec($request); // execute curl post and store results in $response
	}
}
