<?php

namespace App;

class BuildingMeta extends BaseMetaModel 
{
    protected $fillable = ['building_id', 'meta_key', 'meta_value'];
    public $timestamps  = false;
    public $key_id = 'building_id';

    public static $rules = array(
            'building_id' => 'required|numeric',
            'meta_key'    => 'required|max:255',
    	);
}
