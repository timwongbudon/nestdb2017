<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyDeleteLogs extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    public $table = 'property_delete_logs';
    protected $fillable = ['id', 'property_id','deleted_by', 'confirmed_by'];


    public function deletedby()
    {
        return $this->hasOne(User::class, 'id', 'deleted_by');
    }

    public function confirmedby()
    {
        return $this->hasOne(User::class, 'id', 'confirmed_by');
    }

    public function deleted_property()
    {
        return $this->hasOne(Property::class, 'id', 'property_id')->withTrashed();
    }
}
