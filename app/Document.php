<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

use App\LogDocuments;

class Document extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['id', 'doctype', 'docversion', 'shortlistid', 'consultantid','shared_with', 'clientid', 'propertyid', 'fieldcontents', 'sectionshidden', 'replace', 'revision'];
	
	public function after_save() {
		$this->logDocument();
	}
	
	public function logDocument(){
		$log = new LogDocuments();	
		
		$log->id = $this->id;
		$log->doctype = $this->doctype;
		$log->docversion = $this->docversion;
		$log->shortlistid = $this->shortlistid;
		$log->consultantid = $this->consultantid;
		$log->clientid = $this->clientid;
		$log->propertyid = $this->propertyid;
		$log->sectionshidden = $this->sectionshidden;
		$log->fieldcontents = $this->fieldcontents;
		$log->replace = $this->replace;
		$log->revision = $this->revision;
		$log->userid = auth()->user()->id;
		
		$log->save();
	}
	
}