<?php

namespace App;

class PropertyOutdoor extends BaseModel 
{
	protected $table = 'property_outdoors';
    protected $fillable = ['property_id', 'f1', 'f2', 'f3', 'f4', 'f5'];
    public $timestamps  = false;
    public $key_id = 'property_id';

    public static $rules = array(
            'property_id' => 'required|numeric',
    	);

    public function add_options($post_id, $options) {
    	if (empty($options)) {
    		return false;
    	}
    	$data = array($this->key_id => $post_id);
    	foreach($options as $option) {
			$data['f'.$option] = 1;
    	}
    	$post = self::where($this->key_id, '=', $post_id)->first();
    	if (empty($post)) {
    		return $this->create($data);
    	} else {
			$post->fill($data);
			return $post->save();
    	}
    }

    public function set_options($post_id, $options) {
    	if (empty($options)) {
    		return false;
    	}
    	$data = array($this->key_id => $post_id);
		for ($i = 1; $i <= 5; $i++){
			$data['f'.$i] = 0;
		}
    	foreach($options as $option) {
			$data['f'.$option] = 1;
    	}
    	$post = self::where($this->key_id, '=', $post_id)->first();
    	if (empty($post)) {
    		return $this->create($data);
    	} else {
			$post->fill($data);
			return $post->save();
    	}
    }
}
