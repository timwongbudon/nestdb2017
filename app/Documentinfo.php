<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;


class Documentinfo extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	
    protected $table = 'documentinfo';

    protected $fillable = ['id', 'client_id', 'shortlist_id', 'type_id', 'properties', 'remarks'];
	
}