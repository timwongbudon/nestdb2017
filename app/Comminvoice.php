<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Comminvoice extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'comminvoice';
	
    protected $fillable = ['commissionid', 'consultantid', 'invoicedate', 'invtype', 'whofor', 'buyername', 'buyeraddress', 'sellername', 'selleraddress', 'propertyaddress', 'purchasetext', 'terms', 'feecalculation', 'price', 'duedate','comments', 'status', 'addcosttext', 'addcost', 'work_title', 'work_place','lease_term','month_break_clause','expiration_date','start_date'];
		
    public function consultant() {
       return $this->belongsTo(User::class, 'consultantid'); 
    }
	


    public static function lease_term(){
		return array(
			1 => '3 Months',
			2 => '6 Months',
			3 => '1 Year',
			4 => '2 Years',
			5 => '3 Years',
			6 => '5 Years',			
		);
	}

    public static function month_break_clause(){
		return array(
			1 => '+1 Month',
			2 => '+2 Months',
			3 => '+3 Months',
			4 => '+4 Months',
			5 => '+5 Months',
			6 => '+6 Months',
            7 => '+7 Months',
            8 => '+8 Months',
            9 => '+9 Months',
            10 => '+10 Months',
            11 => '+11 Months',            
		);
	}
	

	
}