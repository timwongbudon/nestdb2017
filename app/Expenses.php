<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Expenses extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'expenses';
	
    protected $fillable = ['id', 'userid', 'year', 'month', 'paymode', 'chequenumber', 'amount', 'employeename', 'employeeaddress', 'payperiodfrom', 'payperiodto', 'expensifyid', 'paymentdate'];
	
    public function user() {
       return $this->belongsTo(User::class, 'userid'); 
    }
	
    public static function getMonthNames() {
		$months = array(
			1 => 'January',
			2 => 'February',
			3 => 'March',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December'
		);
		
		return $months;
	}
	
	
	
}