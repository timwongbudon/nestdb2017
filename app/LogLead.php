<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class LogLead extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'zlog_leads';
	
    protected $fillable = ['id', 'clientid', 'shortlistid', 'consultantid', 'status', 'lastoutreach', 'createdby', 'userid', 'dateinitialcontact', 'sourcehear', 'type', 'req_beds', 'req_baths', 'req_maidrooms', 'req_outdoor', 'req_carparks', 'req_facilites', 'budget', 'budgetto', 'likes', 'currentflat', 'reasonformove', 'noticeperiod', 'preferredareas', 'comments', 'websiteid', 'contactid'];
	public $key_id = 'logid';
	
}