<?php

namespace App\Policies;

use App\User;
use App\Property;
use Illuminate\Auth\Access\HandlesAuthorization;

class PropertyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can delete the given property.
     *
     * @param  User  $user
     * @param  Property $property
     * @return bool
     */
    public function destroy(User $user, Property $property)
    {
        return $user->id === $property->user_id;
    }
}
