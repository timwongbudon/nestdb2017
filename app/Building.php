<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Building extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'display_name', 'address1', 'address2', 'district_id', 'country_code', 'year_built', 'lat', 'lng', 'tel', 'deleted', 'facilities', 'comments', 'year_renov'];

    public static $rules = array(
            'name'         => 'required|max:255',
            'address1'     => 'required|max:255',
            'district_id'  => 'required|numeric',
            'country_code' => 'required',
        );

    public static $messages = array(
            'name.required' => 'The building name field is required.',
        );

    public function metas() {
        return $this->hasMany(BuildingMeta::class);
    } 

    protected $searchable = ['id', 'name', 'address1', 'district_id', 'country_code', 'year_built','latlong'];

    public function scopeApiKeywordSearch($query, $keywords) {
        /*$keys = explode(' ', $keywords);
		
		$query->where('deleted', '=', '0');
		
        foreach ($keys as $value) {
            if (preg_match("/\w+/", $value)) {
                $query->where('name', 'like', "%{$value}%");
				$query->orWhere('address1', 'like', "{$value}%");
            }
        }*/
		
		$query->where('deleted', '=', '0');
		$query->where('name', 'like', "%{$keywords}%");
		$query->orWhere('address1', 'like', "{$keywords}%");
				
        return $query;
    }

    public function scopeIndexSearch($query, $request)
    {
        $input = $request->all();
		
		$query->where('deleted', '=', '0');
		
        foreach ($input as $key => $value) {
            if(in_array($key, $this->searchable) && !empty($value)) {
                if(is_string($value))$value = trim($value);
                

                switch ($key) {
                    
                    case 'latlong':
                     
                        if($value == "true"){
                            $query->where('lat', '=', "");
                            $query->orwhere('lng', '=', "");
                        }
                        
                        break;
                    case 'address1':
                        $query->where(function($query) use ($value){
                            $query->orWhere('address1', 'like', "%{$value}%")
                                ->orWhere('address2', 'like', "%{$value}%");
                        });
                        break;
                  

                    case 'district_id':
                    case 'country_code':
                        $query->where($key, '=', $value);
                        break; 
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        return $query;
    }

	//This function used to automatically get the Lat/Long from Google
	//However, after Google introduced pricing for that, this didn't work anymore (even when we tried to pay for it)
	//So now it's deactivated and people are prompted to use a website to find the Lat/Long manually
    public function after_save() {
        $input = Input::all();
        if (isset($input['features'])) {
            $encoded_features = json_encode($input['features']);
            $meta = new BuildingMeta();
            $meta->add_post_meta($this->id, 'features', $encoded_features, true); 
        }
        /*if (empty($this->lat) || !is_numeric($this->lat)) {
		//if (empty($this->lat)) {
            $_address = $this->shorten_address();
            if($this->country_code == 'hk') {
                $_address1 = (!preg_match('/hong kong/i', $_address))?$_address.', Hong Kong':$_address; 
            }
            // dex($_address1);
            $map_info = self::get_coordinates($_address1);
            if (!empty($map_info)) {
                $this->lat = $map_info['lat'];
                $this->lng = $map_info['lng'];
                $this->save();
            } else {
                $this->lat = 'error';
                $this->lng = 'error';
                $this->save();
            }
        }*/
    }

    public static function sort_feature_ids($feature_ids) {
        if (empty($feature_ids)) return array();
        $matched_res = array();
        $no_matched_res = array();
        $feature_ids_order = self::feature_ids_order();
        foreach ($feature_ids_order as $key => $value) {
            if (in_array($key, $feature_ids)) {
                $matched_res[] = $key; 
                $no_matched_res[] = $key;
            }
        }
        if (!empty($no_matched_res)) {
            foreach ($no_matched_res as $key => $value) {
                $matched_res[] = $value;
            }
        }
        return $matched_res;
    }

    public static function feature_ids_order() {
        // put feature ids new order new here.
        $features = array( 
            "26" => "Garden",
            '1'  => 'Gym',
            '2'  => 'Swimming Pool',
            '3'  => 'Tennis Court',
            '4'  => 'Squash Court',
            '5'  => 'Car Park',
            '6'  => 'Shuttle Bus',
            '7'  => "Children's Playground",
            '8'  => "Pet Friendly",
            "9"  => "Balcony",
            "10" => "Rooftop",
            "11" => "Helper's Room",
            "12" => "Terrace",
            "13" => "Store Room",
            "14" => "Concierge Service",
            "15" => "Sauna",
            "16" => "Duplex",
            "17" => "Walk-In Closet",
            "18" => "Laundry Room",
            "19" => "Children's Play Room",
            "20" => "Play Room",
            "21" => "Brand New Renovation",
            "22" => "Lift",
            "23" => "Stand-Alone House",
            "24" => "Jacuzzi",
            "25" => "Internal Staircase",
            "27" => "Kitchen Pantry",
            "28" => "Barbeque Area",
            "29" => "Mahjong Rooms",
            "30" => "Family Room",
        );
        return $features;
    }

    public static function feature_ids() {
        $features = array(
            "26" => "Garden",
            '1'  => 'Gym',
            '2'  => 'Swimming Pool',
            '3'  => 'Tennis Court',
            '4'  => 'Squash Court',
            '5'  => 'Car Park',
            '6'  => 'Shuttle Bus',
            '7'  => "Children's Playground",
            '8'  => "Pet Friendly",
            "9"  => "Balcony",
            "10" => "Rooftop",
            "11" => "Helper's Room",
            "12" => "Terrace",
            "13" => "Store Room",
            "14" => "Concierge Service",
            "15" => "Sauna",
            "16" => "Duplex",
            "17" => "Walk-In Closet",
            "18" => "Laundry Room",
            "19" => "Children's Play Room",
            "20" => "Play Room",
            "21" => "Brand New Renovation",
            "22" => "Lift",
            "23" => "Stand-Alone House",
            "24" => "Jacuzzi",
            "25" => "Internal Staircase",
            "27" => "Kitchen Pantry",
            "28" => "Barbeque Area",
            "29" => "Mahjong rooms",
            "30" => "Family Room",
        );
        return $features;
    }
	
	public static function feature_ids_search(){
		$ret = array(
			'Features' => array(
				10 => "Rooftop",
				26 => "Garden",
				12 => "Terrace",
				9  => "Balcony",
				
				21 => "Brand New Renovation",
				11 => "Helper’s Room",
				5  => "Car Park",
				
				25  => "Internal Staircase",
				17  => "Walk-In Wardrobe",
				27  => "Kitchen Pantry",
				13  => "Store Room",
				30  => "Family Room",
				18  => "Utility Room",
			),
			'Facilities' => array(
				2  => "Swimming Pool",
				1  => "Gym",
				3  => "Tennis Court",
				4  => "Squash Court",
				7  => "Children’s Play Area / Playground",
				28 => "Barbeque Area",
				14 => "Concierge Service",
				6  => "Shuttle Bus",
				
				31  => "Car Parking Facilities",
				8  => "Pet Friendly",
				32  => "Gated Complex",
				24  => "Jacuzzi",
				15  => "Sauna",
				33  => "Steam Room",
				34  => "Golf Simulator",
				35  => "Cigar Room",
				22  => "Lift",
				29  => "Mahjong rooms",
				36  => "Walk-up",
			)
		);
		
		return $ret;
	}
	
	public function get_nice_country(){
		$ret = '-';
		
		$countries = $this->country_ids();
		if (isset($countries[trim($this->country_code)])){
			$ret = $countries[trim($this->country_code)];
		}
		
		return $ret;
	}

    public static function country_ids() {
        // ref: https://raw.githubusercontent.com/datasets/country-list/master/data.csv
        $countries = array(
            "af"=>"Afghanistan",
            "ax"=>"Åland Islands",
            "al"=>"Albania",
            "dz"=>"Algeria",
            "as"=>"American Samoa",
            "ad"=>"Andorra",
            "ao"=>"Angola",
            "ai"=>"Anguilla",
            "aq"=>"Antarctica",
            "ag"=>"Antigua and Barbuda",
            "ar"=>"Argentina",
            "am"=>"Armenia",
            "aw"=>"Aruba",
            "au"=>"Australia",
            "at"=>"Austria",
            "az"=>"Azerbaijan",
            "bs"=>"Bahamas",
            "bh"=>"Bahrain",
            "bd"=>"Bangladesh",
            "bb"=>"Barbados",
            "by"=>"Belarus",
            "be"=>"Belgium",
            "bz"=>"Belize",
            "bj"=>"Benin",
            "bm"=>"Bermuda",
            "bt"=>"Bhutan",
            "bo"=>"Bolivia, Plurinational State of",
            "bq"=>"Bonaire, Sint Eustatius and Saba",
            "ba"=>"Bosnia and Herzegovina",
            "bw"=>"Botswana",
            "bv"=>"Bouvet Island",
            "br"=>"Brazil",
            "io"=>"British Indian Ocean Territory",
            "bn"=>"Brunei Darussalam",
            "bg"=>"Bulgaria",
            "bf"=>"Burkina Faso",
            "bi"=>"Burundi",
            "kh"=>"Cambodia",
            "cm"=>"Cameroon",
            "ca"=>"Canada",
            "cv"=>"Cape Verde",
            "ky"=>"Cayman Islands",
            "cf"=>"Central African Republic",
            "td"=>"Chad",
            "cl"=>"Chile",
            "cn"=>"China",
            "cx"=>"Christmas Island",
            "cc"=>"Cocos (Keeling) Islands",
            "co"=>"Colombia",
            "km"=>"Comoros",
            "cg"=>"Congo",
            "cd"=>"Congo, the Democratic Republic of the",
            "ck"=>"Cook Islands",
            "cr"=>"Costa Rica",
            "ci"=>"Côte d'Ivoire",
            "hr"=>"Croatia",
            "cu"=>"Cuba",
            "cw"=>"Curaçao",
            "cy"=>"Cyprus",
            "cz"=>"Czech Republic",
            "dk"=>"Denmark",
            "dj"=>"Djibouti",
            "dm"=>"Dominica",
            "do"=>"Dominican Republic",
            "ec"=>"Ecuador",
            "eg"=>"Egypt",
            "sv"=>"El Salvador",
            "gq"=>"Equatorial Guinea",
            "er"=>"Eritrea",
            "ee"=>"Estonia",
            "et"=>"Ethiopia",
            "fk"=>"Falkland Islands (Malvinas)",
            "fo"=>"Faroe Islands",
            "fj"=>"Fiji",
            "fi"=>"Finland",
            "fr"=>"France",
            "gf"=>"French Guiana",
            "pf"=>"French Polynesia",
            "tf"=>"French Southern Territories",
            "ga"=>"Gabon",
            "gm"=>"Gambia",
            "ge"=>"Georgia",
            "de"=>"Germany",
            "gh"=>"Ghana",
            "gi"=>"Gibraltar",
            "gr"=>"Greece",
            "gl"=>"Greenland",
            "gd"=>"Grenada",
            "gp"=>"Guadeloupe",
            "gu"=>"Guam",
            "gt"=>"Guatemala",
            "gg"=>"Guernsey",
            "gn"=>"Guinea",
            "gw"=>"Guinea-Bissau",
            "gy"=>"Guyana",
            "ht"=>"Haiti",
            "hm"=>"Heard Island and McDonald Islands",
            "va"=>"Holy See (Vatican City State)",
            "hn"=>"Honduras",
            "hk"=>"Hong Kong",
            "hu"=>"Hungary",
            "is"=>"Iceland",
            "in"=>"India",
            "id"=>"Indonesia",
            "ir"=>"Iran, Islamic Republic of",
            "iq"=>"Iraq",
            "ie"=>"Ireland",
            "im"=>"Isle of Man",
            "il"=>"Israel",
            "it"=>"Italy",
            "jm"=>"Jamaica",
            "jp"=>"Japan",
            "je"=>"Jersey",
            "jo"=>"Jordan",
            "kz"=>"Kazakhstan",
            "ke"=>"Kenya",
            "ki"=>"Kiribati",
            "kp"=>"Korea, Democratic People's Republic of",
            "kr"=>"Korea, Republic of",
            "kw"=>"Kuwait",
            "kg"=>"Kyrgyzstan",
            "la"=>"Lao People's Democratic Republic",
            "lv"=>"Latvia",
            "lb"=>"Lebanon",
            "ls"=>"Lesotho",
            "lr"=>"Liberia",
            "ly"=>"Libya",
            "li"=>"Liechtenstein",
            "lt"=>"Lithuania",
            "lu"=>"Luxembourg",
            "mo"=>"Macao",
            "mk"=>"Macedonia, the Former Yugoslav Republic of",
            "mg"=>"Madagascar",
            "mw"=>"Malawi",
            "my"=>"Malaysia",
            "mv"=>"Maldives",
            "ml"=>"Mali",
            "mt"=>"Malta",
            "mh"=>"Marshall Islands",
            "mq"=>"Martinique",
            "mr"=>"Mauritania",
            "mu"=>"Mauritius",
            "yt"=>"Mayotte",
            "mx"=>"Mexico",
            "fm"=>"Micronesia, Federated States of",
            "md"=>"Moldova, Republic of",
            "mc"=>"Monaco",
            "mn"=>"Mongolia",
            "me"=>"Montenegro",
            "ms"=>"Montserrat",
            "ma"=>"Morocco",
            "mz"=>"Mozambique",
            "mm"=>"Myanmar",
            "na"=>"Namibia",
            "nr"=>"Nauru",
            "np"=>"Nepal",
            "nl"=>"Netherlands",
            "nc"=>"New Caledonia",
            "nz"=>"New Zealand",
            "ni"=>"Nicaragua",
            "ne"=>"Niger",
            "ng"=>"Nigeria",
            "nu"=>"Niue",
            "nf"=>"Norfolk Island",
            "mp"=>"Northern Mariana Islands",
            "no"=>"Norway",
            "om"=>"Oman",
            "pk"=>"Pakistan",
            "pw"=>"Palau",
            "ps"=>"Palestine, State of",
            "pa"=>"Panama",
            "pg"=>"Papua New Guinea",
            "py"=>"Paraguay",
            "pe"=>"Peru",
            "ph"=>"Philippines",
            "pn"=>"Pitcairn",
            "pl"=>"Poland",
            "pt"=>"Portugal",
            "pr"=>"Puerto Rico",
            "qa"=>"Qatar",
            "re"=>"Réunion",
            "ro"=>"Romania",
            "ru"=>"Russian Federation",
            "rw"=>"Rwanda",
            "bl"=>"Saint Barthélemy",
            "sh"=>"Saint Helena, Ascension and Tristan da Cunha",
            "kn"=>"Saint Kitts and Nevis",
            "lc"=>"Saint Lucia",
            "mf"=>"Saint Martin (French part)",
            "pm"=>"Saint Pierre and Miquelon",
            "vc"=>"Saint Vincent and the Grenadines",
            "ws"=>"Samoa",
            "sm"=>"San Marino",
            "st"=>"Sao Tome and Principe",
            "sa"=>"Saudi Arabia",
            "sn"=>"Senegal",
            "rs"=>"Serbia",
            "sc"=>"Seychelles",
            "sl"=>"Sierra Leone",
            "sg"=>"Singapore",
            "sx"=>"Sint Maarten (Dutch part)",
            "sk"=>"Slovakia",
            "si"=>"Slovenia",
            "sb"=>"Solomon Islands",
            "so"=>"Somalia",
            "za"=>"South Africa",
            "gs"=>"South Georgia and the South Sandwich Islands",
            "ss"=>"South Sudan",
            "es"=>"Spain",
            "lk"=>"Sri Lanka",
            "sd"=>"Sudan",
            "sr"=>"Suriname",
            "sj"=>"Svalbard and Jan Mayen",
            "sz"=>"Swaziland",
            "se"=>"Sweden",
            "ch"=>"Switzerland",
            "sy"=>"Syrian Arab Republic",
            "tw"=>"Taiwan, Province of China",
            "tj"=>"Tajikistan",
            "tz"=>"Tanzania, United Republic of",
            "th"=>"Thailand",
            "tl"=>"Timor-Leste",
            "tg"=>"Togo",
            "tk"=>"Tokelau",
            "to"=>"Tonga",
            "tt"=>"Trinidad and Tobago",
            "tn"=>"Tunisia",
            "tr"=>"Turkey",
            "tm"=>"Turkmenistan",
            "tc"=>"Turks and Caicos Islands",
            "tv"=>"Tuvalu",
            "ug"=>"Uganda",
            "ua"=>"Ukraine",
            "ae"=>"United Arab Emirates",
            "gb"=>"United Kingdom",
            "us"=>"United States",
            "um"=>"United States Minor Outlying Islands",
            "uy"=>"Uruguay",
            "uz"=>"Uzbekistan",
            "vu"=>"Vanuatu",
            "ve"=>"Venezuela, Bolivarian Republic of",
            "vn"=>"Viet Nam",
            "vg"=>"Virgin Islands, British",
            "vi"=>"Virgin Islands, U.S.",
            "wf"=>"Wallis and Futuna",
            "eh"=>"Western Sahara",
            "ye"=>"Yemen",
            "zm"=>"Zambia",
            "zw"=>"Zimbabwe",
        );
        return $countries;
    }

    public static function district_ids() {
		$ret = array(
			2 =>   "Aberdeen",
			501 => "Admiralty",
			69 =>  "Ap Lei Chau",
			50 =>  "Castle Peak Road",
			1 =>   "Causeway Bay",
			4 =>   "Central",
			5 =>   "Chai Wan",
			29 =>  "Cheung Sha Wan / Sham Shui Po",
			6 =>   "Chung Hom Kok",
			85 =>  "Clear Water Bay",
			7 =>   "Deep Water Bay",
			64 =>  "Discovery Bay",
			79 =>  "Gold Coast",
			8 =>   "Happy Valley",
			101 => "Heng Fa Chuen",
			31 =>  "Ho Man Tin / Waterloo Road",
			32 =>  "Hung Hom / To Kwa Wan",
			9 =>   "Jardine's Lookout",
			221 => "Kai Tak",			
			204 => "Kowloon City", 
			36 =>  "Kowloon Tong",
			53 =>  "Kwai Chung",
			502 => "Lamma Island",
			503 => "Lantau Island",
			201 => "Ma On Shan",
			504 => "Ma Wan",
			3 =>   "Mid-levels Central",
			10 =>  "Mid-levels East",
			11 =>  "Mid-levels West",
			39 =>  "Mong Kok",
			40 =>  "Ngau Chi Wan / Choi Hung",
			12 =>  "North Point",
			13 =>  "North Point Hill",
			14 =>  "Pokfulam",
			15 =>  "Quarry Bay",
			16 =>  "Repulse Bay",
			55 =>  "Sai Kung",
			100 => "Sai Ying Pun",
			202 => "Sham Tseng",
			78 =>  "Shatin",
			56 =>  "Shatin / Fo Tan",
			67 =>  "Shatin / Tai Wai",
			17 =>  "Shau Kei Wan",
			205 => "Shek O",
			57 =>  "Sheung Shui",
			20 =>  "Sheung Wan",
			19 =>  "Shouson Hill",
            207 =>  "South Bay",
			22 =>  "Stanley",
			23 =>  "Tai Hang",
			44 =>  "Tai Kok Tsui",
			58 =>  "Tai Po",
			24 =>  "Tai Tam",
			98 =>  "Taikoo Shing",
			25 =>  "The Peak",
			59 =>  "Tseung Kwan O / Hang Hau",
			45 =>  "Tsim Sha Tsui",
			203 => "Tuen Mun",
			66 =>  "Tung Chung",
			26 =>  "Wan Chai",
			27 =>  "Western / Kennedy Town",
			28 =>  "Wong Chuk Hang",
			47 =>  "Yau Ma Tei",
			49 =>  "Yau Yat Chuen",
			63 =>  "Yuen Long",
		);
		
        return $ret;
    }
	
    public static function district_ids_search_main(){
		$ret = array(
			'Hong Kong Island' => array(
				999 => "Hong Kong Island (All)",
				501 => "Admiralty",
				1 => "Causeway Bay",
				4 => "Central",
				8 => "Happy Valley",
				100 => "Sai Ying Pun",
				27 => "Western / Kennedy Town",
				10 => "Mid-levels East",
				3 => "Mid-levels Central",
				11 => "Mid-levels West",
				25 => "The Peak",
				20 => "Sheung Wan",
				23 => "Tai Hang",
				26 => "Wan Chai",
			),
			'Island South' => array(
				998 => "Island South (All)",
				6 => "Chung Hom Kok",
				14 => "Pokfulam",
				16 => "Repulse Bay",
				19 => "Shouson Hill",
				22 => "Stanley",
				207 => "South Bay",
				69 => "Ap Lei Chau",
				28 => "Wong Chuk Hang",
				24 => "Tai Tam",
				7 => "Deep Water Bay",
				2 => "Aberdeen"
			),
			'New Territories' => array(
				997 => "New Territories (All)",
				85 => "Clear Water Bay",
				502 => "Lamma Island",
				503 => "Lantau Island",
				504 => "Ma Wan",
				55 => "Sai Kung",
				79 =>  "Gold Coast",
                59=> 'Tseung Kwan O / Hang Hau'
			),
			'Kowloon' => array(
				31 => "Ho Man Tin / Waterloo Road",
				32 => "Hung Hom / To Kwa Wan",
				221 => "Kai Tak",
				204 => "Kowloon City",
				36 => "Kowloon Tong",
				39 => "Mong Kok",
// 				504 => "Sham Shui",
// 				55 => "Po Tai Kok",
// 				79 =>  "Tsui Tsim",
//              59 => "Sha Tsui",
//              59 => "Yau Ma Tei",
//              59 => "Yau Yat Chuen",
                47 => "Yau Ma Tei",
                49 => 'Yau Yat Chuen'
// 				39 => "Mong Kok",
// 				504 => "Sham Shui",
// 				55 => "Po Tai Kok",
// 				79 =>  "Tsui Tsim"
			)
        );
        
        $international = District::where('international', 1)->orderBy('name', 'asc')->get();
        $ret['International Countries'] = array_combine(
            $international->pluck('id')->toArray(),
            $international->pluck('name')->toArray()
        );
		
        return $ret;
    }
	
    public static function district_ids_search(){
		$ret = array(
			'Hong Kong Island' => array(
				501 => "Admiralty",
				1 => "Causeway Bay",
				4 => "Central",
				8 => "Happy Valley",
				100 => "Sai Ying Pun",
				27 => "Western / Kennedy Town",
				10 => "Mid-levels East",
				3 => "Mid-levels Central",
				11 => "Mid-levels West",
				25 => "The Peak",
				20 => "Sheung Wan",
				23 => "Tai Hang",
				26 => "Wan Chai",
			),
			'Island South' => array(
				6 => "Chung Hom Kok",
				14 => "Pokfulam",
				16 => "Repulse Bay",
				19 => "Shouson Hill",
				22 => "Stanley",
				207 => "South Bay",
				69 => "Ap Lei Chau",
				28 => "Wong Chuk Hang",
				24 => "Tai Tam",
				7 => "Deep Water Bay",
				2 => "Aberdeen"
			),
			'New Territories' => array(
				85 => "Clear Water Bay",
				502 => "Lamma Island",
				503 => "Lantau Island",
				504 => "Ma Wan",
				55 => "Sai Kung",
				79 =>  "Gold Coast",
            ),
            'Kowloon' => array(
				31 => "Ho Man Tin / Waterloo Road",
				32 => "Hung Hom / To Kwa Wan",
				221 => "Kai Tak",
				204 => "Kowloon City",
				36 => "Kowloon Tong",
				39 => "Mong Kok",
// 				504 => "Sham Shui",
// 				55 => "Po Tai Kok",
// 				79 =>  "Tsui Tsim",
//              59 => "Sha Tsui",
//              59 => "Yau Ma Tei",
//              59 => "Yau Yat Chuen",
                47 => "Yau Ma Tei",
                49 => 'Yau Yat Chuen'
// 				39 => "Mong Kok",
// 				504 => "Sham Shui",
// 				55 => "Po Tai Kok",
// 				79 =>  "Tsui Tsim"
			)
		);
        
        $international = District::where('international', 1)->orderBy('name', 'asc')->get();
        $ret['International Countries'] = array_combine(
            $international->pluck('id')->toArray(),
            $international->pluck('name')->toArray()
        );
        
        return $ret;
    }

    /**
     * Helpers
     */
    public function shorten_name() {
        list($name) = explode('(', $this->name);
        $name = str_replace(',', '', $name);
        return $name;
    }

    public function shorten_print_name() {
        list($name) = explode('(', $this->name);
		if (strpos($name, '-') > 10){
			list($name) = explode('-', $name);
		}
        $name = str_replace(',', '', $name);
        return $name;
    }

    public function shorten_address() {
        list($name) = explode('(', $this->address1);
        return $name;
    }

    public function district() {
        $district_ids = self::district_ids();
        return !empty($this->district_id)?$district_ids[$this->district_id]:'';
    }

    public function is_selected_feature($feature_id=null) {
        $meta = new BuildingMeta();
        $_feature_ids = json_decode($meta->get_post_meta($this->id, 'features', true));
        return !empty($_feature_ids)?in_array($feature_id, $_feature_ids):false;
    }

    public function fix_address1() {
        if (!empty($this->address1)) {
            if (preg_match("/^(\d[^,]*),(.+)/", $this->address1, $m)) {
                $this->address1 = $m[1] . $m[2];
            }
        }
    }

    public static function get_coordinates($address) {
        // ref: https://colinyeoh.wordpress.com/2013/02/12/simple-php-function-to-get-coordinates-from-address-through-google-services/
        $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern 
        //$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAGcm0uCNBLiUfoHnedu5UZFhcHuYW6nK0';
        $response = file_get_contents($url);
        $json = json_decode($response,TRUE); //generate array object from the response from the web
        if (!isset($json['results'][0]['geometry']['location']['lat'])) {
            if (isset($json['status'])) {
                return array('address' => $address,
						'error' => $json['error_message'],
                        'lat' => $json['status'],
                        'lng' => $json['status']
                    );
            } else {
                return array();
            }
        }
        return array(
                'address' => $address,
                'lat'     => $json['results'][0]['geometry']['location']['lat'],
                'lng'     => $json['results'][0]['geometry']['location']['lng']
            ); 
    } 
}
