<?php

namespace App\Repositories;

use App\User;

class ClientRepository
{
    /**
     * Get all of the properties for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return $user->clients()
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}