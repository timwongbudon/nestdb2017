<?php

namespace App\Repositories;

use App\User;

class BuildingRepository
{
    /**
     * Get all of the properties for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return $user->buildings()
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}