<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdfTutorial extends Model
{
    protected $fillable = ['display_name', 'filename', 'path'];
}
