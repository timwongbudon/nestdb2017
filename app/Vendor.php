<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Vendor extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['parent_id', 'company', 'type_id', 
                        'firstname', 'lastname', 'tel', 'email',
                        'firstname2', 'lastname2', 'tel2', 'email2',
                        'address1', 'address2', 'district_id', 'country_code', 
    					'bri', 'hkid', 'agency_fee', 'comments', 'v_title', 'v_title2', 'edm', 'corporatelandlord'];

    public static $rules = array(
            'type_id' => 'required|numeric',
            'company' => 'required|max:255',
        );

    public static $_rules = array(
            '1' => array(
                'type_id'   => 'required|numeric',
                'company'   => 'required_without_all:firstname,lastname|max:255',
                'firstname' => 'required_without_all:company,lastname|max:255',
                'lastname' => 'required_without_all:company,firstname|max:255',
                'email'     => 'email',
            ),
            '2' => array(
                'type_id'   => 'required|numeric',
                'firstname' => 'required_without:lastname|max:255',
                'lastname' => 'required_without:firstname|max:255',
                'parent_id' => 'required|numeric',
                'email'     => 'email',
            ),
            '3' => array(
                'type_id' => 'required|numeric',
                'company' => 'required|max:255',
                'email'   => 'email',
            ),
            '4' => array(
                'type_id'   => 'required|numeric',
                'firstname' => 'required_without:lastname|max:255',
                'lastname' => 'required_without:firstname|max:255',
                'parent_id' => 'required|numeric',
                'email'     => 'email',
            ),
        );

    public static $messages = array(
            'type_id.required' => 'The type field is required.',
            'company.required' => 'The company name field is required.',
        );

    public static $_messages = array(
            '1' => array(
                'type_id.required' => 'The type field is required.',
                'company.required_without' => 'The company name field is required without first name.',
                'firstname.required_without' => 'The first name field is required without company name.',
            ),
            '2' => array(
                'type_id.required'   => 'The type field is required.',
                'firstname.required' => 'The first name field is required.',
                'parent_id.required' => 'The owner name field is required.',
            ),
            '3' => array(
                'type_id.required' => 'The type field is required.',
                'company.required' => 'The agency name field is required.',
            ),
            '4' => array(
                'type_id.required'   => 'The type field is required.',
                'firstname.required' => 'The first name field is required.',
                'parent_id.required' => 'The agency name field is required.',
            ),
        );
	
	//Note that a Rep always belongs to an Owner and an Agent always belongs to an Agency
    public $type_ids = array(
            '1' => 'Owner',
            '2' => 'Rep',
            '3' => 'Agency',
            '4' => 'Agent',
        );

    protected $searchable = ['id', 'company', 'type_id', 
                        'firstname', 'lastname', 'tel', 'email',
                        'address1'];

    public function parent() {
        return $this->belongsTo(Vendor::class, 'parent_id');
    }

    public function children() {
        return $this->hasMany(Vendor::class, 'parent_id', 'id');
    }

    public function scopeIndexSearch($query, $request)
    {
        $input = $request->all();
		
        foreach ($input as $key => $value) {
            if((in_array($key, $this->searchable) || $key == 'sstring') && !empty($value)) {
                if(is_string($value))
					$value = trim($value);
                switch ($key) {
                    case 'firstname':
                    case 'lastname':
                    case 'tel':
                    case 'email':
                        $query->where(function($query) use ($key, $value){
                            $query->orWhere($key, 'like', "%{$value}%")
                                ->orWhere($key.'2', 'like', "%{$value}%");
                        });
                        break;
                    case 'sstring':
						$s = filter_var($value, FILTER_SANITIZE_STRING, FILTER_UNSAFE_RAW);
						if (trim($s) != ''){
							$query->whereRaw('(firstname like "%'.$s.'%" or lastname like "%'.$s.'%" or firstname2 like "%'.$s.'%" or lastname2 like "%'.$s.'%")');
						}
                        break;
                    case 'company':
						$s = filter_var($value, FILTER_SANITIZE_STRING, FILTER_UNSAFE_RAW);
						if (trim($s) != ''){
							$query->whereRaw('(company like "%'.$s.'%" or email like "%'.$s.'%" or email2 like "%'.$s.'%" or address1 like "%'.$s.'%")');
						}
                        break;
                    case 'address1':
                        $query->where(function($query) use ($value){
                            $query->orWhere('address1', 'like', "%{$value}%")
                                ->orWhere('address2', 'like', "%{$value}%");
                        });
                        break;
                    case 'type_id':
                        $query->where($key, '=', $value);
                        break; 
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        return $query;
    }

    /**
     * Helpers
     */
    public function firstname() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function lastname() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function email() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function tel() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }
	
    public function telLinked() {
		if (empty($this->tel) || trim($this->tel == '')){
			return '-';
		}
        return '<a href="tel:'.trim($this->tel).'" class="nest-clickable-phone-number">'.$this->tel.'</a>';
    }

    public function company() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }
    
    public function firstname2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function lastname2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function email2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function tel2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }
	
    public function tel2Linked() {
		if (empty($this->tel2) || trim($this->tel2 == '')){
			return '-';
		}
        return '<a href="tel:'.trim($this->tel2).'" class="nest-clickable-phone-number">'.$this->tel2.'</a>';
    }

    public function comments() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function address1() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function address2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function bri() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function hkid() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function agency_fee() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function district() {
        $district_ids = Building::district_ids();
        return !empty($this->district_id)?$district_ids[$this->district_id]:'';
    }

    public function type() {
        return $this->type_ids[$this->type_id];
    }

    public function poc() {
        return $this->poc_ids[$this->poc_id];
    }

    public function name() {
		if ($this->v_title > 0){
			$res[] = $this->getVTitle();
		}
        if(!empty($this->firstname))$res[] = $this->firstname;
        if(!empty($this->lastname))$res[] = $this->lastname;
        return !empty($res)?join(' ', $res):'-';
    }

    public function name2() {
		if ($this->v_title2 > 0){
			$res[] = $this->getVTitle2();
		}
        if(!empty($this->firstname2))$res[] = $this->firstname2;
        if(!empty($this->lastname2))$res[] = $this->lastname2;
        return !empty($res)?join(' ', $res):'-';
    }
	
    public function name_company() {
		$res = array();
        if (trim($this->company) != '') $res[] = $this->company;
        if((!empty($this->firstname) && trim($this->firstname) != '') || (!empty($this->lastname) && trim($this->lastname) != '')){
			$add = $this->name();
			if((!empty($this->firstname2) && trim($this->firstname2) != '') || (!empty($this->lastname2) && trim($this->lastname2) != '')){
				$add .= ' / '.$this->name2();
			}
			$res[] = $add;
		}
		
        return !empty($res)?join(', ', $res):'';
    }

    public function basic_info() {
		$res = array();
        if (trim($this->company) != '') $res[] = $this->company;
        if((!empty($this->firstname) && trim($this->firstname) != '') || (!empty($this->lastname) && trim($this->lastname) != ''))$res[] = $this->name();
        if(!empty($this->tel) && trim($this->tel) != '')$res[] = 'Tel: '.$this->tel;
        if(!empty($this->email) && trim($this->email) != '')$res[] = $this->email;
        //if(!empty($this->comments) && trim($this->comments) != '')$res[] = "Vendor Comments: " . $this->comments;
		
		$commentout = '';
		$short = substr(trim($this->comments), 0, 2);
		if ($short == '[]' || $short == '[{'){
			$comments = json_decode($this->comments, true);
			if (count($comments) > 0){
				$commentout = $comments[count($comments)-1]['content'];
			}
		}else{
			$commentout = $this->comments;
		}
		if ($commentout != ''){
			$res[] = "Last Comment: ".$commentout;
		}
		
		
        return !empty($res)?join(', ', $res):'';
    }

    public function basic_info_property_search() {
		$res = array();
		if ($this->type_id == 1 || $this->type_id == 3){
			if (trim($this->company) != '') $res[] = trim($this->company);
		}
        if((!empty($this->firstname) && trim($this->firstname) != '') || (!empty($this->lastname) && trim($this->lastname) != ''))$res[] = trim($this->name());
        if(!empty($this->tel) && trim($this->tel) != '')$res[] = ''.$this->tel;
		
        return !empty($res)?join(' &nbsp;', $res):'';
    }

    public function basic_info_property_search_linked() {
		$ret = '';
		
		if ($this->type_id == 1 || $this->type_id == 3){
			$ret = '<a href="'.url('/vendor/show/'.$this->id).'" target="_blank">'.trim($this->company).'</a>';
		}
        if((!empty($this->firstname) && trim($this->firstname) != '') || (!empty($this->lastname) && trim($this->lastname) != '')){
			if ($ret != ''){
				$ret .= ' &nbsp;';
			}
			$ret .= '<a href="'.url('/vendor/show/'.$this->id).'" target="_blank">';
				$ret .= trim($this->name());
				if((!empty($this->firstname2) && trim($this->firstname2) != '') || (!empty($this->lastname2) && trim($this->lastname2) != '')){
					$ret .= ' / '.trim($this->name2());
				}
			$ret .= '</a>';
		}
        if(!empty($this->tel) && trim($this->tel) != ''){
			if ($ret != ''){
				$ret .= ' &nbsp;';
			}
			$ret .= '<a href="tel:'.trim($this->tel).'" class="nest-clickable-phone-number">'.$this->tel.'</a>';
		}
		
        return $ret;
    }
	
    public function linked_info() {
		$ret = '';
		
		if ((!empty($this->firstname) && trim($this->firstname) != '') || (!empty($this->lastname) && trim($this->lastname) != '')){
			$ret .= '<a href="'.url('/vendor/show/'.$this->id).'" target="_blank">';
				$ret .= $this->name();
				if((!empty($this->firstname2) && trim($this->firstname2) != '') || (!empty($this->lastname2) && trim($this->lastname2) != '')){
					$ret .= ' / '.trim($this->name2());
				}
			$ret .= '</a>';
		}
		if (!empty($this->tel) && trim($this->tel) != ''){
			$ret .= ' ('.$this->tel.')';
		}else if (!empty($this->email) && trim($this->email) != ''){
			$ret .= ' ('.$this->email.')';
		}
		
        return $ret;
    }
	
	public function get_nice_country(){
		$ret = '-';
		
		$countries = Building::country_ids();
		if (isset($countries[trim($this->country_code)])){
			$ret = $countries[trim($this->country_code)];
		}
		
		return $ret;
	}

    public function is_selected_type($type_id=null) {
        return $type_id == $this->type_id;
    }

    public function fix_vendor_name() {
        if (!empty($this->tel)) {
            $this->company = $this->company . ' (' . $this->tel . ')';
        }
    } 
	  

    /**
     *  After Save
     */
	//The things below are run to make sure that when the type of a vendor changes, that it's still that Reps belong to Owners and Agents belong to Agencies
	//If that is not the case anymore, the Rep needs to be changed to be an Agent and vice versa
    public function after_save() {
        //$this->_fix_properties_belongto_new_type();
        $this->_fix_child_belongto_new_type();
    }

    private function _correct_child_type_id() {
        $res = null;
        if ($this->type_id == 1) { //owner
            $res = 2;
        } else if($this->type_id == 3) { //agency
            $res = 4;
        }
        return $res;
    }

    private function _fix_child_belongto_new_type() {
        if (!$this->_correct_child_type_id()) return;
        $vendors = Vendor::where('parent_id', $this->id)->where('type_id', '!=', $this->_correct_child_type_id())->get();
        foreach ($vendors as $key => $vendor) {
            $vendor->type_id = $this->_correct_child_type_id();
            $vendor->save();
        }
    }

    private function _get_incorrect_type_field() {
        $res = null;
        if ($this->type_id == 1) { //owner
            $res = 'agent_id';
        } else if($this->type_id == 3) { //agency
            $res = 'owner_id';
        }
        return $res;
    }

    private function _fix_properties_belongto_new_type() {
        if (!$this->_get_incorrect_type_field()) return;
        $properties = Property::where($this->_get_incorrect_type_field(), $this->id)->get();
        foreach ($properties as $key => $property) {
            if ($this->type_id == 1) {
                $property->owner_id = $property->agent_id;
                $property->agent_id = 0;
                if(empty($property->rep_ids) && !empty($property->agent_ids)) {
                    $property->rep_ids = $property->agent_ids;
                    $property->agent_ids = '';
                }
				$property->tst = time();
                $property->save();
            } elseif($this->type_id == 3) {
                $property->agent_id = $property->owner_id;
                $property->owner_id = 0;
                if(empty($property->agent_ids) && !empty($property->rep_ids)) {
                    $property->agent_ids = $property->rep_ids;
                    $property->rep_ids = '';
                }
				$property->tst = time();
                $property->save();
            }
        }
    }
	
	public function getVTitle(){
		/*if ($this->v_title == 1){
			return 'Ms.';
		}else if ($this->v_title == 2){
			return 'Mrs.';
		}else if ($this->v_title == 3){
			return 'Mr.';
		}*/
		return '';
	}
	
	public function getVTitle2(){
		/*if ($this->v_title2 == 1){
			return 'Ms.';
		}else if ($this->v_title2 == 2){
			return 'Mrs.';
		}else if ($this->v_title2 == 3){
			return 'Mr.';
		}*/
		return '';
	}

}
