<?php

namespace App;

class PropertyFeature extends BaseModel 
{
	protected $table = 'property_features';
    protected $fillable = ['property_id', 
        'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10',
        'f11', 'f12', 'f13', 'f14', 'f15', 'f16', 'f17', 'f18', 'f19', 'f110',
        'f21', 'f22', 'f23', 'f24', 'f25', 'f26', 'f27', 'f28', 'f29', 'f30', 'f31', 'f32', 'f33', 'f34', 'f35'];
    public $timestamps  = false;
    public $key_id = 'property_id';

    public static $rules = array(
            'property_id' => 'required|numeric',
    	);

    public function add_options($post_id, $options) {
    	if (empty($options)) {
    		return false;
    	}
    	$data = array($this->key_id => $post_id);
    	foreach($options as $option) {
			$data['f'.$option] = 1;
    	}
    	$post = self::where($this->key_id, '=', $post_id)->first();
    	if (empty($post)) {
    		return $this->create($data);
    	} else {
			$post->fill($data);
			return $post->save();
    	}
    }
	
    public function set_options($post_id, $options) {
    	if (empty($options)) {
    		return false;
    	}
    	$data = array($this->key_id => $post_id);
		for ($i = 1; $i <= 35; $i++){
			$data['f'.$i] = 0;
		}
    	foreach($options as $option) {
			$data['f'.$option] = 1;
    	}
    	$post = self::where($this->key_id, '=', $post_id)->first();
    	if (empty($post)) {
    		return $this->create($data);
    	}else{
			$post->fill($data);
			return $post->save();
    	}
    }
}
