<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogProperties extends BaseMediaModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'zlog_properties';
    
    protected $fillable = ['id', 'name', 'display_name', 'building_id', 'unit', 'type_id', 'address1', 
    					'address2', 'district_id', 'country_code', 'floor_zone', 'layout',
    					'description', 'bedroom', 'bathroom', 'maidroom', 'year_built',
    					'gross_area', 'saleable_area', 'asking_rent', 'asking_sale', 'inclusive',
    					'management_fee', 'government_rate',
                        'poc_id', 'owner_id', 'agent_id', 'rep_ids', 'agent_ids', 'web', 
                        'car_park', 'outdoor', 'hkh_url', 'leased', 'featured', 'slug',
                        'key_loc_id', 'key_loc_other', 'door_code', 'agency_fee', 'comments', 'hot',
                        'contact_info', 'available_date', 'external_id', 'source', 'user_id', 'created_at', 
						'sold', 'updated_at', 'feat_order', 'tst', 'pictst', 'userid', 'floornr', 
						'bonuscomm', 'commission', 'otherfeatures', 'otherfacilities', 'webtext', 'nosaleprice'];
    public $key_id = 'logid';
	
	
	
}
