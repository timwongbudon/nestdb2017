<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Input;
use DB;

class BaseModel extends Model {
	public $key_id = 'OBJID';

	public function save(array $options = array()) {
        parent::save($options);
        $this->after_save();
    }

    public function after_save() {
    }
}
