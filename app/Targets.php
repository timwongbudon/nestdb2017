<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Targets extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'targets';
	
    protected $fillable = ['id', 'year', 'consultantid', 'currentlease', 'monthlylease', 'currentleasedeals', 'monthlyleasedeals', 'currentsale', 'currentsaledeals'];
		
    public function consultant() {
       return $this->belongsTo(User::class, 'consultantid'); 
    }
	
	public static function getmonths(){
		return array(
			0 => 'January',
			1 => 'February',
			2 => 'March',
			3 => 'April',
			4 => 'May',
			5 => 'June',
			6 => 'July',
			7 => 'August',
			8 => 'September',
			9 => 'October',
			10 => 'November',
			11 => 'December'
		);
	}
	
	
	
	
}