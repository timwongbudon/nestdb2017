<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XeroLogs extends Model
{
    public $timestamps = true;
    protected $fillable = ['id','user_id', 'message'];
}
