<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Image;

class PropertyImageVariation extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['property_media_id', 'url', 'dimension', 'format'];

    public static $rules = array(
            'url' => 'required|max:255',
        );

    public static $formats = array(
            'large'              => array(2180, 1453),
            'medium'              => array(1090, 726),
            'small'              => array(420, 280),
            'social'              => array(1200, 630),
            'thumbnail'          => array(300, 300),
            'system-thumbnail'   => array(357, 237),
            'system-print_extralarge' => array(1020, 680),
			'system-print_xxxextralarge' => array(2040, 1360),
            'system-print_large' => array(510, 340),
            'system-print_small' => array(250, 166),
    	);

    public static $website_formats = array(
            'large',
			'medium',
			'small',
			'social',
            'thumbnail',
        );

    public static function url_encode($string){
        // $path_parts = pathinfo($string);
        // $string = $path_parts['dirname'] . '/' .urlencode($path_parts['basename']);
        return str_replace(' ', '%20', $string);
    }

    public static function read_image($property_media_id, $format, $overwrite=0) {
        $image_variation = self::where('property_media_id', $property_media_id)->where('format', $format)->first();
		
		//$image_variation = false;
		
		/*if ($format == 'system-print_large'){
			$overwrite = true;
		}*/
		
		if (!$image_variation || ($image_variation && $overwrite)) {
			$image = PropertyMedia::where('id', $property_media_id)->first();
			if (empty($image)) {
				return self::dummy_image();
			}
			$property = Property::where('id', $image->property_id)->first();
			if (empty($property)) {
				return self::dummy_image();
			}
			$url = $image->get_local_first_url();
			/*if (strstr($url, 'nest-property.com/propertybase/') && strstr($url, '-300x225.jpg')){
				$urlnew = str_replace('-300x225.jpg', '.jpg', $url);
				$_img = Image::make(\NestImage::fixOldUrl(self::url_encode($urlnew)));
				if (!$_img){
					$_img = Image::make(\NestImage::fixOldUrl(self::url_encode($url)));
				}
			}else{
				$_img = Image::make(\NestImage::fixOldUrl(self::url_encode($url)));
			}*/
			
			$_img = Image::make(\NestImage::fixOldUrl(self::url_encode($url)));
			$img_ext = pathinfo($image->get_local_first_url(), PATHINFO_EXTENSION);
			switch ($format) {
				case 'large':
					$_img = self::resize_image($_img, self::$formats[$format][0], self::$formats[$format][1], 0);
					$_img = self::watermark_image($_img);
					break;
				case 'medium':
					$_img = self::resize_image($_img, self::$formats[$format][0], self::$formats[$format][1], 0);
					$_img = self::watermark_image($_img);
					break;
				case 'small':
					$_img = self::resize_image($_img, self::$formats[$format][0], self::$formats[$format][1], 0);
					$_img = self::watermark_image($_img);
					break;
				case 'social':
					$_img = self::resize_image($_img, self::$formats[$format][0], self::$formats[$format][1], 0);
					$_img = self::watermark_image($_img);
					break;
				case 'system-thumbnail':
				case 'system-print_xxxextralarge':
				case 'system-print_extralarge':
				case 'system-print_large':
				case 'system-print_small':
					$_img = self::resize_image($_img, self::$formats[$format][0], self::$formats[$format][1], 0);
					break;
				case 'thumbnail':
					$_img = self::thumbnail_image($_img);
					break;
				default:
					return self::dummy_image(); 
					break;
			}
			if ($overwrite) {
				$image_variation->forceDelete();
			}
			
			$_dimension = !empty($_img)?$_img->width() . 'x' . $_img->height():'';
			$storage_path = public_path();
			if (in_array($format, self::$website_formats)) {
				$file_path = 'images' .'/'. 'website' .'/';
			} else {
				$file_path = 'images' .'/'. 'system' .'/';
			}
			$file_name = sprintf("%d-%d-%s-%s.%s", $property->id, $property_media_id, $property->property_slug(), $_dimension, $img_ext);
			$type = $format;

			$_img->save($file_path . $file_name);

			$PropertyImageVariation = new PropertyImageVariation;
			$image_variation = $PropertyImageVariation->create(array(
						'property_media_id' => $property_media_id,
						'url'               => url($file_path . $file_name),
						'dimension'         => $_dimension,
						'format'            => $type,
					));
			return $_img;
		} else {
			// dex($image_variation->toArray());
			// dex($image_variation->url);
			try {			
				return Image::make($image_variation->url);
			} catch (\Exception $e) {
				//not throwing  error when exception occurs
			}

			return null;
		}
    }

    public static function read_download_image($property_id, $type, $download_url, $format='system-thumbnail') {
        if (!empty($type) && !preg_match("/db.nest-property.com.+featured/", \NestImage::fixOldUrl($download_url))) { // if not log
            $img_ext = pathinfo($download_url, PATHINFO_EXTENSION);
			$urlback = '/cms/placeholder-300x225.png';
			
			if (strstr($download_url, 'Photo.php')){
				return $download_url;
			}else if (strtolower($img_ext) == 'jpg' || strtolower($img_ext) == 'jpeg' || strtolower($img_ext) == 'png'){
				$file_path = 'images' .'/'. 'system' .'/';
				
				try {
					$_img = Image::make(\NestImage::fixOldUrl(self::url_encode($download_url)));
					$_img = self::resize_image($_img, self::$formats[$format][0], self::$formats[$format][1], 0);
					$file_name = sprintf("%d-featured.%s", $property_id, $img_ext);
					$_img->save($file_path . $file_name);

					$photo = PropertyPhoto::where('property_id', $property_id)->first();
					$photo->$type = url($file_path . $file_name);
					$photo->save();
					
					$urlback = url($file_path . $file_name);
				}
				catch (\Exception $e) {
					
				}
			} 
			
            return $urlback;
        }
		if (strstr($download_url, '-featured')){
			$download_url .= '?t='.time();
		}
    	return $download_url;
    }

    public static function dummy_image() {
    	$watermark_image = 'nestproperty-logo-white.png';
    	$img = Image::make(public_path().'/'.$watermark_image);
    	return self::resize_image($img, 800, 400, 1);
    }

    public static function resize_image($img, $width, $height, $aspect_ratio=1) {
    	if($aspect_ratio) {
    		$img->resize($width, null, function ($constraint) {$constraint->aspectRatio();});
    	} else {
    		$img->fit($width, $height);
    	}
    	return $img;
    }

    public static function thumbnail_image($img) {
    	$img->fit(self::$formats['thumbnail'][0], self::$formats['thumbnail'][1]);
    	return $img;
    }

    public static function watermark_image($img, $ratio=0.5) {
    	// $watermark_image = 'Nest-Only-White-30.png';
        // $watermark_image = 'Nest-Only-White-40.png';
        // $watermark_image = 'Nest-Only-White-50.png';
        // $watermark_image = 'nestproperty-logo-white.png';
        $watermark_image = 'Nest-Watermark.png';
    	$watermark_width  = $img->width() * $ratio;
    	$watermark = Image::make(public_path().'/'.$watermark_image)->resize($watermark_width, null, function ($constraint) {$constraint->aspectRatio();});
    	$img->insert($watermark, 'center');
    	return $img;
    }

    public static function watermark_image_by_id($property_media_id, $ratio=0.5) {
    	$image = PropertyMedia::where('id', $property_media_id)->first();
    	$url = $image->get_local_first_url();
    	$img = Image::make($url);
		return self::watermark_image($img);
    }
	
	public static function download_from_url($property_id, $property_media_id, $overwrite=0) {
		$type = 'download';
        $property = Property::where('id', $property_id)->first();
        $image = PropertyMedia::where('id', $property_media_id)->first();
        $status = 'completed';

        if (!empty($image) && !empty($property)) {
        	$count = self::where('property_media_id', $image->id)->count();
        	if ($count!=0 && !$overwrite) {
        		return "download abort - duplicate";
        	} elseif ($count!=0 && $overwrite) {
        		$image_variation = self::where('property_media_id', $image->id)->first();
        		$image_variation->forceDelete();
        		$status = 'overwrite';
        	}
            $storage_path = public_path();
            $directory = $type;
            $file_path = 'images' .'/'. $directory .'/';
            $url = \NestImage::fixOldUrl($image->url);
            if (!self::is_external_server($url)) {
            	return "download fail - file on local server";
            }
            $img_ext = pathinfo($url, PATHINFO_EXTENSION);
            $file_name = sprintf('%d-%d-%s.%s', $image->property_id, $image->id, $property->property_slug(), $img_ext);
            $img = $file_path . $file_name;
            $bytes = file_put_contents($img, file_get_contents($url));

            $_img = Image::make($img);
            $_dimension = !empty($_img)?$_img->width() . 'x' . $_img->height():'';
            if (!empty($bytes)) {
                $PropertyImageVariation = new PropertyImageVariation;
                $image_variation = $PropertyImageVariation->create(array(
                        'property_media_id' => $image->id,
                        'url'               => url($file_path . $file_name),
                        'dimension'         => $_dimension,
                        'format'            => $type,
                    ));
            }
            return "download {$status} - " . (microtime(true) - LARAVEL_START) . ' sec';
        }
        return 'downlaod fail';
	} 

	public static function is_external_server($url) {
		if (empty($url)) return null;
		$my_host = env('MY_HOST', '');
		return !preg_match("/{$my_host}/", $url);
	}
}
