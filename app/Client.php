<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Client extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['firstname', 'lastname', 'tel', 'email', 'workplace', 'title', 'address1', 'address2', 'address3', 'hkidpassport', 
                            'firstname2', 'lastname2', 'tel2', 'email2', 'workplace2', 'title2', 
                            'budget_max', 'budget_min', 'tempature', 'comments', 'clienttype', 'uhnw'];

    public static $rules = array(
            'firstname' => 'required|max:255',
        );

    public static $messages = array(
            'firstname.required' => 'The first name field is required.',
        );

    protected $searchable = ['id', 'firstname', 'lastname', 'tel', 'email',
                        'workplace', 'title', 'budget_max', 'budget_min', 'tempature', 'user_id'];

    public static $tempatures = array(
            'Hot'  => 'Hot',
            'Warm' => 'Warm',
            'Cold' => 'Cold',
        ); 
		
    public $clienttypes = array(
            0  => 'Normal Client',
            1 => 'Co-op',
            2 => 'Referral',
        ); 

    /**
     * Get all of the shortlists for the client.
     */
    public function shortlists()
    {
        return $this->hasMany(ClientShortlist::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function options()
    {
        return $this->hasMany(ClientOption::class);
    }

    public function scopeIndexSearch($query, $request)
    {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if((in_array($key, $this->searchable) || $key == 'sstring') && $value!='') {
                if(is_string($value))$value = trim($value);
                switch ($key) {
                    case 'firstname':
                    case 'lastname':
                    case 'tel':
                    case 'email':
                    case 'workplace':
                    case 'title':
                        $query->where(function($query) use ($key, $value){
                            $query->orWhere($key, 'like', "%{$value}%")
                                ->orWhere($key.'2', 'like', "%{$value}%");
                        });
                        break;
                    case 'sstring':
						$s = filter_var($value, FILTER_SANITIZE_STRING, FILTER_UNSAFE_RAW);
						if (trim($s) != ''){
							$query->whereRaw('(workplace like "%'.$s.'%" or firstname like "%'.$s.'%" or lastname like "%'.$s.'%" or email like "%'.$s.'%" or title like "%'.$s.'%" or firstname2 like "%'.$s.'%" or lastname2 like "%'.$s.'%" or email2 like "%'.$s.'%" or workplace2 like "%'.$s.'%" or title2 like "%'.$s.'%")');
						}
                        break;
                    case 'budget_min':
                        if (is_numeric($value) && $value>0):
                            $query->where($key, '>=', $value);
                        endif;
                        break;
                    case 'budget_max':
                        if (is_numeric($value) && $value>0):
                            $query->where($key, '<=', $value);
                        endif;
                        break;
                    case 'tempature':
                        $query->where($key, '=', $value);
                        break; 
					case 'clients.id':
						if (strtolower($value) == 'uhnw'){
							$query->where('uhnw', '=', 1);
						}else{
							$query->where($key, '=', $value);
						}
						break;
                    case 'user_id':
                        $query->where($key, $value);
                    break;
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        return $query;
    }
	
    public function scopePropertyClientSearch($query){
        $query->join('client_options', 'clients.id', '=', 'client_options.client_id');
		
        return $query;
    }

    public function save_shortlist_options($shortlist_id = null, $option_ids = array()) {
        if (empty($shortlist_id) || empty($option_ids)) {
            return false;
        }
		$shortlist = ClientShortlist::findOrFail($shortlist_id);
		$existing = $this->options()->where('shortlist_id', $shortlist_id)->withTrashed()->get();
		$ex = array();
		$trashed = array();
		if (count($existing) > 0){
			foreach ($existing as $e){
				$ex[intval($e->property_id)] = 1;
				if ($e->trashed()){
					$trashed[intval($e->property_id)] = $e;
				}
			}
		}
        foreach ($option_ids as $option_id) {
            if($option_id > 0) {
                if (!isset($ex[intval($option_id)])){
					$tid = 1;
					if ($shortlist->typeid == 2){
						$tid = 2;
					}
					$this->options()->create(array(
						'shortlist_id' => $shortlist_id,
						'property_id'   => $option_id,
						'type_id' => $tid
					));
				}else if (isset($trashed[intval($option_id)])){
					$trashed[intval($option_id)]->restore();
				}
            }
        }
    }

    /**
     * Helpers
     */
    public function name() {
        if(!empty($this->firstname)) $res[] = $this->firstname;
        if(!empty($this->lastname)) $res[] = $this->lastname;
        return !empty($res)?join(' ', $res):'';
    }

    public function description() {
        if(!empty($this->workplace)) $res[] = $this->workplace;
        if(!empty($this->title)) $res[] = $this->title;
        return !empty($res)?join(', ', $res):'';
    }

    public function name2() {
        if(!empty($this->firstname2)) $res[] = $this->firstname2;
        if(!empty($this->lastname2)) $res[] = $this->lastname2;
        return !empty($res)?join(' ', $res):'';
    }

    public function description2() {
        if(!empty($this->workplace2)) $res[] = $this->workplace2;
        if(!empty($this->title2)) $res[] = $this->title2;
        return !empty($res)?join(', ', $res):'';
    }

    public function is_selected_tempature($tempature=null) {
        return $tempature == $this->tempature;
    }

    public function basic_info() {
        $res[] = $this->name();
        if(!empty($this->tel))$res[] = 'Tel: '.$this->tel;
        if(!empty($this->email))$res[] = $this->email;
        return !empty($res)?join(', ', $res):'';
    }

    public function budget() {
        $max = $this->budget_max();
        $min = $this->budget_min();
        // dex([$max, $min]);
        if ($max == 0 && $min == 0) {
            return '-';
        } elseif($max==0) {
            return $min . 'up';
        } else {
            return $min .' - '. $max;
        }

    }

    public function budget_max() {
        return !empty($this->{__FUNCTION__})?number_format($this->{__FUNCTION__}, 0, '', ','):0;
    }

    public function budget_min() {
        return !empty($this->{__FUNCTION__})?number_format($this->{__FUNCTION__}, 0, '', ','):0;
    }

    public function email() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function tel() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }
	
    public function telLinked() {
		if (empty($this->tel) || trim($this->tel == '')){
			return '-';
		}
        return '<a href="tel:'.trim($this->tel).'" class="nest-clickable-phone-number">'.$this->tel.'</a>';
    }

    public function tempature() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function email2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }

    public function tel2() {
        return !empty($this->{__FUNCTION__})?$this->{__FUNCTION__}:'-';
    }
	
    public function tel2Linked() {
		if (empty($this->tel2) || trim($this->tel2 == '')){
			return '-';
		}
        return '<a href="tel:'.trim($this->tel2).'" class="nest-clickable-phone-number">'.$this->tel2.'</a>';
    }
	
	public function getClienttype(){
		if (isset($this->clienttypes[$this->clienttype])){
			return $this->clienttypes[$this->clienttype];
		}else{
			return '';
		}
	}
	
	

}


