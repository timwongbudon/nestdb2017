<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IOCommission extends Model
{
    public $timestamps = true;
    public $table = 'io_commissions';
    protected $fillable = ['id', 'user_id', 'property_id', 'status', 'commission', 'salarydate','owner_id','invoiceId','created_at'];
}
