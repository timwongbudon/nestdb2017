<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Commcons extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'commcons';
	
    protected $fillable = ['id', 'commissionid', 'shortlistid', 'consultantid', 'propertyid', 'leadid', 'commissiondate', 'invoicedate', 'salelease', 'commissiontotal', 'payout', 'commissionconsultant', 'tenantpaiddate', 'landlordpaiddate', 'commstruct'];

    const LEADING_COMMISION = 0.1; //10%
		
    public function property() {
       return $this->belongsTo(Property::class, 'propertyid'); 
    }
	
    public function shortlist() {
       return $this->belongsTo(ClientShortlist::class, 'shortlistid'); 
    }
	
    public function consultant() {
       return $this->belongsTo(User::class, 'consultantid'); 
    }
		
	
	
	
	
	
	
}