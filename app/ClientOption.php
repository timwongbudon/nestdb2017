<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class ClientOption extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['client_id', 'shortlist_id', 'property_id', 'remarks', 'order', 'type_id'];

    public $type_ids = array(
            '1' => 'Rent',
            '2' => 'Sale',
        );

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function type() {
        return isset($this->type_ids[$this->type_id])?$this->type_ids[$this->type_id]:'';
    }


    public function print_type() {
        $this->type_ids[1] = 'Lease';
        return isset($this->type_ids[$this->type_id])?$this->type_ids[$this->type_id]:'';
    }
}
