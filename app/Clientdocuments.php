<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;


class Clientdocuments extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	
    protected $table = 'clientdocuments';

    protected $fillable = ['id', 'doctype', 'clientid', 'consultantid', 'docpath'];
	
	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
}