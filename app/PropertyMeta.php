<?php

namespace App;

class PropertyMeta extends BaseMetaModel 
{
	protected $table = 'property_metas';
    protected $fillable = ['property_id', 'meta_key', 'meta_value'];
    public $timestamps  = false;
    public $key_id = 'property_id';

    public static $rules = array(
            'property_id' => 'required|numeric',
            'meta_key'    => 'required|max:255',
    	);
}
