<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Photoshoot extends BaseModel 
{
    use SoftDeletes;
	
	protected $table = 'photoshoot';
    protected $dates = ['deleted_at'];

    protected $fillable = ['v_date', 'driver', 'use_car',  'status', 'comments', 'hour', 'minute', 'ampm', 'commentsafter', 'timefrom', 'timeto'];


    public static $messages = array(
    );

    protected $searchable = ['shortlist_id', 'user_id', 'client_id', 'v_date', 'driver', 'status', 'comments'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function client() {
       return $this->belongsTo(Client::class); 
    }

    public function shortlist() {
       return $this->belongsTo(ClientShortlist::class); 
    }

    public function options(){
        return $this->hasMany(ClientViewingOption::class, 'photoshoot_id', 'id')->with('property');
    }
	
	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
	public function updated_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->updated_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
	public function v_date(){
		$d = date_create_from_format('Y-m-d', $this->v_date);
		return date_format($d, 'd/m/Y');
	}
	
	public function v_date_day(){
		$d = date_create_from_format('Y-m-d', $this->v_date);
		return date_format($d, 'l');
	}
	
	public function getOption($propertyid){
		return ClientViewingOption::where('photoshoot_id', $this->id)->where('property_id', $propertyid)->first();
	}

	public function getNiceTimeFrom(){
		$ret = $this->getNiceTimeOld();
		
		if (trim($this->timefrom) != ''){
			$ret = trim($this->timefrom);
		}
		
		return $ret;
	}
	
	public function getNiceTime(){
		$ret = $this->getNiceTimeOld();
		
		if (trim($this->timefrom) != ''){
			$ret = trim($this->timefrom);
		}
		if (trim($this->timeto) != ''){
			$ret .= '-'.trim($this->timeto);
		}
		
		return $ret;
	}
    
	public function getNiceTimeOld(){
		$ret = $this->hour;
		$ret .= ':';
		if ($this->minute < 10){
			$ret .= '0';
		}
		$ret .= $this->minute;
		if ($this->ampm == 1){
			$ret .= 'pm';
		}else{
			$ret .= 'am';
		}
		return $ret;
	}

	public function getUsedCar ()
	{
		switch($this->use_car) {
			case 1: return asset('/alphard_logo.png'); break;
			case 2: return asset('/tesla.png'); break;
                        case 3: return asset('/tesla2.png'); break;
			default: return asset('/alphard_logo.png'); break;
		}
	}


}
