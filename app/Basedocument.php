<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

use App\LogBasedocuments;

class Basedocument extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['id', 'doctype', 'docversion', 'consultantid', 'fieldcontents', 'sectionshidden', 'replace', 'revision'];
	
	public function after_save() {
		$this->logDocument();
	}
	
	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
	public function logDocument(){
		$log = new LogBasedocuments();	
		
		$log->id = $this->id;
		$log->doctype = $this->doctype;
		$log->docversion = $this->docversion;
		$log->consultantid = $this->consultantid;
		$log->sectionshidden = $this->sectionshidden;
		$log->fieldcontents = $this->fieldcontents;
		$log->replace = $this->replace;
		$log->revision = $this->revision;
		$log->userid = auth()->user()->id;
		
		$log->save();
	}

	public function getFieldValue ($field) 
	{
		$data = json_decode($this->fieldcontents, true);
		return !empty($data[$field]) ? $data[$field] : false;
	}
	
}