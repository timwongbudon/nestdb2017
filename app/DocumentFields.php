<?php

namespace App;

use Input;

class DocumentFields{
	
	public static function getCurDocVersions($doc){
		//Offer Letter
		if ($doc == 'ol'){
			return 1;
		}
		if ($doc == 'ta'){
			return 1;
		}
		if ($doc == 'tfl'){
			return 1;
		}
		if ($doc == 'lfl'){
			return 1;
		}
		if ($doc == 'lou'){
			return 1;
		}
		if ($doc == 'hdl'){
			return 1;
		}
		if ($doc == 'ua'){
			return 1;
		}
		if ($doc == 'hr'){
			return 1;
		}
		if ($doc == 'hre'){
			return 1;
		}
		if ($doc == 'cl'){
			return 1;
		}
		if ($doc == 'sl'){
			return 1;
		}
		if ($doc == 'cul'){
			return 1;
		}
		return 0;
	}
	
    public static function getOfferLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Address (Dear xxx)
					5|radio|Subject to Contract|SUBJECT TO CONTRACT#BINDING OFFER
					6|text|Premises (Address as per Landsearch)
					7|text|Tenant Full Name
					8|text|Work Title, Company
					9|text|Address 1
					10|text|Address 2
					11|text|Address 3
					58|radio|Tenant Document Type|Passport#HKID#BRI
					12|text|HKID / Passport / BRI
					42|radio|2nd Tenant Section|show#hide
					43|text|2nd Tenant Full Name
					44|text|2nd Tenant Work Title, Company
					45|text|2nd Tenant Address 1
					46|text|2nd Tenant Address 2
					47|text|2nd Tenant Address 3
					59|radio|2nd Tenant Document Type|Passport#HKID#BRI
					48|text|2nd Tenant HKID / Passport
					37|radio|Occupant Section|show#hide
					38|textarea|Occupant Details (If Shown)
					36|text|Landlord
					50|text|Landlord Address 1
					51|text|Landlord Address 2
					52|text|Landlord Address 3
					13|number|Monthly Rent (Amount)	
					53|text|Lease Duration			
					35|radio|Rent Inclusive/Exclusive of Management Fees, Government Rates and Government Rent|inclusive#exclusive
					14|number|Number of Car Parks
					15|number|Monthly Cost for Car Park(s)
					16|date|Lease Commencement From
					17|radio|Management Fees Paid by|Landlord#Tenant#Hide
					18|number|Management Fees (Amount)
					19|radio|Government Rates Paid by|Landlord#Tenant#Hide
					20|number|Government Rates (Amount)
					21|radio|Government Rent Paid by|Landlord#Tenant#Hide
					22|number|Government Rent (Amount)
					23|date|Rent Free / Early Poss. From
					24|date|Rent Free / Early Poss. To
					25|date|Handover Date
					26|text|Minimum Period|twelve (12) months
					49|radio|Break Clause|Tenant#Landlord#Both
					55|text|Break Clause Notice Period
					27|radio|Refund Deposit if Agreement Not Signed?|Yes#No
					56|date|No Refund Date
					57|number|No Refund Amount					
					28|text|Consultant Email
					29|text|Consultant Phone
					30|text|Consultant Name
					31|text|Consultant Role/Title
					32|text|Consultant License Number
					33|radio|Email Sentence|No Email Sentence#Thereafter kindly send us the draft Tenancy Agreement as soon as possible for the prospective Tenant’s perusal#Thereafter we shall forthwith prepare the Tenancy Agreement for your perusal.
					40|textareadrag|Other Conditions Landlord
					34|textareadrag|Other Conditions Tenant
					41|textareadrag|Special Conditions Landlord
					39|textareadrag|Special Conditions Tenant
					54|radio|Page break before last page?|Yes#No
					';
					//59 highest
		}
		
        return ''; 
    }
	public static function getOfferLetterShowHide($version){
        if ($version == 1){
			return '1|0|1|Carpark Section
					5|0|1|Rent-Free Period Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';
		}
		
        return ''; 
    }
	public static function getOfferLetterReplace($version){
        if ($version == 1){
			return array(
				'rental' => 'Rental',
				'carpark' => 'Car Park',
				'terms' => 'Terms',
				'mngfees' => 'Management Fees',
				'govrates' => 'Government Rates',
				'govrent' => 'Government Rent',
				'rentfree' => 'Rent-Free Period',
				'early' => 'Early Possession',
				'break' => 'Break Clause',
				'holding' => 'Holding Deposit',
				'security' => 'Security Deposit',
				'stamp' => 'Stamp Duty',
				'legal' => 'Legal Fees',
				'bank' => 'Bank Consent',
				'toplast' => 'Top Part of Last Page',
				'final' => 'Final Legal Remark',
			);
		}
		
        return ''; 
    }
	public static function getOfferLetterReplaceAutoText($version){
        if ($version == 1){
			return array(
				'rental' => 'HK$[[ !!! ]].00 per month, payable monthly in advance, [[ inclusive/exclusive ]] of Management Fees, Government Rates and Government Rent.',
				'carpark' => '[[ !!! ]] covered car parks to be provided for the account of the Tenant at HK$[[ !!! ]].00 per month.',
				'terms' => '[[ e.g. Two (2) years ]], lease commencement from [[ !!! ]].',
				'mngfees' => 'For the account of the [[ Landlord/Tenant ]], presently set at HK$[[ !!! ]].00 per month but subject to revision.',
				'govrates' => 'For the account of the [[ Landlord/Tenant ]], presently set at HK$[[ !!! ]].00 per month but subject to revision.',
				'govrent' => 'For the account of the [[ Landlord/Tenant ]], presently set at HK$[[ !!! ]].00 per month but subject to revision.',
				'rentfree' => 'The Landlord agrees to grant to the Tenant a rent-free period from [[ !!! ]] to [[ !!! ]] (both dates inclusive).  The Tenant agrees to pay all utility charges during the rent-free period.',
				'early' => 'The premises shall be handed over to the Tenant on [[ !!! ]] subject to signing of the Tenancy Agreement. Rental, Management Fees and Government Rates for the period from [[ !!! ]] to [[ !!! ]] shall be waived. However, the Tenant should take up all tenant’s responsibilities from the date of handover.',
				'break' => 'The [[ Tenant and the Landlord ]] shall have the right to terminate the lease (by giving [[ e.g. three (3) months\' written notice ]] to the Landlord or the Tenant) after a minimum period of [[ e.g. 12 months ]]\' occupancy.',
				'holding' => 'Please copy from PDF!',
				'security' => 'A deposit of HK$[[ !!! ]].00, representing two (2) months\' rental is payable to the Landlord upon the signing of the formal Tenancy Agreement.  This security deposit will be held by the Landlord for the term of the tenancy and shall be returned to the Tenant upon the expiry of term, without interest, and on the terms and conditions as set out in the Tenancy Agreement.',
				'stamp' => 'To be shared equally by both parties.',
				'legal' => 'Each party to bear its own legal costs. (if any)',
				'bank' => 'The Landlord shall provide to the Tenant a copy of the Written Consent for leasing purposes from the existing mortgagee, prior to the signing of the Tenancy Agreement for the leasing of the above property OR sign a Letter of Undertaking.',
				'toplast' => 'Should you agree to the above terms and conditions, kindly sign, scan and return a copy of this letter to us at [[ !!! ]] at your earliest convenience. Alternatively, please do not hesitate to contact me on [[ !!! ]], should you have any questions regarding the above.',
				'final' => 'SUBJECT TO CONTRACT:  This document is prepared by Nest Property for information only.  Whilst reasonable care has been exercised in preparing this document, it is subject to change and Nest Property makes no representation to its truth, accuracy or completeness, and accordingly cannot be held responsible for any liability whatsoever for any loss howsoever arising from or in reliance upon the whole or any part of the contents of this document.',
			);
		}
		
        return ''; 
    }
	public static function getOfferLetterHelpPics($version){
        if ($version == 1){
			return array(
				'fields' => array(
					'1' => '/images/tools/offerletter/f1.png', //Date of Issue
					'2' => '/images/tools/offerletter/f2.png', //To
					'3' => '/images/tools/offerletter/f3.png', //Attn
					'4' => '/images/tools/offerletter/f4.png', //Address (Dear xxx)
					'5' => '/images/tools/offerletter/f5.png', //Subject to Contract|SUBJECT TO CONTRACT#BINDING OFFER
					'6' => '/images/tools/offerletter/f6.png', //Premises (Address as per Landsearch)
					'7' => '/images/tools/offerletter/f7.png', //Tenant Full Name
					'8' => '/images/tools/offerletter/f8.png', //Work Title, Company
					'9' => '/images/tools/offerletter/f9.png', //Address 1
					'10' => '/images/tools/offerletter/f10.png', //Address 2
					'11' => '/images/tools/offerletter/f11.png', //Address 3
					'58' => '/images/tools/offerletter/f58.png', //Tenant Document Type|Passport#HKID#BRI
					'12' => '/images/tools/offerletter/f12.png', //HKID / Passport / BRI
					'42' => '/images/tools/offerletter/f42.png', //2nd Tenant Section|show#hide
					'43' => '/images/tools/offerletter/f43.png', //2nd Tenant Full Name
					'44' => '/images/tools/offerletter/f44.png', //2nd Tenant Work Title, Company
					'45' => '/images/tools/offerletter/f45.png', //2nd Tenant Address 1
					'46' => '/images/tools/offerletter/f46.png', //2nd Tenant Address 2
					'47' => '/images/tools/offerletter/f47.png', //2nd Tenant Address 3
					'59' => '/images/tools/offerletter/f59.png', //2nd Tenant Document Type|Passport#HKID#BRI
					'48' => '/images/tools/offerletter/f48.png', //2nd Tenant HKID / Passport
					'37' => '/images/tools/offerletter/f37.png', //Occupant Section|show#hide
					'38' => '/images/tools/offerletter/f38.png', //Occupant Details (If Shown)
					'36' => '/images/tools/offerletter/f36.png', //Landlord
					'50' => '/images/tools/offerletter/f50.png', //Landlord Address 1
					'51' => '/images/tools/offerletter/f51.png', //Landlord Address 2
					'52' => '/images/tools/offerletter/f52.png', //Landlord Address 3
					'13' => '/images/tools/offerletter/f13.png', //Monthly Rent (Amount)	
					'53' => '/images/tools/offerletter/f53.png', //Lease Duration			
					'35' => '/images/tools/offerletter/f35.png', //Rent Inclusive/Exclusive of Management Fees, Government Rates and Government Rent|inclusive#exclusive
					'14' => '/images/tools/offerletter/f14.png', //Number of Car Parks
					'15' => '/images/tools/offerletter/f15.png', //Monthly Cost for Car Park(s)
					'16' => '/images/tools/offerletter/f16.png', //Lease Commencement From
					'17' => '/images/tools/offerletter/f17.png', //Management Fees Paid by|Landlord#Tenant#Hide
					'18' => '/images/tools/offerletter/f18.png', //Management Fees (Amount)
					'19' => '/images/tools/offerletter/f19.png', //Government Rates Paid by|Landlord#Tenant#Hide
					'20' => '/images/tools/offerletter/f20.png', //Government Rates (Amount)
					'21' => '/images/tools/offerletter/f21.png', //Government Rent Paid by|Landlord#Tenant#Hide
					'22' => '/images/tools/offerletter/f22.png', //Government Rent (Amount)
					'23' => '/images/tools/offerletter/f23.png', //Rent Free / Early Poss. From
					'24' => '/images/tools/offerletter/f24.png', //Rent Free / Early Poss. To
					'25' => '/images/tools/offerletter/f25.png', //Handover Date
					'26' => '/images/tools/offerletter/f26.png', //Minimum Period|twelve (12) months
					'49' => '/images/tools/offerletter/f49.png', //Break Clause|Tenant#Landlord#Both
					'55' => '/images/tools/offerletter/f55.png', //Break Clause Notice Period
					'27' => '/images/tools/offerletter/f27.png', //Refund Deposit if Agreement Not Signed?|Yes#No
					'56' => '/images/tools/offerletter/f56.png', //No Refund Date
					'57' => '/images/tools/offerletter/f57.png', //No Refund Amount					
					'28' => '/images/tools/offerletter/f28.png', //Consultant Email
					'29' => '/images/tools/offerletter/f29.png', //Consultant Phone
					'30' => '/images/tools/offerletter/f30.png', //Consultant Name
					'31' => '/images/tools/offerletter/f31.png', //Consultant Role/Title
					'32' => '/images/tools/offerletter/f32.png', //Consultant License Number
					'33' => '/images/tools/offerletter/f33.png', //Email Sentence|No Email Sentence#Thereafter kindly send us the draft Tenancy Agreement as soon as possible for the prospective Tenant’s perusal#Thereafter we shall forthwith prepare the Tenancy Agreement for your perusal.
					'40' => '/images/tools/offerletter/f40.png', //Other Conditions Landlord
					'34' => '/images/tools/offerletter/f34.png', //Other Conditions Tenant
					'41' => '/images/tools/offerletter/f41.png', //Special Conditions Landlord
					'39' => '/images/tools/offerletter/f39.png', //Special Conditions Tenant
				),
				'showhide' => array(
					'1_0' => '/images/tools/offerletter/1_0.png',
					'2_0' => '/images/tools/offerletter/2_0.png',
					'4_0' => '/images/tools/offerletter/4_0.png',
					'5_0' => '/images/tools/offerletter/5_0.png',
				),
				'replace' => array(
					'rental' => '/images/tools/offerletter/rental.png',
					'carpark' => '/images/tools/offerletter/carpark.png',
					'terms' => '/images/tools/offerletter/terms.png',
					'mngfees' => '/images/tools/offerletter/mngfees.png',
					'govrates' => '/images/tools/offerletter/govrates.png',
					'govrent' => '/images/tools/offerletter/govrent.png',
					'rentfree' => '/images/tools/offerletter/rentfree.png',
					'early' => '/images/tools/offerletter/early.png',
					'break' => '/images/tools/offerletter/break.png',
					'holding' => '/images/tools/offerletter/holding.png',
					'security' => '/images/tools/offerletter/security.png',
					'stamp' => '/images/tools/offerletter/stamp.png',
					'legal' => '/images/tools/offerletter/legal.png',
					'bank' => '/images/tools/offerletter/bank.png',
					'toplast' => '/images/tools/offerletter/toplast.png',
					'final' => '/images/tools/offerletter/final.png',
				)
			);
		}
		
        return ''; 
    }
	
	public static function getTenancyAgreementFields($version){
        if ($version == 1){
					//2|number|Days to Repay Deposit [taken out, now standard at 15 days via DocumentsController]
			return '1|date|Date of Agreement
					3|text|Minimum Term (Months)
					4|text|Months Notice for Early Termination|e.g. two (2)
					5|text|Landlord Name/Company
					6|textarea|Landlord Further Details
					23|radio|Landlord Document Type|BRI#HKID#Passport
					17|text|Landlord Document ID for Signing Fields (BRI, HKID, etc.)
					7|text|Tenant Name
					8|textarea|Tenant Further Details
					24|radio|Tenant Document Type|Passport#HKID#BRI
					18|text|Tenant Document ID for Signing Fields (Passport, HKID, etc.)
					25|text|Occupant 1 Name
					27|radio|Occupant 1 Document Type|Passport#HKID#BRI
					26|text|Occupant 1 Document ID
					28|text|Occupant 2 Name
					30|radio|Occupant 2 Document Type|Passport#HKID#BRI
					29|text|Occupant 2 Document ID
					9|text|Premises
					10|text|Lease Term
					11|text|Prepayment|e.g. Thirty Four Thousand (HK$34,000.00)
					12|text|Deposit|e.g. Sixty Eight Thousand (HK$68,000.00)
					13|text|Monthly Rent|e.g. Thirty Four Thousand (HK$34,000.00)
					21|text|Day of Monthly Rental Payment|first, second, twelfth, ...
					14|text|Landlord Bank Account: Bank
					15|text|Landlord Bank Account: Name
					16|text|Landlord Bank Account: Account Number
					19|date|Lease Commencement Date
					20|date|Lease Expiry Date
					22|radio|Break Clause|Tenant#Landlord#Both
					31|text|Section X, Add. 1 - Title
					32|textarea|Section X, Add. 1 - Text
					33|text|Section X, Add. 2 - Title
					34|textarea|Section X, Add. 2 - Text
					35|text|Section X, Add. 3 - Title
					36|textarea|Section X, Add. 3 - Text
					37|text|Section X, Add. 4 - Title
					38|textarea|Section X, Add. 4 - Text
					39|text|Section X, Add. 5 - Title
					40|textarea|Section X, Add. 5 - Text
					41|text|Part III, Add (Early Possession/Rent Free)
					42|textarea|Part III, Add - Text
					';
					//42 highest
		}
		
        return ''; 
    }
	public static function getTenancyAgreementShowHide($version){
        if ($version == 1){
			return '1|0|1|Pets and Infestation Section
					2|0|1|Show Break Clause (Hide If FIXED Term)
					3|0|1|Line Break before Section III
					4|0|1|Line Break before Section V
					5|0|1|Line Break before Section VII
					6|0|1|Line Break before Section IX
					7|0|1|Line Break before Section X
					8|0|1|Section IV - Compliance with Ordinances
					9|0|1|Section IV - Tenants Other Conditions
					10|0|1|Section V - Landlord Other Conditions
					11|0|1|Section IX - Early Termination
					12|0|1|Section X - Counterparts
					13|0|1|Part III - Early Possession/Rent Free ';
		}
		
        return ''; 
    }
	public static function getTenancyAgreementReplace($version){
        if ($version == 1){
			return array(
				'section-i-bottom' => 'Section I Bottom',
				'section-ii-1' => 'Section II 1 - Rent',
				'section-ii-2' => 'Section II 2 - Default in Payment of Rent',
				'section-ii-3' => 'Section II 3 - Utility Charges and Deposits',
				'section-iii-1-i' => 'Section III 1 I',
				'section-iii-1-ii' => 'Section III 1 II',
				'section-iii-1-iii' => 'Section III 1 III',
				'section-iii-1-iv' => 'Section III 1 IV',
				'section-iii-2' => 'Section III 2 - Repayment of Deposit',
				'section-iv-1' => 'Section IV 1 - User',
				'section-iv-2' => 'Section IV 2 - Compliance with Ordinances',
				'section-iv-3' => 'Section IV 3 - Fitting Out Interior',
				'section-iv-4' => 'Section IV 4 - Good Repair',
				'section-iv-5' => 'Section IV 5 - Indemnity against Loss / Damage ...',
				'section-iv-6' => 'Section IV 6 - Landlord to Enter and View',
				'section-iv-7' => 'Section IV 7 - Yield Up Premises and Reinstatement',
				'section-v-1' => 'Section V 1 - Premises and Furniture',
				'section-v-2-i' => 'Section V 2 I',
				'section-v-2-ii' => 'Section V 2 II',
				'section-v-3' => 'Section V 3 - Payment of Fees',
				'section-v-4' => 'Section V 4 - Main Structure, Structural Repair ...',
				'section-vi-1-a' => 'Section VI 1 A',
				'section-vi-1-b' => 'Section VI 1 B',
				'section-vi-1-c' => 'Section VI 1 C',
				'section-vi-2' => 'Section VI 2 - Locks',
				'section-vi-3' => 'Section VI 3 - Illegal Use or Nuisance',
				'section-vi-4' => 'Section VI 4 - Pets and Infestation',
				'section-vi-5' => 'Section VI 5 - Subletting, Assigning',
				'section-vi-6' => 'Section VI 6 - Dangerous Articles',
				'section-vii-1' => 'Section VII 1 - Lifts and Utilities, Fire and ...',
				'section-vii-2' => 'Section VII 2 - Security',
				'section-viii-1' => 'Section VIII 1',
				'section-viii-2' => 'Section VIII 2',
				'section-ix-1' => 'Section IX 1',
				'section-ix-2' => 'Section IX 2',
				'section-ix-3' => 'Section IX 3 - Early Termination',
				'section-ix-3-a' => 'Section IX 3 A',
				'section-ix-3-b' => 'Section IX 3 B',
				'section-ix-3-c' => 'Section IX 3 C',
				'section-ix-3-d' => 'Section IX 3 D',
				'section-x-1' => 'Section X 1 - Condonation not a Waiver',
				'section-x-2' => 'Section X 2 - Letting Notices',
				'section-x-3' => 'Section X 3 - Service of Notice',
				'section-x-4' => 'Section X 4 - Stamp Duty and Costs',
				'section-x-5' => 'Section X 5 - No Premium Paid',
				'section-x-6' => 'Section X 6 - Counterparts',
				'break' => 'Break Clause',
				'part-iii-terms' => 'Part II - Terms',
				'landLordOtherConditions' => 'Section V - Landlord Other Conditions',
				'tenantOtherConditions' => 'Section IV - Tenants Other Conditions'
			);
		}
		
        return ''; 
    }
	public static function getTenancyAgreementReplaceAutoText($version){
        if ($version == 1){
			return array(
				'section-i-bottom' => 'The Landlord shall let and the Tenant shall take ALL the Premises (“the Premises”) forming part of the Building (“the Building”) as set out in Part II of the First Schedule TOGETHER with furniture, fixtures, fittings, and appliances (collectively called “the Furniture”), including those set out in the Third Schedule, all on an “as is, where is” basis, AND TOGETHER with the use in common with the Landlord and all others having the like right of the common facilities in the Building and for the Term as set out in Part III of the First Schedule (the “Term”) AND PAYING throughout the Term the rent (the “Rent”) as set out in the Second Schedule and other charges as provided herein. ',
				'section-ii-1' => 'The Tenant shall pay the Rent without deductions, monthly in advance, on or before the [[ day e.g. twelfth ]] day of each and every calendar month by bank auto-pay to the Landlord’s specified bank account, as set out in the Second Schedule.  The first and last of such payments shall be apportioned according to the number of days in the month included in the said Term. If the first day of the month falls on a Sunday or public holiday of Hong Kong, the Tenant shall pay the said Rent on the business day prior to the first day of the month concerned.',
				'section-ii-2' => 'If any rent or any part thereof shall be 15 days in arrear (whether legally demanded or not) or if the Tenant shall fail to observe or perform any stipulation herein contained and on his part to be observed or performed or if the Tenant shall become bankrupt or, being a limited company, shall go into liquidation or shall become insolvent, or make any composition or arrangement with creditors, or shall suffer any execution to be levied on the said premises, or otherwise on the Tenant\'s goods, it shall be lawful for the Landlord to re-enter upon the said premises or upon any part thereof in the name of the whole, and this Tenancy shall thereupon absolutely cease, and determine but without prejudice, to any claim which the Landlord may have against the Tenant, in respect of any breach of the stipulations on the part of the Tenant herein contained, or to the Landlord\'s right to deduct all monetary loss thereby incurred from the deposit paid by the Tenant.  ',
				'section-ii-3' => 'The Tenant shall pay all deposits and charges in respect of any gas, water, electricity, telephone, internet, broadband, cable TV and any other utilities consumed on or in the Premises.',
				'section-iii-1-i' => 'Upon the signing of this Agreement, the Tenant shall pay a deposit to the Landlord as specified in Part IV of the First Schedule (the “Deposit”), Provided Always that the Deposit shall be equivalent to twice the amount of the Rent as security for the due payment of all sums payable herein and the due observance by the Tenant of the terms and conditions of this Agreement.  ',
				'section-iii-1-ii' => 'The Landlord may deduct from the Deposit the amount of Rent and other charges, expenses, loss or damage sustained by the Landlord as the result of any breach of this Agreement by the Tenant. In the event of any deduction being made by the Landlord from the Deposit, the Tenant shall on demand by the Landlord make a further deposit equal to the amount so deducted and failure by the Tenant to do so shall entitle the Landlord to forthwith terminate this Agreement but without prejudice to any right of action by the Landlord in respect of any outstanding breach of this Agreement by the Tenant.',
				'section-iii-1-iii' => 'In the event and upon the Landlord selling the said premises during the term of this tenancy, the Landlord shall be entitled to transfer the said deposit or the balance thereof to the new owner subject to the Landlord obtaining an undertaking from the new owner given in favour of the Landlord and the Tenant to hold and refund the said deposit to the Tenant in accordance with the terms and conditions of this Agreement.',
				'section-iii-1-iv' => 'The Tenant shall not apply the Deposit as payment of Rent or payment in lieu of notice.',
				'section-iii-2' => 'Subject to the above, the Deposit shall be refunded to the Tenant without interest within 15 days after the expiration or sooner determination of this Agreement and delivery of vacant possession of the Premises to the Landlord, and after settlement of all claims by the Landlord against the Tenant in respect of any breach, non-observance or non-performance of any of the agreements, stipulations, terms or conditions herein contained, and on the part of the Tenant to be observed and performed whichever is the later. ',
				'section-iv-1' => 'To use the Premises for domestic purpose only. The Premises shall only be occupied by the Tenant and his immediate family. ',
				'section-iv-2' => 'To comply with and to indemnify the Landlord against the breach of any ordinances of Hong Kong and the Deed of Mutual Covenants of the Building, and any other act, nonfeasance, or negligence by the Tenant, the designated occupier, or any contractors, employees, invitees, visitors, agents, licensees of the Tenant or family of the designated occupier of the Tenant (collectively the “Tenant’s Agents”).',
				'section-iv-3' => 'To maintain the interior of the Premises throughout the Term in good, clean and tenantable condition and repair at the Tenant’s cost and expense (fair wear and tear excepted); and except for hanging a wall-mounted television and small paintings on the walls, the Tenant shall seek the Landlord’s prior approval in writing of all plans and specifications in the event that the Tenant wants to decorate, renovate and/or fit out the interior of the Premises.  ',
				'section-iv-4' => 'To keep all the Furniture and the interior of the Premises, windows, glass, electrical wiring, drains, pipes, sanitary or plumbing apparatus and all additions thereto, in good repair and condition (fair wear and tear excepted).',
				'section-iv-5' => 'To be responsible for any loss, damage or injury caused to any person directly or indirectly through whatever cause, including the defective or damaged condition of any part of the interior of the Premises or any Furniture for the repair of which the Tenant is responsible.  The Tenant shall indemnify the Landlord against all such costs, losses and claims as a result therefrom.  As the occupier of the Premises, the Tenant is obligated under Hong Kong law to effect and maintain during the Term adequate insurance coverage against occupiers liability.  ',
				'section-iv-6' => 'To permit the Landlord to enter upon the Premises, with prior appointment to be made with the Tenant (consent shall not be unreasonably withheld), to view the condition and take inventories of the Furniture and to carry out any repair required to be done, provided that in the event of an emergency, the Landlord or its authorized agents may enter without notice and forcibly if necessary.',
				'section-iv-7' => 'To yield up the Premises and the Furniture and additions thereto at the expiration or sooner termination of the Term in good, clean and tenantable condition and repair (fair wear and tear excepted). Where the Tenant has made any alterations or renovations, with or without the Landlord’s consent and/or any damage caused, the Tenant shall at its cost and expense reinstate the Premises and the Furniture in its original condition before handing over the Premises to the Landlord. ',
				'section-v-1' => 'To convey to the Tenant at the start of the Term the Premises and the Furniture in good, clean and tenantable condition and repair, with all of the Furniture in good working order.  Further, to maintain Premises and the Furniture in good working order and repair at the Landlord’s expense, except where, pursuant to Section IV, the Tenant is responsible for the expense of repairs.',
				'section-v-2-i' => 'Subject to the Tenant duly paying the Rent and other charges and observing the terms and conditions of this Agreement, to permit Tenant to have quiet possession and enjoyment of the Premises during the Term without any interruption by the Landlord.',
				'section-v-2-ii' => 'The Landlord declares that he has not received and does not have knowledge of existence of any notice relating to the renovation or refurbishment of the common parts or external wall of the building of which the said premises form part.',
				'section-v-3' => 'To pay the Management Fees, Government Rates and Rent, and Property Tax. ',
				'section-v-4' => 'To keep the main structure of the Premises in proper and tenantable repair and condition, and to be responsible for structural repairs and latent defects whenever necessary, except those caused by the act, negligence or nonfeasance by the Tenant or the Tenant’s Agents. ',
				'section-vi-1-a' => 'Not without the prior written consent of the Landlord, to install or alter any structure, fixtures, partitioning, installation, any electrical wiring, air condition ducting or lighting fixtures on and in the Premises, or any equipment or machinery including any safe which imposes a weight on the flooring in excess of its design.',
				'section-vi-1-b' => 'Not to damage, remove or alter any doors, windows, walls, or any part of the fabric of the Premises nor any of the plumbing or sanitary apparatus or installations. ',
				'section-vi-1-c' => 'Not to affix anything or paint or make any alteration to the exterior of the Premises.',
				'section-vi-2' => 'Not without the prior written consent of the Landlord to install additional locks, bolts or other fittings to the entrance doors or grille gate to the Premises, and in the event that consent is given under this Clause, to immediately deposit with the Landlord duplicate keys to all such additional locks, or other fittings as may be approved for installation. All keys are to be returned to the Landlord pursuant to the Tenant’s obligations under Section IV clause (7) of this Agreement.',
				'section-vi-3' => 'Not to use the Premises for any illegal, immoral or improper purposes, nor to cause disturbance or nuisance to the occupiers of the neighbouring premises.  Not to place or leave or (so far as may be in his power) suffer or permit to be placed or left any boxes furniture articles or rubbish in the entrance of the Premises or the entrance of or any of the staircases passages or landings of the said building used in common with other occupiers of the said building or otherwise encumber the same.',
				'section-vi-4' => 'Not to keep any pets or animals in the said premises which would cause nuisance or annoyance or threat to other tenants or occupiers of the building of which the said premises forms part, and to take all precautions to prevent the Premises from becoming infested by termites, rats, mice, cockroaches or any other pests or vermin. ',
				'section-vi-5' => 'Not to assign, underlet, share, lend or otherwise part with the possession of the Premises whereby any third person obtains the use or possession of the Premises.  In such event, this Agreement shall terminate forthwith. The Tenant shall forthwith vacate the Premises on Landlord’s notice.   This tenancy shall be personal to the Tenant only.',
				'section-vi-6' => 'Not to store on or in the Premises any combustible or hazardous goods.',
				'section-vii-1' => 'In respect of any loss or damage to person or property sustained by the Tenant owing to any defect in or breakdown of the lifts, electric power and water supplies, or any other service provided in the Premises or the Building, or owing to the escape of fumes, smoke, fire or any other substance or thing or the overflow of water from anywhere within the Premises or the Building, or',
				'section-vii-2' => 'For the security or safekeeping of the Premises or any contents therein and the provision by the Building Management of watchmen and caretakers shall not create any obligation on the part of the Landlord, nor shall the rent and other charges herein provided abate or cease to be payable on account of any security breach. The responsibility for the safety of the Premises and the contents thereof shall at all times rest with the Tenant.',
				'section-viii-1' => 'If the Premises or the Building shall at any time during the Term be condemned as a dangerous structure, or be subject to a demolition order or closing order, or be destroyed or damaged or inaccessible owing to any calamity or material circumstances beyond the control of the Landlord and not attributable to the negligence or fault of the Tenant or the Tenant’s Agents, and the policy of insurance on the Premises shall not have been vitiated or payment of the policy moneys refused in consequence of any act or default of the Tenant or the Tenant’s Agents, then, the Rent or a fair proportion thereof according to the nature of the damage sustained or order made shall, after the expiration of the then current month, be suspended until the Premises shall again be fit for use.  ',
				'section-viii-2' => 'Should the Premises or the Building not have been reinstated in the meantime, either the Landlord or the Tenant may at any time after 3 months from the occurrence of such damage or destruction or order, give to the other party 1 month’s notice to terminate this present tenancy and thereupon the same shall cease as from the date of the occurrence of such destruction or damage or order or the Premises becoming inaccessible but without prejudice to the rights of the Landlord in respect of the Rent payable hereunder prior to the coming into effect of the suspension.',
				'section-ix-1' => 'If the Rent or any part thereof is in arrears for 15 days or if the Tenant shall persistently delay in paying the Rent or use the premises for immoral or illegal purposes; or cause nuisance and/or make unauthorized structural alteration to the Premises; or if there shall be any breach of a material term herein contained, in any such case it shall be lawful for the Landlord to forfeit this tenancy and terminate this Agreement forthwith at any time thereafter.',
				'section-ix-2' => 'The parties agree it shall be lawful for the Landlord at any time thereafter to re-enter on the Premises; or, at the sole discretion of the Landlord, to serve a notice to quit (the “Notice”) to the Tenant and the Tenant agrees to move out from the Premises within the time specified in the Notice.  The Deposit shall be forfeited to the Landlord without prejudice to the Landlord’s other rights and entitlements.   All costs and expenses incurred by the Landlord in demanding payment of the Rent and other charges shall be paid by the Tenant and shall be recoverable from the Tenant as a debt.',
				'section-ix-3' => 'Notwithstanding anything herein contained to the contrary, it is hereby agreed by the parties hereto that [[ either the Landlord or the Tenant ]] shall be at liberty to earlier terminate this Agreement at any time after the first [[ !!! ]] months of the Term subject to the following conditions:-',
				'section-ix-3-a' => 'By giving not less than one (1) calendar month prior notice in writing of such intention to terminate this Agreement or in lieu of such notice pay an amount equivalent to one (1) month of the agreed Rent. The Tenant shall not use the Deposit as Rent in lieu of notice for this purpose.',
				'section-ix-3-b' => 'The Tenant shall duly observe, perform and comply with all agreements, conditions, provisions, terms and stipulations on the Tenant\'s part herein contained up to the date of such earlier determination.',
				'section-ix-3-c' => 'The Tenant shall upon the expiration of such notice of earlier termination deliver up vacant possession of the Premises to the Landlord in accordance with Section IV Clause (7) hereof.',
				'section-ix-3-d' => 'Such earlier termination shall not prejudice the rights of either party against the other in respect of any antecedent claim for breach of agreement and shall not affect the right of the Tenant to recover the Deposit from the Landlord, after due deductions (if any).',
				'section-x-1' => 'No condoning, or overlooking by the Landlord of any default or breach of this Agreement by the Tenant shall operate as a waiver of the Landlord’s rights or so as to affect in any way the rights and remedies of the Landlord hereunder and no waiver by the Landlord shall be implied by anything done or omitted by the Landlord.  Any consent given by the Landlord shall operate as a consent only for the particular matter to which it relates and shall not be construed as dispensing with the necessity of obtaining the specific written consent of the Landlord in the future.',
				'section-x-2' => 'Notwithstanding anything herein contained, during the 1 month immediately before the expiration or sooner termination of the Term, the Tenant shall permit Landlord’s authorized agent, with prior appointment to be made with the Tenant (consent shall not be unreasonably withheld), to enter and view the Premises at all reasonable times and the Landlord shall during such period be at liberty to make known to the public that the Premises are to be let and other relevant information.',
				'section-x-3' => 'Any notice required to be served on either the Tenant or the Landlord shall be deemed to have been validly served if delivered by ordinary or registered post or left at the Premises or at the last known address of the Tenant or the Landlord.   A notice sent by ordinary or registered post shall be deemed to be given next business day after posting.',
				'section-x-4' => 'Each party shall bear its own costs of the preparation of this Agreement and the stamp duty thereon shall be borne by the Landlord and the Tenant in equal shares.',
				'section-x-5' => 'The Tenant hereby declares that he has not paid any premium, construction money or other consideration for the tenancy.',
				'break' => 'The Tenant and the Landlord shall have the right to terminate the lease (by giving two (2) months written notice to the other Party) after a minimum period of twelve (12) months occupancy.',
				'section-x-6' => 'This agreement may be executed by each party, in twp or more counterparts. These said counterparts, all of which together, shall constitute one and the same instrumental and shall be deemed to be a full and complete contract between the parties hereto. While there may be duplicate originals of this Agreement, including counterparts, only one need be produced as evidence of terms thereof.',
				'part-iii-terms' => '[Terms] commencing on [start_date] and expiring on [expiry_date] (both days inclusive)'
			);
		}
		
        return ''; 
    }
	
	public static function getTenancyAgreementHelpPics($version){
        if ($version == 1){
			return array(
				'fields' => array(
					'1' => '/images/tools/tenancyagreement/f1.png', //Date of Agreement
					'3' => '/images/tools/tenancyagreement/f3.png', //Minimum Term (Months)
					'4' => '/images/tools/tenancyagreement/f4.png', //Months Notice for Early Termination|e.g. two (2)
					'5' => '/images/tools/tenancyagreement/f5.png', //Landlord Name/Company
					'6' => '/images/tools/tenancyagreement/f6.png', //Landlord Further Details
					'23' => '/images/tools/tenancyagreement/f23.png', //Landlord Document Type|BRI#HKID#Passport
					'17' => '/images/tools/tenancyagreement/f17.png', //Landlord Document ID for Signing Fields (BRI, HKID, etc.)
					'7' => '/images/tools/tenancyagreement/f7.png', //Tenant Name
					'8' => '/images/tools/tenancyagreement/f8.png', //Tenant Further Details
					'24' => '/images/tools/tenancyagreement/f24.png', //Tenant Document Type|Passport#HKID#BRI
					'18' => '/images/tools/tenancyagreement/f18.png', //Tenant Document ID for Signing Fields (Passport, HKID, etc.)
					'25' => '/images/tools/tenancyagreement/f25.png', //Occupant 1 Name
					'27' => '/images/tools/tenancyagreement/f27.png', //Occupant 1 Document Type|Passport#HKID#BRI
					'26' => '/images/tools/tenancyagreement/f26.png', //Occupant 1 Document ID
					'28' => '/images/tools/tenancyagreement/f28.png', //Occupant 2 Name
					'30' => '/images/tools/tenancyagreement/f30.png', //Occupant 2 Document Type|Passport#HKID#BRI
					'29' => '/images/tools/tenancyagreement/f29.png', //Occupant 2 Document ID
					'9' => '/images/tools/tenancyagreement/f9.png', //Premises
					'10' => '/images/tools/tenancyagreement/f10.png', //Lease Term
					'11' => '/images/tools/tenancyagreement/f11.png', //Prepayment|e.g. Thirty Four Thousand (HK$34,000.00)
					'12' => '/images/tools/tenancyagreement/f12.png', //Deposit|e.g. Sixty Eight Thousand (HK$68,000.00)
					'13' => '/images/tools/tenancyagreement/f13.png', //Monthly Rent|e.g. Thirty Four Thousand (HK$34,000.00)
					'21' => '/images/tools/tenancyagreement/f21.png', //Day of Monthly Rental Payment|first, second, twelfth, ...
					'14' => '/images/tools/tenancyagreement/f14.png', //Landlord Bank Account: Bank
					'15' => '/images/tools/tenancyagreement/f15.png', //Landlord Bank Account: Name
					'16' => '/images/tools/tenancyagreement/f16.png', //Landlord Bank Account: Account Number
					'19' => '/images/tools/tenancyagreement/f19.png', //Lease Commencement Date
					'20' => '/images/tools/tenancyagreement/f20.png', //Lease Expiry Date
					'22' => '/images/tools/tenancyagreement/f22.png', //Break Clause|Tenant#Landlord#Both
					'31' => '/images/tools/tenancyagreement/f31.png', //Section X, Add. 1 - Title
					'32' => '/images/tools/tenancyagreement/f32.png', //Section X, Add. 1 - Text
					'33' => '/images/tools/tenancyagreement/f33.png', //Section X, Add. 2 - Title
					'34' => '/images/tools/tenancyagreement/f34.png', //Section X, Add. 2 - Text
					'35' => '/images/tools/tenancyagreement/f35.png', //Section X, Add. 3 - Title
					'36' => '/images/tools/tenancyagreement/f36.png', //Section X, Add. 3 - Text
					'37' => '/images/tools/tenancyagreement/f37.png', //Section X, Add. 4 - Title
					'38' => '/images/tools/tenancyagreement/f38.png', //Section X, Add. 4 - Text
					'39' => '/images/tools/tenancyagreement/f39.png', //Section X, Add. 5 - Title
					'40' => '/images/tools/tenancyagreement/f40.png', //Section X, Add. 5 - Text
				),
				'showhide' => array(
					// '1_0' => '/images/tools/tenancyagreement/1_0.png',
					// '2_0' => '/images/tools/tenancyagreement/2_0.png',
					'1_0' => '',
					'2_0' => '',
					'3_0' => '',
					'4_0' => '',
					'5_0' => '',
					'6_0' => '',
					'7_0' => '',
				),
				'replace' => array(
					'section-i-bottom' => '/images/tools/tenancyagreement/section-i-bottom.png',
					'section-ii-1' => '/images/tools/tenancyagreement/section-ii-1.png',
					'section-ii-2' => '/images/tools/tenancyagreement/section-ii-2.png',
					'section-ii-3' => '/images/tools/tenancyagreement/section-ii-3.png',
					'section-iii-1-i' => '/images/tools/tenancyagreement/section-iii-1-i.png',
					'section-iii-1-ii' => '/images/tools/tenancyagreement/section-iii-1-ii.png',
					'section-iii-1-iii' => '/images/tools/tenancyagreement/section-iii-1-iii.png',
					'section-iii-1-iv' => '/images/tools/tenancyagreement/section-iii-1-iv.png',
					'section-iii-2' => '/images/tools/tenancyagreement/section-iii-2.png',
					'section-iv-1' => '/images/tools/tenancyagreement/section-iv-1.png',
					'section-iv-2' => '/images/tools/tenancyagreement/section-iv-2.png',
					'section-iv-3' => '/images/tools/tenancyagreement/section-iv-3.png',
					'section-iv-4' => '/images/tools/tenancyagreement/section-iv-4.png',
					'section-iv-5' => '/images/tools/tenancyagreement/section-iv-5.png',
					'section-iv-6' => '/images/tools/tenancyagreement/section-iv-6.png',
					'section-iv-7' => '/images/tools/tenancyagreement/section-iv-7.png',
					'section-v-1' => '/images/tools/tenancyagreement/section-v-1.png',
					'section-v-2-i' => '/images/tools/tenancyagreement/section-v-2-i.png',
					'section-v-2-ii' => '/images/tools/tenancyagreement/section-v-2-ii.png',
					'section-v-3' => '/images/tools/tenancyagreement/section-v-3.png',
					'section-v-4' => '/images/tools/tenancyagreement/section-v-4.png',
					'section-vi-1-a' => '/images/tools/tenancyagreement/section-vi-1-a.png',
					'section-vi-1-b' => '/images/tools/tenancyagreement/section-vi-1-b.png',
					'section-vi-1-c' => '/images/tools/tenancyagreement/section-vi-1-c.png',
					'section-vi-2' => '/images/tools/tenancyagreement/section-vi-2.png',
					'section-vi-3' => '/images/tools/tenancyagreement/section-vi-3.png',
					'section-vi-4' => '/images/tools/tenancyagreement/section-vi-4.png',
					'section-vi-5' => '/images/tools/tenancyagreement/section-vi-5.png',
					'section-vi-6' => '/images/tools/tenancyagreement/section-vi-6.png',
					'section-vii-1' => '/images/tools/tenancyagreement/section-vii-1.png',
					'section-vii-2' => '/images/tools/tenancyagreement/section-vii-2.png',
					'section-viii-1' => '/images/tools/tenancyagreement/section-viii-1.png',
					'section-viii-2' => '/images/tools/tenancyagreement/section-viii-2.png',
					'section-ix-1' => '/images/tools/tenancyagreement/section-ix-1.png',
					'section-ix-2' => '/images/tools/tenancyagreement/section-ix-2.png',
					'section-ix-3' => '/images/tools/tenancyagreement/section-ix-3.png',
					'section-ix-3-a' => '/images/tools/tenancyagreement/section-ix-3-a.png',
					'section-ix-3-b' => '/images/tools/tenancyagreement/section-ix-3-b.png',
					'section-ix-3-c' => '/images/tools/tenancyagreement/section-ix-3-c.png',
					'section-ix-3-d' => '/images/tools/tenancyagreement/section-ix-3-d.png',
					'section-x-1' => '/images/tools/tenancyagreement/section-x-1.png',
					'section-x-2' => '/images/tools/tenancyagreement/section-x-2.png',
					'section-x-3' => '/images/tools/tenancyagreement/section-x-3.png',
					'section-x-4' => '/images/tools/tenancyagreement/section-x-4.png',
					'section-x-5' => '/images/tools/tenancyagreement/section-x-5.png',
					'section-x-6' => '/images/tools/tenancyagreement/section-x-5.png',
					'break' => '/images/tools/tenancyagreement/break.png',
					'part-iii-terms' => '/images/tools/tenancyagreement/break.png'
				)
			);
		}
		
        return ''; 
    }
	
	
    public static function getLandlordFeeLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Landlord Name/Company
					5|textarea|Landlord Address
					6|text|Address (Dear xxx)
					7|text|Premises
					8|number|xxx% of 1 Month\'s Rent
					9|text|Special Bonus|e.g. , in addition to a special promotional cash bonus of 15% of the net monthly rental
					10|number|Fee Total
					11|text|Consultant Name
					12|text|Consultant Role/Title
					13|text|Consultant License Number
					';
					//13 highest
		}
		
        return ''; 
    }
	public static function getLandlordFeeLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getLandlordFeeLetterReplace($version){
        if ($version == 1){
			return array(
				'maintext' => 'Main Text',
			);
		}
		
        return ''; 
    }
	public static function getLandlordFeeReplaceAutoText($version){
        if ($version == 1){
			return array(
				'maintext' => 'In respect of the above premises, we would like to reconfirm that the Landlord shall pay an agency fee to Nest Property equal to [[ !!! ]]% of one month’s rent [[ !!! ]] upon the successful signing of the Tenancy Agreement. Cheques should be made payable to “NEST PROPERTY”. For the avoidance of doubt the agency fee for the above-mentioned property, should the fee stated in the Offer Letter be accepted, totals HK$[[ !!! ]].'
			);
		}
		
        return ''; 
    }
	
    public static function getTenantFeeLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Tenant Name/Company
					5|textarea|Tenant Address
					6|text|Address (Dear xxx)
					7|text|Premises
					8|number|xxx% of 1 Month\'s Rent
					9|text|Special Bonus|e.g. , in addition to a special promotional cash bonus of 15% of the net monthly rental
					10|number|Fee Total
					11|text|Consultant Name
					12|text|Consultant Role/Title
					13|text|Consultant License Number
					';
					//13 highest
		}
		
        return ''; 
    }
	public static function getTenantFeeLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getTenantFeeLetterReplace($version){
        if ($version == 1){
			return array(
				'maintext' => 'Main Text',
			);
		}
		
        return ''; 
    }
	public static function getTenantFeeReplaceAutoText($version){
        if ($version == 1){
			return array(
				'maintext' => 'In respect of the above premises, we would like to reconfirm that the Tenant shall pay an agency fee to Nest Property equal to [[ !!! ]]% of one month’s rent [[ !!! ]] upon the successful signing of the Tenancy Agreement. Cheques should be made payable to “NEST PROPERTY”. For the avoidance of doubt the agency fee for the above-mentioned property, should the fee stated in the Offer Letter be accepted, totals HK$[[ !!! ]].'
			);
		}
		
        return ''; 
    }
	
    public static function getLetterOfUndertakingFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|Premises
					3|text|Landlord Name
					4|text|Tenant Name
					';
					//4 highest
		}
		
        return ''; 
    }
	public static function getLetterOfUndertakingShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getLetterOfUndertakingReplace($version){
        if ($version == 1){
			return array(
				'maintext' => 'Main Text',
			);
		}
		
        return ''; 
    }
	public static function getLetterOfUndertakingReplaceAutoText($version){
        if ($version == 1){
			return array(
				'maintext' => 'The Landlord hereby declares that the Said Premises has been mortgaged to secure for repayment of certain advancement made by the Mortgage Bank or Financial Institution.  The Tenant hereby agrees that the Landlord shall not be obliged to, and the Tenant hereby waives the requirement that the Landlord shall, obtain the consent of the mortgagee of the Premises (“the Mortgagee”) to the signing of this Agreement.  In the event that the Mortgagee shall commence a mortgagee action against the Landlord to re-possess the Said Premises or shall otherwise exert its title to the Said Premises against the Tenant, the Tenant shall be entitled to terminate this Agreement forthwith and recover the Deposit.  In such event, the Landlord agrees to indemnify the Tenant against any loss or damage incurred by the Tenant in connection with this Agreement.  The Landlord will be responsible for the Tenant’s relocation expenses including but not limited to agency fees, legal fees, stamp duty etc.'
			);
		}
		
        return ''; 
    }
	
    public static function getHoldingDepositLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|Landlord Name/Company
					3|textarea|Landlord Address
					4|text|Address (Dear xxx)
					5|text|Premises
					6|number|Fee Total
					7|text|Consultant Name
					8|text|Consultant Role/Title
					9|text|Consultant License Number
					';
					//9 highest
		}
		
        return ''; 
    }
	public static function getHoldingDepositLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getHoldingDepositLetterReplace($version){
        if ($version == 1){
			return array(
				'maintext' => 'Main Text',
			);
		}
		
        return ''; 
    }
	public static function getHoldingDepositLetterReplaceAutoText($version){
        if ($version == 1){
			return array(
				'maintext' => 'We herewith enclose a cheque for the sum of HK$[[ !!! ]] from the Tenant (cheque no:______________________) being the initial deposit for the above-mentioned premises. As stipulated in our offer letter, this holding deposit shall be refunded to the Tenant in full should either party fail to sign the Tenancy Agreement.'
			);
		}
		
        return ''; 
    }
	
    public static function getUtilityAccountsFields($version){
        if ($version == 1){
			return '21|radio|Tenant Type|Tenant#Occupant
					1|text|Name
					22|radio|Document Type|HKID No.#Passport No.#Business Registration No.
					2|text|Document No.
					3|text|Property Address
					4|text|Lease Term
					5|text|Contact Tel
					6|text|Water Account Name
					7|text|Water Billing Address
					8|text|Water Account Nr. 
					9|date|Water Effective Date
					10|text|Water Deposit
					11|text|Electricity Account Name
					12|text|Electricity Billing Address
					13|text|Electricity Account Nr. 
					14|date|Electricity Effective Date
					15|text|Electricity Deposit
					16|text|Gas Account Name
					17|text|Gas Billing Address
					18|text|Gas Account Nr. 
					19|date|Gas Effective Date
					20|text|Gas Deposit
					';
					//22 highest
		}
		
        return ''; 
    }
	public static function getUtilityAccountsShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '1|0|1|Water Section
					2|0|1|Electricity Section
					3|0|1|Gas Section';
		}
		
        return ''; 
    }
	public static function getUtilityAccountsReplace($version){
        if ($version == 1){
			return array();
		}
		
        return ''; 
    }
	
    public static function getHandoverReportFields($version){
        if ($version == 1){
			return '1|text|Address
					2|text|Car Park No.
					3|text|Vehicle Reg. No.
					4|text|Taken By
					5|date|Handover Date
					6|date|Lease Term: Commencement Date
					7|date|Lease Term: Expiry Date
					8|number|Lease Term: Monthly Rental 
					9|number|Lease Term: Management Fee
					10|number|Lease Term: Government Rates
					11|number|Lease Term: Government Rent
					29|radio|Lease Term: Air-Conditioner Maintenance Charges|show#hide
					12|number|Lease Term: Air-Conditioner Maintenance Charges
					13|text|Rental Payment: Name of Bank 
					14|text|Rental Payment: Bank A/C No.
					15|text|Rental Payment: A/C Name
					30|radio|Tenant|person#company
					16|text|Tenant: Name
					17|text|Tenant: Address
					18|text|Tenant: Occupant
					21|text|Tenant: Contact Person
					19|text|Tenant: Tel
					20|text|Tenant: Email
					31|radio|Landlord|person#company
					23|text|Landlord: Name
					24|text|Landlord: Address
					27|text|LL / Contact Person
					25|text|LL / Contact Person Tel
					28|text|LL / Contact Person Email
					32|radio|First Page Gaps|big#small
					';
					//32 highest
		}
		
        return ''; 
    }
	public static function getHandoverReportShowHide($version){
        if ($version == 1){
			return '1|0|1|Living / Dining Area
					2|0|1|Kitchen
					3|0|1|Hallway
					4|0|1|Master Bedroom
					5|0|1|Ensuite Bathroom
					6|0|1|Bedroom 1
					7|0|1|Bedroom 2
					8|0|1|Bedroom 3
					17|0|1|Bedroom 4
					18|0|1|Bedroom 5
					9|0|1|Study Room
					10|0|1|Guest Bathroom / Bathroom 1
					11|0|1|Guest Bathroom / Bathroom 2
					12|0|1|Guest Bathroom / Bathroom 3
					19|0|1|Guest Bathroom / Bathroom 4
					20|0|1|Guest Bathroom / Bathroom 5
					13|0|1|Utility Area
					14|0|1|Helper’s Bedroom
					15|0|1|Helper’s Bathroom
					16|0|1|Remarks
					21|0|1|Utilities Table
					22|0|1|Line Break before Last Page (Ticking this might help with empty page after "Defects/Remarks")
					';
		}
		
        return ''; 
    }
	public static function getHandoverReportReplace($version){
        if ($version == 1){
			return array();
		}
		
        return ''; 
    }
	
	
	
    public static function getHandoverReportExtFields($version){
        if ($version == 1){
			return '1|text|Address
					2|text|Car Park No.
					3|text|Vehicle Reg. No.
					4|text|Taken By
					5|date|Handover Date
					6|date|Lease Term: Commencement Date
					7|date|Lease Term: Expiry Date
					8|number|Lease Term: Monthly Rental 
					9|number|Lease Term: Management Fee
					10|number|Lease Term: Government Rates
					11|number|Lease Term: Government Rent
					29|radio|Lease Term: Air-Conditioner Maintenance Charges|show#hide
					12|number|Lease Term: Air-Conditioner Maintenance Charges
					13|text|Rental Payment: Name of Bank 
					14|text|Rental Payment: Bank A/C No.
					15|text|Rental Payment: A/C Name
					30|radio|Tenant|person#company
					16|text|Tenant: Name
					17|text|Tenant: Address
					18|text|Tenant: Occupant
					21|text|Tenant: Contact Person
					19|text|Tenant: Tel
					20|text|Tenant: Email
					31|radio|Landlord|person#company
					23|text|Landlord: Name
					24|text|Landlord: Address
					27|text|LL / Contact Person
					25|text|LL / Contact Person Tel
					28|text|LL / Contact Person Email
					32|radio|First Page Gaps|big#small
					33|json|Custom Rooms JSON
					';
					//32 highest
		}
		
        return ''; 
    }
	public static function getHandoverReportExtShowHide($version){
        if ($version == 1){
			return '16|0|1|Remarks
					21|0|1|Utilities Table
					';
		}
		
        return ''; 
    }
	public static function getHandoverReportExtReplace($version){
        if ($version == 1){
			return array();
		}
		
        return ''; 
    }
	
	
    public static function getCoverLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Address (Dear xxx)
					5|text|Premises
					6|textareadrag|List of Documents
					7|text|Phone Number
					8|text|Consultant Name
					9|text|Consultant Role/Title
					10|text|Consultant License Number
					';
					//10 highest
		}
		
        return ''; 
    }
	public static function getCoverLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getCoverLetterReplace($version){
        if ($version == 1){
			return array(
				'introsentence' => 'Introduction Sentence',
				'maintext' => 'Main Text',
				'finalsentence' => 'Final Sentence',
			);
		}
		
        return ''; 
    }
	public static function getCoverLetterReplaceAutoText($version){
        if ($version == 1){
			return array(
				'introsentence' => 'Please find enclosed the following documents for your attention:',
				'maintext' => 'Kindly sign / counter-sign and return all the above documents to us. Or in the interest of time, you may inform either myself, or someone in our office, once all documents are ready for collection, and we can send a courier to pick them up from your home.',
				'finalsentence' => 'Should you should have any questions regarding the above, feel free to call me on [[ !!! ]].',
			);
		}
		
        return ''; 
    }
	
	
    public static function getSideLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Address (Dear xxx)
					5|text|Premises
					6|date|Date of Tenancy Agreement
					7|textarea|Main Text Section
					8|text|Landlord Name
					9|text|Tenant Signature Name
					';
					//9 highest
		}
		
        return ''; 
    }
	public static function getSideLetterShowHide($version){
        if ($version == 1){
			return '1|0|1|Introduction Sentence
					2|0|1|Final Sentences
					';
		}
		
        return ''; 
    }
	public static function getSideLetterReplace($version){
        if ($version == 1){
			return array(
				'introsentence' => 'Introduction Sentence',
				'finalsentence' => 'Final Sentences',
			);
		}
		
        return ''; 
    }
	public static function getSideLetterReplaceAutoText($version){
        if ($version == 1){
			return array(
				'introsentence' => 'We refer to the tenancy agreement dated [[ !!! ]] made between us as Landlord and your good self as Tenant in respect of the Property (“the Tenancy Agreement”). ',
				'finalsentence' => 'This Side Letter is supplemental to the Tenancy Agreement. Save for the above variation or modification, the terms and provisions of the Tenancy Agreement shall remain unchanged and continue be in full force and effect. ',
			);
		}
		
        return ''; 
    }
	
	
    public static function getCustomLetterFields($version){
        if ($version == 1){
			return '1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Address (Dear xxx)
					5|text|Premises
					6|textarea|Main Content
					7|radio|Signature Type|No Signature#Consultant#1 Party#2 Parties
					8|text|Consultant Name
					9|text|Consultant Role/Title
					10|text|Consultant License Number
					11|text|Party 1 Name
					12|text|Party 2 Name
					13|textarea|Custom Address
					';
					//12 highest
		}
		//Consultant:Holding Deposit Letter
		//2 Parties: Letter of Undertaking
		
        return ''; 
    }
	public static function getCustomLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getCustomLetterReplace($version){
        if ($version == 1){
			return array(
				
			);
		}
		
        return ''; 
    }

	public static $offer_letter_validations = array(
		'f7' => 'required',
		'f8' => 'required',
		'f9' => 'required',
		'f58' => 'required',
		'f12' => 'required'
	);

	public static $offer_letter_messages = array(
		'f7.required' => 'Full Name is required',
		'f8.required' => 'Work Title, Company is required',
		'f9.required' => 'Address 1 is required',
		'f58.required' => 'Tenant Document Type is required',
		'f12.required' => 'HKID/Passport/BRI is required'
	);
	
	
	
}










