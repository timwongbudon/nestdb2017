<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Illuminate\Support\Facades\DB;

use App\LogLead;
use App\FormContact;
use App\FormProperty;
use App\Calendar;
use App\Commission;
use App\Comminvoice;
use App\Infoofficermessage;

class Lead extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'leads';
	
    protected $fillable = ['id', 'clientid', 'shortlistid', 'consultantid', 'shared_with', 'status', 'lastoutreach', 'remindme', 'createdby', 'userid', 'dateinitialcontact', 'sourcehear', 'type', 'clienttype', 'req_beds', 'req_baths', 'req_maidrooms', 'req_outdoor', 'req_carparks', 'req_facilites', 'offerprice', 'budget', 'budgetto', 'likes', 'currentflat', 'reasonformove', 'noticeperiod', 'preferredareas', 'comments', 'websiteid', 'contactid', 'googledrivelink'];
	
	public function after_save() {
		$this->logLead();
	}
	
    public function client() {
       return $this->belongsTo(Client::class, 'clientid'); 
    }
	
    public function consultant() {
       return $this->belongsTo(User::class, 'consultantid'); 
    }
	
	
	public function logLead(){
		$log = new LogLead();	
		
		$log->id = $this->id;
		$log->clientid = $this->clientid;
		$log->shortlistid = $this->shortlistid;
		$log->consultantid = $this->consultantid;
		$log->status = $this->status;
		$log->lastoutreach = $this->lastoutreach;
		$log->remindme = $this->remindme;
		$log->createdby = $this->createdby;
		$log->userid = $this->userid;
		$log->dateinitialcontact = $this->dateinitialcontact;
		$log->sourcehear = $this->sourcehear;
		$log->type = $this->type;
		$log->clienttype = $this->clienttype;		
		$log->req_beds = $this->req_beds;
		$log->req_baths = $this->req_baths;
		$log->req_maidrooms = $this->req_maidrooms;
		$log->req_outdoor = $this->req_outdoor;
		$log->req_carparks = $this->req_carparks;
		$log->req_facilites = $this->req_facilites;
		$log->budget = $this->budget;
		$log->budgetto = $this->budgetto;
		$log->likes = $this->likes;
		$log->currentflat = $this->currentflat;
		$log->reasonformove = $this->reasonformove;
		$log->noticeperiod = $this->noticeperiod;
		$log->preferredareas = $this->preferredareas;
		$log->comments = $this->comments;
		$log->websiteid = $this->websiteid;
		$log->contactid = $this->contactid;
		$log->googledrivelink = $this->googledrivelink;
		
		$log->save();
	}
	
	public static function allstatus(){
		return array(
			0 => 'New',
			1 => 'Hot',
			2 => 'Warm',
			3 => 'Cold',
			9 => 'Cupping Room',
			13 => 'Sales',
			10 => 'Relocation',
			12 => 'Lease Renewal',
			//4 => 'Stage 3',
			5 => 'Offering',
			6 => 'Invoicing',
			7 => 'Completed',
			8 => 'Dead',
			11 => 'Deleted',
		);
	}
	
	public static function allsources(){
		return array(
			1 => 'Web Enquiry',
			2 => 'Referred by Client or Friend',
			8 => 'Previous Client',
			//3 => 'Minibus',
			4 => 'Social Media',
			//5 => 'Instagram',
			//6 => 'Ads',
			7 => 'Phone Enquiry',
			9 => 'EDM',
			10=> 'Spacious'
		);
	}
	
	public function getLog(){
		return LogLead::where('id', $this->id)->orderBy('logid', 'desc')->get();
	}
	
	public function typeOut(){
		if ($this->type == 2){
			return 'Sale';
		}else{
			return 'Lease';
		}
	}
	
	public function budgetOut(){
		$budget = intval($this->budget);
		$budgetto = intval($this->budgetto);
		
		if ($budget > 1000000){
			$budget = '$'.round(($budget/1000000), 1).'M';
		}else if ($budget > 1000){
			$budget = '$'.round(($budget/1000), 1).'K';
		}
		if ($budgetto > 1000000){
			$budgetto = '$'.round(($budgetto/1000000), 1).'M';
		}else if ($budgetto > 1000){
			$budgetto = '$'.round(($budgetto/1000), 1).'K';
		}
		
		if (intval($this->budget) > 0 && intval($this->budgetto) > 0){
			return $budget.' - '.$budgetto;
		}else if (intval($this->budgetto) > 0){
			return $budgetto;
		}else if (intval($this->budget) > 0){
			return $budget;
		}else{
			return '';
		}
	}
	
	public function statusOut(){
		$s = $this->allstatus();
		if (isset($s[$this->status])){
			return $s[$this->status];
		}
	}
	
	public function created_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->created_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
	public function updated_at(){
		$d = date_create_from_format('Y-m-d H:i:s', $this->updated_at);
		return date_format($d, 'd/m/Y H:i');
	}
	
	public function getNiceDateinitialcontact(){
		if ($this->dateinitialcontact != null && $this->dateinitialcontact != '0000-00-00'){
			$d = date_create_from_format('Y-m-d', $this->dateinitialcontact);
			return date_format($d, 'd/m/Y');
		}else{
			return null;
		}
	}
	
	public function getNiceLastoutreach(){
		if ($this->lastoutreach != null && $this->lastoutreach != '0000-00-00'){
			$d = date_create_from_format('Y-m-d', $this->lastoutreach);
			return date_format($d, 'd/m/Y');
		}else{
			return '';
		}
	}
	
	public function getNiceRemindMe(){
		if ($this->remindme != null && $this->remindme != '0000-00-00'){
			$d = date_create_from_format('Y-m-d', $this->remindme);
			return date_format($d, 'd/m/Y');
		}else{
			return '';
		}
	}
	
	public function getWebsiteFormName(){
		$name = 'Contact via Website';
		
		if ($this->websiteid > 0){
			$form = FormProperty::where('id', '=', $this->websiteid)->first();
			$name = $form->name;
		}else if ($this->contactid > 0){
			$form = FormContact::where('id', '=', $this->contactid)->first();
			$name = $form->name;
		}
		
		return $name;
	}
	
	public static function headerdata(){
		
		$ret = array(
			'warnings' => 0,
			'new' => 0
		);
		
		if (!empty(\Auth::user())){
			$leads = Lead::where('status', '<', 7)->where('consultantid', '=', \Auth::user()->id)->with('client')->get();
			foreach ($leads as $l){
				if ($l->status == 0){
					$ret['new']++;
				}else if ($l->status == 2 || $l->status == 3 || $l->status == 4){
					if ($l->lastoutreach == null || $l->lastoutreach == '0000-00-00' || $l->lastoutreach <= date('Y-m-d', time() - 7*24*60*60)){
						$ret['warnings']++;
					}
				}
				if ($l->remindme != null && $l->remindme != '0000-00-00' && $l->remindme <= date('Y-m-d')){
					$ret['warnings']++;
				}
			}
			
			$ret['websiteform'] = 0;
			$ret['messages'] = 0;
			$ret['vacation'] = 0;
			$ret['invoice'] = 0;
			$ret['invoices'] = 0;
			$ret['invoicenotes'] = 0;
			
			if (\Auth::user()->getUserRights()['Superadmin'] || \Auth::user()->getUserRights()['Director']){
				$contactforms = FormContact::where('status', '=', 0)->get();
				$propertyforms = FormProperty::where('status', '=', 0)->get();
				$landlordforms = FormContact::where('status', '=', 3)->where('landlorduser', '=', \Auth::user()->id)->get();
				$ret['websiteform'] += count($contactforms);
				$ret['websiteform'] += count($propertyforms);
				$ret['websiteform'] += count($landlordforms);
				$cal = Calendar::where('type', 3)->where('accepted', 0)->get();
				$ret['vacation'] += count($cal);
			}else if (\Auth::user()->getUserRights()['Informationofficer']){
				$landlordforms = FormContact::where('status', '=', 3)->where('landlorduser', '=', \Auth::user()->id)->get();
				$ret['websiteform'] += count($landlordforms);		
			}
			if (\Auth::user()->getUserRights()['Informationofficer']){
				$infoofficermessages = Infoofficermessage::where('status', '=', 0)->get();
				$ret['messages'] += count($infoofficermessages);
			}
			if (\Auth::user()->getUserRights()['Superadmin'] || \Auth::user()->getUserRights()['Accountant']){
				$comms = Commission::where('status', '=', 1)->get();
				$ret['invoice'] += count($comms);	
				$cal = Calendar::where('type', 3)->where('accepted', 1)->get();
				$ret['vacation'] += count($cal);
			}
			if (\Auth::user()->getUserRights()['Superadmin'] || \Auth::user()->getUserRights()['Director'] || \Auth::user()->getUserRights()['Accountant']){
				$comminv = Comminvoice::whereIn('status', [0,2])->get();
				$ret['invoices'] += count($comminv);
			}
			$comminv = Comminvoice::whereIn('status', [1])->where('consultantid', \Auth::user()->id)->get();
			$ret['invoicenotes'] += count($comminv);
			
			$forwardedforms = FormContact::where('status', '=', 3)->where('forwarduser', '=', \Auth::user()->id)->get();
			$ret['messages'] += count($forwardedforms);	
			$forwardedforms = FormProperty::where('status', '=', 3)->where('forwarduser', '=', \Auth::user()->id)->get();
			$ret['messages'] += count($forwardedforms);	
		}
		
		return $ret;
	}
	
	
}