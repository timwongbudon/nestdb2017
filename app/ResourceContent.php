<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceContent extends Model
{
    protected $fillable = ['name', 'key', 'content'];

    const KEY_PERSONAL = 'personal';

    const KEY_USEFUL_LINKS = 'useful_links';
}
