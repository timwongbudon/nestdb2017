<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Import extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'filename', 'comments', 'encoded_string', 'preference', 'web'];

    public static $rules = array(
            'name'     => 'max:255',
            'filename' => 'required|max:255',
        );

    public static function available_options() {
    	$fields = array(
                ''                => '-',
                'external_id'     => 'External reference ID',
                'name'            => 'Building Name',
                'unit'            => 'Unit',
                'type_id'         => 'Property Type ID (Rent=1/Sale=2/Both=3)',
                'address1'        => 'Address',
                'address2'        => 'District',
                'district_id'     => 'District ID',
                'country_code'    => 'Country Code (i.e. hk)',
                'coordinates'     => 'Coordinates for web',
                // 'floor_zone'   => 'Floor Zone',
                'bedroom'         => 'Bedroom Number',
                'bathroom'        => 'Bathroom Number',
                'maildroom'       => 'Maidroom Number',
                'gross_area'      => 'Gross Area',
                'saleable_area'   => 'Saleable Area',
                'asking_rent'     => 'Asking Rent',
                'asking_sale'     => 'Asking Sale',
                'inclusive'       => 'Is Inclusive (1/0)',
                'management_fee'  => 'Management Fee',
                'government_rate' => 'Government Rate',
                'key_loc_id'      => 'Key Location (Managment Office=1,Door Code=2, Agency=3, Other=4',
                'key_loc_other'   => 'Key Location Other',
                'door_code'       => 'Door Code',
                'agency_fee'      => 'Agency Fee',
                'comments'        => 'Comments',
                'contact_info'    => 'Contact Info.',
                'created_at'      => 'Created Date',
                'available_date'  => 'Available Date',
                'car_park'        => 'Car Park Number',
                'leased'          => 'Status - Is Leased (1/0)',
                'slug'            => 'Slug',
                'outdoor'         => 'Outdoor Number',
                'featured'        => 'Featured',
                'featured_image'  => 'Featured Image URL',
                'nest_photos[]'   => 'Nest Photo URL (accept multiple)',
                'other_photos[]'  => 'Other Photo URL (accept multiple)',
                'features'        => 'Feature IDs',
    		);
    	return $fields;
    }
}