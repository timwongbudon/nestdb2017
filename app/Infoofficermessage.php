<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Carbon\Carbon;

class Infoofficermessage extends BaseModel 
{
    use SoftDeletes;

    protected $table = 'infoofficermessage';
	
    protected $fillable = ['id', 'ts', 'status', 'comments', 'userid', 'nestpictureupload', 'otherpictureupload', 'hkhomeslink', 'propertysource', 'pocname', 'pocemail', 'poctel', 'dcheckdb'];
	public $key_id = 'id';
	
	
	public function getDateTime(){
		return Carbon::createFromFormat('Y-m-d H:i:s', $this->ts, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y H:i');
	}
	
}