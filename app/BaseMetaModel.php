<?php

namespace App;

class BaseMetaModel extends BaseModel 
{ 
    public $key_id = 'OBJID';

    public function get_post_meta($post_id=null, $meta_key=null, $unique=false) {
        if($unique) {
            $meta = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->first();
            return !empty($meta)?$meta->meta_value:null;
        } else {
            $res = array();
            $metas = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->get();
            if (!empty($metas)) {
                foreach ($metas as $meta) {
                    $res[] = $meta->meta_value;
                }
            }
            return $res;
        }
    }

	public function add_post_meta($post_id=null, $meta_key=null, $meta_value=null, $unique=false) {
        if (empty($post_id) || empty($meta_key) || empty($meta_value)) {
            return false;
        }
        $data = array(
                $this->key_id => $post_id,
                'meta_key'    => $meta_key,
                'meta_value'  => $meta_value,
            );
        if ($unique) {
            $this->delete_post_meta($post_id, $meta_key);
        }
        return $this->create($data);
    }

    public function update_post_meta($post_id=null, $meta_key=null, $meta_value=null, $prev_value=null) {
        if (empty($post_id) || empty($meta_key)) {
            return false;
        }
        if($prev_value!==null) {
            $objects = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->where('meta_value', $prev_value)->get();
        }else{
            $objects = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->get();
        }
        $count = 0;
        if (!empty($objects)) {
            foreach ($objects as $object) {
                $object->meta_value = $meta_value;
                $res = $object->save();
                if ($res) $count++;
            }
        }
        return $count;
    }

    public function delete_post_meta($post_id=null, $meta_key=null, $meta_value=null) {
        if (empty($post_id) || empty($meta_key)) {
            return false;
        }
        if($meta_value!==null) {
            $count = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->where('meta_value', $meta_value)->delete();
            return $count;
        }else{
            $count = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->delete();
            return $count;
        }
    }

    /**
     * Helpers
     */
    public function is_unique_meta($post_id=null, $meta_key=null) {
        $count = $this->where($this->key_id, $post_id)->where('meta_key', $meta_key)->count();
        return $count===1;
    }
}
