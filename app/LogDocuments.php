<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogDocuments extends BaseMediaModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'zlog_documents';
    
    protected $fillable = ['id', 'doctype', 'docversion', 'shortlistid', 'consultantid', 'clientid', 'propertyid', 'fieldcontents', 'sectionshidden', 'replace', 'revision'];
	
    public $key_id = 'logid';
	
	
	
}
