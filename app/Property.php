<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use App\LogProperties;
use App\PropertyDeleteLogs;

class Property extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'display_name', 'building_id', 'unit', 'type_id', 'address1', 
    					'address2', 'district_id', 'country_code', 'floor_zone', 'layout',
    					'description', 'bedroom', 'bathroom', 'maidroom', 'year_built',
    					'gross_area', 'saleable_area', 'asking_rent', 'asking_sale', 'inclusive',
    					'management_fee', 'government_rate',
                        'poc_id', 'owner_id', 'agent_id', 'rep_ids', 'agent_ids', 'web', 
                        'car_park', 'outdoor', 'hkh_url', 'hkhnew_url', 'leased','sole_agency','information_officer','direct_landlord', 'featured', 'slug',
                        'key_loc_id', 'key_loc_other', 'door_code', 'agency_fee', 'comments', 'hot',
                        'contact_info', 'available_date', 'external_id', 'source', 'user_id', 'created_at', 
						'sold', 'updated_at', 'feat_order', 'tst', 'pictst', 'webtst', 'floornr', 
						'bonuscomm', 'commission', 'otherfeatures', 'otherfacilities', 'webtext', 'nosaleprice', 'photoshoot'];

    public static $rules = array(
			'name'     => 'required|max:255',
			'type_id'  => 'required|numeric',
			// 'unit'     => 'required|max:255',
			'address1' => 'required|max:255',
			// 'address2' => 'required|max:255',
    	);

    public static $messages = array(
            'name.required' => 'The property name field is required.',
        );

    public $type_ids = array(
            '1' => 'Rent',
            '2' => 'Sale',
            '3' => 'Rent & Sale',
        );

    public $poc_ids = array(
            '1' => 'Owner',
            '2' => 'Rep',
            '3' => 'Agent',
        );

    public static $media_ids = array(
            'nest-photo' => 'Nest Photo',
            'nest-doc'   => 'Nest Document',
            'other-photo'  => 'Other Photo',
        );

    public static $hot_option_ids = array(
            ''  => 'Hot and Other Properties',
            '1' => 'Shows Hot Properties',
        );

    public static $photo_option_ids = array(
            ''  => 'Any Photos',
            '1' => 'With Nest Photo',
            '2' => 'Without Nest Photo',
        );

    public static $web_option_ids = array(
            ''  => 'Frontend And Other Properties',
            '1' => 'Frontend Property',
            '2' => 'Non Frontend Property',
    	);

    public static function outdoor_ids() {
        $features = array(
            '1' => 'Terrace',
            '2' => 'Balcony',
            '3' => 'Rooftop',
            '4' => 'Garden',
            '5' => 'Patio',
        );
        return $features;
    }

    protected $searchable = ['id', 'name', 'unit', 'type_id', 'address1', 
                        'country_code', 'district_id', 'description',
                        'price_min', 'price_max', 'size_min', 'size_max',
                        'bedroom_min', 'bedroom_max', 'bathroom_min', 'bathroom_max',
                        'districts',
                        'outdoors', 'features', 'comments',
                        'vendor_id', 'rep_ids',
                        'excluded_id', 'included_id'];
    
    public static function default_listing_order() {
        $order = array(
                'date_priority'  => ['priority'=>0, 'direction'=>'asc'],
                'date_priority2' => ['priority'=>0, 'direction'=>'desc'],
                'properties.id'  => ['priority'=>0, 'direction'=>'desc'],
        );
        return $order;
    }

    public static function custom_listing_options() {
        $order = array(
                'date-asc'      => 'Closed to Available Date (Default)',
                // 'date-desc'     => 'Date Desc',
                'created-desc'  => 'Created Desc',
                'created-asc'   => 'Created Asc',
                'modified-desc' => 'Modified Desc',
                'modified-asc'  => 'Modified Asc',
                'rental-desc'   => 'Asking Rent Desc',
                'rental-asc'    => 'Asking Rent Asc',
                'sale-desc'     => 'Asking Sale Desc',
                'sale-asc'      => 'Asking Sale Asc',
                'gross-desc'    => 'Gross Area Desc',
                'gross-asc'     => 'Gross Area Asc',
                'saleable-desc' => 'Saleable Area Desc',
                'saleable-asc'  => 'Saleable Area Asc',
                'id-desc'       => 'ID Desc',
                'id-asc'        => 'ID Asc',
        );
        return $order;
    }

    public static function custom_listing_order($order_str) {
        $order = array(
                'date-asc'    => ['date_priority' => ['priority'=>1, 'direction'=>'asc']],
                //'date-desc'   => ['date_priority' => ['priority'=>1, 'direction'=>'desc']],
				'date-desc'   => ['properties.updated_at' => ['priority'=>1, 'direction'=>'desc']],
                'created-desc'   => ['properties.created_at' => ['priority'=>1, 'direction'=>'desc']],
                'created-asc'    => ['properties.created_at' => ['priority'=>1, 'direction'=>'asc']],
                'modified-desc'   => ['properties.updated_at' => ['priority'=>1, 'direction'=>'desc']],
                'modified-asc'    => ['properties.updated_at' => ['priority'=>1, 'direction'=>'asc']],
                'rental-desc' => ['properties.asking_rent' => ['priority'=>1, 'direction'=>'desc']],
                'rental-asc'  => ['properties.asking_rent' => ['priority'=>1, 'direction'=>'asc']],
                'sale-desc'   => ['properties.asking_sale' => ['priority'=>1, 'direction'=>'desc']],
                'sale-asc'    => ['properties.asking_sale' => ['priority'=>1, 'direction'=>'asc']],
                'gross-desc'  => ['properties.gross_area' => ['priority'=>1, 'direction'=>'desc']],
                'gross-asc'   => ['properties.gross_area' => ['priority'=>1, 'direction'=>'asc']],
                'saleable-desc'  => ['properties.saleable_area' => ['priority'=>1, 'direction'=>'desc']],
                'saleable-asc'   => ['properties.saleable_area' => ['priority'=>1, 'direction'=>'asc']],
                'id-desc'     => ['properties.id' => ['priority'=>1, 'direction'=>'desc']],
                'id-asc'      => ['properties.id' => ['priority'=>1, 'direction'=>'asc']],
        );
        return isset($order[$order_str])?$order[$order_str]:false;
    }

    public function after_save() {
        $input = Input::all();
		if (isset($input['unit'])){
			$meta = new PropertyMeta();
			$outdoor = new PropertyOutdoor();
			$feature = new PropertyFeature();
			if(!isset($input['features']) || $input['features'] == 0){
				$nf = array();
			}else{
				$nf = $input['features'];
			}
			if (isset($input['outdoors']) && is_array($input['outdoors']) && count($input['outdoors']) > 0){
				foreach ($input['outdoors'] as $od){
					if ($od == 1){
						//Terrace / 1 / 12
						$nf[] = 12;
					}else if ($od == 2){
						//Balcony / 2 / 9
						$nf[] = 9;						
					}else if ($od == 3){
						//Rooftop / 3 / 10
						$nf[] = 10;						
					}else if ($od == 4){
						//Garden / 4 / 26
						$nf[] = 26;
					}
				}
			}
			if (isset($input['layout']) && $input['layout'] == 'Duplex'){
				$nf[] = 16;
			}
			if (isset($input['layout']) && $input['layout'] == 'Stand-Alone'){
				$nf[] = 23;
			}
			
			$encoded_features = json_encode($nf);
			$meta->add_post_meta($this->id, 'features', $encoded_features, true); 
			$feature->set_options($this->id, $nf);
			if (isset($input['outdoors'])){
				if($input['outdoors']==0)$input['outdoors']=array();
				$encoded_outdoors = json_encode($input['outdoors']);
				$meta->add_post_meta($this->id, 'outdoors', $encoded_outdoors, true); 
				$outdoor->set_options($this->id, $input['outdoors']);
			}
			if (isset($input['outdoorareas'])) {
				$encoded_outdoor_areas = json_encode($input['outdoorareas']);
				$meta->add_post_meta($this->id, 'outdoorareas', $encoded_outdoor_areas, true); 
			}
		}
		
		if ($this->tst > 0){
			$this->logProperty();
		}
    }
	
	public function logProperty(){
		$log = new LogProperties();	
		
		$log->id = $this->id;
		$log->name = $this->name;
		$log->display_name = $this->display_name;
		$log->building_id = $this->building_id;
		$log->unit = $this->unit;
		$log->floornr = $this->floornr;
		$log->type_id = $this->type_id;
		$log->address1 = $this->address1;
		$log->address2 = $this->address2;
		$log->district_id = $this->district_id;
		$log->country_code = $this->country_code;
		$log->floor_zone = $this->floor_zone;
		$log->layout= $this->layout;
		$log->description = $this->description;
		$log->bedroom = $this->bedroom;
		$log->bathroom = $this->bathroom;
		$log->maidroom = $this->maidroom;
		$log->year_built= $this->year_built;
		$log->gross_area = $this->gross_area;
		$log->saleable_area = $this->saleable_area;
		$log->asking_rent = $this->asking_rent;
		$log->asking_sale = $this->asking_sale;
		$log->nosaleprice = $this->nosaleprice;
		$log->inclusive= $this->inclusive;
		$log->management_fee = $this->management_fee;
		$log->government_rate= $this->government_rate;
		$log->poc_id = $this->poc_id;
		$log->owner_id = $this->owner_id;
		$log->agent_id = $this->agent_id;
		$log->rep_ids = $this->rep_ids;
		$log->agent_ids = $this->agent_ids;
		$log->web = $this->web;
		$log->car_park = $this->car_park;
		$log->outdoor = $this->outdoor;
		$log->hkh_url = $this->hkh_url;
		$log->leased = $this->leased;
		$log->featured = $this->featured;
		$log->slug = $this->slug;
		$log->key_loc_id = $this->key_loc_id;
		$log->key_loc_other = $this->key_loc_other;
		$log->door_code = $this->door_code;
		$log->agency_fee = $this->agency_fee;
		$log->comments = $this->comments;
		$log->hot = $this->hot;
		$log->bonuscomm = $this->bonuscomm;
		$log->commission = $this->commission;
		$log->contact_info = $this->contact_info;
		$log->available_date = $this->available_date;
		$log->external_id = $this->external_id;
		$log->source = $this->source;
		$log->user_id = $this->user_id;
		$log->created_at = $this->created_at;
		$log->sold = $this->sold;
		$log->updated_at = $this->updated_at;
		$log->feat_order = $this->feat_order;
		$log->otherfeatures = $this->otherfeatures;
		$log->otherfacilities = $this->otherfacilities;
		$log->webtext = $this->webtext;
		$log->tst = $this->tst;
		$log->pictst = $this->pictst;
		$log->webtst = $this->webtst;
		$log->userid = auth()->user()->id;
		
		$log->save();
	}
	
	public function getLog(){
		return LogProperties::where('id', $this->id)->orderBy('logid', 'desc')->get();
	}

    /**
     * API
     */
    public function api_output() {
        return array(
			'realId'		=> $this->id,
			'propertyId'    => $this->nest_id(),
			'name'          => $this->shorten_building_name(),
			'address'       => $this->print_address(),
			'district'      => $this->district(),
			'country'      => $this->country(),
			'typeId'        => $this->type_id,
			'type'          => $this->type(),
			// 'netSize'       => $this->property_saleablesize(),//to be deleted.
			'grossSize'     => $this->property_grosssize(),
			'saleableSize'  => $this->property_saleablesize(),
			'askingRent'    => $this->asking_rent(),
			'askingSale'    => $this->asking_sale(),
			'price'         => $this->price(),
			'priceinclexcl' => $this->priceinclexcl(),
			'bedroomCount'  => $this->bedroom_count(),
			'bathroomCount' => $this->bathroom_count(),
			'maidroomCount' => $this->maidroom_count(),
			'outdoorCount'  => $this->outdoor_count(),
			'carparkCount'  => $this->carpark_count(),
			'photos'        => $this->get_api_photos(),
			'featureString' => $this->get_feature_str(),
			'lat'           => $this->lat(),
			'lng'           => $this->lng(),
			'isLeased'      => $this->leased,
			'isSold'        => $this->sold,
			'inclusive'     => $this->inclusive,
			'isForSale'     => $this->is_for_sale(),
			'apiPath'       => $this->get_property_api_path(),
			'otherfeatures'    => trim($this->otherfeatures),
			'otherfacilities'  => trim($this->otherfacilities),
			'webtext'       => trim($this->webtext),
			'tst'           => $this->tst,
			'pictst'        => $this->pictst,
			'webtst'        => $this->webtst,
			'avdate'        => $this->available_date,
			'updated_at'    => $this->get_updated_at(),
            'feat_order'    => $this->feat_order,
            'sole_agency'    => $this->sole_agency
		);
    }

    public function get_property_api_path() {
        return sprintf('property/%s/%s/%d', \NestString::sanitize_title($this->district()), \NestString::sanitize_title($this->print_address()), $this->id);
    } 

    public function is_for_sale() {
        return ($this->type_id == 1)?0:1;
    }

    public function get_feature_str() {
        $feature_ids  = Building::feature_ids();
        $features     = array();
        $feature_str  = '';
        $PropertyMeta = new PropertyMeta;
        $feature_json = $PropertyMeta->get_post_meta($this->id, 'features', true);
        $feature_arr  = Building::sort_feature_ids(json_decode($feature_json));
        if(!empty($feature_arr)){
			foreach ($feature_arr as $feature_id) {
				if (isset($feature_ids[$feature_id])) {
					$features[$feature_ids[$feature_id]] = 1;
				}
			}
		}
        if(!empty($features)) {
			$features_r = array_keys($features);
            $feature_str = join(', ', $features_r);
        }
        return $feature_str;
    }

    public function get_api_photos() {
        $res = array();
        $photos = $this->nest_photos_website;
        if(isset($photos)) {
            foreach ($photos as $photo) {
                $res[] = $photo->get_api_photo();
            }
        }
        return $res;
    }

    public function lat() {
        if (!empty($this->building)) {
            return $this->building->lat;
        } else {
            $PropertyMeta = new PropertyMeta;
            $coordinates = $PropertyMeta->get_post_meta($this->id, 'coordinates', true);
            $lat = '';
            if (!empty($coordinates)) {
                list($lat) = explode(',', $coordinates);
            }
            return $lat;
        }
    }

    public function lng() {
        if (!empty($this->building)) {
            return $this->building->lng;
        } else {
            $PropertyMeta = new PropertyMeta;
            $coordinates = $PropertyMeta->get_post_meta($this->id, 'coordinates', true);
            $lng = '';
            if (!empty($coordinates)) {
                $e = explode(',', $coordinates);
				if (count($e) >= 2){
					$lng = $e[1];
				}
            }
            return $lng;
        }
    }

    /**
     * migration helper
     */
    public function create_migration($data) {
        if (!$this->is_duplicate_migration($data)) {
            $property = (array)$data;
            return $this->create($property);
        }
        return false;
    }

    private function is_duplicate_migration($data) {
        if (!isset($data->external_id) || !isset($data->source)) {
            return false;
        }
        $count = $this->where('external_id', $data->external_id)->where('source', $data->source)->count();
        return $count > 0;
    }

    /**
     * Get the user that owns the property.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function owner()
    {
        return $this->hasOne(Vendor::class, 'id', 'owner_id');
    }

    public function agent()
    {
        return $this->hasOne(Vendor::class, 'id', 'agent_id');
    }

    public function allreps()
    {
        return $this->hasMany(Vendor::class, 'parent_id', 'owner_id')->orderBy('id', 'desc');
    }

    public function allagents()
    {
        return $this->hasMany(Vendor::class, 'parent_id', 'agent_id')->orderBy('id', 'desc');
    }

    public function building()
    {
        return $this->belongsTo(Building::class);
    }

    public function meta() 
    {
        return $this->hasMany(PropertyMeta::class);
    }

    public function migration_meta() 
    {
        return $this->hasMany(PropertymigrationMeta::class);
    }

    public function property_media()
    {
        return $this->hasMany(PropertyMedia::class, 'property_id', 'id');
    }

    public function indexed_photo() 
    {
        return $this->hasOne(PropertyPhoto::class, 'property_id', 'id');
    }

    public function featured_photo() 
    {
        return $this->hasOne(PropertyMedia::class, 'property_id', 'id')->where('group', 'nest-photo');
    }

    public function nest_photos() 
    {
        return $this->hasMany(PropertyMedia::class, 'property_id', 'id')->orderby('order', 'asc')->where('group', 'nest-photo');
    }

    public function nest_photos_website() 
    {
        return $this->hasMany(PropertyMedia::class, 'property_id', 'id')->orderby('web_order', 'asc')->where('group', 'nest-photo');
    }

    public function other_photos() 
    {
        return $this->hasMany(PropertyMedia::class, 'property_id', 'id')->orderby('order', 'asc')->where('group', 'other-photo');
    } 

    public function shortlist_option() {
        return $this->hasOne(ClientOption::class, 'property_id');
    }

    public function property_deleted()
    {
        return $this->belongsTo('App\PropertyDeleteLogs','property_id');
    }

    public function scopeIndexSearchAdvancedSort($query, $request) 
    {
        $listing_order = self::default_listing_order();
        $sort_by = $request->input('sort_by', '');
		if ($sort_by == ''){
			$sort_by = 'date-desc';
		}
        $special_order = self::custom_listing_order($sort_by);
		$featured = intval($request->input('featured', 0));
		if ($featured == 1){
			$query->orderBy('feat_order', 'desc');
		}
		$type_id = intval($request->input('type_id', 1));
		if ($type_id == 1){
			$leased = $request->input('leased', '');
			if ($leased) {
				$query->orderBy('leased', 'desc');
			}else{
				$query->orderBy('leased', 'asc');
			}
		}else{
			$query->where('sold', '=', 0);
		}
        if(!empty($special_order)) foreach ($special_order as $key => $value) {
            $listing_order[$key] = $value;
        }
        foreach ($listing_order as $key => $value) {
            if ($value['priority'] > 0) {
                $query->orderBy($key, $value['direction']);
            }
        }
        foreach ($listing_order as $key => $value) {
            if ($value['priority'] == 0) {
                $query->orderBy($key, $value['direction']);
            }
        }
    }
	
	public function scopePropertyBackendSort($query, $request) 
    {
		$query->orderBy('leased', 'asc');
		
		if (isset($_POST['sort_by'])){
			if ($_POST['sort_by'] == 'date-asc'){
				$query->orderBy('available_date', 'desc');
			}else{
				$query->orderBy('available_date', 'desc');
			}
		}else{
			$query->orderBy('available_date', 'desc');
		}
    }
	
	

    public function scopeIncompleteSearch($query, $request)
    {
        // $query->where(function($query) {
        //                     $query->orWhere(function($query){
        //                         $query->where('properties.agent_id', '=', "")
        //                             ->where('properties.owner_id', '=', "");
        //                         })->orWhere('properties.building_id', '=', '');
        //                 });
        $query->where('properties.building_id', '=', '');
        return $query;
    }

    public function scopeApiFrontendSearch($query, $request)
    {
        $input = $request->all();
        // $query->leftJoin('property_photos', 'properties.id', '=', 'property_photos.property_id');
        // $query->where('property_photos.nest_photo', '!=', '');
        $query->where('web', 1);
        if (isset($input['type_id']) && $input['type_id'] == 4) { // international
            $query->where('country_code', '!=', 'hk');
        } else {
            $query->where('country_code', 'hk');
        }
        if (isset($input['leased']) && $input['leased'] == 1) {
            $query->where('leased', 1);
        }else if (isset($input['leased']) && $input['leased'] == 0) {
            $query->where('leased', 0);
        }else if (isset($input['type_id']) && $input['type_id'] == 1){
			$query->whereRaw('not (type_id = 3 and leased = 1 and sold = 0)');
		}
		if (isset($input['type_id']) && $input['type_id'] == 2){
			$query->where('sold', 0);
		}
        return $query;
    }

    public function scopeApiKeywordSearch($query, $request)
    {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if(in_array($key, array('keywords')) && $value!='') {
                if(is_string($value))$value = trim($value);
                switch ($key) {
                    case 'keywords':
                        $query->where(function($query) use ($key, $value){
                            $query->orWhere('name', 'like', "%{$value}%");
                            $query->orWhere('address1', 'like', "%{$value}%");
							
							if (is_numeric($value) && intval($value) > 0){
								$query->orWhere('properties.id', '=', intval($value));
							}
                        });
                        break;
                }
            }
        }
        return $query;
    }

    public function scopeFrontendSearch($query, $request)
    {
        $input = $request->all();
        if (!empty($input['photo_option'])) {
            switch ($input['photo_option']) {
                case '1':
                    $query->leftJoin('property_photos', 'properties.id', '=', 'property_photos.property_id');
                    $query->where('property_photos.nest_photo', '!=', '');
                    break;
                case '2':
                    $query->leftJoin('property_photos', 'properties.id', '=', 'property_photos.property_id');
                    $query->where('property_photos.nest_photo', '=', '');
                    break;
                default:
                    break;
            }
        }
        if (!empty($input['web_option'])) {
            switch ($input['web_option']) {
                case '1':
                    $query->where('web', '=', '1');
                    break;
                case '2':
                    $query->where('web', '!=', '1');
                    break;
                default:
                    break;
            }
        }
        return $query;
    }
	
	public function scopeDashboardHot($query){
		$query->whereraw('properties.hot = 1');
		
		return $query;
	}
	
	public function scopeAvailableProps($query){
		$query->whereraw('properties.available_date <= "'.date('Y-m-d').'" and properties.leased = 0');
		
		return $query;
	}
	
	public function scopeLeaseComingUp($query){
		$query->whereraw('properties.available_date > "'.date('Y-m-d').'" and properties.leased = 1');
		
		return $query;
	}
	
	public function scopeLeaseSort($query) 
    {
		$query->orderBy('available_date', 'asc');
    }
	
	
	

    public function scopeIndexSearch($query, $request)
    {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if(in_array($key, $this->searchable) && $value!='') {
                if(is_string($value))$value = trim($value);
                switch ($key) {
                    case 'outdoors':
                        if (is_array($value) && count($value)>0) {
                            $query->leftJoin('property_outdoors', 'properties.id', '=', 'property_outdoors.property_id');
                            foreach ($value as $option) {
                                $query->where('property_outdoors.f'.$option, '=', 1);
                            }
                        }
                        break;
                    case 'features':
                        if (is_array($value) && count($value)>0) {
                            $query->leftJoin('property_features', 'properties.id', '=', 'property_features.property_id');
                            foreach ($value as $option) {
                                $query->where('property_features.f'.$option, '=', 1);
                            }
                        } else {
                            $feature_ids = explode(',',$value);
                            $query->leftJoin('property_features', 'properties.id', '=', 'property_features.property_id');
                            foreach ($feature_ids as $option) {
                                $query->where('property_features.f'.$option, '=', 1);
                            }
							/*
                            foreach ($feature_ids as $key => $feature_id) {
                                if(!is_numeric($feature_id))unset($feature_ids[$key]);
                            }
                            if(!empty($feature_ids))$query->whereIn('properties.district_id', $feature_ids);*/
                        }
                        break;
                    case 'type_id':
                        if(is_numeric($value)){
                            if (in_array($value, array(1,2))):
                                $type_ids = array($value, 3);// add rent & sale
                                $query->whereIn('type_id', $type_ids);
                            endif;
                        }
                        break;
                    case 'price_min':
                        if (isset($input['type_id']) && $input['type_id']==1) {
                            $query->where('asking_rent', '>=', $value);
                        }
                        if (isset($input['type_id']) && $input['type_id']==2) {
                            $query->where('asking_sale', '>=', $value);
                        }
                        break;
                    case 'price_max':
                        if (is_numeric($value) && isset($input['type_id']) && $input['type_id']==1) {
                            $query->where('asking_rent', '<=', $value);
                        }
                        if (is_numeric($value) && isset($input['type_id']) && $input['type_id']==2) {
                            $query->where('asking_sale', '<=', $value);
                        }
                        break;
                    case 'size_min':
                        if(is_numeric($value))$query->where('gross_area', '>=', $value);
                        break;
                    case 'size_max':
                        if(is_numeric($value))$query->where('gross_area', '<=', $value);
                        break;
                        break;
                    case 'bedroom_min':
                        if(is_numeric($value))$query->where('bedroom', '>=', $value);
                        break;
                    case 'bedroom_max':
                        if(is_numeric($value))$query->where('bedroom', '<=', $value);
                        break;
                    case 'bathroom_min':
                        if(is_numeric($value))$query->where('bathroom', '>=', $value);
                        break;
                    case 'bathroom_max':
                        if(is_numeric($value))$query->where('bathroom', '<=', $value);
                        break;
                    case 'excluded_id':
                        if(is_numeric($value))$query->where('properties.id', '!=', "{$value}");
                        if(is_string($value)) {
                            $_value = explode(',', $value);
                            if(!empty($_value))$query->whereNotIn('properties.id', $_value);
                        }
                        if(is_array($value)) {
                            if(!empty($value))$query->whereNotIn('properties.id', $value);
                        }
                        break;
                    case 'id':
                    case 'included_id':
                        if(is_numeric($value))$query->where('properties.id', 'like', "{$value}");
                        if(is_string($value)) {
                            $_value = explode(',', $value);
                            if(!empty($_value))$query->whereIn('properties.id', $_value);
                        }
                        if(is_array($value)) {
                            if(!empty($value))$query->whereIn('properties.id', $value);
                        }
                        break;
                    case 'districts':
                        $district_ids = explode(',',$value);
                        foreach ($district_ids as $key => $district_id) {
                            if(!is_numeric($district_id))unset($district_ids[$key]);
                        }
                        if(!empty($district_ids))$query->whereIn('properties.district_id', $district_ids);
                        break;
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        if (!empty($input['hot_option'])) {
            switch ($input['hot_option']) {
                case '1':
                    $query->where('properties.hot', '1');
                    break;
                case '2':
                    $query->where('properties.hot', '0');
                    break;
                default:
                    break;
            }
        }
        // die($query->toSql());
        return $query;
    }

    public function scopeShortlistSearch($query, $shortlist_id)
    {
        $query->orderBy('properties.district_id', 'asc');
        $query->leftJoin('client_options', 'properties.id', '=', 'client_options.property_id');
        $query->where('client_options.shortlist_id', $shortlist_id);
        $query->orderBy('client_options.order', 'asc');
        $query->select(\DB::raw('properties.*'));
        return $query;
    }

    /**
     * Helpers
     */
    public function get_reps() {
        $rep_ids = explode(',', $this->rep_ids);
        if (empty($rep_ids))return null;
        $reps = Vendor::whereIn('id', $rep_ids)->get();
        return !empty($reps)?$reps:null;
    }
	
    public function get_agents() {
        $agent_ids = explode(',', $this->agent_ids);
        if (empty($agent_ids))return null;
        $agents = Vendor::whereIn('id', $agent_ids)->get();
        return !empty($agents)?$agents:null;
    }

    public function nest_id() {
        $rent_label = $this->is_rent()?'L':'';
        $sale_label = $this->is_sale()?'S':'';
        $district_id = isset($this->Building)?$this->Building->district_id:0;
        return sprintf("%s%s-%03d-%06d", $rent_label, $sale_label, $district_id, $this->id);
    }

    public function type() {
        return isset($this->type_ids[$this->type_id])?$this->type_ids[$this->type_id]:'';
    }

    public function print_type($default = 'Lease') {
        $this->type_ids[1] = 'Lease';
        $this->type_ids[3] = $default;
        return isset($this->type_ids[$this->type_id])?$this->type_ids[$this->type_id]:'';
    }

    public function poc() {
    	return isset($this->poc_ids[$this->poc_id])?$this->poc_ids[$this->poc_id]:'';
    }

    public function district() {
		if ($this->country_code != 'hk'){
			return 'International';
		}else{
			$district_ids = Building::district_ids();
			return isset($district_ids[$this->district_id])?$district_ids[$this->district_id]:$this->address2;
		}
    }
	
	public function country(){
		if ($this->country_code != 'hk'){
			$countries = Building::country_ids();
			if (isset($countries[$this->country_code])){
				return $countries[$this->country_code];
			}else{
				return 'International';
			}
		}else{
			return 'Hong Kong';
		}		
	}

    public function district_for_shortlist() {
		if ($this->country_code != 'hk'){
			return 'International';
		}else{
			$district_ids = Building::district_ids();
			return isset($district_ids[$this->district_id])?$district_ids[$this->district_id]:'-';
		}
    }

    public function asking_rent() {
        if (!$this->is_rent() || $this->asking_rent == 0) {
            return '';
        }
        return number_format($this->asking_rent, 0, '', ',');
    }

    public function asking_sale() {
		if ($this->nosaleprice == 1){
			return 'Subject to Offer';
		}
        if (!$this->is_sale() || $this->asking_sale == 0) {
            return '';
        }
        return number_format($this->asking_sale, 0, '', ',');
    }

    public function is_rent() {
        return in_array($this->type_id, array(1,3));
    }

    public function is_sale() {
        return in_array($this->type_id, array(2,3));
    }

    public function price(){
    	$price = array();
    	if ($this->asking_rent() && $this->leased == 0){
    		$price[] = $this->asking_rent();
    	}
		if ($this->asking_sale() && $this->sold == 0){
    		$price[] = $this->asking_sale();
    	}
    	if ($this->leased == 1 && $this->sold == 1 && $this->type_id == 3){
    		$price[] = $this->asking_rent();
    	}else if ($this->leased == 1 && $this->type_id == 1){
    		$price[] = $this->asking_rent();
    	}
    	if (!empty($price)) {
    		return join(' / ', $price);
    	}else{
    		return '-';
    	}
    }
	
	public function priceinclexcl(){
    	$price = array();
    	if ($this->asking_rent() && $this->leased == 0){
			if ($this->inclusive == 0){
				$price[] = $this->asking_rent().' excl';
			}else{
				$price[] = $this->asking_rent().' incl';
			}
    	}
		if ($this->asking_sale() && $this->sold == 0){
    		$price[] = $this->asking_sale();
    	}
    	if ($this->leased == 1 && $this->sold == 1 && $this->type_id == 3){
			if ($this->inclusive == 0){
				$price[] = $this->asking_rent().' excl';
			}else{
				$price[] = $this->asking_rent().' incl';
			}
    	}else if ($this->leased == 1 && $this->type_id == 1){
			if ($this->inclusive == 0){
				$price[] = $this->asking_rent().' excl';
			}else{
				$price[] = $this->asking_rent().' incl';
			}
    	}
    	if (!empty($price)) {
    		return join(' / ', $price);
    	}else{
    		return '-';
    	}
	}
	
	public function price_db_searchresults(){
    	$price = array();
    	if ($this->asking_rent()){
			if ($this->leased == 0){
				$price[] = ''.$this->asking_rent();
			}else{
				$price[] = ''.$this->asking_rent().' [leased]';
			}
    	}
    	if ($this->asking_sale()){
			if ($this->sold == 0){
				$price[] = ''.$this->asking_sale();
			}else{
				$price[] = ''.$this->asking_sale().' [sold]';
			}
    	}
    	if (!empty($price)) {
    		return join(' / ', $price);
    	}else{
    		return '-';
    	}		
	}
	
	public function price_db_searchresults_new(){
    	$price = array();
		if ($this->type_id == 3 && $this->asking_rent == 0){
			$price[] = '-';
		}else if ($this->asking_rent()){
			if ($this->leased == 0){
				if ($this->inclusive == 0){
					$price[] = 'HK$ '.$this->asking_rent().' <span style="font-size:8px;">EX</span>';
				}else{
					$price[] = 'HK$ '.$this->asking_rent();
				}
			}else{
				if ($this->inclusive == 0){
					$price[] = '<s>HK$ '.$this->asking_rent().' <span style="font-size:8px;">EX</span></s>';
				}else{
					$price[] = '<s>HK$ '.$this->asking_rent().'</s>';
				}
			}
    	}
		if ($this->nosaleprice == 1){
			if ($this->sold == 0){
				$price[] = ''.$this->asking_sale();
			}else{
				$price[] = '<s>'.$this->asking_sale().'</s>';
			}
		}else if ($this->type_id == 3 && $this->asking_sale == 0){
			$price[] = '-';
		}else if ($this->asking_sale()){
			if ($this->sold == 0){
				$price[] = 'HK$ '.$this->asking_sale();
			}else{
				$price[] = '<s>HK$ '.$this->asking_sale().'</s>';
			}
    	}
    	if (!empty($price)) {
    		return join(' / ', $price);
    	}else{
    		return '-';
    	}		
	}
	
	
    public function property_size() {//depreciated
        return number_format($this->gross_area, 0, '', ',');
    }

    public function property_netsize() {//depreciated
        return number_format($this->saleable_area, 0, '', ',');
    }

    public function property_grosssize() {
        return number_format($this->gross_area, 0, '', ',');   
    }

    public function property_saleablesize() {
        return number_format($this->saleable_area, 0, '', ',');
    }

    public function show_size() {
        return $this->property_saleablesize() . ' / ' . $this->property_grosssize();
    }

    public function shorten_building_name($print = 1) {
        $name = '';
        if (!empty($this->display_name)) {
            $name = $this->display_name;
        } elseif (empty($this->Building->name)) { 
            list($name) = explode('(', $this->name);
        } elseif($print) {
            $name = $this->Building->shorten_print_name();
        } else {
            $name = $this->Building->shorten_name();
        }
        return $name;
    }

    public function property_slug() {
        return \NestString::sanitize_title($this->shorten_building_name());
    }

    public function fix_address1() {
        if (!empty($this->building_id)) {
            return $this->Building->address1;
        }else{
            return $this->address1;
        }
    }

    public function shorten_address() {
        $address = $this->address1;
        $address_array = explode(',', $address);
        if (count($address_array) >= 4) {
            $address_array = array_slice($address_array, 0, count($address_array)-2);
        }
        return join(', ', $address_array);
    }

    public function print_address() {
        if ($this->Building) {
            $address = $this->_building_address_shorten($this->Building->address1, $this->Building->district());
        } else {
            $address = $this->fix_address1();
        }
        $address_array = explode(',', $address);
        if (count($address_array) >= 2) {
            return $address_array[0];
        }
        return $address;
    }

    private function _building_address_shorten($address, $district) {
        list($_address) = explode(", " . $district . ",", $address);
        $shorten_address = $_address;
        if (preg_match("/(\d[^,]*),(.+)/", $_address, $m)) {
            $shorten_address = $m[1] . $m[2];
        }
        list($shorten_address) = explode(' / ', $shorten_address);
        return $shorten_address;
    }

    public function shorten_asking_rent() {
        if ($this->asking_rent == 0) {
            return null;
        }
        return \NestNumber::room_num($this->asking_rent / 1000) . 'K';
    }

    public function shorten_asking_sale() {
        if ($this->asking_sale == 0) {
            return null;
        }
        return \NestNumber::sale_price_printing($this->asking_sale / 1000000) . 'M';
    }

    public function outdoor_count() {
        $PropertyMeta = new PropertyMeta();
        $outdoors = json_decode($PropertyMeta->get_post_meta($this->id, 'outdoors', true));
		
		//Commented out to make outdoor count work for API
        /*if (count($outdoors)==0) {
            if(!empty($this->outdoor))return $this->outdoor;
        }*/
        return count($outdoors);
    }

    public function carpark_count() {
        $PropertyMeta = new PropertyMeta();
        $features = json_decode($PropertyMeta->get_post_meta($this->id, 'features', true));
        $carpark_feature = !empty($features) && in_array(5, $features);// car park id = 4

        if ((int)$this->car_park > 0) {
            return $this->car_park;
        } elseif($carpark_feature) {
            return 1;
        } else {
            return 0;
        }
        // return !empty($features) && in_array(5, $features)?1:0; // car park id = 4
    }
	
	public function backend_search_outdoor(){
		$ret = '-';
		
		$PropertyMeta = new PropertyMeta();
        $outdoors = json_decode($PropertyMeta->get_post_meta($this->id, 'outdoors', true));
		
		$keys = array();
		if (is_array($outdoors) && count($outdoors) > 0){
			foreach ($outdoors as $od){
				$od = intval($od);
				$keys[$od] = 1;
			}
		}
		if (isset($keys[4])){
			$ret = 'Garden';
		}else if (isset($keys[1])){
			$ret = 'Terrace';
		}else if (isset($keys[3])){
			$ret = 'Rooftop';
		}else if (isset($keys[2])){
			$ret = 'Balcony';
		}
		
		return $ret;
	}

    public function first_print_image($size = 'system-print_large') {
        $photos = $this->get_print_photos();
		
		if (!empty($photos[0])){
			return trim($photos[0]->_get_print_url($size));
		}
		
        return '';
    }

    public function second_print_image($size = 'system-print_large') {
        $photos = $this->get_print_photos();
        return !empty($photos[1])?$photos[1]->_get_print_url($size):'';
    }

    public function third_print_image($size = 'system-print_large') {
        $photos = $this->get_print_photos();
        return !empty($photos[2])?$photos[2]->_get_print_url($size):'';
    }

    public function bedroom_count() {
       return \NestNumber::room_num($this->bedroom);
    }

    public function bathroom_count() {
       return \NestNumber::room_num($this->bathroom);
    }

    public function maidroom_count() {
       return \NestNumber::room_num($this->maidroom);
    }

    public function fix_url($url) {
        return str_replace(' ', '%20', $url);
    }

    public function get_print_photos() {
        $photos = $this->nest_photos;
        if (empty($photos->toArray()) || count($photos->toArray()) < 3) {
            $photos = $this->other_photos;
        }
        if (empty($photos->toArray())) {
            $PropertymigrationMeta = new \App\PropertymigrationMeta();
            $content = $PropertymigrationMeta->get_post_meta($this->id, 'post_content', true);
            $html = str_get_html($content);
            $photos = array();
            if(!empty($html))foreach ($html->find('img') as $img) {
                $image = new \stdClass();
                $image->url = $img->src;
                $photos[] = $image;
            }
        }
        return $photos;
    }

    public function available_date() {
        if ($this->available_date == '0000-00-00' || strlen(trim($this->available_date)) < 8) {
            return '-';
        }
		//return $this->available_date;
        return \NestDate::nest_date_format($this->available_date, 'Y-m-d');
    }

    public function show_timestamp() {
        $create_at = \NestDate::nest_date_format($this->created_at);
        $updated_at = \NestDate::nest_date_format($this->updated_at);
        echo <<<end
Created: {$create_at}<br/>
Last Updated: {$updated_at}
end;
    }

    public function is_old_property() {
        // apply old property photo order logic
        $property_id = $this->id;
        $property_count = Property::where('id', $property_id)->where('external_id', '!=', '')->count();
        $nest_media_count = PropertyMedia::where('property_id', $property_id)->where('group', 'nest-photo')->where('order', '>', 0)->count();
        $other_media_count = PropertyMedia::where('property_id', $property_id)->where('group', 'other-photo')->where('order', '>', 0)->count();
        // dex([$property_count,$nest_media_count,$other_media_count]);
        return $property_count && !$nest_media_count && !$other_media_count;
    }
	
	public function get_updated_at(){
		if (!empty($this->updated_at)){
			return $this->updated_at;
		}else{
			return 0;
		}
	}
	
	public function get_created_at(){
		if (!empty($this->created_at)){
			return $this->created_at;
		}else{
			return 0;
		}
	}
	
    public function get_nice_updated_at() {
        if ($this->updated_at == '0000-00-00') {
            return '-';
        }
        return \NestDate::nest_date_format($this->updated_at, 'Y-m-d H:i:s');
    }
	
    public function get_nice_created_at() {
        if ($this->created_at == '0000-00-00') {
            return '-';
        }
        return \NestDate::nest_date_format($this->created_at, 'Y-m-d H:i:s');
    }
	
	public function show_last_comment() {
        $out = '';
		
		if (trim($this->comments) != ''){
			$short = substr(trim($this->comments), 0, 2);
			if ($short == '[]'){
				$out = '';
			}else if ($short == '[{' || $short == '{"'){
				$comments = json_decode($this->comments, true);
				if (isset($comments[count($comments)-1])){
					$out = $comments[count($comments)-1]['content'];
				}
			}else{
				$out = $this->comments;
			}
		}
		
        return $out;
    }
	
	//basic listing of all properties
    public function scopePropertylistSearch($query, $request)
    {
        return $query;
    }
	
    public function scopePropertylistsearchSearch($query, $request)
    {
        $input = $request->all();
		if (isset($input['s'])){
			$search = trim(filter_var($input['s'], FILTER_SANITIZE_STRING));
			$search = str_replace("&#39;", '\\\'', $search);
			
			if (is_numeric($search)){
				$query->where('properties.id', '=', $search);
			}else{
				$query->where(function ($query) use ($search) {
					$query->where('properties.name', 'like', '%'.$search.'%')
						  ->orWhere('properties.address1', 'like', '%'.$search.'%');
				});
			}
		}
		//echo $search;
		//echo $query->toSql();
		
		//die();
        return $query;
    }
	
    public function scopePropertylistadvsearchSearch($query, $request){
        $input = $request->all();
		
		//Property
		if (isset($input['name']) && !empty($input['name']) && trim($input['name']) != ''){
			$query->where('name', 'like', "%".$input['name']."%");
		}
		
		//Address
		if (isset($input['address1']) && !empty($input['address1']) && trim($input['address1']) != ''){
			$query->where('address1', 'like', "%".$input['address1']."%");
		}
		
		//Property ID
        if (isset($input['id']) && !empty($input['id']) && is_numeric($input['id']) && intval($input['id']) > 0){
			$query->where('id', '=', intval($input['id']));
        }
		
		//Rent & Sale
        if (!empty($input['type_id'])){
			if ($input['type_id'] == 1){
				$query->whereRaw('(type_id = 1 or type_id = 3)');
			}else if ($input['type_id'] == 2){
				$query->whereRaw('(type_id = 2 or type_id = 3)');
			}
        }
		
		//Price from
		if (isset($input['price_min']) && !empty($input['price_min']) && is_numeric($input['price_min'])){
			if (isset($input['type_id']) && $input['type_id'] == 1) {
				$query->where('asking_rent', '>=', $input['price_min']);
			}
			if (isset($input['type_id']) && $input['type_id'] == 2) {
				$query->where('asking_sale', '>=', $input['price_min']);
			}
			if (isset($input['type_id']) && $input['type_id'] == 0){
				$query->whereRaw('(asking_rent >= '.intval($input['price_min']).' or asking_sale >= '.intval($input['price_min']).')');
			}
		}
		
		//Price to
		if (isset($input['price_max']) && !empty($input['price_max']) && is_numeric($input['price_max'])){
			if (isset($input['type_id']) && $input['type_id'] == 1) {
				$query->where('asking_rent', '<=', $input['price_max']);
			}
			if (isset($input['type_id']) && $input['type_id'] == 2) {
				$query->where('asking_sale', '<=', $input['price_max']);
			}
			if (isset($input['type_id']) && $input['type_id'] == 0){
				$query->whereRaw('(asking_rent <= '.intval($input['price_max']).' or asking_sale <= '.intval($input['price_max']).')');
			}
		}
		
		//Size from
		if (isset($input['size_min']) && !empty($input['size_min']) && is_numeric($input['size_min'])){
			$query->where('saleable_area', '>=', $input['size_min']);
		}
		
		//Size to
		if (isset($input['size_max']) && !empty($input['size_max']) && is_numeric($input['size_max'])){
			$query->where('gross_area', '<=', $input['size_max']);
		}
		
		//Bedrooms from
		if (isset($input['bedroom_min']) && !empty($input['bedroom_min']) && is_numeric($input['bedroom_min'])){
			$query->where('bedroom', '>=', $input['bedroom_min']);
		}
		
		//Bedrooms to
		if (isset($input['bedroom_max']) && !empty($input['bedroom_max']) && is_numeric($input['bedroom_max'])){
			$query->where('bedroom', '<=', $input['bedroom_max']);
		}		
		
		//Neighbourhoods and country
		if (isset($input['district_id']) && !empty($input['district_id']) && is_array($input['district_id'])){
            if (!empty($input['country_code'])) {
                $query->where(function($q) use ($input) {
                    $q->whereIn('district_id', $input['district_id'])
                            ->orWhereIn('country_code', $input['country_code']);
                });
            } else {
                $query->whereIn('district_id', $input['district_id']);
            }
			
        }
        	
		//Features / Facilities
		if (isset($input['features']) && !empty($input['features']) && is_array($input['features'])){
			$query->leftJoin('property_features', 'properties.id', '=', 'property_features.property_id');
			foreach ($input['features'] as $f){
				$query->where('property_features.f'.$f, '=', 1);
			}
		}
		
		//Unit
		if (isset($input['unit']) && !empty($input['unit']) && trim($input['unit']) != ''){
			$query->where('unit', 'like', "%".$input['unit']."%");
		}
		
		//Comments
		if (isset($input['comments']) && !empty($input['comments']) && trim($input['comments']) != ''){
			$query->where('comments', 'like', "%".$input['comments']."%");
		}		
		
		//Description
		if (isset($input['description']) && !empty($input['description']) && trim($input['description']) != ''){
			$query->where('description', 'like', "%".$input['description']."%");
		}		
		
		//Hot
        if (!empty($input['hot_option'])){
			if ($input['hot_option'] == 1){
				$query->where('properties.hot', '1');
			}else if ($input['hot_option'] == 2){
				$query->where('properties.hot', '0');
			}
        }
		
		//Bathrooms from
		if (isset($input['bathroom_min']) && !empty($input['bathroom_min']) && is_numeric($input['bathroom_min'])){
			$query->where('bathroom', '>=', $input['bathroom_min']);
		}
		
		//Bathrooms to
		if (isset($input['bathroom_max']) && !empty($input['bathroom_max']) && is_numeric($input['bathroom_max'])){
			$query->where('bathroom', '<=', $input['bathroom_max']);
		}		
		
		//Photos
        if (!empty($input['photo_option'])){
			if ($input['photo_option'] == 1){
				$query->leftJoin('property_photos', 'properties.id', '=', 'property_photos.property_id');
				$query->where('property_photos.nest_photo', '!=', '');
			}else if ($input['photo_option'] == 2){
				$query->leftJoin('property_photos', 'properties.id', '=', 'property_photos.property_id');
				$query->where('property_photos.nest_photo', '=', '');
			}
        }		
		
		//Website
        if (!empty($input['web_option'])){
			if ($input['web_option'] == 1){
				$query->where('web', '=', '1');
			}else if ($input['web_option'] == 2){
				$query->where('web', '!=', '1');
			}
        }
		
		
		
        /*foreach ($input as $key => $value){
            if(in_array($key, $this->searchable) && $value != ''){
                if(is_string($value)){
					$value = trim($value);
				}
                switch ($key) {
                    case 'outdoors':
                        if (is_array($value) && count($value)>0) {
                            $query->leftJoin('property_outdoors', 'properties.id', '=', 'property_outdoors.property_id');
                            foreach ($value as $option) {
                                $query->where('property_outdoors.f'.$option, '=', 1);
                            }
                        }
                        break;
                    case 'features':
                        if (is_array($value) && count($value)>0) {
                            $query->leftJoin('property_features', 'properties.id', '=', 'property_features.property_id');
                            foreach ($value as $option) {
                                $query->where('property_features.f'.$option, '=', 1);
                            }
                        } else {
                            $feature_ids = explode(',',$value);
                            $query->leftJoin('property_features', 'properties.id', '=', 'property_features.property_id');
                            foreach ($feature_ids as $option) {
                                $query->where('property_features.f'.$option, '=', 1);
                            }
                        }
                        break;
                    case 'type_id':
                        if(is_numeric($value)){
                            if (in_array($value, array(1,2))):
                                $type_ids = array($value, 3);// add rent & sale
                                $query->whereIn('type_id', $type_ids);
                            endif;
                        }
                        break;
                    case 'price_min':
                        break;
                    case 'price_max':
                        if (is_numeric($value) && isset($input['type_id']) && $input['type_id']==1) {
                            $query->where('asking_rent', '<=', $value);
                        }
                        if (is_numeric($value) && isset($input['type_id']) && $input['type_id']==2) {
                            $query->where('asking_sale', '<=', $value);
                        }
                        break;
                    case 'size_min':
                        if(is_numeric($value))$query->where('gross_area', '>=', $value);
                        break;
                    case 'size_max':
                        if(is_numeric($value))$query->where('gross_area', '<=', $value);
                        break;
                        break;
                    case 'bedroom_min':
                        if(is_numeric($value))$query->where('bedroom', '>=', $value);
                        break;
                    case 'bedroom_max':
                        if(is_numeric($value))$query->where('bedroom', '<=', $value);
                        break;
                    case 'bathroom_min':
                        if(is_numeric($value))$query->where('bathroom', '>=', $value);
                        break;
                    case 'bathroom_max':
                        if(is_numeric($value))$query->where('bathroom', '<=', $value);
                        break;
                    case 'excluded_id':
                        if(is_numeric($value))$query->where('properties.id', '!=', "{$value}");
                        if(is_string($value)) {
                            $_value = explode(',', $value);
                            if(!empty($_value))$query->whereNotIn('properties.id', $_value);
                        }
                        if(is_array($value)) {
                            if(!empty($value))$query->whereNotIn('properties.id', $value);
                        }
                        break;
                    case 'id':
                    case 'included_id':
                        if(is_numeric($value))$query->where('properties.id', 'like', "{$value}");
                        if(is_string($value)) {
                            $_value = explode(',', $value);
                            if(!empty($_value))$query->whereIn('properties.id', $_value);
                        }
                        if(is_array($value)) {
                            if(!empty($value))$query->whereIn('properties.id', $value);
                        }
                        break;
                    case 'district_id':
						if (is_numeric($value)){
							$query->where($key, '=', $value);
						}
                        break;
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }*/
        // die($query->toSql());
        return $query;
    }
	
	public static function getPropertyFeaturesList(){
		$ret = array(
			5 => 'Car Park',
			10 => 'Rooftop',
			26 => 'Garden',
			12 => 'Terrace',
			9 => 'Balcony',
			21 => 'Brand New Renovation',
			11 => 'Helper’s Room',
			25 => 'Internal Staircase',
			17 => 'Walk-In Wardrobe',
			27 => 'Kitchen Pantry',
			13 => 'Store Room',
			30 => 'Family Room',
			18 => 'Utility Room',
		);
		
		return $ret;
	}
	
	public static function getBuildingFacilitiesList(){
		$ret = array(
			2 => 'Swimming Pool',
			1 => 'Gym',
			3 => 'Tennis Court',
			4 => 'Squash Court',
			7 => 'Children’s Play Area / Playground', //+19
			28 => 'Barbeque Area',
			14 => 'Concierge Service',
			6 => 'Shuttle Bus',
			31 => 'Car Parking Facilities',
			8 => 'Pet Friendly',
			32 => 'Gated Complex',
			24 => 'Jacuzzi',
			15 => 'Sauna',
			33 => 'Steam Room',
			34 => 'Golf Simulator',
			35 => 'Cigar Room',
			22 => 'Lift',
			29 => 'Mahjong rooms',
			36  => "Walk-up",
		);
		
		return $ret;
	}
	
	public static function getOtherOldFeatures(){
		$ret = array(
			16 => 'Duplex',
			//20 => 'Play Room', //=> together  in Children’s Play Area / Playground?
			23 => 'Stand-Alone House',
		);
		
		return $ret;
	}
	
	public function getOwner(){
		return Vendor::where('id', '=', $this->owner_id)->get();
	}
	public function getAgency(){
		return Vendor::where('id', '=', $this->agent_id)->get();
	}
	public function getRep(){
		return Vendor::whereIn('id', explode(',',$this->rep_ids))->limit(1)->get();
	}
	public function getReps(){
		return Vendor::whereIn('id', explode(',',$this->rep_ids))->get();
	}
	public function getAgent(){
		return Vendor::whereIn('id', explode(',',$this->agent_ids))->limit(1)->get();
	}
	public function getAgents(){
		return Vendor::whereIn('id', explode(',',$this->agent_ids))->get();
	}




	
	
	public function PropertyDeleteLogs($property_id, $deleted_by, $confirmed_by){
        $delete_logs = new PropertyDeleteLogs();	
        
        $delete_logs->property_id = $property_id;
        $delete_logs->deleted_by = $deleted_by;
        $delete_logs->confirmed_by = $confirmed_by;
        
        if($confirmed_by !== 0){
            $delete_logs->status = 'Confirmed';
        }
        
        $delete_logs->save();
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
}
