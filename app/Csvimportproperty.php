<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Csvimportproperty extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'csvimportproperty';
	
    protected $fillable = ['id', 'lastupdate', 'cid', 'pid', 'address', 'district', 'building', 'unit', 'floor', 'available', 'size', 'bedrooms', 'bathrooms', 'parking', 'yearbuilt', 'type', 'lease', 'sale', 'inclusive', 'mfee', 'govrates', 'status', 'city', 'keyloc', 'contactinfo', 'finfo1', 'finfo2', 'details'];
	
	
}