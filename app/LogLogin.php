<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogLogin extends BaseMediaModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'zlog_login';
    
    protected $fillable = ['userid'];
    public $key_id = 'logid';
	
	
	
}
