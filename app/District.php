<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class District extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['id', 'name', 'old_id', 'parent_id', 'order', 'user_id', 'international'];

    public static $rules = array(
            'name'     => 'max:255',
        );

    protected $searchable = ['web'];

    public function scopeIndexSearch($query, $request)
    {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if(in_array($key, $this->searchable) && !empty($value)) {
                if(is_string($value))$value = trim($value);
                switch ($key) {
                    case 'web':
                        $query->where($key, '=', $value);
                        break; 
                    default:
                        $query->where($key, 'like', "%{$value}%");
                        break;
                }
            }
        }
        return $query;
    }

    // for first insert
    public function createDistrict($id, $name) {
        $data = array(
            'id' => $id,
            'user_id' => 1,
            'name' => $name,
            'parent_id' => 0,
            );
        return $this->create($data); 
    }
}