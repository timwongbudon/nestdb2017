<?php

namespace App;

use Input;

class BasedocumentFields{
	
	public static function getCurDocVersions($doc){
		//Blank Letter
		if ($doc == 'bl'){
			return 1;
		}
		//Employment Letter
		if ($doc == 'el'){
			return 1;
		}
		//Letter of Dismissal
		if ($doc == 'lod'){
			return 1;
		}
		//Offer of Employment
		if ($doc == 'ooe'){
			return 1;
		}
		return 0;
	}
	
	
    public static function getBlankLetterFields($version){
        if ($version == 1){
			return '0|text|Title
					1|date|Date of Issue
					2|text|To
					3|text|Attn
					4|text|Address (Dear xxx)
					5|text|Premises
					6|textarea|Main Content
					7|radio|Signature Type|No Signature#Consultant#1 Party#2 Parties
					8|text|Manager Name
					9|text|Manager Role/Title
					10|text|Manager License Number
					11|text|Party 1 Name
					12|text|Party 2 Name
					';
					//12 highest
		}
		//Consultant:Holding Deposit Letter
		//2 Parties: Letter of Undertaking
		
        return ''; 
    }
	public static function getBlankLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getBlankLetterReplace($version){
        if ($version == 1){
			return array(
				
			);
		}
		
        return ''; 
    }
	
    public static function getEmploymentLetterFields($version){
        if ($version == 1){
			return '0|text|Title
					1|date|Date of Issue
					5|text|Premises
					2|text|Employee Name
					3|text|HKID
					4|text|Employed Since
					6|textarea|Following Content
					7|text|Manager Name
					8|text|Manager Role/Title
					9|text|Manager License Number
					';
					//9 highest
		}
		
        return ''; 
    }
	public static function getEmploymentLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getEmploymentLetterReplace($version){
        if ($version == 1){
			return array(
				
			);
		}
		
        return ''; 
    }
	
	
    public static function getDismissalLetterFields($version){
        if ($version == 1){
			return '0|text|Title
					1|date|Date of Issue
					2|text|Attn:
					3|text|By ...
					4|text|Dear xxx
					5|text|Premises
					6|date|Notice Date
					7|text|Notice Period
					8|date|Termination Date
					9|textarea|Remaining Paragraph
					10|text|Final Renumeration Month 1 Left
					11|text|Final Renumeration Month 1 Right
					12|text|Final Renumeration Month 2 Left
					13|text|Final Renumeration Month 2 Right
					14|text|Final Renumeration Month 3 Left
					15|text|Final Renumeration Month 3 Right
					16|text|Employee Name
					17|text|Manager Name
					18|text|Manager Role/Title
					';
					//9 highest
		}
		
        return ''; 
    }
	public static function getDismissalLetterShowHide($version){
        if ($version == 1){
			/*return '1|0|1|Carpark Section
					2|0|1|Early Possession Section
					3|0|0|Other conditions
					3|7|1|To re-plaster where necessary and repaint the premises (in a neutral colour) throughout.
					3|1|1|To fill gaps with wood putty (if necessary), and to sand and polyurethane all parquet flooring.
					3|2|1|To revarnish all natural wood surfaces including doors.
					3|3|1|To provide blinds or curtain rails and curtains throughout.
					3|4|1|To allow the Tenant to hang a wall-mounted TV and other personal effects on the walls.
					3|5|1|To provide one standard-sized washing machine and clothes dryer.
					3|8|1|To ensure that all electrical wiring and sockets, plumbing and gas points are in good working order.
					3|9|1|To service air-conditioning and other appliances (if any) so as to ensure that they are clean and in good working order.
					3|6|1|To regrout around the toilet, shower, bathtub, washbasin, sinks and backsplash areas in all bathrooms and kitchen where necessary.
					3|10|1|To repair any defects.
					3|11|1|To clean the premises throughout including the interior and exterior of windows and cupboards.
					3|12|1|To prepare an inventory of furniture / appliances / fittings for attachment to the Tenancy Agreement, this inventory being subject to verification upon hand-over of the premises.
					3|13|1|A detailed inspection of the premises will be undertaken by the Tenant, and a list of any further defects may be submitted thereafter, the rectification of such defects being subject to agreement by both parties.
					4|0|1|Bank Consent Section
					';*/
			return '';
		}
		
        return ''; 
    }
	public static function getDismissalLetterReplace($version){
        if ($version == 1){
			return array(
				'first' => 'First Sentence',
				'middle' => 'Middle Section',
				'middle2' => 'Your salary up to ...',
				'middle3' => 'The details of ...',
				'last' => 'Last Sentence'
			);
		}
		
        return ''; 
    }
	
	
	
    public static function getLetterOfEmploymentFields($version){
        if ($version == 1){
			return '0|text|Title
					1|date|Date of Issue
					2|text|Employee Name
					3|text|HKID No.
					4|text|Contact Name
					5|text|Contact Role
					6|text|Contact Email
					7|text|Dear xxx
					8|text|Commencement Date
					9|text|Job Title
					10|text|Working Hours
					11|textarea|Holiday
					12|text|Role (short)
					13|radio|Gender|his#her
					14|textareadrag|Job Description
					15|textareadrag|Facilities Provided
					16|textareadrag|Facilities not Provided
					17|textareadrag|Commission
					';
					//17 highest
		}
		
        return ''; 
    }
	public static function getLetterOfEmploymentShowHide($version){
        if ($version == 1){
			return '1|0|1|Commencement Date Section
					2|0|1|Job Title Section
					3|0|1|Basic Salary Section
					4|0|1|MPF Section
					5|0|1|Working Hours Section
					6|0|1|Probation Period Section
					7|0|1|Holiday Section
					8|0|1|Code of Conduct Section
					9|0|1|Confidentiality Section
					10|0|1|Exclusivity Section
					11|0|1|Job Description Section
					12|0|1|Insurance Cover Section
					13|0|1|Facilities Section
					14|0|1|Commission Section
					';
			return '';
		}
		
        return ''; 
    }
	public static function getLetterOfEmploymentReplace($version){
        if ($version == 1){
			return array(
				'first' => 'First Sentence',
				'commencement' => 'Commencement Date Section',
				'jobtitle' => 'Job Title Section',
				'basicsalary' => 'Basic Salary Section',
				'mpf' => 'MPF Section',
				'workinghours' => 'Working Hours Section',
				'probationperiod' => 'Probation Period Section',
				'holiday' => 'Holiday Section',
				'codeofconduct' => 'Code of Conduct Section',
				'confidentiality' => 'Confidentiality Section',
				'exclusivity' => 'Exclusivity Section',
				'jobdesc' => 'Job Description Section',
				'jobdescbottom' => 'Job Description Bottom Section',
				'insurance' => 'Insurance Cover Section',
				'facilities' => 'Facilities Section',
				'commission' => 'Commission Section',
				'lastpage' => 'Last Page Top'
			);
		}
		
        return ''; 
    }
	
	
}










