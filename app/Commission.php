<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Commission extends BaseModel 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'commission';
	
    protected $fillable = ['id', 'shortlistid', 'consultantid', 'clientid', 'propertyid', 'stamping', 'finishdate', 'invoicedate', 'invoicenumbers', 'commissiondate', 'salelease', 'landlordamount', 'tenantamount', 'type', 'status', 'comment', 'fullamount', 'source', 'tenantpaiddate', 'landlordpaiddate', 'sd_term', 'sd_rent', 'sd_keymoney', 'sd_amount'];
	
	protected $statustext = array(
		1 =>  'Submitted',
		2 =>  'Invoice Created',
		3 =>  'Invoiced',
		5 =>  'Waiting for Tenant Payment',
		6 =>  'Waiting for Landlord Payment',
		7 =>  'Waiting for Other Payment',
		8 =>  'Waiting for Several Payments',
		10 => 'Fully Paid',
		11 => 'Cancelled',
	);
	
    public function client() {
       return $this->belongsTo(Client::class, 'clientid'); 
    }
	
    public function property() {
       return $this->belongsTo(Property::class, 'propertyid')->withTrashed(); 
    }
	
    public function shortlist() {
       return $this->belongsTo(ClientShortlist::class, 'shortlistid'); 
    }
	
    public function consultant() {
       return $this->belongsTo(User::class, 'consultantid'); 
    }
	
	public function getStatus(){
		if (isset($this->statustext[$this->status])){
			return $this->statustext[$this->status];
		}else{
			return '';
		}
	}
	
	
	
	
	
	
	
}