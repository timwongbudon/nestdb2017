<?php

class PropertiesController
{
    /**
     * @SWG\Get(
     *     path="/property/{property_id}",
     *     description="Returns one property",
     *     operationId="property",
     *     produces={"application/json"},
     *     
     *     @SWG\Parameter(
     *         name="property_id",
     *         in="path",
     *         description="Property id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Property response",
     *         @SWG\Schema(
     *             ref="#/definitions/PropertyResult"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/ErrorModel"
     *         )
     *     )
     * )
     */
     public function property()
     {
     }

    /**
     * @SWG\Get(
     *     path="/properties",
     *     description="Returns all properties from the system that the user has access to",
     *     operationId="properties",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="type_id",
     *         in="query",
     *         description="Type ID - Rent for HK(1) / Sale for HK(2) / International (4)",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="keywords",
     *         in="query",
     *         description="Keywords",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="districts",
     *         in="query",
     *         description="District IDs",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="price_min",
     *         in="query",
     *         description="Price min.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="price_max",
     *         in="query",
     *         description="Price max.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="size_min",
     *         in="query",
     *         description="Size min.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="size_max",
     *         in="query",
     *         description="Size max.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="bedroom_min",
     *         in="query",
     *         description="Bedroom min.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="bedroom_max",
     *         in="query",
     *         description="Bedroom max.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="bathroom_min",
     *         in="query",
     *         description="Bathroom min.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="bathroom_max",
     *         in="query",
     *         description="Bathroom max.",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),     
     *     @SWG\Parameter(
     *         name="features",
     *         in="query",
     *         description="Feature IDs",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),    
     *     @SWG\Parameter(
     *         name="outdoors",
     *         in="query",
     *         description="Outdoor IDs",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number",
     *         required=false,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="query",
     *         description="Sort by i.e. date, rental-asc, rental-desc, gross-area-asc, gross-area-desc",
     *         required=false,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         name="included_id",
     *         in="query",
     *         description="IDs",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="leased",
     *         in="query",
     *         description="lease i.e. all=empty, yes=1, no=0",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Property response",
     *         @SWG\Schema(
     *             ref="#/definitions/PropertyListingResult"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/ErrorModel"
     *         )
     *     )
     * )
     */
    public function properties()
    {
    }
}
