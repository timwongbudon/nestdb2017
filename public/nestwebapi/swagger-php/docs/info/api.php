<?php

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     host="db.nest-property.com/cms",
 *     schemes={"http"},
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Nest Property API docs (for internal user only)",
 *         description="It is an API that integrates between WordPress site and Nest Property Database.",
 *         termsOfService="tbc",
 *     ),
 *     @SWG\Definition(
 *         definition="ErrorModel",
 *         type="object",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */