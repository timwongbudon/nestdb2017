<?php

/**
 * @SWG\Definition(
 *   required={},
 * )
 */
class PropertyResult
{
    /**
     * @SWG\Property(type="string")
     */
    public $error; 
    /**
     * @SWG\Property(ref="#/definitions/Property")
     */
    public $data;    
}
