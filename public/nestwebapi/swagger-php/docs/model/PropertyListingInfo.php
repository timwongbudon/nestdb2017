<?php

/**
 * @SWG\Definition(
 *   required={},
 * )
 */
class PropertyListingInfo
{
    /**
     * @SWG\Property(type="string")
     */
    public $total;  
    /**
     * @SWG\Property(type="string")
     */
    public $perPage;   
    /**
     * @SWG\Property(type="string")
     */
    public $currentPage;   
    /**
     * @SWG\Property(type="string")
     */
    public $lastPage;
}
