<?php

/**
 * @SWG\Definition(
 *   required={},
 * )
 */
class Property
{
    /**
     * @SWG\Property(type="string")
     */
    public $propertyId;  
    /**
     * @SWG\Property(type="string")
     */
    public $name;   
    /**
     * @SWG\Property(type="string")
     */
    public $address;   
    /**
     * @SWG\Property(type="string")
     */
    public $district;    
    /**
     * @SWG\Property(type="string")
     */
    public $typeId;  
    /**
     * @SWG\Property(type="string")
     */
    public $type; 
    /**
     * @SWG\Property(type="string")
     */
    public $grossSize; 
    /**
     * @SWG\Property(type="string")
     */
    public $saleableSize; 
    /**
     * @SWG\Property(type="string")
     */
    public $askingRent;   
    /**
     * @SWG\Property(type="string")
     */
    public $askingSale;  
    /**
     * @SWG\Property(type="string")
     */
    public $price;    
    /**
     * @SWG\Property(type="string")
     */
    public $bedroomCount;    
    /**
     * @SWG\Property(type="string")
     */
    public $bathroomCount;    
    /**
     * @SWG\Property(type="string")
     */
    public $maidroomCount;  
    /**
     * @SWG\Property(type="string")
     */
    public $outdoorCount;  
    /**
     * @SWG\Property(type="string")
     */
    public $carparkCount;  
    /**
     * @SWG\Property(type="array", items={"$ref":"#/definitions/Photo"})
     */
    public $photos;  
    /**
     * @SWG\Property(type="string")
     */
    public $featureString;  
    /**
     * @SWG\Property(type="string")
     */
    public $lat;   
    /**
     * @SWG\Property(type="string")
     */
    public $lng;   
    /**
     * @SWG\Property(type="string")
     */
    public $isLeased;   
    /**
     * @SWG\Property(type="string")
     */
    public $isForSale; 
    /**
     * @SWG\Property(type="string")
     */
    public $apiPath; 
}
