<?php

/**
 * @SWG\Definition(
 *   required={},
 * )
 */
class Photo
{
    /**
     * @SWG\Property(type="string")
     */
    public $photoId;  
    /**
     * @SWG\Property(type="string")
     */
    public $url;
 
    /**
     * @SWG\Property(type="string")
     */
    public $thumb;
}

