<?php

/**
 * @SWG\Definition(
 *   required={},
 * )
 */
class PropertyListingResult
{
    /**
     * @SWG\Property(type="string")
     */
    public $error;  
    /**
     * @SWG\Property(ref="#/definitions/PropertyListingInfo")
     */
    public $info;   
    /**
     * @SWG\Property(type="array", items={"$ref":"#/definitions/Property"})
     */
    public $data;    
}
