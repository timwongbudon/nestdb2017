<?php return array (
  'codeToName' => 
  array (
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    160 => 'space',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    173 => 'hyphen',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    177 => 'plusminus',
    180 => 'acute',
    181 => 'mu',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    256 => 'Amacron',
    257 => 'amacron',
    258 => 'Abreve',
    259 => 'abreve',
    260 => 'Aogonek',
    261 => 'aogonek',
    262 => 'Cacute',
    263 => 'cacute',
    264 => 'Ccircumflex',
    265 => 'ccircumflex',
    266 => 'Cdotaccent',
    267 => 'cdotaccent',
    268 => 'Ccaron',
    269 => 'ccaron',
    270 => 'Dcaron',
    271 => 'dcaron',
    272 => 'Dcroat',
    273 => 'dmacron',
    274 => 'Emacron',
    275 => 'emacron',
    276 => 'Ebreve',
    277 => 'ebreve',
    278 => 'Edotaccent',
    279 => 'edotaccent',
    280 => 'Eogonek',
    281 => 'eogonek',
    282 => 'Ecaron',
    283 => 'ecaron',
    284 => 'Gcircumflex',
    285 => 'gcircumflex',
    286 => 'Gbreve',
    287 => 'gbreve',
    288 => 'Gdotaccent',
    289 => 'gdotaccent',
    290 => 'Gcommaaccent',
    291 => 'gcommaaccent',
    292 => 'Hcircumflex',
    293 => 'hcircumflex',
    294 => 'Hbar',
    295 => 'hbar',
    296 => 'Itilde',
    297 => 'itilde',
    298 => 'Imacron',
    299 => 'imacron',
    300 => 'Ibreve',
    301 => 'ibreve',
    302 => 'Iogonek',
    303 => 'iogonek',
    304 => 'Idotaccent',
    305 => 'dotlessi',
    306 => 'IJ',
    307 => 'ij',
    308 => 'Jcircumflex',
    309 => 'jcircumflex',
    312 => 'kgreenlandic',
    313 => 'Lacute',
    314 => 'lacute',
    317 => 'Lcaron',
    318 => 'lcaron',
    319 => 'Ldot',
    320 => 'ldot',
    321 => 'Lslash',
    322 => 'lslash',
    323 => 'Nacute',
    324 => 'nacute',
    327 => 'Ncaron',
    328 => 'ncaron',
    330 => 'Eng',
    331 => 'eng',
    332 => 'Omacron',
    333 => 'omacron',
    334 => 'Obreve',
    335 => 'obreve',
    336 => 'Ohungarumlaut',
    337 => 'ohungarumlaut',
    338 => 'OE',
    339 => 'oe',
    340 => 'Racute',
    341 => 'racute',
    344 => 'Rcaron',
    345 => 'rcaron',
    346 => 'Sacute',
    347 => 'sacute',
    348 => 'Scircumflex',
    349 => 'scircumflex',
    350 => 'Scedilla',
    351 => 'scedilla',
    352 => 'Scaron',
    353 => 'scaron',
    356 => 'Tcaron',
    357 => 'tcaron',
    358 => 'Tbar',
    359 => 'tbar',
    360 => 'Utilde',
    361 => 'utilde',
    362 => 'Umacron',
    363 => 'umacron',
    364 => 'Ubreve',
    365 => 'ubreve',
    366 => 'Uring',
    367 => 'uring',
    368 => 'Uhungarumlaut',
    369 => 'uhungarumlaut',
    370 => 'Uogonek',
    371 => 'uogonek',
    372 => 'Wcircumflex',
    373 => 'wcircumflex',
    374 => 'Ycircumflex',
    375 => 'ycircumflex',
    376 => 'Ydieresis',
    377 => 'Zacute',
    378 => 'zacute',
    379 => 'Zdotaccent',
    380 => 'zdotaccent',
    381 => 'Zcaron',
    382 => 'zcaron',
    508 => 'AEacute',
    509 => 'aeacute',
    510 => 'Oslashacute',
    511 => 'oslashacute',
    710 => 'circumflex',
    711 => 'caron',
    728 => 'breve',
    729 => 'dotaccent',
    730 => 'ring',
    731 => 'ogonek',
    732 => 'tilde',
    733 => 'hungarumlaut',
    894 => 'semicolon',
    7808 => 'Wgrave',
    7809 => 'wgrave',
    7810 => 'Wacute',
    7811 => 'wacute',
    7812 => 'Wdieresis',
    7813 => 'wdieresis',
    7922 => 'Ygrave',
    7923 => 'ygrave',
    8211 => 'endash',
    8212 => 'emdash',
    8213 => 'afii00208',
    8215 => 'underscoredbl',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8219 => 'quotereversed',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8224 => 'dagger',
    8225 => 'daggerdbl',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8240 => 'perthousand',
    8242 => 'minute',
    8243 => 'second',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8252 => 'exclamdbl',
    8260 => 'fraction',
    8355 => 'franc',
    8356 => 'lira',
    8359 => 'peseta',
    8364 => 'Euro',
    8453 => 'afii61248',
    8470 => 'afii61352',
    8482 => 'trademark',
    8486 => 'Omega',
    8592 => 'arrowleft',
    8593 => 'arrowup',
    8594 => 'arrowright',
    8595 => 'arrowdown',
    8596 => 'arrowboth',
    8597 => 'arrowupdn',
    8616 => 'arrowupdnbse',
    8722 => 'minus',
    8725 => 'fraction',
    8729 => 'periodcentered',
    8734 => 'infinity',
    8747 => 'integral',
    8776 => 'approxequal',
    8800 => 'notequal',
    8801 => 'equivalence',
    8804 => 'lessequal',
    8805 => 'greaterequal',
    8976 => 'revlogicalnot',
    64257 => 'uniF001',
    64258 => 'uniF002',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Champagne & Limousines',
  'FullName' => 'Champagne & Limousines',
  'Version' => 'Version 1.00 April 6, 2009, initial release',
  'PostScriptName' => 'Champagne&Limousines',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '73',
  'UnderlinePosition' => '-106',
  'FontHeightOffset' => '85',
  'Ascender' => '904',
  'Descender' => '-162',
  'FontBBox' => 
  array (
    0 => '-293',
    1 => '-226',
    2 => '1243',
    3 => '904',
  ),
  'StartCharMetrics' => '379',
  'C' => 
  array (
    32 => 248,
    33 => 131,
    34 => 194,
    35 => 522,
    36 => 522,
    37 => 522,
    38 => 570,
    39 => 93,
    40 => 270,
    41 => 270,
    42 => 308,
    43 => 522,
    44 => 164,
    45 => 291,
    46 => 131,
    47 => 306,
    48 => 522,
    49 => 522,
    50 => 522,
    51 => 522,
    52 => 522,
    53 => 522,
    54 => 522,
    55 => 522,
    56 => 522,
    57 => 522,
    58 => 131,
    59 => 143,
    60 => 522,
    61 => 522,
    62 => 522,
    63 => 388,
    64 => 647,
    65 => 545,
    66 => 504,
    67 => 658,
    68 => 581,
    69 => 455,
    70 => 382,
    71 => 664,
    72 => 523,
    73 => 158,
    74 => 149,
    75 => 488,
    76 => 382,
    77 => 704,
    78 => 568,
    79 => 769,
    80 => 468,
    81 => 769,
    82 => 468,
    83 => 460,
    84 => 419,
    85 => 560,
    86 => 545,
    87 => 808,
    88 => 497,
    89 => 545,
    90 => 525,
    91 => 188,
    92 => 306,
    93 => 188,
    94 => 407,
    95 => 388,
    96 => 215,
    97 => 548,
    98 => 552,
    99 => 481,
    100 => 548,
    101 => 509,
    102 => 187,
    103 => 529,
    104 => 419,
    105 => 155,
    106 => 139,
    107 => 310,
    108 => 146,
    109 => 561,
    110 => 429,
    111 => 544,
    112 => 542,
    113 => 541,
    114 => 198,
    115 => 314,
    116 => 243,
    117 => 430,
    118 => 428,
    119 => 566,
    120 => 323,
    121 => 380,
    122 => 363,
    123 => 317,
    124 => 93,
    125 => 317,
    126 => 516,
    160 => 248,
    161 => 131,
    162 => 522,
    163 => 522,
    164 => 522,
    165 => 522,
    166 => 93,
    167 => 522,
    168 => 280,
    169 => 525,
    170 => 271,
    171 => 398,
    172 => 522,
    173 => 291,
    174 => 475,
    175 => 328,
    176 => 187,
    177 => 522,
    178 => 208,
    179 => 228,
    180 => 215,
    181 => 398,
    182 => 522,
    183 => 120,
    184 => 212,
    185 => 193,
    186 => 260,
    187 => 398,
    188 => 750,
    189 => 750,
    190 => 750,
    191 => 388,
    192 => 545,
    193 => 545,
    194 => 545,
    195 => 545,
    196 => 545,
    197 => 545,
    198 => 704,
    199 => 658,
    200 => 455,
    201 => 455,
    202 => 455,
    203 => 455,
    204 => 158,
    205 => 158,
    206 => 158,
    207 => 158,
    208 => 605,
    209 => 568,
    210 => 769,
    211 => 769,
    212 => 769,
    213 => 769,
    214 => 769,
    215 => 522,
    216 => 804,
    217 => 560,
    218 => 560,
    219 => 560,
    220 => 560,
    221 => 545,
    222 => 462,
    223 => 478,
    224 => 548,
    225 => 548,
    226 => 548,
    227 => 548,
    228 => 548,
    229 => 548,
    230 => 915,
    231 => 481,
    232 => 509,
    233 => 509,
    234 => 509,
    235 => 509,
    236 => 180,
    237 => 180,
    238 => 180,
    239 => 180,
    240 => 544,
    241 => 429,
    242 => 544,
    243 => 544,
    244 => 544,
    245 => 544,
    246 => 544,
    247 => 522,
    248 => 527,
    249 => 430,
    250 => 430,
    251 => 430,
    252 => 430,
    253 => 380,
    254 => 588,
    255 => 380,
    256 => 545,
    257 => 548,
    258 => 545,
    259 => 548,
    260 => 545,
    261 => 548,
    262 => 658,
    263 => 481,
    264 => 658,
    265 => 481,
    266 => 658,
    267 => 481,
    268 => 658,
    269 => 481,
    270 => 581,
    271 => 620,
    272 => 616,
    273 => 559,
    274 => 455,
    275 => 509,
    276 => 455,
    277 => 509,
    278 => 455,
    279 => 509,
    280 => 455,
    281 => 507,
    282 => 455,
    283 => 509,
    284 => 664,
    285 => 529,
    286 => 664,
    287 => 529,
    288 => 664,
    289 => 529,
    290 => 664,
    291 => 529,
    292 => 523,
    293 => 419,
    294 => 523,
    295 => 410,
    296 => 158,
    297 => 180,
    298 => 158,
    299 => 180,
    300 => 158,
    301 => 180,
    302 => 158,
    303 => 145,
    304 => 158,
    305 => 180,
    306 => 308,
    307 => 293,
    308 => 149,
    309 => 129,
    312 => 350,
    313 => 382,
    314 => 146,
    317 => 382,
    318 => 214,
    319 => 382,
    320 => 215,
    321 => 425,
    322 => 184,
    323 => 568,
    324 => 429,
    327 => 568,
    328 => 429,
    330 => 568,
    331 => 432,
    332 => 769,
    333 => 544,
    334 => 769,
    335 => 544,
    336 => 769,
    337 => 544,
    338 => 983,
    339 => 892,
    340 => 468,
    341 => 198,
    344 => 468,
    345 => 198,
    346 => 460,
    347 => 314,
    348 => 460,
    349 => 314,
    350 => 460,
    351 => 314,
    352 => 460,
    353 => 314,
    356 => 419,
    357 => 275,
    358 => 419,
    359 => 243,
    360 => 560,
    361 => 430,
    362 => 560,
    363 => 430,
    364 => 560,
    365 => 430,
    366 => 560,
    367 => 430,
    368 => 560,
    369 => 430,
    370 => 560,
    371 => 430,
    372 => 808,
    373 => 566,
    374 => 545,
    375 => 380,
    376 => 545,
    377 => 525,
    378 => 363,
    379 => 525,
    380 => 363,
    381 => 525,
    382 => 363,
    508 => 704,
    509 => 915,
    510 => 804,
    511 => 527,
    710 => 320,
    711 => 320,
    713 => 328,
    728 => 276,
    729 => 139,
    730 => 179,
    731 => 212,
    732 => 260,
    733 => 314,
    894 => 143,
    7808 => 808,
    7809 => 566,
    7810 => 808,
    7811 => 566,
    7812 => 808,
    7813 => 566,
    7922 => 545,
    7923 => 380,
    8211 => 333,
    8212 => 612,
    8213 => 1500,
    8215 => 388,
    8216 => 125,
    8217 => 125,
    8218 => 125,
    8219 => 125,
    8220 => 233,
    8221 => 233,
    8222 => 233,
    8224 => 336,
    8225 => 336,
    8226 => 1281,
    8230 => 1000,
    8240 => 771,
    8242 => 159,
    8243 => 237,
    8249 => 247,
    8250 => 247,
    8252 => 263,
    8254 => 388,
    8260 => 160,
    8319 => 27,
    8355 => 522,
    8356 => 522,
    8359 => 813,
    8364 => 522,
    8453 => 943,
    8470 => 775,
    8482 => 549,
    8486 => 631,
    8592 => 964,
    8593 => 437,
    8594 => 964,
    8595 => 437,
    8596 => 964,
    8597 => 437,
    8616 => 437,
    8722 => 522,
    8725 => 160,
    8729 => 120,
    8734 => 522,
    8747 => 243,
    8776 => 522,
    8800 => 522,
    8801 => 522,
    8804 => 522,
    8805 => 522,
    8976 => 522,
    61441 => 332,
    61442 => 335,
    64257 => 332,
    64258 => 335,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt2GOwHVkUhuF3TWw7ubFt27ZtGzfJ2LZt27Zt27Y9p05lMvmRO5kk594zmXmfqt67a9Xa3d/urv7TsJ1ykZs85CUf+SlAQQpRmCIUpRjFKUFJSlGaMpSlHOWpQEUqUZkqZFCValSnBjWpRW3qUJd61KcBDWlEY5rQlGY0pwUtaUVr2tCWdrSnAx3pRGe60JVudKcHPelFb/rQl370ZwADGcRghjCUYQxnBCMZxWjGMJZxjGcCE5nEZKYwlWlM397NJ/c/g5nMYjZzmMs85rOAhSxicWL/S1jKMpazgpWsYjVryGQt61jPzuzCruzG7uzBnuzF3uzDvuzH/hzAgRzEwRzCoRzG4RzBkRzF0RzDsRzH8ZzAiZzEyZzCqZzG6ZzBmZzF2ZzDuZzH+VzAhVzExVzCpVzG5VzBlVzF1VzDtVzH9dzAjdzEzdzCrdzG7dzBndzF3dzDvdzH/TzAgzzEwzzCozzG4zzBkzzF0zzDszzH87zAi7zEy7zCq7zG67zBm7zF27zDu7zH+3zAh3zEx3zCp3zG53zBl3zF13zDt8nn9R3f80Py7Ed+4md+4Vd+4/dIliJip8SYK3JHnsgb+SJ/FIiCUSgKR5EomuwoFsWjRJSMUlE6ykTZKBflk/UKUTEqReWoEhlRNapF9agRNaNW1I46UTfqRf1oEA2jUTSOJtE0mkXzaBEtU/D+d1DRKlpHm2ib7hz6S7SL9omxw9/2dIxO0Tm6RNfoljOp/sMy0h1AkiRJkiRJkiRJkiRJkiRJkiRJkpSV6B49omf0it7RJ91Zskv0jX7pzqAdWfSPATEwMQ+KwTEkhsawGB4jYmSiMipGx5gNXWM3s3Jc4hgfEzapTIxJiXFy4piySXVq9u5g82Jaiq4zPWYk55kbK7NSc+WcE7O32DEni/rcDfO8VOb5d4j5sSAWxqJYHEv+Uf/S7E6U82JZcvzzC83cWF+e5YoV23yvlVvZvypWJ+c1kbmlXm29WJvuBJIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkbYtYF+vTnUGSJEmSJEmSlHr+/5UkSfo/+QPbwZ9F',
  '_version_' => 6,
);