@extends('layouts.app')

@section('content')

<div class="nest-new admusers">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>CSV Import 
					@php
						$d = new DateTime();
						$d->setTimestamp($upload->tst);
						echo NestDate::nest_datetime_format($d->format('Y-m-d H:i:s'));
					@endphp
					</h4>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-xs-12">
					<div class="nest-buildings-search-wrapper row">
						<div class="col-sm-4">
							<b>Properties:</b> {{ $upload->pcnt }}
						</div>
						<div class="col-sm-4">
							<b>Waiting:</b> {{ $waiting }}
						</div>
						<div class="col-sm-4">
							<b>Processed:</b> {{ $processed }}
						</div> 
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@if (count($show) > 0)
	<div>
		<div class="nest-buildings-result-wrapper">
			@foreach ($show as $index => $s)
				<div class="row 
				@if ($index%2 == 1)
					nest-striped
				@endif
				">
					<div class="col-lg-5 col-md-5 col-sm-4">
						<b>Building:</b> {{ $s->building }}
						@if ($s->status > 1)
							<i class="fa fa-check-circle"></i>
						@endif
						<br />
						<b>Address:</b> {{ $s->address }}
					</div>
					<div class="col-lg-5 col-md-5 col-sm-4">
						<b>Unit:</b> {{ $s->unit }}<br />
						@if ($s->available != '0000-00-00')
							<b>Available:</b> {{ NestDate::nest_date_format($s->available, 'Y-m-d') }}
						@else
							<b>Available:</b> -
						@endif
					</div>
					<div class="col-lg-2 col-md-2 col-sm-4">
						<a class="nest-button nest-right-button btn btn-primary" href="{{ url('csvpshow/'.$s->id) }}">
							<i class="fa fa-btn fa-pencil-square-o"></i>
							@if ($s->status == 1)
								Process
							@else
								Show
							@endif
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			@endforeach
		</div>
		@if ($show->lastPage() > 1)
			<div class="panel-footer nest-buildings-pagination">
				{{ $show->links() }}
			</div>
		@endif
	</div>
		
@endif

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
</script>
@endsection
