@extends('layouts.app')

@section('content')

<div class="nest-new admusers">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<a href="{{ url('csvuploadshow/'.$show->cid) }}">back</a><br />
					<h4>{{ $show->building }}</h4>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-xs-12">
					<div class="nest-buildings-search-wrapper row">
						<div class="col-sm-4">
							<b>Building:</b> {{ $show->building }}<br />
							<b>Address:</b> {{ $show->address }}<br />
							<b>District:</b> {{ $show->district }}<br />
							<b>City:</b> {{ $show->city }}<br />
							<b>Unit:</b> {{ $show->unit }}<br />
							<b>Floor:</b> {{ $show->floor }}<br />
						</div>
						<div class="col-sm-4">							
							@if ($show->available != '0000-00-00')
								<b>Available:</b> {{ NestDate::nest_date_format($show->available, 'Y-m-d') }}
							@else
								<b>Available:</b> -
							@endif
							<br />
							<b>Size:</b> {{ $show->size }}<br />
							<b>Bedrooms:</b> {{ $show->bedrooms }}<br />
							<b>Bathrooms:</b> {{ $show->bathrooms }}<br />
							<b>Parking:</b> {{ $show->parking }}<br />
							<b>Year built:</b> {{ $show->yearbuilt }}<br />
						</div>
						<div class="col-sm-4">
							<b>Type:</b> 
							@if ($show->type == 1)
								Lease<br />
								<b>Lease:</b> {{ $show->lease }}<br />
							@elseif ($show->type == 2)
								Sale<br />
								<b>Sale:</b> {{ $show->sale }}<br />
							@elseif ($show->type == 3)
								Lease & Sale<br />
								<b>Lease:</b> {{ $show->lease }}<br />
								<b>Sale:</b> {{ $show->sale }}<br />
							@endif
							<b>Inclusive:</b> 
							@if ($show->inclusive == 1)
								Yes
							@else
								No
							@endif
							<br />
							<b>Management Fee:</b> {{ $show->mfee }}<br />
							<b>Government Rates:</b> {{ $show->govrates }}<br />
						</div> 
						<div class="clearfix"></div>
					</div>
					@if ($show->pid != null && $show->pid > 0)
						<b>Linked property:</b> <a href="{{ url('/property/edit/'.$show->pid) }}" target="_blank" style="display:inline;">Open</a>
					@endif
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-body" style="padding-bottom:15px;">
				<div class="col-xs-12" style="padding-bottom:10px;">
					If no buildings were found, please make a <a href="{{ url('/buildings') }}" target="_blank" style="display:inline;">custom building search</a> 
					to make sure there is really no existing building in the database. 
					If there is a building, simply copy the Building ID and add it in the text field below. If there is not, 
					<a href="{{ url('/building/create') }}" target="_blank" style="display:inline;">create a new building</a> and 
					then <a href="" style="display:inline;">reload this page here</a>. 
				</div>
				@if ($buildings->count() < 91)
					@foreach ($buildings as $index => $b)
						<div class="col-sm-4">
							<input type="radio" name="buildingradio" onclick="setBuildingID({{ $b->id }});" /> <a href="{{ url('/building/show/'.$b->id) }}" target="_blank" style="display:inline;">{{ $b->name }}</a>
						</div>
					@endforeach
				@endif
				<div class="col-sm-4">
					<input type="radio" name="buildingradio" onclick="setBuildingID('');" /> manually enter building ID
				</div>
			</div>
		</div>
	</div>
	<script>
		function setBuildingID(id){
			jQuery('#buildingid').val(id);
		}
	</script>
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-body" style="padding-bottom:15px;">
				<form action="{{ url('csvnewproperty/'.$show->id) }}" method="post" target="_blank">
					{{ csrf_field() }}
					<div class="col-xs-12" style="padding-bottom:5px;">
						If the property does not exist yet, simply add the Building ID below (if not automatically added by ticking one of the buildings above) 
						and click "Create new Property". The system will then create a new property based on the information provided above. 
					</div>
					<div class="col-sm-3 col-xs-6">
						<b>Building ID</b>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input id="buildingid" type="number" name="bid" class="form-control" value="" required />
					</div>
					<div class="col-sm-6">
						<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Create new Property</button>
					</div>
					<div class="clearfix"></div>
				</form>
				<form action="{{ url('csvupdateproperty/'.$show->id) }}" method="post" target="_blank">
					{{ csrf_field() }}
					<div class="col-xs-12" style="padding-bottom:5px;padding-top:20px;">
						If the property does already exist, simply add the Property ID below and click "Update existing Property". The system will then update 
						parts of the existing property record like bedrooms, availability date and lease/sale price + add a new comment with the additional information 
						provided in the uploaded document.
					</div>
					<div class="col-sm-3 col-xs-6">
						<b>Property ID</b>
					</div>
					<div class="col-sm-3 col-xs-6">
						@if ($show->pid != null && $show->pid > 0)
							<input id="propertyid" type="number" class="form-control" name="pid" value="{{ $show->pid }}" required />
						@else
							<input id="propertyid" type="number" class="form-control" name="pid" value="" required />
						@endif
					</div>
					<div class="col-sm-6">
						<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-edit"></i>Update existing Property</button>
					</div>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-body" style="padding-bottom:15px;">
				<form action="{{ url('csvstatusupdate/'.$show->id) }}" method="post">
					{{ csrf_field() }}
					@if ($show->status == 1)
						<input type="hidden" name="newstatus" value="2" />
						<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-lock"></i>Close Record</button>
					@else
						<input type="hidden" name="newstatus" value="1" />
						<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-unlock"></i>Reopen Record</button>
					@endif
				</form>
			</div>
		</div>
	</div>
</div>




<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
</script>
@endsection
