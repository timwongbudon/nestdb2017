@extends('layouts.app')

@section('content')

<div class="nest-new admusers">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>CSV Property Import</h4>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-xs-12">
					<form action="{{ url('csvupload') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="nest-buildings-search-wrapper row">
							<div class="col-sm-4">
								<b>Upload a new file</b>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-4">
								<input type="file" name="csvdoc" placeholder="New document" class="form-control">
							</div>
							<div class="col-sm-2">
								<button type="submit" name="action" value="upload" class="nest-button nest-right-button btn btn-default">
									<i class="fa fa-btn fa-arrow-up"></i>Upload </button>
							</div> 
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



@if (count($uploads) > 0)
	<div>
		<div class="nest-buildings-result-wrapper">
			@foreach ($uploads as $index => $up)
				<div class="row 
				@if ($index%2 == 1)
					nest-striped
				@endif
				">
					<div class="col-lg-5 col-md-5 col-sm-4">
						<b>Upload:</b> 
						@php
							$d = new DateTime();
							$d->setTimestamp($up->tst);
							echo NestDate::nest_datetime_format($d->format('Y-m-d H:i:s'));
						@endphp
					</div>
					<div class="col-lg-5 col-md-5 col-sm-4">
						<b>Properties:</b> {{ $up->pcnt }}
					</div>
					<div class="col-lg-2 col-md-2 col-sm-4">
						<a class="nest-button nest-right-button btn btn-primary" href="{{ url('csvuploadshow/'.$up->id) }}">
							<i class="fa fa-btn fa-pencil-square-o"></i>Show
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			@endforeach
		</div>
		@if ($uploads->lastPage() > 1)
			<div class="panel-footer nest-buildings-pagination">
				{{ $uploads->links() }}
			</div>
		@endif
	</div>
		
@endif

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
</script>
@endsection
