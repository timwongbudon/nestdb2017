@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>



<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
				<div class="col-lg-5 col-sm-5">
			@else
				<div class="col-lg-6 col-sm-6">
			@endif
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Upcoming Viewings</h4>
					</div>

					@if ($viewings && count($viewings) > 0)
						<div class="panel-body">
							<div class="nest-property-edit-row">
								@foreach ($viewings as $v)
									<div class="col-xs-5">
										<a href="{{url('/viewing/show/'.$v->id)}}">
											@if ($v->v_date == date('Y-m-d'))
												Today
											@elseif ($v->v_date > date('Y-m-d', strtotime("+6 day", time())))
												{{ $v->v_date() }}
											@else
												{{ $v->v_date_day() }}
											@endif
											{{ $v->getNiceTime() }}
										</a>
									</div>
									<div class="col-xs-5">
										{{ $v['client']->name() }}
									</div>
									<div class="col-xs-2">
										@if ($v->driver == 1)
											Driver
										@else
											No Driver
										@endif
									</div>
									<div class="clearfix"></div>
								@endforeach
							</div>
						</div>
					@else
						<div class="panel-body">
							No Viewings Scheduled
						</div>
					@endif
				</div>
			</div>
			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
				<div class="col-lg-4 col-sm-4">
			@else
				<div class="col-lg-6 col-sm-6">
			@endif
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Calendar</h4>
					</div>

					@if ($calendar && count($calendar) > 0)
						<div class="panel-body">
							<div class="nest-property-edit-row">
								@foreach ($calendar as $c)
									<div class="col-xs-7">
										@if ($c->type == 3)
											@if ($c->datefrom == $c->dateto)
												@if ($c->datefrom == date('Y-m-d'))
													Today
												@elseif ($c->datefrom > date('Y-m-d', strtotime("+6 day", time())))
													{{ $c->niceDateFrom() }}
												@else
													{{ $c->niceDayFrom() }}
												@endif
											@else
												@if ($c->datefrom == date('Y-m-d'))
													Today
												@elseif ($c->datefrom > date('Y-m-d', strtotime("+6 day", time())))
													{{ $c->niceDateFrom() }}
												@else
													{{ $c->niceDayFrom() }}
												@endif
												-
												@if ($c->dateto == date('Y-m-d'))
													Today
												@elseif ($c->dateto > date('Y-m-d', strtotime("+6 day", time())))
													{{ $c->niceDateTo() }}
												@else
													{{ $c->niceDayTo() }}
												@endif
											@endif
										@else
											@if ($c->datefrom == date('Y-m-d'))
												Today
											@elseif ($c->datefrom > date('Y-m-d', strtotime("+6 day", time())))
												{{ $c->niceDateFrom() }}
											@else
												{{ $c->niceDayFrom() }}
											@endif
											{{ $c->getNiceTime() }}
										@endif
									</div>
									<div class="col-xs-5">
										@if ($c->type == 1)
											<a href="{{url('/lead/edit/'.$c->leadid)}}">
												Handover
												@if (isset($calendarleads[$c->leadid]))
													({{ $calendarleads[$c->leadid]['client']->name() }})
												@endif
											</a>
										@elseif ($c->type == 3)
											Vacation
										@else
											{{ $c->driveroptions }}
										@endif
										@if (trim($c->comments) != '')
											({{ $c->comments }})
										@endif
									</div>
									<div class="clearfix"></div>
								@endforeach
							</div>
						</div>
					@else
						<br />
					@endif
				</div>
			</div>

			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
				<div class="col-lg-3 col-sm-3">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Consultant</div>
								</div>
								<div class="col-xs-8">
									<select id="consultant" class="form-control">
										<option value="0">Pick a Consultant</option>
										@foreach ($users as $u)
											@if ($u->isConsultant())
												<option value="{{ $u->id }}"
												@if ($u->id == $consultantid)
													selected
												@endif
												>
												@if ($u->status == 1)
													[inactive]
												@endif
												{{ $u->name }}
												</option>
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
								<script>
									jQuery(document).ready(function(){
										jQuery("#consultant").on('change', function(event){
											var val = jQuery("#consultant").val();
											if (val == 0){
												window.location = "{{ url('dashboardnew') }}";
											}else{
												window.location = "{{ url('dashboardnew') }}/"+val;
											}
										});
									});
								</script>
							</div>
						</div>
					</div>
				</div>
			@endif
			<div class="clearfix"></div>

			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">New</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 0)
										<div class="nest-dashboardnew-lead">
											@if ($lead->clientid > 0)
												<div class="col-xs-6">
													<span class="badge badge-danger">New</span>&nbsp;
													<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
														{{ $lead['client']->name() }}
													</a>
												</div>
												<div class="col-xs-2">{{ $lead->typeOut() }}</div>
												<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
												<div class="clearfix"></div>
											@else
												<div class="col-xs-6"><span class="badge badge-danger">New</span>&nbsp; <a href="{{ url('/lead/enterclient/'.$lead->id) }}">{{ $lead->getWebsiteFormName() }}</a></div>
												<div class="col-xs-2">{{ $lead->typeOut() }}</div>
												<div class="col-xs-4 text-right">Pending</div>
												<div class="clearfix"></div>
											@endif
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Offering</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($offerings) && count($offerings) > 0)
								@foreach ($offerings as $lead)
									@if ($lead->status == 5)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">
												@if ($lead->offerprice > 0)
													{{ '$'.number_format($lead->offerprice, 2, '.', ',') }}
												@elseif (isset($offers[$lead->shortlistid]))
													<?php
														//Only works for lease yet, as there is no data about sales stored (no form for provisional sale & purchase agreement)
														$fields = json_decode($offers[$lead->shortlistid]->fieldcontents);
														echo $fields->f13 ? '$'.number_format($fields->f13, 2, '.', ',') : '';
													?>
												@endif
											</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Handover</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 6)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-5">
												@if (isset($commissions[$lead->shortlistid]))
													@if ($commissions[$lead->shortlistid]->status == 2)
														<span class="badge badge-danger">Created</span>&nbsp;
													@elseif ($commissions[$lead->shortlistid]->status == 10)
														<span class="badge badge-danger">Paid</span>&nbsp;
													@elseif ($commissions[$lead->shortlistid]->status > 2 && $commissions[$lead->shortlistid]->invoicedate != null && $commissions[$lead->shortlistid]->invoicedate != '0000-00-00' && $commissions[$lead->shortlistid]->invoicedate <= date('Y-m-d', time() - 30*24*60*60))
														<span class="badge badge-danger">Late</span>&nbsp;
													@endif
												@endif
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-3">
												{{ $lead->typeOut() }}
												@if (isset($handoverdates[$lead->id]))
													<br />{{ NestDate::nest_date_format($handoverdates[$lead->id], 'Y-m-d') }}
												@endif
											</div>
											<div class="col-xs-4 text-right">
												@if (isset($commissions[$lead->shortlistid]))
													Price: ${{ number_format($commissions[$lead->shortlistid]->fullamount, 2, '.', ',') }}<br />
													Comm.: ${{ number_format(($commissions[$lead->shortlistid]->landlordamount + $commissions[$lead->shortlistid]->tenantamount), 2, '.', ',') }}
												@endif
											</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>


			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Hot</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 1)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Warm</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 2)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->lastoutreach == null || $lead->lastoutreach == '0000-00-00' || $lead->lastoutreach <= date('Y-m-d', time() - 7*24*60*60))
													<span class="badge badge-danger">Follow Up</span>&nbsp;
												@elseif ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Cold</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 3)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->lastoutreach == null || $lead->lastoutreach == '0000-00-00' || $lead->lastoutreach <= date('Y-m-d', time() - 7*24*60*60))
													<span class="badge badge-danger">Follow Up</span>&nbsp;
												@elseif ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>



			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Sales</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 13)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Cupping Room</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 9)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Relocation</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 10)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>


			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Lease Renewal</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($leads) && count($leads) > 0)
								@foreach ($leads as $lead)
									@if ($lead->status == 12)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-6">
												@if ($lead->remindme != null && $lead->remindme != '0000-00-00' && $lead->remindme <= date('Y-m-d'))
													<span class="badge badge-primary">Follow Up</span>&nbsp;
												@endif
												<a href="{{ url('/lead/edit/'.$lead->id) }}">
													@if (isset($clientpics[$lead->clientid]))
														<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
													@endif
													{{ $lead['client']->name() }}
												</a>
											</div>
											<div class="col-xs-2">{{ $lead->typeOut() }}</div>
											<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="background:#ffffff;">
					<div class="panel-heading" style="background:#ffffff;">
					   <h4 style="text-transform:uppercase;">Completed</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($completed) && count($completed) > 0)
								@foreach ($completed as $lead)
									<div class="nest-dashboardnew-lead">
										<div class="col-xs-6"><a href="{{ url('/lead/edit/'.$lead->id) }}">
											@if (isset($clientpics[$lead->clientid]))
												<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
											@endif
											{{ $lead['client']->name() }}
										</a></div>
										<div class="col-xs-2">{{ $lead->typeOut() }}</div>
										<div class="col-xs-4 text-right">
											@if (isset($commissions[$lead->shortlistid]))
												Price: ${{ number_format($commissions[$lead->shortlistid]->fullamount, 2, '.', ',') }}<br />
												Comm.: ${{ number_format(($commissions[$lead->shortlistid]->landlordamount + $commissions[$lead->shortlistid]->tenantamount), 2, '.', ',') }}
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							@endif
						</div>
						@if (!empty($completed) && count($completed) > 0 && $consultantid > 0)
							<div class="text-center" style="padding-top:10px;">
								<a class="nest-button btn btn-primary" href="{{ url('/lead/completed/'.$consultantid) }}">Show Full List</a>
							</div>
						@endif
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default" style="margin-bottom:10px;">
					<div class="panel-heading">
					   <h4 style="text-transform:uppercase;">Dead</h4>
					</div>

					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($dead) && count($dead) > 0)
								@foreach ($dead as $lead)
									<div class="nest-dashboardnew-lead">
										<div class="col-xs-6"><a href="{{ url('/lead/edit/'.$lead->id) }}">
											@if (isset($clientpics[$lead->clientid]))
												<img src="{{ url('/client/showpic/'.$clientpics[$lead->clientid]) }}" height="24px" />
											@endif
											{{ $lead['client']->name() }}
										</a></div>
										<div class="col-xs-2">{{ $lead->typeOut() }}</div>
										<div class="col-xs-4 text-right">{{ $lead->budgetOut() }}</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							@endif
						</div>
						@if (!empty($dead) && count($dead) > 0 && $consultantid > 0)
							<div class="text-center" style="padding-top:10px;">
								<a class="nest-button btn btn-primary" href="{{ url('/lead/dead/'.$consultantid) }}">Show Full List</a>
							</div>
						@endif
					</div>
				</div>
				<div class="text-center" style="padding-top:0px;padding-bottom:10px;">
					<a href="{{ url('/lead/deleted/'.$consultantid) }}">Deleted Enquiries</a>
				</div>
			</div>
			<div class="clearfix"></div>

			@if (count($opencommissions) > 0)
				<div class="col-sm-12">
					<div class="panel panel-default" style="background:#ffffff;">
						<div class="panel-heading" style="background:#ffffff;">
						   <h4 style="text-transform:uppercase;">Open Invoices</h4>
						</div>

						<div class="panel-body">
							<div class="nest-buildings-result-wrapper">
								@foreach ($opencommissions as $index => $comm)
									<div class="row" style="border-left:0px;border-right:0px;padding-left:0px;padding-right:0px;">
										<div style="margin-left:-5px;margin-right:-5px;">
											<div class="col-lg-3 col-md-3 col-sm-6">
												@if ($comm->status == 1)
													<span class="badge badge-danger">New</span>&nbsp;
												@elseif ($comm->status == 2)
													<span class="badge badge-danger">Created</span>&nbsp;
												@elseif ($comm->status < 10 && $comm->invoicedate != null && $comm->invoicedate != '0000-00-00' && $comm->invoicedate <= date('Y-m-d', time() - 14*24*60*60))
													<span class="badge badge-danger">Late</span>&nbsp;
												@endif
												<b>Property:</b> <a href="javascript:;"  data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $comm->propertyid }}, '{{ str_replace("'", "\'", $comm->property->name) }}', '')">{{ $comm->property->name }}</a><br />
												<b>Client:</b> <a href="{{ url('client/show/'.$comm->client->id) }}" target="_blank">{{ $comm->client->name() }}</a><br />
												<b>Transaction</b>:
												@if ($comm->shortlist->typeid == 2)
													Sale
												@else
													Lease
												@endif
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6">
												<b>Finished on:</b> {{ NestDate::nest_date_format($comm->finishdate, 'Y-m-d') }}<br />
												<b>Invoice Date:</b> {{ NestDate::nest_date_format($comm->invoicedate, 'Y-m-d') }}<br />
												<b>Offer Signing Date:</b> {{ NestDate::nest_date_format($comm->commissiondate, 'Y-m-d') }}<br />
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6">
												<b>Amount Landlord:</b> HK$ {{ number_format($comm->landlordamount, 2) }}
												@if ($comm->landlordamount > 0)
													@if (trim($comm->landlordpaiddate) == '' || trim($comm->landlordpaiddate) == '0000-00-00')
														<i class="fa fa-times" style="color:#cc0000;"></i>
													@else
														<i class="fa fa-check" style="color:#00cc00;"></i>
													@endif
												@endif
												<br />
												<b>Amount Tenant:</b> HK$ {{ number_format($comm->tenantamount, 2) }}
												@if ($comm->tenantamount > 0)
													@if (trim($comm->tenantpaiddate) == '' || trim($comm->tenantpaiddate) == '0000-00-00')
														<i class="fa fa-times" style="color:#cc0000;"></i>
													@else
														<i class="fa fa-check" style="color:#00cc00;"></i>
													@endif
												@endif
												<br />
												<b>Status:</b> {{ $comm->getStatus() }}
												@if ($comm->status == 7)
													<i class="fa fa-times" style="color:#cc0000;"></i>
												@endif
												<br />
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6">
												<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/invoiceedit/'.$comm->id) }}">
													<i class="fa fa-btn fa-pencil-square-o"></i> Edit
												</a>
												@if ($comm->shortlist->typeid == 2)
													<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('/documents/index-sale/'.$comm->shortlistid) }}">
														<i class="fa fa-btn fa-book"></i> Documents
													</a>
												@else
													<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('/documents/index-lease/'.$comm->shortlistid) }}">
														<i class="fa fa-btn fa-book"></i> Documents
													</a>
												@endif
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			@endif

			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div style="float:right;">
							<select id="analyticsyear" class="form-control" style="padding:3px 10px;" onChange="changeyear();">
								@for ($i = intval(date('Y')); $i >= 2018 ; $i--)
									@if ($i == $year)
										<option value="{{ $i }}" selected>{{ $i }}</option>
									@else
										<option value="{{ $i }}">{{ $i }}</option>
									@endif
								@endfor
							</select>
						</div>
					    <h4>Target (leasing)</h4>
					</div>

					<script>
						function changeyear(){
							var year = jQuery("#analyticsyear").val();

							window.location = "/dashboardnew/{{ $consultantid }}/"+year;
						}
					</script>

					<div class="panel-body">
						@if ($year < intval(date('Y')))
							<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
								<!-- <a href="javascript:;" id="chartlink1" onClick="showChart(1)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">{{ date('M Y') }}</a> -->
								<!-- <a href="javascript:;" id="chartlink2" onClick="showChart(2)" class="chartlinkbutton btn btn-white">Quarter</a> -->
								<a href="javascript:;" id="chartlink4" onClick="showChart(4)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">Annual</a>
								<!-- <a href="javascript:;" id="chartlink3" onClick="showChart(3)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Commission</a> -->
								<!-- <a href="javascript:;" id="chartlink5" onClick="showChart(5)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Deals</a> -->
							</div>
						@else
							<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
								<a href="javascript:;" id="chartlink1" onClick="showChart(1)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">{{ date('M Y') }}</a>
								<!-- <a href="javascript:;" id="chartlink2" onClick="showChart(2)" class="chartlinkbutton btn btn-white">Quarter</a> -->
								<a href="javascript:;" id="chartlink4" onClick="showChart(4)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">Annual</a>
								<!-- <a href="javascript:;" id="chartlink3" onClick="showChart(3)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Commission</a> -->
								<!-- <a href="javascript:;" id="chartlink5" onClick="showChart(5)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Deals</a> -->
							</div>
						@endif
						<script>
							function showChart(id){
								jQuery('.chartlinkbutton').removeClass('nest-button');
								jQuery('.chartlinkbutton').removeClass('btn-default');
								jQuery('.chartlinkbutton').removeClass('btn-white');
								jQuery('.chartlinkbutton').addClass('btn-white');
								jQuery('.dashboard-chart-wrapper-lease').css('display', 'none');
								jQuery('#chartlink'+id).addClass('nest-button');
								jQuery('#chartlink'+id).removeClass('btn-white');
								jQuery('#chartlink'+id).addClass('btn-default');
								jQuery('#lease'+id).css('display', 'block');
							}
						</script>
						@if ($year == intval(date('Y')))
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease1" style="padding-bottom:20px;">
								<div class="row">
									<div class="text-left col-md-6">
										<h4>HK$ {{ number_format($fulldone['monthdollar'], 0, '.', ',') }}</h4>
										<h5>Done</h5>
									</div>
									<div class="text-right col-md-6">
										<h4>HK$ {{ number_format($fulltargets['monthdollar'], 0, '.', ',') }}</h4>
										<h5>Target</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="progress progress-mini">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['monthdollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['monthdollar'] }}%">
												<span class="sr-only">{{ $fullpercent['monthdollar'] }}% Complete</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="text-left col-md-6">
										@if ($fulldone['monthdeals'] == 1)
											<h4>1 Deal</h4>
										@else
											<h4>{{ $fulldone['monthdeals'] }} Deals</h4>
										@endif
										<h5>Done</h5>
									</div>
									<div class="text-right col-md-6">
										@if ($fulltargets['monthdeals'] == 1)
											<h4>1 Deal</h4>
										@else
											<h4>{{ $fulltargets['monthdeals'] }} Deals</h4>
										@endif
										<h5>Target</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="progress progress-mini">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['monthdeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['monthdeals'] }}%">
												<span class="sr-only">{{ $fullpercent['monthdeals'] }}% Complete</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif
						<!--
						<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease2" style="display:none;">
							<div class="row">
								<div class="text-left col-md-6">
									<h4>HK$ {{ number_format($fulldone['quarterdollar'], 0, '.', ',') }}</h4>
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									<h4>HK$ {{ number_format($fulltargets['quarterdollar'], 0, '.', ',') }}</h4>
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['quarterdollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['quarterdollar'] }}%">
											<span class="sr-only">{{ $fullpercent['quarterdollar'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="text-left col-md-6">
									@if ($fulldone['quarterdeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulldone['quarterdeals'] }} Deals</h4>
									@endif
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									@if ($fulltargets['quarterdeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulltargets['quarterdeals'] }} Deals</h4>
									@endif
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['quarterdeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['quarterdeals'] }}%">
											<span class="sr-only">{{ $fullpercent['quarterdeals'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						-->
						@if ($year < intval(date('Y')))
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease4" style="padding-bottom:20px;">
						@else
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease4" style="display:none;padding-bottom:20px;">
						@endif
							<div class="row">
								<div class="text-left col-md-6">
									<h4>HK$ {{ number_format($fulldone['anndollar'], 0, '.', ',') }}</h4>
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									<h4>HK$ {{ number_format($fulltargets['anndollar'], 0, '.', ',') }}</h4>
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['anndollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['anndollar'] }}%">
											<span class="sr-only">{{ $fullpercent['anndollar'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="text-left col-md-6">
									@if ($fulldone['anndeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulldone['anndeals'] }} Deals</h4>
									@endif
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									@if ($fulltargets['anndeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulltargets['anndeals'] }} Deals</h4>
									@endif
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['anndeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['anndeals'] }}%">
											<span class="sr-only">{{ $fullpercent['anndeals'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						@if ($dollarsum > 0 && $monthcount > 1 && $year == intval(date('Y')))
							<div class="nest-commission-average">Average Previous {{ $monthcount }} Months: HK$ {{ number_format($dollarsum, 0, '.', ',') }}</div>
						@endif
						<h4>Commission Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-wrapper" id="lease3">
							<canvas id="allmonthsdollar" height="88px"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="col-sm-4">
				<div class="panel panel-default" style="margin-bottom:10px;padding-bottom:20px;">
					<div class="panel-heading">
						<h4>Target (sales)</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
							<a href="javascript:;" class="nest-button btn btn-default" style="border-radius:0px;">Annual</a>
						</div>
						<div class="dashboard-chart-wrapper" id="sale1">
							<div class="row">
								<div class="text-left col-md-6">
									<h4>HK$ {{ number_format($fulldone['saledollar'], 0, '.', ',') }}</h4>
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									<h4>HK$ {{ number_format($fulltargets['saledollar'], 0, '.', ',') }}</h4>
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['saledollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['saledollar'] }}%">
											<span class="sr-only">{{ $fullpercent['saledollar'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="text-left col-md-6">
									@if ($fulldone['saledeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulldone['saledeals'] }} Deals</h4>
									@endif
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									@if ($fulltargets['saledeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulltargets['saledeals'] }} Deals</h4>
									@endif
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['saledeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['saledeals'] }}%">
											<span class="sr-only">{{ $fullpercent['saledeals'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						@if ($dealsum > 0 && $monthcount > 1)
							<div class="nest-commission-average">Average Previous {{ $monthcount }} Months: {{ number_format($dealsum, 2, '.', ',') }} Deals</div>
						@endif
					   <h4>Deals Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-wrapper" id="lease5">
							<canvas id="allmonthsdeals" height="87px"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			
			<div class="panel panel-default">
				
				<div class="panel-heading">
				
					<h4>{{$year}} Outstanding Payments</h4>
				
				</div>
				
				<div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped" style="margin-bottom: 0px;">
                    <tr>	
                      					
                        <td width="15%" class="text-left">
                            <b>Consultant</b>
                        </td>
                        <td width="15%" class="text-left">
                            <b>Client</b>
                        </td>                     
                        <td width="10%" class="text-left">
                            <b>Property</b>
                        </td>

                        <td width="10%" class="text-center">
                            <b>Start Date</b>
                        </td>
                        
                        <td width="10%" class="text-center">
                            <b>Landlord Paid Date</b>
                        </td>

                        <td width="10%" class="text-center">
                            <b>Landlord Amount</b>
                        </td>

                        <td width="10%" class="text-center">
                            <b>Tenant Paid Date</b>
                        </td>
                                           
                        <td width="10%" class="text-center">
                            <b>Tenant Amount</b>
                        </td>
                    </tr>
                    <tbody id="">

                    @php $i = 0; 
                    $outstandingpayment1 = 0;
                    $outstandingTenant = 0;  
                    $outstandingLandloard = 0;      

                    @endphp

                    @foreach ($outstandingpayments as  $inv) 
                        @php 

                        $add = "&#10004;";
                        $add1 = "&#10004;";
                        $color = "green";
                        $color1 = "green";

                        if ($inv['lmonth'] > 0){                           
                            
                        }else{                          
                            $outstandingpayment1 += $inv['landlordamount'];                            
                            $add = "&#10006;";
                            $color = "red";
                            $outstandingLandloard += $inv['landlordamount'];        

                            if($inv['landlordamount'] > 0){  
                                $i++;                                
                            }
                        }

                        //Tenant
                        if ($inv['tmonth'] > 0){
                           
                        }else{                        
                            $outstandingpayment1 += $inv['tenantamount'];
                            $add1 = "&#10006;";
                            $color1 = "red";

                            $outstandingTenant += $inv['tenantamount'];  
                            
                            if($inv['tenantamount'] > 0){  
                                $i++;                                     
                            }
                        }

                        @endphp   
                        <tr {{$inv['cid']}}>
                            <td class="text-left">
                                {{$inv['cnfirstname']}} {{$inv['cnlastname']}}
                            </td>

                            <td class="text-left">
                                {{$inv['cfirstname']}} {{$inv['clastname']}}
                            </td>

                            <td class="text-left">
                                {{$inv['property']}}
                            </td>

                            <td class="text-center">                              
                            
                                @php             
                                if(isset($inv['startdate']) && !empty($inv['startdate'])){
                                    echo date('d.m.Y',strtotime($inv['startdate']));    
                                    
                                }
                                @endphp
                            </td>

                            <td class="text-center">
                                
                                @php
                                if( $inv['landlordpaiddate'] != '0000-00-00' && !empty($inv['landlordpaiddate']) ){
                                    echo date('d.m.Y',strtotime($inv['landlordpaiddate']));
                                }else {
                                    echo "-";
                                }
                                @endphp
                                
                                
                            </td>

                            <td class="text-center">       
                                @php
                                    if($inv['landlordamount'] > 0):
                                @endphp   

                                {{number_format($inv['landlordamount'], 0, '.', ',')}} <span style="color: {{$color}}; font-weight:bold; ">{{ $add }}</span>
                                @php else:  @endphp  
                                        -
                                @php endif; @endphp          
                            </td>

                            <td class="text-center">
                                @php
                                if( $inv['tenantpaiddate'] != '0000-00-00' && !empty($inv['tenantpaiddate']) ){
                                    echo date('d.m.Y',strtotime($inv['tenantpaiddate']));
                                } else {
                                    echo "-";
                                }
                                @endphp
                                
                            </td>

                            <td class="text-center">                                
                                @php
                                    if($inv['tenantamount'] > 0):
                                @endphp   

                                {{number_format($inv['tenantamount'], 0, '.', ',')}}  <span style="color: {{$color1}}; font-weight:bold; ">{{ $add1 }}</span>
                                
                                @php else:  @endphp  
                                        -
                                @php endif; @endphp             
                            </td>
                        </tr>
                    @endforeach
                  
                    </tbody>

                    </table>
                    </div>
            </div>
        </div>      
        Total Outstanding for Landlord: HK$ {{number_format($outstandingLandloard, 0, '.', ',')}}<br>
        Total Outstanding for Tenant: HK$ {{number_format($outstandingTenant, 0, '.', ',')}}<br><br>
     <strong>{{ $i }} Payments Outstanding: HK$ {{ number_format($outstandingpayment1, 0, '.', ',')}}       </strong>
    </div>


			</div>


<script>
	var ctx = document.getElementById("allmonthsdollar").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Earned Commission',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['dollardone']) }}]
			}, {
				label: 'Target',
				backgroundColor: '#cccccc',
				data: [{{ implode(',', $monthlyvalues['dollartarget']) }}]
			}]
		},
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				var year = {{ date('Y') }};
				window.location = "/commission/invoicing?year="+year+"&month="+month+"&user_id={{ $consultantid }}";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					min: 0,
					beginAtZero: true,
					stacked: true,
					ticks: {
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return '$' + value + '';
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						if (tooltipItem.datasetIndex == 0){
							label = 'Earned Commission HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						}else if (tooltipItem.datasetIndex == 1){
							label = 'Below Target HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						}

						return label;
					}
				}
			}
		}
	});

	var ctx = document.getElementById("allmonthsdeals").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Made Deals',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['dealsdone']) }}]
			}, {
				label: 'Target',
				backgroundColor: '#cccccc',
				data: [{{ implode(',', $monthlyvalues['dealstarget']) }}]
			}]
		},
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				//var year = {{  date('Y') }};
				var year = {{  $year }};
				window.location = "{{url('/commission/invoicing')}}"+"?year="+year+"&month="+month+"&user_id={{ $consultantid }}";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true,
					ticks: {
						min: 0,
						beginAtZero: true,
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return value;
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						if (tooltipItem.datasetIndex == 0){
							label = 'Made Deals: '+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						}else if (tooltipItem.datasetIndex == 1){
							label = 'Below Target: '+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						}

						return label;
					}
				}
			}
		}
	});

</script>

<style>
.chart-yaxis{
	color:#ff0000;
}
</style>

		</div>
	</div>
</div>
@endsection
