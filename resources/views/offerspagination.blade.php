
            @if (count($offers) > 0)
                @php
                    $prevKey=0;
                @endphp

                @foreach ($offers as  $key=> $offerrs)     

                 @foreach ($offerrs as $offer)    
                        @php
                        $hasChild = "";
                        $strike = "";
                        $parent = "";
                        $x ="-";
                      

                            if($key != $prevKey){
                                $prevKey  = $key;
                                $class = "parent";
                                $parent = "parent";
                                

                                if(count($offerrs) > 1){
                                    $hasChild = "haschild";
                                }
                                
                                if(!empty($offer['commissiondate']) && $offer['commissiondate']!=='-' ){
                                    $x = "<span style='color: green'>&#10004; </span>";
                                    $Invoice = true;
                                } else {
                                    $Invoice = false;
                                }
                              
                            }else {
                                $class = "child child".$key;     

                                if($Invoice == true){
                                    $x = "<span style='color: red'>&#10006</span>";
                                }                 
                                
                            }

                            if($offer['status'] == 8){
                                $x = "<span style='color: red'>&#10006</span>";
                            }
                            
                        @endphp
                    <tr id="{{$key}}" class="{{ $class }} {{ $hasChild }} {{$strike}}">

                        <td class="text-center">
                            @php if($hasChild!==""): @endphp
                                <button  class="showchild" data-class="{{$key}}">more</button>
                            @php else: @endphp
                                @php if($class == "child child".$key): @endphp
                                    &#8594;
                                @php endif; @endphp
                            @php endif; @endphp
                        </td>
                        <td class="text-left">
                            {{$offer['cnfirstname']}} {{$offer['cnlastname']}}
                        </td>
                        <td class="text-left">
                            {{$offer['cfirstname']}} {{$offer['clastname']}}
                        </td>
                        <td class="text-center">
                            @php                            
                                echo json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";
                            @endphp
                        </td>
                        <td class="text-center">
                        @php
                           // echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                          
                            echo $offer['commissiondate'];
                            @endphp
                        </td>
                        <td class="text-left">
                            <a href="{{url('/property/edit/'.$offer['propertyid'])}}">
                                {{$offer['property']}}
                            </a>
                        </td>
                        <td class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td class="text-center">
                            HK$&nbsp;
                            @php                 
                           
                           echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>

                        <td class="text-center">                            
                           @php        
                           echo $x;                       
                            /*   if($offer['status']!=="" && isset($statuses[$offer['status']]) && $statuses[$offer['status']] == "Completed"){
                                   echo "<span style='color:green; font-weight:bold'>Completed</span>";
                               } else {
                                 
                                   if(!empty($offer['status'])){
                                     echo $statuses[$offer['status']];
                                   } else {
                                       echo "-";
                                   }
                                   
                               }
                               */
                           @endphp
                        </td>
                        
                        <td class="text-center">                            
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                                                            
                    </tr>
                    @endforeach

                @endforeach
                <tr>
                <td colspan="6" class="text-left">{{ $offers2->links() }}</td>
                </tr>
            @else    
                <tr>
                    <td colspan="6" class="text-left">No data found.</td>
                </tr>
            @endif
      

      <style>
      .child{
          display: none;
          
      }
      .selected{
        background-color: #e6efe6  !important;
      }

      .showchild{
        font-size: 12px;
    border: 0;
    background: #86726a;
    color: #fff;
      }
      .strike{
        text-decoration: line-through;
      }
      </style>

<script>
$(document).ready(function(){
$('.showchild').click(function(){
   var parent = $(this).data('class');

   $(this).parent('tr').addClass('selected');
   $('#'+parent).toggleClass('selected');
   $('.child'+parent).toggle().addClass('selected');
});

});
</script>