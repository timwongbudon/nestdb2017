@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>


<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Year/Month</div>
							</div>
							<div class="col-xs-8">
								<select id="year" class="form-control">
									@foreach ($yearmonths as $y)
										<option value="{{ $y['y'].'-'.$y['m'] }}"
										@if ($y['y'] == $year && $y['m'] == $month)
											selected
										@endif
										>
											{{ $months[$y['m']].' '.$y['y'] }}
										</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
						
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Users</div>
							</div>
							<div class="col-xs-8">
								<select id="user" class="form-control">
									@foreach ($users2 as $y)
									<option value="{{ $y->id }}"
									@if ($selected_user == $y->id)
											selected
										@endif
									>{{ $y->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#year").on('change', function(event){
										var val = jQuery("#year").val();
										var user = jQuery("#user").val();
										window.location = "{{ url('companydbusage') }}/"+val+"/"+user;
									});

									jQuery("#user").on('change', function(event){
										var user = jQuery("#user").val();
										var val = jQuery("#year").val();
										window.location = "{{ url('companydbusage') }}/"+val+"/"+user;
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			
			@foreach ($metrics as $userid => $m)
				@if ($userid > 0)
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
							   <h4>{{ $usersbyid[$userid]->name }}</h4>
							</div>
							<div class="panel-body">
								<canvas id="user{{ $userid }}"></canvas>
								
								<script>
									var ctx = document.getElementById("user{{ $userid }}").getContext('2d');
									var cChart = new Chart(ctx, {
										type: 'scatter',
										data: {
											datasets: [
												{
													label: 'Login',
													data: [
														@foreach ($m['login'] as $index => $log)
															@if ($index > 0)
																, 
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@else
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@endif	
														@endforeach
													],
													backgroundColor: "#037d50",
													borderColor: "#037d50",
													hoverBackgroundColor: "#037d50",
													hoverBorderColor: "#037d50",
													pointRadius: 6,
													pointHoverRadius: 8
												},{
													label: 'Property Logs',
													data: [
														@foreach ($m['properties'] as $index => $log)
															@if ($index > 0)
																, 
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@else
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@endif	
														@endforeach
													],
													backgroundColor: "#ffa3a3",
													borderColor: "#ffa3a3",
													hoverBackgroundColor: "#ffa3a3",
													hoverBorderColor: "#ffa3a3",
													pointRadius: 6,
													pointHoverRadius: 8
												},{
													label: 'Enquiry Logs',
													data: [
														@foreach ($m['lead'] as $index => $log)
															@if ($index > 0)
																, 
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@else
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@endif	
														@endforeach
													],
													backgroundColor: "#accaff",
													borderColor: "#accaff",
													hoverBackgroundColor: "#accaff",
													hoverBorderColor: "#accaff",
													pointRadius: 6,
													pointHoverRadius: 8
												},{
													label: 'Document Logs',
													data: [
														@foreach ($m['documents'] as $index => $log)
															@if ($index > 0)
																, 
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@else
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@endif	
														@endforeach
													],
													backgroundColor: "#ffc8a2",
													borderColor: "#ffc8a2",
													hoverBackgroundColor: "#ffc8a2",
													hoverBorderColor: "#ffc8a2",
													pointRadius: 6,
													pointHoverRadius: 8
												},{
													label: 'Click',
													data: [
														@foreach ($m['click'] as $index => $log)
															@if ($index > 0)
																, 
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@else
																{
																	x: {{ $log['x'] }},
																	y: {{ $log['y'] }}
																}
															@endif	
														@endforeach
													],
													backgroundColor: "#cccccc",
													borderColor: "#cccccc",
													hoverBackgroundColor: "#cccccc",
													hoverBorderColor: "#cccccc",
													pointRadius: 4,
													pointHoverRadius: 4
												}
											]
										},
										options: {
											legend: {
												display: true,
												position: 'bottom'
											},
											scales: {
												yAxes: [{
													ticks: {
														max: {{ $lastdayofmonth }},
														min: 0,
														stepSize: 1,
														callback: function(value, index, values) {
															if (value == 1 || value == 21 || value == 31){
																return '{{ $months[$month] }} ' + value + 'st';
															}else if (value == 2 || value == 22){
																return '{{ $months[$month] }} ' + value + 'nd';
															}else if (value == 3 || value == 23){
																return '{{ $months[$month] }} ' + value + 'rd';
															}else if (value == 0){
																return '';
															}
															return '{{ $months[$month] }} ' + value + 'th';
														}
													},
													beginAtZero: true
												}],
												xAxes: [{
													beginAtZero: true,
													ticks: {
														max: 24,
														min: 0,
														stepSize: 1,
														callback: function(value, index, values) {
															return value+':00';
														}
													}
												}]
											},
											tooltips: {
												callbacks: {
													label: function(t, d) {
														var label = ' ' + Math.floor(t.xLabel) + ':';
														if (Math.floor((t.xLabel-Math.floor(t.xLabel))*60) < 10){
															label = label + '0' + Math.floor((t.xLabel-Math.floor(t.xLabel))*60)
														}else{
															label = label + '' + Math.floor((t.xLabel-Math.floor(t.xLabel))*60)
														}
														
														return label;
													}
												}
											}
										}
									});
								</script>
								
								
							</div>
						</div>
					</div>
				@endif
			@endforeach 
			
			
			
		</div>
	</div>
</div>
@endsection
