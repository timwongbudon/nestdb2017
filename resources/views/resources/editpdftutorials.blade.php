@extends('layouts.app')

@section('content')
<div class="nest-new container">
    <div class="row">
		<div class="nest-property-edit-wrapper">
            <div class="col-sm-6">
                <div class="panel panel-default">
					<div class="panel-heading d-flex">
						<h4>Add PDF Tutorials</h4>
					</div>
					<div class="panel-body">
                        <form action="{{url('/resources/pdf/save')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="file" name="pdfFile" accept=".pdf">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" role="button">Upload</button>
                            </div>
                        </form>
					</div>
				</div>
            </div>
            <div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading d-flex">
						<h4>PDF Tutorials List</h4>
					</div>
					<div class="panel-body">
                        <ul class="list-style-none pdf-list">
                            @foreach ($pdfs as $pdf)
                                <li>
                                    <a href="{{url('/resources/pdf/remove/'.$pdf->id)}}"><span class="fa fa-times text-danger mr-5" title="Remove"></span></a>
                                    <a href="{{ url('/pdfresource/'.str_replace('.pdf', '', $pdf->filename)) }}" target="_blank">{{ucwords($pdf->display_name)}}</a>
                                </li>    
                            @endforeach
                        </ul>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@endsection