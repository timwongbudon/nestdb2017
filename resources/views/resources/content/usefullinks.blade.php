@if (!empty($usefulLinks))
    {!!$usefulLinks->content!!}
@else
    <a href="https://www.esd.wsd.gov.hk/esd/aws/takeUp/init.do?pageFlag=1&USER_LANG=LANG_ENG" target="_blank">Water</a><br />
    <a href="https://aol.hkelectric.com/AOL/aol#/eforms/appl" target="_blank">Electricity</a><br />
    <a href="https://eservice.towngas.com/en/OpenCloseAccount/OpenGasAccount" target="_blank">Towngas</a><br />
    <br />
    <a href="http://www.eprc.com.hk/mlcLogin.html" target="_blank">EPRC</a><br />
    <a href="http://www1.centadata.com/ephome.aspx" target="_blank">Centadata</a><br />
    <br />
    <a href="http://www.ird.gov.hk/eng/tax/e_stamp.htm" target="_blank">Tenancy E-Stamping</a><br />
    <a href="http://www.gov.hk/en/residents/taxes/docs/IRSD123(E).pdf" target="_blank">Stamp Duty Rates Table</a><br />
    <a href="http://www.ird.gov.hk/eng/ese/sd_comp/sdpt_avd_2014.htm" target="_blank">Stamp Duty Calculator – Sale & Purchase</a><br />
    <a href="http://www.ird.gov.hk/eng/ese/sd_comp/sdta.htm" target="_blank">Stamp Duty Calculator — Tenancy Agreement</a><br />
    <br />
    <a href="http://www.icris.cr.gov.hk/csci" target="_blank">Companies Search</a><br />
    <a href="http://www1.iris.gov.hk/eservices/common/selectuser.jsp" target="_blank">Land Search</a><br />
    <a href="http://www.rvdpi.gov.hk/epayment/public/pihHome.do" target="_blank">Rating & Valuation Department</a><br />
    <a href="https://offshoreleaks.icij.org/" target="_blank">Offshore Leaks</a><br />
    <a href="https://bravo.bd.gov.hk/login" target="_blank">Building Records Access</a><br />
    <a href="https://www.mreferral.com/en/new-property/?utm_source=Promotion&utm_medium=SEM&utm_campaign=Regular&utm_term=2018DEC&gclid=EAIaIQobChMI7YL8h8qG4wIVl3ZgCh1XggL4EAAYASAAEgK8QvD_BwE" target="_blank">M Referral Motgage / Bank Valuation</a><br />
    <br />
    <a href="https://www.wikihow.com/Image:Work-out-a-Rental-Yield-Step-2.jpg" target="_blank">Rental Yield Calculation</a><br />
    <br />
    <a href="https://emojipedia.org/" target="_blank">Emojipedia</a><br />
    <br />

    Bank Valuation: 
    <a href="https://www.hsbc.com.hk/personal/mortgages/property-valuation-tool.html" target="_blank">HSBC</a>, 
    <a href="https://bank.hangseng.com/1/2/e-valuation/keyword-search" target="_blank">Hang Seng Bank</a>, 
    <a href="https://www.bochk.com/en/mortgage/tools/freevaluation.html" target="_blank">Bank of China</a>, 
    <a href="https://www.sc.com/hk/borrow/mortgage-planner/property-valuation/" target="_blank">Standard Chartered</a>, 
    <a href="https://www.hkbea.com/html/en/bea-mortgage-residential-property-valuation.html" target="_blank">Bank of East Asia</a> 
    <br /><br />

    Corporate / Big Landlords:<br />
    <a href="www.signaturehomes.com.hk/vacancy" target="_blank">Signature Homes</a> (User name: signature, Password: listing)<br />
    <a href="http://www.feo.hku.hk/hkuhousing/1280_index3.html" target="_blank">HKU Properties</a><br />
    <a href="https://www.pokfulam.com.hk/en/for-lease/residential/3-headland-road/" target="_blank">Pokfulam Development</a><br />
    <a href="http://www.vervain.com.hk/#3" target="_blank">Vervain</a><br />
    <a href="http://www.easyrent.com.hk/" target="_blank">KHI Management</a><br />
    <a href="http://www.chinafavourproperties.com/search_d.php?type=2&letting_price=0&selling_price=0&location=&no_bedroom=&no_maid_room=&no_parking_space=&x=29&y=3" target="_blank">China Favour Properties</a><br />
    <a href="http://www.yth.com/eng/index.html#" target="_blank">Yu Tai Hing</a><br />
    <a href="http://www.longhahfung.com/homes/" target="_blank">Long Hah Fung Properties</a><br />
    <a href="https://sohoprana.com/7princester5a/" target="_blank">Marie Le Masre de Chermont</a><br />
    <a href="https://sohoprintingpress.com/portfolio/" target="_blank">Soho Printing Press (Dare Koslow)</a><br />
    <a href="https://provestfund.com/augury-building" target="_blank">Provest Holdings</a><br />
    <a href="https://www.taipan8.com.hk/INDEX.asp" target="_blank">Taipan Group</a><br />
    <br />

    <a href="http://www.16seats.net/eng/gmb/gmb.html" target="_blank">Minibus Route Directory</a><br />
    <br />

    Competitor Sites:<br />
    <a href="https://www.hongkonghomes.com/en" target="_blank">HongKongHomes</a><br />
    <a href="http://www.habitat-property.com/" target="_blank">Habitat Property</a><br />
    <a href="https://www.okay.com/en/property-search/rent/sortby-updateddate" target="_blank">Okay.com</a><br />
    <a href="https://www.executivehomeshk.com/" target="_blank">Executive Homes</a><br />
    <a href="https://www.qi-homes.com/" target="_blank">Qi Homes</a><br />
    <a href="http://www.manksquarters.com/main/mqdeal.asp" target="_blank">Manks Quarters</a><br />
    <a href="https://www.landscope.com/" target="_blank">Landscope Christies</a><br />
    <a href="https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-rent/" target="_blank">AsiaXpat Direct Owner Apartments for Rent</a><br />
    <a href="https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-sale/" target="_blank">AsiaXpat Direct Owner Apartments for Sale</a><br />
    <a href="https://hongkong.asiaxpat.com/servicedapartments/#" target="_blank">AsiaXpat Serviced Apartments</a><br />
    <a href="https://www.28hse.com/en/" target="_blank">28Hse</a><br />
@endif
<br />