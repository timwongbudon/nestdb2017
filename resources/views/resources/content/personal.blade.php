@if (!empty($personal))
    {!!$personal->content!!}
@else
    <a href="http://www.ink-hk.com/hk/account_login.asp?returnPage=Repeat_Order.asp" target="_blank">Ink Orders</a><br />
    <br />

    Flights:<br />
    <a href="https://www.skyscanner.com.hk/?previousCultureSource=GEO_LOCATION&redirectedFrom=www.skyscanner.com" target="_blank">Skyscanner</a><br />
    <a href="https://www.flightcentre.com.hk/?gclid=EAIaIQobChMIjYWy-9GG4wIVgnZgCh28twzQEAAYASAAEgJ3UfD_BwE&gclsrc=aw.ds" target="_blank">FlightCentre</a> (Matt Brien - 2830 2887, 9711 9382)<br />
    <a href="https://www.qatarairways.com/en-hk/homepage.html" target="_blank">Qatar Airways</a><br />
    <a href="https://www.cathaypacific.com/cx/en_HK.html?gclid=EAIaIQobChMI2Ljdw9KG4wIVhmkqCh0CBgjUEAAYASAAEgJ9LvD_BwE&gclsrc=aw.ds" target="_blank">Cathay Pacific</a><br />
    <br />

    Cars:<br />
    Range Rover Sport: RS4984<br />
    Audi TT: NH5264<br />
    Alphard: NEST<br />
    <a href="https://www.gov.hk/en/residents/transport/vehicle/renewvehiclelicense.htm" target="_blank">Vehicle License Renewal</a><br />
    <br />

    Car Insurance:<br />
    Policy Number: 400113217<br />
    Contact: Priyanka CHAME, Abacare Hong Kong Limited<br />
    Phone: +852 28954449 (113)<br />
    Email: individualhk@abacare.com<br />
    <br />

    Pets:<br />
    Dog: Snuffy<br />
    Cat: Mia<br />
    <a href="http://www.creaturecomforts.com.hk/" target="_blank">Creature Comforts</a> (Dr. David – Dr. David 9773 0372 / 9323 1099)<br />
    <br />


    Home:<br />
    Jocelyn Cabero: +852 9549 9577<br />
    Irine Ducoy: +852 6469 6512<br />
    Irene Santos: +852 9145 9298<br />
    <br />

    Passports / ID:<br />
    Naomi Jade Budden<br />
    William Patrick Budden<br />
    <br />
    Ethan Blair Budden<br />
    Joshua Blake Budden<br />
    Bella Aster Saffron Budden<br />
    <br />

    Home Address:<br />
    House 5<br />
    Silver Strand Lodge<br />
    6 Silver Cape Road<br />
    Silverstrand<br />
    Hong Kong<br />
@endif
<br />