@extends('layouts.app')

@section('header-scripts')
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <style>
        #quillEditor {
            min-height: 250px;
        }
    </style>
@endsection

@section('content')
<div class="nest-new container">
    <div class="row">
		<div class="nest-property-edit-wrapper">
            <div class="col-sm-6">
                <div class="panel panel-default">
					<div class="panel-heading d-flex">
						<h4>Edit {{ucwords(str_replace('_', ' ', $key))}} </h4>
					</div>
					<div class="panel-body">
                        <form action="{{url('/resources/'.$key.'/save')}}" method="POST" enctype="multipart/form-data" name="frmResource">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div id="standalone-container">
                                    <div id="quill-toolbar-container">
                                        <span class="ql-formats">
                                            <select class="ql-font"></select>
                                            <select class="ql-size"></select>
                                        </span>
                                        <span class="ql-formats">
                                            <button class="ql-bold"></button>
                                            <button class="ql-italic"></button>
                                            <button class="ql-underline"></button>
                                            <button class="ql-strike"></button>
                                        </span>
                                        <span class="ql-formats">
                                            <select class="ql-color"></select>
                                            <select class="ql-background"></select>
                                        </span>
                                        <span class="ql-formats">
                                            <button class="ql-script" value="sub"></button>
                                            <button class="ql-script" value="super"></button>
                                        </span>
                                        <span class="ql-formats">
                                            <button class="ql-header" value="1"></button>
                                            <button class="ql-header" value="2"></button>
                                            <button class="ql-blockquote"></button>
                                            <button class="ql-code-block"></button>
                                        </span>
                                        <span class="ql-formats">
                                            <button class="ql-list" value="ordered"></button>
                                            <button class="ql-list" value="bullet"></button>
                                            <button class="ql-indent" value="-1"></button>
                                            <button class="ql-indent" value="+1"></button>
                                        </span>
                                        <span class="ql-formats">
                                            <button class="ql-direction" value="rtl"></button>
                                            <select class="ql-align"></select>
                                            <button class="ql-link"></button>
                                            <button class="ql-clean"></button>
                                        </span>
                                    </div>
                                    <div id="quillEditor"></div>
                                    <textarea class="form-control d-none" name="quillContent" hidden style="display: none;">
                                        @if (!empty($resource))
                                            {!!$resource->content!!}
                                        @elseif ($key == \App\ResourceContent::KEY_PERSONAL)
                                            <a href="http://www.ink-hk.com/hk/account_login.asp?returnPage=Repeat_Order.asp" target="_blank">Ink Orders</a><br>
                                            <br>
                                        
                                            Flights:<br>
                                            <a href="https://www.skyscanner.com.hk/?previousCultureSource=GEO_LOCATION&amp;redirectedFrom=www.skyscanner.com" target="_blank">Skyscanner</a><br>
                                            <a href="https://www.flightcentre.com.hk/?gclid=EAIaIQobChMIjYWy-9GG4wIVgnZgCh28twzQEAAYASAAEgJ3UfD_BwE&amp;gclsrc=aw.ds" target="_blank">FlightCentre</a> (Matt Brien - 2830 2887, 9711 9382)<br>
                                            <a href="https://www.qatarairways.com/en-hk/homepage.html" target="_blank">Qatar Airways</a><br>
                                            <a href="https://www.cathaypacific.com/cx/en_HK.html?gclid=EAIaIQobChMI2Ljdw9KG4wIVhmkqCh0CBgjUEAAYASAAEgJ9LvD_BwE&amp;gclsrc=aw.ds" target="_blank">Cathay Pacific</a><br>
                                            <br>
                                        
                                            Cars:<br>
                                            Range Rover Sport: RS4984<br>
                                            Audi TT: NH5264<br>
                                            Alphard: NEST<br>
                                            <a href="https://www.gov.hk/en/residents/transport/vehicle/renewvehiclelicense.htm" target="_blank">Vehicle License Renewal</a><br>
                                            <br>
                                        
                                            Car Insurance:<br>
                                            Policy Number: 400113217<br>
                                            Contact: Priyanka CHAME, Abacare Hong Kong Limited<br>
                                            Phone: +852 28954449 (113)<br>
                                            Email: individualhk@abacare.com<br>
                                            <br>
                                        
                                            Pets:<br>
                                            Dog: Snuffy<br>
                                            Cat: Mia<br>
                                            <a href="http://www.creaturecomforts.com.hk/" target="_blank">Creature Comforts</a> (Dr. David – Dr. David 9773 0372 / 9323 1099)<br>
                                            <br>
                                        
                                        
                                            Home:<br>
                                            Jocelyn Cabero: +852 9549 9577<br>
                                            Irine Ducoy: +852 6469 6512<br>
                                            Irene Santos: +852 9145 9298<br>
                                            <br>
                                        
                                            Passports / ID:<br>
                                            Naomi Jade Budden<br>
                                            William Patrick Budden<br>
                                            <br>
                                            Ethan Blair Budden<br>
                                            Joshua Blake Budden<br>
                                            Bella Aster Saffron Budden<br>
                                            <br>
                                        
                                            Home Address:<br>
                                            House 5<br>
                                            Silver Strand Lodge<br>
                                            6 Silver Cape Road<br>
                                            Silverstrand<br>
                                            Hong Kong<br>
                                            <br>
                                        @elseif ($key == \App\ResourceContent::KEY_USEFUL_LINKS)
<p><a href="https://www.esd.wsd.gov.hk/esd/aws/takeUp/init.do?pageFlag=1&amp;USER_LANG=LANG_ENG" target="_blank">Water</a></p><p><a href="https://aol.hkelectric.com/AOL/aol#/eforms/appl" target="_blank">Electricity</a></p><p><a href="https://eservice.towngas.com/en/OpenCloseAccount/OpenGasAccount" target="_blank">Towngas</a></p><p></p><p><a href="http://www.eprc.com.hk/mlcLogin.html" target="_blank">EPRC</a></p><p><a href="http://www1.centadata.com/ephome.aspx" target="_blank">Centadata</a></p><p></p><p><a href="http://www.ird.gov.hk/eng/tax/e_stamp.htm" target="_blank">Tenancy E-Stamping</a></p><p><a href="http://www.gov.hk/en/residents/taxes/docs/IRSD123(E).pdf" target="_blank">Stamp Duty Rates Table</a></p><p><a href="http://www.ird.gov.hk/eng/ese/sd_comp/sdpt_avd_2014.htm" target="_blank">Stamp Duty Calculator – Sale &amp; Purchase</a></p><p><a href="http://www.ird.gov.hk/eng/ese/sd_comp/sdta.htm" target="_blank">Stamp Duty Calculator — Tenancy Agreement</a></p><p></p><p><a href="http://www.icris.cr.gov.hk/csci" target="_blank">Companies Search</a></p><p><a href="http://www1.iris.gov.hk/eservices/common/selectuser.jsp" target="_blank">Land Search</a></p><p><a href="http://www.rvdpi.gov.hk/epayment/public/pihHome.do" target="_blank">Rating &amp; Valuation Department</a></p><p><a href="https://offshoreleaks.icij.org/" target="_blank">Offshore Leaks</a></p><p><a href="https://bravo.bd.gov.hk/login" target="_blank">Building Records Access</a></p><p><a href="https://www.mreferral.com/en/new-property/?utm_source=Promotion&amp;utm_medium=SEM&amp;utm_campaign=Regular&amp;utm_term=2018DEC&amp;gclid=EAIaIQobChMI7YL8h8qG4wIVl3ZgCh1XggL4EAAYASAAEgK8QvD_BwE" target="_blank">M Referral Motgage / Bank Valuation</a></p><p></p><p><a href="https://www.wikihow.com/Image:Work-out-a-Rental-Yield-Step-2.jpg" target="_blank">Rental Yield Calculation</a></p><p></p><p><a href="https://emojipedia.org/" target="_blank">Emojipedia</a></p><p><br></p><p>Bank Valuation: <a href="https://www.hsbc.com.hk/personal/mortgages/property-valuation-tool.html" target="_blank">HSBC</a>, <a href="https://bank.hangseng.com/1/2/e-valuation/keyword-search" target="_blank">Hang Seng Bank</a>, <a href="https://www.bochk.com/en/mortgage/tools/freevaluation.html" target="_blank">Bank of China</a>, <a href="https://www.sc.com/hk/borrow/mortgage-planner/property-valuation/" target="_blank">Standard Chartered</a>, <a href="https://www.hkbea.com/html/en/bea-mortgage-residential-property-valuation.html" target="_blank">Bank of East Asia</a></p><p><br></p><p>Corporate / Big Landlords:</p><p><a href="www.signaturehomes.com.hk/vacancy" target="_blank">Signature Homes</a>  (User name: signature, Password: listing)</p><p><a href="http://www.feo.hku.hk/hkuhousing/1280_index3.html" target="_blank">HKU Properties</a></p><p><a href="https://www.pokfulam.com.hk/en/for-lease/residential/3-headland-road/" target="_blank">Pokfulam Development</a></p><p><a href="http://www.vervain.com.hk/#3" target="_blank">Vervain</a></p><p><a href="http://www.easyrent.com.hk/" target="_blank">KHI Management</a></p><p><a href="http://www.chinafavourproperties.com/search_d.php?type=2&amp;letting_price=0&amp;selling_price=0&amp;location=&amp;no_bedroom=&amp;no_maid_room=&amp;no_parking_space=&amp;x=29&amp;y=3" target="_blank">China Favour Properties</a></p><p><a href="http://www.yth.com/eng/index.html#" target="_blank">Yu Tai Hing</a></p><p><a href="http://www.longhahfung.com/homes/" target="_blank">Long Hah Fung Properties</a></p><p><a href="https://sohoprana.com/7princester5a/" target="_blank">Marie Le Masre de Chermont</a></p><p><a href="https://sohoprintingpress.com/portfolio/" target="_blank">Soho Printing Press (Dare Koslow)</a></p><p><a href="https://provestfund.com/augury-building" target="_blank">Provest Holdings</a></p><p><a href="https://www.taipan8.com.hk/INDEX.asp" target="_blank">Taipan Group</a></p><p></p><p><a href="http://www.16seats.net/eng/gmb/gmb.html" target="_blank">Minibus Route Directory</a></p><p></p><p>Competitor Sites:</p><p><a href="https://www.hongkonghomes.com/en" target="_blank">HongKongHomes</a></p><p><a href="http://www.habitat-property.com/" target="_blank">Habitat Property</a></p><p><a href="https://www.okay.com/en/property-search/rent/sortby-updateddate" target="_blank">Okay.com</a></p><p><a href="https://www.executivehomeshk.com/" target="_blank">Executive Homes</a></p><p><a href="https://www.qi-homes.com/" target="_blank">Qi Homes</a></p><p><a href="http://www.manksquarters.com/main/mqdeal.asp" target="_blank">Manks Quarters</a></p><p><a href="https://www.landscope.com/" target="_blank">Landscope Christies</a></p><p><a href="https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-rent/" target="_blank">AsiaXpat Direct Owner Apartments for Rent</a></p><p><a href="https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-sale/" target="_blank">AsiaXpat Direct Owner Apartments for Sale</a></p><p><a href="https://hongkong.asiaxpat.com/servicedapartments/#" target="_blank">AsiaXpat Serviced Apartments</a></p><p><a href="https://www.28hse.com/en/" target="_blank">28Hse</a></p>
                                        @endif
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group d-flex">
                                <a href="{{url('/resources')}}" class="btn btn-primary">Back</a>
                                <button type="submit" class="btn btn-primary ml-auto" role="button">Save</button>
                            </div>
                        </form>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-script')
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <script type="text/javascript">

        var personalQuill;
        var quillContent;

        $(document).ready(function(e){

            personalQuill = new Quill('#quillEditor', {
                modules: {
                    formula: false,
                    syntax: false,
                    toolbar: '#quill-toolbar-container'
                },
                placeholder: 'Enter Content Here',
                theme: 'snow',
            });

            quillContent = $('textarea[name="quillContent"]').val();

            if (quillContent != '') {
                personalQuill.root.innerHTML = quillContent;
            }

        });

        $('form[name="frmResource"]').submit(function(e){
            quillContent = personalQuill.root.innerHTML;
            if (quillContent.length <= 11) {
                alert('Please create a content');
                return false;
            }
            $('textarea[name="quillContent"]').val(quillContent);
        });
    </script>
@endsection