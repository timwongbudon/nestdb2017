@extends('layouts.app')

@section('header-scripts')
	<style>
		.resource-content p {
			margin-bottom: 0;
		}
	</style>
@endsection

@section('content')

<div class="nest-new container">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading d-flex">
						<h4>PDF Tutorials</h4>
						@if (Auth::user()->getUserRights()['Director'])
							<div class="ml-auto d-inline-block">
								<a href="{{url('/resources/pdf/edit')}}">Edit</a>
							</div>
						@endif
					</div>
					<div class="panel-body">
						@foreach ($pdfs as $pdf)
							<a href="{{ url('/pdfresource/'.str_replace('.pdf', '', $pdf->filename)) }}" target="_blank">{{ucwords($pdf->display_name)}}</a><br />	
						@endforeach
					</div>
				</div>
				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer'])
					<div class="panel panel-default">
						<div class="panel-heading d-flex">
							<h4>Personal</h4>
							@if (Auth::user()->getUserRights()['Director'])
								<div class="ml-auto d-inline-block">
									<a href="{{url('/resources/'.\App\ResourceContent::KEY_PERSONAL.'/edit')}}">Edit</a>
								</div>
							@endif
						</div>
						<div class="panel-body resource-content">
							@include('resources.content.personal')
						</div>
					</div>
				@endif
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading d-flex">
						<h4>Useful Links</h4>
						@if (Auth::user()->getUserRights()['Director'])
							<div class="ml-auto d-inline-block">
								<a href="{{url('/resources/'.\App\ResourceContent::KEY_USEFUL_LINKS.'/edit')}}">Edit</a>
							</div>
						@endif
					</div>
					<div class="panel-body resource-content">
						@include('resources.content.usefullinks')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection
