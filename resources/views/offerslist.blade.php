@extends('layouts.app')

@section('content')

<div class="col-sm-12 nest-new dashboardnew"  >
	<div class=""></div>
<div class="panel panel-default nest-dashboardnew-wrapper">
    <div class="panel-heading">
        @if($yearlist == 'true')
            @if(app('request')->input('from') != null )
                <h4>Offers {{ date('d.m.Y',strtotime(app('request')->input('from'))) }} - {{date('d.m.Y',strtotime(app('request')->input('to'))) }}</h4>
            @else
                <h4>Offers {{ $year }}</h4>
            @endif

            
        @else
        <h4>Leaderboard Last 90 Days</h4>
        @endif
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	
                <td  width="2%"  class="text-left">
                
                </td>							
                <td width="15%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="15%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Date</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Offer</b>
                </td>	
                <td width="10%" class="text-center">
                    <b>Status</b>
                </td>
                <td width="10%" class="text-center">
                    <b></b>
                </td>
            </tr>
            <tbody id="offerspagination">
                @include('offerspagination')
            </tbody>
            </table>
            </div>
    </div>
</div>

    </div>

<div class="clearfix"></div>



<style>
td{
    font-size:13px;
}
</style>



<script>

$(document).ready(function(){

    $(document).on('click', '#offerspagination .pagination a', function(event){
        event.preventDefault(); 
        
        var page = $(this).attr('href').split('page=')[1];
        $('#offerspagination').html("<div class='ml-5'> Loading...</div>");		
        offers(page);
    });

    function offers(page)
    {
        @php
        echo 'var val= "'.$yearlist.'";';
        $from = request('from', $default = null);
        $to = request('to', $default = null);

        $date_filter = "";
        if($from !== null){
            $date_filter = '&from='.$from.'&to='.$to;
        }
        @endphp
        $.ajax({
        //url:"http://localhost/nestdbLocal/nestdb2017/public//companyoverview/offersfetch/{{$consultantid}}/{{$year}}/"+val+"?page="+page+"@php echo $date_filter @endphp",
        url:"/companyoverview/offersfetch/{{$consultantid}}/{{$year}}/"+1+"?page="+page,
        success:function(data)
        {									
            $('#offerspagination').html(data);
        },
        error:function(res){
            $('#offerspagination').html("Error");
        }
        });
    }



});
</script>

@endsection