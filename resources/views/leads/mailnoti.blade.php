<html>
    <head>
        <title>{{$title}}</title>
    </head>
    <body>
        Hello {{ $name }},
        <p style="text-align: left;">
            {{$username}} has added a comment to the following client enquiry: <a href="https://db.nest-property.com/lead/edit/{{$id}}">https://db.nest-property.com/lead/edit/{{$id}}</a><br><br>

            Consultant: {{$consultantName}} <br />
            Client Name: {{$client}} <br />
            The comment is: {{$comment}} <br />

        </p>
    </body>
</html>