@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Completed Enquiries</h4>
					</div>
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Consultant</div>
							</div>
							<div class="col-xs-8">
								<select id="consultant" class="form-control">
									@foreach ($users as $u)
										@if ($u->isConsultant())
											<option value="{{ $u->id }}" 
											@if ($u->id == $consultantid)
												selected
											@endif
											>
											@if ($u->status == 1)
												[inactive]
											@endif
											{{ $u->name }}
											</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#consultant").on('change', function(event){
										var val = jQuery("#consultant").val();
										if (val == 0){
											window.location = "{{ url('/lead/completed') }}";
										}else{
											window.location = "{{ url('/lead/completed') }}/"+val;
										}
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-dashboardnew-list" style="max-height:none;">
							@if (!empty($completed) && count($completed) > 0)
								<div class="nest-dashboardnew-lead">
									<div class="col-xs-1 text-left"><label>Date Created </label></div>
									<div class="col-xs-2 text-left"><label>Offer Date</label></div>
									<div class="col-xs-2 text-left"><label>Client</label></div>
									<div class="col-xs-2 text-left"><label>Property</label></div>
									<div class="col-xs-1 text-left"><label>Type</label></div>
									<div class="col-xs-2 text-left"><label>Handover Date</label></div>
									<div class="col-xs-2 text-right"><label>Price/Commission</label></div>
									<div class="clearfix"></div>
								</div>
								@foreach ($completed as $lead)
									<div class="nest-dashboardnew-lead">
										<div class="col-xs-1">{{ $lead->created_at() }}</div>
										<div class="col-xs-2">{{ NestDate::nest_date_format($lead->commissiondate, 'Y-m-d') }}</div>
										<div class="col-xs-2"><a href="{{ url('/lead/edit/'.$lead->id) }}">{{ $lead['client']->name() }}</a></div>
										<div class="col-xs-2"><a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $lead->propertyid }}, '{{ str_replace("'", "\'", $lead->name) }}', '')">
											{{ $lead->name }}
										</a></div>
										<div class="col-xs-1">{{ $lead->typeOut() }}</div>
										<div class="col-xs-2">
											@if (isset($handover[$lead->id]))
												{{ NestDate::nest_date_format($handover[$lead->id]->datefrom, 'Y-m-d') }}, {{ $handover[$lead->id]->timefrom }}
											@else
												&nbsp;
											@endif
										</div>
										<div class="col-xs-2 text-right">
											@if (isset($commissions[$lead->shortlistid]))
											@endif
										</div>
										<div class="col-xs-2 text-right">
											Price: ${{ number_format($lead->fullamount) }}<br />
											Comm.: ${{ number_format($lead->landlordamount+$lead->tenantamount) }}
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			
			
		</div>
	</div>
</div>
@endsection
