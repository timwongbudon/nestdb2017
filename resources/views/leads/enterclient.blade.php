@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Form Content</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list" style="max-height:none;">
							<div class="col-xs-3" style="padding-left:0px;"><label>Date</label></div>
							<div class="col-xs-9">{{ $form->getDateTime() }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Name</label></div>
							<div class="col-xs-9">{{ $form->name }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Phone</label></div>
							<div class="col-xs-9">{{ $form->phone }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Email</label></div>
							<div class="col-xs-9">{{ $form->email }}</div>
							<div class="clearfix"></div>
							@if ($lead->websiteid > 0)
								<div class="col-xs-3" style="padding-left:0px;"><label>Budget From</label></div>
								<div class="col-xs-9">{{ $form->budgetfrom }}</div>
								<div class="clearfix"></div>
								<div class="col-xs-3" style="padding-left:0px;"><label>Budget To</label></div>
								<div class="col-xs-9">{{ $form->budgetto }}</div>
								<div class="clearfix"></div>
							@endif
							<div class="col-xs-3" style="padding-left:0px;"><label>Message</label></div>
							<div class="col-xs-9">{{ $form->message }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Newsletter</label></div>
							<div class="col-xs-9">
								@if ($form->newsletter == 1)
									Yes
								@else
									No
								@endif
							</div>
							<div class="clearfix"></div>
							@if ($lead->websiteid > 0)
								<div class="col-xs-3" style="padding-left:0px;"><label>Property</label></div>
								<div class="col-xs-9"><a href="{{ $form->propertylink }}" target="_blank">{{ $form->propertylink }}</a></div>
								<div class="clearfix"></div>
							@endif
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Enter Client ID</h4>
					</div>
					
					<div class="panel-body">
						Please click <a href="/clients" target="_blank">here</a> and run through the following steps:
						<ol>
							<li>Use the name on the left to search for an existing client</li>
							<li>Use the email on the left to search for an existing client</li>
							<li>Use the phone number on the left to search for an existing client</li>
							<li>If no client exist, create a new one with the information provided on the left</li>
							<li>Once you have found an existing client or create a new one, copy the client ID into the field below and click "Submit"</li>
						</ol>
						
						<br />
						<form action="" method="POST">
							{{ csrf_field() }}
							<input type="hidden" value="{{ $lead->id }}" name="leadid" />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Client ID</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="clientid" id="clientid" class="form-control" value="" required>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="clearfix"></div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" style="margin-top:10px;"><i class="fa fa-btn fa-plus"></i> Submit </button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Comments</h4>
					</div>
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addComment();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
		</div>
	</div>
</div>

<script>	
	$(document).ready(function(){
		loadCommentsEdit();
	});
	
	function loadCommentsEdit(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/lead/comments/".$lead->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsEdit').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsEdit').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/lead/comments/".$lead->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsEdit').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/lead/comments/".$lead->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsEdit').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsEdit').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function addComment(){
		var comment = jQuery('#newcomment').val().trim();
		if (comment.length > 0){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/lead/commentadd/".$lead->id) }}',
				data: {comments: comment},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					jQuery('#newcomment').val('');
					loadCommentsEdit();
				},
				error: function (data) {
					console.log(data);
					alert('Something went wrong, please try again!');
				}
			});
		}else{
			alert('Please enter a comment');
		}
	}
	
	$('#newcomment').keydown(function( event ) {
		if ( event.keyCode == 13 ) {
			event.preventDefault();
			addComment();
		}				
	});
	
	function removeComment(t){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'POST',
			url: '{{ url("/lead/commentremove/".$lead->id) }}',
			data: {t: t},
			dataType: 'text',
			cache: false,
			async: false,
			success: function (data) {
				loadCommentsEdit();
			},
			error: function (data) {
				alert('Something went wrong, please try again!');
			}
		});
	}



	
	
</script>

@endsection
