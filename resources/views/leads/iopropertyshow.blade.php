@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading" style="padding-bottom:15px;">
						<h4>Message to Information Officer</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-6">
				{{ csrf_field() }}
				<div class="panel panel-default">
					<div class="panel-body">
						@include('common.errors')
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Comments</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{!! nl2br(preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-~]*(\?\S+)?)?)?)@', '<a href="$1" target="_blank">$1</a>', $iomessage->comments)) !!}
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Submitted By</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{{ $userarr[$iomessage->userid]->name }}
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Property Source</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{{ $iomessage->propertysource }}
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">PoC Name</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{{ $iomessage->pocname }}
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">PoC Email</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{{ $iomessage->pocemail }}
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">PoC Tel</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{{ $iomessage->poctel }}
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Check Database?</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								@if ($iomessage->dcheckdb == 1)
									Yes
								@else
									No
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">HK Homes Link</div>
							</div>
							<div class="col-xs-8" style="padding-top:8px;">
								{{ $iomessage->hkhomeslink }}
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						
						
						
						
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<form action="{{ url('iopropertycopyimgs') }}" method="POST" target="_blank">
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-body">
							@include('common.errors')
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Nest Photos</div>
								</div>
								<div class="col-xs-8">
									@if (count($nestpics) > 0)
										@foreach ($nestpics as $img)
											<a href="{{ $img->url }}" target="_blank"><img src="{{ $img->url }}" height="40px" /></a>
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							
							<br />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Other Photos</div>
								</div>
								<div class="col-xs-8">
									@if (count($otherpics) > 0)
										@foreach ($otherpics as $img)
											<a href="{{ $img->url }}" target="_blank"><img src="{{ $img->url }}" height="40px" /></a>
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<br />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Copy to Property</div>
								</div>
								<div class="col-xs-4">
									<input type="text" placeholder="Property ID" name="propertyid" class="form-control" value="">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<input type="hidden" name="id" value="{{ $id }}" />
							
							<br />
							<div class="nest-property-edit-row">
								<div class="col-xs-12">
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-copy"></i>Copy </button>
								</div>
							</div>
						</div>
					</div>
				</form>
				@if ($iomessage->status == 0)
					<div class="panel panel-default">
						<div class="panel-body">
							<a href="{{ url('/iopropertyfinish/'.$id) }}" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-check"></i> Finished</a>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	
	$(document).ready(function(){
		//auto increase by a certain amount
		$("#property-budget").on('keydown', function(event) { 
			var keyCode = event.keyCode || event.which; 
			if (keyCode == 9) { //tab keydown
				var type = jQuery('[name="type"]:checked').val();
				var res = parseFloat($(this).val());
				if (type == 2){
					if (res < 1000000) {
						$(this).val(res*1000000);
					}
				}else{
					if (res < 1000) {
						$(this).val(res*1000);
					}
				}
			} 
		});
		
	});
	
</script>

@endsection

@section('footer-script')
@endsection