
<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker(
		{ dateFormat: 'yy-mm-dd' }
	);
  } );


  $(document).ready(function(){
	  $('.filter-date').click(function(){
			var from = $('.datepicker.from').val();
			var to = $('.datepicker.to').val();

			window.location = "{{ url('companyoverview') }}?from="+from+"&to="+to;
	  });
	  $('.bfilter-date').click(function(){
			var from = $('.datepicker.bfrom').val();
			var to = $('.datepicker.bto').val();

			window.location = "{{ url('companyoverview') }}?from="+from+"&to="+to;
	  });
  });
  </script>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Year</div>
							</div>
							<div class="col-xs-8">
								<select id="year" class="form-control">
									@foreach ($years as $y)
										<option value="{{ $y }}"
										@if ($y == $year)
											selected
										@endif
										>
											{{ $y }}
										</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#year").on('change', function(event){
										var val = jQuery("#year").val();
										window.location = "{{ url('companyoverview') }}/"+val;
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-offset-4 col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Consultant</div>
							</div>
							<div class="col-xs-8">
								<select id="consultant" class="form-control">
									<option value="0">Company Overview</option>
									@foreach ($users as $u)
										@if ($u->isConsultant())
											<option value="{{ $u->id }}">
											@if ($u->status == 1)
												[inactive]
											@endif
											{{ $u->name }}
											</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#consultant").on('change', function(event){
										var val = jQuery("#consultant").val();
										var year = jQuery("#year").val();

										window.location = "{{ url('companyoverviewuser') }}/"+val+"/"+year;
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 style="float:left;">Leaderboard {{ $daterange == null ? $year : $daterange }}</h4>
						
						<div style="float:right; width:60%; ">
							<a style="float:right; width: 13%; margin-right:10px; height: 25px; padding:0px; font-size: 12px;    padding-top: 5px;" href="{{url('companyoverview')}}" class="nest-button btn btn-default">Reset</a>
							<input type="button" style="float:right; width: 13%; margin-right:10px; height: 25px; padding:0px; font-size: 12px;    padding-top: 2px;" value="Filter" class="nest-button btn btn-default filter-date" >
							<input type="text" style="float:right; width: 30%; margin-right:10px; height: 25px;" class="datepicker to form-control" value="{{$to}}"  placeholder="To">
							<input type="text" style="float:right; width: 30%; margin-right:10px; height: 25px;" class="datepicker from form-control" value="{{$from}}" placeholder="From">
							
						</div>
						<div class="clearfix"></div>
						
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<tr>
								<td width="20%"></td>
								<td width="15%" class="text-center">
									<b>Name</b>
								</td>
								<td width="15%" class="text-center">
									<b>Commission</b>
								</td>
								<td width="10%" class="text-center">
									<b>Invoices</b>
								</td>
								<td width="25%" class="text-center">
									<b>Average Rental </b>
								</td>
								<td width="10%" class="text-center">
									<b>Offers</b>
								</td>
								<td width="10%" class="text-center">
									<b>Viewings</b>
								</td>
							</tr>
							@if (count($lead1) > 0)
								@php
									$totalcom = 0;
									$totalinvoices = 0;
									$totalAveCom = 0;
									$totalOffer = 0;
									$totalviewings = 0;
									$AveRental = 0;
									$totalAveRental = 0;
									$count = 0;
								@endphp

								@foreach ($lead1 as $l)									

									@if($year >= 2021 &&  $daterange==null)
									
										@if( $usersbyid[$l['id']]->id !== 7  && $usersbyid[$l['id']]->id !== 6   )

										@php
											$totalcom = $totalcom + round($l['commission']);
											$totalinvoices = $totalinvoices + $l['invoices'];

											$AveCom =  $l['invoices'] ? $l['commission'] / $l['invoices'] : 0;
											$totalAveCom = $totalAveCom + $AveCom;
											
											$totalOffer = $totalOffer + $l['offers'];
											$totalviewings = $totalviewings + $l['viewings'];

											$AveRental =  $l['averental'] ? $l['averental'] : 0;
											$totalAveRental = $totalAveRental +round($AveRental);

											if($l['averental'] != '0'){
												$count++;
											}
											
										@endphp

										<tr>
											<td class="text-center">
												@if (isset($usersbyid[$l['id']]))
													<img src="/{{ $usersbyid[$l['id']]->picpath }}" width="60px" style="border-radius:30px;" />
												@endif
											</td>
											<td>
												@if (isset($usersbyid[$l['id']]))
													<a href="{{url('/dashboardnew/'.$l['id'])}}"  target="_blank">{!! $usersbyid[$l['id']]->name !!}</a>
												@endif
											</td>
											<td class="text-right">
												HK$&nbsp;{{ number_format($l['commission'], 0, '.', ',') }}
											</td>
											<td class="text-center">
												{{ $l['invoices'] }}
											</td>
											<td class="text-center">
												HK$ {{ number_format($l['averental']) }}
											</td>
											<td class="text-center">
												@if( ($l['offers'] !== 0) && (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director']))
													@php
													$from = request('from', $default = null);
													$to = request('to', $default = null);

													$date_filter = "";
													if($from !== null){
														$date_filter = '?from='.$from.'&to='.$to;
													}
													@endphp
													<a style="text-decoration: underline" href="{{url('/companyoverview/offers/'.$l['id'].'/'.$year.'/true')}} @php echo $date_filter @endphp">{{$l['offers']}}</a>
												@else
													{{ $l['offers'] }}												
												@endif									
												
											</td>
											<td class="text-center">
												{{ $l['viewings'] }}
											</td>
										</tr>
										@endif
										
									@else
										@php
											$totalcom = $totalcom + round($l['commission']);
											$totalinvoices = $totalinvoices + $l['invoices'];

											$AveCom =  $l['invoices'] ? $l['commission'] / $l['invoices'] : 0;
											$totalAveCom = $totalAveCom + $AveCom;
											
											$totalOffer = $totalOffer + $l['offers'];
											$totalviewings = $totalviewings + $l['viewings'];

											$AveRental =  $l['averental'] ? $l['averental'] : 0;
											$totalAveRental = $totalAveRental +round($AveRental);
											
											if($l['averental'] != '0'){
												$count++;
											}
											

											
										@endphp
										<tr>
											<td class="text-center">
												@if (isset($usersbyid[$l['id']]))
													<img src="/{{ $usersbyid[$l['id']]->picpath }}" width="60px" style="border-radius:30px;" />
												@endif
											</td>
											<td>
												@if (isset($usersbyid[$l['id']]))
													<a href="{{url('/dashboardnew/'.$l['id'])}}" target="_blank">{!! $usersbyid[$l['id']]->name !!}</a>
												@endif
											</td>
											<td class="text-right">
												HK$&nbsp;{{ number_format(ceil($l['commission']), 0, '.', ',') }}
											</td>
											<td class="text-center">
												{{ $l['invoices'] }}
											</td>
											<td class="text-center">
												HK$ {{ number_format($l['averental']) }}
											</td>
											<td class="text-center">
												@if( ($l['offers'] !== 0) && (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director']))
													@php
													$from = request('from', $default = null);
													$to = request('to', $default = null);

													$date_filter = "";
													if($from !== null){
														$date_filter = '?from='.$from.'&to='.$to;
													}
													@endphp
													<a style="text-decoration: underline" href="{{url('/companyoverview/offers/'.$l['id'].'/'.$year.'/true')}} @php echo $date_filter @endphp">{{$l['offers']}}</a>
												@else
													{{ $l['offers'] }}												
												@endif									
												
											</td>
											<td class="text-center">
												{{ $l['viewings'] }}
											</td>
										</tr>

									@endif

									
								@endforeach

								<tr>
									<td class="text-center"><img src="{{url('Nest_Logo.png')}}" width="80px"></td>
									<td><strong>Total Value:</strong></td>
									<td class="text-right"><strong>HK$&nbsp;{{ number_format($totalcom, 0, '.', ',') }}</strong></td>
									<td class="text-center"><strong>{{$totalinvoices}}</strong></td>
									@if ($count)
									<td class="text-center"><strong>HK$&nbsp;{{ number_format($totalAveRental / $count , 0, '.', ',') }}</strong></td>
									@endif
									<td class="text-center"><strong>{{$totalOffer}}</strong></td>
									<td class="text-center"><strong>{{$totalviewings}}</strong></td>
									
								</tr>
							@endif
						</table>
					</div>
				</div>

				

			</div>

			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					<h4>Consultant Overview</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
							@if ($year == intval(date('Y')))
								<a href="javascript:;" id="chartlink21" onClick="showChart2(1)" class="chartlinkbutton2 nest-button btn btn-default" style="border-radius:0px;">{{ date('M Y') }}</a>
								<a href="javascript:;" id="chartlink24" onClick="showChart2(4)" class="chartlinkbutton2 btn btn-white" style="border-radius:0px;">Annual</a>
							@else
								<a href="javascript:;" id="chartlink24" onClick="showChart2(4)" class="chartlinkbutton2 nest-button btn btn-default" style="border-radius:0px;">Annual</a>
							@endif
						</div>
						<script>
							function showChart2(id){
								jQuery('.chartlinkbutton2').removeClass('nest-button');
								jQuery('.chartlinkbutton2').removeClass('btn-default');
								jQuery('.chartlinkbutton2').removeClass('btn-white');
								jQuery('.chartlinkbutton2').addClass('btn-white');
								jQuery('.dashboard-chart-wrapper-lease2').css('display', 'none');
								jQuery('#chartlink2'+id).addClass('nest-button');
								jQuery('#chartlink2'+id).removeClass('btn-white');
								jQuery('#chartlink2'+id).addClass('btn-default');
								jQuery('#lease2'+id).css('display', 'block');
							}
						</script>
						@if ($year == intval(date('Y')))
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease2" id="lease21" style="padding-bottom:20px;">
								<div class="row">
									<div class="col-xs-12">
										<canvas id="piemonthly" height="85px"></canvas>
									</div>
								</div>
							</div>
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease2" id="lease24" style="display:none;padding-bottom:20px;">
						@else
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease2" id="lease24" style="padding-bottom:20px;">
						@endif
								<div class="row">
									<div class="col-xs-12">
										<canvas id="pieyearly" height="85px"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
						<h4>Target (leasing)</h4>
						</div>
						<div class="panel-body">
							<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
								@if ($year == intval(date('Y')))
									<a href="javascript:;" id="chartlink1" onClick="showChart(1)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">{{ date('M Y') }}</a>
									<a href="javascript:;" id="chartlink4" onClick="showChart(4)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">Annual</a>
								@else
									<a href="javascript:;" id="chartlink4" onClick="showChart(4)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">Annual</a>
								@endif
								<!-- <a href="javascript:;" id="chartlink2" onClick="showChart(2)" class="chartlinkbutton btn btn-white">Quarter</a> -->

								<!-- <a href="javascript:;" id="chartlink3" onClick="showChart(3)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Commission</a> -->
								<!-- <a href="javascript:;" id="chartlink5" onClick="showChart(5)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Deals</a> -->
							</div>
							<script>
								function showChart(id){
									jQuery('.chartlinkbutton').removeClass('nest-button');
									jQuery('.chartlinkbutton').removeClass('btn-default');
									jQuery('.chartlinkbutton').removeClass('btn-white');
									jQuery('.chartlinkbutton').addClass('btn-white');
									jQuery('.dashboard-chart-wrapper-lease').css('display', 'none');
									jQuery('#chartlink'+id).addClass('nest-button');
									jQuery('#chartlink'+id).removeClass('btn-white');
									jQuery('#chartlink'+id).addClass('btn-default');
									jQuery('#lease'+id).css('display', 'block');
								}
							</script>
							@if ($year == intval(date('Y')))
								<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease1" style="padding-bottom:20px;">
									<div class="row">
										<div class="text-left col-md-6">
											<h4>HK$ {{ number_format($fulldone['monthdollar'], 0, '.', ',') }}</h4>
											<h5>Done</h5>
										</div>
										<div class="text-right col-md-6">
											<h4>HK$ {{ number_format($fulltargets['monthdollar'], 0, '.', ',') }}</h4>
											<h5>Target</h5>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="progress progress-mini">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['monthdollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['monthdollar'] }}%">
													<span class="sr-only">{{ $fullpercent['monthdollar'] }}% Complete</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="text-left col-md-6">
											@if ($fulldone['monthdeals'] == 1)
												<h4>1 Deal</h4>
											@else
												<h4>{{ $fulldone['monthdeals'] }} Deals</h4>
											@endif
											<h5>Done</h5>
										</div>
										<div class="text-right col-md-6">
											@if ($fulltargets['monthdeals'] == 1)
												<h4>1 Deal</h4>
											@else
												<h4>{{ $fulltargets['monthdeals'] }} Deals</h4>
											@endif
											<h5>Target</h5>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="progress progress-mini">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['monthdeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['monthdeals'] }}%">
													<span class="sr-only">{{ $fullpercent['monthdeals'] }}% Complete</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease4" style="display:none;padding-bottom:20px;">
							@else
								<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease4" style="padding-bottom:20px;">
							@endif
								<div class="row">
									<div class="text-left col-md-6">
										<h4>HK$ {{ number_format($fulldone['anndollar'], 0, '.', ',') }}</h4>
										<h5>Done</h5>
									</div>
									<div class="text-right col-md-6">
										<h4>HK$ {{ number_format($fulltargets['anndollar'], 0, '.', ',') }}</h4>
										<h5>Target</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="progress progress-mini">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['anndollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['anndollar'] }}%">
												<span class="sr-only">{{ $fullpercent['anndollar'] }}% Complete</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="text-left col-md-6">
										@if ($fulldone['anndeals'] == 1)
											<h4>1 Deal</h4>
										@else
											<h4>{{ $fulldone['anndeals'] }} Deals</h4>
										@endif
										<h5>Done</h5>
									</div>
									<div class="text-right col-md-6">
										@if ($fulltargets['anndeals'] == 1)
											<h4>1 Deal</h4>
										@else
											<h4>{{ $fulltargets['anndeals'] }} Deals</h4>
										@endif
										<h5>Target</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="progress progress-mini">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['anndeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['anndeals'] }}%">
												<span class="sr-only">{{ $fullpercent['anndeals'] }}% Complete</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						
							
					</div>

					
				</div>

				<div class="panel panel-default" style="margin-bottom:10px;padding-bottom:20px;">
					<div class="panel-heading">
						<h4>Target (sales)</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
							<a href="javascript:;" class="nest-button btn btn-default" style="border-radius:0px;">Annual</a>
						</div>
						<div class="dashboard-chart-wrapper" id="sale1">
							<div class="row">
								<div class="text-left col-md-6">
									<h4>HK$ {{ number_format($fulldone['saledollar'], 0, '.', ',') }}</h4>
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									<h4>HK$ {{ number_format($fulltargets['saledollar'], 0, '.', ',') }}</h4>
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['saledollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['saledollar'] }}%">
											<span class="sr-only">{{ $fullpercent['saledollar'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="text-left col-md-6">
									@if ($fulldone['saledeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulldone['saledeals'] }} Deals</h4>
									@endif
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									@if ($fulltargets['saledeals'] == 1)
										<h4>1 Deal</h4>
									@else
										<h4>{{ $fulltargets['saledeals'] }} Deals</h4>
									@endif
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['saledeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['saledeals'] }}%">
											<span class="sr-only">{{ $fullpercent['saledeals'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				


				</div>
				

				
			</div>
			<div class="clearfix"></div>

			<div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 style="float:left; ">Bubble Chart  {{ $daterange == null ? $year : $daterange }}</h4>
						<div style="float:right; width:70%; ">
							<a style="float:right; width: 13%; margin-right:10px; height: 25px; padding:0px; font-size: 12px;    padding-top: 5px;" href="{{url('companyoverview')}}" class="nest-button btn btn-default">Reset</a>
							<input type="button" style="float:right; width: 13%; margin-right:10px; height: 25px; padding:0px; font-size: 12px;    padding-top: 2px;" value="Filter" class="nest-button btn btn-default bfilter-date" >
							
							<input type="text" style="float:right; width: 30%; margin-right:10px; height: 25px;" class="datepicker bto form-control" value="{{$to}}"  placeholder="To">							
							<input type="text" style="float:right; width: 30%; margin-right:10px; height: 25px;" class="datepicker bfrom form-control" value="{{$from}}"  placeholder="From">
						</div>
					</div>
					<div class="panel-body">
						<div class="wrapper"><canvas id="bubble-chart-1" height="100px"></canvas></div>

						<script>
							var chart = new Chart('bubble-chart-1', {
								type: 'bubble',
								data: {
									datasets: [
										@if (count($bubble1) > 0)
											@php
												$counter = 0;
											@endphp
											@foreach ($bubble1 as $cid => $b)
												@if (isset($usersbyid[$cid]))
													@if ($counter > 0)
														,
													@endif

													{
														label: '{!! $usersbyid[$cid]->name !!}',
														data: [{
															x: {{ $b['viewings'] }},
															y: {{ $b['offers'] }},
															r: {{ ($b['invoices']+2)*2 }}
														}],
														backgroundColor:"#{{ $usersbyid[$cid]->color }}",
														hoverBackgroundColor: "#{{ $usersbyid[$cid]->color }}"
													}
													@php
														$counter++;
													@endphp
												@endif
											@endforeach
										@endif
										,
										{
											label: 'Help Point',
											data: [{ x: 0, y: {{ $maxy }}, r: 0 }],
											backgroundColor: "rgba(255, 255, 255, 0)",
											hoverBackgroundColor: "rgba(255, 255, 255, 0)"
										}
										,
										{
											label: 'Help Point',
											data: [{ x: {{ $maxx1 }}, y: 0, r: 0 }],
											backgroundColor: "rgba(255, 255, 255, 0)",
											hoverBackgroundColor: "rgba(255, 255, 255, 0)"
										}
									]
								},
								options: {
									legend: {
										display: false,
										position: 'bottom'
									},
									responsive: true,
									scales: {
										yAxes: [{
											min: 0,
											beginAtZero: true
										}]
									},
									tooltips: {
										callbacks: {
											label: function(t, d) {
												var label = d.datasets[t.datasetIndex].label + ': (Viewings:' + t.xLabel + ', Offers:' + t.yLabel + ', Invoices: ' + (d.datasets[t.datasetIndex].data[t.index].r/2-2) + ')';

												return label;
											}
										}
									}
								}
							});
						</script>
					</div>
				</div>
			</div>
			

			<div class="col-sm-4">
				
			</div>
			
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="nest-commission-average">
							@if ($daytoday > 0 && $year >= 2018)
								@if ($dollarsum > 0 && $monthcount > 1)
									{{ round($daycounter/$daytoday, 2) }} Consultants, Average Previous {{ $monthcount }} Months: HK$ {{ number_format($dollarsum, 0, '.', ',') }}
								@else
									<!--{{ round($daycounter/$daytoday, 2) }}--> {{$consultant_count}} Consultants
								@endif
							@endif
						</div>
						<h4>Commission Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-wrapper" id="lease3">
							<canvas id="allmonthsdollar" height="88px"></canvas>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="col-sm-4">
				
			</div>
			
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
					{{--	@if ($lastoutstandingpaymentcnt > 1)
							<div class="nest-commission-average">{{ $lastoutstandingpaymentcnt }} Payements Outstanding from {{ ($year-1) }}: HK$ {{ number_format($lastoutstandingpayment, 0, '.', ',') }}</div>
						@elseif ($lastoutstandingpaymentcnt > 0)
							<div class="nest-commission-average">{{ $lastoutstandingpaymentcnt }} Payement Outstanding from {{ ($year-1) }}: HK$ {{ number_format($lastoutstandingpayment, 0, '.', ',') }}</div>
						@endif --}} 
						@if ($outstandingpaymentcnt > 1)
							<a href="{{url('/companyoverview/outstanding-payments/'.$year)}}"><div class="nest-commission-average">{{ $outstandingpaymentcnt }} Payments Outstanding: HK$ {{ number_format($outstandingpayment, 0, '.', ',') }}</div></a>
						@elseif ($outstandingpaymentcnt > 0)
						<a href="{{url('/companyoverview/outstanding-payments/'.$year)}}"><div class="nest-commission-average">{{ $outstandingpaymentcnt }} Payment Outstanding: HK$ {{ number_format($outstandingpayment, 0, '.', ',') }}</div></a>
						@endif
					<h4>Live and Oustanding Payments Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-wrapper" id="lease6">
							<canvas id="allmonthspaymentsoutstanding" height="87px"></canvas>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

			
			
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					<h4>Completed vs. Dead Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<tr>
								<td width="50%"></td>
								<td width="25%" class="text-center">
									<b>Completed</b>
								</td>
								<td width="25%" class="text-center">
									<b>Dead</b>
								</td>
							</tr>
							@foreach ($leadsources as $key => $leadname)
								@if ($key != 4)
									<tr>
										<td width="50%">{{ $leadname }}</td>
										<td width="25%" class="text-center">
											@if ($leadsdeadvscompletebysource[$key]['complete'] > 0)
												{{ $leadsdeadvscompletebysource[$key]['complete'] }}
												({{ round($leadsdeadvscompletebysource[$key]['complete']/$leadsdeadvscompletebysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
										<td width="25%" class="text-center">
											@if ($leadsdeadvscompletebysource[$key]['dead'] > 0)
												{{ $leadsdeadvscompletebysource[$key]['dead'] }}
												({{ 100-round($leadsdeadvscompletebysource[$key]['complete']/$leadsdeadvscompletebysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
									</tr>
								@endif
							@endforeach
							<tr>
								<td width="50%">No Source</td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscompletebysource[0]['complete'] > 0)
										{{ $leadsdeadvscompletebysource[0]['complete'] }}
										({{ round($leadsdeadvscompletebysource[0]['complete']/$leadsdeadvscompletebysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscompletebysource[0]['dead'] > 0)
										{{ $leadsdeadvscompletebysource[0]['dead'] }}
										({{ 100-round($leadsdeadvscompletebysource[0]['complete']/$leadsdeadvscompletebysource[0]['total']*100, 0) }}%)
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Total</b></td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscomplete['complete'] > 0)
										<b>{{ $leadsdeadvscomplete['complete'] }}
										({{ round($leadsdeadvscomplete['complete']/$leadsdeadvscomplete['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscomplete['dead'] > 0)
										<b>{{ $leadsdeadvscomplete['dead'] }}
										({{ 100-round($leadsdeadvscomplete['complete']/$leadsdeadvscomplete['total']*100, 0) }}%)</b>
									@endif
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Active Clients Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<tr>
								<td width="28%"></td>
								<td width="12%" class="text-center">
									<b>New</b>
								</td>
								<td width="12%" class="text-center">
									<b>Hot</b>
								</td>
								<td width="12%" class="text-center">
									<b>Offering</b>
								</td>
								<td width="12%" class="text-center">
									<b>Invoicing</b>
								</td>
								<td width="12%" class="text-center">
									<b>Active</b>
								</td>
								<td width="12%" class="text-center">
									<b>Stored</b>
								</td>
							</tr>
							@foreach ($leadsources as $key => $leadname)
								@if ($key != 4)
									<tr>
										<td>{{ $leadname }}</td>
										<td class="text-center">
											@if ($leadstotalbysource[$key]['new'] > 0)
												{{ $leadstotalbysource[$key]['new'] }}
												(~{{ round($leadstotalbysource[$key]['new']/$leadstotalbysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
										<td class="text-center">
											@if ($leadstotalbysource[$key]['hot'] > 0)
												{{ $leadstotalbysource[$key]['hot'] }}
												(~{{ round($leadstotalbysource[$key]['hot']/$leadstotalbysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
										<td class="text-center">
											@if ($leadstotalbysource[$key]['offering'] > 0)
												{{ $leadstotalbysource[$key]['offering'] }}
												(~{{ round($leadstotalbysource[$key]['offering']/$leadstotalbysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
										<td class="text-center">
											@if ($leadstotalbysource[$key]['invoicing'] > 0)
												{{ $leadstotalbysource[$key]['invoicing'] }}
												(~{{ round($leadstotalbysource[$key]['invoicing']/$leadstotalbysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
										<td class="text-center">
											@if ($leadstotalbysource[$key]['active'] > 0)
												{{ $leadstotalbysource[$key]['active'] }}
												(~{{ round($leadstotalbysource[$key]['active']/$leadstotalbysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
										<td class="text-center">
											@if ($leadstotalbysource[$key]['stored'] > 0)
												{{ $leadstotalbysource[$key]['stored'] }}
												(~{{ round($leadstotalbysource[$key]['stored']/$leadstotalbysource[$key]['total']*100, 0) }}%)
											@endif
										</td>
									</tr>
								@endif
							@endforeach
							<tr>
								<td>No Source</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['new'] > 0)
										{{ $leadstotalbysource[0]['new'] }}
										(~{{ round($leadstotalbysource[0]['new']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['hot'] > 0)
										{{ $leadstotalbysource[0]['hot'] }}
										(~{{ round($leadstotalbysource[0]['hot']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['offering'] > 0)
										{{ $leadstotalbysource[0]['offering'] }}
										(~{{ round($leadstotalbysource[0]['offering']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['invoicing'] > 0)
										{{ $leadstotalbysource[0]['invoicing'] }}
										(~{{ round($leadstotalbysource[0]['invoicing']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['active'] > 0)
										{{ $leadstotalbysource[0]['active'] }}
										(~{{ round($leadstotalbysource[0]['active']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['stored'] > 0)
										{{ $leadstotalbysource[0]['stored'] }}
										(~{{ round($leadstotalbysource[0]['stored']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Total</b></td>
								<td class="text-center">
									@if ($leadstotal['new'] > 0)
										<b>{{ $leadstotal['new'] }}
										(~{{ round($leadstotal['new']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['hot'] > 0)
										<b>{{ $leadstotal['hot'] }}
										(~{{ round($leadstotal['hot']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['offering'] > 0)
										<b>{{ $leadstotal['offering'] }}
										(~{{ round($leadstotal['offering']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['invoicing'] > 0)
										<b>{{ $leadstotal['invoicing'] }}
										(~{{ round($leadstotal['invoicing']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['active'] > 0)
										<b>{{ $leadstotal['active'] }}
										(~{{ round($leadstotal['active']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['stored'] > 0)
										<b>{{ $leadstotal['stored'] }}
										(~{{ round($leadstotal['stored']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

<script>
	var ctx = document.getElementById("allmonthsdollar").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Consultant Fees',
				backgroundColor: '#e74c3c',
				data: [{{ implode(',', $monthlyvalues['dollardonecons']) }}]
			}, {
				label: 'Company Net Fees',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['dollardonecomp']) }}]
			}, {
				label: 'Target',
				backgroundColor: '#cccccc',
				data: [{{ implode(',', $monthlyvalues['dollartarget']) }}]
			}]
		},
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				var year = {{ date('Y') }};
				window.location = "/commission/invoicing?year="+year+"&month="+month+"";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					min: 0,
					beginAtZero: true,
					stacked: true,
					ticks: {
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return 'HK$' + value + '';
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						if (tooltipItem.datasetIndex == 0){
							label = 'Consultant Fees: HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						}else if (tooltipItem.datasetIndex == 1){
							label = ['Company Gross Fees HK$'+(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + data.datasets[0].data[tooltipItem.index]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')];
							label.push('Company Net Fees      HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
						}else if (tooltipItem.datasetIndex == 2){
							label = 'Below Target: HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						}

						return label;
					}
				}
			},annotation: {
				annotations: [{
					type: 'line',
					mode: 'horizontal',
					scaleID: 'y-axis-0',
					value: 500000,
					borderColor: 'rgb(0, 0, 0)',
					borderDash: [2,2],
					borderWidth: 1,
					label: {
						enabled: false
					}
				},{
					type: 'line',
					mode: 'horizontal',
					scaleID: 'y-axis-0',
					value: 1000000,
					borderColor: 'rgba(0, 0, 0, 0)',
					borderWidth: 0,
					label: {
						enabled: false
					}
				}]
			}
		}
	});

	/*var ctx = document.getElementById("allmonthsdeals").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Made Deals',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['dealsdone']) }}]
			}, {
				label: 'Target',
				backgroundColor: '#cccccc',
				data: [{{ implode(',', $monthlyvalues['dealstarget']) }}]
			}]
		},
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				var year = {{ date('Y') }};
				window.location = "/commission/invoicing?year="+year+"&month="+month+"";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true,
					ticks: {
						min: 0,
						beginAtZero: true,
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return value;
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						if (tooltipItem.datasetIndex == 0){
							label = 'Made Deals: '+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						}else if (tooltipItem.datasetIndex == 1){
							label = 'Below Target: '+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						}

						return label;
					}
				}
			}
		}
	});*/

	/*var ctx = document.getElementById("allmonthspayments").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Payments',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['payments']) }}]
			}]
		},
		options: {
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					min: 0,
					beginAtZero: true,
					stacked: true,
					ticks: {
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return 'HK$' + value + '';
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						label = 'Payments: HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

						return label;
					}
				}
			},
			annotation: {
				annotations: [{
					type: 'line',
					mode: 'horizontal',
					scaleID: 'y-axis-0',
					value: 300000,
					borderDash: [2,2],
					borderColor: 'rgb(0, 0, 0)',
					borderWidth: 1,
					label: {
						enabled: false
					}
				},{
					type: 'line',
					mode: 'horizontal',
					scaleID: 'y-axis-0',
					value: 1000000,
					borderColor: 'rgba(0, 0, 0, 0)',
					borderWidth: 0,
					label: {
						enabled: false
					}
				}]
			}
		}
	});*/


	//**merge live and outstanding */
	var ctx = document.getElementById("allmonthspaymentsoutstanding").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Outstanding Payments',
				backgroundColor: '#e74c3c',
				data: [{{ implode(',', $monthlyvalues['paymentsoutstanding']) }}],
				id:0,
			},{
				label: 'Live Payments',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['chartlivepayments']) }}],
				id:1,
			}],
			livepayments:{
				datasets: [{
					
					data: [{{ implode(',', $monthlyvalues['payments']) }}]				
				}]
			}
		},	
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				var year = {{ date('Y') }};
				window.location = "/commission/invoicing?year="+year+"&month="+month+"";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					ticks: {
						beginAtZero:true
					}
				}],
				yAxes: [{				
					stacked: true,
					ticks: {
						beginAtZero:true,
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return 'HK$' + value + '';
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';
						
						if(tooltipItem.datasetIndex == 0){
							label = 'Outstanding Payments: HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						//	label = 'Live Payments: HK$'+(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + data.datasets[0].data[tooltipItem.index]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

							//label = ['Live Payments: HK$'+(data.datasets[1].data[tooltipItem.index] + data.datasets[0].data[tooltipItem.index]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')];
							//label = ['Live Payments: HK$'+(data.livepayments.datasets[0].data[tooltipItem.index]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')];
							//label.push('Outstanding Payments:    HK$'+data.datasets[0].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
						}else if (tooltipItem.datasetIndex == 1){
							//label = 'Live Payments: HK$'+(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + data.datasets[0].data[tooltipItem.index]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						//	label = 'Outstanding Payments: HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
							label = ['Live Payments: HK$'+(data.livepayments.datasets[0].data[tooltipItem.index]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')];
						}

						return label;
					}
				}
			}
			
		}
	});

	var ctx = document.getElementById("piemonthly").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets: [{
				data: [<?php
				foreach ($conscurmonthout as $index => $c){
					if ($index > 0){
						echo ', ';
					}
					echo $c['amount'];
				}
				?>],
				backgroundColor: [<?php
				foreach ($conscurmonthout as $index => $c){
					if ($index > 0){
						echo ', ';
					}
					echo '"'.$c['color'].'"';;
				}
				?>],
				labels: [<?php
					foreach ($conscurmonthout as $index => $c){
						if ($index > 0){
							echo ', ';
						}
						echo '"'.$c['username'].'"';
					}
				?>],
			}]
		},
		options: {
			responsive: true,
			legend: {
				display: false,
				position: 'bottom'
			},
			title: {
				display: false
			},
			animation: {
				animateScale: true,
				animateRotate: true
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						label = data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index]+': HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

						return label;
					}
				}
			},
			circumference: Math.PI,
			rotation: -Math.PI,
			onClick: function(event, array){
				console.log(array)
				var js_array = JSON.parse(JSON.stringify(<?php echo json_encode($conscurmonthout); ?>))
				var url = "{{ url('companyoverview/consultantdeals/') }}/" + js_array[array[0]._index].uid+"/"+{{ date('m') }}+"/"+{{ date('Y') }};
				window.open(url, '_blank');
			}
		}
	});

	var ctx = document.getElementById("pieyearly").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets: [{
				data: [<?php
				foreach ($conscuryearout as $index => $c){
					if ($index > 0){
						echo ', ';
					}
					echo $c['amount'];
				}
				?>],
				backgroundColor: [<?php
				foreach ($conscuryearout as $index => $c){
					if ($index > 0){
						echo ', ';
					}
					echo '"'.$c['color'].'"';;
				}
				?>],
				labels: [<?php
					foreach ($conscuryearout as $index => $c){
						if ($index > 0){
							echo ', ';
						}
						echo '"'.$c['username'].'"';
					}
				?>],
			}]
		},
		options: {
			responsive: true,
			legend: {
				display: false,
				position: 'bottom'
			},
			title: {
				display: false
			},
			animation: {
				animateScale: true,
				animateRotate: true
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						label = data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index]+': HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

						return label;
					}
				}
			},
			circumference: Math.PI,
			rotation: -Math.PI,
			onClick: function(event, array){
				var js_array = JSON.parse(JSON.stringify(<?php echo json_encode($conscuryearout); ?>))
				//var url = "{{ url('dashboardnew') }}/" + js_array[array[0]._index].uid;
				var url = "{{ url('companyoverview/consultantdeals') }}/" + js_array[array[0]._index].uid+"/0/{{$year}}";
				window.open(url, '_blank');
			}
		}
	});


</script>

<style>
.chart-yaxis{
	color:#ff0000;
}
</style>





		</div>
	</div>
</div>
