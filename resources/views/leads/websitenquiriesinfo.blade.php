@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			@if (!empty($landlordforms) && count($landlordforms) > 0)
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Hong Kong Landlord Enquiries</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($landlordforms) && count($landlordforms) > 0)
									@foreach ($landlordforms as $lead)
										@if ($lead->landlordregion == 1)
											<div class="nest-dashboardnew-lead">
												<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
												<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
												<div class="col-xs-3">{{ $lead->phone }}</div>
												<div class="clearfix"></div>
											</div>
										@endif
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>International Landlord Enquiries</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($landlordforms) && count($landlordforms) > 0)
									@foreach ($landlordforms as $lead)
										@if ($lead->landlordregion == 2)
											<div class="nest-dashboardnew-lead">
												<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
												<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
												<div class="col-xs-3">{{ $lead->phone }}</div>
												<div class="clearfix"></div>
											</div>
										@endif
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			@endif
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Finished Hong Kong Landlord Enquiries</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($landlordformsold) && count($landlordformsold) > 0)
								@foreach ($landlordformsold as $lead)
									@if ($lead->landlordregion == 1)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Finished International Landlord Enquiries</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($landlordformsold) && count($landlordformsold) > 0)
								@foreach ($landlordformsold as $lead)
									@if ($lead->landlordregion == 2)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
		</div>
	</div>
</div>
@endsection
