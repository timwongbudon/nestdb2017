@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Form Content</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list" style="max-height:none;">
							<div class="col-xs-3" style="padding-left:0px;"><label>Date</label></div>
							<div class="col-xs-9">{{ $form->getDateTime() }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Name</label></div>
							<div class="col-xs-9">{{ $form->name }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Phone</label></div>
							<div class="col-xs-9">{{ $form->phone }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Email</label></div>
							<div class="col-xs-9">{{ $form->email }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Message</label></div>
							<div class="col-xs-9">{{ $form->message }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Newsletter</label></div>
							<div class="col-xs-9">
								@if ($form->newsletter == 1)
									Yes
								@else
									No
								@endif
							</div>
							<div class="clearfix"></div>
							@if (trim($form->viewedids) != '')
								<div class="col-xs-3" style="padding-left:0px;"><label>Viewed Properties</label></div>
								<div class="col-xs-9">{{ str_replace(',', ', ', $form->viewedids) }}</div>
								<div class="clearfix"></div>
							@endif
						</div>
					</div>
				</div>
				@if ($form->status == 1)
					<div class="panel panel-default">
						<div class="panel-body">
							<a href="{{ url('/websitenquiriesderemovecontact/'.$form->id) }}" class="nest-button btn btn-default"><i class="fa fa-btn fa-trash-o"></i> Recover Enquiry</a>
						</div>
					</div>
				@endif
				@if ($form->status == 3)
					<div class="panel panel-default">
						<div class="panel-body">
							<a href="{{ url('/websitenquiriesfinishcontact/'.$form->id) }}" class="nest-button btn btn-default"><i class="fa fa-btn fa-check"></i> Finished</a>
						</div>
					</div>
				@endif
				@if ($form->status == 10 && $lead != null)
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Consultant Enquiry</h4>
						</div>
						
						<div class="panel-body">
							@if ($lead->clientid == 0)
								<a href="{{ url('/lead/enterclient/'.$lead->id) }}" class="nest-button btn btn-default" target="_blank"><i class="fa fa-btn fa-info-circle"></i> Show</a>
							@else
								<a href="{{ url('/lead/edit/'.$lead->id) }}" class="nest-button btn btn-default" target="_blank"><i class="fa fa-btn fa-info-circle"></i> Show</a>
							@endif
						</div>
					</div>
				@endif
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addCommentMessage();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
		</div>
	</div>
</div>

<script>
	function loadCommentsMessage(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/websiteenquirycontact/comments/".$form->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsEdit').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsEdit').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/websiteenquirycontact/comments/".$form->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsEdit').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/websiteenquirycontact/comments/".$form->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsEdit').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsEdit').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function addCommentMessage(){
		var comment = jQuery('#newcomment').val().trim();
		if (comment.length > 0){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/websiteenquirycontact/commentadd/".$form->id) }}',
				data: {comment: comment},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					jQuery('#newcomment').val('');
					loadCommentsMessage();
				},
				error: function (data) {
					console.log(data);
					alert('Something went wrong, please try again!');
				}
			});
		}else{
			alert('Please enter a comment');
		}
	}
	
	$('#newcomment').keydown(function( event ) {
		if ( event.keyCode == 13 ) {
			event.preventDefault();
			addCommentMessage();
		}				
	});
	
	function removeCommentMessage(t){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'POST',
			url: '{{ url("/websiteenquirycontact/commentremove/".$form->id) }}',
			data: {t: t},
			dataType: 'text',
			cache: false,
			async: false,
			success: function (data) {
				loadCommentsMessage();
			},
			error: function (data) {
				alert('Something went wrong, please try again!');
			}
		});
	}
	
	jQuery( document ).ready(function(){
		loadCommentsMessage();
	});
	
</script>

@endsection
