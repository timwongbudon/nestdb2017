@extends('layouts.app')

@section('content')
<div class="col-sm-12 nest-new dashboardnew"  >
	<div class=""></div>
        <div class="panel panel-default nest-dashboardnew-wrapper">
            <div class="panel-heading">
              
                <h4>{{$year}} Outstanding Payments</h4>
              
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped" style="margin-bottom: 0px;">
                    <tr>	
                      					
                        <td width="15%" class="text-left">
                            <b>Consultant</b>
                        </td>
                        <td width="15%" class="text-left">
                            <b>Client</b>
                        </td>                     
                        <td width="10%" class="text-left">
                            <b>Property</b>
                        </td>

                        <td width="10%" class="text-center">
                            <b>Start Date</b>
                        </td>
                        
                        <td width="10%" class="text-center">
                            <b>Landlord Paid Date</b>
                        </td>

                        <td width="10%" class="text-center">
                            <b>Landlord Amount</b>
                        </td>

                        <td width="10%" class="text-center">
                            <b>Tenant Paid Date</b>
                        </td>
                                           
                        <td width="10%" class="text-center">
                            <b>Tenant Amount</b>
                        </td>
                    </tr>
                    <tbody id="">

                    @php $i = 0; 
                    $outstandingpayment1 = 0;
                    $outstandingTenant = 0;  
                    $outstandingLandloard = 0;      

                    @endphp

                    @foreach ($data as  $inv) 
                        @php 

                        $add = "&#10004;";
                        $add1 = "&#10004;";
                        $color = "green";
                        $color1 = "green";

                        if ($inv['lmonth'] > 0){                           
                            
                        }else{                          
                            $outstandingpayment1 += $inv['landlordamount'];                            
                            $add = "&#10006;";
                            $color = "red";
                            $outstandingLandloard += $inv['landlordamount'];        

                            if($inv['landlordamount'] > 0){  
                                $i++;                                
                            }
                        }

                        //Tenant
                        if ($inv['tmonth'] > 0){
                           
                        }else{                        
                            $outstandingpayment1 += $inv['tenantamount'];
                            $add1 = "&#10006;";
                            $color1 = "red";

                            $outstandingTenant += $inv['tenantamount'];  
                            
                            if($inv['tenantamount'] > 0){  
                                $i++;                                     
                            }
                        }

                        @endphp   
                        <tr {{$inv['cid']}}>
                            <td class="text-left">
                                {{$inv['cnfirstname']}} {{$inv['cnlastname']}}
                            </td>

                            <td class="text-left">
                                {{$inv['cfirstname']}} {{$inv['clastname']}}
                            </td>

                            <td class="text-left">
                                {{$inv['property']}}
                            </td>

                            <td class="text-center">                              
                            
                                @php             
                                if(isset($inv['startdate']) && !empty($inv['startdate'])){
                                    echo date('d.m.Y',strtotime($inv['startdate']));    
                                    
                                }
                                @endphp
                            </td>

                            <td class="text-center">
                                
                                @php
                                if( $inv['landlordpaiddate'] != '0000-00-00' && !empty($inv['landlordpaiddate']) ){
                                    echo date('d.m.Y',strtotime($inv['landlordpaiddate']));
                                }else {
                                    echo "-";
                                }
                                @endphp
                                
                                
                            </td>

                            <td class="text-center">       
                                @php
                                    if($inv['landlordamount'] > 0):
                                @endphp   

                                {{number_format($inv['landlordamount'], 0, '.', ',')}} <span style="color: {{$color}}; font-weight:bold; ">{{ $add }}</span>
                                @php else:  @endphp  
                                        -
                                @php endif; @endphp          
                            </td>

                            <td class="text-center">
                                @php
                                if( $inv['tenantpaiddate'] != '0000-00-00' && !empty($inv['tenantpaiddate']) ){
                                    echo date('d.m.Y',strtotime($inv['tenantpaiddate']));
                                } else {
                                    echo "-";
                                }
                                @endphp
                                
                            </td>

                            <td class="text-center">                                
                                @php
                                    if($inv['tenantamount'] > 0):
                                @endphp   

                                {{number_format($inv['tenantamount'], 0, '.', ',')}}  <span style="color: {{$color1}}; font-weight:bold; ">{{ $add1 }}</span>
                                
                                @php else:  @endphp  
                                        -
                                @php endif; @endphp             
                            </td>
                        </tr>
                    @endforeach
                  
                    </tbody>

                    </table>
                    </div>
            </div>
        </div>      
        Total Outstanding for Landlord: HK$ {{number_format($outstandingLandloard, 0, '.', ',')}}<br>
        Total Outstanding for Tenant: HK$ {{number_format($outstandingTenant, 0, '.', ',')}}<br><br>
     <strong>{{ $i }} Payments Outstanding: HK$ {{ number_format($outstandingpayment1, 0, '.', ',')}}       </strong>
    </div>

<div class="clearfix"></div>

@endsection
