@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
.select2-container .select2-choices .select2-search-field input, .select2-container .select2-choice, .select2-container .select2-choices{
    background-color: #ffffff;	
}
</style>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2({
		placeholder: "Select",
	});
});
</script>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-4">
				<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Enquiry Information</h4>
						</div>
						<div class="panel-body">
							@include('common.errors')
							
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Consultant</div>
								</div>
								<div class="col-xs-8">
									<!-- consultantid -->
									<select name="consultantid" class="form-control">
										@foreach ($users as $u)
											@if ($u->isConsultant())
												<option value="{{ $u->id }}" 												
												@if ($u->id == $lead->consultantid)
													selected
												@endif
												>
												@if ($u->status == 1)
													[inactive]
												@endif
												{{ $u->name }}
												</option>
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Other Consultant</div>
								</div>
								<div class="col-xs-8">
									<!-- consultantid -->
									<select name="shared_with[]" class="form-control js-example-basic-multiple" style="    background: #FFF;
    border: 1px solid;
    border-radius: 0;" multiple="">
										@foreach ($users as $u)
											@if ($u->isConsultant() && $u->status !== 1)												
												<option value="{{ $u->id }}" 
												@if ( !empty($lead->shared_with) && in_array($u->id, json_decode($lead->shared_with)) )												
													selected
												@endif
												>
												{{ $u->name }}
												</option>
												
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Status</div>
								</div>
								<div class="col-xs-8">
									<!-- status -->
									<select name="status" class="form-control">
										@foreach ($avstatus as $si => $stat)
											<option value="{{ $si }}" 
											@if ($si == $lead->status)
												selected
											@endif
											>{{ $stat }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Contact</div>
								</div>
								<div class="col-xs-8">
									<input type="date" name="dateinitialcontact" class="form-control" value="{{ old('dateinitialcontact', $lead->dateinitialcontact) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Outreach</div>
								</div>
								<div class="col-xs-8">
									<input type="date" name="lastoutreach" class="form-control" value="{{ old('lastoutreach', $lead->lastoutreach) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Reminde Me</div>
								</div>
								<div class="col-xs-8">
									<input type="date" name="remindme" class="form-control" value="{{ old('remindme', $lead->remindme) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Source</div>
								</div>
								<div class="col-xs-8">
									<!-- sourcehear -->
									<select name="sourcehear" class="form-control">
										@foreach ($avsources as $si => $sour)
											<option value="{{ $si }}" 
											@if ($si == $lead->sourcehear)
												selected
											@endif
											>{{ $sour }}</option>
										@endforeach
									</select>
									
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Type</div>
								</div>
								<div class="col-xs-4">
									<label for="type-1" class="control-label">{!! Form::radio('type', 1, ($lead->type == 1), ['id'=>'type-1']) !!} Lease</label>
								</div>
								<div class="col-xs-4">
									<label for="type-2" class="control-label">{!! Form::radio('type', 2, ($lead->type == 2), ['id'=>'type-2']) !!} Sale</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							@if ($lead->type == 2)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Offer Price</div>
									</div>
									<div class="col-xs-8">
										<!-- offerprice -->
										<input type="number" name="offerprice" id="property-offerprice" class="form-control" value="{{ old('offerprice', $lead->offerprice) }}" min="0"> 
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							
							<input type="hidden" name="clienttype" value="1" />
							<div id="moreinfoenquirybtn" class="text-center" style="padding-top:10px;">
								<a href="javascript:;" onClick="showmoreinfoenquiry();">Show All Fields</a>
							</div>
							
							<script>
								function showmoreinfoenquiry(){
									jQuery('#moreinfoenquirybtn').css('display', 'none');
									jQuery('#moreinfoenquiry').css('display', 'block');
								}
							</script>
							
							
							<div id="moreinfoenquiry" style="display:none;">
								@if ($lead->type == 1)
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Offer Price</div>
										</div>
										<div class="col-xs-8">
											<!-- offerprice -->
											<input type="number" name="offerprice" id="property-offerprice" class="form-control" value="{{ old('offerprice', $lead->offerprice) }}" min="0"> 
										</div>
										<div class="clearfix"></div>
									</div>
								@endif
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Budget From</div>
									</div>
									<div class="col-xs-8">
										<!-- budget -->
										<input type="number" name="budget" id="property-budget" class="form-control" value="{{ old('budget', $lead->budget) }}" min="0"> 
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Budget To</div>
									</div>
									<div class="col-xs-8">
										<!-- budget -->
										<input type="number" name="budgetto" id="property-budget" class="form-control" value="{{ old('budgetto', $lead->budgetto) }}" min="0"> 
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Bedrooms</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="req_beds" class="form-control" value="{{ old('req_beds', $lead->req_beds) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Bathrooms</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="req_baths" class="form-control" value="{{ old('req_baths', $lead->req_baths) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Maid's Room(s)</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="req_maidrooms" class="form-control" value="{{ old('req_maidrooms', $lead->req_maidrooms) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Carpark(s)</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="req_carparks" class="form-control" value="{{ old('req_carparks', $lead->req_carparks) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Outdoor Space</div>
									</div>
									<div class="col-xs-8">
										<textarea name="req_outdoor" class="form-control">{{{ Input::old('req_outdoor', $lead->req_outdoor) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Facilities</div>
									</div>
									<div class="col-xs-8">
										<textarea name="req_facilites" class="form-control">{{{ Input::old('req_facilites', $lead->req_facilites) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Likes / Dislikes</div>
									</div>
									<div class="col-xs-8">
										<textarea name="likes" class="form-control">{{{ Input::old('likes', $lead->likes) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Currently Living</div>
									</div>
									<div class="col-xs-8">
										<textarea name="currentflat" class="form-control">{{{ Input::old('currentflat', $lead->currentflat) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Reason for Move</div>
									</div>
									<div class="col-xs-8">
										<textarea name="reasonformove" class="form-control">{{{ Input::old('reasonformove', $lead->reasonformove) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Notice Period</div>
									</div>
									<div class="col-xs-8">
										<textarea name="noticeperiod" class="form-control">{{{ Input::old('noticeperiod', $lead->noticeperiod) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Preferred Area(s)</div>
									</div>
									<div class="col-xs-8">
										<textarea name="preferredareas" class="form-control">{{{ Input::old('preferredareas', $lead->preferredareas) }}}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<br />
							<div class="nest-property-edit-row">
								<div class="col-xs-12">
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						@if (!empty($clientpic))
							<div class="lead-client-pic">
								<img src="{{ url('/client/showpic/'.$clientpic->id) }}" />
							</div>
						@endif
						
						<div class="panel-heading">
							<h4>Client Information</h4>
						</div>
						<div class="panel-body">
							@if ($lead['client'])
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <b>Client Name</b>
									</div>
									<div class="col-xs-6" id="clientName">
										{{ $lead['client']->name() }}
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <b>Phone</b>
									</div>
									<div class="col-xs-8">
										{!! $lead['client']->telLinked() !!} 
										@if( !empty($lead['client']->tel2) )
											/ {!! $lead['client']->tel2Linked() !!}
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <b>Email</b>
									</div>
									<div class="col-xs-8">
										{{ $lead['client']->email() }} 
										@if( !empty($lead['client']->email2) )
											/ {{ $lead['client']->email2() }}
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <b>Type</b>
									</div>
									<div class="col-xs-8">
										{{ $lead['client']->getClienttype() }}
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row" style="padding-top:15px;">
									<div class="col-lg-6 col-xs-6">
										<a href="{{ url('client/show/'.$lead['client']->id) }}" class="nest-button btn btn-default" style="width:100%;"><i class="fa fa-btn fa-info-circle"></i> Show Client</a>
									</div>
									<div class="col-lg-6 col-xs-6">
										<a href="{{ url('client/edit/'.$lead['client']->id) }}" class="nest-button btn btn-default" style="width:100%;"><i class="fa fa-btn fa-pencil-square-o"></i> Edit Client</a>
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
						</div>
					</div>
				</form>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Shortlist</h4>
					</div>
					<div class="panel-body">
						@if ($lead->shortlistid > 0)
							@if ($shortlist)
								@if ($shortlist->status == 10)
									<div class="nest-property-edit-row" style="">
										<div class="col-lg-6 col-xs-6">
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id) }}" style="width:100%;">
												<i class="fa fa-btn fa-info-circle"></i> Show Shortlist
											</a>
										</div>
										<div class="col-lg-6 col-xs-6">
											@if ($shortlist->typeid == 2)
												<a class="nest-button btn btn-primary" href="{{ url('/documents/index-sale/'.$shortlist->id) }}" style="width:100%;">
													<i class="fa fa-btn fa-book"></i> Documents
												</a>
											@else
												<a class="nest-button btn btn-primary" href="{{ url('/documents/index-lease/'.$shortlist->id) }}" style="width:100%;">
													<i class="fa fa-btn fa-book"></i> Documents
												</a>
											@endif
										</div>
										<div class="col-xs-12" style="padding-top:10px;">
											<a href="{{ url('lead/removeshortlist/'.$lead->id) }}">Remove</a> this Shortlist from this Enquiry
										</div>
										<div class="clearfix"></div>
									</div>
								@else
									<div class="nest-property-edit-row" style="">
										<div class="col-lg-6 col-xs-6">
											<a class="nest-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id) }}" style="width:100%;">
												<i class="fa fa-btn fa-info-circle"></i> Show Shortlist
											</a>
										</div>
										<div class="col-lg-6 col-xs-6">
											@if ($shortlist->typeid == 2)
												<a class="nest-button btn btn-primary" href="{{ url('/documents/index-sale/'.$shortlist->id) }}" style="width:100%;">
													<i class="fa fa-btn fa-book"></i> Documents
												</a>
											@else
												<a class="nest-button btn btn-primary" href="{{ url('/documents/index-lease/'.$shortlist->id) }}" style="width:100%;">
													<i class="fa fa-btn fa-book"></i> Documents
												</a>
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row" style="padding-top:15px;">
										<div class="col-lg-6 col-xs-6">
											<a class="nest-button btn btn-primary" href="{{ url('shortlist/manage/'.$shortlist->id) }}" style="width:100%;">
												<i class="fa fa-btn fa-plus"></i> Properties
											</a>
										</div>
										<div class="col-lg-6 col-xs-6">
											<a class="nest-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id.'?statuschange=close') }}" style="width:100%;">
												<i class="fa fa-btn fa-lock"></i> Close Shortlist
											</a>
										</div>
										<div class="clearfix"></div>
									</div>
								@endif
							@endif
						@else
							@if (!empty($shortlists) && count($shortlists) != 0 && $shortlists[0]->status < 10)
								<!-- how to handle existing open shortlist -->
								There is an existing open shortlist for the client, so no new shortlist can be created. Your options are to either 
								<a href="{{ url('client/show/'.$lead['client']->id) }}">go to the client area</a> and close the shortlist or click 
								<a href="{{ url('lead/addshortlist/'.$lead->id.'/'.$shortlists[0]->id) }}">here</a> to add the open shortlist to this enquiry.
							@else
								<!-- create new shortlist button -->
								<div class="nest-property-edit-row">
									<div class="col-lg-6 col-xs-6">
										<a class="nest-button btn btn-primary" href="{{ url('lead/createshortlist/'.$lead->id) }}" style="width:100%;">
											<i class="fa fa-btn fa-plus"></i> Create Shortlist
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
						@endif
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"> 
						<img src="/images/googledrive2.png" style="float:right;height:40px;" />
						<h4>Google Drive</h4>
					</div>
					<div class="panel-body">
						<div class="nest-buildings-result-wrapper">
							@if (trim($lead->googledrivelink) == '')
								<form action="/lead/updategooglelink/{{ $lead->id }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
									{{ csrf_field() }}
									
									<div class="row">
										<div class="col-sm-8" style="padding-top:4px;">
											<input type="text" class="form-control" name="googledrivelink" placeholder="Google Drive Folder Link" style="height:32px;" />
										</div>
										<div class="col-sm-4">
											<button type="submit" class="nest-button btn btn-default" style="width:100%;">Save</button>
										</div>
									</div>
								</form>
							@else
								<a href="{{ trim($lead->googledrivelink) }}" target="_blank">
									Google Drive Folder
								</a> &nbsp; &nbsp; 
								<a href="/lead/removegooglelink/{{ $lead->id }}" onclick="return confirm('Are you sure you want to remove the link?')">
									<i class="fa fa-trash-o"></i>
								</a>
							@endif
						</div>
					</div>
				</div>
				@if ($lead->shortlistid > 0)
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Viewings</h4>
						</div>
						<div class="panel-body">
							@if ($viewings != null && count($viewings) > 0)
								<div class="nest-buildings-result-wrapper">
									@foreach ($viewings as $index => $v)
										<div class="row 
										@if ($index%2 == 1)
											nest-striped
										@endif
										">
											<div class="col-xs-9">
												<b>Date/Time:</b> {{ $v->v_date() }} {{ $v->getNiceTime() }}<br />
												<b>Status:</b> 
												@if ($v->status == 10)
													Closed
												@else
													Open
												@endif
											</div>
											<div class="col-xs-3">
												<a href="{{ url('/viewing/show/'.$v->id) }}" class="nest-button nest-right-button btn btn-default">
													<i class="fa fa-btn fa-pencil-square-o"></i>View
												</a>
											</div>
										</div>
									@endforeach
								</div>
							@else
								No Viewings Found
							@endif
						</div>
					</div>
					
					@if ($lead->type != 2)
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Documents</h4>
							</div>
							<div class="panel-body">
								@if (!empty($docs) && $docs != null && count($docs) > 0)
									<div class="nest-buildings-result-wrapper">
										@php
											$index = 0;
										@endphp
										@if (isset($docs['offerletter']))
											@foreach ($docs['offerletter'] as $d)
												@if (isset($offerletters[$d->propertyid]))
													@foreach ($offerletters[$d->propertyid] as $dd)
														<div class="row 
														@if ($index%2 == 1)
															nest-striped
														@endif
														">
															<div class="col-xs-9">
																<b>Document:</b> Offer Letter [Version {{ $dd->revision }}]<br />
																<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($dd->fieldcontents))->f1) ? json_decode(json_encode($dd->fieldcontents))->f1 : json_decode($dd->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
															
															</div>
															<div class="col-xs-3">
																<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/offerletter/'.$d->propertyid).'/'.$dd->revision }}">
																	<i class="fa fa-btn fa-pencil"></i>Edit
																</a>
															</div>
														</div>
														@php
															$index++;
														@endphp
													@endforeach
												@endif
											@endforeach
										@endif
										@if (isset($docs['letterofundertaking']))
											@foreach ($docs['letterofundertaking'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Letter of Undertaking<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/letterofundertaking/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['holdingdepositletter']))
											@foreach ($docs['holdingdepositletter'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Holding Deposit Letter<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/holdingdepositletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['tenantfeeletter']))
											@foreach ($docs['tenantfeeletter'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Tenant Fee Letter<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenantfeeletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['landlordfeeletter']))
											@foreach ($docs['landlordfeeletter'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Landlord Fee Letter<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/landlordfeeletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['tenancyagreement']))
											@foreach ($docs['tenancyagreement'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Tenancy Agreement<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenancyagreement/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['sideletter']))
											@foreach ($docs['sideletter'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Side Letter<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/sideletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['coverletter']))
											@foreach ($docs['coverletter'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Cover Letter<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/coverletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['customletter']))
											@foreach ($docs['customletter'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Custom Letter<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 															
																$t =   isset(json_decode(json_encode($d->fieldcontents))->f1) ? json_decode(json_encode($d->fieldcontents))->f1 : json_decode($d->fieldcontents)->f1;
																echo date_format(date_create($t),'m/d/Y');																	
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/customletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['handoverreport']))
											@foreach ($docs['handoverreport'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Handover Report<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 													
																$t =  json_decode(json_encode($d->fieldcontents),true);
																echo isset(json_decode($t)->f5) ? date_format(date_create(json_decode($t)->f5),'m/d/Y') : date_format($d->created_at,'m/d//Y');
																
																											
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreport/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										@if (isset($docs['utilityaccounts']))
											@foreach ($docs['utilityaccounts'] as $d)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-9">
															<b>Document:</b> Utility Accounts<br />
															<b>Property:</b> {{ $docsprops[$d->propertyid]->shorten_building_name() }}<br />
																<b>Date Created:</b> 
																<?php 																													
																echo  date_format($d->created_at,'m/d/Y');																											
																?>
														</div>
														<div class="col-xs-3">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/utilityaccounts/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</div>
													</div>
													@php
														$index++;
													@endphp
											@endforeach
										@endif
										
									</div>
								@else
									No Documents Found
								@endif
							</div>
						</div>
					@endif
					
					@if (!empty($lastofferlettercalendar))
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Handover</h4>
							</div>
							<div class="panel-body">
								<div class="nest-buildings-result-wrapper">
									<div class="row">
										<div class="col-xs-9">
											<b>Status:</b> Confirmed<br />
											<b>Date:</b> {{ $lastofferlettercalendar->niceDateFrom() }}<br />
											<b>Time:</b> {{ $lastofferlettercalendar->timefrom }}
										</div>
										<div class="col-xs-3">
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/calendar/edit/'.$lastofferlettercalendar->id.'/1') }}">
												<i class="fa fa-btn fa-pencil"></i>Edit
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					@elseif ($lastofferletter != null)
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Handover</h4>
							</div>
							<div class="panel-body">
								<div class="nest-buildings-result-wrapper">
									<div class="row">
										<form action="/calendar/addhandover" method="post">
											{{ csrf_field() }}
											<input type="hidden" value="{{ $lead->id }}" name="leadid" />
											<div class="col-xs-9">
												<b>Status:</b> Pending<br />
												<b>Date:</b> <input type="date" value="{{ $lastofferletter['fieldcontents']['f25'] }}" name="datefrom" class="form-control" style="margin-top:3px;width:50%;display:inline-block;margin-left:10px;" /><br />
												<b>Time:</b> 
													<select name="timefrom" class="form-control" style="width:50%;display:inline-block;margin-left:7px;margin-top:3px;">
														@foreach ($times as $t)
															<option value="{{ $t }}">{{ $t }}</option>
														@endforeach
													</select>
											</div>
											<div class="col-xs-3">
												<button type="submit" class="nest-button nest-right-button btn btn-primary">
													<i class="fa fa-btn fa-pencil"></i>Confirm
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					@endif						
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Invoicing</h4>
						</div>
						<div class="panel-body">
							@if ($doc != null && trim($doc->properties) != '')
								@if ($commission == null)
									<div class="nest-property-edit-row" style="">
										<div class="col-xs-12">
											<a href="{{ url('/commission/readyforinvoicing/'.$shortlist->id.'') }}" class="nest-button btn btn-primary" style="width:100%;">Invoice Tenant and Landlord</a>
										</div>
									</div>
									<div class="clearfix"></div>
								@else
									<div class="nest-property-edit-row" style="">
										<div class="col-sm-12">
											@if ($lead->type == 2)
												<label>Full Sale Price:</label> HK$ {{ number_format($commission->fullamount, 2, '.', ',') }}
											@else
												<label>Monthly Rent:</label> HK$ {{ number_format($commission->fullamount, 2, '.', ',') }}
											@endif
										</div>
										<div class="col-sm-12">
											<label>Invoice to Landlord:</label> HK$ {{ number_format($commission->landlordamount, 2, '.', ',') }}
										</div>
										<div class="col-sm-12">
											<label>Invoice to Tenant:</label> HK$ {{ number_format($commission->tenantamount, 2, '.', ',') }}
										</div>
										<div class="clearfix" style="padding-bottom:12px;"></div>
										<div class="col-xs-12">
											<a href="{{ url('/commission/invoiceedit/'.$commission->id.'') }}" class="nest-button btn btn-primary" style="width:100%;">Show Invoice Record</a>
										</div>
									</div>
									<div class="clearfix"></div>
								@endif
							@else
								<div class="nest-property-edit-row" style="">
									<div class="col-sm-12">
										To be able to initiate the invoicing process, please add at least one property to the document section
									</div>
								</div>
								<div class="clearfix"></div>
								
							@endif
						</div>
					</div>
				@endif
			</div>
			<div class="col-sm-4"> 
				@if (!empty($properties) && count($properties) > 0)
					@foreach ($properties as $property)
						<div class="nest-property-list-item">
							<div class="panel panel-default">
								<div class="nest-property-list-item-top">
									<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}" style="padding-left:10px;">
										{{ $property->shorten_building_name(0) }}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-list-item-image-wrapper">
									<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
										@if(!empty($property->featured_photo_url))
											<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
										@else
											<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
										@endif
										
										
										<div class="nest-property-list-item-price-wrapper">
											<div class="nest-property-list-item-price">
												{!! $property->price_db_searchresults_new() !!}
											</div>
										</div>
									</a>
									<div class="nest-property-list-item-image-top-bar-left">
										@if (count($property->nest_photos) > 0)
											<div class="nest-property-list-item-nestpics-wrapper">
												<div class="nest-property-list-item-nestpics">
													<img src="/images/camera2.png" />
												</div>
											</div>
										@endif
									</div>
									<div class="nest-property-list-item-image-top-bar">
										@if($property->bonuscomm == 1)
											<div class="nest-property-list-item-bonus-wrapper">
												<div class="nest-property-list-item-bonus">
													$$$
												</div>
											</div>
										@endif
										@if($property->hot == 1)
											<div class="nest-property-list-item-hot-wrapper">
												<div class="nest-property-list-item-hot">
													HOT
												</div>
											</div>
										@endif
										@if($property->web)
											<div class="nest-property-list-item-web-wrapper">
												<div class="nest-property-list-item-web">
													<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
														WEB
													</a>
												</div>
											</div>
										@endif
										@if ($property->owner_id > 0)
											@if ($property->getOwner() && $property->getOwner()->count() > 0)
												@foreach ($property->getOwner() as $owner)
													@if ($owner->corporatelandlord == 1)
														<div class="nest-property-list-item-cp-wrapper">
															<div class="nest-property-list-item-cp">
																<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																	CL
																</a>
															</div>
														</div>
													@endif
												@endforeach
											@endif
										@elseif ($property->poc_id == 3 || $property->poc_id == 4)
											@if ($property->getAgency() && $property->getAgency()->count() > 0)
												@foreach ($property->getAgency() as $agent)
													@if ($agent->corporatelandlord == 1)
														<div class="nest-property-list-item-cp-wrapper">
															<div class="nest-property-list-item-cp">
																<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																	CL
																</a>
															</div>
														</div>
													@endif
												@endforeach
											@endif
										@endif
									</div>
								</div>
								<div class="nest-property-list-item-bottom-address-wrapper">
									<div class="nest-property-list-item-bottom-address">
										{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
									</div>
									<div class="nest-property-list-item-bottom-unit">
										{{ $property->unit }}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-list-item-bottom-rooms-wrapper">
									<div class="nest-property-list-item-bottom-rooms-left">
										Room(s)
									</div>
									<div class="nest-property-list-item-bottom-rooms-right">
										@if ($property->bedroom_count() == 1)
											{{ $property->bedroom_count() }} bed, 
										@else
											{{ $property->bedroom_count() }} beds, 
										@endif
										@if ($property->bathroom_count() == 1)
											{{ $property->bathroom_count() }} bath
										@else
											{{ $property->bathroom_count() }} baths
										@endif
										
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-list-item-bottom-rooms-wrapper">
									<div class="nest-property-list-item-bottom-rooms-left">
										Size
									</div>
									<div class="nest-property-list-item-bottom-rooms-right">
										{{ $property->property_netsize() }} / {{ $property->property_size() }}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-list-item-bottom-rooms-wrapper">
									<div class="nest-property-list-item-bottom-rooms-left">
										@if ($property->poc_id ==  1)
											Landlord
										@elseif ($property->poc_id ==  2)
											Rep
										@elseif ($property->poc_id ==  3)
											Agency
										@else
											Contact
										@endif
									</div>
									<div class="nest-property-list-item-bottom-rooms-right">
										@php
											$vendorshown = false;
										@endphp
										@if ($property->poc_id ==  1)
											@if ($property->getOwner() && $property->getOwner()->count() > 0)
												@foreach ($property->getOwner() as $owner)
													{!! $owner->basic_info_property_search_linked() !!}
												@endforeach
												@php
													$vendorshown = true;
												@endphp
											@endif
										@elseif ($property->poc_id ==  2)
											@if ($property->getRep() && $property->getRep()->count() > 0)
												@foreach ($property->getRep() as $rep)
													{!! $rep->basic_info_property_search_linked() !!}
												@endforeach
												@php
													$vendorshown = true;
												@endphp
											@endif
										@elseif ($property->poc_id ==  3)
											@if ($property->getAgency() && $property->getAgency()->count() > 0)
												@foreach ($property->getAgency() as $agent)
													{!! $agent->basic_info_property_search_linked() !!}
												@endforeach
												@php
													$vendorshown = true;
												@endphp
											@endif
										@endif
										@if (!$vendorshown)
											@if ($property->getOwner() && $property->getOwner()->count() > 0)
												@foreach ($property->getOwner() as $owner)
													{!! $owner->basic_info_property_search_linked() !!}
												@endforeach														
												@if ($property->getRep() && $property->getRep()->count() > 0)
													@foreach ($property->getRep() as $rep)
														{!! $rep->basic_info_property_search_linked() !!}
													@endforeach
												@endif
											@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
												@foreach ($property->getAgency() as $agent)
													{!! $agent->basic_info_property_search_linked() !!}
												@endforeach
											@else
												-
											@endif
										@endif
									</div>
									<div class="clearfix"></div>
									@if ($property->poc_id == 1 && $vendorshown)
										@if ($property->getRep() && $property->getRep()->count() > 0)
											<div class="nest-property-list-item-bottom-rooms-left">
												Rep
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@foreach ($property->getRep() as $rep)
													{!! $rep->basic_info_property_search_linked() !!}
												@endforeach
											</div>
										@endif
									@endif
									@if ($property->poc_id == 2 && $vendorshown)
										@if ($property->getOwner() && $property->getOwner()->count() > 0)
											<div class="nest-property-list-item-bottom-rooms-left">
												Owner
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@foreach ($property->getOwner() as $owner)
													{!! $owner->basic_info_property_search_linked() !!}
												@endforeach
											</div>
										@endif
									@endif
									@if ($property->poc_id == 3 && $vendorshown)
										@if ($property->getAgent() && $property->getAgent()->count() > 0)
											<div class="nest-property-list-item-bottom-rooms-left">
												Agent
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@foreach ($property->getAgent() as $agent)
													{!! $agent->basic_info_property_search_linked() !!}
												@endforeach
											</div>
										@endif	
									@endif
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-list-item-bottom-features-wrapper">
									<div class="col-xs-4">
										<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
										{{ $property->maidroom_count() }}
									</div>
									<div class="col-xs-4">
										<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
										{{ $property->backend_search_outdoor() }}
									</div>
									<div class="col-xs-4">
										<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
										{{ $property->carpark_count() }}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-list-item-bottom-buttons-wrapper">
									<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
										<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
									</div>
									<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
										<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')" style="width:100%;border:0px;">
											<i class="fa fa-btn fa-dot-circle-o"></i>Show
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					@endforeach
				@endif
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Comments</h4>
					</div>
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addComment();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Change Log</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-changes" id="existingChangesEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				
				@if ($form != null)
					<div class="panel panel-default">
						<div class="panel-heading">
							@if( (isset($form_details[12]) &&  $form_details[12] !=="") || (isset($form_details[14]) &&  $form_details[14] !=="") )
						   		<h4>Contact via EDM</h4>
							@else
								<h4>Contact via Website</h4>
							@endif
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list" style="max-height:none;">
								<div class="col-xs-3" style="padding-left:0px;"><label>Date</label></div>
								<div class="col-xs-9">{{ $form->getDateTime() }}</div>
								<div class="clearfix"></div>
								<div class="col-xs-3" style="padding-left:0px;"><label>Name</label></div>
								<div class="col-xs-9">{{ $form->name }}</div>
								<div class="clearfix"></div>
								<div class="col-xs-3" style="padding-left:0px;"><label>Phone</label></div>
								<div class="col-xs-9">{{ $form->phone }}</div>
								<div class="clearfix"></div>
								<div class="col-xs-3" style="padding-left:0px;"><label>Email</label></div>
								<div class="col-xs-9">{{ $form->email }}</div>
								<div class="clearfix"></div>
								@if ($lead->websiteid > 0)
									<div class="col-xs-3" style="padding-left:0px;"><label>Budget From</label></div>
									<div class="col-xs-9">{{ $form->budgetfrom }}</div>
									<div class="clearfix"></div>
									<div class="col-xs-3" style="padding-left:0px;"><label>Budget To</label></div>
									<div class="col-xs-9">{{ $form->budgetto }}</div>
									<div class="clearfix"></div>
								@endif
								<div class="col-xs-3" style="padding-left:0px;"><label>Message</label></div>
								<div class="col-xs-9">{{ $form->message }}</div>
								<div class="clearfix"></div>
								<div class="col-xs-3" style="padding-left:0px;"><label>Newsletter</label></div>
								<div class="col-xs-9">
									@if ($form->newsletter == 1)
										Yes
									@else
										No
									@endif
								</div>
								<div class="clearfix"></div>
								@if (trim($form->viewedids) != '')
									<div class="col-xs-3" style="padding-left:0px;"><label>Viewed Properties</label></div>
									<div class="col-xs-9">{{ str_replace(',', ', ', $form->viewedids) }}</div>
									<div class="clearfix"></div>
								@endif
								@if ($lead->websiteid > 0)
									<div class="col-xs-3" style="padding-left:0px;"><label>Property</label></div>
									<div class="col-xs-9"><a href="{{ $form->propertylink }}" target="_blank">{{ $form->propertylink }}</a></div>
									<div class="clearfix"></div>
								@endif

								@if(isset($form_details[12]) &&  $form_details[12] !=="")
									<div class="col-xs-3" style="padding-left:0px;"><label>GA Content</label></div>
									<div class="col-xs-9">{{ $form_details[12] }}</div>
									<div class="clearfix"></div>
								@endif

								@if(isset($form_details[11]) &&  $form_details[11] !=="")
									<div class="col-xs-3" style="padding-left:0px;"><label>GA Campaign</label></div>
									<div class="col-xs-9">{{ isset($form_details[11]) ? $form_details[11] : ""  }}</div>
									<div class="clearfix"></div>
								@endif

								@if(isset($form_details[14]) &&  $form_details[14] !=="")
									<div class="col-xs-3" style="padding-left:0px;"><label>GA Content</label></div>
									<div class="col-xs-9">{{ $form_details[14] }}</div>
									<div class="clearfix"></div>
								@endif

								@if(isset($form_details[13]) &&  $form_details[13] !=="")
									<div class="col-xs-3" style="padding-left:0px;"><label>GA Campaign</label></div>
									<div class="col-xs-9">{{ $form_details[13] }}</div>
									<div class="clearfix"></div>
								@endif

							</div>
						</div>
					</div>
				@endif
				
			</div>
		</div>
	</div>
</div>

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	
	$(document).ready(function(){
		//auto increase by a certain amount
		$("#property-budget").on('keydown', function(event) { 
			var keyCode = event.keyCode || event.which; 
			if (keyCode == 9) { //tab keydown
				var type = jQuery('[name="type"]:checked').val();
				var res = parseFloat($(this).val());
				if (type == 2){
					if (res < 1000000) {
						$(this).val(res*1000000);
					}
				}else{
					if (res < 1000) {
						$(this).val(res*1000);
					}
				}
			} 
		});
		
		loadCommentsEdit();
		loadChangesEdit();
	
	});
	
	function loadCommentsEdit(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/lead/comments/".$lead->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsEdit').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsEdit').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/lead/comments/".$lead->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsEdit').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/lead/comments/".$lead->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsEdit').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsEdit').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function addComment(){
		var comment = jQuery('#newcomment').val().trim();
		//var consultant = jQuery('select[name ="consultantid"] option:selected').text().trim();
		var consultant = jQuery('select[name ="consultantid"]').val();
		var clientName = jQuery('#clientName').text().trim();
		
		if (comment.length > 0){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/lead/commentadd/".$lead->id) }}',
				data: {comments: comment,consultant:consultant, clientName:clientName},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					jQuery('#newcomment').val('');
					loadCommentsEdit();
				},
				error: function (data) {
					console.log(data);
					alert('Something went wrong, please try again!');
				}
			});
		}else{
			alert('Please enter a comment');
		}
	}
	
	$('#newcomment').keydown(function( event ) {
		if ( event.keyCode == 13 ) {
			event.preventDefault();
			addComment();
		}				
	});
	
	function removeComment(t){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'POST',
			url: '{{ url("/lead/commentremove/".$lead->id) }}',
			data: {t: t},
			dataType: 'text',
			cache: false,
			async: false,
			success: function (data) {
				loadCommentsEdit();
			},
			error: function (data) {
				alert('Something went wrong, please try again!');
			}
		});
	}
	
	function loadChangesEdit(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/lead/changes/".$lead->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingChangesEdit').html(data);
			},
			error: function (data) {
				jQuery('#existingChangesEdit').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/lead/changes/".$lead->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingChangesEdit').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/lead/changes/".$lead->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingChangesEdit').html(data);
							},
							error: function (data) {
								jQuery('#existingChangesEdit').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
</script>

@endsection

@section('footer-script')
@endsection