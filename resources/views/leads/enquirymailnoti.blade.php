<html>
    <head>
        <title>{{$subject}}</title>
    </head>
    <body>
        Hello  {{ $consultantName }},
        <p style="text-align: left;">
        You have a new client enquiry: <a href="{{$comment_url}}">{{$comment_url}}</a><br>
        Client Name: {{$client}}<br>

        @php if(!empty($prop_url) ): @endphp
        Property: <a href="{{$prop_url}}">{{$prop_url}}</a>
        @php endif; @endphp
        </p>
    </body>
</html>