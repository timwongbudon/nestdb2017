@extends('layouts.app')

@section('content')



<div class="col-sm-12 nest-new dashboardnew"  >


    
        <div class="col-lg-4 col-sm-6" style="padding: 0;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="nest-property-edit-row">
                        <div class="col-xs-4">
                            <div class="nest-property-edit-label">Year</div>
                        </div>
                        <div class="col-xs-8">
                            <select id="year" class="form-control">
                                @foreach ($years as $y)
                                    <option value="{{ $y }}"
                                    @if ($y == $year)
                                        selected
                                    @endif
                                    >
                                        {{ $y }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <script>
                            jQuery(document).ready(function(){
                                jQuery("#year").on('change', function(event){
                                    var val = jQuery("#year").val();
                                    window.location = "{{ url('market-insight') }}/"+val;
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-6" style="padding: 0;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="nest-property-edit-row">
                        <div class="col-xs-4">
                            <div class="nest-property-edit-label">Month</div>
                        </div>
                        <div class="col-xs-8">
                            <select id="month" class="form-control">
                                @foreach ($months as $key=>$m)
                                    <option value="{{ $key }}"
                                    @if ($key == $month)
                                        selected
                                    @endif
                                    >
                                        {{ $m }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <script>
                            jQuery(document).ready(function(){
                                jQuery("#month").on('change', function(event){
                                    var val = jQuery("#year").val();
                                    var mon = jQuery("#month").val();
                                    window.location = "{{ url('market-insight') }}/"+val+"/"+mon;
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-6" style="padding: 0; ">
            <div class="panel panel-default" style="min-height: 79px;">
                <div class="panel-body">
                    <div class="nest-property-edit-row">
                        <div class="col-xs-12">
                            <div class="nest-property-edit-label">Total Average:  {{$Totalpercentage}}%</div>
                        </div>                      
                        <div class="clearfix"></div>                      
                    </div>
                </div>
            </div>
        </div>
   
        <div style="clear: both;"></div>
        
        <div class="panel panel-default nest-dashboardnew-wrapper" >
            <h4 style="margin-left: 15px; padding: 10px">Market Insight</h4>
            <canvas id="allmonthspaymentsoutstanding" height="87px"></canvas>
        </div>

<div class="panel panel-default nest-dashboardnew-wrapper" id="bracket_w1">
    

    <div class="panel-heading">
      
        <h4>HK$20,000 to HK$39,000</h4>
     
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	
           				
                <td width="15%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="15%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Date</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Rented Price</b>
                </td>	

                <td width="10%" class="text-center">
                    <b>%</b>
                </td>	

                <td width="10%" class="text-center">
                    <b>Status</b>
                </td>

                <td width="10%" class="text-center">
                    <b></b>
                </td>
            </tr>
            <tbody id="bracket1">
                
                
            @if (count($bracket1) > 0)
                @php
                    $prevKey=0;
                    $percentage = 0;
                    $count = 0;
                @endphp

                @foreach ($bracket1 as  $key=> $offer)     

                
                        @php
                        $hasChild = "";
                        $strike = "";
                        $parent = "";
                        $x ="-";
                        $Invoice="";
                      

                            if($key != $prevKey){
                                $prevKey  = $key;
                                $class = "parent";
                                $parent = "parent";
                                

                                if(count($offer) > 1){
                                    $hasChild = "haschild";
                                }

                                if(!empty($offer['commissiondate']) ){
                                    $x = "<span style='color: green'>&#10004;</span>";
                                    $Invoice = true;
                                } else {
                                    $Invoice = false;
                                }
                              
                            }else {
                                $class = "child child".$key;     

                                if($Invoice == true){
                                    $x = "<span style='color: red'>&#10006</span>";
                                }                 
                                
                            }
                            /** override */
                             $x = "<span style='color: green'>&#10004;</span>";


                             $percentage = $percentage +  $offer['percentage'];
                             $count++;
                        @endphp
                    <tr id="{{$key}}" class="{{ $class }} {{ $hasChild }} {{$strike}}">

                       
                        <td width="15%"  class="text-left">
                            {{$offer['cnfirstname']}} {{$offer['cnlastname']}}
                        </td>
                        <td width="15%"  class="text-left">
                            {{$offer['cfirstname']}} {{$offer['clastname']}}
                        </td>
                        <td width="15%"  class="text-center">
                            @php                            
                                echo json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";                          
                            @endphp
                        </td>
                        <td width="15%" class="text-center">
                        @php
                            echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                          
                            /*echo $offer['commissiondate'];*/
                            @endphp
                        </td>
                        <td width="10%" class="text-left">
                            <a href="{{url('/property/edit/'.$offer['propertyid'])}}">
                                {{$offer['property']}}
                            </a>
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;
                            @php                 
                           
                           echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>

                        <td width="10%" class="text-center">                            
                            {{$offer['percentage']}}
                        </td>

                        <td width="10%" class="text-center">                            
                           @php        
                           echo $x;                       
                           
                           @endphp
                        </td>

                     
                        
                        <td width="10%" class="text-center">                            
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                                                            
                    </tr>
                 

                @endforeach
                <tr>
                    <td colspan="3" class="text-right"> <strong>Average:</strong> </td>
                    <td  class="text-left" style="padding-left: 53px;"> <strong>{{number_format($percentage / $count, 2, '.', ',')}}%</strong></td>
                    @php
                        $bracket1 = number_format($percentage / $count, 2, '.', ',');
                    @endphp
                </tr>
            @else    
                <tr>
                    <td colspan="6" class="text-left">No data found.</td>
                </tr>
            @endif
      
            </tbody>
            </table>
            </div>
    </div>
</div>


<div class="panel panel-default nest-dashboardnew-wrapper" id="bracket_w2">
    <div class="panel-heading">
      
        <h4>HK$40,000 to HK$79,000</h4>
     
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	              						
                <td width="15%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="15%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Date</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking</b>
                </td>
                <td width="10%" class="text-center">
                <b>Rented Price</b>
                </td>	

                <td width="10%" class="text-center">
                <b>%</b>
                </td>

                <td width="10%" class="text-center">
                    <b>Status</b>
                </td>
                <td width="10%" class="text-center">
                    <b></b>
                </td>
            </tr>
            <tbody id="bracket2">
                
                
            @if (count($bracket2) > 0)
                @php
                    $prevKey=0;
                    $percentage = 0;
                    $count = 0;
                @endphp

                @foreach ($bracket2 as  $key=> $offer)     

                
                        @php
                        $hasChild = "";
                        $strike = "";
                        $parent = "";
                        $x ="-";
                        $Invoice="";
                      

                            if($key != $prevKey){
                                $prevKey  = $key;
                                $class = "parent";
                                $parent = "parent";
                                

                                if(count($offer) > 1){
                                    $hasChild = "haschild";
                                }

                                if(!empty($offer['commissiondate']) ){
                                    $x = "<span style='color: green'>&#10004;</span>";
                                    $Invoice = true;
                                } else {
                                    $Invoice = false;
                                }
                              
                            }else {
                                $class = "child child".$key;     

                                if($Invoice == true){
                                    $x = "<span style='color: red'>&#10006</span>";
                                }                 
                                
                            }

                            /** override */
                             $x = "<span style='color: green'>&#10004;</span>";

                             $percentage = $percentage +  $offer['percentage'];
                             $count++;
                            
                        @endphp
                    <tr id="{{$key}}" class="{{ $class }} {{ $hasChild }} {{$strike}}">

                       
                        <td  width="15%"  class="text-left">
                            {{$offer['cnfirstname']}} {{$offer['cnlastname']}}
                        </td>
                        <td  width="15%"  class="text-left">
                            {{$offer['cfirstname']}} {{$offer['clastname']}}
                        </td>
                        <td  width="15%"  class="text-center">
                            @php                            
                                echo json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";                          
                            @endphp
                        </td>
                        <td  width="15%" class="text-center">
                        @php
                            echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                          
                            /*echo $offer['commissiondate'];*/
                            @endphp
                        </td>
                        <td width="10%" class="text-left">
                            <a href="{{url('/property/edit/'.$offer['propertyid'])}}">
                                {{$offer['property']}}
                            </a>
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;
                            @php                 
                           
                           echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>

                        <td width="10%" class="text-center">                            
                            {{$offer['percentage']}}
                        </td>

                        <td width="10%" class="text-center">                            
                           @php        
                           echo $x;                       
                            /*   if($offer['status']!=="" && isset($statuses[$offer['status']]) && $statuses[$offer['status']] == "Completed"){
                                   echo "<span style='color:green; font-weight:bold'>Completed</span>";
                               } else {
                                 
                                   if(!empty($offer['status'])){
                                     echo $statuses[$offer['status']];
                                   } else {
                                       echo "-";
                                   }
                                   
                               }
                               */
                           @endphp
                        </td>
                        
                        <td width="10%" class="text-center">                            
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                                                            
                    </tr>
                 

                @endforeach
                <tr>
                    <td colspan="3" class="text-right"> <strong>Average:</strong> </td>
                    <td  class="text-left" style="padding-left: 53px;"> <strong>{{number_format($percentage / $count, 2, '.', ',')}}%</strong></td>
                    @php
                        $bracket2 = number_format($percentage / $count, 2, '.', ',');
                    @endphp
                </tr>
            @else    
                <tr>
                    <td colspan="6" class="text-left">No data found.</td>
                </tr>
            @endif
      
            </tbody>
            </table>
            </div>
    </div>
</div>


<div class="panel panel-default nest-dashboardnew-wrapper" id="bracket_w3">
    <div class="panel-heading">
      
        <h4>HK$80,000 to HK$129,000</h4>
     
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	
              						
                <td width="15%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="15%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Date</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking</b>
                </td>
                <td width="10%" class="text-center">
                <b>Rented Price</b>
                </td>	
                <td width="10%" class="text-center">
                <b> %</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Status</b>
                </td>
                <td width="10%" class="text-center">
                    <b></b>
                </td>
            </tr>
            <tbody id="bracket3">
                
                
            @if (count($bracket3) > 0)
                @php
                    $prevKey=0;
                    $percentage = 0;
                    $count = 0;
                @endphp

                @foreach ($bracket3 as  $key=> $offer)     

                
                        @php
                        $hasChild = "";
                        $strike = "";
                        $parent = "";
                        $x ="-";
                        $Invoice="";
                      

                            if($key != $prevKey){
                                $prevKey  = $key;
                                $class = "parent";
                                $parent = "parent";
                                

                                if(count($offer) > 1){
                                    $hasChild = "haschild";
                                }

                                if(!empty($offer['commissiondate']) ){
                                    $x = "<span style='color: green'>&#10004;</span>";
                                    $Invoice = true;
                                } else {
                                    $Invoice = false;
                                }
                              
                            }else {
                                $class = "child child".$key;     

                                if($Invoice == true){
                                    $x = "<span style='color: red'>&#10006</span>";
                                }                 
                                
                            }

                            /** override */
                             $x = "<span style='color: green'>&#10004;</span>";


                             $percentage = $percentage +  $offer['percentage'];
                             $count++;
                            
                        @endphp
                    <tr id="{{$key}}" class="{{ $class }} {{ $hasChild }} {{$strike}}">

                       
                        <td width="15%"  class="text-left">
                            {{$offer['cnfirstname']}} {{$offer['cnlastname']}}
                        </td>
                        <td width="15%"  class="text-left">
                            {{$offer['cfirstname']}} {{$offer['clastname']}}
                        </td>
                        <td width="15%" class="text-center">
                            @php                            
                                echo json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";                          
                            @endphp
                        </td>
                        <td width="15%" class="text-center">
                        @php
                            echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                          
                            /*echo $offer['commissiondate'];*/
                            @endphp
                        </td>
                        <td width="10%" class="text-left">
                            <a href="{{url('/property/edit/'.$offer['propertyid'])}}">
                                {{$offer['property']}}
                            </a>
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;
                            @php                 
                           
                           echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>

                        <td width="10%" class="text-center">                            
                            {{$offer['percentage']}}
                        </td>

                        <td width="10%" class="text-center">                            
                           @php        
                           echo $x;                       
                            /*   if($offer['status']!=="" && isset($statuses[$offer['status']]) && $statuses[$offer['status']] == "Completed"){
                                   echo "<span style='color:green; font-weight:bold'>Completed</span>";
                               } else {
                                 
                                   if(!empty($offer['status'])){
                                     echo $statuses[$offer['status']];
                                   } else {
                                       echo "-";
                                   }
                                   
                               }
                               */
                           @endphp
                        </td>
                        
                        <td width="10%" class="text-center">                            
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                                                            
                    </tr>
                 

                @endforeach
                <tr>
                    <td colspan="3" class="text-right"> <strong>Average:</strong> </td>
                    <td  class="text-left" style="padding-left: 53px;"> <strong>{{number_format($percentage / $count, 2, '.', ',')}}%</strong></td>
                    @php
                        $bracket3 = number_format($percentage / $count, 2, '.', ',');
                    @endphp
                </tr>
            @else    
                <tr>
                    <td colspan="6" class="text-left">No data found.</td>
                </tr>
            @endif
      
            </tbody>
            </table>
            </div>
    </div>
</div>



<div class="panel panel-default nest-dashboardnew-wrapper" id="bracket_w4">
    <div class="panel-heading">
      
        <h4>HK$130,000 to HK$199,000</h4>
     
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	
               						
                <td width="15%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="15%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Date</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking</b>
                </td>
                <td width="10%" class="text-center">
                <b>Rented Price</b>
                </td>	
                <td width="10%" class="text-center">
                <b> %</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Status</b>
                </td>
                <td width="10%" class="text-center">
                    <b></b>
                </td>
            </tr>
            <tbody id="bracket4">
                
                
            @if (count($bracket4) > 0)
                @php
                    $prevKey=0;
                    $percentage = 0;
                    $count = 0;
                @endphp

                @foreach ($bracket4 as  $key=> $offer)     

                
                        @php
                        $hasChild = "";
                        $strike = "";
                        $parent = "";
                        $x ="-";
                        $Invoice="";
                      

                            if($key != $prevKey){
                                $prevKey  = $key;
                                $class = "parent";
                                $parent = "parent";
                                

                                if(count($offer) > 1){
                                    $hasChild = "haschild";
                                }

                                if(!empty($offer['commissiondate']) ){
                                    $x = "<span style='color: green'>&#10004;</span>";
                                    $Invoice = true;
                                } else {
                                    $Invoice = false;
                                }
                              
                            }else {
                                $class = "child child".$key;     

                                if($Invoice == true){
                                    $x = "<span style='color: red'>&#10006</span>";
                                }                 
                                
                            }

                            /** override */
                             $x = "<span style='color: green'>&#10004;</span>";

                             $percentage = $percentage +  $offer['percentage'];
                             $count++;

                        @endphp
                    <tr id="{{$key}}" class="{{ $class }} {{ $hasChild }} {{$strike}}">

                     
                        <td width="15%" class="text-left">
                            {{$offer['cnfirstname']}} {{$offer['cnlastname']}}
                        </td>
                        <td width="15%" class="text-left">
                            {{$offer['cfirstname']}} {{$offer['clastname']}}
                        </td>
                        <td width="15%" class="text-center">
                            @php                            
                                echo json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";                          
                            @endphp
                        </td>
                        <td width="15%" class="text-center">
                        @php
                            echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                          
                            /*echo $offer['commissiondate'];*/
                            @endphp
                        </td>
                        <td width="10%" class="text-left">
                            <a href="{{url('/property/edit/'.$offer['propertyid'])}}">
                                {{$offer['property']}}
                            </a>
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;
                            @php                 
                           
                           echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>
                        
                        <td width="10%" class="text-center">                            
                            {{$offer['percentage']}}
                        </td>

                        <td width="10%" class="text-center">                            
                           @php        
                           echo $x;                       
                            /*   if($offer['status']!=="" && isset($statuses[$offer['status']]) && $statuses[$offer['status']] == "Completed"){
                                   echo "<span style='color:green; font-weight:bold'>Completed</span>";
                               } else {
                                 
                                   if(!empty($offer['status'])){
                                     echo $statuses[$offer['status']];
                                   } else {
                                       echo "-";
                                   }
                                   
                               }
                               */
                           @endphp
                        </td>
                        
                        <td width="10%" class="text-center">                            
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                                                            
                    </tr>
                 

                @endforeach
                <tr>
                    <td colspan="3" class="text-right"> <strong>Average:</strong> </td>
                    <td  class="text-left" style="padding-left: 53px;"> <strong>{{number_format($percentage / $count, 2, '.', ',')}}%</strong></td>
                    @php
                        $bracket4 = number_format($percentage / $count, 2, '.', ',');
                    @endphp
                </tr>
            @else    
                <tr>
                    <td colspan="6" class="text-left">No data found.</td>
                </tr>
            @endif
      
            </tbody>
            </table>
            </div>
    </div>
</div>




<div class="panel panel-default nest-dashboardnew-wrapper" id="bracket_w5">
    <div class="panel-heading">
      
        <h4>HK$200,000+++</h4>
     
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	               						
                <td width="15%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="15%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Date</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Rented Price</b>
                </td>	
                <td width="10%" class="text-center">
                <b> %</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Status</b>
                </td>
                <td width="10%" class="text-center">
                    <b></b>
                </td>
            </tr>
            <tbody id="bracket5">
                
                
            @if (count($bracket5) > 0)
                @php
                    $prevKey=0;
                    $percentage = 0;
                    $count = 0;
                @endphp

                @foreach ($bracket5 as  $key=> $offer)     

                
                        @php
                        $hasChild = "";
                        $strike = "";
                        $parent = "";
                        $x ="-";
                        $Invoice="";
                      

                            if($key != $prevKey){
                                $prevKey  = $key;
                                $class = "parent";
                                $parent = "parent";
                                

                                if(count($offer) > 1){
                                    $hasChild = "haschild";
                                }

                                if(!empty($offer['commissiondate']) ){
                                    $x = "<span style='color: green'>&#10004;</span>";
                                    $Invoice = true;
                                } else {
                                    $Invoice = false;
                                }
                              
                            }else {
                                $class = "child child".$key;     

                                if($Invoice == true){
                                    $x = "<span style='color: red'>&#10006</span>";
                                }                 
                                
                            }

                            /** override */
                             $x = "<span style='color: green'>&#10004;</span>";


                             $percentage = $percentage +  $offer['percentage'];
                             $count++;
                            
                        @endphp
                    <tr id="{{$key}}" class="{{ $class }} {{ $hasChild }} {{$strike}}">

                       
                        <td width="15%" class="text-left">
                            {{$offer['cnfirstname']}} {{$offer['cnlastname']}}
                        </td>
                        <td width="15%" class="text-left">
                            {{$offer['cfirstname']}} {{$offer['clastname']}}
                        </td>
                        <td width="15%" class="text-center">
                            @php                            
                                echo json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";                          
                            @endphp
                        </td>
                        <td width="15%" class="text-center">
                        @php
                            echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                          
                            /*echo $offer['commissiondate'];*/
                            @endphp
                        </td>
                        <td width="10%" class="text-left">
                            <a href="{{url('/property/edit/'.$offer['propertyid'])}}">
                                {{$offer['property']}}
                            </a>
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td width="10%" class="text-center">
                            HK$&nbsp;
                            @php                 
                           
                           echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>

                        <td width="10%" class="text-center">                            
                            {{$offer['percentage']}}
                        </td>

                        <td width="10%" class="text-center">                            
                           @php        
                           echo $x;                       
                            /*   if($offer['status']!=="" && isset($statuses[$offer['status']]) && $statuses[$offer['status']] == "Completed"){
                                   echo "<span style='color:green; font-weight:bold'>Completed</span>";
                               } else {
                                 
                                   if(!empty($offer['status'])){
                                     echo $statuses[$offer['status']];
                                   } else {
                                       echo "-";
                                   }
                                   
                               }
                               */
                           @endphp
                        </td>

                        
                        
                        <td width="10%" class="text-center">                            
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                                                            
                    </tr>
                 

                @endforeach
                <tr>
                    <td colspan="3" class="text-right"> <strong>Average:</strong> </td>
                    <td  class="text-left" style="padding-left: 53px;"> <strong>{{number_format($percentage / $count, 2, '.', ',')}}%</strong></td>
                    @php
                        $bracket5 = number_format($percentage / $count, 2, '.', ',');
                    @endphp
                </tr>
            @else    
                <tr>
                    <td colspan="6" class="text-left">No data found.</td>
                </tr>
            @endif
      
            </tbody>
            </table>
            </div>
    </div>
</div>

    </div>

<div class="clearfix"></div>


<style>
#bracket_w1,
#bracket_w2,
#bracket_w3,
#bracket_w4,
#bracket_w5{
    display: none;
}    
#bracket1,
#bracket2,
#bracket3,
#bracket4,
#bracket5{
   /* min-height: 100px;
    height: 300px;
    overflow-x: scroll;
    min-height: 50px;
    max-height: 300px;
    height: auto;*/

}

tbody {
    display:block;
   /* height:50px;*/
    overflow:auto;
}
thead, tbody tr {
    display:table;
    width:100%;
    table-layout:fixed;
}
thead {
    width: calc( 100% - 1em )
}
table {
    width:100%;
}
    
</style>



<script type="text/javascript">
var ctx = document.getElementById("allmonthspaymentsoutstanding").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["HK$20,000 to HK$39,000", "HK$40,000 to HK$79,000", "HK$80,000 to HK$129,000", "HK$130,000 to HK$199,000", "HK$200,000+++"],
			datasets: [{
				label: 'Percentage discount per price bracket',
				backgroundColor: ['#e74c3c','#001680','#03a6bd','#03bd19','#ff8202'],
				data: [{{$bracket1_percentage}},{{$bracket2_percentage}},{{$bracket3_percentage}},{{$bracket4_percentage}},{{$bracket5_percentage}}]
			},]
		},
		options: {
			onClick: function(event, array){
              
				var index = array[0]._index+1;
                $('#bracket_w1').hide();
                $('#bracket_w2').hide();
                $('#bracket_w3').hide();
                $('#bracket_w4').hide();
                $('#bracket_w5').hide();

                $('#bracket_w'+index).show();
				//var year = {{ date('Y') }};
				//window.location = "/commission/invoicing?year="+year+"&month="+month+"";
			},
			responsive: true,
            legend: {
                display: false
            },
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					min: 0,
					beginAtZero: true,
					stacked: true,
					ticks: {
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return '' + value + '%';
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';

						label = ''+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+"%";

						return label;
					}
				}
			},
			/*annotation: {
				annotations: [{
					type: 'line',
					mode: 'horizontal',
					scaleID: 'y-axis-0',
					value: 1000000,
					borderColor: 'rgba(0, 0, 0, 0)',
					borderWidth: 0,
					label: {
						enabled: false
					}
				}]
			}*/
		}
	});
</script>
@endsection
