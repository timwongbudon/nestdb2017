@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}

input[type="text"],
select{
	width: 100%;
	border: 1px solid #666;
	padding: 5px 5px;

}
</style>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker(
		{ dateFormat: 'yy-mm-dd' }
	);
  } );

  $(document).ready(function(){
	  $('.filter').submit(function(e){
		 
		if( $('#datepicker1').val()=="" && $('#datepicker1').val() ==""){
			
		//	e.preventDefault();
		}
	});
  });

</script>

<div class="nest-new dashboardnew">
	<div class="row">
			<div class="nest-dashboardnew-wrapper">

				<div class="panel panel-default " style="padding: 5px;">
					
					   <h4 style="margin-left: 10px;">Filter</h4>
					
					<form action="" class="filter" method="GET">

						<div class="col-sm-2">
							Consultant: <br>
							<select name="consultantid" class="consultantid">
							<option value="">Select</option>
								@foreach ($userarr as $user)
								
									@if($user->status == 0 && $user->isConsultant() == true )
										@if($user->id == $inputs['consultantid'] )
											<option value="{{$user->id}}" selected>{{$user->name}}</option>
										@else 
											<option value="{{$user->id}}">{{$user->name}}</option>
										@endif
									@endif
								@endforeach
							</select>
						</div>
						<div class="col-sm-1">
							From:<br> <input type="text" autocomplete="off" value="{{$inputs['from']}}" name="from" id="datepicker1" class="datepicker">
						</div>
						<div class="col-sm-1">
							To: <br><input type="text" autocomplete="off" value="{{$inputs['to']}}" name="to" id="datepicker2" class="datepicker">
						</div>

						<div class="col-sm-2">
							<input type="submit" class="btn btn-primary" style="margin-top: 16px; border: none" value="Filter">
							<a class="btn btn-primary" href="{{url('websitenquiriesall')}}" style="margin-top: 16px; border: none">Reset</a>
						</div>
						
						<div class="clearfix"></div>
					</form>	
				</div>				
				
			</div>
	</div>
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Property Contact Form (All)</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list" style="max-height:none;">
							@if (!empty($propertyforms) && count($propertyforms) > 0)
								@foreach ($propertyforms as $lead)
									<div class="nest-dashboardnew-lead">
										<div class="col-xs-2" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
										<div class="col-xs-2">
											<a href="{{ url('websitenquiriesshowproperty/'.$lead->id) }}">{{ $lead->name }}</a>
										
										</div>
										<div class="col-xs-3">
											@if ($lead->forwarduser > 0)
												@if ($lead->status == 3 || $lead->status == 4)
													forwarded to {{ $userarr[$lead->forwarduser]->name }}
												@endif
											@endif
											
											@if ($lead->status == 10)											
												@if(isset($lead->consultantid))
													 passed on to {{ $userarr[$lead->consultantid]->name }}
												@endif
											@endif
										</div>

										<div class="col-xs-3">{{ isset(unserialize($lead->form_details)['14'] ) && unserialize($lead->form_details)['14'] !="" ?  'Contact via EDM' : 'Contact via Website'}}</div>

										<div class="col-xs-2">
											@if ($lead->status == 0)
												Open
											@elseif ($lead->status == 1)
												Deleted
											@elseif ($lead->status == 3)
												Forwarded Pending
											@elseif ($lead->status == 4)
												Forwarded Finished
											@elseif ($lead->status == 10)
												Passed On 
												@if(isset($lead->passon_consultantid))
													
												@endif
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>

  				@if($filter == true)
					Total: {{count($propertyforms)}}
				@endif

			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Generic Contact Form (All)</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list" style="max-height:none;">
							@if (!empty($contactforms) && count($contactforms) > 0)
								@foreach ($contactforms as $lead)
									<div class="nest-dashboardnew-lead">
										<div class="col-xs-2" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
										<div class="col-xs-2">
											<a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a>
											
										</div>
										<div class="col-xs-3">
										@if ($lead->landlorduser > 0)
												@if ($lead->status == 3 || $lead->status == 4)
													managed by {{ $userarr[$lead->landlorduser]->name }}
												@endif
											@elseif ($lead->forwarduser > 0)
												@if ($lead->status == 3 || $lead->status == 4)
													forwarded to {{ $userarr[$lead->forwarduser]->name }}
												@endif
											@endif

										@if($lead->status == 10)
											{{ isset($userarr[$lead->consultantid]) ? 'passed on to '.$userarr[$lead->consultantid]->name : '' }}
										@endif
										</div>

										<div class="col-xs-3">{{ !empty($lead->form_details) && unserialize($lead->form_details)['14'] != ''  ?  'Contact via EDM' : 'Contact via Website'}}</div>

										<div class="col-xs-2">
											@if ($lead->status == 0)
												Open
											@elseif ($lead->status == 1)
												Deleted
											@elseif ($lead->status == 3)
												@if ($lead->landlorduser > 0)
													Landlord Pending
												@elseif ($lead->forwarduser > 0)
													Forwarded Pending
												@endif
											@elseif ($lead->status == 4)
												@if ($lead->landlorduser > 0)
													Landlord Finished
												@elseif ($lead->forwarduser > 0)
													Forwarded Finished
												@endif
											@elseif ($lead->status == 10)
												Passed On
											@endif
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
				@if($filter == true)
					Total: {{count($contactforms)}}
				@endif
			</div>
			<div class="clearfix"></div>
			
		</div>
	</div>
</div>
@endsection
