@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Property Contact Form</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($propertyforms) && count($propertyforms) > 0)
								@foreach ($propertyforms as $lead)
									@if ($lead->status == 0)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-3"><a href="{{ url('websitenquiriescreateproperty/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ isset(unserialize($lead->form_details)['14'] ) && unserialize($lead->form_details)['14'] !="" ?  'Contact via EDM' : 'Contact via Website'}}</div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Generic Contact Form</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list">
							@if (!empty($contactforms) && count($contactforms) > 0)
								@foreach ($contactforms as $lead)
									@if ($lead->status == 0)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-3"><a href="{{ url('websitenquiriescreatecontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ !empty($lead->form_details) && unserialize($lead->form_details)['14'] != ''  ?  'Contact via EDM' : 'Contact via Website'}}</div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			@if (!empty($landlordforms) && count($landlordforms) > 0)
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Hong Kong Landlord Enquiries</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($landlordforms) && count($landlordforms) > 0)
									@foreach ($landlordforms as $lead)
										@if ($lead->landlordregion == 1)
											<div class="nest-dashboardnew-lead">
												<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
												<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
												<div class="col-xs-3">{{ $lead->phone }}</div>
												<div class="clearfix"></div>
											</div>
										@endif
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>International Landlord Enquiries</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($landlordforms) && count($landlordforms) > 0)
									@foreach ($landlordforms as $lead)
										@if ($lead->landlordregion == 2)
											<div class="nest-dashboardnew-lead">
												<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
												<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
												<div class="col-xs-3">{{ $lead->phone }}</div>
												<div class="clearfix"></div>
											</div>
										@endif
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			@endif
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<a href="{{ url('/websitenquiriesall') }}" class="nest-button btn btn-default"><i class="fa fa-btn fa-info-circle"></i> Show All Archived Enquiries</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
