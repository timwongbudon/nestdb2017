@extends('layouts.app')

@section('content')

<div class="col-sm-12 nest-new dashboardnew"  >
	<div class=""></div>
<div class="panel panel-default nest-dashboardnew-wrapper">
    <div class="panel-heading">      
        <h4>Consultant Overview {{$month !=0 ? date("F", mktime(0, 0, 0, $month, 1)): "" }} {{ $year }}</h4>      
    </div>

    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	
                							
                <td width="12%" class="text-left">
                    <b>Consultant</b>
                </td>
                <td width="14%" class="text-left">
                    <b>Client</b>
                </td>
                <td width="10%" class="text-left">
                    <b>Offering Date</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Offering Signed</b>
                </td>
                <td width="15%" class="text-center">
                    <b>Property</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Asking Price</b>
                </td>
                <td width="10%" class="text-center">
                    <b>Rented Price</b>
                </td>	 
                <td width="10%" class="text-left">
                    <b>Commission</b>
                </td>	
                <td width="5%" class="text-left">
                    <b>Status</b>
                </td>    
                <td width="10%" class="text-left">
                    
                </td>                
            </tr>
            <tbody id="offerspagination">
                    @php
                        $totalComm = 0;
                    @endphp
                @foreach ($offers as $offer)
                    @php
                        $totalComm = $totalComm + $offer['commissionconsultant'];

                        if(!empty($offer['commissiondate']) ){
                                    $x = "<span style='color: green'>&#10004;</span>";
                                   
                                } else {
                                    $x = "<span> - </span>";
                                }
                    @endphp

                    <tr>
                        <td>{{$offer['cn_name']}}</td>
                        <td>{{$offer['c_name']}}</td>
                        <td class="text-left">
                            @php                            
                                echo isset($offer['fieldcontents']) && json_decode($offer['fieldcontents'])->f1 !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime(json_decode($offer['fieldcontents'])->f1)) : "-";
                            @endphp
                        </td>
                        <td class="text-center">
                            @php
                                echo !empty($offer['commissiondate']) ? date('d.m.Y',strtotime($offer['commissiondate'])) : "-";                                                      
                            @endphp
                        </td>
                        <td class="text-center"><a href="{{url('/property/edit/'.$offer['propertyid'])}}">{{$offer['property']}}</a></td>
                        <td class="text-center">
                            HK$&nbsp;{{ number_format($offer['asking_rent'], 0, '.', ',') }}
                        </td>
                        <td class="text-center">
                            HK$&nbsp;
                            @php
                                echo number_format($offer['offerprice'], 0, '.', ',');											
                            @endphp
                        </td>
                        <td>
                            HK$&nbsp;{{number_format($offer['commissionconsultant'], 2, '.', ',')}}
                        </td>
                        <td>
                            @php echo $x; @endphp
                        </td>
                        <td>
                            <a href="{{url('/lead/edit/'.$offer['leadsid'])}}">View</a>
                        </td>
                    </tr>
                @endforeach

                <tr>
                    <td colspan="7" class="text-right"><strong>Total: </strong></td>
                    <td><strong>HK$&nbsp;{{number_format($totalComm, 2, '.', ',')}}</strong></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
            </table>
            </div>
    </div>
</div>

    </div>

<div class="clearfix"></div>



@endsection