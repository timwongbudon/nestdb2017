@extends('layouts.app')

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('lead/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Enquiry Information</h4>
						</div>
						<div class="panel-body">
							@include('common.errors')
							
							<input type="hidden" value="{{ $clientid }}" name="clientid" />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Consultant</div>
								</div>
								<div class="col-xs-8">
									<!-- consultantid -->
									<select name="consultantid" class="form-control">
										@foreach ($users as $u)
											@if ($u->isConsultant())
												<option value="{{ $u->id }}" 
												@if ($u->id == Auth::user()->id)
													selected
												@endif
												>
												@if ($u->status == 1)
													[inactive]
												@endif
												{{ $u->name }}
												</option>
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Status</div>
								</div>
								<div class="col-xs-8">
									<!-- status -->
									<select name="status" class="form-control">
										@foreach ($avstatus as $si => $stat)
											<option value="{{ $si }}">{{ $stat }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Contact</div>
								</div>
								<div class="col-xs-8">
									<input type="date" name="dateinitialcontact" class="form-control" value="{{ old('dateinitialcontact') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Outreach</div>
								</div>
								<div class="col-xs-8">
									<input type="date" name="lastoutreach" class="form-control" value="{{ old('lastoutreach') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Remind Me</div>
								</div>
								<div class="col-xs-8">
									<input type="date" name="remindme" class="form-control" value="{{ old('remindme') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Source</div>
								</div>
								<div class="col-xs-8">
									<!-- sourcehear -->
									<select name="sourcehear" class="form-control">
										@foreach ($avsources as $si => $sour)
											<option value="{{ $si }}">{{ $sour }}</option>
										@endforeach
									</select>
									
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Request Details</h4>
						</div>
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Type</div>
								</div>
								<div class="col-xs-4">
									<label for="type-1" class="control-label">{!! Form::radio('type', 1, true, ['id'=>'type-1']) !!} Lease</label>
								</div>
								<div class="col-xs-4">
									<label for="type-2" class="control-label">{!! Form::radio('type', 2, false, ['id'=>'type-2']) !!} Sale</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<input type="hidden" name="clienttype" value="1" />						
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Budget From</div>
								</div>
								<div class="col-xs-8">
									<!-- budget -->
									<input type="number" name="budget" id="property-budget" class="form-control" value="{{ old('budget') }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Budget To</div>
								</div>
								<div class="col-xs-8">
									<!-- budget -->
									<input type="number" name="budgetto" id="property-budgetto" class="form-control" value="{{ old('budgetto') }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Bedrooms</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="req_beds" class="form-control" value="{{ old('req_beds') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Bathrooms</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="req_baths" class="form-control" value="{{ old('req_baths') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Maid's Room(s)</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="req_maidrooms" class="form-control" value="{{ old('req_maidrooms') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Carpark(s)</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="req_carparks" class="form-control" value="{{ old('req_carparks') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Outdoor Space</div>
								</div>
								<div class="col-xs-8">
									<textarea name="req_outdoor" class="form-control">{{{ Input::old('req_outdoor') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Facilities</div>
								</div>
								<div class="col-xs-8">
									<textarea name="req_facilites" class="form-control">{{{ Input::old('req_facilites') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Likes / Dislikes</div>
								</div>
								<div class="col-xs-8">
									<textarea name="likes" class="form-control">{{{ Input::old('likes') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 style="text-decoration:none;">&nbsp;</h4>
						</div>
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Currently Living</div>
								</div>
								<div class="col-xs-8">
									<textarea name="currentflat" class="form-control">{{{ Input::old('currentflat') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Reason for Move</div>
								</div>
								<div class="col-xs-8">
									<textarea name="reasonformove" class="form-control">{{{ Input::old('reasonformove') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Notice Period</div>
								</div>
								<div class="col-xs-8">
									<textarea name="noticeperiod" class="form-control">{{{ Input::old('noticeperiod') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Preferred Area(s)</div>
								</div>
								<div class="col-xs-8">
									<textarea name="preferredareas" class="form-control">{{{ Input::old('preferredareas') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Other Comments</div>
								</div>
								<div class="col-xs-8">
									<textarea name="comments" class="form-control">{{{ Input::old('comments') }}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Submit </button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	
	$(document).ready(function(){
		//auto increase by a certain amount
		$("#property-budget").on('keydown', function(event) { 
			var keyCode = event.keyCode || event.which; 
			if (keyCode == 9) { //tab keydown
				var type = jQuery('[name="type"]:checked').val();
				var res = parseFloat($(this).val());
				if (type == 2){
					if (res < 1000000) {
						$(this).val(res*1000000);
					}
				}else{
					if (res < 1000) {
						$(this).val(res*1000);
					}
				}
			} 
		});
	});
</script>

@endsection

@section('footer-script')
@endsection