@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		@if ($user->isInformationofficer())
			<div class="nest-dashboardnew-wrapper">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Property Additions</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($infoofficermessages) && count($infoofficermessages) > 0)
									@foreach ($infoofficermessages as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-4" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-8"><a href="{{ url('iopropertyshow/'.$lead->id) }}">{{ $userarr[$lead->userid]->name }}</a></div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Property Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($propertyforms) && count($propertyforms) > 0)
									@foreach ($propertyforms as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowproperty/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>General Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($contactforms) && count($contactforms) > 0)
									@foreach ($contactforms as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Finished Property Additions</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($infoofficermessagesold) && count($infoofficermessagesold) > 0)
									@foreach ($infoofficermessagesold as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-4" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-8"><a href="{{ url('iopropertyshow/'.$lead->id) }}">{{ $userarr[$lead->userid]->name }}</a></div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Finished Property Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($propertyformsold) && count($propertyformsold) > 0)
									@foreach ($propertyformsold as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowproperty/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Finished General Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($contactformsold) && count($contactformsold) > 0)
									@foreach ($contactformsold as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		@else
			<div class="nest-dashboardnew-wrapper">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Property Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($propertyforms) && count($propertyforms) > 0)
									@foreach ($propertyforms as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowproperty/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>General Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($contactforms) && count($contactforms) > 0)
									@foreach ($contactforms as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Finished Property Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($propertyformsold) && count($propertyformsold) > 0)
									@foreach ($propertyformsold as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowproperty/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Finished General Messages</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-dashboardnew-list">
								@if (!empty($contactformsold) && count($contactformsold) > 0)
									@foreach ($contactformsold as $lead)
										<div class="nest-dashboardnew-lead">
											<div class="col-xs-3" style="padding-left:0px;">{{ $lead->getDateTime() }}</div>
											<div class="col-xs-6"><a href="{{ url('websitenquiriesshowcontact/'.$lead->id) }}">{{ $lead->name }}</a></div>
											<div class="col-xs-3">{{ $lead->phone }}</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				
			</div>
		@endif
	</div>
</div>
@endsection
