@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading" style="padding-bottom:15px;">
						<h4>Message to Information Officer</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-body">
							@include('common.errors')
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Comments</div>
								</div>
								<div class="col-xs-8">
									<textarea name="comments" class="form-control" style="height:200px;">{{ old('comments') }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<br />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Nest Picture Upload</div>
								</div>
								<div class="col-xs-8">
									<input type="file" name="nestpictureupload[]" class="form-control" value="{{ old('nestpictureupload') }}" multiple>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Other Picture Upload</div>
								</div>
								<div class="col-xs-8">
									<input type="file" name="otherpictureupload[]" class="form-control" value="{{ old('otherpictureupload') }}" multiple>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">HK Homes Pictures</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="hkhomeslink" class="form-control" value="{{ old('hkhomeslink') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-body">
							@include('common.errors')
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Property Source</div>
								</div>
								<div class="col-xs-8">
									<select name="propertysource" class="form-control">
										<option value="Landlord">Landlord</option>
										<option value="Agent">Agent</option>
										<option value="EPRC">EPRC</option>
										<option value="Referral">Referral</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<br />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">PoC Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="pocname" class="form-control" value="{{ old('pocname') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">PoC Email</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="pocemail" class="form-control" value="{{ old('pocemail') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">PoC Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="poctel" class="form-control" value="{{ old('poctel') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<br />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Double Check Database</div>
								</div>
								<div class="col-xs-4">
									<label for="type-1" class="control-label">{!! Form::radio('dcheckdb', 1, true, ['id'=>'dcheckdb-1']) !!} Yes</label>
								</div>
								<div class="col-xs-4">
									<label for="type-2" class="control-label">{!! Form::radio('dcheckdb', 2, false, ['id'=>'dcheckdb-2']) !!} No</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<br />
							<div class="nest-property-edit-row">
								<div class="col-xs-12">
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-envelope"></i>Send to Information Officer </button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	
	$(document).ready(function(){
		//auto increase by a certain amount
		$("#property-budget").on('keydown', function(event) { 
			var keyCode = event.keyCode || event.which; 
			if (keyCode == 9) { //tab keydown
				var type = jQuery('[name="type"]:checked').val();
				var res = parseFloat($(this).val());
				if (type == 2){
					if (res < 1000000) {
						$(this).val(res*1000000);
					}
				}else{
					if (res < 1000) {
						$(this).val(res*1000);
					}
				}
			} 
		});
		
	});
	
</script>

@endsection

@section('footer-script')
@endsection