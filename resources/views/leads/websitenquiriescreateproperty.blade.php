@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Form Content</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-dashboardnew-list" style="max-height:none;">
							<div class="col-xs-3" style="padding-left:0px;"><label>Date</label></div>
							<div class="col-xs-9">{{ $form->getDateTime() }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Name</label></div>
							<div class="col-xs-9">{{ $form->name }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Phone</label></div>
							<div class="col-xs-9">{{ $form->phone }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Email</label></div>
							<div class="col-xs-9">{{ $form->email }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Budget From</label></div>
							<div class="col-xs-9">{{ $form->budgetfrom }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Budget To</label></div>
							<div class="col-xs-9">{{ $form->budgetto }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Message</label></div>
							<div class="col-xs-9">{{ $form->message }}</div>
							<div class="clearfix"></div>
							<div class="col-xs-3" style="padding-left:0px;"><label>Newsletter</label></div>
							<div class="col-xs-9">
								@if ($form->newsletter == 1)
									Yes
								@else
									No
								@endif
							</div>
							<div class="clearfix"></div>
							@if (trim($form->viewedids) != '')
								<div class="col-xs-3" style="padding-left:0px;"><label>Viewed Properties</label></div>
								<div class="col-xs-9">{{ str_replace(',', ', ', $form->viewedids) }}</div>
								<div class="clearfix"></div>
							@endif
							<div class="col-xs-3" style="padding-left:0px;"><label>Property</label></div>
							<div class="col-xs-9"><a href="{{ $form->propertylink }}" target="_blank">{{ $form->propertylink }}</a></div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<a href="{{ url('/websitenquiriesremoveproperty/'.$form->id) }}" class="btn btn-danger"><i class="fa fa-btn fa-trash-o"></i> Delete Enquiry</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Create Consultant Enquiry</h4>
					</div>
					
					<div class="panel-body">
						<form action="" method="POST">
							{{ csrf_field() }}
							<input type="hidden" value="{{ $form->id }}" name="websiteid" />
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Consultant</div>
								</div>
								<div class="col-xs-8">
									<select name="consultantid" class="form-control">
										@foreach ($users as $u)
											@if ($u->isConsultant())
												@if ($u->status != 1)
													<option value="{{ $u->id }}">
														{{ $u->name }}
													</option>
												@endif
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Type</div>
								</div>
								<div class="col-xs-4" style="padding-top:8px;">
									<label for="type-1" class="control-label">{!! Form::radio('type', 1, true, ['id'=>'type-1']) !!} Lease</label>
								</div>
								<div class="col-xs-4" style="padding-top:8px;">
									<label for="type-2" class="control-label">{!! Form::radio('type', 2, false, ['id'=>'type-2']) !!} Sale</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" style="margin-top:10px;"><i class="fa fa-btn fa-arrow-right"></i>Pass on to Consultant </button>
						</form>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Forward To User</h4>
					</div>
					
					<div class="panel-body">
						<form action="" method="POST">
							{{ csrf_field() }}
							<input type="hidden" value="{{ $form->id }}" name="websiteid" />
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">User</div>
								</div>
								<div class="col-xs-8">
									<select name="userid" class="form-control">
										@foreach ($users as $u)
											@if ($u->status != 1)
												<option value="{{ $u->id }}">
													{{ $u->name }}
												</option>
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" style="margin-top:10px;"><i class="fa fa-btn fa-arrow-right"></i>Pass on to User </button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addCommentMessage();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Similar Existing Clients</h4>
					</div>
					
					<div class="panel-body">
						<b>Search by Email:</b><br />
						@if (count($emailclients) > 0)
							@foreach ($emailclients as $index => $c)
								@if ($index < 10)
									<a href="/client/show/{{ $c->id }}" target="_blank">{{ $c->name() }}</a> 
									@if (isset($userbyid[$c->user_id]))
										(ID: {{ $c->id }} / {{ $userbyid[$c->user_id]->name }}):
									@else
										(ID: {{ $c->id }}):
									@endif
									{{ $c->email() }} / {{ $c->email2() }}<br />
								@endif
							@endforeach
							@if ($index >= 10)
								...<br />
							@endif
						@else
							-<br />
						@endif
						<br />
						
						<b>Search by Phone Number:</b><br />
						@if (count($phoneclients) > 0)
							@foreach ($phoneclients as $index => $c)
								@if ($index < 10)
									<a href="/client/show/{{ $c->id }}" target="_blank">{{ $c->name() }}</a> 
									@if (isset($userbyid[$c->user_id]))
										(ID: {{ $c->id }} / {{ $userbyid[$c->user_id]->name }}):
									@else
										(ID: {{ $c->id }}):
									@endif
									{{ $c->tel() }} / {{ $c->tel2() }}<br />
								@endif
							@endforeach
							@if ($index >= 10)
								...<br />
							@endif
						@else
							-<br />
						@endif
						<br />
						
						<b>Search by Name:</b><br />
						@if (count($nameclients) > 0)
							@foreach ($nameclients as $index => $c)
								@if ($index < 10)
									<a href="/client/show/{{ $c->id }}" target="_blank">{{ $c->name() }}</a> 
									@if (isset($userbyid[$c->user_id]))
										(ID: {{ $c->id }} / {{ $userbyid[$c->user_id]->name }})
									@else
										(ID: {{ $c->id }})
									@endif
									<br />
								@endif
							@endforeach
							@if ($index >= 10)
								...<br />
							@endif
						@else
							-<br />
						@endif
						<br />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
		</div>
	</div>
</div>

<script>
	function loadCommentsMessage(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/websiteenquiryproperty/comments/".$form->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsEdit').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsEdit').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/websiteenquiryproperty/comments/".$form->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsEdit').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/websiteenquiryproperty/comments/".$form->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsEdit').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsEdit').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function addCommentMessage(){
		var comment = jQuery('#newcomment').val().trim();
		if (comment.length > 0){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/websiteenquiryproperty/commentadd/".$form->id) }}',
				data: {comment: comment},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					jQuery('#newcomment').val('');
					loadCommentsMessage();
				},
				error: function (data) {
					console.log(data);
					alert('Something went wrong, please try again!');
				}
			});
		}else{
			alert('Please enter a comment');
		}
	}
	
	$('#newcomment').keydown(function( event ) {
		if ( event.keyCode == 13 ) {
			event.preventDefault();
			addCommentMessage();
		}				
	});
	
	function removeCommentMessage(t){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'POST',
			url: '{{ url("/websiteenquiryproperty/commentremove/".$form->id) }}',
			data: {t: t},
			dataType: 'text',
			cache: false,
			async: false,
			success: function (data) {
				loadCommentsMessage();
			},
			error: function (data) {
				alert('Something went wrong, please try again!');
			}
		});
	}
	
	jQuery( document ).ready(function(){
		loadCommentsMessage();
	});
	
</script>



@endsection
