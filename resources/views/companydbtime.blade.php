@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>


<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Year/Month</div>
							</div>
							<div class="col-xs-8">
								<select id="year" class="form-control">
									@foreach ($yearmonths as $y)
										<option value="{{ $y['y'].'-'.$y['m'] }}"
										@if ($y['y'] == $year && $y['m'] == $month)
											selected
										@endif
										>
											{{ $months[$y['m']].' '.$y['y'] }}
										</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Users</div>
							</div>
							<div class="col-xs-8">
								<select id="user" class="form-control">
									@foreach ($users2 as $y)
									<option value="{{ $y->id }}"
									@if ($selected_user == $y->id)
											selected
										@endif
									>{{ $y->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#year").on('change', function(event){
										var val = jQuery("#year").val();
										var user = jQuery("#user").val();
										window.location = "{{ url('companydbtime') }}/"+val+"/"+user;
									});

									jQuery("#user").on('change', function(event){
										var user = jQuery("#user").val();
										var val = jQuery("#year").val();
										window.location = "{{ url('companydbtime') }}/"+val+"/"+user;
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			
			@foreach ($metrics as $userid => $m)
				@if ($userid > 0)
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
							   <h4>{{ $usersbyid[$userid]->name }}</h4>
							</div>
							<div class="panel-body">
								<canvas id="user{{ $userid }}"></canvas>
								
								<script>
									var ctx = document.getElementById("user{{ $userid }}").getContext('2d');
									var cChart = new Chart(ctx, {
										type: 'horizontalBar',
										data: {
											labels: [<?php
												for ($i = intval($lastdayofmonth); $i >= 1; $i--){
													if ($i == 1 || $i == 21 || $i == 31){
														echo "'".$months[$month]." ".$i."st'";
													}else if ($i == 2 || $i == 22){
														echo "'".$months[$month]." ".$i."nd'";
													}else if ($i == 3 || $i == 23){
														echo "'".$months[$month]." ".$i."rd'";
													}else{
														echo "'".$months[$month]." ".$i."th'";
													}
													
													
													
													if ($i > 1){
														echo ",";
													}
												}
											?>],
											datasets: [
												{
													data: [
														<?php
															for ($i = intval($lastdayofmonth); $i >= 1; $i--){
																if ($i > 1){
																	echo '{x: '.($m[$i]['time']/60).'},';
																}else{
																	echo '{x: '.($m[$i]['time']/60).'}';
																}
															}
														?>
													]
												}
											]
										},
										options: {
											legend: {
												display: false
											},
											scales: {
												xAxes: [{
													beginAtZero: true,
													ticks: {
														max: 12,
														min: 0,
														stepSize: 1,
														callback: function(value, index, values) {
															return value+'h';
														}
													}
												}]
											},
											tooltips: {
												callbacks: {
													label: function(t, d) {
														var label = ' ';
														
														if (Math.floor(t.xLabel) == 1){
															label = label + Math.floor(t.xLabel) + ' hour ';
														}else{
															label = label + Math.floor(t.xLabel) + ' hours ';
														}
														
														if (Math.floor((t.xLabel-Math.floor(t.xLabel))*60) == 1){
															label = label + '' + Math.floor((t.xLabel-Math.floor(t.xLabel))*60) + ' minute';
														}else{
															label = label + '' + Math.floor((t.xLabel-Math.floor(t.xLabel))*60) + ' minutes';
														}
														
														return label;
													}
												}
											}
										}
									});
								</script>
								
								
							</div>
						</div>
					</div>
				@endif
			@endforeach 
			
			
			
		</div>
	</div>
</div>
@endsection
