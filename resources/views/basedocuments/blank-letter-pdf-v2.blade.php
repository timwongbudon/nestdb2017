<html>
<head>

<title>Custom Letter</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 1.2cm 1.5cm 0.5cm 1.5cm;
}

.nest-pdf-sl-top-left{
	padding-top:40px;
	padding-bottom:0px;
}
.nest-pdf-sl-bold{
	font-family:MyriadBold;
}
.nest-pdf-sl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-sl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-sl-content{
	text-align:justify;
}
.nest-pdf-sl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-pdf-sl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-sl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-sl-faithfully{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-1{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-lfl-faithfully{
	
}
.nest-pdf-lfl-signature{
	padding:20px 0px 10px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

#footer{
	height: 10px !important;
	margin-bottom: -3px !important;
	font-family:Myriad;
}

#footer a {
	background-color:#C5C2C0;
}

.nest-hide-first-header{
	margin-top:-95px;
}
.nest-pdf-lfl-consultant{
	line-height: unset !important;
}

</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
    </div>
    <div class="nest-hide-first-header">
        <div style="width: 49%; display: inline-block; text-align: left !important">
            <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
        </div>
        <div style="width: 50%; display: inline-block;">
            <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
        </div>
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>

		@if ( request()->get('version') == 2 ) 
			<div class="nest-pdf-sl-top-left">
				@if (trim($data['fieldcontents']['f1']) != '')
					{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
				@else
					xxxxxx<br /><br />
				@endif
				@if (trim($data['fieldcontents']['f2']) != '')
					To: {{ $data['fieldcontents']['f2'] }}<br />
				@endif
				@if (trim($data['fieldcontents']['f3']) != '')
					Attn: {{ $data['fieldcontents']['f3'] }}<br />
				@else
					<br /><br />
				@endif
				<br /><br /><br /><br /><br /><br />
				@if (trim($data['fieldcontents']['f4']) != '')
					Dear {{ $data['fieldcontents']['f4'] }},
				@endif
			</div>	
		@else
			<div class="nest-pdf-sl-top-left">
				@if (trim($data['fieldcontents']['f2']) != '')
					To: {{ $data['fieldcontents']['f2'] }}<br />
				@endif
				@if (trim($data['fieldcontents']['f3']) != '')
					Attn: {{ $data['fieldcontents']['f3'] }}<br />
				@else
					<br /><br />
				@endif
				<br />
				@if (trim($data['fieldcontents']['f1']) != '')
					{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br /><br />
				@else
					xxxxxx<br /><br />
                @endif
                <br />
				@if (trim($data['fieldcontents']['f4']) != '')
					Dear {{ $data['fieldcontents']['f4'] }},
				@endif
			</div>	
		@endif

		
		<div class="nest-pdf-sl-top-premises">
			{{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			{!! nl2br($data['fieldcontents']['f6']) !!}
		</div>
		@if (trim($data['fieldcontents']['f7']) == 'Consultant')
			<table cellpadding="0" cellspacing="0" border="0"><tr><td>
				<div class="nest-pdf-lfl-bottom-1">
					Yours sincerely, 	
				</div>
				<div class="nest-pdf-lfl-signature">
					<br /><br /><br />
				</div>
				<div class="nest-pdf-lfl-consultant">
					{{ $data['fieldcontents']['f8'] }}<br />
					{{ $data['fieldcontents']['f9'] }}<br />
					Residential Search & Investment
				</div>
				@if (trim($data['fieldcontents']['f10']) != '')
					<div class="nest-pdf-lfl-licensnr">
						Individual Licence No: {{ $data['fieldcontents']['f10'] }}
					</div>
				@endif
			</td></tr></table>
		@elseif (trim($data['fieldcontents']['f7']) == '1 Party')
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%"></td>
				</tr>
				<tr>
					<td>
					@if (isset($data['fieldcontents']['f11']) && trim($data['fieldcontents']['f11']) != '')
						{{ trim($data['fieldcontents']['f11']) }}
					@endif
					</td>
					<td></td>
					<td>
					</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		@elseif (trim($data['fieldcontents']['f7']) == '2 Parties')
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
				</tr>
				<tr>
					<td>
					@if (isset($data['fieldcontents']['f11']) && trim($data['fieldcontents']['f11']) != '')
						{{ trim($data['fieldcontents']['f11']) }}
					@endif
					</td>
					<td></td>
					<td>
					@if (isset($data['fieldcontents']['f12']) && trim($data['fieldcontents']['f12']) != '')
						{{ trim($data['fieldcontents']['f12']) }}
					@endif
					</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td></td>
					<td>Date:</td>
				</tr>
			</table>
		@endif
	</div>
</body>
</html>
