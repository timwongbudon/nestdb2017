@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('basedocuments/dismissalletter') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Letter of Dismissal Fields</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							{{ Form::hidden('id', $data['id']) }}
							{{ Form::hidden('docversion', $data['docversion']) }}
							
							@if ($data['id'] == 0)
								<input type="hidden" name="consultantid" value="{{ Auth::user()->id }}" />
							@else
								<input type="hidden" name="consultantid" value="{{ $data['consultantid'] }}" />
							@endif
							
							@foreach ($fields as $field)
								@if (count($field) >= 3)
									@if ($field[1] == 'date')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<input type="date" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'hidden')
										{{ Form::hidden('f'.$field[0], old('f'.$field[0], $data['fieldcontents']['f'.$field[0]])) }}
									@elseif ($field[1] == 'text')
										<div class="nest-property-edit-row nest-property-custom-row-field-{{ $field[0] }}">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												@if (isset($field[3]))
													<input type="text" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}" placeholder="{{ $field[3] }}">
												@else
													<input type="text" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
												@endif
											</div>
											<div class="clearfix"></div>
										</div>	
									@elseif ($field[1] == 'number')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<input type="number" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>		
									@elseif ($field[1] == 'radio')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											@php
												$rbs = explode('#', $field[3]);
											@endphp
											<div class="col-xs-8">
												@foreach ($rbs as $index => $rb)
													@if (isset($data['fieldcontents']['f'.$field[0]]))
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, $data['fieldcontents']['f'.$field[0]] == $rb, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@else
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, false, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@endif
												@endforeach
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'textarea')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<textarea name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" rows="12">{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}</textarea>
											</div>
											<div class="clearfix"></div>
										</div>
									@endif
								@endif
							@endforeach
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					@if (false)
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Letter of Dismissal Content</h4>
							</div>

							<div class="panel-body">
								<b>Tick the boxes for the parts you don't want to show up in the contract:</b><br />
								<div class="nest-form-builder-hide-wrapper">
									@foreach ($hides as $field)
										@if (count($field) >= 4)
											@if ($field[1] == 0)
												@if ($field[2] == 1)
													<div class="nest-form-builder-hide-section">
														{{ $field[3] }}
													</div>
													<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
														{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} Hide this section
													</label>
												@else
													<div class="nest-form-builder-hide-section">
														{{ $field[3] }} (tick what to hide)
													</div>
												@endif
											@else
												<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
													{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} {{ $field[3] }}
												</label>
											@endif
										@endif
									@endforeach
									@foreach ($hiddenhides as $hh)
										{{ Form::hidden('hidevals[]', $hh) }}
									@endforeach
								</div>
							</div>
						</div>
					@endif
					<div class="panel panel-default">
						<div class="panel-body">
							@foreach ($replaceFields as $key => $name)
								<div class="nest-property-edit-row">
									<div class="nest-property-edit-label">Replace Text for Section "{{ $name }}"
									@if (!isset($data['replace'][$key]) || trim($data['replace'][$key]) == '')
										<a href="javascript:;" onClick="showReplace('{{ $key }}');">show</a>
									@endif
									</div>
									@if (isset($data['replace'][$key]) && trim($data['replace'][$key]) != '')
										<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
									@else
										<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2" style="display:none;">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
									@endif
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-xs-12">
								@if ($data['id'] > 0)
									<a id="pdfbutton" href="{{ url('/basedocuments/dismissalletterpdf/'.$data['id'].'/no') }}" target="_blank" class="pdfbuttonhide nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download Letterhead PDF </a>
									<a id="pdfbutton" href="{{ url('/basedocuments/dismissalletterpdf/'.$data['id']) }}" target="_blank" class="pdfbuttonhide nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download PDF </a>
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								@else
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Save </button>
								@endif
								<a href="{{ url('/basedocuments') }}" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-arrow-left"></i>Back to Documents
								</a>
							</div>
						</div>
					</div>
				</div>
				
			</form>
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('.pdfbuttonhide').css('display', 'none');
	});
	jQuery('textarea').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('.pdfbuttonhide').css('display', 'none');
	});
	function showReplace(id){
		jQuery('#replace-'+id).css('display', 'block');
	}
	$( document ).ready(function() {
		
	});
</script>
@endsection
