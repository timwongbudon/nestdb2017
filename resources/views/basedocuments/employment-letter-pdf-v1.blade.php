<html>
<head>

<title>Employment Letter</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-sl-top-left{
	padding-top:180px;
	padding-bottom:0px;
}
.nest-pdf-sl-bold{
	font-family:MyriadBold;
}
.nest-pdf-sl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-sl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-sl-content{
	text-align:justify;
}
.nest-pdf-sl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-100px;
	text-align:right;
	padding-top:60px;
}
.nest-hide-first-header img{
	width:140px;
	padding-right:20px;
}
.nest-pdf-sl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-sl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-sl-faithfully{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-1{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-lfl-faithfully{
	
}
.nest-pdf-lfl-signature{
	padding:20px 0px 100px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
				<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		@else
			<div class="nest-pdf-letterhead" style="width:152px;">
				<br /><br /><br /><br /><br /><br /><br /><br />
		@endif
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-sl-top-left">
			<br /><br /><br />
			<br /><br /><br /><br /><br /><br />
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			<br />
		</div>
		<div class="nest-pdf-sl-top-premises">
			{{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			This is to certify that {{ $data['fieldcontents']['f2'] }}, the holder of HKID No. {{ $data['fieldcontents']['f3'] }} has been employed by our company since {{ $data['fieldcontents']['f4'] }}. {{ $data['fieldcontents']['f6'] }}
		</div>
		<table cellpadding="0" cellspacing="0" border="0"><tr><td>
			<div class="nest-pdf-lfl-bottom-1">
				Yours faithfully, 
			</div>
			<div class="nest-pdf-lfl-signature">
				As agent for<br />
				NEST PROPERTY LIMITED
				<br />
			</div>
			<div class="nest-pdf-lfl-consultant">
				{{ $data['fieldcontents']['f7'] }}<br />
				{{ $data['fieldcontents']['f8'] }}
			</div>
			@if (trim($data['fieldcontents']['f9']) != '')
				<div class="nest-pdf-lfl-licensnr">
					Individual Licence No: {{ $data['fieldcontents']['f9'] }}
				</div>
			@endif
		</td></tr></table>
	</div>
</body>
</html>

















