<html>
<head>

<title>Employment Letter</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">

<style>
@page {
	margin: 3cm 1.5cm 1cm 1.5cm;
}
#header {
	top: -78px;
	padding-bottom:5px;
	z-index:10;
}
#footer {
	
	height: 10px !important;
	margin-bottom: -22px !important;
}
#footer a{
background-color: #c5c2c0 !important;
}
.nest-pdf-ol-top-subject-header{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	text-align:right;
	text-transform:uppercase;
	text-decoration:underline;
	color:#000000;
	font-size:14px;
	padding-top:15px;
	padding-bottom:10px;
}

.heading-section{
	font-family:MyriadBold;
	padding-bottom:3px;
	border-bottom:2px solid #000000;
	margin-bottom:10px;
	font-size:16px;
}

.nest-offer-letter-title {
	font-size: 16px;
	display: block;
	margin-bottom: 20px;
}

.nest-pdf-sl-top-left {
	margin-top: 12px;
}

.nest-pdf-sl-top-premises{
	margin-top: 80px;
}

.nestdb-header-date {
	margin-top: -3px;
	margin-bottom: 20px;
	display: block;
}
.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-163px;
	text-align:right;
	padding-top:50px;
	padding-bottom: 15px; 
}
.nest-pdf-sl-bold {
	font-family:MyriadBold;
}
.text-justify{
	text-align:justify;
}

.nest-pdf-lfl-signature{
	padding:20px 0px 100px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

.nest-pdf-lfl-bottom-1{
	padding-top:17px;
}
</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<br><br>
		<div class="nest-pdf-sl-top-left">
			
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			<br />
		</div>
		<div class="nest-pdf-sl-top-premises">
			{{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			This is to certify that {{ $data['fieldcontents']['f2'] }}, the holder of HKID No. {{ $data['fieldcontents']['f3'] }} has been employed by our company since {{ $data['fieldcontents']['f4'] }}. {{$data['fieldcontents']['f6']}}</p>
		</div>
		<table cellpadding="0" cellspacing="0" border="0"><tr><td>
			<div class="nest-pdf-lfl-bottom-1">
				Yours faithfully, 
			</div>
			<div class="nest-pdf-lfl-signature">
				As agent for<br />
				NEST PROPERTY LIMITED
				<br />
			</div>
			<div class="nest-pdf-lfl-consultant">
				{{ $data['fieldcontents']['f7'] }}<br />
				{{ $data['fieldcontents']['f8'] }}
			</div>
			@if (trim($data['fieldcontents']['f9']) != '')
				<div class="nest-pdf-lfl-licensnr">
					Individual Licence No: {{ $data['fieldcontents']['f9'] }}
				</div>
			@endif
		</td></tr></table>
	</div>
</body>
</html>