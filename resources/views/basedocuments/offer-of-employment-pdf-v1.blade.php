<html>
<head>

<title>Offer of Employment</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 3cm 1.7cm 1.5cm;
}

#header,
#footer{
	position: fixed;
	left: 0;
	right: 0;
	color: #000000;
	font-size: 75%;
}
#footer{
	opacity:0.5;
}

#header {
	top: -70px;
	padding-bottom:5px;
	z-index:10;
}

#footer {
	bottom: 0;
	border-top: 0.1pt solid #000000;
	padding-top:5px;
}

#header table,
#footer table {
	width: 100%;
	border-collapse: collapse;
	border: none;
}

#footer td {
	padding: 0;
	width: 50%;
}

.page-number {
  text-align: right;
  font-size:75%;
}

.page-number:before {
  content: counter(page);
}


.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-58px;
	text-align:right;
}
.nest-hide-first-header img{
	width:160px;
}

.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
	margin-top:5px;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-sl-top-left{
	padding-top:0px;
	padding-bottom:0px;
}
.nest-pdf-sl-bold{
	font-family:MyriadBold;
}
.nest-pdf-sl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-sl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-sl-content{
	text-align:justify;
}
.nest-pdf-sl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-pdf-sl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-sl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-sl-faithfully{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-1{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-lfl-faithfully{
	
}
.nest-pdf-lfl-signature{
	padding:20px 0px 100px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}
.heading-section{
	font-family:MyriadBold;
	padding-bottom:3px;
	border-bottom:2px solid #000000;
	margin-bottom:10px;
	font-size:16px;
}
.text-justify{
	text-align:justify;
}

</style>


</head>
<body>
	<div id="header" style="padding-top:0px;padding-left:5px;padding-right:4px;">
		@if ($logo != 'no')
			<div style="text-align:right;">
				<img src="{{ url('/images/tools/nest-letterhead-logo-header.png') }}" style="padding-top:10px;width:80px;" />
			</div>
		@endif
	</div>
	<div id="footer" style="border-top:0px !important;">
		<table><tr>
			<td><div class="page-number" style="text-align:center !important;"></div></td>
		</tr></table>
	</div>
	@if ($logo == 'no')
		<div class="nest-hide-first-header" style="visibility:hidden;">
			
		</div>
	@else
		<div class="nest-hide-first-header">
			<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		</div>
	@endif
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
		@else
			<div class="nest-pdf-letterhead" style="width:152px;padding-top:4px;">
				<br /><br /><br />
		@endif
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-sl-top-left">
			@if ($logo == 'no')
				<br /><br /><br /><br /><br /><br /><br /><br />
			@endif
			<br /><br /><br /><br /><br /><br />
			<div class="nest-pdf-sl-bold">Offer Letter</div><br /><br />
			{{ $data['fieldcontents']['f2'] }}<br />
			HKID No. {{ $data['fieldcontents']['f3'] }}<br />
			<br />
			{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}
			<br /><br /><br /><br /><br />
			On Behalf Of <br /><br />
			<div class="nest-pdf-sl-bold" style="font-size:20px;">Nest Property Limited</div><br /><br />
		</div>
		
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br /><br /><br /><br /><br /><br /><br /><br /><br />
		
		<div class="nest-pdf-sl-bold">Contact:</div>
		{{ $data['fieldcontents']['f4'] }}<br />
		{{ $data['fieldcontents']['f5'] }}<br />
		{{ $data['fieldcontents']['f6'] }}<br />
		
		<div style="page-break-after:always;"></div>
		
		<table width="100%"><tr><td>
		Dear {{ $data['fieldcontents']['f7'] }},<br /><br />
		<div class="text-justify">
			@if (isset($data['replace']['first']) && trim($data['replace']['first']) != '')
				{!! nl2br($data['replace']['first']) !!}
			@else
				It gives me great pleasure to offer you a position of employment with Nest Property Limited. This is an exciting time in the development of the company and I very much hope and look forward to you becoming an integral part of our dynamic team, in what will continue to be a highly successful venture.
			@endif
		</div>
		</td></tr></table>
		<br />
		@if (!in_array('1_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">COMMENCEMENT DATE</div>
			<div class="text-justify">
				@if (isset($data['replace']['commencement']) && trim($data['replace']['commencement']) != '')
					{!! nl2br($data['replace']['commencement']) !!}
				@else
					{{ $data['fieldcontents']['f8'] }}
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('2_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">JOB TITLE</div>
			<div class="text-justify">
				@if (isset($data['replace']['jobtitle']) && trim($data['replace']['jobtitle']) != '')
					{!! nl2br($data['replace']['jobtitle']) !!}
				@else
					{{ $data['fieldcontents']['f9'] }}
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('3_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">BASIC SALARY</div>
			<div class="text-justify">
				@if (isset($data['replace']['basicsalary']) && trim($data['replace']['basicsalary']) != '')
					{!! nl2br($data['replace']['basicsalary']) !!}
				@else
					You will receive monthly commission on all completed transactions, details of which are set out below.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('4_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">GROUP MANDATORY PROVIDENT FUND SCHEME (MPF)</div>
			<div class="text-justify">
				@if (isset($data['replace']['mpf']) && trim($data['replace']['mpf']) != '')
					{!! nl2br($data['replace']['mpf']) !!}
				@else
					You are required to enter into the Group Mandatory Provident Fund Scheme with HSBC. Your monthly mandatory deductions will vary month to month, capped at HK$1,500.00. The company will match your contribution to a maximum of 5% of your monthly basic salary subject to Inland Revenue limits.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('5_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">WORKING HOURS</div>
			<div class="text-justify">
				@if (isset($data['replace']['workinghours']) && trim($data['replace']['workinghours']) != '')
					{!! nl2br($data['replace']['workinghours']) !!}
				@else
					Our standard office hours are from {{ $data['fieldcontents']['f10'] }}, Monday to Friday inclusive, with a one hour lunch break from 1-2 pm. Bearing in mind the nature of our work and client deadlines, we do require all employees to remain flexible about working hours, as it is oftentimes necessary to extend these hours as dictated by client demands. We therefore ask you to remain fluid and cater your working hours around your clients and client deadlines.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('6_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">PROBATION PERIOD</div>
			<div class="text-justify">
				@if (isset($data['replace']['probationperiod']) && trim($data['replace']['probationperiod']) != '')
					{!! nl2br($data['replace']['probationperiod']) !!}
				@else
					The first three months of employment will be deemed a probationary period. During the first month, either party may end this agreement by serving one day notice period whereas for the second and third month, one week notice period will be required. Thereafter, the notice period will be set at one month.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('7_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">HOLIDAY</div>
			<div class="text-justify">
				@if (isset($data['replace']['holiday']) && trim($data['replace']['holiday']) != '')
					{!! nl2br($data['replace']['holiday']) !!}
				@else
					{{ $data['fieldcontents']['f11'] }}
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('8_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">CODE OF CONDUCT</div>
			<div class="text-justify">
				@if (isset($data['replace']['codeofconduct']) && trim($data['replace']['codeofconduct']) != '')
					{!! nl2br($data['replace']['codeofconduct']) !!}
				@else
					The Management expects you to conduct and behave yourself properly and at all times both within and without the company premises. You agree to conduct the business within the philosophy, company policies and corporate culture of the Company and at all times to uphold the image, goodwill, business, profits and reputation of the Company.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('9_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">CONFIDENTIALITY, CONFLICT OF INTEREST</div>
			<div class="text-justify">
				@if (isset($data['replace']['confidentiality']) && trim($data['replace']['confidentiality']) != '')
					{!! nl2br($data['replace']['confidentiality']) !!}
				@else
					You should not leak any information, proprietary or otherwise, relating to the operation of the Company to any person or to any organization. Violation of this may result in disciplinary action being taken against you.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('10_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">EXCLUSIVITY OF SERVICE</div>
			<div class="text-justify">
				@if (isset($data['replace']['exclusivity']) && trim($data['replace']['exclusivity']) != '')
					{!! nl2br($data['replace']['exclusivity']) !!}
				@else
					You shall not, without the written consent of the Company during the continuance of this Agreement, be engaged or interested, either directly or indirectly, in any capacity, in any trade, business or occupation whatsoever, other than the business of the Company. In this Agreement, the expression "occupation" shall include any public or other office that is likely to interfere with, or otherwise affect the performance of the {{ $data['fieldcontents']['f12'] }} - in {{ $data['fieldcontents']['f13'] }} job role under this Agreement. In the furtherance of this clause, the {{ $data['fieldcontents']['f12'] }} is required to declare her interests from time to time as requested by the Company.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('11_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">JOB DESCRIPTION</div>
			<div class="text-justify">
				@if (isset($data['replace']['jobdesc']) && trim($data['replace']['jobdesc']) != '')
					{!! nl2br($data['replace']['jobdesc']) !!}
				@else
					@php
						$econd = explode("\n", $data['fieldcontents']['f14']);
					@endphp
					<ul>
					@foreach ($econd as $ec)
						@if (trim($ec) != '')
							<li>{{ $ec }}</li>
						@endif
					@endforeach
					</ul>
					@if (isset($data['replace']['jobdescbottom']) && trim($data['replace']['jobdescbottom']) != '')
						{!! nl2br($data['replace']['jobdescbottom']) !!}
					@else
						N.B. You shall be responsible for maintaining a valid Estate Agent’s or Salesperson's License during your employment with the Company.
					@endif
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('12_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">INSURANCE COVER</div>
			<div class="text-justify">
				@if (isset($data['replace']['insurance']) && trim($data['replace']['insurance']) != '')
					{!! nl2br($data['replace']['insurance']) !!}
				@else
					Under the Employees’ Compensation Ordinance, you will be covered by a valid insurance policy maintained by the Company. Further details of the scheme may be provided upon request.
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('13_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">FACILITIES, SUPPORT & EXPENSES</div>
			<div class="text-justify">
				@if (isset($data['replace']['facilities']) && trim($data['replace']['facilities']) != '')
					{!! nl2br($data['replace']['facilities']) !!}
				@else
					The Company agrees to provide facilities as deemed necessary to conduct as {{ $data['fieldcontents']['f9'] }}.
					<br /><br />
					Facilities provided by the Company are:
					@php
						$econd = explode("\n", $data['fieldcontents']['f15']);
					@endphp
					<ul>
					@foreach ($econd as $ec)
						@if (trim($ec) != '')
							<li>{{ $ec }}</li>
						@endif
					@endforeach
					</ul>
					
					<br />
					The Company shall NOT be liable to the {{ $data['fieldcontents']['f9'] }} for any expenses incurred in the following:
					@php
						$econd = explode("\n", $data['fieldcontents']['f16']);
					@endphp
					<ul>
					@foreach ($econd as $ec)
						@if (trim($ec) != '')
							<li>{{ $ec }}</li>
						@endif
					@endforeach
					</ul>
				@endif
			</div>
			</td></tr></table>
			<br />
		@endif
		@if (!in_array('14_0', $data['sectionshidden']))
			<table width="100%"><tr><td>
			<div class="heading-section">COMMISSION STRUCTURE</div>
			<div class="text-justify">
				@if (isset($data['replace']['commission']) && trim($data['replace']['commission']) != '')
					{!! nl2br($data['replace']['commission']) !!}
				@else
					When you satisfactorily conclude a property transaction, the Consultant shall be entitled to earn commission on the total fee generated for the Company, as set out below:<br />
					@php
						$econd = explode("\n", $data['fieldcontents']['f17']);
					@endphp
					<ul>
					@foreach ($econd as $ec)
						@if (trim($ec) != '')
							<li>{{ $ec }}</li>
						@endif
					@endforeach
					</ul>
					Commission will be payable at the end of the month in which the invoice is settled by the client, if the monies are received prior to the 25th of the month.
					<br /><br />
					The Company shall have final say in determining the commission to be charged to clients for any services performed. When you conclude a transaction for which a commission is earned, you shall be entitled to the commission as set out above.
					<br /><br />
					The commission entitlement is at the Company 's discretion and is subject to variations and substitutions from time to time by the Company provided due notice is given beforehand. If there is any dispute on commission, it will be resolved based on the final decision of the Company.
				@endif
			</div>
			</td></tr></table>
		@endif
		

		<div style="page-break-after:always;"></div>
		<div class="text-justify">
			@if (isset($data['replace']['lastpage']) && trim($data['replace']['lastpage']) != '')
				{!! nl2br($data['replace']['lastpage']) !!}
			@else
				I very much hope this clarifies all the key points of our offer of employment, however please let me know if you have any questions or queries. To accept this offer of employment, please indicate your express agreement by dating and countersigning the enclosed copy of this letter.
				<br /><br />
				This is a very exciting and significant opportunity for everyone involved in the business and we wish you the greatest of success. You will of course have the total commitment, respect and support of the Nest Property Limited team, enabling you to achieve your personal and professional objectives and to feel rewarded in your job satisfaction. On behalf of Nest Property Limited, may I say how much we are looking forward to you being part of what has become a very successful and dynamic team.
			@endif
		</div>
		
		<br />
		<br /><br />
		
		Yours sincerely,
		
		<div class="nest-pdf-ol-signature" style="margin:10px 0px;">
			@if ($consultant->sigpath != '')
				<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" style="height:100px;" />
			@else
				<br /><br /><br /><br /><br />
			@endif
		</div>
		
		{{ $data['fieldcontents']['f4'] }}<br />
		{{ $data['fieldcontents']['f5'] }}<br />
		Date: {{ \NestDate::nest_date_format($data['fieldcontents']['f1'], 'Y-m-d') }}
		
		<br /><br /><br />
		<div class="text-justify">
			I hereby confirm that I understand and accept the above terms and as such enter into an employment agreement with Nest Property Limited.
		</div>
		<br />
		<br /><br />
		<br /><br />
		<br /><br />
		
		<div style="text-align:right;">
			<div style="float:right;display:inline-block;width:240px;border-top:1px solid #000000;padding-top:10px;text-align:left;">
				&nbsp; &nbsp; &nbsp; {{ $data['fieldcontents']['f2'] }}<br />
				&nbsp; &nbsp; &nbsp; HKID: {{ $data['fieldcontents']['f3'] }}<br />
				&nbsp; &nbsp; &nbsp; Date: <br />
			</div>
		</div>
		
		
		
		
		
	</div>
</body>
</html>

















