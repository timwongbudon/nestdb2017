<html>
<head>

<title>Letter of Dismissal</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 3cm 1.5cm 1cm 1.5cm;
}
#header {
	top: -78px;
	padding-bottom:5px;
	z-index:10;
}
#footer {
	height: 10px !important;
	margin-bottom: -22px !important;
}
#footer a{
background-color: #c5c2c0 !important;
}
.nest-pdf-ol-top-subject-header{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	text-align:right;
	text-transform:uppercase;
	text-decoration:underline;
	color:#000000;
	font-size:14px;
	padding-top:15px;
	padding-bottom:10px;
}

.heading-section{
	font-family:MyriadBold;
	padding-bottom:3px;
	border-bottom:2px solid #000000;
	margin-bottom:10px;
	font-size:16px;
}

.nest-offer-letter-title {
	font-size: 16px;
	display: block;
	margin-bottom: 20px;
}

.nest-pdf-sl-top-left {
	margin-top: 14px;
}
.nestdb-header-date {
	margin-top: -3px;
	margin-bottom: 20px;
	display: block;
}
.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-163px;
	text-align:right;
	padding-top:50px;
	padding-bottom: 15px; 
}
.nest-pdf-sl-bold {
	font-family:MyriadBold;
}
.text-justify{
	text-align:justify;
}
</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" style="height: 196px;"/>
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<br><br>
		<div class="nest-pdf-sl-top-left">			
			Attn: {{ $data['fieldcontents']['f2'] }}
			<br />
			{{ $data['fieldcontents']['f3'] }}
            <br /><br />
            
            @if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br /><br />
			@else
				xxxxxx<br /><br />
            @endif
            <br />
			{{ $data['fieldcontents']['f4'] }},
		</div>
		
		<div class="nest-pdf-sl-top-premises">
			{{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			@if (isset($data['replace']['first']) && trim($data['replace']['first']) != '')
				{!! nl2br($data['replace']['first']) !!}
			@else
				It is with great regret that we are not able to take forward your employment with Nest Property Limited.
			@endif
			<br /><br />
			@if (isset($data['replace']['middle']) && trim($data['replace']['middle']) != '')
				{!! nl2br($data['replace']['middle']) !!}
			@else
				Your period of notice commences on {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f6']) }} and contractually stands at {{ $data['fieldcontents']['f7'] }}; your employment with the company will therefore terminate on {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f8']) }}. 
				{{ $data['fieldcontents']['f9'] }}
			@endif
			<br /><br />
			@if (isset($data['replace']['middle2']) && trim($data['replace']['middle2']) != '')
				{!! nl2br($data['replace']['middle2']) !!}
			@else
				Your salary up to {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f8']) }} will be paid directly into your bank account on the dates set out below along with your accumulated holiday up to the last day of your notice period.
			@endif
			<br /><br />
			@if (isset($data['replace']['middle3']) && trim($data['replace']['middle3']) != '')
				{!! nl2br($data['replace']['middle3']) !!}
			@else
				The details of your final remuneration are as follows:
			@endif
			<table width="100%">
			@if (trim($data['fieldcontents']['f10']) != '' && trim($data['fieldcontents']['f11']) != '')
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="40%">{{ $data['fieldcontents']['f10'] }}</td>
					<td width="55%">{{ $data['fieldcontents']['f11'] }}</td>
				</tr>
			@endif
			@if (trim($data['fieldcontents']['f12']) != '' && trim($data['fieldcontents']['f13']) != '')
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="40%">{{ $data['fieldcontents']['f12'] }}</td>
					<td width="55%">{{ $data['fieldcontents']['f13'] }}</td>
				</tr>
			@endif
			@if (trim($data['fieldcontents']['f14']) != '' && trim($data['fieldcontents']['f15']) != '')
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="40%">{{ $data['fieldcontents']['f14'] }}</td>
					<td width="55%">{{ $data['fieldcontents']['f15'] }}</td>
				</tr>
			@endif
			</table><br />
			@if (isset($data['replace']['last']) && trim($data['replace']['last']) != '')
				{!! nl2br($data['replace']['last']) !!}
			@else
				Finally, I would like to thank you for your efforts whilst with the company and wish you all the best in the future.
			@endif
			<br /><br />
			Yours sincerely,
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
				</tr>
				<tr>
					<td>
					{{ $data['fieldcontents']['f17'] }}<br />
					{{ $data['fieldcontents']['f18'] }}<br />
					Nest Property Limited
					</td>
					<td></td>
					<td valign="top">
					Signed & Confirmed:<br />
					{{ $data['fieldcontents']['f16'] }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
