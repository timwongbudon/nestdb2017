<html>
<head>

<title>Letter of Dismissal</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-sl-top-left{
	padding-top:180px;
	padding-bottom:0px;
}
.nest-pdf-sl-bold{
	font-family:MyriadBold;
}
.nest-pdf-sl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-sl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-sl-content{
	text-align:justify;
}
.nest-pdf-sl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-100px;
	text-align:right;
	padding-top:60px;
}
.nest-hide-first-header img{
	width:140px;
	padding-right:20px;
}
.nest-pdf-sl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-sl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-sl-faithfully{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-1{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-lfl-faithfully{
	
}
.nest-pdf-lfl-signature{
	padding:20px 0px 100px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
				<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		@else
			<div class="nest-pdf-letterhead" style="width:152px;">
				<br /><br /><br /><br /><br /><br /><br /><br />
		@endif
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-sl-top-left">
			<br /><br /><br /><br /><br /><br />
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			Attn: {{ $data['fieldcontents']['f2'] }}
			<br />
			{{ $data['fieldcontents']['f3'] }}
			<br /><br />
			{{ $data['fieldcontents']['f4'] }}
		</div>
		<div class="nest-pdf-sl-top-premises">
			{{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			@if (isset($data['replace']['first']) && trim($data['replace']['first']) != '')
				{!! nl2br($data['replace']['first']) !!}
			@else
				It is with great regret that we are not able to take forward your employment with Nest Property Limited.
			@endif
			<br /><br />
			@if (isset($data['replace']['middle']) && trim($data['replace']['middle']) != '')
				{!! nl2br($data['replace']['middle']) !!}
			@else
				Your period of notice commences on {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f6']) }} and contractually stands at {{ $data['fieldcontents']['f7'] }}; your employment with the company will therefore terminate on {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f8']) }}. 
				{{ $data['fieldcontents']['f9'] }}
			@endif
			<br /><br />
			@if (isset($data['replace']['middle2']) && trim($data['replace']['middle2']) != '')
				{!! nl2br($data['replace']['middle2']) !!}
			@else
				Your salary up to {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f8']) }} will be paid directly into your bank account on the dates set out below along with your accumulated holiday up to the last day of your notice period.
			@endif
			<br /><br />
			@if (isset($data['replace']['middle3']) && trim($data['replace']['middle3']) != '')
				{!! nl2br($data['replace']['middle3']) !!}
			@else
				The details of your final remuneration are as follows:
			@endif
			<table width="100%">
			@if (trim($data['fieldcontents']['f10']) != '' && trim($data['fieldcontents']['f11']) != '')
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="40%">{{ $data['fieldcontents']['f10'] }}</td>
					<td width="55%">{{ $data['fieldcontents']['f11'] }}</td>
				</tr>
			@endif
			@if (trim($data['fieldcontents']['f12']) != '' && trim($data['fieldcontents']['f13']) != '')
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="40%">{{ $data['fieldcontents']['f12'] }}</td>
					<td width="55%">{{ $data['fieldcontents']['f13'] }}</td>
				</tr>
			@endif
			@if (trim($data['fieldcontents']['f14']) != '' && trim($data['fieldcontents']['f15']) != '')
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="40%">{{ $data['fieldcontents']['f14'] }}</td>
					<td width="55%">{{ $data['fieldcontents']['f15'] }}</td>
				</tr>
			@endif
			</table><br />
			@if (isset($data['replace']['last']) && trim($data['replace']['last']) != '')
				{!! nl2br($data['replace']['last']) !!}
			@else
				Finally, I would like to thank you for your efforts whilst with the company and wish you all the best in the future.
			@endif
			<br /><br />
			Yours sincerely,
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
				</tr>
				<tr>
					<td>
					{{ $data['fieldcontents']['f17'] }}<br />
					{{ $data['fieldcontents']['f18'] }}<br />
					Nest Property Limited
					</td>
					<td></td>
					<td valign="top">
					Signed & Confirmed:<br />
					{{ $data['fieldcontents']['f16'] }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>

















