@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('basedocuments/letterofemployment') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Offer of Employment Fields</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							{{ Form::hidden('id', $data['id']) }}
							{{ Form::hidden('docversion', $data['docversion']) }}
							
							@if ($data['id'] == 0)
								<input type="hidden" name="consultantid" value="{{ Auth::user()->id }}" />
							@else
								<input type="hidden" name="consultantid" value="{{ $data['consultantid'] }}" />
							@endif
							
							@foreach ($fields as $field)
								@if (count($field) >= 3)
									@if ($field[1] == 'date')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<input type="date" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'hidden')
										{{ Form::hidden('f'.$field[0], old('f'.$field[0], $data['fieldcontents']['f'.$field[0]])) }}
									@elseif ($field[1] == 'text')
										<div class="nest-property-edit-row nest-property-custom-row-field-{{ $field[0] }}">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												@if (isset($field[3]))
													<input type="text" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}" placeholder="{{ $field[3] }}">
												@else
													<input type="text" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
												@endif
											</div>
											<div class="clearfix"></div>
										</div>	
									@elseif ($field[1] == 'number')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<input type="number" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>		
									@elseif ($field[1] == 'radio')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											@php
												$rbs = explode('#', $field[3]);
											@endphp
											<div class="col-xs-8">
												@foreach ($rbs as $index => $rb)
													@if (isset($data['fieldcontents']['f'.$field[0]]))
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, $data['fieldcontents']['f'.$field[0]] == $rb, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@else
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, false, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@endif
												@endforeach
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'textarea')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<textarea name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" rows="12">{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}</textarea>
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'textareadrag')	
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												
												
												<ul class="o-sortable" id="fdrag{{ $field[0] }}">
													<li class="" style="position: relative; z-index: 10">Item 1</li>
												</ul>
												
												<input type="text" id="fadd{{ $field[0] }}" class="form-control" value="">
												<a href="javascript:;" class="nest-button nest-right-button btn btn-default" style="margin-top:5px;" onClick="addCondition({{ $field[0] }})">
													<i class="fa fa-btn fa-plus"></i>Add Condition
												</a>
												<textarea name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" style="height:5px;opacity:0;visibility:hidden;">{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}</textarea>
												<div class="clearfix"></div>
												
												<script>
													jQuery( document ).ready(function() {
														loadConditions({{ $field[0] }});
														sortable('#fdrag{{ $field[0] }}', {
															forcePlaceholderSize: true,
															placeholderClass: 'ph-class',
															hoverClass: 'bg-maroon yellow'
														});
														sortable('#fdrag{{ $field[0] }}')[0].addEventListener('sortstop', function(e) {
															storeConditions({{ $field[0] }});
														});
														
														jQuery('#fdrag{{ $field[0] }} a.clearitem').on('click', function(){
															jQuery(this).parent().remove();
															sortable('#fdrag{{ $field[0] }}');
															storeConditions({{ $field[0] }});
														});
														jQuery('#fdrag{{ $field[0] }} a.edititem').on('click', function(){
															var val = jQuery(this).parent().html();
															val = val.replace(/(<([^>]+)>)/ig,"");
															jQuery('#fadd{{ $field[0] }}').val(val);
															jQuery(this).parent().remove();
															sortable('#fdrag{{ $field[0] }}');
															storeConditions({{ $field[0] }});
														});
														jQuery('#fadd{{ $field[0] }}').keydown(function(event){
															if(event.keyCode == 13) {
																event.preventDefault();
																addCondition({{ $field[0] }});
																jQuery('#fadd{{ $field[0] }}').val('');
																return false;
															}
														});
														
														//Refund
														var val = jQuery('input[name="f27"]:checked').val();
														if (val != 'No'){
															jQuery('#row56').css('display', 'none');
															jQuery('#row57').css('display', 'none');
														}
														jQuery('input[name="f27"]').on('change', function(){
															var val = jQuery('input[name="f27"]:checked').val();
															if (val != 'No'){
																jQuery('#row56').css('display', 'none');
																jQuery('#row57').css('display', 'none');
															}else{
																jQuery('#row56').css('display', 'block');
																jQuery('#row57').css('display', 'block');
															}
														});
														
													});
												</script>
												
												
												@if ($field[0] == 34)
													<div style="padding-top:5px;">
														<p>
															<b>Some Standard Conditions</b>
														</p>
														@foreach ($hides as $field)
															@if (count($field) >= 4)
																@if ($field[0] == 3 && $field[2] == 1)
																	<p>
																		{{ $field[3] }} 
																	</p>
																@endif
															@endif
														@endforeach
													</div>
												@endif
												
												
											</div>
											<div class="clearfix"></div>
										</div>	
										
									@endif
								@endif
							@endforeach
							
						</div>
					</div>
				</div>
				
				<script>
					function loadConditions(id){
						jQuery('#fdrag'+id).empty();
						var v = jQuery('#f'+id).val();
						var e = v.split("\n");
						for(var i = 0; i < e.length; i++){
							var ee = e[i];
							if (ee.trim() != ''){
								jQuery('#fdrag'+id).append('<li class="" style="position: relative; z-index: 10">'+ee.trim()+' <a class="edititem"><i class="fa fa-pencil"></i></a> <a class="clearitem"><i class="fa fa-trash"></i></a></li>');
							}
						}
					}
					function storeConditions(id){
						var content = '';
						jQuery('#fdrag'+id+' li').each(function(i){
						   content = content + jQuery(this).text() + "\n";
						   
						});
						jQuery('#f'+id).val(content);
						jQuery('#pdfbutton').css('display', 'none');									
					}
					
					function addCondition(id){
						var a = jQuery('#fadd'+id).val().trim();
						if (a != ''){
							jQuery('#fdrag'+id).append('<li class="" style="position: relative; z-index: 10">'+a+' <a class="edititem"><i class="fa fa-pencil"></i></a> <a class="clearitem"><i class="fa fa-trash"></i></a></li>');
							sortable('#fdrag'+id);
							storeConditions(id);
							jQuery('#fadd'+id).val('');
							jQuery('#fdrag'+id+' a.clearitem').on('click', function(){
								jQuery(this).parent().remove();
								sortable('#fdrag'+id);
								storeConditions(id);
							});
							jQuery('#fdrag'+id+' a.edititem').on('click', function(){
								var val = jQuery(this).parent().html();
								val = val.replace(/(<([^>]+)>)/ig,"");
								jQuery('#fadd'+id).val(val);
								jQuery(this).parent().remove();
								sortable('#fdrag'+id);
								storeConditions(id);
							});
						}
					}
					
				</script>
				
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Offer of Employment Content</h4>
						</div>

						<div class="panel-body">
							<b>Tick the boxes for the parts you don't want to show up in the contract:</b><br />
							<div class="nest-form-builder-hide-wrapper">
								@foreach ($hides as $field)
									@if (count($field) >= 4)
										@if ($field[1] == 0)
											@if ($field[2] == 1)
												<div class="nest-form-builder-hide-section">
													{{ $field[3] }}
												</div>
												<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
													{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} Hide this section
												</label>
											@else
												<div class="nest-form-builder-hide-section">
													{{ $field[3] }} (tick what to hide)
												</div>
											@endif
										@else
											<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
												{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} {{ $field[3] }}
											</label>
										@endif
									@endif
								@endforeach
								@foreach ($hiddenhides as $hh)
									{{ Form::hidden('hidevals[]', $hh) }}
								@endforeach
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							@foreach ($replaceFields as $key => $name)
								<div class="nest-property-edit-row">
									<div class="nest-property-edit-label">Replace Text for Section "{{ $name }}"
									@if (!isset($data['replace'][$key]) || trim($data['replace'][$key]) == '')
										<a href="javascript:;" onClick="showReplace('{{ $key }}');">show</a>
									@endif
									</div>
									@if (isset($data['replace'][$key]) && trim($data['replace'][$key]) != '')
										<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
									@else
										<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2" style="display:none;">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
									@endif
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-xs-12">
								@if ($data['id'] > 0)
									<a id="pdfbutton" href="{{ url('/basedocuments/letterofemploymentpdf/'.$data['id'].'/no') }}" target="_blank" class="pdfbuttonhide nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download Letterhead PDF </a>
									<a id="pdfbutton" href="{{ url('/basedocuments/letterofemploymentpdf/'.$data['id']) }}" target="_blank" class="pdfbuttonhide nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download PDF </a>
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								@else
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Save </button>
								@endif
								<a href="{{ url('/basedocuments') }}" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-arrow-left"></i>Back to Documents
								</a>
							</div>
						</div>
					</div>
				</div>
				
			</form>
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('.pdfbuttonhide').css('display', 'none');
	});
	jQuery('textarea').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('.pdfbuttonhide').css('display', 'none');
	});
	function showReplace(id){
		jQuery('#replace-'+id).css('display', 'block');
	}
	$( document ).ready(function() {
		
	});
</script>
@endsection
