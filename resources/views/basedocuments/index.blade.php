@extends('layouts.app')

@section('content')

	<!-- Current Properties -->
	<div class="nest-new">
		<div class="row">
			<div class="nest-property-edit-wrapper">	
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>Documents</h4>
							</div>
						</div>
						<div class="panel-body">
							<div class="col-xs-12">
								<a class="nest-button btn btn-primary" href="{{ url('basedocuments/blankletter/0') }}">
									<i class="fa fa-btn fa-plus"></i>New Blank Letter
								</a>
								<a class="nest-button btn btn-primary" href="{{ url('basedocuments/employmentletter/0') }}">
									<i class="fa fa-btn fa-plus"></i>New Employment Letter
								</a>
								<a class="nest-button btn btn-primary" href="{{ url('basedocuments/dismissalletter/0') }}">
									<i class="fa fa-btn fa-plus"></i>New Letter of Dismissal
								</a>
								<a class="nest-button btn btn-primary" href="{{ url('basedocuments/letterofemployment/0') }}">
									<i class="fa fa-btn fa-plus"></i>New Offer of Employment
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		@if (count($docs) > 0)
			<div class="nest-buildings-result-wrapper">
				@foreach ($docs as $index => $doc)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-4 col-md-4 col-sm-6">
							<b>Date:</b> {{ $doc->created_at() }}<br />
							<b>Type:</b> 
							@if ($doc->doctype == 'blank')
								Blank Letter
							@elseif ($doc->doctype == 'employmentletter')
								Employment Letter
							@elseif ($doc->doctype == 'letterofdismissal')
								Letter of Dismissal
							@elseif ($doc->doctype == 'offerofemployment')
								Offer of Employment
							@endif
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
							<b>User:</b> {{ $consultants[$doc->consultantid]->name }}
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							@if ($title = $doc->getFieldValue('f0'))
								<b>Title:</b> {{$title}}
							@endif
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12">
							@if ($doc->doctype == 'blank')
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('basedocuments/blankletter/'.$doc->id) }}">
									<i class="fa fa-btn fa-pencil-square-o"></i>Edit
								</a>
							@elseif ($doc->doctype == 'employmentletter')
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('basedocuments/employmentletter/'.$doc->id) }}">
									<i class="fa fa-btn fa-pencil-square-o"></i>Edit
								</a>
							@elseif ($doc->doctype == 'letterofdismissal')
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('basedocuments/dismissalletter/'.$doc->id) }}">
									<i class="fa fa-btn fa-pencil-square-o"></i>Edit
								</a>
							@elseif ($doc->doctype == 'offerofemployment')
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('basedocuments/letterofemployment/'.$doc->id) }}">
									<i class="fa fa-btn fa-pencil-square-o"></i>Edit
								</a>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
			</div>
		@endif
		@if ($docs->lastPage() > 1)
			<div class="panel-footer nest-buildings-pagination">
				{{ $docs->links() }}
			</div>
		@endif
	</div>

@endsection