@extends('layouts.app')

@section('content')


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker(
		{ dateFormat: 'yy-mm-dd' }
	);
  } );


  $(document).ready(function(){
	  $('.filter-date').click(function(){
			var from = $('.datepicker.from').val();
			var to = $('.datepicker.to').val();

			window.location = "{{ url('companyoverview') }}?from="+from+"&to="+to;
	  });
	  $('.bfilter-date').click(function(){
			var from = $('.datepicker.bfrom').val();
			var to = $('.datepicker.bto').val();

			window.location = "{{ url('companyoverview') }}?from="+from+"&to="+to;
	  });
  });
  </script>

<div id="company-overview">
	<div style="text-align: center;"> <img style="width: 35px"src="/ZKZg.gif"> <br>Loading.. </div>
</div>


<script>
	$('document').ready(function(){
		var year = '<?php echo $year; ?>';
		$.ajax(
				{
				type: 'get',
				url: '/companyoverview_ajx/'+year,
				data: '',
				success: function (result) {
					console.log(result);
					$('#company-overview').html(result);
				
				//	alert(result);

				},
				error: function (xhr, status, error) {
					alert("Something went wrong");
					console.log(status)
					console.log(xhr)
					console.log(error)
				}
			});

	});
	</script>
@endsection
