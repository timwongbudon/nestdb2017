@extends('layouts.app')

@section('content')


<div class="nest-new container">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Client Information</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>ID</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->id }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>First Name</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->firstname }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Last Name</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->lastname }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Client Type</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->getClienttype() }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Tel</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{!! $client->telLinked() !!}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Email</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->email }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Workplace</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->workplace }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Title</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->title }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Address 1</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->address1 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Address 2</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->address2 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Address 3</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->address3 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>HKID / Passport No.</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->hkidpassport }}
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>First Name</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->firstname2 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Last Name</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->lastname2 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Tel</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{!! $client->tel2Linked() !!}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Email</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->email2 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Workplace</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->workplace2 }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Title</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->title2 }}
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Budget (Min)</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->budget_min }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Budget (Max)</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->budget_max }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Tempature</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $client->tempature }}
							</div>
						</div>
				
					</div>
				</div>
            </div>
			
			
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Pictures</h4>
					</div>
					<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
						<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
							<div class="nest-propety-docs-uploaded col-xs-12">
								@if (count($docuploads) > 0)
									@foreach ($docuploads as $d)
										@if ($d->doctype == 'picture')
											<a href="{{ url('/client/showdoc/'.$d->id.'') }}" target="_blank">
												<img src="{{ url('/client/showpic/'.$d->id.'') }}" height="100px" />
											</a>
										@endif
									@endforeach
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
						
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Documents</h4>
					</div>
					<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
						<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
							<div class="nest-propety-docs-uploaded col-xs-12">
								@if (count($docuploads) > 0)
									@foreach ($docuploads as $d)
										@if ($d->doctype == 'businesscard')
											Business Card
										@elseif ($d->doctype == 'idcard')
											ID Card
										@elseif ($d->doctype == 'passport')
											Passport
										@elseif ($d->doctype == 'picture')
											Picture
										@elseif ($d->doctype == 'employmentcontract')
											Employment Contract
										@elseif ($d->doctype == 'salaryslip')
											Salary Slip
										@elseif ($d->doctype == 'other')
											Other Document
										@endif
										from 
										{{ $d->created_at() }}
										@if (isset($consultants[$d->consultantid]))
											by {{ $consultants[$d->consultantid]->name }}
										@endif
										<a href="{{ url('/client/showdoc/'.$d->id.'') }}" target="_blank">download</a><br />
									@endforeach
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
						
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-comments" id="existingCommentsClient"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
			</div>
		</div>
    </div>
	
    <div class="row">
		<div class="nest-property-edit-wrapper">	
			<div class="col-sm-12">
				<div class="panel panel-default" style="margin-bottom:0px;">
					<div class="panel-heading">
					   <h4>Enquiries</h4>
					</div>
				</div>
				<div class="nest-buildings-result-wrapper">
					@foreach ($leads as $index => $lead)
						<div class="row 
						@if ($index%2 == 1)
							nest-striped
						@endif
						" style="border-left:0px;border-right:0px;">
							<div class="col-lg-4 col-md-4 col-sm-3">
								<b>ID:</b> {{ $lead->id }}<br />
								<b>Creation date:</b> {{ $lead->created_at() }}
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
								<b>Budget:</b> {{ $lead->budgetOut() }}<br />
								<b>Status:</b> {{ $lead->statusOut() }}
							</div>
							<div class="col-lg-5 col-md-5 col-sm-6">
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('lead/edit/'.$lead->id) }}">
									<i class="fa fa-btn fa-pencil-square-o"></i> View
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endforeach
				</div>

				<div class="panel panel-default">
					<div class="panel-body" style="padding-right:7.5px;">
						<div class="col-sm-12">
							<a class="nest-button nest-right-button btn btn-default" href="{{ url('lead/newlead/'.$client->id) }}">
								<i class="fa fa-btn fa-plus"></i> Create New Enquiry
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="row">
		<div class="nest-property-edit-wrapper">	
			<div class="col-sm-12">
				<div class="panel panel-default" style="margin-bottom:0px;">
					<div class="panel-heading">
					   <h4>Shortlists</h4>
					</div>
				</div>
				<div class="nest-buildings-result-wrapper">
					@php
						$openshortlist = false;
					@endphp
					@foreach ($shortlists as $index => $shortlist)
						<div class="row 
						@if ($index%2 == 1)
							nest-striped
						@endif
						" style="border-left:0px;border-right:0px;">
							<div class="col-lg-4 col-md-4 col-sm-3">
								<b>ID:</b> {{ $shortlist->id }}<br />
								<b>Creation date:</b> {{ $shortlist->created_at() }}
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
								<b>Count:</b> {{ $shortlist->options()->count() }}<br />
								<b>Status:</b> 
								@if ($shortlist->status == 0)
									Open
								@elseif ($shortlist->status == 10)
									Closed
								@endif
							</div>
							<div class="col-lg-5 col-md-5 col-sm-6">
								@if ($shortlist->status == 0)
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id.'?statuschange=close') }}">
										<i class="fa fa-btn fa-lock"></i> Close
									</a>
									@php
										$openshortlist = true;
									@endphp
								@elseif ($shortlist->status == 10)
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id.'?statuschange=open') }}">
										<i class="fa fa-btn fa-unlock"></i> Reopen
									</a>
								@endif
								
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/manage/'.$shortlist->id) }}">
									<i class="fa fa-btn fa-plus"></i> Properties
								</a>
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id) }}">
									<i class="fa fa-btn fa-pencil-square-o"></i> View
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endforeach
				</div>

				<div class="panel panel-default">
					<div class="panel-body" style="padding-right:7.5px;">
						<div class="col-sm-12">
							@if ($openshortlist)
								<div class="text-right">Please close all existing shortlists for this client before starting a new one</div>
							@else
								<a class="nest-button nest-right-button btn btn-default" href="{{ url('client/'.$client->id.'/store-shortlist') }}">
									<i class="fa fa-btn fa-plus"></i>Add shortlist
								</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<script>			
	function loadCommentsClient(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/client/comments/".$client->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsClient').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsClient').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/client/comments/".$client->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsClient').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/client/comments/".$client->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsClient').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsClient').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	jQuery( document ).ready(function(){
		loadCommentsClient();
	});
</script>
	
@endsection

@section('footer-script')
@endsection