@extends('layouts.app')

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('client/store') }}" method="POST" id="addClient" class="form-horizontal" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>New Client</h4>
						</div>
						<div class="panel-body">
							@include('common.errors')
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="firstname" id="client-firstname" class="form-control" value="{{ old('firstname') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="lastname" id="client-lastname" class="form-control" value="{{ old('lastname') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Client Type</div>
								</div>
								<div class="col-xs-8">
									<select name="clienttype">
										<option value="0">Normal Client</option>
										<option value="1">Co-op</option>
										<option value="2">Referral</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel" id="client-tel" class="form-control" value="{{ old('tel') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="email" id="client-email" class="form-control" value="{{ old('email') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Workplace</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="workplace" id="client-workplace" class="form-control" value="{{ old('workplace') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Title</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="title" id="client-title" class="form-control" value="{{ old('title') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address 1</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address1" id="client-address1" class="form-control" value="{{ old('address1') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address 2</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address2" id="client-address2" class="form-control" value="{{ old('address2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address 3</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address3" id="client-address3" class="form-control" value="{{ old('address3') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">HKID / Passport No.</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="hkidpassport" id="client-hkidpassport" class="form-control" value="{{ old('hkidpassport') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Ultra High Net Worth</div>
								</div>
								<div class="col-xs-8">
									<select name="uhnw">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Second Contact</h4>
						</div>
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="firstname2" id="client-firstname2" class="form-control" value="{{ old('firstname2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="lastname2" id="client-lastname2" class="form-control" value="{{ old('lastname2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel2" id="client-tel2" class="form-control" value="{{ old('tel2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="email2" id="client-email2" class="form-control" value="{{ old('email2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Workplace</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="workplace2" id="client-workplace2" class="form-control" value="{{ old('workplace2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Title</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="title2" id="client-title2" class="form-control" value="{{ old('title2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">


							<button type="submit" id="create-client-btn" value="Submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i> Submit</button>

							<div id="confirm-create-client-btn" style="display: none; ">
							<div id="similar-clients" class="alert alert-danger" ></div>
							<div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" value="Confirm"><i class="fa fa-btn fa-plus"></i>Confirm </button>
							</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});

	$('#client-firstname, #client-lastname, #client-email').keyup(function(){
		$('#create-client-btn').show();
		$('#confirm-create-client-btn').hide();
	});


	jQuery('form').on('submit',  function (e) {
				

		if( $(document.activeElement).val() == "Submit"){
			e.preventDefault();
			var clientfirstname = $('#client-firstname').val();
			var clientlastname = $('#client-lastname').val();
			var clientemail = $('#client-email').val();
			

			$.ajax(
				{
				type: 'get',
				url: '/client/check_client?clientfirstname='+clientfirstname+'&clientlastname='+clientlastname+'&clientemail='+clientemail,
				data: '',
				success: function (result) {
					console.log(result);
					var r = JSON.parse(result);
					if(r.count == 0){						
						e.currentTarget.submit();
					}else{
						$('#create-client-btn').hide();
						$('#confirm-create-client-btn').show();
						$('#similar-clients').html(r.results);
					}
				
				//	alert(result);

				},
				error: function (xhr, status, error) {
					alert("Something went wrong");
					console.log(status)
					console.log(xhr)
					console.log(error)
				}
			});

		}



	});

	
</script>

@endsection

@section('footer-script')
@endsection