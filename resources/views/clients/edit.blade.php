@extends('layouts.app')

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('client/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Edit Client (ID {{ $client->id }})</h4>
						</div>
						<div class="panel-body">
							@include('common.errors')
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="firstname" id="client-firstname" class="form-control" value="{{ old('firstname', $client->firstname) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="lastname" id="client-lastname" class="form-control" value="{{ old('lastname', $client->lastname) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Client Type</div>
								</div>
								<div class="col-xs-8">
									<select name="clienttype">
										<option value="0" 
										@if ($client->clienttype == 0)
											selected
										@endif
										>Normal Client</option>
										<option value="1" 
										@if ($client->clienttype == 1)
											selected
										@endif
										>Co-op</option>
										<option value="2" 
										@if ($client->clienttype == 2)
											selected
										@endif
										>Referral</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel" id="client-tel" class="form-control" value="{{ old('tel', $client->tel) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="hidden" name="emailold" value="{{ $client->email }}">
									<input type="text" name="email" id="client-email" class="form-control" value="{{ old('email', $client->email) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Workplace</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="workplace" id="client-workplace" class="form-control" value="{{ old('workplace', $client->workplace) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Title</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="title" id="client-title" class="form-control" value="{{ old('title', $client->title) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address 1</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address1" id="client-address1" class="form-control" value="{{ old('address1', $client->address1) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address 2</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address2" id="client-address2" class="form-control" value="{{ old('address2', $client->address2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address 3</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address3" id="client-address3" class="form-control" value="{{ old('address3', $client->address3) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">HKID / Passport No.</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="hkidpassport" id="client-hkidpassport" class="form-control" value="{{ old('hkidpassport', $client->hkidpassport) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Ultra High Net Worth</div>
								</div>
								<div class="col-xs-8">
									<select name="uhnw">
										<option value="0" 
										@if ($client->uhnw == 0)
											selected
										@endif
										>No</option>
										<option value="1" 
										@if ($client->uhnw == 1)
											selected
										@endif
										>Yes</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Second Contact</h4>
						</div>
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="firstname2" id="client-firstname2" class="form-control" value="{{ old('firstname2', $client->firstname2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="lastname2" id="client-lastname2" class="form-control" value="{{ old('lastname2', $client->lastname2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel2" id="client-tel2" class="form-control" value="{{ old('tel2', $client->tel2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="hidden" name="email2old" value="{{ $client->email2 }}">
									<input type="text" name="email2" id="client-email2" class="form-control" value="{{ old('email2', $client->email2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Workplace</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="workplace2" id="client-workplace2" class="form-control" value="{{ old('workplace2', $client->workplace2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Title</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="title2" id="client-title2" class="form-control" value="{{ old('title2', $client->title2) }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							{{ Form::hidden('previous_url', $previous_url) }}
							{{ Form::hidden('id', $client->id) }}
							
							<button type="submit" class="nest-button nest-right-button btn btn-default" id="update-client-btn" value="Update"><i class="fa fa-btn fa-pencil"></i>Update </button>

							<div id="confirm-create-client-btn" style="display: none; ">
							<div id="similar-clients" class="alert alert-danger" ></div>
							<div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" value="Confirm"><i class="fa fa-btn fa-plus"></i>Confirm </button>
							</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Document/Picture Upload</h4>
					</div>
					<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
						<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
							<div class="nest-propety-docs-uploaded col-xs-12">
								@if (count($docuploads) > 0)
									@foreach ($docuploads as $d)
										@if ($d->doctype == 'businesscard')
											Business Card
										@elseif ($d->doctype == 'idcard')
											ID Card
										@elseif ($d->doctype == 'passport')
											Passport
										@elseif ($d->doctype == 'picture')
											Picture
										@elseif ($d->doctype == 'employmentcontract')
											Employment Contract
										@elseif ($d->doctype == 'salaryslip')
											Salary Slip
										@elseif ($d->doctype == 'other')
											Other Document
										@endif
										from 
										{{ $d->created_at() }}
										@if (isset($consultants[$d->consultantid]))
											by {{ $consultants[$d->consultantid]->name }}
										@endif
										<a href="{{ url('/client/showdoc/'.$d->id.'') }}" target="_blank">download</a> 
										@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
											<a href="{{ url('/client/removedoc/'.$d->id.'/'.$d->clientid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
										@endif
										<br />
									@endforeach
								@endif
							</div>
							<div class="clearfix"></div>
							<div class="nest-propety-prop-docs-upload" style="padding-top:12px;">
								<form action="{{ url('/client/docupload/'.$client->id.'') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="col-xs-6">
										<select name="doctype" class="form-control" style="font-weight:normal;">
											<option value="businesscard">Business Card</option>
											<option value="idcard">ID Card</option>
											<option value="passport">Passport</option>
											<option value="picture">Picture</option>
											<option value="employmentcontract">Employment Contract</option>
											<option value="salaryslip">Salary Slip</option>
											<option value="other">Other</option>
										</select>
									</div>
									<div class="col-xs-6">
										<button class="nest-button nest-right-button btn btn-default" type="submit">Upload Document</button>
									</div>
									<div class="clearfix"></div>
									<div class="draganddropup col-xs-12">
										{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
									</div>
								</form>
							</div>
							<div class="clearfix"></div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcommentclient" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addCommentClient();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsClient"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>			
	function loadCommentsClient(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/client/comments/".$client->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsClient').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsClient').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/client/comments/".$client->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsClient').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/client/comments/".$client->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsClient').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsClient').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function addCommentClient(){
		var comment = jQuery('#newcommentclient').val().trim();
		if (comment.length > 0){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/client/commentadd/".$client->id) }}',
				data: {comment: comment},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					jQuery('#newcommentclient').val('');
					loadCommentsClient();
				},
				error: function (data) {
					console.log(data);
					alert('Something went wrong, please try again!');
				}
			});
		}else{
			alert('Please enter a comment');
		}
	}
	
	$('#newcommentclient').keydown(function( event ) {
		if ( event.keyCode == 13 ) {
			event.preventDefault();
			addCommentClient();
			loadCommentsClient();
		}				
	});
	
	function removeCommentBuilding(t){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'POST',
			url: '{{ url("/client/commentremove/".$client->id) }}',
			data: {t: t},
			dataType: 'text',
			cache: false,
			async: false,
			success: function (data) {
				loadCommentsClient();
			},
			error: function (data) {
				alert('Something went wrong, please try again!');
			}
		});
	}
	
	jQuery( document ).ready(function(){
		loadCommentsClient();
	});
	
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});


	$('#client-firstname, #client-lastname, #client-email').keyup(function(){
		$('#update-client-btn').show();
		$('#confirm-create-client-btn').hide();
	});

	jQuery('form').on('submit',  function (e) {
		

				if( $(document.activeElement).val() == "Update"){
					e.preventDefault();
					var clientfirstname = $('#client-firstname').val();
					var clientlastname = $('#client-lastname').val();
					var clientemail = $('#client-email').val();
					
		
					$.ajax(
						{
						type: 'get',
						url: '/client/check_client?clientfirstname='+clientfirstname+'&clientlastname='+clientlastname+'&clientemail='+clientemail+'&id={{$client->id}}',
						data: '',
						success: function (result) {
							console.log(result);
							var r = JSON.parse(result);
							if(r.count == 0){						
								e.currentTarget.submit();
							}else{
								$('#update-client-btn').hide();
								$('#confirm-create-client-btn').show();
								$('#similar-clients').html(r.results);
							}
						
						//	alert(result);
		
						},
						error: function (xhr, status, error) {
							alert("Something went wrong");
							console.log(status)
							console.log(xhr)
							console.log(error)
						}
					});
		
				}
		
		
		
			});
</script>


@endsection

@section('footer-script')
@endsection