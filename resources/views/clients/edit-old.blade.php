@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Client
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- Edit Property Form -->
                    <form action="{{ url('client/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="client-firstname" class="col-sm-3 control-label">ID</label>
                            <div class="col-sm-6">
                                 <span>{{ $client->id }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-firstname" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="firstname" id="client-firstname" class="form-control" value="{{ old('firstname', $client->firstname) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-lastname" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="lastname" id="client-lastname" class="form-control" value="{{ old('lastname', $client->lastname) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-tel" class="col-sm-3 control-label">Tel</label>
                            <div class="col-sm-6">
                                <input type="number" name="tel" id="client-tel" class="form-control" value="{{ old('tel', $client->tel) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="text" name="email" id="client-email" class="form-control" value="{{ old('email', $client->email) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-workplace" class="col-sm-3 control-label">Workplace</label>
                            <div class="col-sm-6">
                                <input type="text" name="workplace" id="client-workplace" class="form-control" value="{{ old('workplace', $client->workplace) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-title" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="title" id="client-title" class="form-control" value="{{ old('title', $client->title) }}">
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Second Contact</label>
                        </div>

                        <div class="form-group">
                            <label for="client-firstname2" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="firstname2" id="client-firstname2" class="form-control" value="{{ old('firstname2', $client->firstname2) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-lastname2" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="lastname2" id="client-lastname2" class="form-control" value="{{ old('lastname2', $client->lastname2) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-tel2" class="col-sm-3 control-label">Tel</label>
                            <div class="col-sm-6">
                                <input type="number" name="tel2" id="client-tel2" class="form-control" value="{{ old('tel2', $client->tel2) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-email2" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="text" name="email2" id="client-email2" class="form-control" value="{{ old('email2', $client->email2) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-workplace2" class="col-sm-3 control-label">Workplace</label>
                            <div class="col-sm-6">
                                <input type="text" name="workplace2" id="client-workplace2" class="form-control" value="{{ old('workplace2', $client->workplace2) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-title2" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="title2" id="client-title2" class="form-control" value="{{ old('title2', $client->title2) }}">
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="client-budget_min" class="col-sm-3 control-label">Budget (Min)</label>
                            <div class="col-sm-6">
                                <input type="number" name="budget_min" id="client-budget_min" class="form-control" value="{{ old('budget_min', $client->budget_min) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-budget_max" class="col-sm-3 control-label">Budget (Max)</label>
                            <div class="col-sm-6">
                                <input type="number" name="budget_max" id="client-budget_max" class="form-control" value="{{ old('budget_max', $client->budget_max) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-tempature" class="col-sm-3 control-label">Tempature</label>
                            <div class="col-sm-6">
                                 <label for="client-tempature-1" class="control-label">{!! Form::radio('tempature', 'Cold', $client->is_selected_tempature('Cold'), ['id'=>'client-tempature-1']) !!} Cold</label>
                                 <label for="client-tempature-2" class="control-label">{!! Form::radio('tempature', 'Warm', $client->is_selected_tempature('Warm'), ['id'=>'client-tempature-2']) !!} Warm</label>
                                 <label for="client-tempature-3" class="control-label">{!! Form::radio('tempature', 'Hot', $client->is_selected_tempature('Hot'), ['id'=>'client-tempature-3']) !!} Hot</label>
                            </div>
                        </div>

                        <hr/>
    
                        <!-- Add Update Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                {{ Form::hidden('previous_url', $previous_url) }}
                                {{ Form::hidden('id', $client->id) }}
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-pencil"></i>Update </button>
                                <!-- <button type="button" class="btn btn-default">
                                    <i class="fa fa-btn fa-ban"></i>Cancel </button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection

@section('footer-script')
@endsection