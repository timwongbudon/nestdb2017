	@extends('layouts.app')

@section('content')

            <!-- Client Search -->
            <div class="nest-buildings-search-panel panel panel-default">
                <div class="panel-body">
                    <!-- New Property Form -->
                    <form action="{{ url('clients') }}" method="GET" class="form-horizontal">
                        <div class="nest-buildings-search-wrapper row">
                            <div class="col-sm-4">
                                <input type="text" name="sstring" placeholder="First name, last name, workplace, email, etc." class="form-control" value="{{ old('name', $input->name) }}">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="tel" placeholder="Phone number" id="client-tel" class="form-control" value="{{ old('tel', $input->tel) }}">
                            </div> 
                            <div class="col-sm-4">
                                <input type="text" name="id" placeholder="Client ID" id="client-id" class="form-control" value="{{ old('id', $input->id) }}">
                            </div>
							<div class="clearfix"></div>
                        </div>
                        <div class="nest-buildings-search-wrapper row">
                            <div class="col-sm-4">
                                <input type="number" name="budget_min" placeholder="Budget (Min)" id="client-budget_min" class="form-control" value="{{ old('budget_min', $input->budget_min) }}">
                            </div>
                            <div class="col-sm-4">
                                <input type="number" name="budget_max" placeholder="Budget (Max)" id="client-budget_max" class="form-control" value="{{ old('budget_max', $input->budget_max) }}">
                            </div>
                            @if(Auth::user()->is_management)
                            <div class="col-sm-4">
                                <select id="consultant" name="user_id" class="form-control">
                                    <option value="">All Consultants</option>
                                    @foreach ($consultants as $c)
                                        <option {{(isset($input->user_id)  && $input->user_id == $c->id) ? 'selected': ''}} value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
							<input type="hidden" name="tempature" value="" />
							<!--
                            <div class="col-sm-4">
                                <select class="form-control" name="tempature">
									<option value="">Choose a temperature</option>
									<option value="Cold">Cold</option>
									<option value="Warm">Warm</option>
									<option value="Hot">Hot</option>
								</select>
                            </div>
							-->
							<div class="clearfix"></div>
                        </div>
                        @if(Auth::user()->is_management)
						<div class="nest-buildings-search-wrapper row">
                            <div class="col-sm-4">
                                <select id="sort" name="sort" class="form-control">
                                    <option value="">Order by ID (High to Low)</option>
                                    <option {{(isset($input->sort)  && $input->sort == 'id_asc') ? 'selected': ''}} value="id_asc">Order by ID (Low to High)</option>
                                    <option {{(isset($input->sort)  && $input->sort == 'consultant_asc') ? 'selected': ''}} value="consultant_asc">Order by Consultant's Name (A-Z)</option>
                                    <option {{(isset($input->sort)  && $input->sort == 'consultant_desc') ? 'selected': ''}} value="consultant_desc">Order by Consultant's Name (Z-A)</option>
                                </select>
                            </div>
                        </div>
                        @endif
                        <!-- <div class="form-group">
                            <div class="col-sm-4">
                                <label for="client-firstname" class="control-label">First Name</label>
                                <input type="text" name="firstname" id="client-firstname" class="form-control" value="{{ old('firstname', $input->firstname) }}" >
                            </div>
                            <div class="col-sm-4">
                                <label for="client-lastname" class="control-label">Last Name</label>
                                <input type="text" name="lastname" id="client-lastname" class="form-control" value="{{ old('lastname', $input->lastname) }}" >
                            </div>
                            <div class="col-sm-4">
                                <label for="client-tel" class="control-label">Tel</label>
                                <input type="number" name="tel" id="client-tel" class="form-control" value="{{ old('tel', $input->tel) }}">
                            </div> 
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="client-email" class="control-label">Email</label>
                                <input type="text" name="email" id="client-email" class="form-control" value="{{ old('email', $input->email) }}">  
                            </div>
                            <div class="col-sm-4">
                                <label for="client-workplace" class="control-label">Workplace</label>
                                <input type="text" name="workplace" id="client-workplace" class="form-control" value="{{ old('workplace', $input->workplace) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="client-title" class="control-label">Title</label>
                                <input type="text" name="title" id="client-title" class="form-control" value="{{ old('title', $input->title) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="client-budget_min" class="control-label">Budget (Min)</label>
                                <input type="number" name="budget_min" id="client-budget_min" class="form-control" value="{{ old('budget_min', $input->budget_min) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="client-budget_max" class="control-label">Budget (Max)</label>
                                <input type="number" name="budget_max" id="client-budget_max" class="form-control" value="{{ old('budget_max', $input->budget_max) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="client-id" class="control-label">ID</label>
                                <input type="text" name="id" id="client-id" class="form-control" value="{{ old('id', $input->id) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="client-tempature" class="control-label">Tempature</label>
                                <div style="display: block;">
                                    <label for="client-tempature-0" class="control-label">{!! Form::radio('tempature', '', $input->tempature=='', ['id'=>'client-tempature-0']) !!} Any</label>
                                    <label for="client-tempature-1" class="control-label">{!! Form::radio('tempature', 'Cold', $input->tempature=='Cold', ['id'=>'client-tempature-1']) !!} Cold</label>
                                    <label for="client-tempature-2" class="control-label">{!! Form::radio('tempature', 'Warm', $input->tempature=='Warm', ['id'=>'client-tempature-2']) !!} Warm</label>
                                    <label for="client-tempature-3" class="control-label">{!! Form::radio('tempature', 'Hot', $input->tempature=='Hot', ['id'=>'client-tempature-3']) !!} Hot</label>
                                </div>
                            </div>
                        </div>-->
						
						<div class="nest-buildings-search-buttons-wrapper row">
							<div class="col-xs-6">
								<a class="nest-button btn btn-primary" href="{{ url('client/create') }}">
									<i class="fa fa-btn fa-plus"></i>Add Client
								</a>
							</div>
                            <div class="col-xs-6">
                                <button type="submit" name="action" value="search" class="nest-button nest-right-button btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search </button>
                            </div>
						</div>
                    </form>
					<div style="padding-top:15px;text-align:right;">
						@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
							To search for "Ultra High Net Worth" clients, please type "uhnw" into the "Client ID" field and click "Search"
						@endif
					</div>
                </div>
            </div>

            <!-- Current Clients -->
            @if (count($clients) > 0)
				<div>
					<div class="nest-buildings-result-wrapper">
						@foreach ($clients as $index => $client)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Client:</b> <a href="{{ url('client/show/'.$client->id) }}">{{ $client->name() }} 
										@if( !empty($client->description()))
											(<i>{{ $client->description() }}</i>) 
										@endif
										@if( !empty($client->name2()) )
											/ {{ $client->name2() }} 
										@endif
										@if( !empty($client->description2()))
											(<i>{{ $client->description2() }}</i>)
										@endif</a><br />
									<b>ID:</b> {{ $client->id }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Tel:</b> {!! $client->telLinked() !!} 
										@if( !empty($client->tel2) )
											/ {!! $client->tel2Linked() !!}
										@endif<br />
									<b>Email:</b> {{ $client->email() }} 
										@if( !empty($client->email2) )
											/ {{ $client->email2() }}
										@endif<br />
									<b>Budget:</b> {{ $client->budget() }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Consultant:</b> {{ $client->user->name }}<br />
									<b>Shortlist:</b> {{ $client->shortlists()->count() }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('client/edit/'.$client->id) }}">
										<i class="fa fa-btn fa-pencil-square-o"></i>Edit
									</a>
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('client/show/'.$client->id) }}">
										<i class="fa fa-btn fa-info-circle"></i>View
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
					@if ($clients->lastPage() > 1)
						<div class="panel-footer nest-buildings-pagination">
							<!-- <div style="margin: 10px 0;">Page {{ $clients->currentPage() }} of {{ $clients->lastPage() }} (Total: {{ $clients->total() }} items)</div> -->
							{{ $clients->links() }}
						</div>
					@endif
				</div>
            @endif
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection
