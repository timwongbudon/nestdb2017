@extends('layouts.app')

@section('content')
<div class="nest-new container">
            <!-- Current Properties -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-9" style="padding:0px;">
							<h4 style="margin-top:0px;">Photoshoot</h4>
						</div>
						<div class="col-sm-3 text-right" style="padding:0px;">
							<div class="nest-property-list-select-all">
								<label for="property-option_id-checkbox-all" style="color:#000000;">Select All</label>
								{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!}
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<form action="{{ url('/photoshoot/update/'.$photoshoot->id) }}" method="POST">
					{{ csrf_field() }}
					@if (!isset($_GET['statuschange']))
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
									<div class="nest-property-edit-row">
										<div class="col-sm-4 col-xs-6">
											 <div class="nest-property-edit-label">Date</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											{{ Form::date('v_date', $photoshoot->v_date, ['class'=>'form-control', 'id'=>'viewingdate']) }}
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-sm-4 col-xs-6">
											 <div class="nest-property-edit-label">Time</div>
										</div>
										<div class="col-sm-2 col-xs-2">
											<select name="timefrom" class="calendar-top-time form-control">
												@foreach ($times as $t)
													@if ($photoshoot->timefrom == $t || $oldfrom == $t)
														<option value="{{ $t }}" selected>{{ $t }}</option>
													@else
														<option value="{{ $t }}">{{ $t }}</option>
													@endif
												@endforeach
											</select>
										</div>
										<div class="col-sm-2 col-xs-2">
											<select name="timeto" class="calendar-top-time form-control">
												@foreach ($times as $t)
													@if ($photoshoot->timeto == $t)
														<option value="{{ $t }}" selected>{{ $t }}</option>
													@else
														<option value="{{ $t }}">{{ $t }}</option>
													@endif
												@endforeach
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Driver</div>
										</div>
										<div class="col-xs-4">
											<label for="photoshoot-driver-0" class="control-label">{!! Form::radio('driver', 0, $photoshoot->driver==0, ['id'=>'photoshoot-driver-0']) !!} No</label>
										</div>
										<div class="col-xs-4">
											<label for="photoshoot-driver-1" class="control-label">{!! Form::radio('driver', 1, $photoshoot->driver==1, ['id'=>'photoshoot-driver-1']) !!} Yes</label>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row" id="useCardHolder">
										<div class="col-xs-4">
											<div class="nest-property-edit-label">Select Car</div>
									   </div>
									   <div class="col-sm-8 col-xs-6">
											<select name="use_car" class="calendar-top-time form-control" disabled>
												<option value="" @if (empty($photoshoot->use_car)) selected @endif></option>
												<option value="1" @if ($photoshoot->use_car == 1) selected @endif>Alphard</option>
												<option value="2" @if ($photoshoot->use_car == 2) selected @endif>Audi A8</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
									<div class="nest-property-edit-row">
										<div class="col-sm-4 col-xs-6">
											 <b>Usage of Car that Day</b>
										</div>
										<div class="col-sm-8 col-xs-6" id="usageofcar">
											...
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							
							<script>
								function checkCarDate(){
									var d = jQuery('#viewingdate').val();
									
									jQuery('#usageofcar').html('...');
									
									jQuery.get("/calendar/carbookingphoto/"+d+"/{{ $photoshoot->id }}", function(data) {
										jQuery('#usageofcar').html(data);
									});
								}

								jQuery('input[name="driver"]').on('change', function(){
									// console.log();
									if (jQuery(this).val() == 1) {
										jQuery('select[name="use_car"]').attr('disabled', false);
									} else {
										jQuery('select[name="use_car"]').val('');
										jQuery('select[name="use_car"]').attr('disabled', true);
									}
								});
								
								jQuery(document).ready(function() {
									checkCarDate();
									console.log(jQuery('input[name="driver"]:checked').val());
									if (jQuery('input[name="driver"]:checked').val() == 1) {
										jQuery('select[name="use_car"]').attr('disabled', false);
									} else {
										jQuery('select[name="use_car"]').val('');
										jQuery('select[name="use_car"]').attr('disabled', true);
									}
								});
								jQuery('#viewingdate').on('blur', function(){
									checkCarDate();
								});
							</script>
						</div>
					@endif
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Status</div>
									</div>
									<div class="col-xs-4">
										<label for="photoshoot-status-0" class="control-label">{!! Form::radio('status', 0, (($photoshoot->status == 0 && !isset($_GET['statuschange'])) || (isset($_GET['statuschange']) && $_GET['statuschange']=='open')), ['id'=>'photoshoot-status-0']) !!} Open</label>
									</div>
									<div class="col-xs-4">
										<label for="photoshoot-status-10" class="control-label">{!! Form::radio('status', 10, (($photoshoot->status == 10 && !isset($_GET['statuschange'])) || (isset($_GET['statuschange']) && $_GET['statuschange']=='close')), ['id'=>'photoshoot-status-10']) !!} Closed</label>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Comments</div>
									</div>
									<div class="col-xs-8">
										{!! Form::textarea('comments', $photoshoot->comments, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row" id="commentsafter-wrapper"
								@if (!isset($_GET['statuschange']) && $photoshoot->status == 0)
									style="display:none;"
								@endif
								>
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Comments after Photoshoot</div>
									</div>
									<div class="col-xs-8">
										{!! Form::textarea('commentsafter', $commentaftercontent, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-xs-12" style="padding-top:15px;">
									@if (!isset($_GET['statuschange']))
										<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Update </button>
									@else
										@if ($_GET['statuschange'] == 'open')
											<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Reopen Photoshoot</button>
										@else
											<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Close Photoshoot</button>
										@endif
									@endif
									<a href="{{ url('/property/photoshoot') }}" class="nest-button nest-right-button btn btn-default">
										<i class="fa fa-btn fa-arrow-left"></i>Back to Photoshoots
									</a>
								</div>
								<script>
									$(document).ready(function(){
										$('#photoshoot-status-0').on('click', function(event){
											$('#commentsafter-wrapper').css('display', 'none');
											$('#tempature-wrapper').css('display', 'none');
										});
										$('#photoshoot-status-10').on('click', function(event){
											$('#commentsafter-wrapper').css('display', 'block');
											$('#tempature-wrapper').css('display', 'block');
										});
									});
								</script>
							</div>
						</div>
					</div>
				</form>
			</div>
			
			@if (!isset($_GET['statuschange']))
				@if (count($properties) > 0)
					@php
						$i = 0;
					@endphp
					
					
					<div class="nest-property-list-wrapper">
						<div class="row">
							<form id="propertylistform" action="{{ url('frontend/select') }}" method="post">
								{{ csrf_field() }}
								
								@foreach ($properties as $property)
									<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 nest-property-list-item">
										<div class="panel panel-default">
											<div class="nest-property-list-item-top">
												<div class="nest-property-list-item-top-checkbox">
													{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
												</div>
												<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
													{{ $property->shorten_building_name(0) }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-image-wrapper">
												<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
													@if(!empty($property->featured_photo_url))
														<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
													@else
														<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
													@endif
													
													<div class="nest-property-list-item-price-wrapper">
														<div class="nest-property-list-item-price">
															{!! $property->price_db_searchresults_new() !!}
														</div>
													</div>
												</a>
												<div class="nest-property-list-item-image-top-bar-left">
													@if (count($property->nest_photos) > 0)
														<div class="nest-property-list-item-nestpics-wrapper">
															<div class="nest-property-list-item-nestpics">
																<img src="/images/camera2.png" />
															</div>
														</div>
													@endif
												</div>
												<div class="nest-property-list-item-image-top-bar">
													@if($property->bonuscomm == 1)
														<div class="nest-property-list-item-bonus-wrapper">
															<div class="nest-property-list-item-bonus">
																$$$
															</div>
														</div>
													@endif
													@if($property->hot == 1)
														<div class="nest-property-list-item-hot-wrapper">
															<div class="nest-property-list-item-hot">
																HOT
															</div>
														</div>
													@endif
													@if($property->web)
														<div class="nest-property-list-item-web-wrapper">
															<div class="nest-property-list-item-web">
																<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																	WEB
																</a>
															</div>
														</div>
													@endif
													@if ($property->owner_id > 0)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																@if ($owner->corporatelandlord == 1)
																	<div class="nest-property-list-item-cp-wrapper">
																		<div class="nest-property-list-item-cp">
																			<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																				CL
																			</a>
																		</div>
																	</div>
																@endif
															@endforeach
														@endif
													@elseif ($property->poc_id == 3 || $property->poc_id == 4)
														@if ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																@if ($agent->corporatelandlord == 1)
																	<div class="nest-property-list-item-cp-wrapper">
																		<div class="nest-property-list-item-cp">
																			<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																				CL
																			</a>
																		</div>
																	</div>
																@endif
															@endforeach
														@endif
													@endif
												</div>
											</div>
											<div class="nest-property-list-item-bottom-address-wrapper">
												<div class="nest-property-list-item-bottom-address">
													{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
												</div>
												<div class="nest-property-list-item-bottom-unit">
													{{ $property->unit }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													Room(s)
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													@if ($property->bedroom_count() == 1)
														{{ $property->bedroom_count() }} bed, 
													@else
														{{ $property->bedroom_count() }} beds, 
													@endif
													@if ($property->bathroom_count() == 1)
														{{ $property->bathroom_count() }} bath
													@else
														{{ $property->bathroom_count() }} baths
													@endif
													
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													Size
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													{{ $property->property_netsize() }} / {{ $property->property_size() }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													@if ($property->poc_id ==  1)
														Landlord
													@elseif ($property->poc_id ==  2)
														Rep
													@elseif ($property->poc_id ==  3)
														Agency
													@else
														Contact
													@endif
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													@php
														$vendorshown = false;
													@endphp
													@if ($property->poc_id ==  1)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach		
															@php
																$vendorshown = true;
															@endphp
														@endif
													@elseif ($property->poc_id ==  2)
														@if ($property->getRep() && $property->getRep()->count() > 0)
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@elseif ($property->poc_id ==  3)
														@if ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@endif
													@if (!$vendorshown)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach														
															@if ($property->getRep() && $property->getRep()->count() > 0)
																@foreach ($property->getRep() as $rep)
																	{!! $rep->basic_info_property_search_linked() !!}
																@endforeach
															@endif
														@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
														@else
															-
														@endif
													@endif
												</div>
												<div class="clearfix"></div>
												@if ($property->poc_id == 1 && $vendorshown)
													@if ($property->getRep() && $property->getRep()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Rep
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif
												@endif
												@if ($property->poc_id == 2 && $vendorshown)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Owner
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif
												@endif
												@if ($property->poc_id == 3 && $vendorshown)
													@if ($property->getAgent() && $property->getAgent()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Agent
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getAgent() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif	
												@endif
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-features-wrapper">
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
													{{ $property->maidroom_count() }}
												</div>
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
													{{ $property->backend_search_outdoor() }}
												</div>
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
													{{ $property->carpark_count() }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-buttons-wrapper">
												<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
													<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-pencil-square-o"></i>Edit
													</a>
												</div>
												
												<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
													<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-dot-circle-o"></i>Show
													</a>
												</div>
												
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									@php
										$i++;
										if ($i%3 == 0){
											echo '<div class="hidden-xs hidden-sm hidden-lg clearfix"></div>';
										}
										if ($i%2 == 0){
											echo '<div class="hidden-lg hidden-md clearfix"></div>';
										}
										if ($i%4 == 0){
											echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
										}
									@endphp
									
								@endforeach
								
								{{ Form::hidden('default_type', '') }}
								{{ Form::hidden('action', 'select') }}
							</form>
						</div>
					</div>
					
					@if ($properties->lastPage() > 1)
						<div class="panel panel-default nest-properties-footer">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-12">
										{{ $properties->links() }}  
									</div>
								</div>
							</div>
						</div>
					@endif
				@endif
					
	
				<div class="panel panel-default nest-property-list-buttons">
					<div class="panel-body">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 col-xs-6">
									<button id="print_pdf" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Print
									</button>
								</div>
								<div class="col-md-3 col-xs-6">
									<button id="print_pdf_agent" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Print (Agent)
									</button>
								</div>
								<div class="col-md-3 col-xs-6">
									<button id="reorder_option" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-arrows"></i>Reorder
									</button>
								</div>
								@if (count($properties) > 0)
									<div class="col-md-3 col-xs-6">
										<button id="delete_option" class="btn btn-danger" style="border-radius:0px;border:0px;">
											<i class="fa fa-btn fa-trash"></i>Delete
										</button>
									</div>
								@else
									<div class="col-md-3 col-xs-6">
										<form class="form-horizontal" action="{{url('photoshoot/remove/'.$photoshoot->id)}}" method="post" onsubmit="return confirm('Do you really want to delete this photoshoot?');">
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger" style="border-radius:0px;border:0px;">
												<i class="fa fa-btn fa-trash-o"></i>Delete Photoshoot
											</button>
										</form>
									</div>
								@endif
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-6">
									<button id="add_frontend_property" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-plus"></i>Website
									</button>
								</div>
								<div class="col-md-3 col-xs-6">
									<button id="remove_frontend_property" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-minus"></i>Website
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
</div>

@endsection

@section('footer-script')
<script type="text/javascript">
$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form input[type="checkbox"]').prop('checked', $(this).is(":checked"));
    });
    $('button#delete_option').click(function(event){
        event.preventDefault();
        var url = "{{ url('photoshoot/option/delete/'.$photoshoot->id) }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_self');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose items to delete.');
        }
        return false;
    });
    $('button#reorder_option').click(function(event){
        event.preventDefault();
        $('form#propertylistform').attr('target', '_blank');
        location.href =  "{{url('photoshoot/show-order/' . $photoshoot->id)}}";;
        return false;
    });
    $('button.photoshoot-option-button').click(function(event){
        event.preventDefault();
        var url = "{{url('photoshoot/option/toggle-type/'.$photoshoot->id)}}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_self');
        var property_id = $(this).parent().find('input[name=property_id]').val();
        $('form#propertylistform input[name=property_id]').val(property_id);
        $('form#propertylistform').submit();
        return false;
    });
    $('button#print_pdf').click(function(event){
        event.preventDefault();
        var url = "{{ url('photoshoot/option/print/') }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#print_pdf_agent').click(function(event){
        event.preventDefault();
        var url = "{{ url('photoshoot/option/printagent/' ) }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
	
    $('button#add_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('select');
		
		proprerty_list_form.submit();
    });
    $('button#remove_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('deselect');
		
		proprerty_list_form.submit();
    });
});
</script>
@endsection