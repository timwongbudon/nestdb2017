@extends('layouts.app')

@section('content')
<div class="nest-new container">
	<div class="nest-property-edit-wrapper row">
		<div class="col-md-offset-3 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="">New Photoshoot</h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
					<form action="{{ url('/photoshoot/store') }}" method="POST">
						{{ csrf_field() }}
						
						<input type="hidden" name="status" value="0" />
						
						<div class="nest-property-edit-row">
							<div class="col-sm-4 col-xs-6">
								 <div class="nest-property-edit-label">Date</div>
							</div>
							<div class="col-sm-4 col-xs-6">
								{{ Form::date('v_date', old('v_date'), ['class'=>'form-control', 'id'=>'viewingdate']) }}
							</div>
							<div class="clearfix"></div>
						</div>
						<!-- <div class="nest-property-edit-row">
							<div class="col-sm-4 col-xs-6">
								 <div class="nest-property-edit-label">Time</div>
							</div>
							<div class="col-sm-2 col-xs-2">
								{{ Form::number('hour', old('hour'), ['class'=>'form-control', 'placeholder'=>'Hour']) }}
							</div>
							<div class="col-sm-2 col-xs-2">
								{{ Form::number('minute', old('minute'), ['class'=>'form-control', 'placeholder'=>'Minute']) }}
							</div>
							<div class="col-sm-2 col-xs-2" style="padding-top:5px;">
								<label for="viewing-ampm-0" class="control-label">{!! Form::radio('ampm', 0, false, ['id'=>'viewing-ampm-0']) !!} am</label> &nbsp;
								<label for="viewing-ampm-1" class="control-label">{!! Form::radio('ampm', 1, true, ['id'=>'viewing-ampm-1']) !!} pm</label>
							</div>
							<div class="clearfix"></div>
						</div> -->
						<div class="nest-property-edit-row">
							<div class="col-sm-4 col-xs-6">
								 <div class="nest-property-edit-label">Time</div>
							</div>
							<div class="col-sm-2 col-xs-2">
								<select name="timefrom" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-2 col-xs-2">
								<select name="timeto" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Driver</div>
							</div>
							<div class="col-xs-4">
								<label for="viewing-driver-0" class="control-label">{!! Form::radio('driver', 0, true, ['id'=>'viewing-driver-0']) !!} No</label>
							</div>
							<div class="col-xs-4">
								<label for="viewing-driver-1" class="control-label">{!! Form::radio('driver', 1, false, ['id'=>'viewing-driver-10']) !!} Yes</label>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="nest-property-edit-row" id="useCardHolder">
							<div class="col-xs-4">
								<div class="nest-property-edit-label">Select Car</div>
						   </div>
						   <div class="col-sm-8 col-xs-6">
								<select name="use_car" class="calendar-top-time form-control" disabled>
									<option value=""></option>
									<option value="1">Alphard</option>
									<option value="2">Audi A8</option>
								</select>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <b>Usage of Car that Day</b>
							</div>
							<div class="col-sm-8 col-xs-6" id="usageofcar">
								Please enter a date first
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Comments</div>
							</div>
							<div class="col-xs-8">
								{!! Form::textarea('comments', '', ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-xs-12" style="padding-top:15px;">
							<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Create </button>
						</div>
						
						<script>
							function checkCarDate(){
								var d = jQuery('#viewingdate').val();
								
								jQuery('#usageofcar').html('...');
								
								jQuery.get("/calendar/carbookingphoto/"+d+"/0", function(data) {
									jQuery('#usageofcar').html(data);
								});
							}

							jQuery('input[name="driver"]').on('change', function(){
								// console.log();
								if (jQuery(this).val() == 1) {
									jQuery('select[name="use_car"]').attr('disabled', false);
								} else {
									jQuery('select[name="use_car"]').val('');
									jQuery('select[name="use_car"]').attr('disabled', true);
								}
							});
							
							jQuery('#viewingdate').on('blur', function(){
								checkCarDate();
							});
						</script>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
