@extends('layouts.app')

@section('content')
    
            <!-- Current Properties -->
            <div class="panel panel-default">
<form action="{{ url('photoshoot/option/reorder') }}" method="POST" class="form-horizontal">{{ csrf_field() }}
                    <div class="panel-body">
    <ul id="sortable-1" class="container sortable-listing" style="">
    @if (count($properties) > 0)
    @foreach ($properties as $property)
        <li id="{{ $property->id }}" class="default col-sm-3" style="padding:15px !important;background:#ffffff;">
            @if(!empty($property->featured_photo_url))
				<img src="{{ $property->featured_photo_url }}" alt="" width="100%" />
			@else
				<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
            @endif
            {{ Form::hidden('options_ids[]', $property->id) }}
			<div style="font-size:16px;font-family:'Gill Sans', Calibri;padding:10px 0px;text-transform:uppercase;">
				{{ $property->name }} (ID {{$property->id}})
			</div>
        </li>
    @endforeach
    @endif
    </ul>
                    </div>
            </div>


            <div class="panel panel-default nest-property-list-buttons">
                <div class="panel-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-offset-6 col-sm-3 col-xs-6">
								<a href="{{ url('/photoshoot/show/'.$photoshoot->id) }}" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-arrow-left"></i>Back
								</a>
                            </div>
							<div class="col-sm-3 col-xs-6">
								{{ Form::hidden('photoshoot_id', $photoshoot->id) }}
								<button id="reorder_option" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-arrows"></i>Re-order
								</button>
							</div>
                        </div>
					</div>
                </div>
            </div>
</form>
@endsection

@section('footer-script')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
#sortable-1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 100%;
}

#sortable-1 li {
    padding: 0.4em;
    padding-left: 1.5em;
    font-size: 17px;
    height: auto;
    min-height: 280px;
    cursor: move;
}

.default {
    background: #f5f5f5;
    border: 1px solid #DDDDDD;
    color: #333333;
}
</style>
<script>
$(document).ready(function(){
    $( "#sortable-1" ).sortable();
});
$(document).ready(function(){
    $("#reorder_option").click(function(){
        event.preventDefault();
        $('form').submit();
    });
    $("#backto_option").click(function(){
        event.preventDefault();
        location.href = "{{ url( '/photoshoot/show/'.$photoshoot->id ) }}";
    });
});

</script>
@endsection