@extends('layouts.app')

@section('content')

<style>
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>


<div class="nest-new dashboardnew">
	<div class="row">
		<div class="nest-dashboardnew-wrapper">
			<div class="col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Year</div>
							</div>
							<div class="col-xs-8">
								<select id="year" class="form-control">
									@foreach ($years as $y)
										<option value="{{ $y }}"
										@if ($y == $year)
											selected
										@endif
										>
											{{ $y }}
										</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#year").on('change', function(event){
										var val = jQuery("#consultant").val();
										var year = jQuery("#year").val();
										
										window.location = "{{ url('companyoverviewuser') }}/"+val+"/"+year;
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-offset-4 col-lg-4 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="nest-property-edit-row">
							<div class="col-xs-4">
								 <div class="nest-property-edit-label">Consultant</div>
							</div>
							<div class="col-xs-8">
								<select id="consultant" class="form-control">
									<option value="0">Company Overview</option>
									@foreach ($users as $u)
										@if ($u->isConsultant())
											<option value="{{ $u->id }}" 
											@if ($u->id == $consultantid)
												selected
											@endif
											>
											@if ($u->status == 1)
												[inactive]
											@endif
											{{ $u->name }}
											</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<script>
								jQuery(document).ready(function(){
									jQuery("#consultant").on('change', function(event){
										var val = jQuery("#consultant").val();
										var year = jQuery("#year").val();
										
										if (val == 0){
											window.location = "{{ url('companyoverview') }}/"+year;
										}else{
											window.location = "{{ url('companyoverviewuser') }}/"+val+"/"+year;
										}
									});
								});
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			
			
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Completed vs. Dead Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<tr>
								<td width="50%"></td>
								<td width="25%" class="text-center">
									<b>Completed</b>
								</td>
								<td width="25%" class="text-center">
									<b>Dead</b>
								</td>
							</tr>
							@foreach ($leadsources as $key => $leadname)
								<tr>
									<td width="50%">{{ $leadname }}</td>
									<td width="25%" class="text-center">
										@if ($leadsdeadvscompletebysource[$key]['complete'] > 0)
											{{ $leadsdeadvscompletebysource[$key]['complete'] }} 
											({{ round($leadsdeadvscompletebysource[$key]['complete']/$leadsdeadvscompletebysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
									<td width="25%" class="text-center">
										@if ($leadsdeadvscompletebysource[$key]['dead'] > 0)
											{{ $leadsdeadvscompletebysource[$key]['dead'] }} 
											({{ 100-round($leadsdeadvscompletebysource[$key]['complete']/$leadsdeadvscompletebysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
								</tr>
							@endforeach
							<tr>
								<td width="50%">No Source</td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscompletebysource[0]['complete'] > 0)
										{{ $leadsdeadvscompletebysource[0]['complete'] }} 
										({{ round($leadsdeadvscompletebysource[0]['complete']/$leadsdeadvscompletebysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscompletebysource[0]['dead'] > 0)
										{{ $leadsdeadvscompletebysource[0]['dead'] }} 
										({{ 100-round($leadsdeadvscompletebysource[0]['complete']/$leadsdeadvscompletebysource[0]['total']*100, 0) }}%)
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Total</b></td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscomplete['complete'] > 0)
										<b>{{ $leadsdeadvscomplete['complete'] }} 
										({{ round($leadsdeadvscomplete['complete']/$leadsdeadvscomplete['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td width="25%" class="text-center">
									@if ($leadsdeadvscomplete['dead'] > 0)
										<b>{{ $leadsdeadvscomplete['dead'] }} 
										({{ 100-round($leadsdeadvscomplete['complete']/$leadsdeadvscomplete['total']*100, 0) }}%)</b>
									@endif
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Active Clients Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<tr>
								<td width="28%"></td>
								<td width="12%" class="text-center">
									<b>New</b>
								</td>
								<td width="12%" class="text-center">
									<b>Hot</b>
								</td>
								<td width="12%" class="text-center">
									<b>Offering</b>
								</td>
								<td width="12%" class="text-center">
									<b>Invoicing</b>
								</td>
								<td width="12%" class="text-center">
									<b>Active</b>
								</td>
								<td width="12%" class="text-center">
									<b>Stored</b>
								</td>
							</tr>
							@foreach ($leadsources as $key => $leadname)
								<tr>
									<td>{{ $leadname }}</td>
									<td class="text-center">
										@if ($leadstotalbysource[$key]['new'] > 0)
											{{ $leadstotalbysource[$key]['new'] }} 
											(~{{ round($leadstotalbysource[$key]['new']/$leadstotalbysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
									<td class="text-center">
										@if ($leadstotalbysource[$key]['hot'] > 0)
											{{ $leadstotalbysource[$key]['hot'] }} 
											(~{{ round($leadstotalbysource[$key]['hot']/$leadstotalbysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
									<td class="text-center">
										@if ($leadstotalbysource[$key]['offering'] > 0)
											{{ $leadstotalbysource[$key]['offering'] }} 
											(~{{ round($leadstotalbysource[$key]['offering']/$leadstotalbysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
									<td class="text-center">
										@if ($leadstotalbysource[$key]['invoicing'] > 0)
											{{ $leadstotalbysource[$key]['invoicing'] }} 
											(~{{ round($leadstotalbysource[$key]['invoicing']/$leadstotalbysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
									<td class="text-center">
										@if ($leadstotalbysource[$key]['active'] > 0)
											{{ $leadstotalbysource[$key]['active'] }} 
											(~{{ round($leadstotalbysource[$key]['active']/$leadstotalbysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
									<td class="text-center">
										@if ($leadstotalbysource[$key]['stored'] > 0)
											{{ $leadstotalbysource[$key]['stored'] }} 
											(~{{ round($leadstotalbysource[$key]['stored']/$leadstotalbysource[$key]['total']*100, 0) }}%)
										@endif
									</td>
								</tr>
							@endforeach
							<tr>
								<td>No Source</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['new'] > 0)
										{{ $leadstotalbysource[0]['new'] }} 
										(~{{ round($leadstotalbysource[0]['new']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['hot'] > 0)
										{{ $leadstotalbysource[0]['hot'] }} 
										(~{{ round($leadstotalbysource[0]['hot']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['offering'] > 0)
										{{ $leadstotalbysource[0]['offering'] }} 
										(~{{ round($leadstotalbysource[0]['offering']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['invoicing'] > 0)
										{{ $leadstotalbysource[0]['invoicing'] }} 
										(~{{ round($leadstotalbysource[0]['invoicing']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['active'] > 0)
										{{ $leadstotalbysource[0]['active'] }} 
										(~{{ round($leadstotalbysource[0]['active']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotalbysource[0]['stored'] > 0)
										{{ $leadstotalbysource[0]['stored'] }} 
										(~{{ round($leadstotalbysource[0]['stored']/$leadstotalbysource[0]['total']*100, 0) }}%)
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Total</b></td>
								<td class="text-center">
									@if ($leadstotal['new'] > 0)
										<b>{{ $leadstotal['new'] }} 
										(~{{ round($leadstotal['new']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['hot'] > 0)
										<b>{{ $leadstotal['hot'] }} 
										(~{{ round($leadstotal['hot']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['offering'] > 0)
										<b>{{ $leadstotal['offering'] }} 
										(~{{ round($leadstotal['offering']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['invoicing'] > 0)
										<b>{{ $leadstotal['invoicing'] }} 
										(~{{ round($leadstotal['invoicing']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['active'] > 0)
										<b>{{ $leadstotal['active'] }} 
										(~{{ round($leadstotal['active']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
								<td class="text-center">
									@if ($leadstotal['stored'] > 0)
										<b>{{ $leadstotal['stored'] }} 
										(~{{ round($leadstotal['stored']/$leadstotal['total']*100, 0) }}%)</b>
									@endif
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

		
		
			<div class="clearfix"></div>
			
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Active Enquiries</h4>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped">
								<tr>
									<td class="text-center" width="150px">
										<b>Date</b>
									</td>
									<td class="text-center">
										<b>Client</b>
									</td>
									<td class="text-center" width="80px">
										<b>Status</b>
									</td>
									<td class="text-center" width="120px">
										<b>Budget&nbsp;From</b>
									</td>
									<td class="text-center" width="120px">
										<b>Budget&nbsp;To</b>
									</td>
									<td class="text-center" width="120px">
										<b>Last&nbsp;Outreach</b>
									</td>
									<td class="text-center" width="120px">
										<b>Next&nbsp;Outreach</b>
									</td>
									<td class="text-center" width="150px">
										<b>Viewings</b>
									</td>
									<td class="text-center" width="80px">
										<b>Offering</b>
									</td>
									<td class="text-center" width="120px">
										<b>Offer&nbsp;Amount</b>
									</td>
									
									
								</tr>
								@foreach ($liveleads as $key => $l)
									<tr>
										<td class="text-center">
											{{ $l->created_at() }}
										</td>
										<td class="text-left">
											@if (isset($l['client']))
												<a href="/lead/edit/{{ $l->id }}">
													{{ $l['client']->name() }}
												</a>
											@endif
										</td>
										<td class="text-center"
											@if ($l->status == 0)
												 style="background:#880000;color:#ffffff;"
											@elseif ($l->status == 5 || $l->status == 6)
												 style="background:#008800;color:#ffffff;"
											@endif
										>
											@if (isset($allstatus[$l->status]))
												{{ $allstatus[$l->status] }}
											@endif
										</td>
										<td class="text-right">
											@if ($l->budget > 0)
												HK$&nbsp;{{ number_format($l->budget, 0, '.', ',') }}
											@endif
										</td>
										<td class="text-right">
											@if ($l->budgetto > 0)
												HK$&nbsp;{{ number_format($l->budgetto, 0, '.', ',') }}
											@endif
										</td>
										<td class="text-center"
											@if ($reminderdate > $l->lastoutreach && $l->remindme < $todaydate)
												 style="background:#880000;color:#ffffff;"
											@endif
										>
											{{ $l->getNiceLastoutreach() }}
										</td>
										<td class="text-center"
											@if ($todaydate > $l->remindme && trim($l->remindme) != '' && trim($l->remindme) != '0000-00-00')
												 style="background:#880000;color:#ffffff;"
											@endif
										>
											{{ $l->getNiceRemindMe() }}
										</td>
										<td class="text-center">
											@if (isset($liveviewings[$l->shortlistid]))
												{{ $liveviewings[$l->shortlistid] }}
											@endif
										</td>
										<td class="text-center">
											@if (isset($liveoffer[$l->shortlistid]))
												Yes
											@endif
										</td>
										<td class="text-center">
											@if (isset($liveoffer[$l->shortlistid]))
												@php
													$fields = json_decode($liveoffer[$l->shortlistid]->fieldcontents, true);
												@endphp
												@if (isset($fields['f13']) && $fields['f13'] > 0)
													HK$&nbsp;{{ number_format($fields['f13'], 0, '.', ',') }}
												@endif
											@endif
										</td>
									</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			
			
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Target (leasing)</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
							@if ($year == intval(date('Y')))
								<a href="javascript:;" id="chartlink1" onClick="showChart(1)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">{{ date('M Y') }}</a>
								<a href="javascript:;" id="chartlink4" onClick="showChart(4)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">Annual</a>
							@else
								<a href="javascript:;" id="chartlink4" onClick="showChart(4)" class="chartlinkbutton nest-button btn btn-default" style="border-radius:0px;">Annual</a>
							@endif
							<!-- <a href="javascript:;" id="chartlink2" onClick="showChart(2)" class="chartlinkbutton btn btn-white">Quarter</a> -->
							
							<!-- <a href="javascript:;" id="chartlink3" onClick="showChart(3)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Commission</a> -->
							<!-- <a href="javascript:;" id="chartlink5" onClick="showChart(5)" class="chartlinkbutton btn btn-white" style="border-radius:0px;">12m Deals</a> -->
						</div>
						<script>
							function showChart(id){
								jQuery('.chartlinkbutton').removeClass('nest-button');
								jQuery('.chartlinkbutton').removeClass('btn-default');
								jQuery('.chartlinkbutton').removeClass('btn-white');
								jQuery('.chartlinkbutton').addClass('btn-white');
								jQuery('.dashboard-chart-wrapper-lease').css('display', 'none');
								jQuery('#chartlink'+id).addClass('nest-button');
								jQuery('#chartlink'+id).removeClass('btn-white');
								jQuery('#chartlink'+id).addClass('btn-default');
								jQuery('#lease'+id).css('display', 'block');
							}
						</script>
						@if ($year == intval(date('Y')))
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease1" style="padding-bottom:20px;">
								<div class="row">
									<div class="text-left col-md-6">
										<h4>HK$ {{ number_format($fulldone['monthdollar'], 0, '.', ',') }}</h4>
										<h5>Done</h5>
									</div>
									<div class="text-right col-md-6">
										<h4>HK$ {{ number_format($fulltargets['monthdollar'], 0, '.', ',') }}</h4>
										<h5>Target</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="progress progress-mini">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['monthdollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['monthdollar'] }}%">
												<span class="sr-only">{{ $fullpercent['monthdollar'] }}% Complete</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="text-left col-md-6">
										@if ($fulldone['monthdeals'] == 1)
											<h4>1 Deal</h4>											
										@else
											<h4>{{ $fulldone['monthdeals'] }} Deals</h4>											
										@endif
										<h5>Done</h5>
									</div>
									<div class="text-right col-md-6">
										@if ($fulltargets['monthdeals'] == 1)
											<h4>1 Deal</h4>											
										@else
											<h4>{{ $fulltargets['monthdeals'] }} Deals</h4>											
										@endif
										<h5>Target</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="progress progress-mini">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['monthdeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['monthdeals'] }}%">
												<span class="sr-only">{{ $fullpercent['monthdeals'] }}% Complete</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease4" style="display:none;padding-bottom:20px;">
						@else
							<div class="dashboard-chart-wrapper dashboard-chart-wrapper-lease" id="lease4" style="padding-bottom:20px;">
						@endif
							<div class="row">
								<div class="text-left col-md-6">
									<h4>HK$ {{ number_format($fulldone['anndollar'], 0, '.', ',') }}</h4>
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									<h4>HK$ {{ number_format($fulltargets['anndollar'], 0, '.', ',') }}</h4>
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['anndollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['anndollar'] }}%">
											<span class="sr-only">{{ $fullpercent['anndollar'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="text-left col-md-6">
									@if ($fulldone['anndeals'] == 1)
										<h4>1 Deal</h4>											
									@else
										<h4>{{ $fulldone['anndeals'] }} Deals</h4>											
									@endif
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									@if ($fulltargets['anndeals'] == 1)
										<h4>1 Deal</h4>											
									@else
										<h4>{{ $fulltargets['anndeals'] }} Deals</h4>											
									@endif
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['anndeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['anndeals'] }}%">
											<span class="sr-only">{{ $fullpercent['anndeals'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						@if ($dollarsum > 0 && $monthcount > 1)
							<div class="nest-commission-average">Average Previous {{ $monthcount }} Months: HK$ {{ number_format($dollarsum, 0, '.', ',') }}</div>
						@endif
						<h4>Commission Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-wrapper" id="lease3">
							<canvas id="allmonthsdollar" height="88px"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			<div class="col-sm-4">
				<div class="panel panel-default" style="margin-bottom:10px;padding-bottom:20px;">
					<div class="panel-heading">
						<h4>Target (sales)</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-buttons btn-group btn-group-justified" style="border-radius:0px;">
							<a href="javascript:;" class="nest-button btn btn-default" style="border-radius:0px;">Annual</a>
						</div>
						<div class="dashboard-chart-wrapper" id="sale1">
							<div class="row">
								<div class="text-left col-md-6">
									<h4>HK$ {{ number_format($fulldone['saledollar'], 0, '.', ',') }}</h4>
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									<h4>HK$ {{ number_format($fulltargets['saledollar'], 0, '.', ',') }}</h4>
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['saledollar'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['saledollar'] }}%">
											<span class="sr-only">{{ $fullpercent['saledollar'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="text-left col-md-6">
									@if ($fulldone['saledeals'] == 1)
										<h4>1 Deal</h4>											
									@else
										<h4>{{ $fulldone['saledeals'] }} Deals</h4>											
									@endif
									<h5>Done</h5>
								</div>
								<div class="text-right col-md-6">
									@if ($fulltargets['saledeals'] == 1)
										<h4>1 Deal</h4>											
									@else
										<h4>{{ $fulltargets['saledeals'] }} Deals</h4>											
									@endif
									<h5>Target</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="progress progress-mini">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $fullpercent['saledeals'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fullpercent['saledeals'] }}%">
											<span class="sr-only">{{ $fullpercent['saledeals'] }}% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						@if ($dealsum > 0 && $monthcount > 1)
							<div class="nest-commission-average">Average Previous {{ $monthcount }} Months: {{ number_format($dealsum, 2, '.', ',') }} Deals</div>
						@endif
					   <h4>Deals Jan-Dec {{ $year }}</h4>
					</div>
					<div class="panel-body">
						<div class="dashboard-chart-wrapper" id="lease5">
							<canvas id="allmonthsdeals" height="87px"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

			
<script>
	var ctx = document.getElementById("allmonthsdollar").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Earned Commission',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['dollardone']) }}]
			}, {
				label: 'Target',
				backgroundColor: '#cccccc',
				data: [{{ implode(',', $monthlyvalues['dollartarget']) }}]
			}]
		},
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				var year = {{ date('Y') }};
				window.location = "/commission/invoicing?year="+year+"&month="+month+"";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					min: 0,
					beginAtZero: true,
					stacked: true,
					ticks: {
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return '$' + value + '';
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';
						
						if (tooltipItem.datasetIndex == 0){
							label = 'Earned Commission HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						}else if (tooltipItem.datasetIndex == 1){
							label = 'Below Target HK$'+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
						}
						
						return label;
					}
				}
			}
		}
	});
	
	var ctx = document.getElementById("allmonthsdeals").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: 'Made Deals',
				backgroundColor: '#81b53e',
				data: [{{ implode(',', $monthlyvalues['dealsdone']) }}]
			}, {
				label: 'Target',
				backgroundColor: '#cccccc',
				data: [{{ implode(',', $monthlyvalues['dealstarget']) }}]
			}]
		},
		options: {
			onClick: function(event, array){
				var month = array[0]._index+1;
				var year = {{ date('Y') }};
				window.location = "/commission/invoicing?year="+year+"&month="+month+"";
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true,
					ticks: {
						min: 0,
						beginAtZero: true,
						callback: function(value, index, values) {
							if (Math.floor(value) == value) {
								return value;
							}
						}
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = '';
						
						if (tooltipItem.datasetIndex == 0){
							label = 'Made Deals: '+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						}else if (tooltipItem.datasetIndex == 1){
							label = 'Below Target: '+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						}
						
						return label;
					}
				}
			}
		}
	});

</script>
			
<style>
.chart-yaxis{
	color:#ff0000;
}
</style>
			
			
			
			
			
		</div>
	</div>
</div>
@endsection
