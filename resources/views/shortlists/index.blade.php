@extends('layouts.app')

@section('content')


            <!-- Shortlist Search -->
            <div class="nest-buildings-search-panel panel panel-default">
                <div class="panel-body">
                    <form action="{{ url('shortlists') }}" method="GET" class="form-horizontal">
                        <div class="nest-buildings-search-wrapper row">
                            <div class="col-sm-2">
                                {!! Form::select('user_id', $user_ids, $curuser, ['id'=>'shortlist-user_id', 'class'=>'form-control', 'placeholder' => 'All consultant shortlists']); !!}
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="client_name" placeholder="Client name" id="client-name" class="form-control" value="{{ old('client_name', $input->client_name) }}">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="budget" placeholder="Budget" class="form-control" value="{{ old('budget', $input->budget) }}" >
                            </div>
                            <div class="col-sm-2">

                                {!! Form::select('tempature', $tempatures, $input->tempature, ['id'=>'shortlist-tempature', 'class'=>'form-control', 'placeholder' => 'Choose a tempature']); !!}
                            </div>

														<div class="col-sm-2">
																{!! Form::select('typeid',
																	[ 1=> 'Lease', 2=>'Sale'], $input->typeid, ['id'=>'shortlist-typeid', 'class'=>'form-control', 'placeholder' => 'Choose a Type']); !!}
														</div>
							<div class="clearfix"></div>
                        </div>
                        <div class="nest-buildings-search-buttons-wrapper row">
                            <div class="col-xs-12">
                                <button type="submit" name="action" value="search" class="nest-button nest-right-button btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current shortlists -->
				@if (count($shortlists) > 0)
					<div>
						<div class="nest-buildings-result-wrapper">
							@foreach ($shortlists as $index => $shortlist)
								<div class="row
								@if ($index%2 == 1)
									nest-striped
								@endif
								">
									<div class="col-lg-2 col-md-3 col-sm-6">
										<b>Client:</b> <a href="{{ url('shortlist/show/'.$shortlist->id) }}">{{ $shortlist->client->name() }}</a><br />
										<b>ID:</b> {{ $shortlist->id }}<br/>
										<b>Type:</b> {{ $shortlist->typeid  == 0 ? '-': ($shortlist->typeid == 1 ? 'Lease' : 'Sale')}}</b>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Budget:</b> {{ $shortlist->client->budget() }}<br />
										<b>Tempature:</b> {{ $shortlist->client->tempature() }}
									</div>
									<div class="col-lg-2 col-md-3 col-sm-6">
										<b>Count:</b> {{ $shortlist->options()->count() }}<br />
										<b>Consultant:</b> {{ $shortlist->user->name }}
									</div>
									<div class="col-lg-5 col-md-3 col-sm-6">
										@if ($shortlist->status == 0)
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id.'?statuschange=close') }}">
												<i class="fa fa-btn fa-lock"></i>Close
											</a>
										@elseif ($shortlist->status == 10)
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id.'?statuschange=open') }}">
												<i class="fa fa-btn fa-unlock"></i>Reopen
											</a>
										@endif

										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/manage/'.$shortlist->id) }}">
											<i class="fa fa-btn fa-plus"></i>Properties
										</a>
										@if ($shortlist->typeid == 2)
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/index-sale/'.$shortlist->id) }}">
												<i class="fa fa-btn fa-book"></i>Documents
											</a>
										@else
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/index-lease/'.$shortlist->id) }}">
												<i class="fa fa-btn fa-book"></i>Documents
											</a>
										@endif
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('shortlist/show/'.$shortlist->id) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>View
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						</div>
                    </div>
					@if ($shortlists->lastPage() > 1)
						<div class="panel-footer nest-buildings-pagination">
							<!-- <div style="margin: 10px 0;">Page {{ $shortlists->currentPage() }} of {{ $shortlists->lastPage() }} (Total: {{ $shortlists->total() }} items)</div> -->
							{{ $shortlists->links() }}
						</div>
					@endif
				@endif
@endsection

@section('footer-script')
@endsection
