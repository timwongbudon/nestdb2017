@extends('layouts.app')

@section('content')
<div class="nest-new container">
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Shortlist for {{ $shortlist['client']->name() }} [{{ $shortlist->created_at}}]</h4>
                    </div>

                    <div class="panel-body" style="background-color: #f5f5f5; border-bottom: 1px solid #ddd;">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label for="property-shortlist" class="control-label" style="float: left; padding: 5px 10px 5px 0;">Save to shortlist</label>
                                <input type="text" name="client-name" id="client-name" class="form-control" value="" placeholder="Type client name" style="width: 60%;">
                                <span id="client-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the shortlist name"><i class="fa fa-btn fa-close"></i></button></span>
                                <span id="property-shortlist-message" style="color: red; font-style: italic; display: none"></span>
                                <span id="property-shortlist-action" style="font-style: italic; display: none"></span>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                    <form id="shortlist_show" action="{{ url('shortlist/print/' . $shortlist->id) }}" method="post">
                        {{ csrf_field() }}
                        <table class="table table-striped property-table">
                            <thead>
                                <th style="width: 3%"><div>{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!} </div></th>
                                <th style="width: 5%">ID</th>
                                <th>Photo</th>
                                <th>Property</th>
                                <th>Type</th>
                                <th>Print Type</th>
                                <th style="width: 8%">&nbsp;</th>
                                <th style="width: 8%">&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($properties as $property)
                                    <tr>
                                        <td class="table-text"><div>{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!} </div></td>
                                        <td class="table-text"><div>{{ $property->id }}
                                        @if($property->external_id)({{$property->external_id}})@endif
                                        </div></td>
                                        <td class="table-text"> 
                                            @if(!empty($property->featured_photo_url))
                                            <img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}" alt="" width="300" />
                                            <img src="{{ $property->first_print_image() }}" alt="" width="80" style="display: none;">
                                            <img src="{{ $property->second_print_image() }}" alt="" width="80" style="display: none;">
                                            <img src="{{ $property->third_print_image() }}" alt="" width="80" style="display: none;">
                                            @endif
                                        </td>
                                        <td class="table-text"><div>
                                             <strong>{{ $property->shorten_building_name(0) }}</strong>
                                            @if($property->hot == 1)
                                                <img src="{{ url('hot.gif')}} " alt="">
                                            @endif
                                            <br/>
                                            <?php $rep_ids = $property->rep_ids?explode(',',$property->rep_ids):array(); ?>
                                            {{ $property->fix_address1() }}<br/><hr style="margin: 5px 0;">

                                            @if($property->unit)
                                                Unit: {{ $property->unit }}<br/>
                                            @endif
                                            
                                            Asking Price: {{ $property->price() }}<br/>

                                            @if($property->bedroom_count())
                                                Bedrooms: {{ $property->bedroom_count() }}<br/>
                                            @endif

                                            @if($property->bathroom_count())
                                                Bathrooms: {{ $property->bathroom_count() }}<br/>
                                            @endif

                                            @if($property->show_size())
                                                Size: {{ $property->show_size() }}<br/>
                                            @endif
                                            
                                            @if($property->size)
                                                Size: {{ $property->property_size() }}<br/>
                                            @endif

                                            @if($property->poc_id)
                                                Point of contact: {{ $property->poc() }}<br/>
                                            @endif
                                            
                                            @if(!empty($property['building']->tel))
                                                Mgmt Office tel: {{$property['building']->tel}}<br/>
                                            @endif
                                            
                                            @if(!empty($property['owner']))
                                                Owner: {{$property['owner']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['owner']) && !empty($property['reps']))
                                                @foreach($property['reps'] as $i => $rep)
                                                Rep {{$i+1}}: {{$rep->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property['agent']))
                                                Agency: {{$property['agent']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['agent']) && !empty($property['agents']))
                                                @foreach($property['agents'] as $i => $agent)
                                                Agent {{$i+1}}: {{$agent->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property->comments) && $property->comments != '[]')
                                                Last Comment: {{$property->show_last_comment()}}
                                            @endif

                                            @if(empty($property['agent']) && empty($property['owner']) && !empty($property->contact_info))
                                                Contact Info: {{$property->contact_info}}<br/>
                                            @endif

                                            @if(!empty($property->available_date()))
                                            Available Date: {{ $property->available_date() }}
                                            @endif
                                            <hr style="margin: 5px 0;">
                                            {{ $property->show_timestamp() }}
                                        </div></td>
                                        <td class="table-text"><div>{{ $property->type() }}</div></td>
                                        <td class="table-text" id="shortlist-option-{{$property->id}}">
                                        @if($property->type_id == 3)
                                        <?php $option = $property->shortlist_option->where('shortlist_id', $shortlist->id)->where('property_id', $property->id)->first();?>
                                                <input type="hidden" name="property_id" value="{{ $property->id }}">
                                                <button id="shortlist-option-{{ $shortlist->id }}" class="btn btn-primary shortlist-option-button">
                                                    <i class="fa fa-btn fa-toggle-on"></i>{{ $option->type() }}
                                                </button>
                                        @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('property/show/'.$property->id) }}">
                                                <i class="fa fa-btn fa-info-circle"></i>View
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('property/edit/'.$property->id) }}">
                                                <i class="fa fa-btn fa-pencil-square-o"></i>Edit
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ Form::hidden('property_id', '') }}
                        {{ Form::hidden('shortlist_id', $shortlist->id) }}
                    </form>
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $properties->currentPage() }} of {{ $properties->lastPage() }} (Total: {{ $properties->total() }} items)</div>
                        {{ $properties->links() }}
                    </div>
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Add Property Button -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                    <button id="print_pdf" class="btn btn-primary">
                                        <i class="fa fa-btn fa-print"></i>Print
                                    </button>
									
                                    <button id="print_pdf_agent" class="btn btn-primary">
                                        <i class="fa fa-btn fa-print"></i>Print (Agent)
                                    </button>

                                    <button id="reorder_option" class="btn btn-primary">
                                        <i class="fa fa-btn fa-arrows"></i>Reorder
                                    </button>

                                    <button id="delete_option" class="btn btn-danger">
                                        <i class="fa fa-btn fa-trash"></i>Delete
                                    </button>
                            </div>
                        </div>
                </div>
            </div>
</div>

@include('partials.property.save-shortlist')

@endsection

@section('footer-script')
<script type="text/javascript">
$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form input').prop('checked', $(this).is(":checked"));
    });
    $('button#print_pdf').click(function(event){
        event.preventDefault();
        var url = "{{ url('shortlist/option/print/') }}";
        $('form#shortlist_show').attr('action', url);
        $('form#shortlist_show').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#shortlist_show').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#print_pdf_agent').click(function(event){
        event.preventDefault();
        var url = "{{ url('shortlist/option/printagent/' ) }}";
        $('form#shortlist_show').attr('action', url);
        $('form#shortlist_show').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#shortlist_show').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#delete_option').click(function(event){
        event.preventDefault();
        var url = "{{url('shortlist/option/delete/')}}";
        $('form#shortlist_show').attr('action', url);
        $('form#shortlist_show').attr('target', '_self');
        if ($('form input.property-option:checked').length > 0) {
            $('form#shortlist_show').submit();
        } else {
            alert('Please choose items to delete.');
        }
        return false;
    });
    $('button#reorder_option').click(function(event){
        event.preventDefault();
        $('form#shortlist_show').attr('target', '_blank');
        location.href =  "{{url('shortlist/show-order/' . $shortlist->id)}}";;
        return false;
    });
    $('button.shortlist-option-button').click(function(event){
        event.preventDefault();
        var url = "{{url('shortlist/option/toggle-type/')}}";
        $('form#shortlist_show').attr('action', url);
        $('form#shortlist_show').attr('target', '_self');
        var property_id = $(this).parent().find('input[name=property_id]').val();
        $('form#shortlist_show input[name=property_id]').val(property_id);
        $('form#shortlist_show').submit();
        return false;
    });
});
</script>
@include('partials.property.save-shortlist-control')
@endsection