@extends('layouts.app')

@section('content')
<div class="nest-new container">
            <!-- Current Properties -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="padding-bottom:0px;">Shortlist for {{ $shortlist['client']->name() }} [{{ $shortlist->created_at()}}]</h4>
				</div>

				@if (count($properties) > 0)
					<div class="nest-shortlist-input panel-body">
						<div class="row">
							<div class="col-sm-9">
								<input type="text" name="client-name" id="client-name" class="form-control" value="" placeholder="Type client name to save to shortlist" style="width: 60%;">
								<span id="client-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the shortlist name"><i class="fa fa-btn fa-close"></i></button></span>
								<span id="property-shortlist-message" style="color: red; font-style: italic; display: none"></span>
								<span id="property-shortlist-action" style="font-style: italic; display: none"></span>
							</div>
							<div class="col-sm-3 text-right">
								<div class="nest-property-list-select-all">
									<label for="property-option_id-checkbox-all">Select All</label>
									{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!}
								</div>
							</div>
						</div>
					</div>
				@else
					<div style="padding-bottom:15px;"></div>
				@endif
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							@if (!isset($_GET['statuschange']))
								<div class="nest-property-edit-label-features">
									<div class="col-xs-12">
										<u>Settings</u>
									</div>
								</div>
							@endif
							<form action="{{ url('/shortlist/store/'.$shortlist->id) }}" method="POST">
								{{ csrf_field() }}
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Status</div>
									</div>
									<div class="col-xs-4">
										<label for="shortlist-status-0" class="control-label">{!! Form::radio('status', 0, (($shortlist->status == 0 && !isset($_GET['statuschange'])) || (isset($_GET['statuschange']) && $_GET['statuschange']=='open')), ['id'=>'shortlist-status-0']) !!} Open</label>
									</div>
									<div class="col-xs-4">
										<label for="shortlist-status-10" class="control-label">{!! Form::radio('status', 10, (($shortlist->status == 10 && !isset($_GET['statuschange'])) || (isset($_GET['statuschange']) && $_GET['statuschange']=='close')), ['id'=>'shortlist-status-10']) !!} Closed</label>
									</div>		
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Type</div>
									</div>
									<div class="col-xs-4">
										<label for="shortlist-type-1" class="control-label">{!! Form::radio('typeid', 1, $shortlist->typeid == 1, ['id'=>'shortlist-type-1']) !!} Lease</label>
									</div>
									<div class="col-xs-4">
										<label for="shortlist-type-2" class="control-label">{!! Form::radio('typeid', 2, $shortlist->typeid == 2, ['id'=>'shortlist-type-2']) !!} Sale</label>
									</div>		
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Consultant</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('user_id', $users, $shortlist->user_id, ['class'=>'form-control', 'placeholder' => 'Pick a consultant', 'required']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Comments</div>
									</div>
									<div class="col-xs-8">
										{!! Form::textarea('comments', $shortlist->comments, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								@if ($previous_url != url('/shortlist/show-order/'.$shortlist->id) && $previous_url != '')
									{{ Form::hidden('previous_url', $previous_url) }}
								@endif
								<div class="col-xs-12" style="padding-top:15px;">
									@if (!isset($_GET['statuschange']))
										<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Update </button>
									@else
										@if ($_GET['statuschange'] == 'open')
											<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Reopen Shortlist</button>
										@else
											<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Close Shortlist</button>
										@endif
									@endif
								</div>
							</form>
						</div>
					</div>
				</div>
				@php
					$openviewing = false;
				@endphp
				@if (!isset($_GET['statuschange']))
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="nest-property-edit-label-features">
									<div class="col-xs-12" style="padding-left:0px;padding-right:0px;padding-bottom:5px;">
										<u>Viewings</u>
									</div>
								</div>
								<div class="clearfix"></div>
								@if (count($viewings) > 0)
									<div style="">
										<div class="nest-buildings-result-wrapper">
											@foreach ($viewings as $index => $v)
												<div class="row 
												@if ($index%2 == 1)
													nest-striped
												@endif
												">
													<div class="col-xs-6">
														<b>Date/Time:</b> {{ $v->v_date() }} {{ $v->getNiceTime() }}<br />
														<b>Status:</b> 
														@if ($v->status == 10)
															Closed
														@else
															Open
														@endif
													</div>
													<div class="col-xs-6">
														@if ($v->status == 0)
															@php
																$openviewing = true;
															@endphp
															<a class="nest-button nest-right-button btn btn-default" href="{{ url('viewing/show/'.$v->id.'?statuschange=close') }}">
																<i class="fa fa-btn fa-lock"></i>Close
															</a>
														@endif
														<a href="{{ url('/viewing/show/'.$v->id) }}" class="nest-button nest-right-button btn btn-default">
															<i class="fa fa-btn fa-pencil-square-o"></i>View
														</a>
													</div>
												</div>
											@endforeach
										</div>
									</div>
								@endif
								<div class="col-xs-12" style="padding-left:0px;padding-right:0px;padding-top:15px;">
									<a href="{{ url('/viewing/create/'.$shortlist->id) }}" class="nest-button nest-right-button btn btn-default">
										<i class="fa fa-btn fa-plus"></i>Add Viewing
									</a>
								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
			
			@if (!isset($_GET['statuschange']))
				@if (count($properties) > 0)
					@php
						$i = 0;
					@endphp
					
					
					<div class="nest-property-list-wrapper">
						<div class="row">
							<form id="propertylistform" action="{{ url('frontend/select') }}" method="post">
								{{ csrf_field() }}
								
								@php
									$curdistrict = -1;
								@endphp
								
								@foreach ($properties as $property)
									@if ($property->district_id != $curdistrict)
										<div class="clearfix"></div>
										<h3 class="nest-property-shortlist-district-h3">{{ $property->district_for_shortlist() }}</h3>
										@php
											$curdistrict = $property->district_id;
											$i = 0;
										@endphp
									@endif
									<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 nest-property-list-item">
										<div class="panel panel-default">
											<div class="nest-property-list-item-top">
												<div class="nest-property-list-item-top-checkbox">
													{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
												</div>
												<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
													{{ $property->shorten_building_name(0) }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-image-wrapper">
												<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
													@if(!empty($property->featured_photo_url))
														<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
													@else
														<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
													@endif
													
													<div class="nest-property-list-item-price-wrapper">
														<div class="nest-property-list-item-price">
															{!! $property->price_db_searchresults_new() !!}
														</div>
													</div>
												</a>
												<div class="nest-property-list-item-image-top-bar-left">
													@if (count($property->nest_photos) > 0)
														<div class="nest-property-list-item-nestpics-wrapper">
															<div class="nest-property-list-item-nestpics">
																<img src="/images/camera2.png" />
															</div>
														</div>
													@endif
												</div>
												<div class="nest-property-list-item-image-top-bar">
													@if($property->bonuscomm == 1)
														<div class="nest-property-list-item-bonus-wrapper">
															<div class="nest-property-list-item-bonus">
																$$$
															</div>
														</div>
													@endif
													@if($property->hot == 1)
														<div class="nest-property-list-item-hot-wrapper">
															<div class="nest-property-list-item-hot">
																HOT
															</div>
														</div>
													@endif
													@if($property->web)
														<div class="nest-property-list-item-web-wrapper">
															<div class="nest-property-list-item-web">
																<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																	WEB
																</a>
															</div>
														</div>
													@endif
													@if ($property->owner_id > 0)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																@if ($owner->corporatelandlord == 1)
																	<div class="nest-property-list-item-cp-wrapper">
																		<div class="nest-property-list-item-cp">
																			<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																				CL
																			</a>
																		</div>
																	</div>
																@endif
															@endforeach
														@endif
													@elseif ($property->poc_id == 3 || $property->poc_id == 4)
														@if ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																@if ($agent->corporatelandlord == 1)
																	<div class="nest-property-list-item-cp-wrapper">
																		<div class="nest-property-list-item-cp">
																			<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																				CL
																			</a>
																		</div>
																	</div>
																@endif
															@endforeach
														@endif
													@endif
												</div>
											</div>
											<div class="nest-property-list-item-bottom-address-wrapper">
												<div class="nest-property-list-item-bottom-address">
													{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
												</div>
												<div class="nest-property-list-item-bottom-unit">
													{{ $property->unit }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													Room(s)
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													@if ($property->bedroom_count() == 1)
														{{ $property->bedroom_count() }} bed, 
													@else
														{{ $property->bedroom_count() }} beds, 
													@endif
													@if ($property->bathroom_count() == 1)
														{{ $property->bathroom_count() }} bath
													@else
														{{ $property->bathroom_count() }} baths
													@endif
													
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													Size
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													{{ $property->property_netsize() }} / {{ $property->property_size() }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													@if ($property->poc_id ==  1)
														Landlord
													@elseif ($property->poc_id ==  2)
														Rep
													@elseif ($property->poc_id ==  3)
														Agency
													@else
														Contact
													@endif
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													@php
														$vendorshown = false;
													@endphp
													@if ($property->poc_id ==  1)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach		
															@php
																$vendorshown = true;
															@endphp
														@endif
													@elseif ($property->poc_id ==  2)
														@if ($property->getRep() && $property->getRep()->count() > 0)
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@elseif ($property->poc_id ==  3)
														@if ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@endif
													@if (!$vendorshown)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach														
															@if ($property->getRep() && $property->getRep()->count() > 0)
																@foreach ($property->getRep() as $rep)
																	{!! $rep->basic_info_property_search_linked() !!}
																@endforeach
															@endif
														@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
														@else
															-
														@endif
													@endif
												</div>
												<div class="clearfix"></div>
												@if ($property->poc_id == 1 && $vendorshown)
													@if ($property->getRep() && $property->getRep()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Rep
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif
												@endif
												@if ($property->poc_id == 2 && $vendorshown)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Owner
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif
												@endif
												@if ($property->poc_id == 3 && $vendorshown)
													@if ($property->getAgent() && $property->getAgent()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Agent
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getAgent() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif	
												@endif
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-features-wrapper">
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
													{{ $property->maidroom_count() }}
												</div>
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
													{{ $property->backend_search_outdoor() }}
												</div>
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
													{{ $property->carpark_count() }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-buttons-wrapper">
												<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
													<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-pencil-square-o"></i>Edit
													</a>
												</div>
												
												@if($property->type_id == 3)
													<div class="col-xs-6" style="padding-left:5px;padding-right:0px;padding-top:0px;">
														<?php $option = $property->shortlist_option->where('shortlist_id', $shortlist->id)->where('property_id', $property->id)->first();?>
														<input type="hidden" name="property_id" value="{{ $property->id }}">
														<button id="shortlist-option-{{ $shortlist->id }}" class="nest-button btn btn-default shortlist-option-button" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-toggle-on"></i>{{ $option->type() }}
														</button>
													</div>
												@else
													<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
														<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-dot-circle-o"></i>Show
														</a>
													</div>
												@endif
												
												<div class="clearfix"></div>
												<div class="col-xs-12" style="padding-left:0px;padding-top:10px;padding-right:0px;">
													<a class="nest-button btn btn-default" href="{{ url('/documents/'.$shortlist->id.'/docaddproperty/'.$property->id) }}" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-book"></i>Create / Edit Documents
													</a>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									@php
										$i++;
										if ($i%3 == 0){
											echo '<div class="hidden-xs hidden-sm hidden-lg clearfix"></div>';
										}
										if ($i%2 == 0){
											echo '<div class="hidden-lg hidden-md clearfix"></div>';
										}
										if ($i%4 == 0){
											echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
										}
									@endphp
									
								@endforeach
								
								{{ Form::hidden('default_type', '') }}
								{{ Form::hidden('action', 'select') }}
								{{ Form::hidden('shortlist_id', $shortlist->id) }}
							</form>
						</div>
					</div>
					
				@endif
					
				@if ($openviewing)
					<div class="panel panel-default nest-property-list-buttons">
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-2 col-xs-6">
										<button id="print_pdf" class="nest-button btn btn-default">
											<i class="fa fa-btn fa-print"></i>Print
										</button>
									</div>
									<div class="col-md-2 col-xs-6">
										<button id="print_pdf_agent" class="nest-button btn btn-default">
											<i class="fa fa-btn fa-print"></i>Print (Agent)
										</button>
									</div>
									<!-- <div class="col-md-2 col-xs-6">
										<button id="reorder_option" class="nest-button btn btn-default">
											<i class="fa fa-btn fa-arrows"></i>Reorder
										</button>
									</div>-->
									<div class="col-md-2 col-xs-6">
										<button id="delete_option" class="btn btn-danger" style="border-radius:0px;border:0px;">
											<i class="fa fa-btn fa-trash"></i>Delete
										</button>
									</div>
									<div class="col-md-6 col-xs-6">
										@if (count($viewings) > 0)
											@foreach ($viewings as $index => $v)
												@if ($v->status == 0)
													<a href="javascript:;" class="nest-button btn btn-default" onClick="addtoviewing({{ $v->id }});">
														<i class="fa fa-btn fa-plus"></i>Add to Viewing {{ $v->v_date() }} ({{ $v->getNiceTime() }})
													</a>
												@endif
											@endforeach
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>					
				@else
					<div class="panel panel-default nest-property-list-buttons">
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<button id="print_pdf" class="nest-button btn btn-default">
											<i class="fa fa-btn fa-print"></i>Print
										</button>
									</div>
									<div class="col-md-4 col-xs-6">
										<button id="print_pdf_agent" class="nest-button btn btn-default">
											<i class="fa fa-btn fa-print"></i>Print (Agent)
										</button>
									</div>
									<!-- <div class="col-md-3 col-xs-6">
										<button id="reorder_option" class="nest-button btn btn-default">
											<i class="fa fa-btn fa-arrows"></i>Reorder
										</button>
									</div> -->
									<div class="col-md-4 col-xs-6">
										<button id="delete_option" class="btn btn-danger" style="border-radius:0px;border:0px;">
											<i class="fa fa-btn fa-trash"></i>Delete
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
			@endif
</div>

@include('partials.property.save-shortlist')

@endsection

@section('footer-script')
<script type="text/javascript">

function addtoviewing(vid){
	var url = "{{ url('shortlist/addtoview/'.$shortlist->id) }}/"+vid;
	$('form#propertylistform').attr('action', url);
	$('form#propertylistform').attr('target', '_self');
	if ($('form input.property-option:checked').length > 0) {
		$('form#propertylistform').submit();
	}else{
		alert('Please choose a property to add to the next view');
	}
}

$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form input[type="checkbox"]').prop('checked', $(this).is(":checked"));
    });
    $('button#print_pdf').click(function(event){
        event.preventDefault();
        var url = "{{ url('shortlist/option/print/') }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#print_pdf_agent').click(function(event){
        event.preventDefault();
        var url = "{{ url('shortlist/option/printagent/' ) }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#delete_option').click(function(event){
        event.preventDefault();
        var url = "{{url('shortlist/option/delete/')}}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_self');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose items to delete.');
        }
        return false;
    });
    $('button#reorder_option').click(function(event){
        event.preventDefault();
        $('form#propertylistform').attr('target', '_blank');
        location.href =  "{{url('shortlist/show-order/' . $shortlist->id)}}";;
        return false;
    });
    $('button.shortlist-option-button').click(function(event){
        event.preventDefault();
        var url = "{{url('shortlist/option/toggle-type/')}}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_self');
        var property_id = $(this).parent().find('input[name=property_id]').val();
        $('form#propertylistform input[name=property_id]').val(property_id);
        $('form#propertylistform').submit();
        return false;
    });
});
</script>
@include('partials.shortlist-control-new')
@include('partials.property.save-shortlist-control-new')
@endsection