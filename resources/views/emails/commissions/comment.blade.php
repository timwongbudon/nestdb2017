<html>
    <head>
        <title>Comment added</title>
    </head>
    <body>
        <p>{{$user->name}} has added a comment to the following invoice: <a href="{{url('/commission/invoiceedit/'. $id)}}">{{url('/commission/invoiceedit/'. $id)}}</a></p>
        <p> Invoice Details: <br>
            
            @if($commission->consultant)
                Consultant:  {{ $commission->consultant->name }} <br>
            @endif
            
            Property: {{$property->name}} <br>
            
            @if($commission->salelease == 1)
                Type: Lease
            @else
                Type: Sale
            @endif
            <br>
        </p>
        <p>The comment is: {{$comment}}</p>
    </body>
</html>