@extends('layouts.app')

@section('content')

	<div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
			<h2>Information Officer Properties</h2>
		</div>
	</div>
	
	
	<!-- Current Clients -->
	@if (count($io_properties) > 0)
		<div>
			<div class="nest-buildings-result-wrapper">

			<div class="row 
				
						nest-striped
				
					">
						<div class="col-lg-3 col-md-2 col-sm-6">
						<b>Information Officer</b>
						</div>
						<div class="col-lg-3 col-md-2 col-sm-6">
						<b>Property</b>
						</div>                       
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Direct Land Lord</b>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Asking Rent</b>
						</div>						
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>	Action</b>
						</div>
						<div class="clearfix"></div>
					</div>


				@foreach ($io_properties as $index => $inv)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-2 col-sm-6">
                        {{ $inv->firstname }}  {{ $inv->lastname }}
							
						</div>                      
                      
						<div class="col-lg-3 col-md-2 col-sm-6">						
						    <a href="{{url('property/edit/'.$inv->property_id)}}">{{ $inv->name }}</a>
						</div>	                      

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            @if( $inv->direct_landlord == 1)
                                Yes
                            @else
                                No
                             @endif                           							
						</div>		

                        <div class="col-lg-2 col-md-2 col-sm-6">                        
                            HK$&nbsp;{{ number_format($inv->asking_rent, 0, '.', ',') }}								
						</div>	

                        <div class="col-lg-2 col-md-2 col-sm-6">                        
                           <a href="/property/edit/{{$inv->id}}" class="nest-button btn btn-primary">View</a>								
						</div>	

                 	
						<div class="clearfix"></div>
					</div>
				@endforeach

				<div class="nest-new ">
			
				</div>
			</div>
			
        </div>
        @else
        <div>No data available.</div>
	@endif

@endsection
