@extends('layouts.app')

@section('content')
    <style>
        .index-image {
            float: left;
            display: block; 
            background-size: 80px 80px; 
            width: 80px; 
            height: 80px;
        }
        .property-table strong {
            font-size: 15px;
        }
    </style>


@include('partials.property.search-form')

            <div class="panel panel-default" style="background-color: #f5f5f5;">
                <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-10" style="border-right: 1px solid #d8d8d8;">
                                <label for="property-shortlist" class="control-label" style="float: left; padding: 5px 10px 5px 0;">Save to shortlist</label>
                                <input type="text" name="client-name" id="client-name" class="form-control" value="" placeholder="Type client name" style="width: 60%;">
                                <span id="client-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the shortlist name"><i class="fa fa-btn fa-close"></i></button></span>
                                <span id="property-shortlist-message" style="color: red; font-style: italic; display: none"></span>
                                <span id="property-shortlist-action" style="font-style: italic; display: none"></span>
                            </div>
                            <div class="col-sm-2 text-right">
                                <a class="btn btn-primary" href="{{ url('property/create') }}">
                                    <i class="fa fa-btn fa-plus"></i>Add Property
                                </a>
                            </div>
                        </div>
                </div>
            </div>
    
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Properties
                    </div>

                    <div class="panel-body">
                    <form id="property-list" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table class="table table-striped property-table">
                            <thead>
                                {{-- @if(Request::session()->has('shortlist_id'))<th style="width: 5%">&nbsp;</th>@endif --}}
                                <th style="width: 5%">&nbsp;</th>
                                <th style="width: 5%">ID</th>
                                @if(NestPreference::isShowPhoto())<th>Photo</th>@endif
                                <th style="max-width: 400px">Property</th>
                                <th>Type</th>
                                <!-- <th style="width: 8%">Unit</th>
                                <th>Size</th>
                                <th>Price (HK$)</th> -->
                                <th style="width: 8%">&nbsp;</th>
                                <th style="width: 8%">&nbsp;</th>
                                <!-- <th style="width: 8%">&nbsp;</th> -->
                            </thead>
                            <tbody>
                                @foreach ($properties as $property)
                                    <tr>
                                        {{-- @if(Request::session()->has('shortlist_id'))<td class="table-text"><div>{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!} </div></td>@endif --}}
                                        <td class="table-text"><div>{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}</div></td>
                                        <td class="table-text"><div>{{ $property->nest_id() }}
                                        @if($property->external_id)({{$property->external_id}})@endif
                                        </div></td>
                                        @if(NestPreference::isShowPhoto())
                                        <td class="table-text"> 
                                            @if(!empty($property->featured_photo_url))
                                            <img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}" alt="" width="300" />
                                            @endif
                                        </td>
                                        @endif
                                        <td class="table-text"><div>
                                            <strong>{{ $property->shorten_building_name(0) }}</strong>
                                            @if($property->hot == 1)
                                                <img src="{{ url('hot.gif')}} " alt="">
                                            @endif
                                            <br/>
                                            <?php $rep_ids = $property->rep_ids?explode(',',$property->rep_ids):array(); ?>
                                            {{ $property->fix_address1() }}<br/><hr style="margin: 5px 0;">

                                            @if($property->unit)
                                                Unit: {{ $property->unit }}<br/>
                                            @endif
                                            
                                            Asking Price: {{ $property->price_db_searchresults() }}<br/>

                                            @if($property->bedroom_count())
                                                Bedrooms: {{ $property->bedroom_count() }}<br/>
                                            @endif

                                            @if($property->bathroom_count())
                                                Bathrooms: {{ $property->bathroom_count() }}<br/>
                                            @endif
                                            
                                            @if($property->show_size())
                                                Size: {{ $property->show_size() }}<br/>
                                            @endif

                                            @if($property->size)
                                                Size: {{ $property->property_size() }}<br/>
                                            @endif

                                            @if($property->poc_id)
                                                Point of contact: {{ $property->poc() }}<br/>
                                            @endif
                                            
                                            @if(!empty($property['building']->tel))
                                                Mgmt Office tel: {{$property['building']->tel}}<br/>
                                            @endif
                                            
                                            @if(!empty($property['owner']))
                                                Owner: {{$property['owner']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['owner']) && !empty($property['reps']))
                                                @foreach($property['reps'] as $i => $rep)
                                                Rep {{$i+1}}: {{$rep->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property['agent']))
                                                Agency: {{$property['agent']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['agent']) && !empty($property['agents']))
                                                @foreach($property['agents'] as $i => $agent)
                                                Agent {{$i+1}}: {{$agent->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property->comments) && $property->comments != '[]')
                                                Last Comment: {{$property->show_last_comment()}}
                                            @endif

                                            @if(empty($property['agent']) && empty($property['owner']) && !empty($property->contact_info))
                                                Contact Info: {{$property->contact_info}}<br/>
                                            @endif

                                            @if(!empty($property->available_date()))
                                            Available Date: {{ $property->available_date() }}
                                            @endif
                                            <hr style="margin: 5px 0;">
                                            {{ $property->show_timestamp() }}
                                        </div></td>
                                        <td class="table-text"><div>{{ $property->type() }}</div></td>
                                        <!-- <td class="table-text"><div>{{ $property->unit }}</div></td>
                                        <td class="table-text"><div>{{ $property->property_size() }}</div></td>
                                        <td class="table-text"><div>{{ $property->price() }}</div></td> -->

                                        <!-- Property Delete Button -->
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('property/show/'.$property->id) }}">
                                                <i class="fa fa-btn fa-info-circle"></i>View
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('property/edit/'.$property->id) }}">
                                                <i class="fa fa-btn fa-pencil-square-o"></i>Edit
                                            </a>
                                        </td>
                                        <!-- Property Delete Button -->
                                        <!-- <td>
                                            <form action="{{url('property/' . $property->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-property-{{ $property->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ Form::hidden('default_type', '') }}
                        {{ Form::hidden('current_url', $current_url) }}
                    </form>
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $properties->currentPage() }} of {{ $properties->lastPage() }} (Total: {{ $properties->total() }} items)</div>
                        {{ $properties->links() }}  
                    </div>
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button id="print_pdf" class="btn btn-primary">
                                <i class="fa fa-btn fa-print"></i>Print (Default: Lease)
                            </button>
                            &nbsp;&nbsp;
                            <button id="print_pdf_sale" class="btn btn-primary">
                                <i class="fa fa-btn fa-print"></i>Print (Default: Sale)
                            </button>
                            &nbsp;&nbsp;
                            <button id="set_hot_properties" class="btn btn-primary">
                                <i class="fa fa-btn fa-print"></i>Set Hot Properties
                            </button>
                            &nbsp;&nbsp;
                            <button id="unset_hot_properties" class="btn btn-primary">
                                <i class="fa fa-btn fa-print"></i>Unset Hot Properties
                            </button>
                            &nbsp;&nbsp;
                            <button id="print_pdf_agent" class="btn btn-primary">
                                <i class="fa fa-btn fa-print"></i>Print (Agent: Lease)
                            </button>
                            &nbsp;&nbsp;
                            <button id="print_pdf_sale_agent" class="btn btn-primary">
                                <i class="fa fa-btn fa-print"></i>Print (Agent: Sale)
                            </button>
                        </div>
                    </div>
                </div>
            </div>

@include('partials.property.save-shortlist')

@endsection

@section('footer-script')
<script type="text/javascript">
$(document).ready(function(){
    $('button#print_pdf, button#print_pdf_sale').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#property-list');
        var url = "{{ url('property/option/print/' ) }}";
        proprerty_list_form.attr('action', url);
        proprerty_list_form.attr('target', '_blank');

        var default_type = ($(this).attr('id') == 'print_pdf')?'lease':'sale';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#set_hot_properties, button#unset_hot_properties').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#property-list');
        var url = "{{ url('property/option/set-as-hot/' ) }}";
        proprerty_list_form.attr('action', url);

        var default_type = ($(this).attr('id') == 'set_hot_properties')?'1':'0';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
            // console.info(proprerty_list_form.serialize());
            // console.info('test!!!');
        } else {
            alert('Please choose property to mark as hot properties.');
        }
        return false;
    });
    $('button#print_pdf_agent, button#print_pdf_sale_agent').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#property-list');
        var url = "{{ url('property/option/printagent/' ) }}";
        proprerty_list_form.attr('action', url);
        proprerty_list_form.attr('target', '_blank');

        var default_type = ($(this).attr('id') == 'print_pdf_agent')?'lease':'sale';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
});
</script>
@include('partials.shortlist-control')
@include('partials.property.save-shortlist-control')
@include('partials.property.building-modal-control')
@endsection