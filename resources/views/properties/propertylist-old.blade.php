@extends('layouts.app')

@section('content')
    <style>
        .index-image {
            float: left;
            display: block; 
            background-size: 80px 80px; 
            width: 80px; 
            height: 80px;
        }
        .property-table strong {
            font-size: 15px;
        }
    </style>

<div class="nest-new container">
	<div class="panel panel-default nest-property-list-heading">
		<div class="panel-body">
			<form action="{{ url('/listsearch') }}" method="get">
				<div class="row">
					<div class="col-sm-6" style="padding-bottom:15px;">
						<input type="text" placeholder="Property Name, Property ID, Address, etc." name="s" id="simple-search" class="form-control" />
					</div>
					<div class="col-sm-3 col-xs-6" style="padding-bottom:15px;">
						<button type="submit" class="nest-button btn btn-default nest-property-list-search-button">Search</button>
					</div>
					<div class="col-sm-3 col-xs-6 text-right">
						@if (!$showadvsearch)
							<a href="javascript:;" id="advanced-search-link" onClick="showAdvancedSearch();" style="display:inline-block;padding-top:8px;">Advanced Search</a>
							<a href="javascript:;" id="advanced-search-link-hide" onClick="hideAdvancedSearch();" style="display:none;padding-top:8px;">Hide Search</a>
						@endif
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="advanced-search" class="panel panel-default" 
	@if (!$showadvsearch)
			style="display:none;"
	@endif
	>
		<div class="panel-body">	
			@include('partials.property.advanced-search-form', array('template_type'=>'frontend'))
		</div>
	</div>
	
	<script>
		function showAdvancedSearch(){
			jQuery('#advanced-search').css('display', 'block');
			jQuery('#advanced-search-link').css('display', 'none');
			jQuery('#advanced-search-link-hide').css('display', 'inline-block');
		}
		function hideAdvancedSearch(){
			jQuery('#advanced-search').css('display', 'none');
			jQuery('#advanced-search-link').css('display', 'inline-block');
			jQuery('#advanced-search-link-hide').css('display', 'none');
		}
	</script>
	

	
    
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-default nest-property-list-heading">
                    <div class="panel-body">
						<div class="row">
							<div class="col-sm-6">
								<!-- @if ($properties->total() == 1)
									<h4>{{ $properties->total() }} property found</h4>
								@else
									<h4>{{ $properties->total() }} properties found</h4>
								@endif -->
								<div class="form-group">
									<div>
										<input type="text" name="client-name" id="client-name" class="form-control" style="width:100% !important;" placeholder="Type client name to save to shortlist" />
										<span id="client-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the shortlist name"><i class="fa fa-btn fa-close"></i></button></span>
										<span id="property-shortlist-message" style="color: red; font-style: italic; display: none"></span>
										<span id="property-shortlist-action" style="font-style: italic; display: none"></span>
									</div>
								</div>
							</div>
							<div class="col-sm-6 text-right">
								<div class="nest-property-list-select-all">
									<label for="options_ids_all">Select All</label>
									{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!}
								</div>
                                <a class="nest-button btn btn-default nest-property-list-search-button" href="{{ url('property/create') }}">
                                    <i class="fa fa-btn fa-plus"></i>Add Property
                                </a>
							</div>
							<div class="clearfix"></div>
						</div>
                    </div>
				</div>
				
				@php
					$i = 0;
				@endphp
				
				
				
				<div class="nest-property-list-wrapper">
					<div class="row">
						<form id="propertylistform" action="{{ url('frontend/select') }}" method="post">
							{{ csrf_field() }}
							
							@foreach ($properties as $property)
								<div class="col-lg-3 col-sm-6 col-xs-12 nest-property-list-item">
									<div class="panel panel-default">
										<div class="nest-property-list-item-top">
											<div class="nest-property-list-item-top-checkbox">
												{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
											</div>
											<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
												{{ $property->shorten_building_name(0) }}
											</div>
											<div class="clearfix"></div>
										</div>
										<!-- <div class="ribbon-mark ribbon-default absolute right">
											<span class="ribbon">
												<span class="text">
													@if($property->bedroom_count())
														<i class="fa fa-fw icon-bed"></i> {{ $property->bedroom_count() }}
													@endif
													@if($property->bathroom_count())
														<i class="fa fa-fw icon-toilet"></i> {{ $property->bathroom_count() }}
													@endif
												</span>
											</span>
										</div>
										@if($property->web)
											<div class="ribbon-mark ribbon-primary absolute left">
												<span class="ribbon">
													<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
														<span class="text"><i class="fa fa-globe"></i></span>
													</a>
												</span>
											</div>
										@endif 
										@if($property->hot == 1)
											<img src="{{ url('hot.gif')}} " alt="">
										@endif -->
										<div class="nest-property-list-item-image-wrapper">
											<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '{{ str_replace("'", "\'", $searchterm) }}')">
												@if(!empty($property->featured_photo_url))
													<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
												@else
													<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
												@endif
												
												<div class="nest-property-list-item-price-wrapper">
													<div class="nest-property-list-item-price">
														{!! $property->price_db_searchresults_new() !!}
													</div>
												</div>
											</a>
											<div class="nest-property-list-item-image-top-bar">
												@if($property->hot == 1)
													<div class="nest-property-list-item-hot-wrapper">
														<div class="nest-property-list-item-hot">
															HOT
														</div>
													</div>
												@endif
												<!-- @if($property->leased == 1)
													<div class="nest-property-list-item-leased-wrapper">
														<div class="nest-property-list-item-leased">
															LEASED
														</div>
													</div>
												@endif
												@if($property->sold == 1)
													<div class="nest-property-list-item-sold-wrapper">
														<div class="nest-property-list-item-sold">
															SOLD
														</div>
													</div>
												@endif -->
												@if($property->web)
													<div class="nest-property-list-item-web-wrapper">
														<div class="nest-property-list-item-web">
															<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																WEB
															</a>
														</div>
													</div>
												@endif
											</div>
										</div>
										<div class="nest-property-list-item-bottom-address-wrapper">
											<div class="nest-property-list-item-bottom-address">
												{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
											</div>
											<div class="nest-property-list-item-bottom-unit">
												{{ $property->unit }}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												Room(s)
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@if ($property->bedroom_count() == 1)
													{{ $property->bedroom_count() }} bed, 
												@else
													{{ $property->bedroom_count() }} beds, 
												@endif
												@if ($property->bathroom_count() == 1)
													{{ $property->bathroom_count() }} bath
												@else
													{{ $property->bathroom_count() }} baths
												@endif
												
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												Size
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												{{ $property->property_netsize() }} / {{ $property->property_size() }}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												@if ($property->poc_id ==  1)
													Landlord
												@elseif ($property->poc_id ==  2)
													Rep
												@elseif ($property->poc_id ==  3)
													Agency
												@else
													Contact
												@endif
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@if ($property->poc_id ==  1)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														@foreach ($property->getOwner() as $owner)
															{!! $owner->basic_info_property_search_linked() !!}
														@endforeach
													@else
														-
													@endif
												@elseif ($property->poc_id ==  2)
													@if ($property->getRep() && $property->getRep()->count() > 0)
														@foreach ($property->getRep() as $rep)
															{!! $rep->basic_info_property_search_linked() !!}
														@endforeach
													@else
														-
													@endif
												@elseif ($property->poc_id ==  3)
													@if ($property->getAgency() && $property->getAgency()->count() > 0)
														@foreach ($property->getAgency() as $agent)
															{!! $agent->basic_info_property_search_linked() !!}
														@endforeach
													@else
														-
													@endif
												@else
													-
												@endif
											</div>
											<div class="clearfix"></div>
											@if ($property->poc_id == 1)
												@if ($property->getRep() && $property->getRep()->count() > 0)
													<div class="nest-property-list-item-bottom-rooms-left">
														Rep
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@foreach ($property->getRep() as $rep)
															{!! $rep->basic_info_property_search_linked() !!}
														@endforeach
													</div>
												@endif
											@endif
											@if ($property->poc_id == 2)
												@if ($property->getOwner() && $property->getOwner()->count() > 0)
													<div class="nest-property-list-item-bottom-rooms-left">
														Owner
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@foreach ($property->getOwner() as $owner)
															{!! $owner->basic_info_property_search_linked() !!}
														@endforeach
													</div>
												@endif
											@endif
											@if ($property->poc_id == 3)
												@if ($property->getAgent() && $property->getAgent()->count() > 0)
													<div class="nest-property-list-item-bottom-rooms-left">
														Agent
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@foreach ($property->getAgent() as $agent)
															{!! $agent->basic_info_property_search_linked() !!}
														@endforeach
													</div>
												@endif	
											@endif
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-features-wrapper">
											<div class="col-xs-4">
												<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
												{{ $property->maidroom_count() }}
											</div>
											<div class="col-xs-4">
												<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
												{{ $property->backend_search_outdoor() }}
											</div>
											<div class="col-xs-4">
												<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
												{{ $property->carpark_count() }}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-buttons-wrapper">
											<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
												<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
													<i class="fa fa-btn fa-pencil-square-o"></i>Edit
												</a>
											</div>
											@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer'])
												<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
													@if($property->web)
														<a class="nest-button btn btn-default" target="_blank" href="{{ url('property/feathome/'.$property->id) }}" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-globe"></i>Feature
														</a>	
													@endif
												</div>
											@else
												<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
													<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '{{ str_replace("'", "\'", $searchterm) }}')" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-dot-circle-o"></i>Show
													</a>
												</div>
											@endif
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@php
									$i++;
									if ($i%2 == 0){
										echo '<div class="hidden-md hidden-lg clearfix"></div>';
									}
									if ($i%2 == 0){
										echo '<div class="hidden-lg hidden-sm hidden-xs clearfix"></div>';
									}
									if ($i%4 == 0){
										echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
									}
								@endphp
								
							@endforeach
							
							{{ Form::hidden('default_type', '') }}
							{{ Form::hidden('action', 'select') }}
						</form>
					</div>
				</div>
				
				@if ($properties->lastPage() > 1)
					<div class="panel panel-default nest-properties-footer">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									{{ $properties->links() }}  
								</div>
							</div>
						</div>
					</div>
				@endif
            @endif
	
	@include('partials.property.save-shortlist')

            <div class="panel panel-default nest-property-list-buttons">
                <div class="panel-body">
					<div class="form-group">
						<div class="row">
							@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer'])
								<div class="col-md-2 col-sm-4 col-xs-6">
									<button id="print_pdf" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Lease
									</button>
								</div>
								<div class="col-md-2 col-sm-4 col-xs-6">
									<button id="print_pdf_sale" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Sale
									</button>
								</div>
								<div class="col-md-2 col-sm-4 col-xs-6">
									<button id="print_pdf_agent" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Lease (Agent)
									</button>
								</div>
								<div class="col-md-2 col-sm-4 col-xs-6">
									<button id="print_pdf_sale_agent" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Sale (Agent)
									</button>
								</div>
								<div class="col-md-2 col-sm-4 col-xs-6">
									<button id="add_frontend_property" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-plus"></i>Website
									</button>
								</div>
								<div class="col-md-2 col-sm-4 col-xs-6">
									<button id="remove_frontend_property" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-minus"></i>Website
									</button>
								</div>
							@else
								<div class="col-md-3 col-xs-6">
									<button id="print_pdf" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Lease
									</button>
								</div>
								<div class="col-md-3 col-xs-6">
									<button id="print_pdf_sale" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Sale
									</button>
								</div>
								<div class="col-md-3 col-xs-6">
									<button id="print_pdf_agent" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Lease (Agent)
									</button>
								</div>
								<div class="col-md-3 col-xs-6">
									<button id="print_pdf_sale_agent" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-print"></i>Sale (Agent)
									</button>
								</div>
							@endif
						</div>
					</div>
                </div>
            </div>
</div>
@endsection

@section('footer-script')
@include('partials.property.building-modal-control')
<script type="text/javascript">
function submit_action(action) {
	if ($('form#propertylistform input.property-option:checked').length > 0) {
		if (action == 'deselect') {
			$('form#propertylistform input[name=action]').val('deselect');
		}
		$('form#propertylistform').submit();
	} else {
		alert('Please choose property to put into frontend property list.');
	}
}
$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form#propertylistform input').prop('checked', $(this).is(":checked"));
    });

    $('button#add_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('select');
		
		proprerty_list_form.submit();
    });
    $('button#remove_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('deselect');
		
		proprerty_list_form.submit();
    });
    $('button#print_pdf, button#print_pdf_sale').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#propertylistform');
        var url = "{{ url('property/option/print/' ) }}";
        proprerty_list_form.attr('action', url);
        proprerty_list_form.attr('target', '_blank');

        var default_type = ($(this).attr('id') == 'print_pdf')?'lease':'sale';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    /*$('button#set_hot_properties, button#unset_hot_properties').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#propertylistform');
        var url = "{{ url('property/option/set-as-hot/' ) }}";
        proprerty_list_form.attr('action', url);

        var default_type = ($(this).attr('id') == 'set_hot_properties')?'1':'0';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to mark as hot properties.');
        }
        return false;
    });*/
    $('button#print_pdf_agent, button#print_pdf_sale_agent').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#propertylistform');
        var url = "{{ url('property/option/printagent/' ) }}";
        proprerty_list_form.attr('action', url);
        proprerty_list_form.attr('target', '_blank');
		
        var default_type = ($(this).attr('id') == 'print_pdf_agent')?'lease':'sale';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
});
</script>
@include('partials.shortlist-control-new')
@include('partials.property.save-shortlist-control-new')
@endsection