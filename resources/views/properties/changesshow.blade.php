
@if (count($changes) == 0)
	No changes found
@else
	@foreach ($changes as $change)
		<div class="media media-comment">
			<div class="media-left">
				<img src="{{ url($change['pic']) }}" class="img-circle media-object" width="60px" style="border:1px solid #cccccc;" />
			</div>
			<div class="media-body message text-left">
				<u>{{ $change['name'] }}</u> <small class="text-muted">{{ $change['datum'] }}</small>
				<br />
				{!! $change['content'] !!}
			</div>
		</div>
		<div class="clearfix"></div>
	@endforeach
@endif