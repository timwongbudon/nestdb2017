
<div class="nest-property-pic-slider-wrapper">
	<div id="show-pics-slider-big">
		@foreach($photos as $group=>$section)
			@if ($section->count() > 0)
				@foreach($section as $index=>$file)
					<img id="bigimage{{ $file->id }}" src="{{ $file->getShowUrlModal() }}" height="600" />
				@endforeach
			@endif
		@endforeach
	
	</div>
	<div id="show-pics-slider">
		@foreach($photos as $group=>$section)
			@if ($section->count() > 0)
				@foreach($section as $index=>$file)
					<div><a href="{{ $file->getShowUrl() }}" target="_blank"><img src="{{ $file->getShowUrlModal() }}" height="200" /></a></div>
				@endforeach
			@endif
		@endforeach
	</div>
</div>