@extends('layouts.app')

@section('content')
    <style>
        .index-image {
            float: left;
            display: block; 
            background-size: 80px 80px; 
            width: 80px; 
            height: 80px;
        }
        .property-table strong {
            font-size: 15px;
        }
    </style>

@php
	if (empty($showtype)){
		$showtype = 'standard';
	}
@endphp

<div class="nest-new container">
	@if (empty($hidesearch) || !$hidesearch)
			@include('partialsnew.property-search-form')
		
	@endif
	
    
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-default nest-property-list-heading">
                    <div class="panel-body">
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<div>
										<input type="text" name="client-name" id="client-name" class="form-control" style="width:100% !important;" placeholder="Type client name to save to shortlist" />
										<span id="client-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the shortlist name"><i class="fa fa-btn fa-close"></i></button></span>
										<span id="property-shortlist-message" style="color: red; font-style: italic; display: none"></span>
										<span id="property-shortlist-action" style="font-style: italic; display: none"></span>
									</div>
								</div>
							</div>
							<div class="col-sm-4 text-center">
								<div style="padding-top:10px;"><b>
									@if ($properties->total() <= 36)
										Properties {{ ($properties->currentPage()-1)*36+1 }} to {{ $properties->total() }} of {{ $properties->total() }}
									@elseif ($properties->currentPage()*36 > $properties->total())
										Properties {{ ($properties->currentPage()-1)*36+1 }} to {{ $properties->total() }} of {{ $properties->total() }}
									@else
										Properties {{ ($properties->currentPage()-1)*36+1 }} to {{ $properties->currentPage()*36 }} of {{ $properties->total() }}
									@endif
								</b></div>
							</div>
							@if ($showtype == 'main')
								<div class="col-sm-2 text-right">
									<div class="nest-property-list-select-all">
										<div style="padding-top:7px;">
											<label for="options_ids_all">Select All</label>
											{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!}
										</div>
									</div>
								</div>
								<div class="col-sm-2 text-right">
									<div class="nest-property-list-sort">
										<div style="padding-top:0px;">
											<label for="sort_by" style="padding-top:10px;">Sort By</label>
											@php
												$sort_options = array(
													1 => 'Relevance',
													2 => 'Last Update',
													3 => 'Price'
												);
											@endphp
											{!! Form::select('sort_by', $sort_options, $input->sort_by_id, ['id'=>'property_sort_by', 'class'=>'form-control']); !!}
										</div>
									</div>
								</div>
							@else
								<div class="col-sm-4 text-right">
									<div class="nest-property-list-select-all">
										<div style="padding-top:7px;">
											<label for="options_ids_all">Select All</label>
											{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!}
										</div>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
                    </div>
				</div>
				
				@php
					$i = 0;
				@endphp
				
				
				
				<div class="nest-property-list-wrapper">
					<div class="row">
						<form id="propertylistform" action="{{ url('frontend/select') }}" method="post">
							{{ csrf_field() }}
							
							@foreach ($properties as $property)
								<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 nest-property-list-item">
									<div class="panel panel-default">
										<div class="nest-property-list-item-top">
											<div class="nest-property-list-item-top-checkbox">
												{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
											</div>
											<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
												{{ $property->shorten_building_name(0) }}
											</div>
											<div class="clearfix"></div>
										</div>
										<!-- <div class="ribbon-mark ribbon-default absolute right">
											<span class="ribbon">
												<span class="text">
													@if($property->bedroom_count())
														<i class="fa fa-fw icon-bed"></i> {{ $property->bedroom_count() }}
													@endif
													@if($property->bathroom_count())
														<i class="fa fa-fw icon-toilet"></i> {{ $property->bathroom_count() }}
													@endif
												</span>
											</span>
										</div>
										@if($property->web)
											<div class="ribbon-mark ribbon-primary absolute left">
												<span class="ribbon">
													<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
														<span class="text"><i class="fa fa-globe"></i></span>
													</a>
												</span>
											</div>
										@endif 
										@if($property->hot == 1)
											<img src="{{ url('hot.gif')}} " alt="">
										@endif -->
										<div class="nest-property-list-item-image-wrapper">
											<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
												@if(!empty($property->featured_photo_url))
													<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
												@else
													<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
												@endif
												
												@if ($showtype == 'upcleases')
													<div class="nest-property-list-item-avdate-wrapper">
														<div class="nest-property-list-item-avdate">
															{{ $property->available_date() }}
														</div>
													</div>
												@endif
												
												<div class="nest-property-list-item-price-wrapper">
													<div class="nest-property-list-item-price">
														{!! $property->price_db_searchresults_new() !!}
													</div>
												</div>
											</a>
											<div class="nest-property-list-item-image-top-bar-left">
												@if (count($property->nest_photos) > 0)
													<div class="nest-property-list-item-nestpics-wrapper">
														<div class="nest-property-list-item-nestpics">
															<img src="/images/camera2.png" />
														</div>
													</div>
												@endif
											</div>
											<div class="nest-property-list-item-image-top-bar">
												@if($property->bonuscomm == 1)
													<div class="nest-property-list-item-bonus-wrapper">
														<div class="nest-property-list-item-bonus">
															$$$
														</div>
													</div>
												@endif
												@if($property->hot == 1)
													<div class="nest-property-list-item-hot-wrapper">
														<div class="nest-property-list-item-hot">
															HOT
														</div>
													</div>
												@endif
												<!-- @if($property->leased == 1)
													<div class="nest-property-list-item-leased-wrapper">
														<div class="nest-property-list-item-leased">
															LEASED
														</div>
													</div>
												@endif
												@if($property->sold == 1)
													<div class="nest-property-list-item-sold-wrapper">
														<div class="nest-property-list-item-sold">
															SOLD
														</div>
													</div>
												@endif -->
												@if($property->web)
													<div class="nest-property-list-item-web-wrapper">
														<div class="nest-property-list-item-web">
															<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																WEB
															</a>
														</div>
													</div>
												@endif
												@if ($property->owner_id > 0)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														@foreach ($property->getOwner() as $owner)
															@if ($owner->corporatelandlord == 1)
																<div class="nest-property-list-item-cp-wrapper">
																	<div class="nest-property-list-item-cp">
																		<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																			CL
																		</a>
																	</div>
																</div>
															@endif
														@endforeach
													@endif
												@elseif ($property->poc_id == 3 || $property->poc_id == 4)
													@if ($property->getAgency() && $property->getAgency()->count() > 0)
														@foreach ($property->getAgency() as $agent)
															@if ($agent->corporatelandlord == 1)
																<div class="nest-property-list-item-cp-wrapper">
																	<div class="nest-property-list-item-cp">
																		<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																			CL
																		</a>
																	</div>
																</div>
															@endif
														@endforeach
													@endif
												@endif
											</div>
										</div>
										<div class="nest-property-list-item-bottom-address-wrapper">
											<div class="nest-property-list-item-bottom-address">
												{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
											</div>
											<div class="nest-property-list-item-bottom-unit">
												{{ $property->unit }}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												Room(s)
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@if ($property->bedroom_count() == 1)
													{{ $property->bedroom_count() }} bed, 
												@else
													{{ $property->bedroom_count() }} beds, 
												@endif
												@if ($property->bathroom_count() == 1)
													{{ $property->bathroom_count() }} bath
												@else
													{{ $property->bathroom_count() }} baths
												@endif
												
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												Size
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												{{ $property->property_netsize() }} / {{ $property->property_size() }}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												@if ($property->poc_id ==  1)
													Landlord
												@elseif ($property->poc_id ==  2)
													Rep
												@elseif ($property->poc_id ==  3)
													Agency
												@else
													Contact
												@endif
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@php
													$vendorshown = false;
												@endphp
												@if ($property->poc_id ==  1)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														@foreach ($property->getOwner() as $owner)
															{!! $owner->basic_info_property_search_linked() !!}
														@endforeach
														@php
															$vendorshown = true;
														@endphp
													@endif
												@elseif ($property->poc_id ==  2)
													@if ($property->getRep() && $property->getRep()->count() > 0)
														@foreach ($property->getRep() as $rep)
															{!! $rep->basic_info_property_search_linked() !!}
														@endforeach
														@php
															$vendorshown = true;
														@endphp
													@endif
												@elseif ($property->poc_id ==  3)
													@if ($property->getAgency() && $property->getAgency()->count() > 0)
														@foreach ($property->getAgency() as $agent)
															{!! $agent->basic_info_property_search_linked() !!}
														@endforeach
														@php
															$vendorshown = true;
														@endphp
													@endif
												@endif
												@if (!$vendorshown)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														@foreach ($property->getOwner() as $owner)
															{!! $owner->basic_info_property_search_linked() !!}
														@endforeach														
														@if ($property->getRep() && $property->getRep()->count() > 0)
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
														@endif
													@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
														@foreach ($property->getAgency() as $agent)
															{!! $agent->basic_info_property_search_linked() !!}
														@endforeach
													@else
														-
													@endif
												@endif
											</div>
											<div class="clearfix"></div>
											@if ($property->poc_id == 1 && $vendorshown)
												@if ($property->getRep() && $property->getRep()->count() > 0)
													<div class="nest-property-list-item-bottom-rooms-left">
														Rep
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@foreach ($property->getRep() as $rep)
															{!! $rep->basic_info_property_search_linked() !!}
														@endforeach
													</div>
												@endif
											@endif
											@if ($property->poc_id == 2 && $vendorshown)
												@if ($property->getOwner() && $property->getOwner()->count() > 0)
													<div class="nest-property-list-item-bottom-rooms-left">
														Owner
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@foreach ($property->getOwner() as $owner)
															{!! $owner->basic_info_property_search_linked() !!}
														@endforeach
													</div>
												@endif
											@endif
											@if ($property->poc_id == 3 && $vendorshown)
												@if ($property->getAgent() && $property->getAgent()->count() > 0)
													<div class="nest-property-list-item-bottom-rooms-left">
														Agent
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@foreach ($property->getAgent() as $agent)
															{!! $agent->basic_info_property_search_linked() !!}
														@endforeach
													</div>
												@endif	
											@endif
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-rooms-wrapper">
											<div class="nest-property-list-item-bottom-rooms-left">
												Available Date
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@if (date('d/m/Y') == $property->available_date())
													Today
												@elseif (date('d/m/Y', (time()-24*60*60)) == $property->available_date())
													Yesterday
												@else
													{{ $property->available_date() }}
												@endif
											</div>
											<div class="clearfix"></div>
											<div class="nest-property-list-item-bottom-rooms-left">
												Last Updated
											</div>
											<div class="nest-property-list-item-bottom-rooms-right">
												@if (date('d/m/Y') == $property->get_nice_updated_at())
													Today
												@elseif (date('d/m/Y', (time()-24*60*60)) == $property->get_nice_updated_at())
													Yesterday
												@else
													{{ $property->get_nice_updated_at() }}
												@endif
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-features-wrapper">
											<div class="col-xs-4">
												<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
												{{ $property->maidroom_count() }}
											</div>
											<div class="col-xs-4">
												<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
												{{ $property->backend_search_outdoor() }}
											</div>
											<div class="col-xs-4">
												<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
												{{ $property->carpark_count() }}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="nest-property-list-item-bottom-buttons-wrapper">
											<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
												<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
													<i class="fa fa-btn fa-pencil-square-o"></i>Edit
												</a>
											</div>
											@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer'])
												<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
													@if($property->web)
														<a class="nest-button btn btn-default" target="_blank" href="{{ url('property/feathome/'.$property->id) }}" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-globe"></i>Feature
														</a>	
													@endif
												</div>
											@else
												<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
													<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '{{ str_replace("'", "\'", $searchterm) }}')" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-dot-circle-o"></i>Show
													</a>
												</div>
											@endif
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@php
									$i++;
									if ($i%3 == 0){
										echo '<div class="hidden-xs hidden-sm hidden-lg clearfix"></div>';
									}
									if ($i%2 == 0){
										echo '<div class="hidden-lg hidden-md clearfix"></div>';
									}
									if ($i%4 == 0){
										echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
									}
								@endphp
								
							@endforeach
							
							{{ Form::hidden('default_type', '') }}
							{{ Form::hidden('action', 'select') }}
						</form>
					</div>
				</div>
				
				@if ($properties->lastPage() > 1)
					<div class="panel panel-default nest-properties-footer">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									{{ $properties->links() }}  
								</div>
							</div>
						</div>
					</div>
				@endif
            @endif
	
	@include('partials.property.save-shortlist')

            <div class="panel panel-default nest-property-list-buttons">
                <div class="panel-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3 col-sm-4 col-xs-6">
								<button id="print_pdf" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-print"></i>Lease
								</button>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<button id="print_pdf_sale" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-print"></i>Sale
								</button>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<button id="print_pdf_agent" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-print"></i>Lease (Agent)
								</button>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<button id="print_pdf_sale_agent" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-print"></i>Sale (Agent)
								</button>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-6">
								<button id="add_photoshoot_property" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Photoshoot
								</button>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<button id="add_frontend_property" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Website
								</button>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<button id="remove_frontend_property" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-minus"></i>Website
								</button>
							</div>
						</div>
					</div>
                </div>
            </div>
</div>
@endsection

@section('footer-script')
@include('partials.property.building-modal-control')
<script type="text/javascript">
function submit_action(action) {
	if ($('form#propertylistform input.property-option:checked').length > 0) {
		if (action == 'deselect') {
			$('form#propertylistform input[name=action]').val('deselect');
		}
		$('form#propertylistform').submit();
	} else {
		alert('Please choose property to put into frontend property list.');
	}
}
$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form#propertylistform input').prop('checked', $(this).is(":checked"));
    });

    $('button#add_photoshoot_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/photoshoot') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('select');
		
		proprerty_list_form.submit();
    });

    $('button#add_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('select');
		
		proprerty_list_form.submit();
    });
    $('button#remove_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('deselect');
		
		proprerty_list_form.submit();
    });
    $('button#print_pdf, button#print_pdf_sale').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#propertylistform');
        var url = "{{ url('property/option/print/' ) }}";
        proprerty_list_form.attr('action', url);
        proprerty_list_form.attr('target', '_blank');

        var default_type = ($(this).attr('id') == 'print_pdf')?'lease':'sale';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    /*$('button#set_hot_properties, button#unset_hot_properties').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#propertylistform');
        var url = "{{ url('property/option/set-as-hot/' ) }}";
        proprerty_list_form.attr('action', url);

        var default_type = ($(this).attr('id') == 'set_hot_properties')?'1':'0';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to mark as hot properties.');
        }
        return false;
    });*/
    $('button#print_pdf_agent, button#print_pdf_sale_agent').click(function(event){
        event.preventDefault();
        var proprerty_list_form = $('form#propertylistform');
        var url = "{{ url('property/option/printagent/' ) }}";
        proprerty_list_form.attr('action', url);
        proprerty_list_form.attr('target', '_blank');
		
        var default_type = ($(this).attr('id') == 'print_pdf_agent')?'lease':'sale';
        $('input[name=default_type]').val(default_type);
        if ($('form input.property-option:checked').length > 0) {
            proprerty_list_form.submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
	$('#property_sort_by').on('change', function(){
		var sortid = $('#property_sort_by').val();
		$('#sort_by_id').val(sortid);
		$('#propertyserachform').submit();
	});
});
</script>
@include('partials.shortlist-control-new')
@include('partials.property.save-shortlist-control-new')
@endsection