
<div class="nest-property-list-detail-wrapper">
	<div class="nest-property-list-detail-first-row row">
		<div class="col-xs-4">
			<b>Bedrooms</b>
		</div>
		<div class="col-xs-8">
			{{ $property->bedroom_count() }}
		</div>
	</div>
	<div class="row nest-striped">
		<div class="col-xs-4">
			<b>Bathrooms</b>
		</div>
		<div class="col-xs-8">
			{{ $property->bathroom_count() }}
		</div>
	</div>
	@php
		$striped = '';
	@endphp
	@if($property->maidroom > 0)
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Maidsrooms</b>
			</div>
			<div class="col-xs-8">
				{{ $property->maidroom_count() }}
			</div>
		</div>
		@php
			$striped = ' nest-striped';
		@endphp
	@endif
	<div class="row{{ $striped }}">
		<div class="col-xs-4">
			<b>Outdoor Space</b>
		</div>
		<div class="col-xs-8">
			@if(!empty($outdoor_ids) && !empty($outdoors))
				@foreach ($outdoor_ids as $outdoor_id => $outdoor)
					@if (in_array($outdoor_id, $outdoors))
						{{ $outdoor}} 
						@if (trim($outdoorareas[strtolower($outdoor)]) != '')
							 ({{ $outdoorareas[strtolower($outdoor)] }} sq.ft.)
						@endif
						<br />
					@endif
				@endforeach
			@endif
		</div>
		@php
			if ($striped == ''){
				$striped = ' nest-striped';
			}else{
				$striped = '';
			}
		@endphp
	</div>
	
	@if(trim($facilitystring) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Features</b>
			</div>
			<div class="col-xs-8">
				{{ $featurestring }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	
	@if(trim($facilitystring) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Facilities</b>
			</div>
			<div class="col-xs-8">
				{{ $facilitystring }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	
	@if(trim($property->floor_zone) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Floor Zone</b>
			</div>
			<div class="col-xs-8">
				{{ $property->floor_zone }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	@if(trim($property->layout) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Layout</b>
			</div>
			<div class="col-xs-8">
				{{ $property->layout }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	@if(trim($property->description) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Description</b>
			</div>
			<div class="col-xs-8">
				{{ $property->description }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	@if($property->year_built > 0)
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Year Built</b>
			</div>
			<div class="col-xs-8">
				{{ $property->year_built }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	<br />

	<div class="nest-property-list-detail-first-row row">
		<div class="col-xs-4">
			<b>Rent Inclusive</b>
		</div>
		<div class="col-xs-8">
			{{ $property->inclusive==1?'yes':'no' }}
		</div>
	</div>
	@if ($property->inclusive != 1)
		<div class="row nest-striped">
			<div class="col-xs-4">
				<b>Management Fee</b>
			</div>
			<div class="col-xs-8">
				{{ $property->management_fee }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4">
				<b>Government Rate</b>
			</div>
			<div class="col-xs-8">
				{{ $property->government_rate }} per quarter
			</div>
		</div>
	@endif
	<div class="clearfix"></div>
	<br />
</div>