@extends('layouts.app')

@section('content')
	
            <!-- Building Search -->
            <div class="nest-buildings-search-panel panel panel-default">
                <div class="panel-body">
					<div class="row">
						<a href="{{ url('property/edit/'.$propertyid) }}">Back to Current Property</a>
					</div>
                </div>
            </div>

            <!-- Current Buildings -->
            @if (count($properties) > 0)
					<div>
						<div class="nest-buildings-result-wrapper">
							@php
								$index = 0;
							@endphp
							@foreach ($properties as $property)
								@if (isset($pictures[$property->id]))
									<div class="row 
									@if ($index%2 == 1)
										nest-striped
									@endif
									">
										<div class="col-lg-4 col-md-4 col-sm-6">
											<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">{{ $property->name }}</a><br />
											<b>ID:</b> {{ $property->id }}
										</div>
										<div class="col-lg-4 col-md-4 col-sm-6">
											<b>Unit:</b> {{ $property->unit }}<br />
											<b>Floor No.:</b> {{ $property->floornr }}
										</div>
										<div class="col-lg-4 col-md-4 col-sm-12">
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/property/addbuildingpics/'.$propertyid.'/'.$property->id) }}">
												<i class="fa fa-btn fa-plus"></i>Add Pictures
											</a>
										</div>
										<div class="clearfix"></div>
										<div class="col-xs-12">
											@foreach ($pictures[$property->id] as $pic)
												<img src="{{ $pic->getShowUrlModal() }}" height="100" style="margin-top:3px;" />
											@endforeach
										</div>
										<div class="clearfix"></div>
									</div>
									@php
										$index++;
									@endphp
								@endif
							@endforeach
						</div>
                    </div>
            @endif

		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection

@section('footer-script')
@include('partials.property.building-modal-control')
@endsection