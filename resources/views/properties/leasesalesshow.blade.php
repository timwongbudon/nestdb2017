
@if (count($salelease) == 0)
	None
@else
	@foreach ($salelease as $sl)
		<div class="media media-comment">
			<div class="media-left">
				<img src="{{ url($sl['pic']) }}" class="img-circle media-object" width="60px" style="border:1px solid #cccccc;" />
			</div>
			<div class="media-body message text-left">
				<u>{{ $sl['name'] }}</u> <small class="text-muted">{{ $sl['datum'] }}</small> <b>{{ $sl['type'] }}</b>
				<br />
				{!! $sl['content'] !!}
			</div>
		</div>
		<div class="clearfix"></div>
	@endforeach
@endif