@extends('layouts.app')

@section('content')


<div class="nest-new">
	<div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('property/update') }}" method="POST" class="form-horizontal property-update" enctype="multipart/form-data">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Edit Property (ID {{ $property->id }})</h4>
						</div>

						<div class="panel-body">
							<!-- Display Validation Errors -->
							@include('common.errors')
						
							<!-- New Property Form -->
						
								{{ csrf_field() }}
							
							@if (!empty($searchterm))
								<input type="hidden" name="oldsearchterm" value="{{ $searchterm }}" />
							@else
								<input type="hidden" name="oldsearchterm" value="" />
							@endif
							
							
							
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Property Name</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="name" id="property-name" class="form-control" value="{{ old('name', $property->name) }}">
									<input type="hidden" name="temp_name" id="temp_property-name" class="form-control" value="{{ old('name', $property->name) }}">
									<span id="property-name-display" style="display:none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Display Name</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="display_name" id="property-display_name" class="form-control" value="{{ old('display_name', $property->display_name) }}">
									<input type="hidden" name="temp_display_name" id="temp_property-display_name" class="form-control" value="{{ old('display_name', $property->display_name) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Address</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1', $property->address1) }}">
									<input type="hidden" name="temp_address1" id="temp_property-address1" class="form-control" value="{{ old('address1', $property->address1) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Unit & Floor</div>
								</div>
								<div class="col-xs-3">
									<input type="text" name="unit" id="property-unit" class="form-control" value="{{ old('unit', $property->unit) }}">
									<input type="hidden" name="temp_unit" id="temp_property-unit" class="form-control" value="{{ old('unit', $property->unit) }}">
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Floor</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="floornr" id="property-floornr" class="form-control" value="{{ old('floornr', $property->floornr) }}">
									<input type="hidden" name="temp_floornr" id="temp_property-floornr" class="form-control" value="{{ old('floornr', $property->floornr) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 {!! Form::select('district_id', $district_ids, $property->district_id, ['id'=>'property-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
								</div>
								<div class="col-xs-6">
									{!! Form::select('country_code', $country_ids, $property->country_code, ['class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Building ID</div>
								</div>
								<div class="col-xs-3">
									<input type="text" name="building_id" id="building_id" class="form-control" value="{{ old('building_id', $property->building_id) }}" />
									<input type="hidden" name="temp_building_id" id="temp_building_id" class="form-control" value="{{ old('building_id', $property->building_id) }}" />
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Year Built</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built', $property->year_built) }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-space.jpg') }}" /></div>Gross</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="gross_area" id="property-gross_area" class="form-control" value="{{ old('gross_area', $property->gross_area) }}" min="0">
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-space.jpg') }}" /></div>Saleable</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="saleable_area" id="property-saleable_area" class="form-control" value="{{ old('saleable_area', $property->saleable_area) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-bed.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="bedroom" id="property-bedroom" class="form-control" value="{{ old('bedroom', $property->bedroom) }}" min="0">
								</div>
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-bath.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="bathroom" id="property-bathroom" class="form-control" value="{{ old('bathroom', $property->bathroom) }}" min="0">
								</div>
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-maid.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="maidroom" id="property-maidroom" class="form-control" value="{{ old('maidroom', $property->maidroom) }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Floor Zone</div>
								</div>
								<div class="col-xs-6">
									{!! Form::select('floor_zone', array('High' => 'High', 'Middle' => 'Middle', 'Low' => 'Low', 'Ground Floor' => 'Ground Floor'), $property->floor_zone, ['class'=>'form-control', 'placeholder' => 'Choose a floor zone...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Layout</div>
								</div>
								<div class="col-xs-6">
									{!! Form::select('layout', array('Stand-Alone' => 'Stand-Alone', 'Penthouse' => 'Penthouse', 'Simplex' => 'Simplex', 'Duplex' => 'Duplex', 'Triplex' => 'Triplex'), $property->layout, ['class'=>'form-control', 'placeholder' => 'Choose a layout...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Description</div>
								</div>
								<div class="col-xs-8">
									{!! Form::textarea('description', $property->description, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Website Text</div>
								</div>
								<div class="col-xs-8">
									{!! Form::textarea('webtext', $property->webtext, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
								</div>
								<div class="clearfix"></div>
							</div>

						

						
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-4">
									<label for="property-type-1" class="control-label">{!! Form::radio('type_id', 1, $property->type_id == 1, ['id'=>'property-type-1']) !!} Rent</label>
								</div>
								<div class="col-xs-4">
									<label for="property-type-2" class="control-label">{!! Form::radio('type_id', 2, $property->type_id == 2, ['id'=>'property-type-2']) !!} Sale</label>
								</div>
								<div class="col-xs-4">
									<label for="property-type-3" class="control-label">{!! Form::radio('type_id', 3, $property->type_id == 3, ['id'=>'property-type-3']) !!} Rent & Sale</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-sale-section" class="nest-property-edit-row" 
							@if ($property->type_id == 1)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Asking Sale</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="asking_sale" id="property-asking_sale" class="form-control" value="{{ old('asking_sale', $property->asking_sale) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Subject to Offer</div>
								</div>
								<div class="col-xs-3">
									<label for="property-nosaleprice-0" class="control-label">{!! Form::radio('nosaleprice', 0, $property->nosaleprice == 0, ['id'=>'property-nosaleprice-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-nosaleprice-1" class="control-label">{!! Form::radio('nosaleprice', 1, $property->nosaleprice == 1, ['id'=>'property-nosaleprice-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-rent-section" class="nest-property-edit-row" 
							@if ($property->type_id == 2)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Asking Rent</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="asking_rent" id="property-asking_rent" class="form-control" value="{{ old('asking_rent', $property->asking_rent) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="rent-inclusive-section" class="nest-property-edit-row nest-property-edit-radio-wrapper" 
							@if ($property->type_id == 2)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Inclusive</div>
								</div>
								<div class="col-xs-3">
									<label for="property-inclusive-1" class="control-label">{!! Form::radio('inclusive', 1, $property->inclusive===1, ['id'=>'property-inclusive-1']) !!} Yes</label>
								</div>
								<div class="col-xs-3">
									<label for="property-inclusive-2" class="control-label">{!! Form::radio('inclusive', 0, $property->inclusive===0, ['id'=>'property-inclusive-2']) !!} No</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-management-fee" class="nest-property-edit-row" 
							@if ($property->type_id == 2 || $property->inclusive == 1)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Management Fee / Month</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="management_fee" id="property-management_fee" class="form-control" value="{{ old('management_fee', $property->management_fee) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-government-fee" class="nest-property-edit-row" 
							@if ($property->type_id == 2 || $property->inclusive == 1)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Government Rate / Quarter</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="government_rate" id="property-government_rate" class="form-control" value="{{ old('government_rate', $property->government_rate) }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Available Date</div>
								</div>
								<div class="col-xs-6">
									{{ Form::date('available_date', old('available_date', $property->available_date), ['class'=>'form-control']) }}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Hot</div>
								</div>
								<div class="col-xs-3">
									<label for="property-hot-0" class="control-label">{!! Form::radio('hot', 0, $property->hot == 0, ['id'=>'property-hot-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-hot-1" class="control-label">{!! Form::radio('hot', 1, $property->hot == 1, ['id'=>'property-hot-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Bonus Commission</div>
								</div>
								<div class="col-xs-3">
									<label for="property-bonuscomm-0" class="control-label">{!! Form::radio('bonuscomm', 0, $property->bonuscomm == 0, ['id'=>'property-bonuscomm-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-bonuscomm-1" class="control-label">{!! Form::radio('bonuscomm', 1, $property->bonuscomm == 1, ['id'=>'property-bonuscomm-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Leased</div>
								</div>
								<div class="col-xs-3">
									<label for="property-leased-0" class="control-label">{!! Form::radio('leased', 0, $property->leased == 0, ['id'=>'property-leased-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-leased-1" class="control-label">{!! Form::radio('leased', 1, $property->leased == 1, ['id'=>'property-leased-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Sold</div>
								</div>
								<div class="col-xs-3">
									<label for="property-sold-0" class="control-label">{!! Form::radio('sold', 0, $property->sold == 0, ['id'=>'property-sold-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-sold-1" class="control-label">{!! Form::radio('sold', 1, $property->sold == 1, ['id'=>'property-sold-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>

							<!-- <div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Sole Agency</div>
								</div>
								<div class="col-xs-3">
									<label for="property-sole_agency-0" class="control-label">{!! Form::radio('sole_agency', 0, $property->sole_agency == 0, ['id'=>'property-sole_agency-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-sole_agency-1" class="control-label">{!! Form::radio('sole_agency', 1, $property->sole_agency == 1, ['id'=>'property-sole_agency-1']) !!} Yes</label>
								</div>
								<div class="col-xs-3">
									<label for="property-sole_agency-2" class="control-label">{!! Form::radio('sole_agency', 2, $property->sole_agency == 2, ['id'=>'property-sole_agency-2']) !!} Dual</label>
								</div>
								
								<div class="clearfix"></div>
							</div> -->
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Agency</div>
								</div>
								<div class="col-xs-6">
								{!! Form::select('sole_agency', array('0' => 'No Agency', '1' => 'Sole Agency', '2' => 'Dual Agency'), $property->sole_agency, ['class'=>'form-control', 'placeholder' => 'Choose a Agency...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Direct Landlord</div>
								</div>
								<div class="col-xs-3">
									<label for="property-direct_landlord-0" class="control-label">{!! Form::radio('direct_landlord', 0, $property->direct_landlord == 0, ['id'=>'property-direct_landlord-0','class'=>'dll']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-direct_landlord-1" class="control-label">{!! Form::radio('direct_landlord', 1, $property->direct_landlord == 1, ['id'=>'property-direct_landlord-1','class'=>'dll']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
								<div style="text-align: left; font-size:11px; padding-left: 10px;">If yes, select an Information Officer below.</div>
							</div>

							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Information Officer</div>
								</div>
								<div class="col-xs-6">
									<select class="form-control io" name="information_officer" >
									<option value="0"></option>
										@foreach($io_users as $io_user)
											<option value="{{$io_user->id}}" 
												@if($io_user->id == $property->information_officer) 
													selected="selected" 
												@endif> {{$io_user->name}}</option>
										@endforeach
									</select>
								</div>
								
								<div class="clearfix"></div>
							</div>

							

						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Commission</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="commission" id="property-commission" class="form-control" value="{{ old('commission', $property->commission) }}" placeholder="" maxlength="100" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Point of Contact</div>
								</div>
								<div class="col-xs-3">
									<label for="vendor-poc-1" class="control-label">{!! Form::radio('poc_id', 1, $property->poc_id == 1, ['id'=>'vendor-poc-1']) !!} Owner</label>
								</div>
								<div class="col-xs-2">
									<label for="vendor-poc-2" class="control-label">{!! Form::radio('poc_id', 2, $property->poc_id == 2, ['id'=>'vendor-poc-2']) !!} Rep</label>
								</div>
								<div class="col-xs-3">
									<label for="vendor-poc-3" class="control-label">{!! Form::radio('poc_id', 3, $property->poc_id == 3, ['id'=>'vendor-poc-3']) !!} Agency</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-vendor-owner-section" class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Owner Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="owner" id="property-owner" class="form-control" value="{{ old('owner', $property['owner']?$property['owner']->basic_info():'') }}">
									<span id="property-owner-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the owner name"><i class="fa fa-btn fa-close"></i></button></span>
									<input type="hidden" name="owner_id" id="property-owner_id" class="form-control" value="{{ old('owner_id', $property['owner']?$property['owner']->id:'') }}"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-vendor-rep-section" class="nest-property-edit-row nest-property-edit-row-contacts" 
							@if (empty($property['owner']))
								style="display:none;"
							@endif
							>
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Rep Name(s)</div>
								</div>
								<div class="col-xs-8 nest-property-edit-rep-wrapper">
									<div id="property-vendor-rep-checkbox-template" style="display: none">
										<label for="property-rep_id-checkbox-n" class="control-label">{!! Form::checkbox('rep_ids[]', 0, null, ['id'=>'property-rep_id-checkbox-n']) !!} </label>
									</div>
									@if (!empty($property['owner']))
										@if(count($property['allreps']) > 0)
											<?php $rep_ids = !empty($property->rep_ids)?explode(',',$property->rep_ids):array();?>
											@foreach($property['allreps'] as $rep)
												<label for="property-rep_id-checkbox-{{$rep->id}}" class="control-label">
													{!! Form::checkbox('rep_ids[]', $rep->id, in_array($rep->id, $rep_ids), ['id'=>'property-rep_id-checkbox-'.$rep->id]) !!} {!! $rep->linked_info() !!}
												</label><br />
											@endforeach
										@endif
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-vendor-agency-section" class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Agency Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="agent" id="property-agent" class="form-control" value="{{ old('agent', $property['agent']?$property['agent']->basic_info():'') }}">
									<span id="property-agent-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the agent name"><i class="fa fa-btn fa-close"></i></button></span>
									<input type="hidden" name="agent_id" id="property-agent_id" class="form-control" value="{{ old('agent_id', $property['agent']?$property['agent']->id:'') }}"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-vendor-agent-section" class="nest-property-edit-row nest-property-edit-row-contacts" 
							@if (empty($property['agent']))
								style="display:none;"
							@endif
							>
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Agent Name(s)</div>
								</div>
								<div class="col-xs-8 nest-property-edit-agent-wrapper">
									<div id="property-vendor-agent-checkbox-template" style="display: none">
										<label for="property-agent_id-checkbox-n" class="control-label">{!! Form::checkbox('agent_ids[]', 0, null, ['id'=>'property-agent_id-checkbox-n']) !!} </label>
									</div>
									@if (!empty($property['agent']))
										@if(count($property['allagents']) > 0)
											<?php $agent_ids = !empty($property->agent_ids)?explode(',',$property->agent_ids):array();?>
											@foreach($property['allagents'] as $agent)
												<label for="property-agent_id-checkbox-{{$agent->id}}" class="control-label">
													{!! Form::checkbox('agent_ids[]', $agent->id, in_array($agent->id, $agent_ids), ['id'=>'property-agent_id-checkbox-'.$agent->id]) !!} {!! $agent->linked_info() !!}
												</label><br />
											@endforeach
										@endif
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							<div id="property-vendor-newcontact-section" class="nest-property-edit-row nest-property-edit-row-contacts" style="display:none;">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">New Contact</div>
								</div>
								<div class="col-xs-8" id="newcontactdiv" style="padding-top:7px;padding-bottom:7px;">
									
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							
							<div class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">PoC by Phone No.</div>
								</div>
								<div class="col-xs-4" style="padding-right:2px;">
									<input type="text" id="poc-phone" class="form-control" value="">
								</div>
								<div class="col-xs-4" style="padding-left:2px;">
									<a id="poc-phone-btn" class="nest-button btn btn-default" style="width:100%;padding:5px 12px;" onClick="pocphonesearch();">Search</a>
								</div>
								<div class="clearfix"></div>
								<div id="poc-phone-search-results"></div>
								<div class="clearfix"></div>
							</div>
							
							<script>
								function pocphonesearch(){
									var phone = jQuery('#poc-phone').val().trim();
									jQuery.ajaxSetup({
										headers: {
											'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
										}
									});
									jQuery.ajax({
										type: 'GET',
										url: '{{ url("/vendor/phonesearch") }}/'+phone,
										cache: false,
										success: function (data) {
											var str = '<div style="padding-top:15px;padding-bottom:20px;">';
											if (data.length > 0){
												for (var i = 0; i < data.length; i++){
													var d = data[i];
													var strout = '';
													str = str + '<div class="col-xs-9" style="padding-bottom:5px;">';
													str = str + '<a href="{{ url("vendor/show") }}/'+d['id']+'" target="_blank">';
													if (d['firstname'].trim() != '' && d['lastname'].trim() != '' && d['company'].trim() != ''){
														strout = strout + d['firstname'].trim() + ' ' + d['lastname'].trim() + ', ' + d['company'].trim();
													}else if (d['firstname'].trim() != '' && d['lastname'].trim() != ''){
														strout = strout + d['firstname'].trim() + ' ' + d['lastname'].trim();
													}else if (d['company'].trim() != ''){
														strout = strout + d['company'].trim();
													}else if (d['lastname'].trim() != ''){
														strout = strout + d['lastname'].trim();
													}else if (d['firstname'].trim() != ''){
														strout = strout + d['firstname'].trim();
													}
													strout = strout + ' (';
													if (d['tel'].trim() != '' && d['tel2'].trim() != ''){
														strout = strout + d['tel'].trim() + ', ' + d['tel2'].trim();
													}else if (d['tel'].trim() != ''){
														strout = strout + d['tel'].trim();
													}else if (d['tel2'].trim() != ''){
														strout = strout + d['tel2'].trim();
													}
													strout = strout + ')';
													str = str + strout;
													str = str + '</a>';
													str = str + '</div>';
													strout = strout.replace("'", "&acute;");
													str = str + '<div class="col-xs-3 text-right">';
														str = str + '<a onClick="setpoc('+d['type_id']+', '+d['id']+', '+d['parent_id']+', \''+strout+'\');" style="cursor:pointer;">use</a>';
													str = str + '</div>';
													str = str + '<div class="clearfix"></div>';
												}
											}
											str = str + '</div>';
											jQuery('#poc-phone-search-results').html(str);
										},
										error: function (data) {
											var str = '<div style="padding-top:15px;padding-bottom:20px;" class="col-xs-12">';
											str = str + 'Please enter a number and click the button again';
											str = str + '</div>';
											jQuery('#poc-phone-search-results').html(str);
										}
									});
								}
								
								function setpoc(type, id, parentid, name){
									jQuery('#property-vendor-owner-section').css('display', 'none');
									jQuery('#property-vendor-rep-section').css('display', 'none');
									jQuery('#property-vendor-agency-section').css('display', 'none');
									jQuery('#property-vendor-agent-section').css('display', 'none');
									jQuery('#property-vendor-newcontact-section').css('display', 'block');
									
									var str = '';
									str = str + '<a href="{{ url("vendor/show") }}/'+id+'" target="_blank">'+name+'</a>';
									if (type == 1){
										jQuery('#vendor-poc-1').prop("checked", true);
										str = str + '<input type="hidden" name="owner_id" value="'+id+'" />';
										str = str + '<input type="hidden" name="agent_id" value="0" />';
									}else if (type == 2){
										jQuery('#vendor-poc-2').prop("checked", true);
										str = str + '<input type="hidden" name="owner_id" value="'+parentid+'" />';
										str = str + '<input type="hidden" name="rep_ids[]" value="'+id+'" />';
										str = str + '<input type="hidden" name="agent_id" value="0" />';
									}else if (type == 3){
										jQuery('#vendor-poc-3').prop("checked", true);
										str = str + '<input type="hidden" name="owner_id" value="0" />';
										str = str + '<input type="hidden" name="agent_id" value="'+id+'" />';
									}else if (type == 4){
										jQuery('#vendor-poc-3').prop("checked", true);
										str = str + '<input type="hidden" name="owner_id" value="0" />';
										str = str + '<input type="hidden" name="agent_id" value="'+parentid+'" />';
										str = str + '<input type="hidden" name="agent_ids[]" value="'+id+'" />';
									}
									jQuery('#newcontactdiv').html(str);
								}


								$('#property-name, #property-unit, #property-floornr, #property-display_name').keyup(function(){
							$('#update-property-btn').show();
						$('#confirm-update-property-btn').hide();
						});

						$('#property-district_id, #country_code').keyup(function(){
							$('#update-property-btn').show();
						$('#confirm-update-property-btn').hide();
						});




								jQuery('.property-update').submit(function(e){
																	

									if(jQuery('.dll:checked').val()=="1"){
										if(jQuery('.io').val()=="0"){
											alert("Please select Information Officer")
											e.preventDefault();

										}
									}


									if( $(document.activeElement).val() == "Update"){

									var name = $('#property-name').val();
									var display_name = $('#property-display_name').val();
									var unit = $('#property-unit').val();
									var floornr = $('#property-floornr').val();
									var address1 = $('#property-address1').val();
									var building_id = $('#building_id').val();

									var temp_name = $('#temp_property-name').val();
									var temp_display_name = $('#temp_property-display_name').val();
									var temp_unit = $('#temp_property-unit').val();
									var temp_floornr = $('#temp_property-floornr').val();
									var temp_address1 = $('#temp_property-address1').val();
									var temp_building_id = $('#temp_building_id').val();

									var changes = false;
									if(name != temp_name){
										changes = true;
									}

									if(display_name != temp_display_name){
										changes = true;
									}

									if(unit != temp_unit){
										changes = true;
									}

									if(floornr != temp_floornr){
										changes = true;
									}

									if(address1 != temp_address1){
										changes = true;
									}

									if(building_id != temp_building_id){
										changes = true;
									}


									if(changes){
										e.preventDefault();
										$.ajax(
											{
											type: 'get',
											url: '/property/property_exists_update?id={{ $property->id }}&name='+name+'&display_name='+display_name+'&unit='+unit+'&floornr='+floornr+'&address1='+address1+'&building_id='+building_id,
											data: '',
											//dataType: 'JSON',
											beforeSend: function () {
												//document.getElementById('loading-app').style.display = 'block';
											},
											complete: function () {
											// document.getElementById('loading-app').style.display = 'none';
											},
											success: function (result) {
												console.log(result);
												var r = JSON.parse(result);
												if(r.count == 0){
													//alert("Good to go");
													e.currentTarget.submit();
												}else{
													$('#update-property-btn').hide();
													$('#confirm-update-property-btn').show();
													$('#similar-property').html(r.results);
												}
											
											//	alert(result);

											},
											error: function (xhr, status, error) {
												alert("Something went wrong");
												console.log(status)
												console.log(xhr)
												console.log(error)
											}
										});

									}

								
								}

								});
								
								
							</script>
							
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Key Location</div>
								</div>
								<div class="col-xs-5">
									<label for="property-key_loc_id-1" class="control-label">{!! Form::radio('key_loc_id', 1, $property->key_loc_id == 1, ['id'=>'property-key_loc_id-1']) !!} Management Office</label>
								</div>
								<div class="col-xs-3">
									<label for="property-key_loc_id-3" class="control-label">{!! Form::radio('key_loc_id', 3, $property->key_loc_id == 3, ['id'=>'property-key_loc_id-3']) !!} Agency</label>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-4">
									&nbsp;
								</div>
								<div class="col-xs-5">
									<label style="padding-top:0px !important;" for="property-key_loc_id-2" class="control-label">{!! Form::radio('key_loc_id', 2, $property->key_loc_id == 2, ['id'=>'property-key_loc_id-2']) !!} Door Code</label>
								</div>
								<div class="col-xs-3">
									<label style="padding-top:0px !important;" for="property-key_loc_id-4" class="control-label">{!! Form::radio('key_loc_id', 4, $property->key_loc_id == 4, ['id'=>'property-key_loc_id-4']) !!} Other</label>
								</div>
								<div class="clearfix"></div>
							</div>

							<div id="property-key-loc-other-section" class="nest-property-edit-row" 
							@if ($property->key_loc_id != 4)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Other Key Location</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="key_loc_other" id="property-key_loc_other" class="form-control" value="{{ old('key_loc_other', $property->key_loc_other) }}">
								</div>
								<div class="clearfix"></div>
							</div>

							<div id="property-key-loc-door-code-section" class="nest-property-edit-row" 
							@if ($property->key_loc_id != 2)
								style="display:none;"
							@endif
							>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Door Code</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="door_code" id="property-door_code" class="form-control" value="{{ old('door_code', $property->door_code) }}">
								</div>
								<div class="clearfix"></div>
							</div>

							<div id="property-key-loc-other-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Agency Fee</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="agency_fee" id="property-agency_fee" class="form-control" value="{{ old('agency_fee', $property->agency_fee) }}" placeholder="">
								</div>
								<div class="clearfix"></div>
							</div>

						
						</div>
					</div>

				</div>
				
				<script>
					$(document).ready(function(){
						//auto increase by a certain amount
						$("#property-asking_sale").on('keydown', function(event) { 
							var keyCode = event.keyCode || event.which; 
							if (keyCode == 9) { //tab keydown
								var res = parseFloat($(this).val());
								if (res < 1000000) {
									$(this).val(res*1000000);
								}
							} 
						});
						$("#property-asking_rent").on('keydown', function(event) { 
							var keyCode = event.keyCode || event.which; 
							if (keyCode == 9) { //tab keydown
								var res = parseFloat($(this).val());
								if (res < 1000) {
									$(this).val(res*1000);
								}
							} 
						});
						
						$('input[name=type_id]').change(function(){
							var typeid = $(this).val();
							
							if (typeid == 1){
								$('#property-sale-section').css('display', 'none');
								$('#property-rent-section').css('display', 'block');
								$('#rent-inclusive-section').css('display', 'block');
								var inclusive = $('input[name=inclusive]:checked').val();
								if (inclusive == 1){
									$('#property-management-fee').css('display', 'none');
									$('#property-government-fee').css('display', 'none');								
								}else{
									$('#property-management-fee').css('display', 'block');
									$('#property-government-fee').css('display', 'block');										
								}
							}else if (typeid == 2){
								$('#property-sale-section').css('display', 'block');
								$('#property-rent-section').css('display', 'none');
								$('#rent-inclusive-section').css('display', 'none');
								$('#property-management-fee').css('display', 'none');
								$('#property-government-fee').css('display', 'none');
							}else if (typeid == 3){
								$('#property-sale-section').css('display', 'block');
								$('#property-rent-section').css('display', 'block');
								$('#rent-inclusive-section').css('display', 'block');
								var inclusive = $('input[name=inclusive]:checked').val();
								if (inclusive == 1){
									$('#property-management-fee').css('display', 'none');
									$('#property-government-fee').css('display', 'none');								
								}else{
									$('#property-management-fee').css('display', 'block');
									$('#property-government-fee').css('display', 'block');										
								}						
							}
						});
						
						$('input[name=inclusive]').change(function(){
							var inclusive = $('input[name=inclusive]:checked').val();
							
							if (inclusive == 1){
								$('#property-management-fee').css('display', 'none');
								$('#property-government-fee').css('display', 'none');								
							}else{
								$('#property-management-fee').css('display', 'block');
								$('#property-government-fee').css('display', 'block');										
							}
						});
						
						$('input[name=key_loc_id]').change(function(){
							var location = $('input[name=key_loc_id]:checked').val();
							
							if (location == 2){
								$('#property-key-loc-door-code-section').css('display', 'block');							
							}else{
								$('#property-key-loc-door-code-section').css('display', 'none');							
							}
							if (location == 4){
								$('#property-key-loc-other-section').css('display', 'block');						
							}else{
								$('#property-key-loc-other-section').css('display', 'none');								
							}
						});
						

						
						
					});

				</script>
				
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;">
									<div class="nest-property-edit-label-features"><u>Outdoor Space</u></div>
								</div>
							</div>
							<div class="nest-property-edit-row">
								{{ Form::hidden('outdoors', '0') }}
								@foreach ($outdoor_ids as $outdoor_id => $outdoor)
									@if ($outdoor_id != 5)
										<div class="col-xs-3">
											<div class="nest-property-edit-label-outdoor">
												<label for="property-outdoors-{{$outdoor_id}}" class="control-label">{!! Form::checkbox('outdoors[]', $outdoor_id, in_array($outdoor_id, $outdoors), ['id'=>'property-outdoors-'.$outdoor_id]) !!} {{ $outdoor}}</label>
											</div>
										</div>
										<div class="col-xs-3">
											<input type="number" name="outdoorareas[{{ strtolower($outdoor) }}]" id="property-outdoor-{{ strtolower($outdoor) }}" class="form-control" value="{{ isset($outdoorareas[strtolower($outdoor)])?$outdoorareas[strtolower($outdoor)]:'' }}" min="0" /> 
										</div>
									@endif
									@if ($outdoor_id == 2 || $outdoor_id == 4)
										<div class="clearfix"></div>
										</div>
										<div class="nest-property-edit-row">
									@endif
								@endforeach
								
								 
								
								
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;padding-top:10px;">
									<div class="nest-property-edit-label-features"><u>Property Features</u></div>
									<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($featurelist as $feature_id => $feature)
											@if ($feature_id != 10 and $feature_id != 12 and $feature_id != 9 and $feature_id != 26)
												@if ($feature_id != 5)
													<div class="col-xs-6 property-features-{{$feature_id}}-wrapper">
														<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, $features), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature }}
														</label>
													</div>
												@else
													<div class="col-xs-3" style="padding-top:7px;">
														<div class="nest-property-edit-label-outdoor">
															<label for="property-features-5" class="control-label">{!! Form::checkbox('features[]', 5, in_array(5, $features), ['id'=>'property-features-5']) !!} Car Park</label>
														</div>
													</div>
													<div class="col-xs-3" style="padding-top:7px;">
														<input type="number" name="car_park" id="property-car_park" class="form-control" value="{{ old('car_park', $property->car_park) }}" min="0">
													</div>
												@endif
											@endif
										@endforeach
									</div>
									<div class="clearfix"></div>
									<div style="padding-top:5px;">
										<input type="text" name="otherfeatures" placeholder="Other Features: e.g. Library, Separate studio, etc." id="property-otherfeatures" class="form-control" value="{{ old('otherfeatures', $property->otherfeatures) }}"> 
									</div>
								</div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;padding-top:10px;">
									<div class="nest-property-edit-label-features"><u>Building Facilities</u></div>
									<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($facilitylist as $feature_id => $feature)
											@if ($feature_id != 7)
												<div class="col-xs-6">
											@else
												<div class="col-xs-12">
											@endif
												<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, $features), ['id'=>'property-features-'.$feature_id, 'class'=>'property-feature-input']) !!} {{ $feature }}
												</label>
											</div>
										@endforeach
									</div>
									<div class="clearfix"></div>
									<div style="padding-top:5px;">
										<input type="text" name="otherfacilities" placeholder="Other Facilities: e.g. Library, Separate studio, etc." id="property-otherfacilities" class="form-control" value="{{ old('otherfacilities', $property->otherfacilities) }}"> 
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							{{ Form::hidden('previous_url', $previous_url) }}
							{{ Form::hidden('id', $property->id) }}
							<div id="update-property-btn">
							<button type="submit" class="nest-button nest-right-button btn btn-default" value="Update"><i class="fa fa-btn fa-pencil-square-o"></i>Update </button>
							</div>
							@if (!empty($previous_url))
								<a href="{{ $previous_url }}" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-arrow-left"></i>Back</a>
							@endif

							<div id="confirm-update-property-btn" style="display: none; ">
							<div id="similar-property" class="alert alert-danger" ></div>
							<div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" value="Confirm"><i class="fa fa-btn fa-plus"></i>Confirm </button>
							</div>
							</div>

						</div>
					</div>
					<!-- <div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;padding-top:10px;">
									<div class="nest-property-edit-label-features"><u>Other Old Features</u></div>
									<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($featurelistold as $feature_id => $feature)
											<div class="col-xs-6">
												<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, $features), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature }}
												</label>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</form>
			<div class="clearfix"></div>
			
			<script>
				$(document).ready(function(){
					carpark_checkbox = '#property-features-5';
					carpark_text = '#property-car_park';

					$(carpark_checkbox).change(function(){
						if ($(carpark_checkbox+':checked').length) {
							if ($(carpark_text).val() == '' || $(carpark_text).val() == '0') {
								$(carpark_text).val('1');
							}
						} else {
							$(carpark_text).val(0);
						}
					});
					$(carpark_text).change(function(){
						if ($(carpark_text).val() == '' || $(carpark_text).val() == '0') {
							$(carpark_checkbox).removeAttr('checked');
						} else {
							$(carpark_checkbox).prop('checked', 'checked');
						}
					});
				});
			</script>
			
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addComment();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				@if (count($salelease) > 0)
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Leases & Sales</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-leasessales">
								@foreach ($salelease as $sl)
									<div class="media media-comment">
										<div class="media-left">
											<img src="{{ url($sl['pic']) }}" class="img-circle media-object" width="60px" style="border:1px solid #cccccc;" />
										</div>
										<div class="media-body message text-left">
											<u>{{ $sl['name'] }}</u> <small class="text-muted">{{ $sl['datum'] }}</small> <b>{{ $sl['type'] }}</b>
											<br />
											{!! $sl['content'] !!}
										</div>
									</div>
									<div class="clearfix"></div>
								@endforeach
							</div>
						</div>
					</div>
					
				@endif
				
				
				
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Change Log</h4>
					</div>
					
					<div class="panel-body">
						<div class="nest-changes" id="existingChangesEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Delete Log</h4>
					</div>
					
					<div class="panel-body">
						
						@if( count($propertyDeleteLogs) == 0 )
							<div>No data found.<br> <br></div>							
						@endif

						<div>
							
						@foreach($propertyDeleteLogs as $pdl)
							
							@php 
								if(!isset($status))
									$status = $pdl->status;
							@endphp


							{{$pdl->status == "Restored" ? "Restored": "Deleted"}} by {{$pdl->deletedby->name}} <small>{{ date('d/m/Y H:i',strtotime($pdl->created_at)) }}</small> <br>

							@if(isset($pdl->confirmedby->name))
								{{$pdl->status}} by {{isset($pdl->confirmedby->name) ? $pdl->confirmedby->name : '-'}} <small>{{ date('d/m/Y H:i',strtotime($pdl->updated_at)) }}</small>  <br><br>
							@else
								<br>
							@endif
						@endforeach

																	

					

						@if(!isset($status) || $status == 'Declined' || $status == 'Restored')

							@if( $isTrashed == false )
							<form class="form-horizontal" action="{{url('/property/delete')}}" method="post" onsubmit="return confirm('Do you really want to delete this property?');">
								<div class="nest-delete-row">
									<div class="col-sm-6">
										{{ csrf_field() }}
										{{ Form::hidden('previous_url', $previous_url) }}
										{{ Form::hidden('property_id', $property->id) }}
										<button type="submit" class="btn btn-danger">
											<i class="fa fa-btn fa-trash-o"></i>Delete Property</button>
									</div>
								</div>
							</form>
							@endif

						@endif

						</div>
					</div>
				</div>
				
				<div class="panel panel-warning">
					<div class="panel-heading">
						Old Data from ProppertyBase
					</div>
					<div class="panel-body">
						<form>
						@if(!empty($migration_metas))
							@foreach($migration_metas as $meta)
							@if(!in_array($meta->meta_key, array('user_id')))
							<div class="form-group">
								<label for="property-type" class="col-sm-12 control-label">{{ $meta->meta_key }} </label>
								<div class="col-sm-12">
									@if (is_array($meta->meta_value))
										@if ($meta->meta_key == 'outdoors')
											@if(!empty($meta->meta_value))
												@foreach($meta->meta_value as $outdoor_id)
												@if(isset($outdoor_ids[$outdoor_id]))
												<span>{{ $outdoor_ids[$outdoor_id] }}</span>
												@endif
												@endforeach
											@else
												<span>n/a</span>
											@endif
										@elseif($meta->meta_key == 'features')
											@if(!empty($meta->meta_value))
												@foreach($meta->meta_value as $feature_id)
												@if(isset($feature_ids[$feature_id]))
												<span>{{ $feature_ids[$feature_id] }}</span>
												@endif
												@endforeach
											@else
												<span>n/a</span>
											@endif
										@endif
									@elseif($meta->meta_key == 'post_content')
									<span>{!! $meta->get_fixed_post_content() !!}</span>
									@else
									<span>{{ $meta->meta_value }}</span>
									@endif
								</div>
							</div>
							@endif
							@endforeach
						@endif
						</form>
					</div>
				</div>
				
				<!--<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal" action="{{url('/property/delete')}}" method="post" onsubmit="return confirm('Do you really want to delete this property?');">
							<div class="nest-delete-row">
								<div class="col-sm-6">
									{{ csrf_field() }}
									{{ Form::hidden('previous_url', $previous_url) }}
									{{ Form::hidden('property_id', $property->id) }}
									<button type="submit" class="btn btn-danger">
										<i class="fa fa-btn fa-trash-o"></i>Delete Property</button>
								</div>
							</div>
						</form>
					</div>
				</div>-->
			</div>
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="/showpicsoriginal/{{ $property->id }}" target="_blank" style="float:right;">Original Images</a> &nbsp; 
						@if ($property->building_id > 0)
							<a href="{{ '/property/buildingpics/'.$property->id.'/'.$property->building_id }}" style="float:right;margin-right:15px;">Add Existing Images</a> &nbsp;
						@endif
						<h4>Property Media</h4> 
					</div>
					<div class="panel-body">
						<form class="form-horizontal">
	@include('partialsnew.include-plupload', array(
			'id'    => 'property_nest_photos',
			'label' => 'Nest Photo',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'nest-photo',
						'type'  => 'database',
					)))

	@include('partialsnew.include-plupload', array(
			'id'    => 'property_nest_photos_website',
			'label' => 'Nest Photo (Website)',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'nest-photo',
						'type'  => 'website',
					)))

	@include('partialsnew.include-plupload', array(
			'id'    => 'property_other_photos',
			'label' => 'Other Photo',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'other-photo',
						'type'  => 'database',
					)))
						</form>
						
						
						<a id="docupload"></a>
						<div id="hongkonghomesphotos" class="col-xs-12" style="padding-left:0px;padding-right:0px;">
							<div class="nest-property-edit-upload-label">Proway (<a href="https://proway.com.hk" target="_blank" style="color:#000000;">proway.com.hk</a>)</div>
							<form class="form-horizontal">
								<div class="nest-property-edit-hk-homes-wrapper">
									<div class="col-xs-8" style="padding-right:0px;">
										<input type="text" name="property_url" placeholder="Proway URL" id="property-property_url" class="form-control" value="{{ old('hkh_url', $property->hkh_url) }}" placeholder="">
									</div>
									<div class="col-xs-4" style="padding-left:0px;">
										{{ Form::hidden('property_id', $property->id) }}
										<button type="submit" class="nest-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Submit </button>
									</div>
									<div class="clearfix"></div>
									<div id="hongkonghomesphotos_result" class="col-xs-12"></div>
								</div>
							</form>
						</div>
						<div id="newhongkonghomesphotos" class="col-xs-12" style="padding-top:15px;padding-left:0px;padding-right:0px;">
							<div class="nest-property-edit-upload-label">HK Homes (<a href="https://www.hongkonghomes.com" target="_blank" style="color:#000000;">hongkonghomes.com</a>)</div>
							<form class="form-horizontal">
								<div class="nest-property-edit-hk-homes-wrapper">
									<div class="col-xs-8" style="padding-right:0px;">
										<input type="text" name="newproperty_url" placeholder="HK Homes URL" id="property-newproperty_url" class="form-control" value="{{ old('hkhnew_url', $property->hkhnew_url) }}" placeholder="">
									</div>
									<div class="col-xs-4" style="padding-left:0px;">
										{{ Form::hidden('newproperty_id', $property->id) }}
										<button type="submit" class="nest-button btn btn-default"><i class="fa fa-btn fa-pencil-square-o"></i>Submit </button>
									</div>
									<div class="clearfix"></div>
									<div id="newhongkonghomesphotos_result" class="col-xs-12"></div>
								</div>
							</form>
						</div>
						<div class="col-xs-12" style="padding-left:0px;padding-right:0px;padding-top:15px;">
							<a href="{{ url('/property/flushmedia/'.$property->id) }}" class="nest-button btn btn-default">Flush Image Cache</a>
						</div>
					</div>
				</div>
				
				
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Document Upload</h4>
					</div>
					<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
						<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
							<div class="nest-propety-docs-uploaded col-xs-12">
								@if (count($docuploads) > 0)
									@foreach ($docuploads as $d)
										@if ($d->doctype == 'landsearch')
											Landsearch
										@elseif ($d->doctype == 'other')
											Other Document
										@endif
										from 
										{{ $d->created_at() }}
										@if (isset($consultants[$d->consultantid]))
											by {{ $consultants[$d->consultantid]->name }}
										@endif
										<a href="{{ url('/property/showdoc/'.$d->id.'') }}" target="_blank">download</a>
										@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
											<a href="{{ url('/property/removedoc/'.$d->id.'/'.$d->propertyid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
										@endif
										<br />
									@endforeach
								@endif
							</div>
							<div class="clearfix"></div>
							<div class="nest-propety-prop-docs-upload" style="padding-top:12px;">
								<form action="{{ url('/property/docupload/'.$property->id.'') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="col-xs-6">
										<select name="doctype" class="form-control" style="font-weight:normal;">
											<option value="landsearch">Landsearch</option>
											<option value="other">Other</option>
										</select>
									</div>
									<div class="col-xs-6">
										<button class="nest-button nest-right-button btn btn-default" type="submit">Upload PDF</button>
									</div>
									<div class="clearfix"></div>
									<div class="draganddropup col-xs-12">
										{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
									</div>
								</form>
							</div>
							<div class="clearfix"></div>
						</div>
						
					</div>
				</div>
				
				
				
				


	<!-- delete property -->
			</div>
		</div>
    </div>
</div>
	
	
	
	<script>			
		function loadCommentsEdit(){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'GET',
				url: '{{ url("/comments/".$property->id) }}',
				cache: false,
				success: function (data) {
					jQuery('#existingCommentsEdit').html(data);
				},
				error: function (data) {
					jQuery('#existingCommentsEdit').html(data);
					jQuery.ajax({
						type: 'GET',
						url: '{{ url("/comments/".$property->id) }}',
						cache: false,
						success: function (data) {
							jQuery('#existingCommentsEdit').html(data);
						},
						error: function (data) {
							jQuery.ajax({
								type: 'GET',
								url: '{{ url("/comments/".$property->id) }}',
								cache: false,
								success: function (data) {
									jQuery('#existingCommentsEdit').html(data);
								},
								error: function (data) {
									jQuery('#existingCommentsEdit').html('An error occurred '+data);
								}
							});
						}
					});
				}
			});
		}
		
		function addComment(){
			var comment = jQuery('#newcomment').val().trim();
			if (comment.length > 0){
				jQuery.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				jQuery.ajax({
					type: 'POST',
					url: '{{ url("/commentadd/".$property->id) }}',
					data: {comment: comment},
					dataType: 'text',
					cache: false,
					async: false,
					success: function (data) {
						jQuery('#newcomment').val('');
						loadCommentsEdit();
					},
					error: function (data) {
						console.log(data);
						alert('Something went wrong, please try again!');
					}
				});
			}else{
				alert('Please enter a comment');
			}
		}
		
		$('#newcomment').keydown(function( event ) {
			if ( event.keyCode == 13 ) {
				event.preventDefault();
				addComment();
			}				
		});
		
		function removeComment(t){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/commentremove/".$property->id) }}',
				data: {t: t},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					loadCommentsEdit();
				},
				error: function (data) {
					alert('Something went wrong, please try again!');
				}
			});
		}
		
		function loadChangesEdit(){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'GET',
				url: '{{ url("/property/changes/".$property->id) }}',
				cache: false,
				success: function (data) {
					jQuery('#existingChangesEdit').html(data);
				},
				error: function (data) {
					jQuery('#existingChangesEdit').html(data);
					jQuery.ajax({
						type: 'GET',
						url: '{{ url("/property/changes/".$property->id) }}',
						cache: false,
						success: function (data) {
							jQuery('#existingChangesEdit').html(data);
						},
						error: function (data) {
							jQuery.ajax({
								type: 'GET',
								url: '{{ url("/property/changes/".$property->id) }}',
								cache: false,
								success: function (data) {
									jQuery('#existingChangesEdit').html(data);
								},
								error: function (data) {
									jQuery('#existingChangesEdit').html('An error occurred '+data);
								}
							});
						}
					});
				}
			});
		}
		
		jQuery( document ).ready(function(){
			loadCommentsEdit();
			loadChangesEdit();
		});
		
		jQuery('form').on('focus', 'input[type=number]', function (e) {
			jQuery(this).on('mousewheel.disableScroll', function (e) {
				e.preventDefault();
			});
		});
		jQuery('form').on('blur', 'input[type=number]', function (e) {
			jQuery(this).off('mousewheel.disableScroll');
		});
		
		jQuery('input[name="poc_id"]').on('change', function(e){
			var poc = jQuery('input[name="poc_id"]:checked').val();
			if (poc == 1){
				jQuery('input[name="commission"]').val('50%');
			}else if (poc == 2){
				jQuery('input[name="commission"]').val('50%');
			}else if (poc == 3){
				jQuery('input[name="commission"]').val('0%');
			}
		});
		
	</script>
	
	
	

@include('partials.property.building-modal')
@include('partials.property.vendor-owner-modal')
@include('partials.property.vendor-agent-modal')
@endsection

@section('footer-script')
<?php
/**
 * Part A) easy automation popup flow:
 * 1) easyAutocomplete setting
 * 2) create new item ajax
 * 3) set content
 *
 * Part B) control - field display control by radio button
 */
?>
@include('partials.property.building-modal-control')
@include('partialsnew.vendor-owner-modal-control')
@include('partialsnew.vendor-agent-modal-control')
@include('partials.property.hongkonghomesphotos-control')
@include('partials.property.hongkonghomesphotosnew-control')

@include('partialsnew.include-plupload-control', array(
    'id'      => 'property_nest_photos',
    'inc_lib' => '1',
    'url'     => secure_url('/property/upload'),
    'types'   => array('image'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'nest-photo',
                    'type'  => 'database',
                ),
    ))

@include('partialsnew.include-plupload-control', array(
    'id'      => 'property_nest_photos_website',
    'inc_lib' => '1',
    'url'     => secure_url('/property/upload'),
    'types'   => array('image'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'nest-photo',
                    'type'  => 'website',
                ),
    ))

@include('partialsnew.include-plupload-control', array(
    'id'      => 'property_other_photos',
    'url'     => secure_url('/property/upload'),
    'types'   => array('image'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'other-photo',
                    'type'  => 'database',
                ),
    ))
@endsection