
<div class="nest-property-pic-slider-wrapper">
	<div id="show-pics-slider-big" class="nest-property-pic-slider-bigimg-wrapper" style="display:none;">
		@foreach($photos as $group=>$section)
			@if ($section->count() > 0)
				@foreach($section as $index=>$file)
					<div class="nest-property-pic-slider-bigimg-slide">
						<a id="downloadlink{{ $file->id }}" href="{{ $file->getShowUrlDownload() }}" target="_blank">
							<img id="bigimage{{ $file->id }}" src="{{ $file->getShowUrl() }}" class="nest-property-pic-slider-bigimg" />
						</a>
					</div>
				@endforeach
			@endif
		@endforeach
	</div>
	<div id="show-pics-slider">
		@php
			$cnt = 0;
		@endphp
		@foreach($photos as $group=>$section)
			@if ($section->count() > 0)
				@foreach($section as $index=>$file)
					<div><a href="javascript:;" onclick="showbigimage({{ $cnt }});"><img src="{{ $file->getShowUrlModal() }}" height="200" /></a></div>
					@php
						$cnt++;
					@endphp
				@endforeach
			@endif
		@endforeach
	</div>
</div>