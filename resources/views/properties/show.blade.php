@extends('layouts.app')

@section('content')
    <div class="nest-new">
		<div class="row">
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>{{ $property->name }}</h4>
					</div>

					<div class="panel-body">
						<!-- Display Validation Errors -->
						@include('common.errors')

						<!-- New Property Form -->
						<form action="{{ url('property/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
							{{ csrf_field() }}

							<div class="form-group">
								<label for="property-type" class="col-sm-3 control-label">Type</label>
								<div class="col-sm-6">
									 <span>{{ $property->type() }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-unit" class="col-sm-3 control-label">Unit</label>
								<div class="col-sm-6">
									<span>{{ $property->unit }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-name" class="col-sm-3 control-label">Property Name</label>
								<div class="col-sm-6">
									<span>{{ $property->name }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-address1" class="col-sm-3 control-label">Address1</label>
								<div class="col-sm-6">
									<span>{{ $property->address1 }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-address2" class="col-sm-3 control-label">Address2</label>
								<div class="col-sm-6">
									<span>{{ $property->address2 }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-district_id" class="col-sm-3 control-label">District</label>
								<div class="col-sm-6">
									<span>{{ $property->district() }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-country_code" class="col-sm-3 control-label">Country</label>
								<div class="col-sm-6">
									<span>{{ $property->country_code }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-year_built" class="col-sm-3 control-label">Year Built</label>
								<div class="col-sm-6">
									<span>{{ $property->year_built }}</span>
								</div>
							</div>

							<hr/>

							<div class="form-group">
								<label for="property-floor_zone" class="col-sm-3 control-label">Floor Zone</label>
								<div class="col-sm-6">
									<span>{{ $property->floor_zone }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-layout" class="col-sm-3 control-label">Layout</label>
								<div class="col-sm-6">
									<span>{{ $property->layout }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-description" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-6">
									<span>{{ $property->description }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-bedroom" class="col-sm-3 control-label">Bedroom</label>
								<div class="col-sm-6">
									<span>{{ $property->bedroom }}</span>
								</div>
							</div>
							
							<div class="form-group">
								<label for="property-bathroom" class="col-sm-3 control-label">Bathroom</label>
								<div class="col-sm-6">
									<span>{{ $property->bathroom }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-maidroom" class="col-sm-3 control-label">Maidroom</label>
								<div class="col-sm-6">
									<span>{{ $property->maidroom }}</span>
								</div>
							</div>

							<hr/>

							<div class="form-group">
								<label for="property-gross_area" class="col-sm-3 control-label">Gross Area (sq.ft.)</label>
								<div class="col-sm-6">
									<span>{{ $property->gross_area }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-saleable_area" class="col-sm-3 control-label">Saleable Area (sq.ft.)</label>
								<div class="col-sm-6">
									<span>{{ $property->saleable_area }}</span>
								</div>
							</div>

							<div class="form-group form-checkbox-group">
								<label for="property-outdoors" class="col-sm-3 control-label">Outdoor Space</label>
								<div class="col-sm-6">
									@if(!empty($outdoor_ids) && !empty($outdoors))
									@foreach ($outdoor_ids as $outdoor_id => $outdoor)
										@if (in_array($outdoor_id, $outdoors))
										<label for="property-outdoors-{{$outdoor_id}}" class="control-label">
											{{ $outdoor}}: {{ $outdoorareas[strtolower($outdoor)] }} sq.ft.
										</label>
										@endif
									@endforeach
									@endif
								</div>
							</div>

							<div class="form-group form-checkbox-group">
								<label for="property-features" class="col-sm-3 control-label">Features</label>
								<div class="col-sm-6">
									@if(!empty($feature_ids) && !empty($features))
									@foreach ($feature_ids as $feature_id => $feature)
										@if (in_array($feature_id, $features))
										<label for="property-features-{{$feature_id}}" class="control-label">
											{{ $feature}}<?php if($feature_id == 5):?>: {{ $property->car_park }}<?php endif;?>
										</label>
										@endif
									@endforeach
									@endif
								</div>
							</div>


							<hr/>

	<div id="property-sale-section">
							<div class="form-group">
								<label for="property-asking_sale" class="col-sm-3 control-label">Asking Sale (HK$)</label>
								<div class="col-sm-6">
									<span>{{ $property->asking_sale() }}</span>
								</div>
							</div>
	</div>
	<div id="property-rent-section">
							<div class="form-group">
								<label for="property-asking_rent" class="col-sm-3 control-label">Asking Rent (HK$)</label>
								<div class="col-sm-6">
									<span>{{ $property->asking_rent() }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-inclusive" class="col-sm-3 control-label">Inclusive</label>
								<div class="col-sm-6">
									 <span>{{ $property->inclusive==1?'yes':'no' }}</span>
								</div>
							</div>
		<div id="property-inclusive-section">
							<div class="form-group">
								<label for="property-management_fee" class="col-sm-3 control-label">Management Fee (HK$)</label>
								<div class="col-sm-6">
									<span>{{ $property->management_fee }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-government_rate" class="col-sm-3 control-label">Government Rate (HK$)</label>
								<div class="col-sm-6">
									<span>{{ $property->government_rate }}</span> per quarter
								</div>
							</div>
		</div>
	</div>
							<hr/>

							<div class="form-group">
								<label for="vendor-poc" class="col-sm-3 control-label">Point of Contact</label>
								<div class="col-sm-6">
									 <span>{{ $property->poc() }}</span>
								</div>
							</div>
							<div class="form-group">
								<label for="property-owner" class="col-sm-3 control-label">Owner Name</label>
								<div class="col-sm-6">
								@if(!empty($owner))
									<span><a target="show_vendor" href="{{ url('/vendor/show/' . $owner->id) }}">{{ $owner->basic_info() }} (ID: {{ $owner->id }})</a></span>
									@if($owner->email)<span>Email: {{ $owner->email }}</span>@endif
									@if($owner->tel)<span>Tel: {{ $owner->tel }}</span>@endif
								@else
									-
								@endif
								</div>
							</div>
	@if (count($reps) > 0)
	<div id="property-vendor-rep-section" style="display: block;">
							<div class="form-group">
								<label for="property-rep_id" class="col-sm-3 control-label">Rep Names</label>
								<div class="col-sm-6">
	@foreach ($reps as $rep)
	<div id="property-vendor-rep-checkbox-template" style="display: block">
										<span><a target="show_vendor" href="{{ url('/vendor/show/' . $rep->id) }}">{{ $rep->basic_info() }} tel:{{ $rep->tel }} (ID: {{ $rep->id }})</a></span>
										@if($rep->email)<span>Email: {{ $rep->email }}</span>@endif
										@if($rep->tel)<span>Tel: {{ $rep->tel }}</span>@endif
	</div>
	@endforeach
								</div>
							</div>
	</div>
	@endif
							<div class="form-group">
								<label for="property-agent" class="col-sm-3 control-label">Agency Name</label>
								<div class="col-sm-6">
								@if(!empty($agent))
									<span><a target="show_vendor" href="{{ url('/vendor/show/' . $agent->id) }}">{{ $agent->basic_info() }} (ID: {{$agent->id}})</a></span>
									@if($agent->email)<span>Email: {{ $agent->email }}</span>@endif
									@if($agent->tel)<span>Tel: {{ $agent->tel }}</span>@endif
								@else
									-
								@endif
								</div>
							</div>
	@if (count($agents) > 0)
	<div id="property-vendor-agent-section" style="display: block;">
							<div class="form-group">
								<label for="property-agent_id" class="col-sm-3 control-label">Agent Names</label>
								<div class="col-sm-6">
	@foreach ($agents as $agent)
	<div id="property-vendor-agent-checkbox-template" style="display: block">
										<span><a target="show_vendor" href="{{ url('/vendor/show/' . $agent->id) }}">{{ $agent->basic_info() }} tel:{{ $agent->tel }} (ID: {{ $agent->id }})</a></span>
										@if($agent->email)<span>Email: {{ $agent->email }}</span>@endif
										@if($agent->tel)<span>Tel: {{ $agent->tel }}</span>@endif
	</div>
	@endforeach
								</div>
							</div>
	</div>
	@endif

	<div id="property-key-loc-other-section">
							<div class="form-group">
								<label for="property-key_loc_other" class="col-sm-3 control-label">Key Location Other</label>
								<div class="col-sm-6">
									<span>{{ $property->key_loc_other }}</span>
								</div>
							</div>
	</div>

							<div class="form-group">
								<label for="property-door_code" class="col-sm-3 control-label">Door Code</label>
								<div class="col-sm-6">
									<span>{{ $property->door_code }}</span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-agency_fee" class="col-sm-3 control-label">Agency Fee</label>
								<div class="col-sm-6">
									<span>{{ $property->agency_fee }}</span>
								</div>
							</div>



							<div class="form-group">
								<label for="test-available_date" class="col-sm-3 control-label">Available Date</label>
								<div class="col-sm-6">
									<span>{{ $property->available_date() }}</span>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						Media
					</div>
					<div class="panel-body">
	@foreach($photos as $group=>$section)
	@if ($section->count() > 0)
						<div class="form-group">
							<label for="property-media" class="col-sm-3 control-label">{{ $media_ids[$group] }}</label>
							<div class="col-sm-9">
								@foreach($section as $index=>$file)
								{{ ($index+1) }}: <a href="{{ $file->getShowUrl() }}" target="property_large_image"><img src="{{ $file->getShowUrl() }}" width="200" style="margin-bottom: 10px;" /></a><br/>
								@endforeach
							</div>
						</div>
	@endif
	@endforeach
					</div>
				</div>


				<div class="panel panel-default">
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<a class="btn btn-primary" href="{{ url('property/edit/' . $property->id) }}">
										<i class="fa fa-btn fa-pencil-square-o"></i>Edit
									</a>
								</div>
							</div>
					</div>
				</div>
			</div>
			<style>
				.nest-comment-remove{
					display:none;
				}
			</style>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Comments</h4>
					</div>
					<div class="panel-body">
						<div class="nest-comments" id="existingCommentsShowOld"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
			</div>
		</div>
		
		<script>			
			function loadCommentsShowOld(){
				jQuery.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/comments/".$property->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsShowOld').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/comments/".$property->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsShowOld').html(data);
							},
							error: function (data) {
								jQuery.ajax({
									type: 'GET',
									url: '{{ url("/comments/".$property->id) }}',
									cache: false,
									success: function (data) {
										jQuery('#existingCommentsShowOld').html(data);
									},
									error: function (data) {
										console.log(data);
										jQuery('#existingCommentsShowOld').html('An error occurred '+data);
									}
								});
							}
						});
					}
				});
			}
			
			jQuery( document ).ready(function(){
				loadCommentsShowOld();
			});
		</script>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
<style>
    #property-sale-section {
        display: block !important;
    }
</style>

@include('partials.property.building-modal')
@include('partials.property.vendor-owner-modal')
@include('partials.property.vendor-agent-modal')
    </div>
@endsection

@section('footer-script')
<?php 
/**
 * Part A) easy automation popup flow:
 * 1) easyAutocomplete setting
 * 2) create new item ajax
 * 3) set content
 *
 * Part B) control - field display control by radio button
 */
?>
@include('partials.property.building-modal-control')
@include('partials.property.vendor-owner-modal-control')
@include('partials.property.vendor-agent-modal-control')
@include('partials.property.field-display-control')
@endsection