
<div class="nest-property-list-detail-wrapper">
	@foreach ($clients as $cindex => $c)
		@if ($cindex == 0)
			<div class="nest-property-list-detail-first-row row">
		@else
			@if ($cindex % 2 == 0)
				<div class="row">
			@else
				<div class="row nest-striped">
			@endif
		@endif
			<div class="col-xs-8">
				<a href="{{ url('/client/show/'.$c->client_id) }}" target="_blank">{{ $c->name() }}</a><br />
				@if (trim($c->tel) != '')
					Tel: {!! $c->telLinked() !!}<br />
				@endif
				@if (trim($c->email) != '')
					Email: {{ trim($c->email) }}
				@endif
			</div>
			<div class="col-xs-4 text-right">
				<a href="{{ url('/shortlist/show/'.$c->shortlist_id) }}" target="_blank">View Shortlist</a>
			</div>
		</div>
	@endforeach
</div>