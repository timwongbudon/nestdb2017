@extends('layouts.app')

@section('content')

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul style="list-style: none; padding:0">
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif




    <div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
            <h2>To be Deleted Properties</h2>
		</div>
	</div>

    <div>
		<div class="nest-buildings-result-wrapper">

            <table class="table">
                <tr>
                    <th>Property</th>
                    <th>Property ID</th>
                    <th>Deleted by</th>
                    <th>Date Deleted</th>
                    
                    <th class="text-center" width="20%">Action</th>
                </tr>
                
                @foreach($propertyDeleteLogs as $pdl)
                <tr>
                    <td> <a href="{{url('/property/edit/'.$pdl->deleted_property->id)}}"> {{$pdl->deleted_property->name}} </a> </td>
                    <td>
                        <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $pdl->property_id }}, '{{ str_replace("'", "\'", $pdl->deleted_property->name) }}', '')">
                        {{$pdl->property_id}}
                        </a>
                    </td>
                    <td> {{$pdl->deletedby->name}}</td>                    
                    <td>{{date('d/m/Y H:i',strtotime($pdl->created_at))}}</td>
                    <td>
                        <form action="{{url('property/delete/decline')}}" method="post">
                            <input type="hidden" name="id" value="{{$pdl->id}}" >
                            <input type="hidden" name="deleted_property_id" value="{{$pdl->deleted_property->id}}" >
                            {{ csrf_field() }}
                            <button type="submit" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-thumbs-down"></i>Decline</button>
                        </form>  


                        <form action="{{url('property/delete/confirm')}}" method="post">
                            <input type="hidden" name="id" value="{{$pdl->id}}" >
                            <input type="hidden" name="deleted_property_id" value="{{$pdl->deleted_property->id}}" >
                            {{ csrf_field() }}
                            <button type="submit" class="nest-button nest-right-button btn btn-primary" onclick="return confirm('Are you sure?')"><i class="fa fa-btn fa-check"></i>Confirm</button>
                        </form>   
                        
                        

                    </td>
                </tr>
                @endforeach
            
                
                @if(count($propertyDeleteLogs) == 0)
                <tr>
                    <td colspan="4"> No data found</td>
                </tr>
                @endif
            </table>

        </div>

    </div>
@endsection