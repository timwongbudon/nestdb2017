@extends('layouts.app')

@section('content')
    <style>
        .index-image {
            float: left;
            display: block; 
            background-size: 80px 80px; 
            width: 80px; 
            height: 80px;
        }
        .property-table strong {
            font-size: 15px;
        }
    </style>


@include('partials.property.search-form', array('template_type'=>'frontend'))
    
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Properties {{-- Request::getRequestUri() --}}
                    </div>

                    <div class="panel-body">
                    <form id="select_frontend_property" action="{{ url('frontend/select') }}" method="post">
                        {{ csrf_field() }}
                        <table class="table table-striped property-table">
                            <thead>
                                <th style="width: 3%"><div>{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!} </div></th>
                                <th>Web</th>
                                <th style="width: 5%">ID</th>
                                @if(NestPreference::isShowPhoto())<th>Photo</th>@endif
                                <th style="max-width: 400px">Property</th>
                                <th>Type</th>
                                <th style="width: 8%">&nbsp;</th>
                                <th style="width: 8%">&nbsp;</th>
                                <!-- <th style="width: 8%">&nbsp;</th> -->
                            </thead>
                            <tbody>
                                @foreach ($properties as $property)
                                    <tr>
                                        <td class="table-text"><div>{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!} </div></td>
                                        <td>{{ $property->web?'yes':'no' }}</td>
                                        <td class="table-text"><div>{{ $property->id }}
                                        @if($property->external_id)({{$property->external_id}})@endif
                                        </div></td>
                                        @if(NestPreference::isShowPhoto())
                                        <td class="table-text"> 
                                            @if(!empty($property->featured_photo_url))
                                            <img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}" alt="" width="300" />
                                            @endif
                                        </td>
                                        @endif
                                        <td class="table-text"><div>
                                             <strong>{{ $property->shorten_building_name(0) }}</strong>
                                            @if($property->hot == 1)
                                                <img src="{{ url('hot.gif')}} " alt="">
                                            @endif
                                            <br/>
                                            <?php $rep_ids = $property->rep_ids?explode(',',$property->rep_ids):array(); ?>
                                            {{ $property->fix_address1() }}<br/><hr style="margin: 5px 0;">

                                            @if($property->unit)
                                                Unit: {{ $property->unit }}<br/>
                                            @endif
                                            
                                            Asking Price: {{ $property->price_db_searchresults() }}<br/>

                                            @if($property->bedroom_count())
                                                Bedrooms: {{ $property->bedroom_count() }}<br/>
                                            @endif

                                            @if($property->bathroom_count())
                                                Bathrooms: {{ $property->bathroom_count() }}<br/>
                                            @endif
                                            
                                            @if($property->size)
                                                Size: {{ $property->property_size() }}<br/>
                                            @endif

                                            @if($property->poc_id)
                                                Point of contact: {{ $property->poc() }}<br/>
                                            @endif
                                            
                                            @if(!empty($property['building']->tel))
                                                Mgmt Office tel: {{$property['building']->tel}}<br/>
                                            @endif
                                            
                                            @if(!empty($property['owner']))
                                                Owner: {{$property['owner']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['reps']))
                                                @foreach($property['reps'] as $i => $rep)
                                                Rep {{$i+1}}: {{$rep->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property['agent']))
                                                Agency: {{$property['agent']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['agents']))
                                                @foreach($property['agents'] as $i => $agent)
                                                Agent {{$i+1}}: {{$agent->basic_info()}}<br/>
                                                @endforeach
                                            @endif
											
                                            @if(!empty($property->comments) && $property->comments != '[]')
                                                Last Comment: {{$property->show_last_comment()}}
                                            @endif

                                            @if(empty($property['agent']) && empty($property['owner']) && !empty($property->contact_info))
                                                Contact Info: {{$property->contact_info}}<br/>
                                            @endif

                                            @if(!empty($property->available_date()))
                                            Available Date: {{ $property->available_date() }}
                                            @endif
                                            <hr style="margin: 5px 0;">
                                            {{ $property->show_timestamp() }}
                                        </div></td>
                                        <td class="table-text"><div>{{ $property->type() }}</div></td>

                                        <!-- Property Delete Button -->
                                        <td colspan="2">
                                            <a class="btn btn-primary" href="{{ url('property/show/'.$property->id) }}">
                                                <i class="fa fa-btn fa-info-circle"></i>View
                                            </a>
											&nbsp;
                                            <a class="btn btn-primary" href="{{ url('property/edit/'.$property->id) }}">
                                                <i class="fa fa-btn fa-pencil-square-o"></i>Edit
                                            </a>
											@if($property->web)
												<!-- @if($property->type_id == 1 || $property->type_id == 3)													
													<a class="btn btn-primary" target="_blank" style="margin-top:10px;" href="{{ url('property/featrent/'.$property->id) }}">
														Feature for Rent
													</a>
												@endif
												@if($property->type_id == 2 || $property->type_id == 3)											
													<a class="btn btn-primary" target="_blank" style="margin-top:10px;" href="{{ url('property/featsale/'.$property->id) }}">
														Feature for Sale
													</a>												
												@endif -->
												<a class="btn btn-primary" target="_blank" style="margin-top:10px;" href="{{ url('property/feathome/'.$property->id) }}">
													Feature on Homepage
												</a>	
											@endif
                                        </td>
                                        <!-- Property Delete Button -->
                                        <!-- <td>
                                            <form action="{{url('property/' . $property->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-property-{{ $property->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ Form::hidden('url', Request::getRequestUri()) }}
                        {{ Form::hidden('action', 'select') }}
                    </form>
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $properties->currentPage() }} of {{ $properties->lastPage() }} (Total: {{ $properties->total() }} items)</div>
                        {{ $properties->links() }}  
                    </div>
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Add Property Button -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button id="add_frontend_property" class="btn btn-primary">
                                    <i class="fa fa-btn fa-plus"></i>Add Properties to Front End Website
                                </button>
                                <button id="remove_frontend_property" class="btn btn-primary">
                                    <i class="fa fa-btn fa-minus"></i>Remove Properties to Front End Website
                                </button>
                            </div>
                        </div>
                </div>
            </div>
@endsection

@section('footer-script')
@include('partials.property.building-modal-control')
<script type="text/javascript">
$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form#select_frontend_property input').prop('checked', $(this).is(":checked"));
    });

    $('button#add_frontend_property.btn').click(function(event){
        event.preventDefault();
        submit_action('select');
        return false;
    });
    $('button#remove_frontend_property.btn').click(function(event){
        event.preventDefault();
        submit_action('deselect');
        return false;
    });
    function submit_action(action) {
        if ($('form#select_frontend_property input.property-option:checked').length > 0) {
            if (action == 'deselect') {
                $('form#select_frontend_property input[name=action]').val('deselect');
            }
            $('form#select_frontend_property').submit();
        } else {
            alert('Please choose property to put into frontend property list.');
        }
    }
});
</script>
@endsection