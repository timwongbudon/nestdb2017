@extends('layouts.app')

@section('content')

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul style="list-style: none; padding:0">
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif




    <div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
            <h2>Deleted Properties</h2>
		</div>

        <form action="" method="GET">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    
                    <input type="text" name="search" class="form-control" value="{{$search !== null ? $search : ''}}" placeholder="Search by: Property ID or Property Name">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <button type="submit" name="action" value="search" class="nest-property-list-search-button nest-button btn btn-default"><i class="fa fa-btn fa-search"></i>Search </button>
                    <a href="{{url('/property/deletedproperties')}}" class="nest-property-list-search-button nest-button btn btn-default">Reset </a>
                </div>

                <div class="clearfix"></div>
                
            </div>
            
            
        </form>
	</div>

    <div>
		<div class="nest-buildings-result-wrapper">

            <table class="table">
                <tr>
                    <th>Property ID</th>
                    <th>Property</th>
                    <th>Property Address</th>
                    <th>Date Deleted</th>
                    <th>Deleted by</th>                    
                    
                    <th class="text-center" width="10%">Action</th>
                </tr>
                
                @foreach($data as $pdl)
                <tr>
                    <td> {{$pdl['id']}}</td>
                    <td> <a href="{{url('/property/edit/'.$pdl['id'])}}"> {{$pdl['name']}} </a> </td>
                    <td> {{$pdl['address1']}}</td>
                    <td>{{date('d/m/Y H:i',strtotime($pdl['deleted_at']))}}</td>
                    <td>
                       {{ $pdl['deleted_by'] }}
                       
                    </td>
                    <td>

                        <form action="{{url('property/restoreproperty')}}" method="post">
                            <input type="hidden" name="id" value="{{$pdl['id']}}" >                            
                            {{ csrf_field() }}
                            <button type="submit" class="nest-button nest-right-button btn btn-primary" onclick="return confirm('Are you sure?')">Restore</button>
                        </form>   
                    </td>
                 
                 
                </tr>
                @endforeach
            
                
                @if(count($propertyDeleteLogs) == 0)
                <tr>
                    <td colspan="4"> No data found</td>
                </tr>
                @endif
            </table>
            {{$propertyDeleteLogs->links()}}

        </div>

    </div>
@endsection