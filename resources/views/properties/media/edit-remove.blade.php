@extends('layouts.app')

@section('content')

<form action="{{ url('property/media/remove') }}" method="POST" class="form-horizontal deletable-pictures" enctype="multipart/form-data">
{{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Remove Property Media ({{ $group }}) - tick the checkbox <b>OVER</b> image then click remove
					<div style="float:right;">
						<input type="checkbox" id="checkall" value="checkall" /> Check All
					</div>
                </div>
				
				<script>
					jQuery("#checkall").on('change', function(){
						if (jQuery("#checkall").prop('checked')){
							jQuery('.deletable-pictures .file-option').prop('checked', true);
						}else{
							jQuery('.deletable-pictures .file-option').prop('checked', false);
						}
					});
				</script>
				
                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')
                    <ul id="sortable-1" class="container sortable-listing">
                        @if (count($files) > 0)
                            @foreach($files as $index => $file)
                        <li id="{{ $file->id }}" class="default col-sm-4 text-center" style="overflow:hidden;">
                            {!! Form::checkbox('options_ids[]', $file->id, null, ['class'=>'file-option', 'id'=>'file-option_id-checkbox-' . $file->id]) !!} 
                            <div>{{ Html::image( $file->_get_url('system-thumbnail'), '', array('class'=>'col-sm-12') ) }}</div>
                        </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="form-group col-sm-4">
                            <div class="col-sm-12">
                                    {{ Form::hidden('property_id', $property->id) }}
                                    {{ Form::hidden('field', $field) }}
                                    {{ Form::hidden('group', $group) }}
                                    <button id="remove_option" class="btn btn-danger">
                                        <i class="fa fa-btn fa-remove"></i>Remove
                                    </button>
                            </div>
                        </div>
                </div>
            </div>
</form>

<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
{{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Trashed Property Media ({{ $group }}) - tick the checkbox <b>OVER</b> image then click restore
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')
                    <ul id="sortable-1" class="container sortable-listing">
                        @if (count($trashed_files) > 0)
                            @foreach($trashed_files as $index => $file)
                        <li id="{{ $file->id }}" class="default col-sm-4 text-center" style="overflow:hidden;">
                            {!! Form::checkbox('options_ids[]', $file->id, null, ['class'=>'file-option', 'id'=>'file-option_id-checkbox-' . $file->id]) !!} 
                            <div>{{ Html::image( $file->_get_url('system-thumbnail'), '', array('class'=>'col-sm-12') ) }}</div>
                        </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
@if (count($trashed_files) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="form-group col-sm-4">
                            <div class="col-sm-12">
                                    {{ Form::hidden('property_id', $property->id) }}
                                    {{ Form::hidden('field', $field) }}
                                    {{ Form::hidden('group', $group) }}
                                    <button id="restore_option" class="btn btn-primary">
                                        <i class="fa fa-btn fa-undo"></i>Restore
                                    </button>
                                    <button id="permanent_remove_option" class="btn btn-danger">
                                        <i class="fa fa-btn fa-remove"></i>Permanent Remove
                                    </button>
                            </div>
                        </div>
                </div>
            </div>
@endif
</form>
    

@endsection

@section('footer-script')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
#sortable-1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 100%;
}
#sortable-1 li {
    font-size: 17px;
    padding: 0 !important;
    max-height: 269px;
    margin-bottom: 10px;
}
#sortable-1 li img {
    padding: 0 !important;
}
.default {
    background: #f5f5f5;
    border: 1px solid #DDDDDD;
    color: #333333;
}
</style>
<script>
$(document).ready(function(){
    $("#reorder_option").click(function(){
        event.preventDefault();
        $('form').submit();
    });
    $("#restore_option").click(function(){
        event.preventDefault();
        $('form').attr('action', "{{ url('property/media/restore') }}");
        $('form').submit();
    });
    $("#permanent_remove_option").click(function(){
        event.preventDefault();
        $('form').attr('action', "{{ url('property/media/permanent-remove') }}");
        $('form').submit();
    });
});

</script>
@endsection