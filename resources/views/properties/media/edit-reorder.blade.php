@extends('layouts.app')

@section('content')

<form action="{{ url('property/media/reorder') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
{{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reorder Property Media ({{ $group }}) - drag and drop to move images then save.
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')
                    <ul id="sortable-1" class="container sortable-listing">
                        @if (count($files) > 0)
                            @foreach($files as $index => $file)
								<li id="{{ $file->id }}" class="default col-sm-4" style="overflow:hidden;">
									{{ Form::hidden('orders[]', $file->id) }}
									{{ Html::image( $file->_get_url('system-thumbnail'), '', array('class'=>'col-sm-12') ) }}
								</li>
                            @endforeach
                        @endif
                    </ul>
					<div class="clearfix"></div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Add Property Button -->
                        <div class="form-group col-sm-4">
                            <div class="col-sm-12">
                                    {{ Form::hidden('property_id', $property->id) }}
                                    {{ Form::hidden('field', $field) }}
                                    {{ Form::hidden('group', $group) }}
                                    <button id="reorder_option" class="btn btn-primary">
                                        <i class="fa fa-btn fa-edit"></i>Save
                                    </button>
                            </div>
                        </div>
                </div>
            </div>
</form>

@endsection

@section('footer-script')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
#sortable-1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 100%;
}
#sortable-1 li {
    font-size: 17px;
    cursor: move;
    padding: 0 !important;
    max-height: 269px;
}
#sortable-1 li img {
    padding: 0 !important;
}
.default {
    background: #f5f5f5;
    border: 1px solid #DDDDDD;
    color: #333333;
}
</style>
<script>
$(document).ready(function(){
    $( "#sortable-1" ).sortable();
});
$(document).ready(function(){
    $("#reorder_option").click(function(){
        event.preventDefault();
        $('form').submit();
    });
});

</script>
@endsection