@extends('layouts.app')
@section('content')


<div class="nest-new">
	<div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('property/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>New Property</h4>
						</div>

						<div class="panel-body">
							@include('common.errors')
							
							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Property Name</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="name" id="property-name" class="form-control" value="{{ old('name') }}">
									<span id="property-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Display Name</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="display_name" id="property-display_name" class="form-control" value="{{ old('display_name') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Address</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Unit</div>
								</div>
								<div class="col-xs-3">
									<input type="text" name="unit" id="property-unit" class="form-control" value="{{ old('unit', $property->unit) }}">
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Floor</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="floornr" id="property-floornr" class="form-control" value="{{ old('floornr', $property->floornr) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 {!! Form::select('district_id', $district_ids, $property->district_id, ['id'=>'property-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
								</div>
								<div class="col-xs-6">
									{!! Form::select('country_code', $country_ids, $property->country_code, ['class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Building ID</div>
								</div>
								<div class="col-xs-3">
									<input type="text" name="building_id" id="building_id" class="form-control" value="{{ old('building_id', $property->building_id) }}" />
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Year Built</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built', $property->year_built) }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-space.jpg') }}" /></div>Gross</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="gross_area" id="property-gross_area" class="form-control" value="{{ old('gross_area', $property->gross_area) }}" min="0">
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-space.jpg') }}" /></div>Saleable</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="saleable_area" id="property-saleable_area" class="form-control" value="{{ old('saleable_area', $property->saleable_area) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-bed.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="bedroom" id="property-bedroom" class="form-control" value="{{ old('bedroom', $property->bedroom) }}" min="0">
								</div>
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-bath.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="bathroom" id="property-bathroom" class="form-control" value="{{ old('bathroom', $property->bathroom) }}" min="0">
								</div>
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-maid.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="maidroom" id="property-maidroom" class="form-control" value="{{ old('maidroom', $property->maidroom) }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Floor Zone</div>
								</div>
								<div class="col-xs-6">
									{!! Form::select('floor_zone', array('High' => 'High', 'Middle' => 'Middle', 'Low' => 'Low', 'Ground Floor' => 'Ground Floor'), $property->floor_zone, ['class'=>'form-control', 'placeholder' => 'Choose a floor zone...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Layout</div>
								</div>
								<div class="col-xs-6">
									{!! Form::select('layout', array('Stand-Alone' => 'Stand-Alone', 'Penthouse' => 'Penthouse', 'Simplex' => 'Simplex', 'Duplex' => 'Duplex', 'Triplex' => 'Triplex'), $property->layout, ['class'=>'form-control', 'placeholder' => 'Choose a layout...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Description</div>
								</div>
								<div class="col-xs-8">
									{!! Form::textarea('description', $property->description, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
								</div>
								<div class="clearfix"></div>
							</div>

						

						
						</div>
					</div>
				</div>
				<div class="col-md-4">
							
							
							
							
						


                        <div class="form-group">
                            <label for="property-type" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-6">
                                <label for="property-type-1" class="control-label">{!! Form::radio('type_id', 1, old('type_id') == 1, ['id'=>'property-type-1']) !!} Rent</label>
                                <label for="property-type-2" class="control-label">{!! Form::radio('type_id', 2, old('type_id') == 2, ['id'=>'property-type-2']) !!} Sale</label>
                                <label for="property-type-3" class="control-label">{!! Form::radio('type_id', 3, old('type_id') == 3, ['id'=>'property-type-3']) !!} Rent & Sale</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-unit" class="col-sm-3 control-label">Unit</label>
                            <div class="col-sm-6">
                                <input type="text" name="unit" id="property-unit" class="form-control" value="{{ old('unit') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-type" class="col-sm-3 control-label">Building id</label>
                            <div class="col-sm-6">
                                <input type="text" name="building_id" id="building_id" class="form-control" value="{{ old('building_id') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-address1" class="col-sm-3 control-label">Address1</label>
                            <div class="col-sm-6">
                                <input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-address2" class="col-sm-3 control-label">Address2</label>
                            <div class="col-sm-6">
                                <input type="text" name="address2" id="property-address2" class="form-control" value="{{ old('address2') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-district_id" class="col-sm-3 control-label">District</label>
                            <div class="col-sm-6">
                                {!! Form::select('district_id', $district_ids, '', ['id'=>'property-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-country_code" class="col-sm-3 control-label">Country</label>
                            <div class="col-sm-6">
                                {!! Form::select('country_code', $country_ids, 'hk', ['id'=>'property-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-year_built" class="col-sm-3 control-label">Year Built</label>
                            <div class="col-sm-6">
                                <input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built') }}" min="0">
                            </div>
                        </div>

                        <hr/>

                        <div class="form-group">
                            <label for="property-floor_zone" class="col-sm-3 control-label">Floor Zone</label>
                            <div class="col-sm-6">
                                {!! Form::select('floor_zone', array('High' => 'High', 'Middle' => 'Middle', 'Low' => 'Low', 'Ground Floor' => 'Ground Floor'), '', ['class'=>'form-control', 'placeholder' => 'Choose a floor zone...']); !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-layout" class="col-sm-3 control-label">Layout</label>
                            <div class="col-sm-6">
                                {!! Form::select('layout', array('Duplex' => 'Duplex', 'Penthouse' => 'Penthouse', 'Simplex' => 'Simplex', 'Triplex' => 'Triplex'), '', ['class'=>'form-control', 'placeholder' => 'Choose a layout...']); !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-description" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('description', '', ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-bedroom" class="col-sm-3 control-label">Bedroom</label>
                            <div class="col-sm-6">
                                <input type="number" step="0.5" name="bedroom" id="property-bedroom" class="form-control" value="{{ old('bedroom') }}" min="0">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="property-bathroom" class="col-sm-3 control-label">Bathroom</label>
                            <div class="col-sm-6">
                                <input type="number" step="0.5" name="bathroom" id="property-bathroom" class="form-control" value="{{ old('bathroom') }}" min="0">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-maidroom" class="col-sm-3 control-label">Maidroom</label>
                            <div class="col-sm-6">
                                <input type="number" step="0.5" name="maidroom" id="property-maidroom" class="form-control" value="{{ old('maidroom') }}" min="0">
                            </div>
                        </div>

                        <hr/>

                        <div class="form-group">
                            <label for="property-gross_area" class="col-sm-3 control-label">Gross Area (sq.ft.)</label>
                            <div class="col-sm-6">
                                <input type="number" name="gross_area" id="property-gross_area" class="form-control" value="{{ old('gross_area') }}" min="0">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-saleable_area" class="col-sm-3 control-label">Saleable Area (sq.ft.)</label>
                            <div class="col-sm-6">
                                <input type="number" name="saleable_area" id="property-saleable_area" class="form-control" value="{{ old('saleable_area') }}" min="0"> 
                            </div>
                        </div>

                        <div class="form-group form-checkbox-group">
                            <label for="property-outdoors" class="col-sm-3 control-label">Outdoor Space</label>
                            <div class="col-sm-6">
                            @foreach ($outdoor_ids as $outdoor_id => $outdoor)
                                <label for="property-outdoors-{{$outdoor_id}}" class="control-label">{!! Form::checkbox('outdoors[]', $outdoor_id, in_array($outdoor_id, array()), ['id'=>'property-outdoors-'.$outdoor_id]) !!} {{ $outdoor}}
                                    <input type="number" name="outdoorareas[{{ strtolower($outdoor) }}]" id="property-outdoor-{{ strtolower($outdoor) }}" class="form-control" value="" min="0">sq.ft.
                                </label>
                            @endforeach
                            </div>
                        </div>

                        <div class="form-group form-checkbox-group">
                            <label for="property-features" class="col-sm-3 control-label">Features</label>
                            <div class="col-sm-6">
                            @foreach ($feature_ids as $feature_id => $feature)
                                 <label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, array()), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature}}
                                <?php if($feature_id == 5): // exception car park?>
                                     <input type="number" name="car_park" id="property-car_park" class="form-control" value="{{ old('car_park') }}" min="0">
                                <?php endif;?>
                                 </label>
                            @endforeach
                            </div>
                        </div>

                        <hr/>

<div id="property-sale-section">
                        <div class="form-group">
                            <label for="property-asking_sale" class="col-sm-3 control-label">Asking Sale (HK$)</label>
                            <div class="col-sm-6">
                                <input type="number" name="asking_sale" id="property-asking_sale" class="form-control" value="{{ old('asking_sale') }}" min="0"> 
                            </div>
                        </div>
</div>
<div id="property-rent-section">
                        <div class="form-group">
                            <label for="property-asking_rent" class="col-sm-3 control-label">Asking Rent (HK$)</label>
                            <div class="col-sm-6">
                                <input type="number" name="asking_rent" id="property-asking_rent" class="form-control" value="{{ old('asking_rent') }}" min="0"> 
                            </div>
                        </div>
</div>
                        <div class="form-group">
                            <label for="property-inclusive" class="col-sm-3 control-label">Inclusive</label>
                            <div class="col-sm-6">
                                 <label for="property-inclusive-1" class="control-label">{!! Form::radio('inclusive', 1, null, ['id'=>'property-inclusive-1']) !!} Yes</label>
                                 <label for="property-inclusive-2" class="control-label">{!! Form::radio('inclusive', 0, null, ['id'=>'property-inclusive-2']) !!} No</label>
                            </div>
                        </div>
<div id="property-inclusive-section">
                        <div class="form-group">
                            <label for="property-management_fee" class="col-sm-3 control-label">Managment Fee (HK$)</label>
                            <div class="col-sm-6">
                                <input type="number" name="management_fee" id="property-management_fee" class="form-control" value="{{ old('management_fee') }}" min="0"> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-government_rate" class="col-sm-3 control-label">Goverment Rate (HK$)</label>
                            <div class="col-sm-6">
                                <input type="number" name="government_rate" id="property-government_rate" class="form-control" value="{{ old('government_rate') }}" min="0"> per quarter
                            </div>
                        </div>
</div>

                        <hr/>

                        <div class="form-group">
                            <label for="vendor-poc" class="col-sm-3 control-label">Point of Contact</label>
                            <div class="col-sm-6">
                                 <label for="vendor-poc-1" class="control-label">{!! Form::radio('poc_id', 1, null, ['id'=>'vendor-poc-1']) !!} Owner</label>
                                 <label for="vendor-poc-2" class="control-label">{!! Form::radio('poc_id', 2, null, ['id'=>'vendor-poc-2']) !!} Rep</label>
                                 <label for="vendor-poc-3" class="control-label">{!! Form::radio('poc_id', 3, null, ['id'=>'vendor-poc-3']) !!} Agency</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-owner" class="col-sm-3 control-label">Owner Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="owner" id="property-owner" class="form-control" value="{{ old('owner') }}">
                                <span id="property-owner-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the owner name"><i class="fa fa-btn fa-close"></i></button></span>
                            </div>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label for="property-owner_id" class="col-sm-3 control-label">Owner id (hidden)</label>
                            <div class="col-sm-6">
                                <input type="hidden" name="owner_id" id="property-owner_id" class="form-control" value="{{ old('owner_id') }}"> 
                            </div>
                        </div>

<div id="property-vendor-rep-section" @if(empty(old('owner_id')))style="display: none;"@endif>
                        <div class="form-group">
                            <label for="property-rep_id" class="col-sm-3 control-label">Rep Name</label>
                            <div class="col-sm-6">
<div id="property-vendor-rep-checkbox-template" style="display: none">
                                <div class="col-sm-12" style="padding-left: 0;">
                                <label for="property-rep_id-checkbox-n" class="control-label">{!! Form::checkbox('rep_ids[]', 0, null, ['id'=>'property-rep_id-checkbox-n']) !!} </label>
                                </div>
</div>
<?php $property['allreps']= \App\Vendor::where('parent_id', old('owner_id'))->get();?>
@if(!empty(old('owner_id')) && count($property['allreps']) > 0)
<?php $rep_ids = !empty(old('rep_ids'))?old('rep_ids'):array();?>
@foreach($property['allreps'] as $rep)
<div class="col-sm-12" style="padding-left: 0;">
    <label for="property-rep_id-checkbox-{{$rep->id}}" class="control-label">
        {!! Form::checkbox('rep_ids[]', $rep->id, in_array($rep->id, $rep_ids), ['id'=>'property-rep_id-checkbox-'.$rep->id]) !!} {{ $rep->name() }}
    </label>
</div>
@endforeach
@endif
                            </div>
                        </div>
</div>

                        <div class="form-group">
                            <label for="property-agent" class="col-sm-3 control-label">Agency Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="agent" id="property-agent" class="form-control" value="{{ old('agent') }}">
                                <span id="property-agent-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the agent name"><i class="fa fa-btn fa-close"></i></button></span>
                            </div>
                        </div>

<div id="property-vendor-agent-section" @if(empty(old('agent_id')))style="display: none;"@endif>
                        <div class="form-group">
                            <label for="property-agent_id" class="col-sm-3 control-label">Agent Name</label>
                            <div class="col-sm-6">
<div id="property-vendor-agent-checkbox-template" style="display: none">
                                <div class="col-sm-12" style="padding-left: 0;">
                                <label for="property-agent_id-checkbox-n" class="control-label">{!! Form::checkbox('agent_ids[]', 0, null, ['id'=>'property-agent_id-checkbox-n']) !!} </label>
                                </div>
</div>
<?php $property['allagents']= \App\Vendor::where('parent_id', old('agent_id'))->get();?>
@if(!empty(old('agent_id')) && count($property['allagents']) > 0)
<?php $agent_ids = !empty(old('agent_ids'))?old('agent_ids'):array();?>
@foreach($property['allagents'] as $agent)
<div class="col-sm-12" style="padding-left: 0;">
    <label for="property-agent_id-checkbox-{{$agent->id}}" class="control-label">
        {!! Form::checkbox('agent_ids[]', $agent->id, in_array($agent->id, $agent_ids), ['id'=>'property-agent_id-checkbox-'.$agent->id]) !!} {{ $agent->name() }}
    </label>
</div>
@endforeach
@endif
                            </div>
                        </div>
</div>

                        <div class="form-group" style="display: none;">
                            <label for="property-agent_id" class="col-sm-3 control-label">Agent id (hidden)</label>
                            <div class="col-sm-6">
                                <input type="hidden" name="agent_id" id="property-agent_id" class="form-control" value="{{ old('agent_id') }}"> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-key_loc_id" class="col-sm-3 control-label">Key Location</label>
                            <div class="col-sm-6">
                                <label for="property-key_loc_id-1" class="control-label">{!! Form::radio('key_loc_id', 1, old('key_loc_id') == 1, ['id'=>'property-key_loc_id-1']) !!} Managment Office</label>
                                <label for="property-key_loc_id-2" class="control-label">{!! Form::radio('key_loc_id', 2, old('key_loc_id') == 2, ['id'=>'property-key_loc_id-2']) !!} Door Code</label>
                                <label for="property-key_loc_id-3" class="control-label">{!! Form::radio('key_loc_id', 3, old('key_loc_id') == 3, ['id'=>'property-key_loc_id-3']) !!} Agency</label>
                                <label for="property-key_loc_id-4" class="control-label">{!! Form::radio('key_loc_id', 4, old('key_loc_id') == 4, ['id'=>'property-key_loc_id-4']) !!} Other
                                </label>
                            </div>
                        </div>

<div id="property-key-loc-other-section">
                        <div class="form-group">
                            <label for="property-key_loc_other" class="col-sm-3 control-label">Key Location Other</label>
                            <div class="col-sm-6">
                                <input type="text" name="key_loc_other" id="property-key_loc_other" class="form-control" value="{{ old('key_loc_other') }}">
                            </div>
                        </div>
</div>

                        <div class="form-group">
                            <label for="property-door_code" class="col-sm-3 control-label">Door Code</label>
                            <div class="col-sm-6">
                                <input type="text" name="door_code" id="property-door_code" class="form-control" value="{{ old('door_code') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-agency_fee" class="col-sm-3 control-label">Agency Fee</label>
                            <div class="col-sm-6">
                                <input type="text" name="agency_fee" id="property-agency_fee" class="form-control" value="{{ old('agency_fee') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="test-available_date" class="col-sm-3 control-label">Available Date</label>
                            <div class="col-sm-6">
                                {{ Form::date('available_date', old('available_date', ''), ['class'=>'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="property-leased" class="col-sm-3 control-label">Leased (for web)</label>
                            <div class="col-sm-6">
                                <label for="property-leased-0" class="control-label">{!! Form::radio('leased', 0, old('leased') == 0, ['id'=>'property-leased-0']) !!} No</label>
                                <label for="property-leased-1" class="control-label">{!! Form::radio('leased', 1, old('leased') == 1, ['id'=>'property-leased-1']) !!} Yes</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="property-sold" class="col-sm-3 control-label">Sold (for web)</label>
                            <div class="col-sm-6">
                                <label for="property-sold-0" class="control-label">{!! Form::radio('sold', 0, old('sold') == 0, ['id'=>'property-sold-0']) !!} No</label>
                                <label for="property-sold-1" class="control-label">{!! Form::radio('sold', 1, old('sold') == 1, ['id'=>'property-sold-1']) !!} Yes</label>
                            </div>
                        </div>

                        <hr/>
						
                        <div class="form-group">
                            <label for="vendor-comments" class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('comments', '', ['class'=>'form-control', 'placeholder' => 'Type your comments here', 'rows' => '4']) !!}
                            </div>
                        </div>

                        <hr/>

                        <!-- Add Submit Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Submit </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>

@include('partials.property.building-modal')
@include('partials.property.vendor-owner-modal')
@include('partials.property.vendor-agent-modal')
    </div>
@endsection

@section('footer-script')
<?php 
/**
 * Part A) easy automation popup flow:
 * 1) easyAutocomplete setting
 * 2) create new item ajax
 * 3) set content
 *
 * Part B) control - field display control by radio button
 */
?>
@include('partials.property.building-modal-control')
@include('partials.property.feature-control')
@include('partials.property.vendor-owner-modal-control')
@include('partials.property.vendor-agent-modal-control')
@include('partials.property.field-display-control')
@endsection