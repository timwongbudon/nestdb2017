@extends('layouts.app')

@section('content')
    <style>
        .index-image {
            float: left;
            display: block; 
            background-size: 80px 80px; 
            width: 80px; 
            height: 80px;
        }
        .property-table strong {
            font-size: 15px;
        }
    </style>


@include('partials.property.search-form', array('template_type'=>'trash'))
    
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <strong style="color: red">Properties in Trash</strong>
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped property-table">
                            <thead>
                                <th style="width: 5%">ID</th>
                                @if(NestPreference::isShowPhoto())<th>Photo</th>@endif
                                <th style="max-width: 400px">Property</th>
                                <th>Type</th>
                                <th style="width: 8%">&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($properties as $property)
                                    <tr>
                                        <td class="table-text"><div>{{ $property->id }}
                                        @if($property->external_id)({{$property->external_id}})@endif
                                        </div></td>
                                        @if(NestPreference::isShowPhoto())
                                        <td class="table-text"> 
                                            @if(!empty($property->featured_photo_url))
                                            <img src="{{ $property->featured_photo_url }}" alt="" width="300" />
                                            @endif
                                        </td>
                                        @endif
                                        <td class="table-text"><div>
                                             <strong>{{ $property->shorten_building_name(0) }}</strong><br/>
                                            <?php $rep_ids = $property->rep_ids?explode(',',$property->rep_ids):array(); ?>
                                            {{ $property->fix_address1() }}<br/><hr style="margin: 5px 0;">

                                            @if($property->unit)
                                                Unit: {{ $property->unit }}<br/>
                                            @endif
                                            
                                            Asking Price: {{ $property->price_db_searchresults() }}<br/>

                                            @if($property->bedroom_count())
                                                Bedrooms: {{ $property->bedroom_count() }}<br/>
                                            @endif

                                            @if($property->bathroom_count())
                                                Bathrooms: {{ $property->bathroom_count() }}<br/>
                                            @endif
                                            
                                            @if($property->show_size())
                                                Size: {{ $property->show_size() }}<br/>
                                            @endif

                                            @if($property->size)
                                                Size: {{ $property->property_size() }}<br/>
                                            @endif

                                            @if($property->poc_id)
                                                Point of contact: {{ $property->poc() }}<br/>
                                            @endif
                                            
                                            @if(!empty($property['building']->tel))
                                                Mgmt Office tel: {{$property['building']->tel}}<br/>
                                            @endif
                                            
                                            @if(!empty($property['owner']))
                                                Owner: {{$property['owner']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['reps']))
                                                @foreach($property['reps'] as $i => $rep)
                                                Rep {{$i+1}}: {{$rep->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property['agent']))
                                                Agency: {{$property['agent']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['agents']))
                                                @foreach($property['agents'] as $i => $agent)
                                                Agent {{$i+1}}: {{$agent->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property->comments) && $property->comments != '[]')
                                                Last Comment: {{$property->show_last_comment()}}
                                            @endif

                                            @if(empty($property['agent']) && empty($property['owner']) && !empty($property->contact_info))
                                                Contact Info: {{$property->contact_info}}<br/>
                                            @endif

                                            @if(!empty($property->available_date()))
                                            Available Date: {{ $property->available_date() }}
                                            @endif
                                            <hr style="margin: 5px 0;">
                                            {{ $property->show_timestamp() }}
                                        </div></td>
                                        <td class="table-text"><div>{{ $property->type() }}</div></td>
                                        <td>
                                            <form action="{{url('property/restore')}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ Form::hidden('property_id', $property->id) }}
                                                <button type="submit" id="restore-property-{{ $property->id }}" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-undo"></i>Restore
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ Form::hidden('default_type', '') }}
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $properties->currentPage() }} of {{ $properties->lastPage() }} (Total: {{ $properties->total() }} items)</div>
                        {{ $properties->links() }}  
                    </div>
                </div>
            @endif

@include('partials.property.save-shortlist')

@endsection

@section('footer-script')
@include('partials.property.building-modal-control')
@endsection