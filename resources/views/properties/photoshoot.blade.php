@extends('layouts.app')

@section('content')
    <style>
        .index-image {
            float: left;
            display: block; 
            background-size: 80px 80px; 
            width: 80px; 
            height: 80px;
        }
        .property-table strong {
            font-size: 15px;
        }
    </style>

@php
	if (empty($showtype)){
		$showtype = 'standard';
	}
@endphp

<div class="nest-new container">
	
    
            <!-- Current Properties -->
            @if (count($properties) > 0)
                <div class="panel panel-default nest-property-list-heading">
                    <div class="panel-body">
						<div class="row">
							<div class="col-sm-4 text-left">
								<div style="padding-top:3px;padding-bottom:0px;">
									<h4>Photoshootings</h4>
								</div>
							</div>
							<div class="col-sm-4 text-center">
								<div style="padding-top:10px;padding-bottom:20px;"><b>
									{{ count($properties) }} Properties
								</b></div>
							</div>
							<div class="col-sm-4 text-right">
								<div class="nest-property-list-select-all">
									<div style="padding-top:7px;padding-bottom:18px;">
										<label for="options_ids_all">Select All</label>
										{!! Form::checkbox('options_ids_all', '', null, ['id'=>'property-option_id-checkbox-all']) !!}
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
                    </div>
				</div>
				
				@php
					$i = 0;
				@endphp
				@php
					$openphotoshoot = false;
				@endphp
				
				<div class="nest-property-list-wrapper">
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="nest-property-edit-label-features">
										<div class="col-xs-12" style="padding-left:0px;padding-right:0px;padding-bottom:5px;">
											<u>Unfinished</u>
										</div>
									</div>
									<div class="clearfix"></div>
									@if (count($unfinishedshootings) > 0)
										@php
											$openphotoshoot = true;
										@endphp
										<div style="">
											<div class="nest-buildings-result-wrapper">
												@foreach ($unfinishedshootings as $index => $v)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-6">
															<b>Date/Time:</b> {{ $v->v_date() }} {{ $v->getNiceTime() }}<br />
															<b>Status:</b> 
															@if ($v->status == 10)
																Closed
															@else
																Open
															@endif
														</div>
														<div class="col-xs-6">
															@if ($v->status == 0)
																@php
																	$openviewing = true;
																@endphp
																<a class="nest-button nest-right-button btn btn-default" href="{{ url('photoshoot/show/'.$v->id.'?statuschange=close') }}">
																	<i class="fa fa-btn fa-lock"></i>Close
																</a>
															@endif
															<a href="{{ url('/photoshoot/show/'.$v->id) }}" class="nest-button nest-right-button btn btn-default">
																<i class="fa fa-btn fa-pencil-square-o"></i>View
															</a>
														</div>
													</div>
												@endforeach
											</div>
										</div>
									@endif
									<div class="col-xs-12" style="padding-left:0px;padding-right:0px;padding-top:15px;">
										<a href="{{ url('/photoshoot/create') }}" class="nest-button nest-right-button btn btn-default">
											<i class="fa fa-btn fa-plus"></i>Add Photoshoot
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="nest-property-edit-label-features">
										<div class="col-xs-12" style="padding-left:0px;padding-right:0px;padding-bottom:5px;">
											<u>Finished</u>
										</div>
									</div>
									<div class="clearfix"></div>
									@if (count($finishedshootings) > 0)
										<div style="">
											<div class="nest-buildings-result-wrapper" style="max-height:230px;overflow-y:scroll;">
												@foreach ($finishedshootings as $index => $v)
													<div class="row 
													@if ($index%2 == 1)
														nest-striped
													@endif
													">
														<div class="col-xs-6">
															<b>Date/Time:</b> {{ $v->v_date() }} {{ $v->getNiceTime() }}<br />
															<b>Status:</b> 
															@if ($v->status == 10)
																Closed
															@else
																Open
															@endif
														</div>
														<div class="col-xs-6">
															@if ($v->status == 0)
																@php
																	$openviewing = true;
																@endphp
																<a class="nest-button nest-right-button btn btn-default" href="{{ url('photoshoot/show/'.$v->id.'?statuschange=close') }}">
																	<i class="fa fa-btn fa-lock"></i>Close
																</a>
															@endif
															<a href="{{ url('/photoshoot/show/'.$v->id) }}" class="nest-button nest-right-button btn btn-default">
																<i class="fa fa-btn fa-pencil-square-o"></i>View
															</a>
														</div>
													</div>
												@endforeach
											</div>
										</div>
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					
					<h3 class="nest-property-shortlist-district-h3">Not Allocated Yet</h3>
					<div class="row">
						<form id="propertylistform" action="{{ url('frontend/select') }}" method="post">
							{{ csrf_field() }}
							@if (!empty($properties) && count($properties) > 0)
								@foreach ($properties as $property)
									<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 nest-property-list-item">
										<div class="panel panel-default">
											<div class="nest-property-list-item-top">
												<div class="nest-property-list-item-top-checkbox">
													{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
												</div>
												<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
													{{ $property->shorten_building_name(0) }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-image-wrapper">
												<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
													@if(!empty($property->featured_photo_url))
														<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
													@else
														<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
													@endif
													
													@if ($showtype == 'upcleases')
														<div class="nest-property-list-item-avdate-wrapper">
															<div class="nest-property-list-item-avdate">
																{{ $property->available_date() }}
															</div>
														</div>
													@endif
													
													<div class="nest-property-list-item-price-wrapper">
														<div class="nest-property-list-item-price">
															{!! $property->price_db_searchresults_new() !!}
														</div>
													</div>
												</a>
												<div class="nest-property-list-item-image-top-bar-left">
													@if (count($property->nest_photos) > 0)
														<div class="nest-property-list-item-nestpics-wrapper">
															<div class="nest-property-list-item-nestpics">
																<img src="/images/camera2.png" />
															</div>
														</div>
													@endif
												</div>
												<div class="nest-property-list-item-image-top-bar">
													@if($property->bonuscomm == 1)
														<div class="nest-property-list-item-bonus-wrapper">
															<div class="nest-property-list-item-bonus">
																$$$
															</div>
														</div>
													@endif
													@if($property->hot == 1)
														<div class="nest-property-list-item-hot-wrapper">
															<div class="nest-property-list-item-hot">
																HOT
															</div>
														</div>
													@endif
													@if($property->web)
														<div class="nest-property-list-item-web-wrapper">
															<div class="nest-property-list-item-web">
																<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																	WEB
																</a>
															</div>
														</div>
													@endif
													@if ($property->owner_id > 0)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																@if ($owner->corporatelandlord == 1)
																	<div class="nest-property-list-item-cp-wrapper">
																		<div class="nest-property-list-item-cp">
																			<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																				CL
																			</a>
																		</div>
																	</div>
																@endif
															@endforeach
														@endif
													@elseif ($property->poc_id == 3 || $property->poc_id == 4)
														@if ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																@if ($agent->corporatelandlord == 1)
																	<div class="nest-property-list-item-cp-wrapper">
																		<div class="nest-property-list-item-cp">
																			<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																				CL
																			</a>
																		</div>
																	</div>
																@endif
															@endforeach
														@endif
													@endif
												</div>
											</div>
											<div class="nest-property-list-item-bottom-address-wrapper">
												<div class="nest-property-list-item-bottom-address">
													{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
												</div>
												<div class="nest-property-list-item-bottom-unit">
													{{ $property->unit }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													Room(s)
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													@if ($property->bedroom_count() == 1)
														{{ $property->bedroom_count() }} bed, 
													@else
														{{ $property->bedroom_count() }} beds, 
													@endif
													@if ($property->bathroom_count() == 1)
														{{ $property->bathroom_count() }} bath
													@else
														{{ $property->bathroom_count() }} baths
													@endif
													
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													Size
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													{{ $property->property_netsize() }} / {{ $property->property_size() }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-rooms-wrapper">
												<div class="nest-property-list-item-bottom-rooms-left">
													@if ($property->poc_id ==  1)
														Landlord
													@elseif ($property->poc_id ==  2)
														Rep
													@elseif ($property->poc_id ==  3)
														Agency
													@else
														Contact
													@endif
												</div>
												<div class="nest-property-list-item-bottom-rooms-right">
													@php
														$vendorshown = false;
													@endphp
													@if ($property->poc_id ==  1)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@elseif ($property->poc_id ==  2)
														@if ($property->getRep() && $property->getRep()->count() > 0)
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@elseif ($property->poc_id ==  3)
														@if ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
															@php
																$vendorshown = true;
															@endphp
														@endif
													@endif
													@if (!$vendorshown)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach														
															@if ($property->getRep() && $property->getRep()->count() > 0)
																@foreach ($property->getRep() as $rep)
																	{!! $rep->basic_info_property_search_linked() !!}
																@endforeach
															@endif
														@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
															@foreach ($property->getAgency() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
														@else
															-
														@endif
													@endif
												</div>
												<div class="clearfix"></div>
												@if ($property->poc_id == 1 && $vendorshown)
													@if ($property->getRep() && $property->getRep()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Rep
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getRep() as $rep)
																{!! $rep->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif
												@endif
												@if ($property->poc_id == 2 && $vendorshown)
													@if ($property->getOwner() && $property->getOwner()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Owner
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getOwner() as $owner)
																{!! $owner->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif
												@endif
												@if ($property->poc_id == 3 && $vendorshown)
													@if ($property->getAgent() && $property->getAgent()->count() > 0)
														<div class="nest-property-list-item-bottom-rooms-left">
															Agent
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@foreach ($property->getAgent() as $agent)
																{!! $agent->basic_info_property_search_linked() !!}
															@endforeach
														</div>
													@endif	
												@endif
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-features-wrapper">
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
													{{ $property->maidroom_count() }}
												</div>
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
													{{ $property->backend_search_outdoor() }}
												</div>
												<div class="col-xs-4">
													<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
													{{ $property->carpark_count() }}
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="nest-property-list-item-bottom-buttons-wrapper">
												<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
													<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
														<i class="fa fa-btn fa-pencil-square-o"></i>Edit
													</a>
												</div>
												<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
													@if($property->web)
														<a class="nest-button btn btn-default" target="_blank" href="{{ url('property/feathome/'.$property->id) }}" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-globe"></i>Feature
														</a>	
													@endif
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									@php
										$i++;
										if ($i%3 == 0){
											echo '<div class="hidden-xs hidden-sm hidden-lg clearfix"></div>';
										}
										if ($i%2 == 0){
											echo '<div class="hidden-lg hidden-md clearfix"></div>';
										}
										if ($i%4 == 0){
											echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
										}
									@endphp
									
								@endforeach
							@endif
							
							{{ Form::hidden('default_type', '') }}
							{{ Form::hidden('action', 'select') }}
						</form>
					</div>
					
					
					
					
					
				</div>
				<div class="panel panel-default nest-property-list-buttons">
					<div class="panel-body">
						<div class="form-group">
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<div style="margin-left:-7.5px;margin-right:-7.5px;">
										<div class="col-xs-6">
											<button id="print_pdf" class="nest-button btn btn-default">
												<i class="fa fa-btn fa-print"></i>Print
											</button>
										</div>
										<div class="col-xs-6">
											<button id="print_pdf_agent" class="nest-button btn btn-default">
												<i class="fa fa-btn fa-print"></i>Print (Agent)
											</button>
										</div>
									</div>
									<div style="margin-left:-7.5px;margin-right:-7.5px;">
										<div class="col-xs-6">
											<button id="add_frontend_property" class="nest-button btn btn-default">
												<i class="fa fa-btn fa-plus"></i>Website
											</button>
										</div>
										<div class="col-xs-6">
											<button id="remove_frontend_property" class="nest-button btn btn-default">
												<i class="fa fa-btn fa-minus"></i>Website
											</button>
										</div>
									</div>
									<button id="delete_option" class="btn btn-danger" style="border-radius:0px;border:0px;">
										<i class="fa fa-btn fa-trash"></i>Delete
									</button>
								</div>
								@if ($openphotoshoot)
									<div class="col-md-6 col-xs-6">
										@if (count($unfinishedshootings) > 0)
											@foreach ($unfinishedshootings as $index => $v)
												<a href="javascript:;" class="nest-button btn btn-default" onClick="addtophotoshoot({{ $v->id }});">
													<i class="fa fa-btn fa-plus"></i>Add to Photoshoot {{ $v->v_date() }} ({{ $v->getNiceTime() }})
												</a>
											@endforeach
										@endif
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>	
            @endif
	

</div>
@endsection

@section('footer-script')
@include('partials.property.building-modal-control')

<script type="text/javascript">

function addtophotoshoot(vid){
	var url = "{{ url('photoshoot/addtoshoot') }}/"+vid;
	$('form#propertylistform').attr('action', url);
	$('form#propertylistform').attr('target', '_self');
	if ($('form input.property-option:checked').length > 0) {
		$('form#propertylistform').submit();
	}else{
		alert('Please choose a property to add to the next photoshoot');
	}
}

$(document).ready(function(){
    $('#property-option_id-checkbox-all').click(function(event){
        $('form#propertylistform input').prop('checked', $(this).is(":checked"));
    });
    $('button#delete_option').click(function(event){
        event.preventDefault();
        var url = "{{url('photoshoot/main/delete')}}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_self');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose items to delete.');
        }
        return false;
    });
    $('button#print_pdf').click(function(event){
        event.preventDefault();
        var url = "{{ url('photoshoot/option/print/') }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
    $('button#print_pdf_agent').click(function(event){
        event.preventDefault();
        var url = "{{ url('photoshoot/option/printagent/' ) }}";
        $('form#propertylistform').attr('action', url);
        $('form#propertylistform').attr('target', '_blank');
        if ($('form input.property-option:checked').length > 0) {
            $('form#propertylistform').submit();
        } else {
            alert('Please choose property to print.');
        }
        return false;
    });
	
    $('button#add_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('select');
		
		proprerty_list_form.submit();
    });
    $('button#remove_frontend_property.btn').click(function(event){
        event.preventDefault();
		
		var proprerty_list_form = $('form#propertylistform');
		var url = "{{ url('frontend/select') }}";
		proprerty_list_form.attr('action', url);
		proprerty_list_form.attr('target', '');
		$('input[name=action]').val('deselect');
		
		proprerty_list_form.submit();
    });
});
</script>
@endsection