@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="nest-user-changes-wrapper row">
		<div class="col-md-6" style="margin-bottom:30px;">
			<div class="panel panel-default" style="margin-bottom:0px;">
				<div class="panel-heading">
					<h4 style="margin-bottom:0px;padding-bottom:0px;">Major property updates</h4>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<div class="nest-buildings-result-wrapper">
				@php
					$curdate = '';
					$index = 0;
				@endphp
				@foreach ($changelist as $list)
					@php
						$first = $list[0];
						$last = null;
						if (isset($list[1])){
							$last = $list[1];
							for ($i = 2; $i < count($list); $i++){
								if ($list[$i]->logtime > $d){
									$last = $list[$i];
								}
							}
						}
					@endphp
					@if ($last == null || ($first->available_date != $last->available_date && ((($first->type_id == 1 || $first->type_id == 3) && $first->leased == 0) || (($first->type_id == 2 || $first->type_id == 3) && $first->sold == 0))) || ($first->leased == 0 && $last->leased == 1) || ($first->sold == 0 && $last->sold == 1))
						@php
							//$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
							//$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('H:i');
							
							$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
							$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'GMT')->setTimezone('Asia/Hong_Kong')->format('H:i');
						@endphp
						@if ($curdate != $fdate)
							<div class="row" style="border:0px;padding-top:20px;">
								<div class="nest-user-changes-wrapper-inner col-xs-12">
									<h5>{{ $fdate }}</h5>
								</div>
							</div>
							@php
								$curdate = $fdate;
							@endphp
						@endif
			
						<div class="row 
						@if ($index%2 == 1)
							nest-striped
						@endif
						">
							<div class="nest-user-changes-wrapper-inner col-xs-12">
								<div class="">
									{{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $first->id }}, '{{ $first->name }}', '')">{{ $first->name }}</a><br />
								</div>
							</div>
						</div>
						@php
							$index++;
						@endphp
					@endif
				@endforeach
			</div>
		</div>
		<div class="col-md-6" style="margin-bottom:30px;">
			<div class="panel panel-default" style="margin-bottom:0px;">
				<div class="panel-heading">
					<h4 style="margin-bottom:0px;padding-bottom:0px;">All changes in last 7 days</h4>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<div class="nest-buildings-result-wrapper">
				@php
					$curdate = '';
					$index = 0;
				@endphp
				@foreach ($changesfull as $change)
					@php
						//$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
						//$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('H:i');
							
						$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'GMT')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
						$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'GMT')->setTimezone('Asia/Hong_Kong')->format('H:i');
					@endphp
					@if ($curdate != $fdate)
					
						<div class="row" style="border:0px;padding-top:20px;">
							<div class="nest-user-changes-wrapper-inner col-xs-12">
								<h5>{{ $fdate }}</h5>
							</div>
						</div>
						
						@php
							$curdate = $fdate;
							$index = 0;
						@endphp
					@endif
							
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="nest-user-changes-wrapper-inner col-xs-12">
							<div class="">
								{{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $change->id }}, '{{ $change->name }}', '')">{{ $change->name }}</a><br />
							</div>
						</div>
					</div>
					@php
						$index++;
					@endphp
				@endforeach
			</div>
		</div>
	</div>		
	@if ($changesfull->lastPage() > 1)	
		<div class="panel panel-default nest-properties-footer">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						{{ $changesfull->links() }}  
					</div>
				</div>
			</div>
		</div>
	@endif
	</div>
</div>
@endsection
