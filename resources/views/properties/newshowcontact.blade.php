
<div class="nest-property-list-detail-wrapper">
	@php
		$striped = '';
	@endphp
	
	@if (trim($property->poc()) != '')
		<div class="nest-property-list-detail-first-row row">
			<div class="col-xs-4">
				<b>Point of Contact</b>
			</div>
			<div class="col-xs-8">
				{{ $property->poc() }}
			</div>
		</div>			
		@php
			if ($striped == ''){
				$striped = ' nest-striped';
			}else{
				$striped = '';
			}
		@endphp
		<div class="row{{ $striped }}">
	@else
		<div class="nest-property-list-detail-first-row row{{ $striped }}">
	@endif
		<div class="col-xs-4">
			<b>Owner</b>
		</div>
		<div class="col-xs-8">
			@if(!empty($owner))
				<a target="show_vendor" href="{{ url('/vendor/show/' . $owner->id) }}">{{ $owner->name_company() }}</a><br />
				@if($owner->email)
						Email: {{ $owner->email }}<br />
				@endif
				@if($owner->tel)
						Tel: <a href="tel:{{ trim($owner->tel) }}" class="nest-clickable-phone-number">{{ $owner->tel }}</a><br />
				@endif
			@else
				-
			@endif
		</div>			
		@php
			if ($striped == ''){
				$striped = ' nest-striped';
			}else{
				$striped = '';
			}
		@endphp
	</div>

	@if (count($reps) > 0)
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Rep</b>
			</div>
			<div class="col-xs-8">
				@foreach ($reps as $rep)
					<div id="property-vendor-rep-checkbox-template" style="display: block">
						<a target="show_vendor" href="{{ url('/vendor/show/' . $rep->id) }}">{{ $rep->name_company() }}</a><br />
						@if($rep->email)
							Email: {{ $rep->email }}<br />
						@endif
						@if($rep->tel)
							Tel: <a href="tel:{{ trim($rep->tel) }}" class="nest-clickable-phone-number">{{ $rep->tel }}</a><br />
						@endif
					</div>
				@endforeach
			</div>			
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
		
	<div class="row{{ $striped }}">
		<div class="col-xs-4">
			<b>Agency</b>
		</div>
		<div class="col-xs-8">
			@if(!empty($agent))
				<a target="show_vendor" href="{{ url('/vendor/show/' . $agent->id) }}">{{ $agent->name_company() }}</a><br />
				@if($agent->email)
					Email: {{ $agent->email }}<br />
				@endif
				@if($agent->tel)
					Tel: <a href="tel:{{ trim($agent->tel) }}" class="nest-clickable-phone-number">{{ $agent->tel }}</a><br />
				@endif
			@else
				-
			@endif
		</div>			
		@php
			if ($striped == ''){
				$striped = ' nest-striped';
			}else{
				$striped = '';
			}
		@endphp
	</div>
	
	@if (count($agents) > 0)
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Agent</b>
			</div>
			<div class="col-xs-8">
				@foreach ($agents as $agent)
					<div id="property-vendor-agent-checkbox-template" style="display: block">
						<a target="show_vendor" href="{{ url('/vendor/show/' . $agent->id) }}">{{ $agent->name_company() }}</a><br />
						@if($agent->email)
							Email: {{ $agent->email }}<br />
						@endif
						@if($agent->tel)
							Tel: <a href="tel:{{ trim($agent->tel) }}" class="nest-clickable-phone-number">{{ $agent->tel }}</a><br />
						@endif
					</div>
				@endforeach			
			</div>			
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	
	@if (trim($property->key_loc_other) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Key Location Other</b>
			</div>
			<div class="col-xs-8">
				{{ $property->key_loc_other }}
			</div>			
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	
	@if (trim($property->door_code) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Door Code</b>
			</div>
			<div class="col-xs-8">
				{{ $property->door_code }}
			</div>
			@php
				if ($striped == ''){
					$striped = ' nest-striped';
				}else{
					$striped = '';
				}
			@endphp
		</div>
	@endif
	
	@if (trim($property->agency_fee) != '')
		<div class="row{{ $striped }}">
			<div class="col-xs-4">
				<b>Agency Fee</b>
			</div>
			<div class="col-xs-8">
				{{ $property->agency_fee }}
			</div>
		</div>
	@endif
	<div class="clearfix"></div>
	
	<br />
	
</div>