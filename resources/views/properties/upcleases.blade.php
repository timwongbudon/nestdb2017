@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>Upcoming Leases</h4>
				</div>
			</div>
			<div class="panel-body">
				<table class="table table-striped margin-none">
					<thead>
					  <tr>
						<th class="text-left">Property</th>
						<th class="text-left">Date</th>
						<th class="text-left">Owner</th>
					  </tr>
					</thead>
					<tbody>
						@foreach ($leasedprops as $prop)
							<tr>
								<td>
									<a href="{{ url('property/edit/'.$prop->id) }}" class="text-primary">{{ $prop->shorten_building_name(0) }}</a>
								</td>
								<td class="text-left">
									{{ $prop->available_date() }}
								</td>
								<td class="text-left">
									@if(!empty($prop['owner']))
										{{$prop['owner']->basic_info()}}<br/>
									@endif
								</td> 
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div>
</div>
@endsection
