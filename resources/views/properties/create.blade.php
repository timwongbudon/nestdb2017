@extends('layouts.app')
@section('content')


<div class="nest-new">
	<div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('property/store') }}" method="POST" class="form-horizontal property-add" enctype="multipart/form-data">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>New Property</h4>
						</div>

						<div class="panel-body">
							@include('common.errors')
							
							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Property Name</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="name" id="property-name" class="form-control" value="{{ old('name') }}">
									<span id="property-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Display Name</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="display_name" id="property-display_name" class="form-control" value="{{ old('display_name') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Address</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Unit</div>
								</div>
								<div class="col-xs-3">
									<input type="text" name="unit" id="property-unit" class="form-control" value="{{ old('unit') }}">
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Floor</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="floornr" id="property-floornr" class="form-control" value="{{ old('floornr') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 {!! Form::select('district_id', $district_ids, old('district_id'), ['id'=>'property-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
								</div>
								<div class="col-xs-6">
									{!! Form::select('country_code', $country_ids, 'hk', ['id'=>'country_code','class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Building ID</div>
								</div>
								<div class="col-xs-3">
									<input type="text" name="building_id" id="building_id" class="form-control" value="{{ old('building_id') }}" />
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label">Year Built</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built') }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-3">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-space.jpg') }}" /></div>Gross</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="gross_area" id="property-gross_area" class="form-control" value="{{ old('gross_area') }}" min="0">
								</div>
								<div class="col-xs-3">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-space.jpg') }}" /></div>Saleable</div>
								</div>
								<div class="col-xs-3">
									<input type="number" name="saleable_area" id="property-saleable_area" class="form-control" value="{{ old('saleable_area') }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-bed.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="bedroom" id="property-bedroom" class="form-control" value="{{ old('bedroom') }}" min="0">
								</div>
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-bath.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="bathroom" id="property-bathroom" class="form-control" value="{{ old('bathroom') }}" min="0">
								</div>
								<div class="col-xs-1">
									 <div class="nest-property-edit-label"><div class="nest-icon-edit"><img src="{{ url('images/tools/icon-maid.jpg') }}" /></div></div>
								</div>
								<div class="col-xs-3">
									<input type="number" step="0.5" name="maidroom" id="property-maidroom" class="form-control" value="{{ old('maidroom') }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Floor Zone</div>
								</div>
								<div class="col-xs-6">
									{!! Form::select('floor_zone', array('High' => 'High', 'Middle' => 'Middle', 'Low' => 'Low', 'Ground Floor' => 'Ground Floor'), '', ['class'=>'form-control', 'placeholder' => 'Choose a floor zone...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									 <div class="nest-property-edit-label">Layout</div>
								</div>
								<div class="col-xs-6">
									{!! Form::select('layout', array('Stand-Alone' => 'Stand-Alone', 'Penthouse' => 'Penthouse', 'Simplex' => 'Simplex', 'Duplex' => 'Duplex', 'Triplex' => 'Triplex'), '', ['class'=>'form-control', 'placeholder' => 'Choose a layout...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Description</div>
								</div>
								<div class="col-xs-8">
									{!! Form::textarea('description', '', ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
								</div>
								<div class="clearfix"></div>
							</div>			
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Website Text</div>
								</div>
								<div class="col-xs-8">
									{!! Form::textarea('webtext', '', ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
								</div>
								<div class="clearfix"></div>
							</div>					
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-4">
									<label for="property-type-1" class="control-label">{!! Form::radio('type_id', 1, false, ['id'=>'property-type-1']) !!} Rent</label>
								</div>
								<div class="col-xs-4">
									<label for="property-type-2" class="control-label">{!! Form::radio('type_id', 2, false, ['id'=>'property-type-2']) !!} Sale</label>
								</div>
								<div class="col-xs-4">
									<label for="property-type-3" class="control-label">{!! Form::radio('type_id', 3, false, ['id'=>'property-type-3']) !!} Rent & Sale</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-sale-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Asking Sale</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="asking_sale" id="property-asking_sale" class="form-control" value="{{ old('asking_sale') }}" min="0"> 
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Subject to Offer</div>
								</div>
								<div class="col-xs-3">
									<label for="property-nosaleprice-0" class="control-label">{!! Form::radio('nosaleprice', 0, true, ['id'=>'property-nosaleprice-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-nosaleprice-1" class="control-label">{!! Form::radio('nosaleprice', 1, false, ['id'=>'property-nosaleprice-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-rent-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Asking Rent</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="asking_rent" id="property-asking_rent" class="form-control" value="{{ old('asking_rent') }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="rent-inclusive-section" class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Inclusive</div>
								</div>
								<div class="col-xs-3">
									<label for="property-inclusive-1" class="control-label">{!! Form::radio('inclusive', 1, true, ['id'=>'property-inclusive-1']) !!} Yes</label>
								</div>
								<div class="col-xs-3">
									<label for="property-inclusive-2" class="control-label">{!! Form::radio('inclusive', 0, false, ['id'=>'property-inclusive-2']) !!} No</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-management-fee" class="nest-property-edit-row" style="display:none;">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Management Fee / Month</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="management_fee" id="property-management_fee" class="form-control" value="{{ old('management_fee') }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-government-fee" class="nest-property-edit-row" style="display:none;">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Government Rate / Quarter</div>
								</div>
								<div class="col-xs-6">
									<input type="number" name="government_rate" id="property-government_rate" class="form-control" value="{{ old('government_rate') }}" min="0">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Available Date</div>
								</div>
								<div class="col-xs-6">
									{{ Form::date('available_date', old('available_date'), ['class'=>'form-control']) }}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Hot</div>
								</div>
								<div class="col-xs-3">
									<label for="property-hot-0" class="control-label">{!! Form::radio('hot', 0, true, ['id'=>'property-hot-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-hot-1" class="control-label">{!! Form::radio('hot', 1, false, ['id'=>'property-hot-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Bonus Commission</div>
								</div>
								<div class="col-xs-3">
									<label for="property-bonuscomm-0" class="control-label">{!! Form::radio('bonuscomm', 0, true, ['id'=>'property-bonuscomm-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-bonuscomm-1" class="control-label">{!! Form::radio('bonuscomm', 1, false, ['id'=>'property-bonuscomm-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Leased</div>
								</div>
								<div class="col-xs-3">
									<label for="property-leased-0" class="control-label">{!! Form::radio('leased', 0, true, ['id'=>'property-leased-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-leased-1" class="control-label">{!! Form::radio('leased', 1, false, ['id'=>'property-leased-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Sold</div>
								</div>
								<div class="col-xs-3">
									<label for="property-sold-0" class="control-label">{!! Form::radio('sold', 0, true, ['id'=>'property-sold-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-sold-1" class="control-label">{!! Form::radio('sold', 1, false, ['id'=>'property-sold-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>

							{{-- <div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Sole Agency</div>
								</div>
								<div class="col-xs-3">
									<label for="property-sole_agency-0" class="control-label">{!! Form::radio('sole_agency', 0, true, ['id'=>'property-sole_agency-0']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-sole_agency-1" class="control-label">{!! Form::radio('sole_agency', 1, false, ['id'=>'property-sole_agency-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div> --}}
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Agency</div>
								</div>
								<div class="col-xs-6">
								{!! Form::select('sole_agency', array('0' => 'No Agency', '1' => 'Sole Agency', '2' => 'Dual Agency'), '', ['class'=>'form-control', 'placeholder' => 'Choose a Agency...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Direct Landlord</div>
								</div>
								<div class="col-xs-3">
									<label for="property-direct_landlord-0" class="control-label">{!! Form::radio('direct_landlord', 0, true, ['id'=>'property-direct_landlord-0','class'=>'dll']) !!} No</label>
								</div>
								<div class="col-xs-3">
									<label for="property-direct_landlord-1" class="control-label">{!! Form::radio('direct_landlord', 1,false, ['id'=>'property-direct_landlord-1','class'=>'dll']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
								<div style="text-align: left; font-size:11px; padding-left: 10px;">If yes, select an Information Officer below.</div>
							</div>

							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Information Officer</div>
								</div>
								<div class="col-xs-6">
									<select class="form-control io" name="information_officer" >
									<option value="0"></option>
										@foreach($io_users as $io_user)
											<option value="{{$io_user->id}}"> {{$io_user->name}}</option>
										@endforeach
									</select>
								</div>
								
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Commission</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="commission" id="property-commission" class="form-control" value="{{ old('commission', '') }}" placeholder="" maxlength="100" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Point of Contact</div>
								</div>
								<div class="col-xs-3">
									<label for="vendor-poc-1" class="control-label">{!! Form::radio('poc_id', 1, false, ['id'=>'vendor-poc-1']) !!} Owner</label>
								</div>
								<div class="col-xs-2">
									<label for="vendor-poc-2" class="control-label">{!! Form::radio('poc_id', 2, false, ['id'=>'vendor-poc-2']) !!} Rep</label>
								</div>
								<div class="col-xs-3">
									<label for="vendor-poc-3" class="control-label">{!! Form::radio('poc_id', 3, false, ['id'=>'vendor-poc-3']) !!} Agency</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Owner Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="owner" id="property-owner" class="form-control" value="{{ old('owner') }}">
									<span id="property-owner-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the owner name"><i class="fa fa-btn fa-close"></i></button></span>
									<input type="hidden" name="owner_id" id="property-owner_id" class="form-control" value="{{ old('owner_id') }}"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="property-vendor-rep-section" class="nest-property-edit-row nest-property-edit-row-contacts" style="display:none;">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Rep Name(s)</div>
								</div>
								<div class="col-xs-8 nest-property-edit-rep-wrapper">
									<div id="property-vendor-rep-checkbox-template" style="display: none">
										<label for="property-rep_id-checkbox-n" class="control-label">{!! Form::checkbox('rep_ids[]', 0, null, ['id'=>'property-rep_id-checkbox-n']) !!} </label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row nest-property-edit-row-contacts">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Agency Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="agent" id="property-agent" class="form-control" value="{{ old('agent') }}">
									<span id="property-agent-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the agent name"><i class="fa fa-btn fa-close"></i></button></span>
									<input type="hidden" name="agent_id" id="property-agent_id" class="form-control" value="{{ old('agent_id') }}"> 
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							<div id="property-vendor-agent-section" class="nest-property-edit-row nest-property-edit-row-contacts" style="display:none;">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Agent Name(s)</div>
								</div>
								<div class="col-xs-8 nest-property-edit-agent-wrapper">
									<div id="property-vendor-agent-checkbox-template" style="display: none">
										<label for="property-agent_id-checkbox-n" class="control-label">{!! Form::checkbox('agent_ids[]', 0, null, ['id'=>'property-agent_id-checkbox-n']) !!} </label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							
							<div class="nest-property-edit-row nest-property-edit-radio-wrapper">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Key Location</div>
								</div>
								<div class="col-xs-5">
									<label for="property-key_loc_id-1" class="control-label">{!! Form::radio('key_loc_id', 1, false, ['id'=>'property-key_loc_id-1']) !!} Management Office</label>
								</div>
								<div class="col-xs-3">
									<label for="property-key_loc_id-3" class="control-label">{!! Form::radio('key_loc_id', 3, false, ['id'=>'property-key_loc_id-3']) !!} Agency</label>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-4">
									&nbsp;
								</div>
								<div class="col-xs-5">
									<label style="padding-top:0px !important;" for="property-key_loc_id-2" class="control-label">{!! Form::radio('key_loc_id', 2, false, ['id'=>'property-key_loc_id-2']) !!} Door Code</label>
								</div>
								<div class="col-xs-3">
									<label style="padding-top:0px !important;" for="property-key_loc_id-4" class="control-label">{!! Form::radio('key_loc_id', 4, false, ['id'=>'property-key_loc_id-4']) !!} Other</label>
								</div>
								<div class="clearfix"></div>
							</div>

							<div id="property-key-loc-other-section" class="nest-property-edit-row" style="display:none;">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Other Key Location</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="key_loc_other" id="property-key_loc_other" class="form-control" value="{{ old('key_loc_other') }}">
								</div>
								<div class="clearfix"></div>
							</div>

							<div id="property-key-loc-door-code-section" class="nest-property-edit-row" style="display:none;">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Door Code</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="door_code" id="property-door_code" class="form-control" value="{{ old('door_code') }}">
								</div>
								<div class="clearfix"></div>
							</div>

							<div id="property-key-loc-other-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Agency Fee</div>
								</div>
								<div class="col-xs-6">
									<input type="text" name="agency_fee" id="property-agency_fee" class="form-control" value="{{ old('agency_fee') }}" placeholder="">
								</div>
								<div class="clearfix"></div>
							</div>

						
						</div>
					</div>

				</div>
				
				<script>
					$(document).ready(function(){
						//auto increase by a certain amount
						$("#property-asking_sale").on('keydown', function(event) { 
							var keyCode = event.keyCode || event.which; 
							if (keyCode == 9) { //tab keydown
								var res = parseFloat($(this).val());
								if (res < 1000000) {
									$(this).val(res*1000000);
								}
							} 
						});
						$("#property-asking_rent").on('keydown', function(event) { 
							var keyCode = event.keyCode || event.which; 
							if (keyCode == 9) { //tab keydown
								var res = parseFloat($(this).val());
								if (res < 1000) {
									$(this).val(res*1000);
								}
							} 
						});
						
						$('input[name=type_id]').change(function(){
							var typeid = $(this).val();
							
							if (typeid == 1){
								$('#property-sale-section').css('display', 'none');
								$('#property-rent-section').css('display', 'block');
								$('#rent-inclusive-section').css('display', 'block');
								var inclusive = $('input[name=inclusive]:checked').val();
								if (inclusive == 1){
									$('#property-management-fee').css('display', 'none');
									$('#property-government-fee').css('display', 'none');								
								}else{
									$('#property-management-fee').css('display', 'block');
									$('#property-government-fee').css('display', 'block');										
								}
							}else if (typeid == 2){
								$('#property-sale-section').css('display', 'block');
								$('#property-rent-section').css('display', 'none');
								$('#rent-inclusive-section').css('display', 'none');
								$('#property-management-fee').css('display', 'none');
								$('#property-government-fee').css('display', 'none');
							}else if (typeid == 3){
								$('#property-sale-section').css('display', 'block');
								$('#property-rent-section').css('display', 'block');
								$('#rent-inclusive-section').css('display', 'block');
								var inclusive = $('input[name=inclusive]:checked').val();
								if (inclusive == 1){
									$('#property-management-fee').css('display', 'none');
									$('#property-government-fee').css('display', 'none');								
								}else{
									$('#property-management-fee').css('display', 'block');
									$('#property-government-fee').css('display', 'block');										
								}						
							}
						});
						
						$('input[name=inclusive]').change(function(){
							var inclusive = $('input[name=inclusive]:checked').val();
							
							if (inclusive == 1){
								$('#property-management-fee').css('display', 'none');
								$('#property-government-fee').css('display', 'none');								
							}else{
								$('#property-management-fee').css('display', 'block');
								$('#property-government-fee').css('display', 'block');										
							}
						});
						
						$('input[name=key_loc_id]').change(function(){
							var location = $('input[name=key_loc_id]:checked').val();
							
							if (location == 2){
								$('#property-key-loc-door-code-section').css('display', 'block');							
							}else{
								$('#property-key-loc-door-code-section').css('display', 'none');							
							}
							if (location == 4){
								$('#property-key-loc-other-section').css('display', 'block');						
							}else{
								$('#property-key-loc-other-section').css('display', 'none');								
							}
						});
						
						$('#property-name, #property-unit, #property-floornr, #property-display_name').keyup(function(){
							$('#create-property-btn').show();
						$('#confirm-create-property-btn').hide();
						});

						$('#property-district_id, #country_code').keyup(function(){
							$('#create-property-btn').show();
						$('#confirm-create-property-btn').hide();
						});

						jQuery('.property-add').submit(function(e){								
								
							if(jQuery('.dll:checked').val()=="1"){
								if(jQuery('.io').val()=="0"){
									alert("Please select Information Officer")
									e.preventDefault();

								}
							}
						
							var name = $('#property-name').val();
							var display_name = $('#property-display_name').val();
							var unit = $('#property-unit').val();
							var floornr = $('#property-floornr').val();
							var address1 = $('#property-address1').val();
							var building_id = $('#building_id').val();
							
							e.preventDefault();

							if( $(document.activeElement).val() == "Create"){

							$.ajax(
            {
                type: 'get',
                url: '/property/property_exists?name='+name+'&display_name='+display_name+'&unit='+unit+'&floornr='+floornr+'&address1='+address1+'&building_id='+building_id,
                data: '',
                //dataType: 'JSON',
                beforeSend: function () {
                    //document.getElementById('loading-app').style.display = 'block';
                },
                complete: function () {
                   // document.getElementById('loading-app').style.display = 'none';
                },
                success: function (result) {
                    console.log(result);
					var r = JSON.parse(result);
					if(r.count == 0){
						//alert("Good to go");
						e.currentTarget.submit();
					}else{
						$('#create-property-btn').hide();
						$('#confirm-create-property-btn').show();
						$('#similar-property').html(r.results);
					}
                
				//	alert(result);

                },
                error: function (xhr, status, error) {
                    alert("Something went wrong");
                    console.log(status)
                    console.log(xhr)
                    console.log(error)
                }
            });

		} else {
			e.currentTarget.submit();
		}

						});

						
						
					});

				</script>
				
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;">
									<div class="nest-property-edit-label-features"><u>Outdoor Space</u></div>
								</div>
							</div>
							<div class="nest-property-edit-row">
								{{ Form::hidden('outdoors', '0') }}
								@foreach ($outdoor_ids as $outdoor_id => $outdoor)
									@if ($outdoor_id != 5)
										<div class="col-xs-3">
											<div class="nest-property-edit-label-outdoor">
												<label for="property-outdoors-{{$outdoor_id}}" class="control-label">{!! Form::checkbox('outdoors[]', $outdoor_id, false, ['id'=>'property-outdoors-'.$outdoor_id]) !!} {{ $outdoor}}</label>
											</div>
										</div>
										<div class="col-xs-3">
											<input type="number" name="outdoorareas[{{ strtolower($outdoor) }}]" id="property-outdoor-{{ strtolower($outdoor) }}" class="form-control" value="{{ isset($outdoorareas[strtolower($outdoor)])?$outdoorareas[strtolower($outdoor)]:'' }}" min="0" /> 
										</div>
									@endif
									@if ($outdoor_id == 2 || $outdoor_id == 4)
										<div class="clearfix"></div>
										</div>
										<div class="nest-property-edit-row">
									@endif
								@endforeach
								
								 
								
								
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;padding-top:10px;">
									<div class="nest-property-edit-label-features"><u>Property Features</u></div>
									<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($featurelist as $feature_id => $feature)
											@if ($feature_id != 10 and $feature_id != 12 and $feature_id != 9 and $feature_id != 26)
												@if ($feature_id != 5)
													<div class="col-xs-6 property-features-{{$feature_id}}-wrapper">
														<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, false, ['id'=>'property-features-'.$feature_id]) !!} {{ $feature }}
														</label>
													</div>
												@else
													<div class="col-xs-3" style="padding-top:7px;">
														<div class="nest-property-edit-label-outdoor">
															<label for="property-features-5" class="control-label">{!! Form::checkbox('features[]', 5, false, ['id'=>'property-features-5']) !!} Car Park</label>
														</div>
													</div>
													<div class="col-xs-3" style="padding-top:7px;">
														<input type="number" name="car_park" id="property-car_park" class="form-control" value="{{ old('car_park') }}" min="0">
													</div>
												@endif
											@endif
										@endforeach
									</div>
									<div class="clearfix"></div>
									<div style="padding-top:5px;">
										<input type="text" name="otherfeatures" placeholder="Other Features: e.g. Library, Separate studio, etc." id="property-otherfeatures" class="form-control" value=""> 
									</div>
								</div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-12" style="padding-bottom:5px;padding-top:10px;">
									<div class="nest-property-edit-label-features"><u>Building Facilities</u></div>
									<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($facilitylist as $feature_id => $feature)
											@if ($feature_id != 7)
												<div class="col-xs-6">
											@else
												<div class="col-xs-12">
											@endif
												<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, false, ['id'=>'property-features-'.$feature_id]) !!} {{ $feature }}
												</label>
											</div>
										@endforeach
									</div>
									<div class="clearfix"></div>
									<div style="padding-top:5px;">
										<input type="text" name="otherfacilities" placeholder="Other Facilities: e.g. Library, Separate studio, etc." id="property-otherfacilities" class="form-control" value=""> 
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="create-property-btn">
							<button type="submit" class="nest-button nest-right-button btn btn-default" value="Create"><i class="fa fa-btn fa-plus"></i>Create </button>
							</div>

							<div id="confirm-create-property-btn" style="display: none; ">
							<div id="similar-property" class="alert alert-danger" ></div>
							<div>
							<button type="submit" class="nest-button nest-right-button btn btn-default" value="Confirm"><i class="fa fa-btn fa-plus"></i>Confirm </button>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</form>
			
			<script>
				$(document).ready(function(){
					carpark_checkbox = '#property-features-5';
					carpark_text = '#property-car_park';

					$(carpark_checkbox).change(function(){
						if ($(carpark_checkbox+':checked').length) {
							if ($(carpark_text).val() == '' || $(carpark_text).val() == '0') {
								$(carpark_text).val('1');
							}
						} else {
							$(carpark_text).val(0);
						}
					});
					$(carpark_text).change(function(){
						if ($(carpark_text).val() == '' || $(carpark_text).val() == '0') {
							$(carpark_checkbox).removeAttr('checked');
						} else {
							$(carpark_checkbox).prop('checked', 'checked');
						}
					});
				});
		
				jQuery('input[name="poc_id"]').on('change', function(e){
					var poc = jQuery('input[name="poc_id"]:checked').val();
					if (poc == 1){
						jQuery('input[name="commission"]').val('50%');
					}else if (poc == 2){
						jQuery('input[name="commission"]').val('50%');
					}else if (poc == 3){
						jQuery('input[name="commission"]').val('0%');
					}
				});
			</script>
		</div>
	</div>
</div>			
							

@include('partials.property.building-modal')
@include('partials.property.vendor-owner-modal')
@include('partials.property.vendor-agent-modal')

@endsection

@section('footer-script')

@include('partials.property.building-modal-control')
@include('partialsnew.vendor-owner-modal-control')
@include('partialsnew.vendor-agent-modal-control')
@endsection