
<div class="nest-property-list-detail-wrapper">
	<div class="nest-property-list-detail-first-row row">
		<div class="col-xs-4">
			<b>Address</b>
		</div>
		<div class="col-xs-8">
			@if (trim($property->unit) != '')
				{{ trim($property->unit) }},
			@endif
			{{ trim($property->address1) }}, {{ $property->district() }}
		</div>
	</div>
	<div class="row nest-striped">
		<div class="col-xs-4">
			<b>Price</b>
		</div>
		<div class="col-xs-8">
			{!! $property->price_db_searchresults_new() !!}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-4">
			<b>Size</b>
		</div>
		<div class="col-xs-8">
			{{ $property->saleable_area }} / {{ $property->gross_area }}
		</div>
	</div>
	<div class="row nest-striped">
		<div class="col-xs-4">
			<b>Available Date</b>
		</div>
		<div class="col-xs-8">
			{{ $property->available_date() }}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-4">
			<b>Last Updated</b>
		</div>
		<div class="col-xs-8">
			{{ $property->get_nice_updated_at() }}
		</div>
	</div>
	<div class="row nest-striped">
		<div class="col-xs-4">
			<b>Date Created</b>
		</div>
		<div class="col-xs-8">
			{{ $property->get_nice_created_at() }}
		</div>
	</div>
	@if (trim($property->commission) != '')
		<div class="row">
			<div class="col-xs-4">
				<b>Commission</b>
			</div>
			<div class="col-xs-8">
				{{ $property->commission }}
			</div>	
		</div>
	@endif
</div>