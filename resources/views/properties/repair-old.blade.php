@extends('layouts.app')

@section('content')

<style>
    .panel-warning .form-group div > span {
        padding-bottom: 17px;
    }
</style>

    <div class="nest-new">
		<div class="row">
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Edit Property</h4>
					</div>

					<div class="panel-body">
						<!-- Display Validation Errors -->
						@include('common.errors')
					
						<!-- New Property Form -->
						<form action="{{ url('property/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
								{{ csrf_field() }}
							
							@if (!empty($searchterm))
								<input type="hidden" name="oldsearchterm" value="{{ $searchterm }}" />
							@else
								<input type="hidden" name="oldsearchterm" value="" />
							@endif
							
							<div class="form-group">
								<label for="property-type" class="col-sm-3 control-label">Type</label>
								<div class="col-sm-6">
									 <label for="property-type-1" class="control-label">{!! Form::radio('type_id', 1, $property->type_id == 1, ['id'=>'property-type-1']) !!} Rent</label>
									 <label for="property-type-2" class="control-label">{!! Form::radio('type_id', 2, $property->type_id == 2, ['id'=>'property-type-2']) !!} Sale</label>
									 <label for="property-type-3" class="control-label">{!! Form::radio('type_id', 3, $property->type_id == 3, ['id'=>'property-type-3']) !!} Rent & Sale</label>
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-unit" class="col-sm-3 control-label">Unit</label>
								<div class="col-sm-6">
									<input type="text" name="unit" id="property-unit" class="form-control" value="{{ old('unit', $property->unit) }}">
								</div>
							</div>

							<div class="form-group">
								<label for="property-name" class="col-sm-3 control-label">Property Name</label>
								<div class="col-sm-6">
									<input type="text" name="name" id="property-name" class="form-control" value="{{ old('name', $property->name) }}">
									<span id="property-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-name" class="col-sm-3 control-label">Display Proeprty Name <br/>(For web only)</label>
								<div class="col-sm-6">
									<input type="text" name="display_name" id="property-display_name" class="form-control" value="{{ old('display_name', $property->display_name) }}">
									<span style="color: green;"><i>If this display name is empty, the building name will be shown.</i></span>
								</div>
							</div>

							<div class="form-group">
								<label for="property-type" class="col-sm-3 control-label">Building id</label>
								<div class="col-sm-6">
									<input type="text" name="building_id" id="building_id" class="form-control" value="{{ old('building_id', $property->building_id) }}" />
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-address1" class="col-sm-3 control-label">Address1</label>
								<div class="col-sm-6">
									<input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1', $property->address1) }}">
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-address2" class="col-sm-3 control-label">Address2</label>
								<div class="col-sm-6">
									<input type="text" name="address2" id="property-address1" class="form-control" value="{{ old('address2', $property->address2) }}">
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-district_id" class="col-sm-3 control-label">District</label>
								<div class="col-sm-6">
									{!! Form::select('district_id', $district_ids, $property->district_id, ['id'=>'property-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-country_code" class="col-sm-3 control-label">Country</label>
								<div class="col-sm-6">
									{!! Form::select('country_code', $country_ids, $property->country_code, ['class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-year_built" class="col-sm-3 control-label">Year Built</label>
								<div class="col-sm-6">
									<input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built', $property->year_built) }}" min="0">
								</div>
							</div>

							<hr/>
						
							<div class="form-group">
								<label for="property-floor_zone" class="col-sm-3 control-label">Floor Zone</label>
								<div class="col-sm-6">
									{!! Form::select('floor_zone', array('High' => 'High', 'Middle' => 'Middle', 'Low' => 'Low', 'Ground Floor' => 'Ground Floor'), $property->floor_zone, ['class'=>'form-control', 'placeholder' => 'Choose a floor zone...']); !!}
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-layout" class="col-sm-3 control-label">Layout</label>
								<div class="col-sm-6">
									{!! Form::select('layout', array('Duplex' => 'Duplex', 'Penthouse' => 'Penthouse', 'Simplex' => 'Simplex', 'Triplex' => 'Triplex'), $property->layout, ['class'=>'form-control', 'placeholder' => 'Choose a layout...']); !!}
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-description" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-6">
									{!! Form::textarea('description', $property->description, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-bedroom" class="col-sm-3 control-label">Bedroom</label>
								<div class="col-sm-6">
									<input type="number" step="0.5" name="bedroom" id="property-bedroom" class="form-control" value="{{ old('bedroom', $property->bedroom) }}" min="0">
								</div>
							</div>
							
							<div class="form-group">
								<label for="property-bathroom" class="col-sm-3 control-label">Bathroom</label>
								<div class="col-sm-6">
									<input type="number" step="0.5" name="bathroom" id="property-bathroom" class="form-control" value="{{ old('bathroom', $property->bathroom) }}" min="0">
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-maidroom" class="col-sm-3 control-label">Maidroom</label>
								<div class="col-sm-6">
									<input type="number" step="0.5" name="maidroom" id="property-maidroom" class="form-control" value="{{ old('maidroom', $property->maidroom) }}" min="0">
								</div>
							</div>
						
							<hr/>
						
							<div class="form-group">
								<label for="property-gross_area" class="col-sm-3 control-label">Gross Area (sq.ft.)</label>
								<div class="col-sm-6">
									<input type="number" name="gross_area" id="property-gross_area" class="form-control" value="{{ old('gross_area', $property->gross_area) }}" min="0">
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-saleable_area" class="col-sm-3 control-label">Saleable Area (sq.ft.)</label>
								<div class="col-sm-6">
									<input type="number" name="saleable_area" id="property-saleable_area" class="form-control" value="{{ old('saleable_area', $property->saleable_area) }}" min="0"> 
								</div>
							</div>

							<div class="form-group form-checkbox-group">
								<label for="property-outdoors" class="col-sm-3 control-label">Outdoor Space</label>
								<div class="col-sm-6">
									{{ Form::hidden('outdoors', '0') }}
									@foreach ($outdoor_ids as $outdoor_id => $outdoor)
										<label for="property-outdoors-{{$outdoor_id}}" class="control-label">{!! Form::checkbox('outdoors[]', $outdoor_id, in_array($outdoor_id, $outdoors), ['id'=>'property-outdoors-'.$outdoor_id]) !!} {{ $outdoor}}
											<input type="number" name="outdoorareas[{{ strtolower($outdoor) }}]" id="property-outdoor-{{ strtolower($outdoor) }}" class="form-control" value="{{ isset($outdoorareas[strtolower($outdoor)])?$outdoorareas[strtolower($outdoor)]:'' }}" min="0">sq.ft.
										</label>
									@endforeach
								</div>
							</div>

							<div class="form-group form-checkbox-group">
								<label for="property-features" class="col-sm-3 control-label">Features</label>
								<div class="col-sm-6">
									{{ Form::hidden('features', '0') }}
									@foreach ($feature_ids as $feature_id => $feature)
										 <label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, $features), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature}}
									<?php if($feature_id == 5): // exception car park?>
										 <input type="number" name="car_park" id="property-car_park" class="form-control" value="{{ old('car_park', $property->car_park) }}" min="0">
									<?php endif;?>
										 </label>
									@endforeach
								</div>
							</div>
						
							<hr/>
						
	<div id="property-sale-section">
							<div class="form-group">
								<label for="property-asking_sale" class="col-sm-3 control-label">Asking Sale (HK$)</label>
								<div class="col-sm-6">
									<input type="number" name="asking_sale" id="property-asking_sale" class="form-control" value="{{ old('asking_sale', $property->asking_sale) }}" min="0"> 
								</div>
							</div>
	</div>
	<div id="property-rent-section">
							<div class="form-group">
								<label for="property-asking_rent" class="col-sm-3 control-label">Asking Rent (HK$)</label>
								<div class="col-sm-6">
									<input type="number" name="asking_rent" id="property-asking_rent" class="form-control" value="{{ old('asking_rent', $property->asking_rent) }}" min="0"> 
								</div>
							</div>
	</div>
						
							<div class="form-group">
								<label for="property-inclusive" class="col-sm-3 control-label">Inclusive</label>
								<div class="col-sm-6">
									 <label for="property-inclusive-1" class="control-label">{!! Form::radio('inclusive', 1, $property->inclusive===1, ['id'=>'property-inclusive-1']) !!} Yes</label>
									 <label for="property-inclusive-2" class="control-label">{!! Form::radio('inclusive', 0, $property->inclusive===0, ['id'=>'property-inclusive-2']) !!} No</label>
								</div>
							</div>
						
	<div id="property-inclusive-section">
							<div class="form-group">
								<label for="property-management_fee" class="col-sm-3 control-label">Managment Fee (HK$)</label>
								<div class="col-sm-6">
									<input type="number" name="management_fee" id="property-management_fee" class="form-control" value="{{ old('management_fee', $property->management_fee) }}" min="0"> 
								</div>
							</div>
						
							<div class="form-group">
								<label for="property-government_rate" class="col-sm-3 control-label">Goverment Rate (HK$)</label>
								<div class="col-sm-6">
									<input type="number" name="government_rate" id="property-government_rate" class="form-control" value="{{ old('government_rate', $property->government_rate) }}" min="0"> per quarter 
								</div>
							</div>
	</div>
							<hr/>

							<div class="form-group">
								<label for="vendor-poc" class="col-sm-3 control-label">Point of Contact</label>
								<div class="col-sm-6">
									 <label for="vendor-poc-1" class="control-label">{!! Form::radio('poc_id', 1, $property->poc_id == 1, ['id'=>'vendor-poc-1']) !!} Owner</label>
									 <label for="vendor-poc-2" class="control-label">{!! Form::radio('poc_id', 2, $property->poc_id == 2, ['id'=>'vendor-poc-2']) !!} Rep</label>
									 <label for="vendor-poc-3" class="control-label">{!! Form::radio('poc_id', 3, $property->poc_id == 3, ['id'=>'vendor-poc-3']) !!} Agency</label>
								</div>
							</div>

							<div class="form-group">
								<label for="property-owner" class="col-sm-3 control-label">Owner Name</label>
								<div class="col-sm-6">
									<input type="text" name="owner" id="property-owner" class="form-control" value="{{ old('owner', $property['owner']?$property['owner']->basic_info():'') }}">
									<span id="property-owner-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the owner name"><i class="fa fa-btn fa-close"></i></button></span>
								</div>
							</div>

							<div class="form-group" style="display: none">
								<label for="property-owner_id" class="col-sm-3 control-label">Owner id (hidden)</label>
								<div class="col-sm-6">
									<input type="hidden" name="owner_id" id="property-owner_id" class="form-control" value="{{ old('owner_id', $property['owner']?$property['owner']->id:'') }}"> 
								</div>
							</div>

	<div id="property-vendor-rep-section" style="display: block;">
							<div class="form-group form-checkbox-group">
								<label for="property-rep_id" class="col-sm-3 control-label">Rep Name</label>
								<div class="col-sm-6">

	<div id="property-vendor-rep-checkbox-template" style="display: none">
									<label for="property-rep_id-checkbox-n" class="control-label">{!! Form::checkbox('rep_ids[]', 0, null, ['id'=>'property-rep_id-checkbox-n']) !!} </label>
	</div>
	@if(!empty($property['owner']) && count($property['allreps']) > 0)
	<?php $rep_ids = !empty($property->rep_ids)?explode(',',$property->rep_ids):array();?>
	@foreach($property['allreps'] as $rep)
		<label for="property-rep_id-checkbox-{{$rep->id}}" class="control-label">
			{!! Form::checkbox('rep_ids[]', $rep->id, in_array($rep->id, $rep_ids), ['id'=>'property-rep_id-checkbox-'.$rep->id]) !!} {{ $rep->basic_info() }}
		</label>
	@endforeach
	@endif
								</div>
							</div>
	</div>

							<div class="form-group">
								<label for="property-agent" class="col-sm-3 control-label">Agency Name</label>
								<div class="col-sm-6">
									<input type="text" name="agent" id="property-agent" class="form-control" value="{{ old('agent', $property['agent']?$property['agent']->basic_info():'') }}">
									<span id="property-agent-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the agent name"><i class="fa fa-btn fa-close"></i></button></span>
								</div>
							</div>

	<div id="property-vendor-agent-section" style="display: block;">
							<div class="form-group form-checkbox-group">
								<label for="property-agent_id" class="col-sm-3 control-label">Agent Name</label>
								<div class="col-sm-6">

	<div id="property-vendor-agent-checkbox-template" style="display: none">
									<label for="property-agent_id-checkbox-n" class="control-label">{!! Form::checkbox('agent_ids[]', 0, null, ['id'=>'property-agent_id-checkbox-n']) !!} </label>
	</div>
	@if(!empty($property['agent']) && count($property['allagents']) > 0)
	<?php $agent_ids = !empty($property->agent_ids)?explode(',',$property->agent_ids):array();?>
	@foreach($property['allagents'] as $agent)
		<label for="property-agent_id-checkbox-{{$agent->id}}" class="control-label">
			{!! Form::checkbox('agent_ids[]', $agent->id, in_array($agent->id, $agent_ids), ['id'=>'property-agent_id-checkbox-'.$agent->id]) !!} {{ $agent->basic_info() }}
		</label>
	@endforeach
	@endif
								</div>
							</div>
	</div>

							<div class="form-group" style="display: none">
								<label for="property-agent_id" class="col-sm-3 control-label">Agent id (hidden)</label>
								<div class="col-sm-6">
									<input type="hidden" name="agent_id" id="property-agent_id" class="form-control" value="{{ old('agent_id', $property['agent']?$property['agent']->id:'') }}"> 
								</div>
							</div>

							<div class="form-group">
								<label for="property-key_loc_id" class="col-sm-3 control-label">Key Location </label>
								<div class="col-sm-6">
									<label for="property-key_loc_id-1" class="control-label">{!! Form::radio('key_loc_id', 1, $property->key_loc_id == 1, ['id'=>'property-key_loc_id-1']) !!} Managment Office</label>
									<label for="property-key_loc_id-2" class="control-label">{!! Form::radio('key_loc_id', 2, $property->key_loc_id == 2, ['id'=>'property-key_loc_id-2']) !!} Door Code</label>
									<label for="property-key_loc_id-3" class="control-label">{!! Form::radio('key_loc_id', 3, $property->key_loc_id == 3, ['id'=>'property-key_loc_id-3']) !!} Agency</label>
									<label for="property-key_loc_id-4" class="control-label">{!! Form::radio('key_loc_id', 4, $property->key_loc_id == 4, ['id'=>'property-key_loc_id-4']) !!} Other</label>
								</div>
							</div>

	<div id="property-key-loc-other-section">
							<div class="form-group">
								<label for="property-key_loc_other" class="col-sm-3 control-label">Key Location Other</label>
								<div class="col-sm-6">
									<input type="text" name="key_loc_other" id="property-key_loc_other" class="form-control" value="{{ old('key_loc_other', $property->key_loc_other) }}">
								</div>
							</div>
	</div>

							<div class="form-group">
								<label for="property-door_code" class="col-sm-3 control-label">Door Code</label>
								<div class="col-sm-6">
									<input type="text" name="door_code" id="property-door_code" class="form-control" value="{{ old('door_code', $property->door_code) }}">
								</div>
							</div>

							<div class="form-group">
								<label for="property-agency_fee" class="col-sm-3 control-label">Agency Fee</label>
								<div class="col-sm-6">
									<input type="text" name="agency_fee" id="property-agency_fee" class="form-control" value="{{ old('agency_fee', $property->agency_fee) }}" placeholder="">
								</div>
							</div>
							
							<div class="form-group">
								<label for="property-hot" class="col-sm-3 control-label">Hot</label>
								<div class="col-sm-6">
									 <label for="property-hot-0" class="control-label">{!! Form::radio('hot', 0, $property->hot == 0, ['id'=>'property-hot-0']) !!} No</label>
									 <label for="property-hot-1" class="control-label">{!! Form::radio('hot', 1, $property->hot == 1, ['id'=>'property-hot-1']) !!} Yes</label>
								</div>
							</div>

							<div class="form-group">
								<label for="test-available_date" class="col-sm-3 control-label">Available Date</label>
								<div class="col-sm-6">
									{{ Form::date('available_date', old('available_date', $property->available_date), ['class'=>'form-control']) }}
								</div>
							</div>

							<div class="form-group">
								<label for="property-leased" class="col-sm-3 control-label">Leased</label>
								<div class="col-sm-6">
									 <label for="property-leased-0" class="control-label">{!! Form::radio('leased', 0, $property->leased == 0, ['id'=>'property-leased-0']) !!} No</label>
									 <label for="property-leased-1" class="control-label">{!! Form::radio('leased', 1, $property->leased == 1, ['id'=>'property-leased-1']) !!} Yes</label>
								</div>
							</div>

							<div class="form-group">
								<label for="property-sold" class="col-sm-3 control-label">Sold</label>
								<div class="col-sm-6">
									 <label for="property-sold-0" class="control-label">{!! Form::radio('sold', 0, $property->sold == 0, ['id'=>'property-sold-0']) !!} No</label>
									 <label for="property-sold-1" class="control-label">{!! Form::radio('sold', 1, $property->sold == 1, ['id'=>'property-sold-1']) !!} Yes</label>
								</div>
							</div>
						
							<hr/>
						
							<!-- Add Submit Button -->
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									{{ Form::hidden('previous_url', $previous_url) }}
									{{ Form::hidden('id', $property->id) }}
									<button type="submit" class="btn btn-default">
										<i class="fa fa-btn fa-pencil-square-o"></i>Update </button>
									<!-- <button type="button" class="btn btn-default">
											<i class="fa fa-btn fa-ban"></i>Cancel </button> -->
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Property Media
					</div>
					<div class="panel-body">
						<form class="form-horizontal">
	@include('partials.include-plupload', array(
			'id'    => 'property_nest_photos',
			'label' => 'Nest Photo',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'nest-photo',
						'type'  => 'database',
					)))

	@include('partials.include-plupload', array(
			'id'    => 'property_nest_photos_website',
			'label' => 'Nest Photo (Website)',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'nest-photo',
						'type'  => 'website',
					)))

	@include('partials.include-plupload', array(
			'id'    => 'property_nest_doc',
			'label' => 'Nest Document',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'nest-doc',
						'type'  => 'database',
					)))

	@include('partials.include-plupload', array(
			'id'    => 'property_other_photos',
			'label' => 'Other Photo',
			'vars'  => array(
						'id'    => $property->id,
						'group' => 'other-photo',
						'type'  => 'database',
					)))
						</form>
					</div>
				</div>

	<!-- connect hong kong homes photos -->
				<div id="hongkonghomesphotos" class="panel panel-default">
					<div class="panel-heading">
						Connect Hong Kong Homes photos
					</div>
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="property-property_url" class="col-sm-3 control-label">Hong Kong Homes URL</label>
								<div class="col-sm-6">
									<input type="text" name="property_url" id="property-property_url" class="form-control" value="{{ old('hkh_url', $property->hkh_url) }}" placeholder="">
								</div>
							</div>

							<!-- Add Submit Button -->
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									{{ Form::hidden('property_id', $property->id) }}
									<button type="submit" class="btn btn-default">
										<i class="fa fa-btn fa-pencil-square-o"></i>Submit </button>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									<div id="hongkonghomesphotos_result"></div>
								</div>
							</div>
						</form>
					</div>
				</div>

	<!-- delete property -->
				<div class="panel panel-default">
					<div class="panel-heading">
						Delete Property
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="{{url('/property/delete')}}" method="post">
							<div class="form-group">
								<div class="col-sm-6">
									{{ csrf_field() }}
									{{ Form::hidden('previous_url', $previous_url) }}
									{{ Form::hidden('property_id', $property->id) }}
									<button type="submit" class="btn btn-danger">
										<i class="fa fa-btn fa-trash-o"></i>Delete </button>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="btn btn-default" onClick="addComment();">Add Comment</button>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading">
						Old Data from ProppertyBase
					</div>
					<div class="panel-body">
						<form>
						@if(!empty($migration_metas))
							@foreach($migration_metas as $meta)
							@if(!in_array($meta->meta_key, array('user_id')))
							<div class="form-group">
								<label for="property-type" class="col-sm-12 control-label">{{ $meta->meta_key }} </label>
								<div class="col-sm-12">
									@if (is_array($meta->meta_value))
										@if ($meta->meta_key == 'outdoors')
											@if(!empty($meta->meta_value))
												@foreach($meta->meta_value as $outdoor_id)
												@if(isset($outdoor_ids[$outdoor_id]))
												<span>{{ $outdoor_ids[$outdoor_id] }}</span>
												@endif
												@endforeach
											@else
												<span>n/a</span>
											@endif
										@elseif($meta->meta_key == 'features')
											@if(!empty($meta->meta_value))
												@foreach($meta->meta_value as $feature_id)
												@if(isset($feature_ids[$feature_id]))
												<span>{{ $feature_ids[$feature_id] }}</span>
												@endif
												@endforeach
											@else
												<span>n/a</span>
											@endif
										@endif
									@elseif($meta->meta_key == 'post_content')
									<span>{!! $meta->get_fixed_post_content() !!}</span>
									@else
									<span>{{ $meta->meta_value }}</span>
									@endif
								</div>
							</div>
							@endif
							@endforeach
						@endif
						</form>
					</div>
				</div>
			</div>
		</div>
    </div>
	
	
	
	<script>			
		function loadCommentsEdit(){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'GET',
				url: '{{ url("/comments/".$property->id) }}',
				cache: false,
				success: function (data) {
					jQuery('#existingCommentsEdit').html(data);
				},
				error: function (data) {
					jQuery('#existingCommentsEdit').html(data);
					jQuery.ajax({
						type: 'GET',
						url: '{{ url("/comments/".$property->id) }}',
						cache: false,
						success: function (data) {
							jQuery('#existingCommentsEdit').html(data);
						},
						error: function (data) {
							jQuery.ajax({
								type: 'GET',
								url: '{{ url("/comments/".$property->id) }}',
								cache: false,
								success: function (data) {
									jQuery('#existingCommentsEdit').html(data);
								},
								error: function (data) {
									jQuery('#existingCommentsEdit').html('An error occurred '+data);
								}
							});
						}
					});
				}
			});
		}
		
		function addComment(){
			var comment = jQuery('#newcomment').val().trim();
			if (comment.length > 0){
				jQuery.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				jQuery.ajax({
					type: 'POST',
					url: '{{ url("/commentadd/".$property->id) }}',
					data: {comment: comment},
					dataType: 'text',
					cache: false,
					async: false,
					success: function (data) {
						jQuery('#newcomment').val('');
						loadCommentsEdit();
					},
					error: function (data) {
						console.log(data);
						alert('Something went wrong, please try again!');
					}
				});
			}else{
				alert('Please enter a comment');
			}
		}
		
		$('#newcomment').keydown(function( event ) {
			if ( event.keyCode == 13 ) {
				event.preventDefault();
				addComment();
			}				
		});
		
		function removeComment(t){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/commentremove/".$property->id) }}',
				data: {t: t},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					loadCommentsEdit();
				},
				error: function (data) {
					alert('Something went wrong, please try again!');
				}
			});
		}
		
		jQuery( document ).ready(function(){
			loadCommentsEdit();
		});
		
		jQuery('form').on('focus', 'input[type=number]', function (e) {
			jQuery(this).on('mousewheel.disableScroll', function (e) {
				e.preventDefault();
			});
		});
		jQuery('form').on('blur', 'input[type=number]', function (e) {
			jQuery(this).off('mousewheel.disableScroll');
		});
	</script>
	
	
	

@include('partials.property.building-modal')
@include('partials.property.vendor-owner-modal')
@include('partials.property.vendor-agent-modal')
@endsection

@section('footer-script')
<?php
/**
 * Part A) easy automation popup flow:
 * 1) easyAutocomplete setting
 * 2) create new item ajax
 * 3) set content
 *
 * Part B) control - field display control by radio button
 */
?>
@include('partials.property.building-modal-control')
@include('partials.property.feature-control')
@include('partials.property.vendor-owner-modal-control')
@include('partials.property.vendor-agent-modal-control')
@include('partials.property.field-display-control')
@include('partials.property.hongkonghomesphotos-control')

@include('partials.include-plupload-control', array(
    'id'      => 'property_nest_photos',
    'inc_lib' => '1',
    'url'     => secure_url('/property/upload'),
    'types'   => array('image'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'nest-photo',
                    'type'  => 'database',
                ),
    ))

@include('partials.include-plupload-control', array(
    'id'      => 'property_nest_photos_website',
    'inc_lib' => '1',
    'url'     => secure_url('/property/upload'),
    'types'   => array('image'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'nest-photo',
                    'type'  => 'website',
                ),
    ))

@include('partials.include-plupload-control', array(
    'id'      => 'property_nest_doc',
    'inc_lib' => '1',
    'url'     => secure_url('/property/upload'),
    'types'   => array('pdf'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'nest-doc',
                    'type'  => 'database',
                ),
    ))

@include('partials.include-plupload-control', array(
    'id'      => 'property_other_photos',
    'url'     => secure_url('/property/upload'),
    'types'   => array('image'),
    'vars'    => array(
                    'model' =>'property',
                    'id'    => $property->id,
                    'group' => 'other-photo',
                    'type'  => 'database',
                ),
    ))
@endsection