@if(Request::session()->has('shortlist_id'))
<div style="position: fixed; right: 20px; bottom: 20px; z-index: 888;">
    <button id="shortlist_button" type="button" class="btn btn-primary btn-lg" disabled="disabled">
        <i class="fa fa-btn fa-thumb-tack"></i><span>Choose and put property into shortlist</span>
    </button>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    	var form_table = '.property-table';
    	var property_ids = [];
    	var button_id = '#shortlist_button';
    	update_checkbox_count();
    	$(form_table+' input[type=checkbox]').change(function(){
    		update_checkbox_count();
    	});
        $(button_id).click(function(){
            console.info(property_ids);
            update_shortlist();
        });
    	function update_checkbox_count() {
    		property_ids = [];
    		$(form_table + ' input[type=checkbox]:checked').each(function(index){
	    		property_ids.push($(this).val());
    		});
	    	if (property_ids.length > 1) {
                $(button_id).removeAttr('disabled').find('span').text('Put '+property_ids.length+' properties into shortlist');
	    	} else if (property_ids.length == 1) {
                $(button_id).removeAttr('disabled').find('span').text('Put property into shortlist');
            } else {
                $(button_id).find('span').text('Choose and put property into shortlist');
                $(button_id).prop('disabled', 'disabled');
            }
            $(button_id).css({fontSize: '1.5em'}).animate({fontSize: '1em'}, 'fast');
    	}
        function update_shortlist() {
            $.get("{{ url('/shortlist/update/'. Request::session()->get('shortlist_id')) }}", {property_ids: property_ids}, function(data) {
                console.info(data.error);
                if (!data.error) {
                    $(form_table+' input[type=checkbox]').prop('checked', '');
                    update_checkbox_count();
                } 
            });
        }
    });
</script>
@endif