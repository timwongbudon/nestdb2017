<script type="text/javascript">
$(document).ready(function(){
    _control_property_sale_rent($('input[name=type_id]:checked').val());
    $('input[name=type_id]').change(function(){
        _control_property_sale_rent($(this).val());
    });
});

function _control_property_sale_rent(type_id) {
    if (type_id == 3) { // both
        $('#property-sale-section').show();
        $('#property-rent-section').show();
    } else if (type_id == 2) {
        $('#property-sale-section').show();
        $('#property-rent-section').hide();
    } else {
        $('#property-sale-section').hide();
        $('#property-rent-section').show();
    }
}

$(document).ready(function(){
    _control_property_inclusive($('input[name=inclusive]:checked').val());
    $('input[name=inclusive]').change(function(){
        _control_property_inclusive($(this).val());
    });
});

function _control_property_inclusive(inclusive) {
    if (inclusive == 1) {
        $('#property-inclusive-section').hide().find('input').val(0);
    } else {
        $('#property-inclusive-section').show();
    }
}

$(document).ready(function(){
    $("#property-asking_sale").on('keydown', function(event) { 
        var keyCode = event.keyCode || event.which; 
        if (keyCode == 9) { // tab keydown
            var res = parseFloat($(this).val());
            if (res < 1000000) {
                $(this).val(res*1000000);
            }
        } 
    });
    $("#property-asking_rent").on('keydown', function(event) { 
        var keyCode = event.keyCode || event.which; 
        if (keyCode == 9) { // tab keydown
            var res = parseFloat($(this).val());
            if (res < 1000) {
                $(this).val(res*1000);
            }
        } 
    });
});

$(document).ready(function(){
    _control_property_key_loc($('input[name=key_loc_id]:checked').val());
    $('input[name=key_loc_id]').change(function(){
        _control_property_key_loc($(this).val());
    });
});

function _control_property_key_loc(key_loc_id) {
    if (key_loc_id != 4) { // other
        $('#property-key-loc-other-section').hide().find('input').val('');
    } else {
        $('#property-key-loc-other-section').show();
    }
}

</script>