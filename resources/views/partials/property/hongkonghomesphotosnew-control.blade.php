<script type="text/javascript">
$(document).ready(function(){
	$('#newhongkonghomesphotos button').click(function(){
		var property_id = $("input[name=newproperty_id]").val();
		var property_url = $("input[name=newproperty_url]").val();
		if (property_url == '') {
			alert('Please input Hong Kong Homes URL');
			$("input[name=newproperty_url]").focus();
			return false;
		}
		$.get("{{ url('/property/connect-newhongkonghomesphotos') }}", { id: property_id, url: property_url }, function(data) {
	        console.info(data);
	        var result = $('#newhongkonghomesphotos_result');
	        jQuery.each( data, function( i, photo ) {
				result.append("<div><img src='"+photo.url+"' width='200' style='margin-bottom: 10px;' /></div>");
			});
	    });
		return false;
	});
});
</script>