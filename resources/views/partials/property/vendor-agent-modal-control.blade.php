<script type="text/javascript">
var agent_input_field = "property-agent";
var agent_input_value = "";
var agent_modal_id = "property-vendor-agent-modal";
var options = {
    url: function(phrase) {return "{{ url('/vendors/api/agency/index?keywords=') }}" + phrase;}, 
    getValue: function(element) {return element.company + ', ' + element.firstname + ' ' + element.lastname;},
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id!='') {
                return item.company + ' ' + item.firstname + ' ' + item.lastname;
            } else {
                return item.name;
            }
        }
    },
    list: {
        maxNumberOfElements: 12,
        onShowListEvent: function() {
            agent_input_value = $("#"+agent_input_field).val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#"+agent_input_field).getSelectedItemData().action;
            switch(action) {
                case "create-new-item":
                    $("#"+agent_modal_id).modal();
                    break;
                case "manage-item":
                    $("#"+agent_input_field).val(agent_input_value);
                    var win = window.open('{{ url('/vendors/') }}', '_vendor');
                    win.focus(); 
                    break;
                default:
                    var content;
                    content = $("#"+agent_input_field).getSelectedItemData();
                    $.get("{{ url('/vendors/api/child/') }}" + '/' + content.id, function(data) {
                        if (!data.error) {
                            $("#"+agent_input_field).val(content.company + ' ' + content.firstname + ' ' + content.lastname);
                            $("#"+agent_input_field+'_id').val(content.id);
                            content = data.content;
                            set_property_agent(content);
                        }
                    }).fail(function(){
                    });
                    break;
            }
        }
    },
    highlightPhrase: false
};
$("#"+agent_input_field).easyAutocomplete(options);

$(document).ready(function(){
    $("#"+agent_input_field+"-display button").click(function(event){
        event.preventDefault();
        unset_property_agent();
    });
    set_property_agent_display();
});

function set_property_agent(content) {
    // $("#"+agent_input_field).val(content.company + ' ' + content.firstname + ' ' + content.lastname);
    // $("#"+agent_input_field+'_id').val(content.id);
    // set_property_agent_display();

    var vendor_agent_section= $("#property-vendor-agent-section .form-group > div");
    unset_property_agents();
    $.each(content, function(index, agent){
        var elem = $('#property-vendor-agent-checkbox-template').clone();
        var elem_id = 'property-agent_id-checkbox-'+index;
        elem.find('label').attr('for', elem_id);
        elem.find('input').attr('id', elem_id).val(agent.id);
        // console.info(agent);
        var elem_tel = '';
        if(agent.tel) elem_tel = ' (' + agent.tel + ')';
        elem.find('label').append(agent.firstname + ' ' + agent.lastname + elem_tel);
        vendor_agent_section.append(elem.html());
    });
    vendor_agent_section.find('span').remove();
    if (content.length==0) {
        vendor_agent_section.append("<span>"+"-"+"</span>");
    }
    $('#property-vendor-agent-section').show();
    set_property_agent_display();
}

function set_property_agent_display() {
    var agent_id = $("#"+agent_input_field+"_id").val();
    if (agent_id) {
        $("#"+agent_input_field+"-display").show()
            .find('a').text($("#"+agent_input_field).hide().val())
            .attr('target', '_vendor')
            .attr('href', get_property_agent_url(agent_id));
    }
}

function unset_property_agent() {
    unset_property_agents();
    $("#property-agent_id").val(''); 
    $("#"+agent_input_field).val('').show().focus();
    $("#"+agent_input_field+"-display").hide().find('a').text(''); 
}


function unset_property_agents() {
    var vendor_agent_section= $("#property-vendor-agent-section .form-group > div");
    vendor_agent_section.find('label').each(function(index, item){
        if (index>0) {$(this).remove();}
    });
    vendor_agent_section.find('span').remove();
    vendor_agent_section.append("<span>"+"-"+"</span>");
}

function get_property_agent_url(vendor_id) {
    return "{{ url('/vendor/show/') }}" + '/' + vendor_id; 
}
</script>