<!-- property vendor owner modal start -->
<div id="property-vendor-owner-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                   Create New Owner
                </h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <label for="property-vendor_owner_name" class="col-sm-3 control-label">Owner Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="property-vendor_owner_name" class="form-control" value="{{ old('name') }}"> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                    <button id="test3_btn" type="submit" class="btn btn-default btn-default pull-left">
                        <span class="fa fa-btn fa-plus"></span> Create
                    </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- property vendor owner modal start -->