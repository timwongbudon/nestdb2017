<script type="text/javascript">
var building_input_field = "property-name";
var building_input_value = "";
var building_modal_id = "property-building-modal";
var options = {
    url: function(phrase) {return "{{ url('/buildings/api/index?keywords=') }}" + phrase;}, 
    getValue: function(element) {return element.name;},
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id!='') {
                return value + ' <i style="color: green">' + item.address1 +'</i>';
            } else {
                return value;
            }
        }
    },
    list: {
        maxNumberOfElements: 30,
        onShowListEvent: function() {
            building_input_value = $("#"+building_input_field).val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#"+building_input_field).getSelectedItemData().action;
            switch(action) {
                case "create-new-item":
                    $("#"+building_modal_id).modal();
                    break;
                case "manage-item":
                    $("#"+building_input_field).val(building_input_value);
                    var win = window.open('{{ url('/buildings/') }}', '_building');
                    win.focus();
                    break;
                default:
                    var content;
                    content = $("#"+building_input_field).getSelectedItemData();
                    set_property_building(content);
                    break;
            }
        },
    },
    highlightPhrase: false
};
var options_search = {
    url: function(phrase) {return "{{ url('/buildings/api/index?keywords=') }}" + phrase;}, 
    getValue: function(element){
		if (element.address1 != undefined){
			return element.name+'<br /><i style="color: green">'+element.address1+'</i>'; 
		}else{
			return element.name;
		}
	},
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id!='') {
                //return value + ' <i style="color: green">' + item.address1 +'</i>';
				return value;
            } else {
                return value;
            }
        }
    },
    list: {
        maxNumberOfElements: 30,
        onShowListEvent: function() {
            building_input_value = $("#search-navbar").val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#search-navbar").getSelectedItemData().action;
            switch(action) {
                case "create-new-item":
                    $("#"+building_modal_id).modal();
                    break;
                case "manage-item":
                    $("#search-navbar").val(building_input_value);
                    var win = window.open('{{ url('/buildings/') }}', '_building');
                    win.focus();
                    break;
                default:
                    var content;
                    content = $("#search-navbar").getSelectedItemData();
					$('#navbar-search-form').submit();
                    break;
            }
        },
    },
    highlightPhrase: false
};
$("#"+building_input_field).easyAutocomplete(options);
$("#search-navbar").easyAutocomplete(options_search);


var search_clients = {
    url: function(phrase) {return "{{ url('/client/api/lookup?keyword=') }}" + phrase;}, 
    getValue: function(element){
		if (element.firstname != undefined){
			return element.firstname+' ' + element.lastname+ ' ('+element.id+')'; 
		}else{
			return element.firstname;
		}
	},
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id!='') {
                //return value + ' <i style="color: green">' + item.address1 +'</i>';
				return value;
            } else {
                return value;
            }
        }
    },
    list: {
        maxNumberOfElements: 30,
        onShowListEvent: function() {
            building_input_value = $("#clientid").val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#clientid").getSelectedItemData().id;
            $("#clientid").val(action);
          
        },
    },
    highlightPhrase: false
};
$("#clientid").easyAutocomplete(search_clients);

$(document).ready(function(){
    $("#"+building_input_field+"-display button").click(function(event){
        event.preventDefault();
        unset_property_building();
    });
    set_property_building_display();
});

function set_property_building(content) {
    $("#"+building_input_field).val(content['name']);
    $("#property-address1").val(content['address1']);
    $("#property-address2").val(content['address2']);
    $("#property-year_built").val(content['year_built']);
    $("#property-district_id option[value='"+content['district_id']+"']").prop("selected", "selected"); 
    $("#property-country_code option[value='"+content['country_code']+"']").prop("selected", "selected"); 
    $("#building_id").val(content['id']);
	
	$(".property-feature-input:checked").prop('checked', false);
		
	if (content['facilities'] != null){
		var facilities = content['facilities'].split(",");
		for (var i = 0; i < facilities.length; i++){
			$("#property-features-"+facilities[i]).prop('checked', true);
		}
	}
	
    set_property_building_display();
}

function set_property_building_display() {
    var building_id = $("#building_id").val();
    if (building_id > 0){
        $("#"+building_input_field+"-display").show()
        .find('a').text($("#"+building_input_field).hide().val())
        .attr('target', '_building')
        .attr('href', get_property_building_url(building_id));
    }else if (building_id == 0 && $("#"+building_input_field).val() != ''){
		$("#"+building_input_field+"-display").show()
        .find('a').text($("#"+building_input_field).hide().val()).css("color", "#212121").css("text-decoration", "none");
	}
}

function unset_property_building() {
    $("#property-address1").val('');
    $("#property-address2").val('');
    $("#property-year_built").val('');
    $("#property-district_id option[value='']").prop("selected", "selected"); 
    $("#property-country_code option[value='hk']").prop("selected", "selected"); 
    $("#building_id").val('');
    $("#"+building_input_field).val('').show().focus();
    $("#"+building_input_field+"-display").hide().find('a').text('');
}

function get_property_building_url(building_id) {
    return "{{ url('/building/show/') }}" + '/' + building_id;
}

</script>