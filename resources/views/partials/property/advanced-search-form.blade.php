

	<!-- New Property Form -->
	<!-- <form action="{{ !empty($template_type)?url($template_type.'/properties'):url('properties') }}" method="GET" class="form-horizontal"> -->
	<form action="{{ url('/listadvsearch') }}" method="get" class="form-horizontal">
		{{ csrf_field() }}
		<div class="form-group">
			<div class="col-sm-3">
				<label for="property-name" class="control-label">Building</label>
				<input type="text" name="name" id="property-name" class="form-control" value="{{ old('name', $input->name) }}">
				<span id="property-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
			</div>
			<div class="col-sm-3">
				<label for="property-unit" class="control-label">Unit</label>
				<input type="text" name="unit" id="property-unit" class="form-control" value="{{ old('unit', $input->unit) }}">
			</div>
			<div class="col-sm-3">
				<label for="property-address1" class="control-label">Address</label>
				<input type="text" name="address1" id="property-address1_exception" class="form-control" value="{{ old('name', $input->address1) }}">
			</div>
			<div class="col-sm-3">
				<label for="property-type_id" class="control-label">Rent or Sale</label>
				{!! Form::select('type_id', array('0'=>'Any', '1'=>'Rent', '2'=>'Sale'), $input->type_id, ['id'=>'property-type_id', 'class'=>'form-control']); !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-3">
				<label for="property-price_min" class="control-label">Min Price (HK$)</label>
				<input type="number" name="price_min" id="property-price_min" class="form-control" value="{{ old('price_min', $input->price_min) }}" min="0">
			</div>
			<div class="col-sm-3">
				<label for="property-price_max" class="control-label">Max Price (HK$)</label>
				<input type="number" name="price_max" id="property-price_max" class="form-control" value="{{ old('price_max', $input->price_max) }}" min="0">
			</div>
			<div class="col-sm-3">
				<label for="property-size_min" class="control-label">Min Size (sq.ft.)</label>
				<input type="number" name="size_min" id="property-size_min" class="form-control" value="{{ old('size_min', $input->size_min) }}" min="0">
			</div>
			<div class="col-sm-3">
				<label for="property-size_max" class="control-label">Max Size (sq.ft.)</label>
				<input type="number" name="size_max" id="property-size_max" class="form-control" value="{{ old('size_max', $input->size_max) }}" min="0">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-3">
				<label for="property-bedroom_min" class="control-label">Min Bedrooms</label>
				<input type="number" name="bedroom_min" id="property-bedroom_min" class="form-control" value="{{ old('bedroom_min', $input->bedroom_min) }}" min="0">
			</div>
			<div class="col-sm-3">
				<label for="property-bedroom_max" class="control-label">Max Bedrooms</label>
				<input type="number" name="bedroom_max" id="property-bedroom_max" class="form-control" value="{{ old('bedroom_max', $input->bedroom_max) }}" min="0">
			</div>
			<div class="col-sm-3">
				<label for="property-bathroom_min" class="control-label">Min Bathrooms</label>
				<input type="number" name="bathroom_min" id="property-bathroom_min" class="form-control" value="{{ old('bathroom_min', $input->bathroom_min) }}" min="0">
			</div>
			<div class="col-sm-3">
				<label for="property-district_id" class="control-label">District</label>
				{!! Form::select('district_id', $district_ids, $input->district_id, ['id'=>'property-district_id_exception', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-3">
				<label for="property-comments" class="control-label">Comments</label>
				<input type="text" name="comments" id="property-comments" class="form-control" value="{{ old('comments', $input->comments) }}" >
			</div>
			<div class="col-sm-3">
				<label for="property-description" class="control-label">Description</label>
				<input type="text" name="description" id="property-description" class="form-control" value="{{ old('description', $input->description) }}" >
			</div>
			<div class="col-sm-3">
				<label for="property-id" class="control-label">ID</label>
				<input type="text" name="id" id="property-id" class="form-control" value="{{ old('id', $input->id) }}" >
			</div>
			<div class="col-sm-3">
				<label for="property-district_id" class="control-label">Sort By</label>
				{!! Form::select('sort_by', $custom_listing_options, $input->sort_by, ['id'=>'property-sort_by', 'class'=>'form-control']); !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">
			<label for="property-outdoors" class="control-label">Outdoors</label><br/>
			@foreach ($outdoor_ids as $outdoor_id => $outdoor)
				<label for="property-outdoors-{{$outdoor_id}}" class="control-label">{!! Form::checkbox('outdoors[]', $outdoor_id, in_array($outdoor_id, isset($input->outdoors)?$input->outdoors:array()), ['id'=>'property-outdoors-'.$outdoor_id]) !!} {{ $outdoor}}
				</label>
			@endforeach
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">
			<label for="property-features" class="control-label">Features</label><br/>
			@foreach ($feature_ids as $feature_id => $feature)
				 <label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, isset($input->features)?$input->features:array()), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature}}</label>
			@endforeach
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-3">
				<label for="property-photo_option" class="control-label">Hot Properties Option</label>
				{!! Form::select('hot_option', $hot_option_ids, $input->hot_option, ['id'=>'property-hot_option', 'class'=>'form-control']); !!}
			</div>
		</div>

		@if(!empty($template_type) && in_array($template_type, array('frontend', 'trash')))
			<hr>

			<div class="form-group">
				<div class="col-sm-3">
					<label for="property-photo_option" class="control-label">Photo Option</label>
					{!! Form::select('photo_option', $photo_option_ids, $input->photo_option, ['id'=>'property-photo_option', 'class'=>'form-control']); !!}
				</div>
				<div class="col-sm-3">
					<label for="property-web" class="control-label">Web Option</label>
					{!! Form::select('web_option', $web_option_ids, $input->web_option, ['id'=>'property-web_option', 'class'=>'form-control']); !!}
				</div>
			</div>
		@endif

		<!-- Add Submit Button -->
		<div class="form-group">
			<div class="col-sm-6">
				<button type="submit" name="action" value="search" class="btn btn-default">
					<i class="fa fa-btn fa-search"></i>Search </button>
			</div>
		</div>
	</form>