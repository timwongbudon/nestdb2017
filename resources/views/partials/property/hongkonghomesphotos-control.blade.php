<script type="text/javascript">
$(document).ready(function(){
	$('#hongkonghomesphotos button').click(function(){
		var property_id = $("input[name=property_id]").val();
		var property_url = $("input[name=property_url]").val();
		if (property_url == '') {
			alert('Please input Proway URL');
			$("input[name=property_url]").focus();
			return false;
		}
		$.get("{{ url('/property/connect-hongkonghomesphotos') }}", { id: property_id, url: property_url }, function(data) {
	        console.info(data);
	        var result = $('#hongkonghomesphotos_result');
	        jQuery.each( data, function( i, photo ) {
				result.append("<div><img src='"+photo.url+"' width='200' style='margin-bottom: 10px;' /></div>");
			});
	    });
		return false;
	});
});
</script>