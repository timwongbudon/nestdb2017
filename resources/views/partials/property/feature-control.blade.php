<script type="text/javascript">
$(document).ready(function(){
    carpark_checkbox = '#property-features-5';
    carpark_text = '#property-car_park';
    // console.info( $(carpark_checkbox).length );

    $(carpark_checkbox).change(function(){
        if ($(carpark_checkbox+':checked').length) {
            if ($(carpark_text).val() == '' || $(carpark_text).val() == '0') {
                $(carpark_text).val('1');
            }
        } else {
            $(carpark_text).val(0);
        }
    });
    $(carpark_text).change(function(){
        if ($(carpark_text).val() == '' || $(carpark_text).val() == '0') {
            $(carpark_checkbox).removeAttr('checked');
        } else {
            $(carpark_checkbox).prop('checked', 'checked');
        }
    });
});
</script>