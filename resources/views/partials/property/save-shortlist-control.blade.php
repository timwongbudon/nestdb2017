<script type="text/javascript">
var client_input_field = "client-name";
var client_input_value = "";
var options = {
    url: function(phrase) {return "{{ url('/shortlists/api/index?keywords=') }}" + phrase;}, 
    getValue: function(element) {
    	if (element.id!='') {
            return element.firstname + ' ' + element.lastname + '-' + element.created;
        } else {
			return '';
		}
    },
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id!='') {
                return value + ' <i style="color: green">' + item.tel +'</i>';
            } else {
                return item.name;
            }
        }
    },
    list: {
        maxNumberOfElements: 62,
        onShowListEvent: function() {
            client_input_value = $("#"+client_input_field).val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#"+client_input_field).getSelectedItemData().action;
            switch(action) {
                case "create-item":
                    $("#property-create-client-modal").modal();
                    $("#client-firstname").focus();
                    break;
            	case "manage-item":
                    var win = window.open('{{ url('/clients/') }}', '_client');
                    win.focus();
                    break;
                default:
                    var content;
                    content = $("#"+client_input_field).getSelectedItemData();
					new_update_checkbox_count(content.id);
                    $("#property-building-modal").modal();
                    break;
            }
        },
    },
    highlightPhrase: false
};
$("#"+client_input_field).easyAutocomplete(options);

function new_update_checkbox_count(shortlist_id) {
	var form_table = '.property-table';
	property_ids = [];
   	$(form_table + ' input[type=checkbox]:checked').each(function(index){
		property_ids.push($(this).val());
    });
	if (property_ids.length > 1) {
		new_update_shortlist(shortlist_id, property_ids);
	} else if (property_ids.length == 1) {
		new_update_shortlist(shortlist_id, property_ids);
	} else {
        var update_shortlist_warning = 'Please choose properties to save.';
		alert(update_shortlist_warning);
        $("#property-shortlist-message").html(update_shortlist_warning + ' And press the button to save again. ').show();
        var alink = $("<a>").attr("href","javascript:new_update_checkbox_count('"+shortlist_id+"');").html("Save to " + $("#client-name").val());
        $("#property-shortlist-action").html(alink).show();
	}
}

function new_update_shortlist(shortlist_id, property_ids) {
	$.get("{{ url('/shortlist/update/') }}/"+shortlist_id, {property_ids: property_ids}, function(data) {
		// console.info(data.error);
		var form_table = '.property-table';
		if (!data.error) {
			$(form_table+' input[type=checkbox]').prop('checked', '');
            alert('Shortlist is saved.');
            var alink = $("<a>").attr("href","{{ url('shortlist/show/') }}" + "/" + shortlist_id).attr('target', '_blank').html("Go to " + $("#client-name").val());
            $("#client-name").val('');
            $("#property-shortlist-message").html('').show();
            $("#property-shortlist-action").html(alink).show();
		} 
	});
}

/**
 * create client via api
 */
$(document).ready(function(){
    $("#property-create-client-modal button").click(function(){
        if ($("#client-firstname").val() != '') {
            var form_data = $("#property-create-client-modal form").serialize();
            $.post("{{ url('client/api/store') }}", form_data, function(data) {
                if(!data.error) {
                    $("#property-create-client-modal").modal('hide');
                    var client_name = $("#client-firstname").val() + ' ' + $("#client-lastname").val();
                    $("#client-firstname").val('');
                    $("#client-lastname").val('');
                    $("#client-name").val(client_name);
                    var alink = $("<a>").attr("href","javascript:new_update_checkbox_count('"+data.shortlist_id+"');").html("Save to " + $("#client-name").val() + " (new)");
                    $("#property-shortlist-action").html(alink).show();
                 }
            });
            return false;
        }
    });
});
</script>