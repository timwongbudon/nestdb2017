<!-- property shortlist modal start -->
<div id="property-create-client-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                   Create New Client
                </h4>
            </div>
            <div class="modal-body row">

                <form method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="client-firstname" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="firstname" id="client-firstname" class="form-control" value="{{ old('firstname') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-lastname" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="lastname" id="client-lastname" class="form-control" value="{{ old('lastname') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-tel" class="col-sm-3 control-label">Tel</label>
                            <div class="col-sm-6">
                                <input type="text" name="tel" id="client-tel" class="form-control" value="{{ old('tel') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" name="email" id="client-email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-workplace" class="col-sm-3 control-label">Workplace</label>
                            <div class="col-sm-6">
                                <input type="text" name="workplace" id="client-workplace" class="form-control" value="{{ old('workplace') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-title" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="title" id="client-title" class="form-control" value="{{ old('title') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-budget_min" class="col-sm-3 control-label">Budget (Min)</label>
                            <div class="col-sm-6">
                                <input type="number" name="budget_min" id="client-budget_min" class="form-control" value="{{ old('budget_min') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-budget_max" class="col-sm-3 control-label">Budget (Max)</label>
                            <div class="col-sm-6">
                                <input type="number" name="budget_max" id="client-budget_max" class="form-control" value="{{ old('budget_max') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client-tempature" class="col-sm-3 control-label">Tempature</label>
                            <div class="col-sm-6">
                                 <label for="client-tempature-1" class="control-label">{!! Form::radio('tempature', 'Cold', !empty($client->tempature), ['id'=>'client-tempature-1']) !!} Cold</label>
                                 <label for="client-tempature-2" class="control-label">{!! Form::radio('tempature', 'Warm', !empty($client->tempature), ['id'=>'client-tempature-2']) !!} Warm</label>
                                 <label for="client-tempature-3" class="control-label">{!! Form::radio('tempature', 'Hot', !empty($client->tempature), ['id'=>'client-tempature-3']) !!} Hot</label>
                            </div>
                        </div>

                        <hr/>
    
                        <!-- Add Submit Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Create client account and the first shortlist</button>
                            </div>
                        </div>
                </form>

            </div>
        </div>
    </div>
</div> 
<!-- property shortlist modal end -->
