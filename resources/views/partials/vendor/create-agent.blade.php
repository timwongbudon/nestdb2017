<!-- create agent form -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>New Agent</h4>
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Property Form -->
                    <form action="{{ url('vendor/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

						<input type="hidden" name="type_id" value="4" />
						<input type="hidden" name="company" id="vendor-company" class="form-control" value="{{ $parent->company }}">
						<input type="hidden" name="parent_id" id="vendor-parent_id" class="form-control" value="{{ $parent->id }}">

						<div class="vendor-add-rep-wrapper">
							<div class="col-sm-6">
								<input type="text" placeholder="First Name" name="firstname" id="vendor-firstname" class="form-control" value="{{ old('firstname') }}">
							</div>
							<div class="col-sm-6">
								<input type="text" placeholder="Last Name" name="lastname" id="vendor-lastname" class="form-control" value="{{ old('lastname') }}">
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="vendor-add-rep-wrapper">
							<div class="col-sm-6">
								<input type="text" placeholder="Phone number" name="tel" id="vendor-tel" class="form-control" value="{{ old('tel') }}">
							</div>
							<div class="col-sm-6">
								<input type="text" placeholder="Email" name="email" id="vendor-email" class="form-control" value="{{ old('email') }}">
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="vendor-add-rep-wrapper">
							<div class="col-xs-12">
								{!! Form::textarea('comments', '', ['class'=>'form-control', 'placeholder' => 'Comments', 'rows' => '4']) !!}
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="vendor-add-rep-wrapper">
							<div class="col-xs-12">
								<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Submit </button>
							</div>
							<div class="clearfix"></div>
						</div>
                        
                        
                    </form>
                </div>