<script type="text/javascript">
    $(document).ready(function(){
        @if (isset($vendor) && $vendor->type_id)
        _control_vendor_owner_info({{ $vendor->type_id }});
        @else
        _control_vendor_owner_info($('input[name=type_id]:checked').val());
        @endif
        $('input[name=type_id]').change(function(){
            _control_vendor_owner_info($(this).val());
        });
    });
    function _control_vendor_owner_info(type_id) {
        if (type_id==4) { // agent
            _set_company_label('Agency Name');
            _set_parent_label('Agency Name (Parent)');
            $('#vendor-owner-info-section').hide();
            $('#vendor-parent-info-section').show();
            $('#vendor-fullname-info-section').show();
            $('#vendor-company-info-section').hide();
            $('#vendor-second-contact-info-section').hide();
        } else if (type_id==3) { // agency
            _set_company_label('Agency Name');
            _set_parent_label('Agency Name (Parent)');
            $('#vendor-owner-info-section').hide();
            $('#vendor-parent-info-section').hide();
            $('#vendor-fullname-info-section').hide();
            $('#vendor-company-info-section').show();
            $('#vendor-second-contact-info-section').hide();
        } else if (type_id == 2) {
            _set_company_label('Company Name');
            _set_parent_label('Owner Name (Parent)');
            $('#vendor-owner-info-section').hide();
            $('#vendor-parent-info-section').show();
            $('#vendor-fullname-info-section').show();
            $('#vendor-company-info-section').hide();
            $('#vendor-second-contact-info-section').hide();
        } else {
            _set_company_label('Company Name');
            _set_parent_label('Owner Name (Parent)');
            $('#vendor-owner-info-section').show();
            $('#vendor-parent-info-section').hide();
            $('#vendor-fullname-info-section').show();
            $('#vendor-company-info-section').show();
            $('#vendor-second-contact-info-section').show();
        }
    }
    function _set_company_label(str) {
        $('label[for=vendor-company]').html(str);
    }
    function _set_parent_label(str) {
        $('label[for=vendor-parent]').html(str);
    }
</script>