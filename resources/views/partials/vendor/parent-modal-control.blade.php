<script type="text/javascript">
var input_field = "vendor-parent";
var input_value = "";
var modal_id = "vendor-parent-modal";
var options = {
    url: function(phrase) {
        parent_type = get_parent_type($('input[name=type_id]:checked').val());
        return "{{ url('/vendors/api') }}" + '/' + parent_type +'/index?keywords=' + phrase;
    }, 
    getValue: function(element) {
        if (element.id != '') {
			if (element.firstname.trim() != '' && element.lastname.trim() != '' && element.company.trim() != ''){
				return element.firstname.trim() + ' ' + element.lastname.trim() + ', ' + element.company.trim();
			}else if (element.lastname.trim() != '' && element.company.trim() != ''){
				return element.lastname.trim() + ', ' + element.company.trim();
			}else{
				return element.company.trim();
			}
		}else{
            return element.name;
		}
	},
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id != '') {
				if (item.firstname.trim() != '' && item.lastname.trim() != '' && item.company.trim() != ''){
					return item.firstname.trim() + ' ' + item.lastname.trim() + ', ' + item.company.trim();
				}else if (item.lastname.trim() != '' && item.company.trim() != ''){
					return item.lastname.trim() + ', ' + item.company.trim();
				}else{
					return item.company.trim();
				}
            }else{
                return item.name;
            }
        }
    },
    list: {
        maxNumberOfElements: 12,
        onShowListEvent: function() {
            input_value = $("#"+input_field).val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#"+input_field).getSelectedItemData().action;
            switch(action) {
                case "manage-item":
                    $("#"+input_field).val(input_value);
                    var win = window.open('{{ url('/vendors/') }}', '_manage_vendor');
                    win.focus(); 
                    break;
                default:
                    var content;
                    content = $("#"+input_field).getSelectedItemData();
                    set_vendor_parent(content);
                    break;
            }
        }
    },
    highlightPhrase: false
};
$("#"+input_field).easyAutocomplete(options);

$(document).ready(function(){
    $("#"+input_field+"-display button").click(function(event){
        event.preventDefault();
        unset_vendor_parent();
    });
    set_vendor_parent_display();
});

function set_vendor_parent(content) {
    $('#vendor-parent_id').val(content.id);
	if (content.firstname.trim() != '' && content.lastname.trim() != '' && content.company.trim() != ''){
		$('#vendor-company').val(content.firstname.trim() + ' ' + content.lastname.trim() + ', ' + content.company.trim());
	}else if (content.lastname.trim() != '' && content.company.trim() != ''){
		$('#vendor-company').val(content.lastname.trim() + ', ' + content.company.trim());
	}else{
		$('#vendor-company').val(content.company.trim());
	}
    set_vendor_parent_display();
}

function set_vendor_parent_display() {
    var parent_id = $("#"+input_field+"_id").val();
    if (parent_id!='' && parent_id!=0) {
        $("#"+input_field+"-display").show()
            .find('a').text($("#"+input_field).hide().val())
            .attr('target', '_vendor')
            .attr('href', get_vendor_parent_url(parent_id));
    }
}

function unset_vendor_parent() {
    $("#"+input_field+"_id").val(''); 
    $("#"+input_field).val('').show().focus();
    $("#"+input_field+"-display").hide().find('a').text(''); 
}

function get_vendor_parent_url(vendor_id) {
    return "{{ url('/vendor/show/') }}" + '/' + vendor_id; 
}

function get_parent_type(type_id) {
    switch (type_id) {
        case '4':
            type = 'agency';
        break;
        case '2':
        default:
            type = 'owner';
        break;
    }
    return type;
}
</script>