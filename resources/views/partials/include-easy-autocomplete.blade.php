<!-- JS file -->
<script src="{{ url('/js/easyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js')}}"></script> 

<!-- CSS file -->
<link rel="stylesheet" href="{{ url('/js/easyAutocomplete-1.3.5/easy-autocomplete.min.css') }}"> 

<!-- Additional CSS Themes file - not required-->
<!-- <link rel="stylesheet" href="{{ url('/js/easyAutocomplete-1.3.5/easy-autocomplete.themes.min.css') }}">  -->
