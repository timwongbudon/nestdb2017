@if(Request::session()->has('shortlist_id'))
<?php
    $shortlist_id = Request::session()->get('shortlist_id');
    $shortlist = \App\ClientShortlist::with('client')->find($shortlist_id);
?>
        <div id="shortlist-property-section">
			<div class="row">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Working on {{ $shortlist['client']->name() }}'s shortlist [{{$shortlist->created_at()}}]
						<a style="margin-left: 10px; color: white" href="{{ url('/shortlist/show/'. $shortlist->id) }}">[view list]</a>
						<a style="margin-left: 10px; color: white" href="{{ url('/shortlist/manage-end') }}">[cancel]</a>
					</div>
					<div class="panel-body" style="display: none">
						<table class="table table-striped shortlist-table">
							<thead>
								<th style="width: 10%">ID</th>
								<th>Client</th>
								<th>Stage</th>
								<th style="width: 50%">Preference Description</th>
								<th style="width: 15%">Created</th>
							</thead>
							<tbody>
								<tr>
									<td class="table-text"><div>{{ $shortlist->id }}</div></td>
									<td class="table-text"><div>{{ $shortlist['client']->name() }}</div></td>
									<td class="table-text"><div>2</div></td>
									<td class="table-text"><div>{{ $shortlist->preference }}</div></td>
									<td class="table-text"><div>{{ $shortlist->created_at() }}</div></td>
								</tr>
							</tbody>
						</table>

<hr/>

@if($shortlist['options']->count()!=0)
<div id="selected-properties">
						<strong>Selected properties</strong>
						<table class="table table-striped shortlist-table">
							<thead>
								<th style="width: 10%">ID</th>
								<th>Name</th>
								<th>Type</th>
								<th>Unit</th>
								<th>Price (HK$)</th>
							</thead>
							<tbody>
@foreach($shortlist['options'] as $option)
	@if(isset($option['property']))
								<tr>
									<td class="table-text"><div>{{ $option['property']->id }}</div></td>
									<td class="table-text"><div>{{ $option['property']->name }}</div></td>
									<td class="table-text"><div>{{ $option['property']->type() }}</div></td>
									<td class="table-text"><div>{{ $option['property']->unit }}</div></td>
									<td class="table-text"><div>{{ $option['property']->price() }}</div></td>
								</tr>
	@endif
@endforeach
							</tbody>
						</table>
</div>
					</div>
					<div class="panel-footer" style="display: none">
						<div class="text-right">
							<button class="btn btn-primary">
								Print
							</button>
						</div>
					</div>
@endif
				</div>
			</div>
        </div>
@endif