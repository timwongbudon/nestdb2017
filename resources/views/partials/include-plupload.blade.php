{{-- plupload form --}}

<div id="{{ $id }}" class="form-group">
    <label class="col-sm-3 control-label">{{ $label }}</label>
    <div class="col-sm-8" style="padding-top: 7px">
        <div id="{{ $id }}_files"></div>
        <div id="{{ $id }}_filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
        <div id="{{ $id }}_container">
            <a id="{{ $id }}_pickfiles" href="javascript:;">[Select files]</a>
            <a id="{{ $id }}_uploadfiles" href="javascript:;">[Upload files]</a>
            @if (preg_match("/photo/", $id))<a id="{{ $id }}_reorderfiles" target="_reorder_files" href="{{ url('property/media/edit-reorder/' . $vars['id'] . '/' . $vars['group'] . '/' . $vars['type']) }}">[Re-order files]</a><a id="{{ $id }}_removefiles" target="_remove_files" href="{{ url('property/media/edit-remove/' . $vars['id'] . '/' . $vars['group'] . '/' . $vars['type']) }}">[Remove files]</a>@endif
        </div>
        <br />
        <pre id="{{ $id }}_console"></pre>
    </div>
</div>