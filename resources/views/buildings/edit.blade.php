@extends('layouts.app')

@section('content')
<?php 
if (!isset($building['features'])) {
    $building['features'] = array();
}
?>
	<div class="nest-new">
		<div class="row">
			<div class="nest-property-edit-wrapper">			
				<form action="{{ url('building/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="col-sm-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Edit Building (ID {{ $building->id }})</h4>
							</div>

							<div class="panel-body">
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Building Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="name" class="form-control" value="{{ old('name', $building->name) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Display Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="display_name" id="property-display_name" class="form-control" value="{{ old('name', $building->display_name) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Address</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1', $building->address1) }}" >
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">District</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('district_id', $district_ids, $building->district_id, ['id'=>'building-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Country</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('country_code', $country_ids, $building->country_code, ['id'=>'property-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body">								
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Management Office</div>
									</div>
									<div class="col-xs-6">
										<input type="text" name="tel" id="property-tel" placeholder="Phone number" class="form-control" value="{{ old('tel', $building->tel) }}" >
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Year Built</div>
									</div>
									<div class="col-xs-6">
										<input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built', $building->year_built) }}" min="0">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Year Last Renovated</div>
									</div>
									<div class="col-xs-6">
										<input type="number" name="year_renov" id="property-year_renov" class="form-control" value="{{ old('year_renov', $building->year_renov) }}" min="0">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Latitude</div>
									</div>
									<div class="col-xs-6">
										<input type="text" name="lat" id="property-lat" class="form-control" value="{{ old('lat', $building->lat) }}" min="0">
										@if(!is_numeric($building->lat))
										<span style="color: red;">{{$building->lat}}</span>
										@endif
									</div>
									<div class="clearfix"></div>
								</div>							
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Longitude</div>
									</div>
									<div class="col-xs-6">
										<input type="text" name="lng" id="property-lng" class="form-control" value="{{ old('lng', $building->lng) }}" min="0">
										@if(!is_numeric($building->lng))
										<span style="color: red;">{{$building->lng}}</span>
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row" style="padding-top:10px;">
									<div class="col-xs-12">
										 You can get Longitude/Latitude by entering the building address here: <a href="https://www.latlong.net/" target="_blank">www.latlong.net</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default">
							<div class="panel-body">		
								<div class="nest-property-edit-row">
									<div class="col-xs-12">
										<div class="nest-property-edit-label-features"><u>Building Facilities</u></div>
										<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($facilitieslist as $feature_id => $feature)
											@if ($feature_id != 7)
												<div class="col-xs-6">
											@else
												<div class="col-xs-12">
											@endif
												<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, in_array($feature_id, $facilities), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature }}
												</label>
											</div>
										@endforeach
									</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body">
								{{ Form::hidden('previous_url', $previous_url) }}
								{{ Form::hidden('id', $building->id) }}
								<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
							</div>
						</div>
					</div>
				</form>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Comments</h4>
						</div>
						
						<div class="panel-body">
							<div class="nest-new-comments-wrapper">
								<textarea id="newcommentbuilding" class="form-control" rows="3"></textarea>
								<button class="nest-button nest-right-button btn btn-default" onClick="addCommentBuilding();">Add Comment</button>
								<div class="clearfix"></div>
							</div>
							<div class="nest-comments" id="existingCommentsBuilding"><img src="{{ url('images/tools/loader.gif') }}" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
		@if ((!empty($properties1) && count($properties1) > 0) || (!empty($properties2) && count($properties2) > 0))
			<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
				@foreach ($properties1 as $index => $p)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">{{ $p->name }}</a><br />
							<b>ID:</b> {{ $p->id }}
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6">
							<b>Address:</b> {{ $p->address1 }}<br />
							<b>Floor:</b> {{ $p->floornr }} <b>Unit:</b> 
							@if (trim($p->unit) == '')
								-
							@else
								{{ $p->unit }}
							@endif
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Available Date:</b> {{ $p->available_date() }}<br />
							<b>Asking Price:</b> {!! $p->price_db_searchresults_new() !!}
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('property/edit/'.$p->id) }}">
								<i class="fa fa-btn fa-pencil-square-o"></i>Edit
							</a>
							<a class="nest-button nest-right-button btn btn-primary" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">
								<i class="fa fa-btn fa-info-circle"></i>View
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
				@foreach ($properties2 as $index => $p)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">{{ $p->name }}</a><br />
							<b>ID:</b> {{ $p->id }}
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6">
							<b>Address:</b> {{ $p->address1 }}<br />
							<b>Floor:</b> - <b>Unit:</b> 
							@if (trim($p->unit) == '')
								-
							@else
								{{ $p->unit }}
							@endif
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Available Date:</b> {{ $p->available_date() }}<br />
							<b>Asking Price:</b> {!! $p->price_db_searchresults_new() !!}
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('property/edit/'.$p->id) }}">
								<i class="fa fa-btn fa-pencil-square-o"></i>Edit
							</a>
							<a class="nest-button nest-right-button btn btn-primary" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">
								<i class="fa fa-btn fa-info-circle"></i>View
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
				
				<div style="text-align:right;padding:2.5px 7.5px 7.5px;">
					<div class="nest-building-show-wrapper" style="display:inline-block;padding:7.5px 15px;">
						<form action="{{ url('building/updatenames') }}" method="POST">
							{{ csrf_field() }}
							{{ Form::hidden('id', $building->id) }}
									<button type="submit" class="nest-button btn btn-default"><i class="fa fa-btn fa-pencil"></i> Update all Property Names</button>
								
						</form>
					</div>
					<div class="nest-building-show-wrapper" style="display:inline-block;padding:7.5px 15px;">
						<form action="{{ url('building/updateaddress') }}" method="POST">
							{{ csrf_field() }}
							{{ Form::hidden('id', $building->id) }}
									<button type="submit" class="nest-button btn btn-default"><i class="fa fa-btn fa-pencil"></i> Update all Property Addresses</button>
								
						</form>
					</div>
					<div class="nest-building-show-wrapper" style="display:inline-block;padding:7.5px 15px;">
						<form action="{{ url('building/updatefacilities') }}" method="POST">
							{{ csrf_field() }}
							{{ Form::hidden('id', $building->id) }}
									<button type="submit" class="nest-button btn btn-default"><i class="fa fa-btn fa-pencil"></i> Update all Property Facilities</button>
								
						</form>
					</div>
					<div class="nest-building-show-wrapper" style="display:inline-block;padding:7.5px 15px;">
						<a class="nest-button btn btn-primary" href="{{ url('/property/createbob/'.$building->id) }}" style="font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight:normal;">
							<i class="fa fa-btn fa-plus"></i>New Property
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>		
		@else
			<div class="row">
				<div class="nest-property-edit-wrapper" style="padding-top:0px;">
					<div class="col-xs-12">
						<div class="nest-buildings-search-panel panel panel-default">
							<div class="panel-body" style="padding-bottom:15px;">
								<div class="text-center">This building has no properties attached</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
				<div class="nest-building-show-wrapper" style="display:block;padding:10px 15px;">
					<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/property/createbob/'.$building->id) }}" style="margin-top:0px;">
						<i class="fa fa-btn fa-plus"></i>New Property
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		@endif
		<div class="clearfix"></div>
		
		
		<div class="row">
			<div class="nest-property-edit-wrapper" style="padding-top:0px;">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;">
							@if (empty($properties) || count($properties) == 0)
						
								<form action="{{ url('building/remove') }}" method="POST" class="form-horizontal" onsubmit="return confirm('Do you really want to delete this building?');">
									{{ csrf_field() }}
									{{ Form::hidden('id', $building->id) }}
									<div class="nest-delete-row">
										<div class="col-xs-12">
											<button type="submit" class="btn btn-danger"><i class="fa fa-btn fa-trash"></i> Delete Building</button>
										</div>
									</div>
								</form>
							@else
								NOTE: To be able to remove this building, all properties need to be either removed or need to be allocated to another building!
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
	<script>			
		function loadCommentsBuilding(){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'GET',
				url: '{{ url("/buildings/comments/".$building->id) }}',
				cache: false,
				success: function (data) {
					jQuery('#existingCommentsBuilding').html(data);
				},
				error: function (data) {
					jQuery('#existingCommentsBuilding').html(data);
					jQuery.ajax({
						type: 'GET',
						url: '{{ url("/buildings/comments/".$building->id) }}',
						cache: false,
						success: function (data) {
							jQuery('#existingCommentsBuilding').html(data);
						},
						error: function (data) {
							jQuery.ajax({
								type: 'GET',
								url: '{{ url("/buildings/comments/".$building->id) }}',
								cache: false,
								success: function (data) {
									jQuery('#existingCommentsBuilding').html(data);
								},
								error: function (data) {
									jQuery('#existingCommentsBuilding').html('An error occurred '+data);
								}
							});
						}
					});
				}
			});
		}
		
		function addCommentBuilding(){
			var comment = jQuery('#newcommentbuilding').val().trim();
			if (comment.length > 0){
				jQuery.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				jQuery.ajax({
					type: 'POST',
					url: '{{ url("/buildings/commentadd/".$building->id) }}',
					data: {comment: comment},
					dataType: 'text',
					cache: false,
					async: false,
					success: function (data) {
						jQuery('#newcommentbuilding').val('');
						loadCommentsBuilding();
					},
					error: function (data) {
						console.log(data);
						alert('Something went wrong, please try again!');
					}
				});
			}else{
				alert('Please enter a comment');
			}
		}
		
		$('#newcommentbuilding').keydown(function( event ) {
			if ( event.keyCode == 13 ) {
				event.preventDefault();
				addCommentBuilding();
				loadCommentsBuilding();
			}				
		});
		
		function removeCommentBuilding(t){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/buildings/commentremove/".$building->id) }}',
				data: {t: t},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					loadCommentsBuilding();
				},
				error: function (data) {
					alert('Something went wrong, please try again!');
				}
			});
		}
		
		jQuery( document ).ready(function(){
			loadCommentsBuilding();
		});
		
		jQuery('form').on('focus', 'input[type=number]', function (e) {
			jQuery(this).on('mousewheel.disableScroll', function (e) {
				e.preventDefault();
			});
		});
		jQuery('form').on('blur', 'input[type=number]', function (e) {
			jQuery(this).off('mousewheel.disableScroll');
		});
	</script>
		
		
@endsection

@section('footer-script')
@endsection