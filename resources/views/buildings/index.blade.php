@extends('layouts.app')

@section('content')
	
            <!-- Building Search -->
            <div class="nest-buildings-search-panel panel panel-default">
                <div class="panel-body">
                    <!-- New Property Form -->
                    <form action="{{ url('buildings') }}" method="GET" class="form-horizontal">
                        <div class="nest-buildings-search-wrapper row">
                            <div class="col-sm-4">
                                <input type="text" name="name" placeholder="Building name, address, etc." id="property-name" class="form-control" value="{{ old('name', $input->name) }}">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="id" placeholder="Building ID" id="property-id_exception" class="form-control" value="{{ old('id', $input->id) }}" >
                            </div>
                            <div class="col-sm-4">
                                {!! Form::select('district_id', $district_ids, $input->district_id, ['id'=>'property-district_id_exception', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
                            </div>
							<div class="clearfix"></div>
                        </div>
						<div class="nest-buildings-search-buttons-wrapper row">
							<div class="col-xs-6">
								<a class="nest-button btn btn-primary" href="{{ url('building/create') }}">
									<i class="fa fa-btn fa-plus"></i>Add Building
								</a><br><br>

								<a class="nest-button btn btn-primary" href="{{ url('buildings?latlong=true') }}">
								Buildings without Longitude/Latitude
								</a>
							</div>
                            <div class="col-xs-6">
                                <button type="submit" name="action" value="search" class="nest-button nest-right-button btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search </button>
                            </div>
						</div>
						

                        <!--<div class="form-group">
                            <div class="col-sm-4">
                                <label for="property-country_code" class="control-label">Country</label>
                                {!! Form::select('country_code', array('hk' => 'Hong Kong'), old('country_code', $input->country_code?$input->country_code:'hk'), ['id'=>'property-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
                            </div>
                            <div class="col-sm-4">
                                <label for="property-year_built" class="control-label">Year Built</label>
                                <input type="number" name="year_built" id="property-year_built_exception" class="form-control" value="{{ old('year_built', $input->year_built) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="property-address1" class="control-label">Address</label>
                                <input type="text" name="address1" id="property-address1_exception" class="form-control" value="{{ old('address1', $input->address1) }}" >
                            </div>
                        </div> -->
    
                    </form>
                </div>
            </div>

            <!-- Current Buildings -->
            @if (count($buildings) > 0)
					<div>
						<div class="nest-buildings-result-wrapper">
							@foreach ($buildings as $index => $building)
								<div class="row 
								@if ($index%2 == 1)
									nest-striped
								@endif
								">
									<div class="col-lg-4 col-md-4 col-sm-6">
										<b>Name:</b> <a href="{{ url('building/show/'.$building->id) }}">{{ $building->name }}</a><br />
										<b>ID:</b> {{ $building->id }}
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6">
										<b>Address:</b> {{ $building->address1 }}<br />
										<b>District:</b> {{ $building->district() }}
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12">
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/property/createbob/'.$building->id) }}">
											<i class="fa fa-btn fa-plus"></i>New Property
										</a>
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('building/edit/'.$building->id) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						</div>
                    </div>
					@if ($buildings->lastPage() > 1)
						<div class="panel-footer nest-buildings-pagination">
							<!-- <div style="margin: 10px 0;">Page {{ $buildings->currentPage() }} of {{ $buildings->lastPage() }} (Total: {{ $buildings->total() }} items)</div> -->
							{{ $buildings->links() }}
						</div>
					@endif
            @endif

		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection

@section('footer-script')
@include('partials.property.building-modal-control')
@endsection