@extends('layouts.app')

@section('content')
<?php 
if (!isset($building['features'])) {
    $building['features'] = array();
}
?>
	<div class="nest-new">
		<div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Edit Building with ID {{ $building->id }}</h4>
                </div>

                <div class="panel-body">					
					<div class="col-md-7">
						<h5>Details</h5>
						<div class="clearfix"></div>
							
						<div class="nest-building-show-wrapper">
							<!-- Display Validation Errors -->
							@include('common.errors')

							<!-- Edit Property Form -->
							<form action="{{ url('building/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
								{{ csrf_field() }}

								<div class="form-group">
									<label for="property-name" class="col-sm-3 control-label">ID</label>
									<div class="col-sm-6">
										<span>{{ $building->id }}</span>
									</div>
								</div>

								<div class="form-group">
									<label for="property-name" class="col-sm-3 control-label">Building Name</label>
									<div class="col-sm-6">
										<input type="text" name="name" id="property-name" class="form-control" value="{{ old('name', $building->name) }}">
									</div>
								</div>

								<div class="form-group">
									<label for="property-display_name" class="col-sm-3 control-label">Display Building Name <br/>(For printing card only)</label>
									<div class="col-sm-6">
										<input type="text" name="display_name" id="property-display_name" class="form-control" value="{{ old('name', $building->display_name) }}">
										<span style="color: green;">System best guess: <i>{{ $building->shorten_name() }}</i></span>
									</div>
								</div>

								<div class="form-group">
									<label for="property-address1" class="col-sm-3 control-label">Address1</label>
									<div class="col-sm-6">
										<input type="text" name="address1" id="property-address1" class="form-control" value="{{ old('address1', $building->address1) }}" >
									</div>
								</div>

								<div class="form-group">
									<label for="property-address2" class="col-sm-3 control-label">Address2</label>
									<div class="col-sm-6">
										<input type="text" name="address2" id="property-address2" class="form-control" value="{{ old('address2', $building->address2) }}" >
									</div>
								</div>

								<div class="form-group">
									<label for="property-district_id" class="col-sm-3 control-label">District</label>
									<div class="col-sm-6">
										{!! Form::select('district_id', $district_ids, $building->district_id, ['id'=>'building-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
									</div>
								</div>

								<div class="form-group">
									<label for="property-country_code" class="col-sm-3 control-label">Country</label>
									<div class="col-sm-6">
										{!! Form::select('country_code', $country_ids, $building->country_code, ['id'=>'property-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
									</div>
								</div>

								<div class="form-group">
									<label for="property-tel" class="col-sm-3 control-label">Mgmt Office Tel</label>
									<div class="col-sm-6">
										<input type="text" name="tel" id="property-tel" class="form-control" value="{{ old('tel', $building->tel) }}" >
									</div>
								</div>

								<div class="form-group">
									<label for="property-year_built" class="col-sm-3 control-label">Year Built</label>
									<div class="col-sm-6">
										<input type="number" name="year_built" id="property-year_built" class="form-control" value="{{ old('year_built', $building->year_built) }}" min="0">
									</div>
								</div>

								<div class="form-group">
									<label for="property-lat" class="col-sm-3 control-label">Lat</label>
									<div class="col-sm-6">
										<input type="text" name="lat" id="property-lat" class="form-control" value="{{ old('lat', $building->lat) }}" min="0">
										@if(!is_numeric($building->lat))
										<span style="color: red;">{{$building->lat}}</span>
										@endif
									</div>
								</div>

								<div class="form-group">
									<label for="property-lng" class="col-sm-3 control-label">Lng</label>
									<div class="col-sm-6">
										<input type="text" name="lng" id="property-lng" class="form-control" value="{{ old('lng', $building->lng) }}" min="0">
										@if(!is_numeric($building->lng))
										<span style="color: red;">{{$building->lng}}</span>
										@endif
									</div>
								</div>

								<!-- <hr/>

								<div class="form-group">
									<label for="property-features" class="col-sm-3 control-label">Features</label>
									<div class="col-sm-6">
									@foreach ($features as $feature_id => $feature)
										 <label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, $building->is_selected_feature($feature_id), ['id'=>'property-features-'.$feature_id]) !!} {{ $feature}}</label>
									@endforeach
									</div>
								</div> -->

								<hr/>
			
								<!-- Add Update Button -->
								<div class="form-group">
									{{ Form::hidden('previous_url', $previous_url) }}
									{{ Form::hidden('id', $building->id) }}
									<div class="col-sm-offset-3 col-sm-6">
										<button type="submit" class="btn btn-default">
											<i class="fa fa-btn fa-pencil"></i>Update </button>
										<!-- <button type="button" class="btn btn-default">
											<i class="fa fa-btn fa-ban"></i>Cancel </button> -->
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-5">
						<h5>Properties</h5>
						<div class="clearfix"></div>
						
						@if (!empty($properties))
							@foreach ($properties as $p)
								<div class="nest-building-show-wrapper">
									<div class="col-xs-2">
										{{ $p->id }}
									</div>
									<div class="col-xs-4">
										<a href="{{ url('/property/edit/'.$p->id) }}" target="_blank">{{ $p->name }}</a>
									</div>
									<div class="col-xs-6">
										{{ $p->address1 }}
										@if($p->unit)
											(Unit: {{ $p->unit }})
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
							
							<form action="{{ url('building/updatenames') }}" method="POST" class="form-horizontal">
								{{ csrf_field() }}
								{{ Form::hidden('id', $building->id) }}
								<br />
								<div class="nest-building-show-wrapper">
									<div class="col-sm-6">
										<button type="submit" class="btn btn-default"><i class="fa fa-btn fa-pencil"></i> Update all Property Names</button>
									</div>
								</div>
							</form>
							<div class="clearfix"></div>
							<form action="{{ url('building/updateaddress') }}" method="POST" class="form-horizontal">
								{{ csrf_field() }}
								{{ Form::hidden('id', $building->id) }}
								<div class="nest-building-show-wrapper" style="padding-top:10px;">
									<div class="col-sm-6">
										<button type="submit" class="btn btn-default"><i class="fa fa-btn fa-pencil"></i> Update all Property Addresses</button>
									</div>
								</div>
							</form>
							
							
						@endif
					</div>
                </div>
            </div>
			<div class="panel panel-default">
				<div class="panel-body" style="padding-bottom:15px;">
					@if (empty($properties) || count($properties) == 0)
				
						<form action="{{ url('building/remove') }}" method="POST" class="form-horizontal" onsubmit="return confirm('Do you really want to delete this building?');">
							{{ csrf_field() }}
							{{ Form::hidden('id', $building->id) }}
							<div class="nest-delete-row">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-danger"><i class="fa fa-btn fa-trash"></i> Delete Building</button>
								</div>
							</div>
						</form>
					@else
						NOTE: To be able to remove this building, all properties need to be either removed or need to be allocated to another building!
					@endif
				</div>
			</div>
        </div>
		
		
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
		
		
@endsection

@section('footer-script')
@endsection