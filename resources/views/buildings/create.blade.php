@extends('layouts.app')

@section('content')
<?php 
if (!isset($building['features'])) {
    $building['features'] = array();
}
?>
	<div class="nest-new">
		<div class="row">
			<div class="nest-property-edit-wrapper">				
				<form action="{{ url('building/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="col-sm-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>New Building</h4>
							</div>

							<div class="panel-body">
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Building Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="name" class="form-control" value="">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Display Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="display_name" id="property-display_name" class="form-control" value="">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Address</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="address1" id="property-address1" class="form-control" value="" >
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">District</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('district_id', $district_ids, 0, ['id'=>'building-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Country</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('country_code', $country_ids, 'hk', ['id'=>'property-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default">

							<div class="panel-body">								
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Management Office</div>
									</div>
									<div class="col-xs-6">
										<input type="text" name="tel" id="property-tel" placeholder="Phone number" class="form-control" value="" >
									</div>
									<div class="clearfix"></div>
								</div>							
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Year Built</div>
									</div>
									<div class="col-xs-6">
										<input type="number" name="year_built" id="property-year_built" class="form-control" value="" min="0">
									</div>
									<div class="clearfix"></div>
								</div>														
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Year Last Renovated</div>
									</div>
									<div class="col-xs-6">
										<input type="number" name="year_renov" id="property-year_renov" class="form-control" value="" min="0">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Latitude</div>
									</div>
									<div class="col-xs-6">
										<input type="text" name="lat" id="property-lat" class="form-control" value="" min="0">
									</div>
									<div class="clearfix"></div>
								</div>							
								<div class="nest-property-edit-row">
									<div class="col-xs-6">
										 <div class="nest-property-edit-label">Longitude</div>
									</div>
									<div class="col-xs-6">
										<input type="text" name="lng" id="property-lng" class="form-control" value="" min="0">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row" style="padding-top:10px;">
									<div class="col-xs-12">
										 You can get Longitude/Latitude by entering the building address here: <a href="https://www.latlong.net/" target="_blank">www.latlong.net</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default">
							<div class="panel-body">		
								<div class="nest-property-edit-row">
									<div class="col-xs-12">
										<div class="nest-property-edit-label-features"><u>Building Facilities</u></div>
										<div class="nest-property-edit-checkbox-wrapper">
										@foreach ($facilitieslist as $feature_id => $feature)
											@if ($feature_id != 7)
												<div class="col-xs-6">
											@else
												<div class="col-xs-12">
											@endif
												<label for="property-features-{{$feature_id}}" class="control-label">{!! Form::checkbox('features[]', $feature_id, false, ['id'=>'property-features-'.$feature_id]) !!} {{ $feature }}
												</label>
											</div>
										@endforeach
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body">
								<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Submit </button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>


		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection

@section('footer-script')
@endsection