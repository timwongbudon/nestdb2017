@extends('layouts.app')

@section('content')
	<div class="nest-new">
		<div class="row">
			<div class="nest-property-edit-wrapper">	
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>{{ $building->name }} (ID {{ $building->id }})</h4>
							</div>
						</div>
						
						<div class="panel-body">
							<div class="col-xs-12">
									
								<div class="nest-building-show-wrapper">
									<div class="col-xs-4">
										<b>Address</b>
									</div>
									<div class="col-xs-8">
										{{ $building->address1 }}
									</div>
									<div class="clearfix"></div>
									
									<div class="col-xs-4">
										<b>District</b>
									</div>
									<div class="col-xs-8">
										{{ $building->district() }}
									</div>
									<div class="clearfix"></div>
									
									<div class="col-xs-4">
										<b>Country</b>
									</div>
									<div class="col-xs-8">
										{{ $building->get_nice_country() }}
									</div>
									<div class="clearfix"></div>
									
									@if (trim($building->tel) != '')
										<div class="col-xs-4">
											<b>Management Office</b>
										</div>
										<div class="col-xs-8">
											{{ $building->tel }}
										</div>
										<div class="clearfix"></div>
									@endif
									<div class="col-xs-4">
										<b>Year Built</b>
									</div>
									<div class="col-xs-8">
										{{ $building->year_built }}
									</div>
									<div class="clearfix"></div>
									@if ($building->year_renov > 0)
										<div class="col-xs-4">
											<b>Year Last Renovated</b>
										</div>
										<div class="col-xs-8">
											{{ $building->year_renov }}
										</div>
										<div class="clearfix"></div>
									@endif
									<div class="col-xs-4">
										<b>Building Facilites</b>
									</div>
									<div class="col-xs-8">
										{{ $facilities }}
									</div>
									<div class="clearfix"></div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<a class="nest-button nest-right-button btn btn-default" href="{{ url('building/edit/'.$building->id) }}" style="border:0px;">
								<i class="fa fa-btn fa-pencil-square-o"></i>Edit Building
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Comments</h4>
						</div>						
						<div class="panel-body">
							<div class="nest-comments" id="existingCommentsBuilding"><img src="{{ url('images/tools/loader.gif') }}" /></div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>	
        </div>
		@if ((!empty($properties1) && count($properties1) > 0) || (!empty($properties2) && count($properties2) > 0))
			<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
				@foreach ($properties1 as $index => $p)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">{{ $p->name }}</a><br />
							<b>ID:</b> {{ $p->id }}
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6">
							<b>Address:</b> {{ $p->address1 }}<br />
							<b>Floor:</b> {{ $p->floornr }} <b>Unit:</b> 
							@if (trim($p->unit) == '')
								-
							@else
								{{ $p->unit }}
							@endif
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Available Date:</b> {{ $p->available_date() }}<br />
							<b>Asking Price:</b> {!! $p->price_db_searchresults_new() !!}
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('property/edit/'.$p->id) }}">
								<i class="fa fa-btn fa-pencil-square-o"></i>Edit
							</a>
							<a class="nest-button nest-right-button btn btn-primary" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">
								<i class="fa fa-btn fa-info-circle"></i>View
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
				@foreach ($properties2 as $index => $p)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">{{ $p->name }}</a><br />
							<b>ID:</b> {{ $p->id }}
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6">
							<b>Address:</b> {{ $p->address1 }}<br />
							<b>Floor:</b> - <b>Unit:</b> 
							@if (trim($p->unit) == '')
								-
							@else
								{{ $p->unit }}
							@endif
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Available Date:</b> {{ $p->available_date() }}<br />
							<b>Asking Price:</b> {!! $p->price_db_searchresults_new() !!}
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('property/edit/'.$p->id) }}">
								<i class="fa fa-btn fa-pencil-square-o"></i>Edit
							</a>
							<a class="nest-button nest-right-button btn btn-primary" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">
								<i class="fa fa-btn fa-info-circle"></i>View
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>		
		@else
			<div class="row">
				<div class="nest-property-edit-wrapper" style="padding-top:0px;">
					<div class="col-xs-12">
						<div class="nest-buildings-search-panel panel panel-default">
							<div class="panel-body" style="padding-bottom:15px;">
								<div class="text-center">This building has no properties attached</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
			<div class="nest-building-show-wrapper" style="display:block;padding:10px 15px;">
				<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/property/createbob/'.$building->id) }}" style="margin-top:0px;">
					<i class="fa fa-btn fa-plus"></i>New Property
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	
	<script>			
		function loadCommentsBuilding(){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'GET',
				url: '{{ url("/buildings/comments/".$building->id) }}',
				cache: false,
				success: function (data) {
					jQuery('#existingCommentsBuilding').html(data);
				},
				error: function (data) {
					jQuery('#existingCommentsBuilding').html(data);
					jQuery.ajax({
						type: 'GET',
						url: '{{ url("/buildings/comments/".$building->id) }}',
						cache: false,
						success: function (data) {
							jQuery('#existingCommentsBuilding').html(data);
						},
						error: function (data) {
							jQuery.ajax({
								type: 'GET',
								url: '{{ url("/buildings/comments/".$building->id) }}',
								cache: false,
								success: function (data) {
									jQuery('#existingCommentsBuilding').html(data);
								},
								error: function (data) {
									jQuery('#existingCommentsBuilding').html('An error occurred '+data);
								}
							});
						}
					});
				}
			});
		}
		
		jQuery( document ).ready(function(){
			loadCommentsBuilding();
		});
		
	</script>
	
	
@endsection

@section('footer-script')
@endsection