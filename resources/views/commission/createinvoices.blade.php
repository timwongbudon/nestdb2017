@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="" method="POST" class="form-horizontal addinvoice" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Basic Information</h4>
						</div>
						
						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')
							
							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Invoice Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="invoicedate" id="invoicedate" class="form-control" value="{{ $basicfields['invoicedate'] }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($basicfields['invtype'] == 1)
											Tenant Name
										@else
											Purchaser Name
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="buyername" id="buyername" class="form-control">{{ $basicfields['buyername'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							@if ($basicfields['invtype'] == 1)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">
											Tenant Work Title
										</div>
									</div>
									<div class="col-xs-8">
										<textarea name="work_title" id="work_title" class="form-control">{{ $basicfields['work_title'] }}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">
											Tenant Work Place
										</div>
									</div>
									<div class="col-xs-8">
										<textarea name="work_place" id="work_place" class="form-control">{{ $basicfields['work_place'] }}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($basicfields['invtype'] == 1)
											Tenant Address
										@else
											Purchaser Address
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="buyeraddress" id="buyeraddress" class="form-control">{{ $basicfields['buyeraddress'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($basicfields['invtype'] == 1)
											Landlord Name
										@else
											Vendor Name
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="sellername" id="sellername" class="form-control">{{ $basicfields['sellername'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($basicfields['invtype'] == 1)
											Landlord Address
										@else
											Vendor Address
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="selleraddress" id="selleraddress" class="form-control">{{ $basicfields['selleraddress'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Property Address</div>
								</div>
								<div class="col-xs-8">
									<textarea name="propertyaddress" id="propertyaddress" class="form-control">{{ $basicfields['propertyaddress'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($basicfields['invtype'] == 1)
											Rental
										@else
											Purchase Price
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="purchasetext" id="purchasetext" class="form-control"
										@if ($basicfields['invtype'] == 1)
											placeholder="e.g. HK$35,000.00 per month inclusive of Management Fees, Government Rates, Government Rent and Property Tax"
										@else
											placeholder="e.g. HK$10,000,000"
										@endif
									>{{ $basicfields['purchasetext'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($basicfields['invtype'] == 1)
											Term
										@else
											Completion Date
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="terms" id="terms" class="form-control" 
										@if ($basicfields['invtype'] == 1)
											placeholder="e.g. 8th August 2019 – 7th August 2021 (both days incl.) [Two Year Lease, 12+2 month Break Clause]"
										@else
											placeholder="e.g. 16th September 2019"
										@endif
									>{{ $basicfields['terms'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Start Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="start_date" id="start_date" class="form-control" value="">
								</div>
								<div class="clearfix"></div>
							</div>
							

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										Lease Term
									</div>
								</div>
								<div class="col-xs-8">
									<select name="lease_term" class="lease_term">
										<option value="">Please Select</option>
										@foreach($lease_term as $key => $lt)
											<option value="{{$key}}">{{$lt}}</option>											
										@endforeach
										
									</select>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row month_break_clause" style="display: none;">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										Month Break Clause
									</div>
								</div>
								<div class="col-xs-8">
									<select name="month_break_clause">
										<option value="">Please Select</option>
										@foreach($month_break_clause as $key => $mbc)
											<option value="{{$key}}">{{$mbc}}</option>											
										@endforeach										
									</select>
								</div>
								<div class="clearfix"></div>
							</div>


						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							@if ($basicfields['invtype'] == 1)
								<h4>Tenant Invoice Information</h4>
							@else
								<h4>Purchaser Invoice Information</h4>
							@endif
						</div>
						
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Fee Calculation Text</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="bfeecalculation" id="bfeecalculation" class="form-control" value="{{ $buyer['feecalculation'] }}" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Invoice Amount</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="bprice" id="bprice" class="form-control" value="{{ $buyer['price'] }}" step=".01">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Additional Costs Text (e.g. Stamp Duty)</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="baddcosttext" id="baddcosttext" class="form-control" placeholder="e.g. 50% of Government Stamp Duty Fee [HK$10,000 ÷ 2]" value="{{ $buyer['addcosttext'] }}" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Additional Costs Amount (e.g. Stamp Duty)</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="baddcost" id="baddcost" class="form-control" value="{{ $buyer['addcost'] }}" step=".01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Due Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="bduedate" id="bduedate" class="form-control" value="{{ $buyer['duedate'] }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							@if ($basicfields['invtype'] == 1)
								<h4>Landlord Invoice Information</h4>
							@else
								<h4>Vendor Invoice Information</h4>
							@endif
						</div>
						
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Fee Calculation Text</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="sfeecalculation" id="sfeecalculation" class="form-control" value="{{ $seller['feecalculation'] }}" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Invoice Amount</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="sprice" id="sprice" class="form-control" value="{{ $seller['price'] }}" step=".01">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Additional Costs Text (e.g. Stamp Duty)</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="saddcosttext" id="saddcosttext" class="form-control" placeholder="e.g. 50% of Government Stamp Duty Fee [HK$10,000 ÷ 2]" value="{{ $seller['addcosttext'] }}" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Additional Costs Amount (e.g. Stamp Duty)</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="saddcost" id="saddcost" class="form-control" value="{{ $seller['addcost'] }}" step=".01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Due Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="sduedate" id="sduedate" class="form-control" value="{{ $seller['duedate'] }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Submit for Approval</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="clearfix"></div>
			<br /><br /><br /><br /><br />
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});


	jQuery('.lease_term').on('change',function (e) {
		var val = 	jQuery(this).val();
		if(val >= 3){
			jQuery('.month_break_clause').show();
		} else {
			jQuery('.month_break_clause').hide();
		}
	});

	jQuery('.addinvoice').on('submit',function (e) {

	if(jQuery('.lease_term').val() == ""){
		e.preventDefault();
		alert("Please select Lease Term");
	} else if(jQuery('#start_date').val() == ""){
		e.preventDefault();
		alert("Please select Start Date");
	}

	});

	
		
</script>
@endsection
