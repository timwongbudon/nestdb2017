@extends('layouts.app')

@section('content')

	<div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
			<h2>Information Officer Commission</h2>
		</div>
	</div>
	
	
	<!-- Current Clients -->
	@if (count($commission) > 0)
		<div>
			<div class="nest-buildings-result-wrapper">

			<div class="row 
				
						nest-striped
				
					">
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Information Officer</b>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Client & Property</b>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Offer Signing Date</b>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Commission $(HKD)</b>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>Status</b>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>	Action</b>
						</div>
						<div class="clearfix"></div>
					</div>


				@foreach ($commission as $index => $inv)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-2 col-md-2 col-sm-6">
						{{ $inv->firstname }}  {{ $inv->lastname }}
							
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						{{$inv->client_firstname}} {{$inv->client_lastname}} - 
						<a href="{{url('property/edit/'.$inv->property_id)}}">{{ $inv->name }}</a>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						{{ $inv->commissiondate !=='0000-00-00 00:00:00' ? date('d.m.Y',strtotime($inv->commissiondate)) : "-" }}
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						{{ $inv->commission }}
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						@if ($inv->status == 'Approve')
							<span style="color: green; font-weight: bold">Approved</span><br>
							<span style="color: black; font-weight: normal">{{ $inv->updated_at !=='0000-00-00 00:00:00' ? date('Y-m-d',strtotime($inv->updated_at)) : "-" }}</span>
						@elseif ($inv->status == 'Decline')					
							<span style="color: red; font-weight:bold ">Declined</span>	<br>
							<span style="color: black; font-weight: normal">{{ $inv->updated_at !=='0000-00-00 00:00:00' ? date('Y-m-d',strtotime($inv->updated_at)) : "-" }}</span>
						@else
						<span style="color: orange; font-weight: bold">Pending</span>	
						@endif
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6">
						
							<form action="{{ url('informationofficer/commission/update/'.$inv->id) }}" method="POST">
							{{ csrf_field() }}
								<input type="hidden" name="id" value="{{$inv->id}}">
								<input type="submit" class="nest-button nest-right-button btn btn-primary" onclick="if (! confirm('Are you sure you want to approve this commission?')) return false" name="status" value="Approve">
							</form>

							<form action="{{ url('informationofficer/commission/update/'.$inv->id) }}" method="POST">
							{{ csrf_field() }}
								<input type="hidden" name="id" value="{{$inv->id}}">
								<input type="submit" class="nest-button nest-right-button btn btn-primary" onclick="if (! confirm('Are you sure you want to decline this commission?')) return false" name="status" value="Decline">
							</form>
						
							
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach

				<div class="nest-new ">
				{!! $commission->links() !!}
				</div>
			</div>
			
        </div>
        @else
        <div>No data available.</div>
	@endif

@endsection
