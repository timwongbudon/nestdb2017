@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
			<div class="nest-property-edit-wrapper">
				<div class="col-md-6 col-md-offset-3 col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Expenses {{ $months[$month] }} {{ $year }} {{ $user->name }}</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Period From</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="payperiodfrom" id="payperiodfrom" class="form-control" value="{{ $expenses->payperiodfrom }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Period To</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="payperiodto" id="payperiodto" class="form-control" value="{{ $expenses->payperiodto }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Mode</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<select name="paymode" id="paymode" class="form-control">
										<option value="Cheque">Cheque</option>
										<option value="Bank Transfer" 
										@if ($expenses->paymode == 'Bank Transfer')
											selected
										@endif
										>Bank Transfer</option>
										<option value="Cash" 
										@if ($expenses->paymode == 'Cash')
											selected
										@endif
										>Cash</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Cheque / Transaction Ref No</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="chequenumber" id="chequenumber" class="form-control" value="{{ $expenses->chequenumber }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Amount</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="amount" id="amount" class="form-control" value="{{ $expenses->amount }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Expensify ID</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="expensifyid" id="expensifyid" class="form-control" value="{{ $expenses->expensifyid }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Employee Name</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="employeename" id="employeename" class="form-control" value="{{ $expenses->employeename }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Employee Address</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<textarea name="employeeaddress" id="employeeaddress" class="form-control">{{ $expenses->employeeaddress }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Payment Date</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="paymentdate" id="paymentdate" class="form-control" value="{{ $expenses->paymentdate }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Update All Data</button>
								<a href="{{ url('/commission/expenses/'.$user->id) }}" class="nest-button nest-right-button btn btn-default">Back</a>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<div class="text-center" id="pdfbuttontext" style="display:none;">Please click "Update All Data" to be able to download the PDF</div>
								<div id="pdfbutton">
									<a href="{{ url('/commission/pdfexpenses/'.$user->id.'/'.$year.'/'.$month) }}" class="nest-button nest-right-button btn btn-default" target="_blank">PDF Download</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<br /><br /><br /><br /><br />
			</div>
		</form>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('change', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('#pdfbuttontext').css('display', 'block');
	});
	
</script>
@endsection
