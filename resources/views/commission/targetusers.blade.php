@extends('layouts.app')

@section('content')
<div class="nest-new admusers">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>Active Consultants</h4>
				</div>
			</div>
			<div class="panel-body" style="padding-bottom:15px;">
				@foreach ($users as $u)
					@if ($u['status'] == 0)
						<div class="col-lg-3 col-md-4 col-sm-6">
							<a href="{{ url('/commission/targetuser/'.$u['id']) }}">
								<div class="col-xs-4">
									@if ($u['picpath'] == "")
										<img src="{{ url('/showimage/users/dummy.jpg') }}" />
									@else
										<img src="{{ url($u['picpath']) }}" />
									@endif
								</div>
								<div class="col-xs-8">
									<b>{{ $u['name'] }}</b>
									<div class="nest-admin-users-black">
										{{ $u['rightsstr'] }}
									</div>
								</div>
								<div class="clearfix"></div>
							</a>
						</div>
					@endif
				@endforeach
				<div class="clearfix"></div>
				
			</div>
		</div>
    </div>
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>Inactive Consultants</h4>
				</div>
			</div>
			<div class="panel-body" style="padding-bottom:15px;">
				@foreach ($users as $u)
					@if ($u['status'] == 1)
						<div class="col-lg-3 col-md-4 col-sm-6">
							<a href="{{ url('/commission/targetuser/'.$u['id']) }}">
								<div class="col-xs-4">
									@if ($u['picpath'] == "")
										<img src="{{ url('/showimage/users/dummy.jpg') }}" />
									@else
										<img src="{{ url($u['picpath']) }}" />
									@endif
								</div>
								<div class="col-xs-8">
									<b>{{ $u['name'] }}</b>
									<div class="nest-admin-users-black">
										{{ $u['rightsstr'] }}
									</div>
								</div>
								<div class="clearfix"></div>
							</a>
						</div>
					@endif
				@endforeach
				<div class="clearfix"></div>
				
			</div>
        </div>
    </div>
</div>
@endsection
