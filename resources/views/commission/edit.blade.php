@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<style>
	.select2-container .select2-choices .select2-search-field input, .select2-container .select2-choice, .select2-container .select2-choices{
    background-color: #ffffff;	
	}
	.select2-container-multi.select2-container-disabled .select2-choices .select2-search-choice{
		background-color: #a0a0a0;	
	}
</style>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2({
		placeholder: "Select",
	});
});
</script>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-sm-6">
				<form action="" method="POST" class="form-horizontal" id="invoice-form" enctype="multipart/form-data">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Invoicing {{ $commission['property']->name }}</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}

							<input type="hidden" name="status" value="1" />
							<input type="hidden" name="commissionid" value="{{ $commission->id }}" />
							<input type="hidden" name="clientid" value="{{ $commission['client']->id }}" />

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Consultant</div>
								</div>
								<div class="col-xs-8">
									<select name="consultantid" id="consultantid" class="form-control">
										@foreach ($consultants as $c)
											@if ($c->isConsultant() && $c->status == 0)
												@if ($c->id == $commission->consultantid)
													<option value="{{ $c->id }}" selected>{{ $c->name }}</option>
												@else
													<option value="{{ $c->id }}">{{ $c->name }}</option>
												@endif
											@endif
										@endforeach
										@foreach ($consultants as $c)
											@if ($c->isConsultant() && $c->status == 1)
												@if ($c->id == $commission->consultantid)
													<option value="{{ $c->id }}" selected>{{ $c->name }} [inactive]</option>
												@else
													<option value="{{ $c->id }}">{{ $c->name }} [inactive]</option>
												@endif
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Other Consultant</div>
								</div>
								<div class="col-xs-8">
									<!-- consultantid -->
									<select name="shared_with[]" class="form-control js-example-basic-multiple" disabled="disabled" style="  background: #FFF;
    border: 1px solid;
    border-radius: 0;" multiple="">
										@foreach ($consultants as $u)
											@if ($u->isConsultant() && $u->status !== 1)												
												<option value="{{ $u->id }}" 
												@if ( !empty($lead->shared_with) && in_array($u->id, json_decode($lead->shared_with)) )												
													selected
												@endif
												>
												{{ $u->name }}
												</option>
												
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>


							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Property</div>
								</div>
								<div class="col-xs-8">
									<select name="propertyid" id="propertyid" class="form-control">
										@foreach ($properties as $p)
											<option value="{{ $p->id }}"
											@if ($p->id == $commission->propertyid)
												selected
											@endif
											>{{ $p->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Lease/Sale</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<select name="salelease" id="salelease" class="form-control">
										@if ($commission->salelease == 2)
											<option value="1">Lease</option>
											<option value="2" selected>Sale</option>
										@else
											<option value="1">Lease</option>
											<option value="2">Sale</option>
										@endif
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Nest managing Stamp Duty?</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									@if (count($invoices) > 0 && $commission->stamping!==1) 
										<select name="stamping" id="stamping" disabled="disabled"  class="form-control">
									@else 
									<select name="stamping" id="stamping"  class="form-control">
									@endif
										<option value="0">N/A</option>
										<option value="1"
										@if ($commission->stamping == 1)
											selected
										@endif
										>Yes</option>
										<option value="2"
										@if ($commission->stamping == 2)
											selected
										@endif
										>No</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									@if ($commission['shortlist']->typeid == 2)
										<div class="nest-property-edit-label">Full Sale Price (HK$)</div>
									@else
										<div class="nest-property-edit-label">Full Lease per Month (HK$)</div>
									@endif
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="fullamount" id="fullamount" class="form-control" value="{{ $commission->fullamount }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Amount to Invoice Landlord/Vendor (HK$)</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="landlordamount" id="landlordamount" class="form-control" value="{{ $commission->landlordamount }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Amount to Invoice Tenant/Purchaser (HK$)</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="tenantamount" id="tenantamount" class="form-control" value="{{ $commission->tenantamount }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Invoice Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="invoicedate" id="invoicedate" class="form-control" value="{{ $commission->invoicedate }}">
								</div>
								<div class="clearfix"></div>
							</div>
							@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Invoice Numbers</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="text" name="invoicenumbers" id="invoicenumbers" class="form-control" value="{{ $commission->invoicenumbers }}">
									</div>
									<div class="clearfix"></div>
								</div>
							@else
								<input type="hidden" name="invoicenumbers" id="invoicenumbers" value="{{ $commission->invoicenumbers }}" />
							@endif

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Offer Signing Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="commissiondate" id="commissiondate" class="form-control" value="{{ $commission->commissiondate }}">
								</div>
								<div class="clearfix"></div>
							</div>
							@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->getUserRights()['Director'])
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Date Landlord/Vendor Paid</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="date" name="landlordpaiddate" id="landlordpaiddate" class="form-control" value="{{ $commission->landlordpaiddate }}">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Date Tenant/Purchaser Paid</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="date" name="tenantpaiddate" id="tenantpaiddate" class="form-control" value="{{ $commission->tenantpaiddate }}">
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Type of Deal</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<select name="type" id="type" class="form-control">
										<option value="direct"
										@if ($commission->type == 'direct')
											selected
										@endif
										>Direct</option>
										<option value="coop"
										@if ($commission->type == 'coop')
											selected
										@endif>Co-operation</option>
										<option value="reduced"
										@if ($commission->type == 'reduced')
											selected
										@endif>Reduced commission</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							@if ($curconsultant->commstruct == 2)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Source of Client</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<select name="source" id="source" class="form-control">
											<option value="0">Nest</option>
											<option value="1"
											@if ($commission->source == 1)
												selected
											@endif>Previous Client / Direct</option>
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
							@else
								<input type="hidden" value="0" name="source" />
							@endif
							@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->getUserRights()['Director'])
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Status</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<select name="status" id="status" class="form-control">
											<option value="1"
											@if ($commission->status == '1')
												selected
											@endif
											>Submitted</option>
											<option value="2"
											@if ($commission->status == '2')
												selected
											@endif
											>Invoice Created</option>
											<option value="3"
											@if ($commission->status == '3')
												selected
											@endif
											>Invoice Sent</option>
											<option value="5"
											@if ($commission->status == '5')
												selected
											@endif
											>Waiting for Tenant Payment</option>
											<option value="6"
											@if ($commission->status == '6')
												selected
											@endif
											>Waiting for Landlord Payment</option>
											<option value="7"
											@if ($commission->status == '7')
												selected
											@endif
											>Waiting for Other Payment</option>
											<option value="8"
											@if ($commission->status == '8')
												selected
											@endif
											>Waiting for Several Payment</option>
											<option value="10"
											@if ($commission->status == '10')
												selected
											@endif
											>Fully Paid</option>
											<option value="11"
											@if ($commission->status == '11')
												selected
											@endif
											>Cancelled</option>
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
							@else
								<input type="hidden" value="{{ $commission->status }}" name="status" />
							@endif

							<div id="stampdutywrapper"
							@if ($commission->stamping != 1)
								style="display:none;"
							@endif
							>
								<br />
								<h4>Stamp Duty</h4>

								<div id="salestampdutywrapper"
								@if ($commission->salelease == 1)
									style="display:none;"
								@endif
								>
									Stamp duty isn't handled by Nest. If it is, please add a comment with the calculated stamp duty.
								</div>
								<div id="leasestampdutywrapper"
								@if ($commission->salelease != 1)
									style="display:none;"
								@endif
								>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Term</div>
										</div>
										<div class="col-lg-5 col-xs-8">
											@php
												$default1 		=  '';
												$default2 		=  '';
												$default3 		=  '';

												switch($commission->sd_term)
												{
														case '1':
														$default1 = 'selected';
														break;
														case '2': default:
														$default2 = 'selected';
														break;
														case '3':
														$default3 = 'selected';
														break;

												}
											@endphp
											<select name="sd_term" id="sd_term" class="form-control">

												<option value="1" {{$default1}}>Less than 1 Year</option>
												<option value="2" {{$default2}}>Between 1 and 3 Years</option>
												<option value="3" {{$default3}}>More than 3 Years</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label" id="sd_rent_label">
												@if ($commission->sd_term == 1)
													Total Rent
												@else
													Average Yearly Rent
												@endif
											</div>
										</div>
										<div class="col-lg-5 col-xs-8">
											<input type="number" name="sd_rent" id="sd_rent" class="form-control" value="{{ $commission->sd_rent }}" step="0.01">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Key Money, Construction Fees, etc.</div>
										</div>
										<div class="col-lg-5 col-xs-8">
											<input type="number" name="sd_keymoney" id="sd_keymoney" class="form-control" value="{{ $commission->sd_keymoney }}" step="0.01">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Stamp Duty</div>
										</div>
										<div class="col-lg-5 col-xs-8" style="padding-top:8px;">
											<input type="hidden" name="sd_amount" id="sd_amount" class="form-control" value="{{ $commission->sd_amount }}" step="1">
											<div id="stampdutyamount">HK$ {{ number_format($commission->sd_amount, 2) }}</div>
											<div id="stampdutycalculation" style="font-size:80%;padding-top:5px;"></div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>

							<script>
								function calculateStampDuty(){
									var term = jQuery('#sd_term').val();
									var rent = jQuery('#sd_rent').val();
									var keymoney = jQuery('#sd_keymoney').val();

									//5 dollars duplication fee
									var stampduty = 5;
									var out = '5 + ';

									//round up yearly average rent to the nearest 100
									if (rent%100 > 0){
										rent = Math.floor(rent/100)*100+100;
									}

									if (term <= 1){
										stampduty = stampduty + rent*0.0025;
										out = out + rent + ' x 0.0025';
									}else if (term == 2){
										stampduty = stampduty + rent*0.005;
										out = out + rent + ' x 0.005';
									}else{
										stampduty = stampduty + rent*0.01;
										out = out + rent + ' x 0.01';
									}

									if (keymoney > 0){
										stampduty = stampduty + keymoney*0.0425;
										out = out + ' + ' + keymoney + ' x 0.0425';
									}

									//round up to full number
									if (Math.floor(stampduty) < stampduty){
										stampduty = Math.floor(stampduty) + 1;
									}

									jQuery('#sd_amount').val(stampduty);
									if (stampduty > 1000){
										var nrout = 'HK$ '+Math.floor(stampduty/1000)+',';
										if (stampduty%1000 < 10){
											nrout = nrout + '00' + stampduty%1000;
										}else if (stampduty%1000 < 100){
											nrout = nrout + '0' + stampduty%1000;
										}else{
											nrout = nrout + '' + stampduty%1000;
										}
										jQuery('#stampdutyamount').html(nrout+'.00');
									}else{
										jQuery('#stampdutyamount').html('HK$ '+stampduty+'.00');
									}
									jQuery('#stampdutycalculation').html('('+out+')');
								}

								jQuery('#stamping').on('change', function(){
									if (jQuery('#stamping').val() == 1){
										jQuery('#stampdutywrapper').css('display', 'block');

										if (jQuery('#salelease').val() == 1){
											jQuery('#salestampdutywrapper').css('display', 'none');
											jQuery('#leasestampdutywrapper').css('display', 'block');
										}else{
											jQuery('#salestampdutywrapper').css('display', 'block');
											jQuery('#leasestampdutywrapper').css('display', 'none');
										}
									}else{
										jQuery('#stampdutywrapper').css('display', 'none');
									}
								});
								jQuery('#salelease').on('change', function(){
									if (jQuery('#stamping').val() == 1){
										jQuery('#stampdutywrapper').css('display', 'block');

										if (jQuery('#salelease').val() == 1){
											jQuery('#salestampdutywrapper').css('display', 'none');
											jQuery('#leasestampdutywrapper').css('display', 'block');
										}else{
											jQuery('#salestampdutywrapper').css('display', 'block');
											jQuery('#leasestampdutywrapper').css('display', 'none');
										}
									}else{
										jQuery('#stampdutywrapper').css('display', 'none');
									}
								});
								jQuery('#sd_term').on('change', function(){
									if (jQuery('#sd_term').val() == 1){
										jQuery('#sd_rent_label').html('Total Rent');
									}else{
										jQuery('#sd_rent_label').html('Average Yearly Rent');
									}
									calculateStampDuty();
								});
								jQuery('#sd_rent').on('change', function(){
									calculateStampDuty();
								});
								jQuery('#sd_keymoney').on('change', function(){
									calculateStampDuty();
								});
							</script>

							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Update</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-sm-6">
				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->id == $commission->consultantid)
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Invoices</h4>
						</div>

						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							@if (count($invoices) > 0)
								@foreach ($invoices as $inv)
									<div class="row">
										<div class="col-xs-3">#{{ ($inv->id) }}</div>
										<div class="col-xs-3">
											@if ($inv->invtype == 1)
												@if ($inv->whofor == 2)
													Tenant
												@else
													Landlord
												@endif
											@else
												@if ($inv->whofor == 2)
													Purchaser
												@else
													Vendor
												@endif
											@endif
										</div>
										<div class="col-xs-3">HK${{ number_format(($inv->price+$inv->addcost), 2) }}</div>
										<div class="col-xs-3">
											@if ($inv->status == 0 || $inv->status == 2)
												pending &nbsp; <a href="/commission/editinvoice/{{ $inv->id }}">edit</a>
											@elseif ($inv->status == 1 || $inv->status == 4)
												<a href="/commission/invoice/{{ $inv->id }}" target="_blank"><i class="fa fa-arrow-circle-o-down"></i> with Logo</a> &nbsp; <a href="/commission/editinvoice/{{ $inv->id }}">edit</a><br />
												<a href="/commission/invoice/{{ $inv->id }}/no" target="_blank"><i class="fa fa-arrow-circle-o-down"></i> without Logo</a>
											@elseif ($inv->status == 3)
												deleted
											@endif
										</div>
									</div>
								@endforeach
							@else
								<div class="text-center"><a href="/commission/createinvoices/{{ $commission->id }}" class="nest-button btn btn-default create-invoice">Create Invoices</a></div>
							@endif
						</div>
					</div>
				@endif
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Invoice Uploads</h4>
					</div>

					<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
						<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
							<div class="nest-propety-docs-uploaded col-xs-12">
								@if (count($docuploads) > 0)
									@foreach ($docuploads as $d)
										@if ($d->doctype == 'landlordinvoice')
											Invoice Landlord
										@elseif ($d->doctype == 'tenantinvoice')
											Invoice Tenant
										@elseif ($d->doctype == 'coopinvoice')
											Invoice Coop
										@elseif ($d->doctype == 'stampduty')
											Stamp Duty
										@endif
										from
										{{ $d->created_at_nice() }}
										@if (isset($consultants[$d->userid]))
											by {{ $consultants[$d->userid]->name }}
										@endif
										<a href="{{ url('/commission/showdoc/'.$d->id.'') }}" target="_blank">download</a>
										@if (Auth::user()->id == $d->userid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
											<a href="{{ url('/commission/removedoc/'.$d->id.'/'.$d->commissionid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a>
										@endif
										<br />
									@endforeach
								@endif
							</div>
							<div class="clearfix"></div>
							<div class="nest-propety-prop-docs-upload" style="padding-top:12px;">
								<form action="{{ url('/commission/docupload/'.$commission->id.'') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="col-xs-6">
										<select name="doctype" class="form-control" style="font-weight:normal;">
											<option value="landlordinvoice">Invoice Landlord</option>
											<option value="tenantinvoice">Invoice Tenant</option>
											<option value="coopinvoice">Invoice Coop</option>
										</select>
									</div>
									<div class="col-xs-6">
										<button class="nest-button nest-right-button btn btn-default" type="submit">Upload Document</button>
									</div>
									<div class="clearfix"></div>
									<div class="draganddropup col-xs-12">
										{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
									</div>
								</form>
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>
				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Consultant Compensation</h4>
						</div>
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:5px;">
							<div class="row" style="padding-bottom:5px;">
								<div class="col-xs-3 text-center"><b>Consultant</b></div>
								<div class="col-xs-3 text-center"><b>Agency Fee</b></div>
								<div class="col-xs-3 text-center"><b>Commission</b></div>
							</div>
							@foreach ($commcons as $index => $comm)
								<div class="row" style="padding-bottom:10px;">
									<form action="{{ url('/commission/commconsedit/'.$comm->id) }}" method="post">
										{{ csrf_field() }}
										<div class="col-xs-3">
											<select name="consultantid" id="consultantid{{ $index }}" class="form-control">
												@foreach ($consultants as $c)
													@if ($c->isConsultant() && $c->status == 0)
														@if ($c->id == $comm->consultantid)
															<option value="{{ $c->id }}" selected>{{ $c->name }}</option>
														@else
															<option value="{{ $c->id }}">{{ $c->name }}</option>
														@endif
													@endif
												@endforeach
												@foreach ($consultants as $c)
													@if ($c->isConsultant() && $c->status == 1)
														@if ($c->id == $comm->consultantid)
															<option value="{{ $c->id }}" selected>{{ $c->name }} [inactive]</option>
														@else
															<option value="{{ $c->id }}">{{ $c->name }} [inactive]</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
										<div class="col-xs-3">
											<input type="number" step="0.01" name="commissionconsultant" id="commissionconsultant{{ $index }}" class="form-control" value="{{ $comm->commissionconsultant }}">
										</div>
										<div class="col-xs-1">
											<button class="nest-button btn btn-default" style="width:100%;" onClick="return calccomp({{ $index }});"><i class="fa fa-calculator"></i></button>
										</div>
										<div class="col-xs-3">
											<input type="number" step="0.01" name="payout" id="payout{{ $index }}" class="form-control" value="{{ $comm->payout }}">
										</div>
										<div class="col-xs-1">
											<button class="nest-button btn btn-default" type="submit" style="width:100%;"><i class="fa fa-save"></i></button>
										</div>
										<div class="col-xs-1">
											<a href="{{ url('/commission/commconsrem/'.$comm->id) }}" onclick="return confirm('Are you sure you want to remove this record?')" class="nest-button btn btn-default" style="width:100%;">X</a>
										</div>
									</form>
								</div>
							@endforeach

							<script>
								function calccomp(index){
									var ctype = 0; 
									var consultantid = jQuery('#consultantid'+index).val();
									var comm = jQuery('#commissionconsultant'+index).val();

									@foreach ($consultants as $c)
										@if ($c->isConsultant() && $c->status == 0)
											if (consultantid == {{ $c->id }}){
												ctype = {{ $c->commstruct }};
											}
										@endif
									@endforeach

									if (ctype == 0){
										alert('No commission type set for this consultant');
										jQuery('#payout'+index).val(0);
									}else if (ctype == 1){
										alert('For standard commission consultants, please calulate the payout amount manually');
										jQuery('#payout'+index).val(0);
									}else if (ctype == 2){
										var percentage = 0;
										var source = jQuery('#source').val();
										var type = jQuery('#salelease').val();

										if(type == 1){
											percentage = 0.40;
										}else if(type == 2){
											if(source == 0){
												percentage = 0.45;
											}else{
												percentage = 0.50;
											}
										}

										jQuery('#payout'+index).val(Math.round(comm*percentage));
									}else if (ctype == 3){
										jQuery('#payout'+index).val(Math.round(comm*0.20));
									}

									return false;
								}
							</script>

							<div class="row">
								<div class="col-xs-12">
									<a href="{{ url('/commission/commconsnew/'.$commission->id) }}" class="nest-button nest-right-button btn btn-default">Add Another Consultant</a>
								</div>
							</div>
						</div>
					</div>
				@endif

				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])

				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Information Officer Commission</h4>
					</div>

					<div class="panel-body">
					@if (count($io_commission) > 0)
						<div>
							<div class="nest-buildings-result-wrapper">

							<div class="row 								
										nest-striped								
									">
										<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Name</b>
										</div>										
										<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Date Created</b>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Commission $(HKD)</b>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6">
										<b>	Status</b>
										</div>
										<div class="clearfix"></div>
									</div>


								@foreach ($io_commission as $index => $inv)
									<div class="row 
									@if ($index%2 == 1)
										nest-striped
									@endif
									">
										<div class="col-lg-3 col-md-3 col-sm-6">
										{{ $inv->firstname }}  {{ $inv->lastname }}											
										</div>
										
										<div class="col-lg-3 col-md-3 col-sm-6">
										{{ $inv->salarydate !=='0000-00-00 00:00:00' ? date('Y-m-d',strtotime($inv->salarydate)) : "-" }}
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6">
										{{ $inv->commission }}
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6">
											@if($inv->status=='Approve')
												<span style="color: green; font-weight: bold">{{$inv->status}}d</span>
											@elseif($inv->status=='Pending')
											<span style="color: orange; font-weight: bold">{{$inv->status}} for Approval</span>
											@elseif($inv->status=='Decline')
											<span style="color: red; font-weight: bold">{{$inv->status}}d</span>
											@endif									
											
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							</div>
							
						</div>
						@else
						<div>No data available.</div>
					@endif
					</div>
				</div>
				
				@endif

				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>

					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcomment" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addComment();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsEdit"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				
				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->getUserRights()['Consultant'])
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Change Log</h4>
					</div>

					<div class="panel-body">
					

						@foreach ($logs as $c)
							<div class="media media-comment">
								<div class="media-left">								
								<img src="{{ url($users[$c->user_id]->picpath) }}" class="img-circle media-object" width="60px" style="border:1px solid #cccccc;" />
								</div>
								<div class="media-body message text-left">
									<u>{{$users[$c->user_id]->name}}</u> <small class="text-muted">{{ date_format($c->created_at, 'd/m/Y H:i' ) }}</small>								
									<br />
									{!! $c->log !!}
								</div>
							</div>
							<div class="clearfix"></div>
						@endforeach
					</div>
				</div>
				@endif
				<!--
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Notify Accountant</h4>
						</div>

						<div class="panel-body">
							Clicking the button below will send an email to the accountant to take a look at this invoice. Please also leave some comments in the comment area
							as to what changed and what the accountant should do.<br /><br />
							<form action="{{ url('/commission/notifyaccountant') }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" value="{{ $commission->id }}" name="commid" />
								<input type="submit" class="nest-button nest-right-button btn btn-default" value="Notify Accountant" />
							</form>
						</div>
					</div>
				-->
			</div>
			<div class="clearfix"></div>
			<br /><br /><br /><br /><br />
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('change', function(){
		jQuery('#pdfbutton').css('display', 'none');
	});


	function loadCommentsEdit(){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/commission/comments/".$commission->id) }}',
			cache: false,
			success: function (data) {
				jQuery('#existingCommentsEdit').html(data);
			},
			error: function (data) {
				jQuery('#existingCommentsEdit').html(data);
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/commission/comments/".$commission->id) }}',
					cache: false,
					success: function (data) {
						jQuery('#existingCommentsEdit').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/commission/comments/".$commission->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsEdit').html(data);
							},
							error: function (data) {
								jQuery('#existingCommentsEdit').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}

	function addComment(){
		var comment = jQuery('#newcomment').val().trim();
		if (comment.length > 0){
			jQuery.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: '{{ url("/commission/commentadd/".$commission->id) }}',
				data: {comment: comment},
				dataType: 'text',
				cache: false,
				async: false,
				success: function (data) {
					jQuery('#newcomment').val('');
					loadCommentsEdit();
				},
				error: function (data) {
					console.log(data);
					alert('Something went wrong, please try again!');
				}
			});
		}else{
			alert('Please enter a comment');
		}
	}

	$('#newcomment').keydown(function( event ) {
		if ( event.keyCode == 13 ) {
			event.preventDefault();
			addComment();
		}
	});

	function removeComment(t){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'POST',
			url: '{{ url("/commission/commentremove/".$commission->id) }}',
			data: {t: t},
			dataType: 'text',
			cache: false,
			async: false,
			success: function (data) {
				loadCommentsEdit();
			},
			error: function (data) {
				alert('Something went wrong, please try again!');
			}
		});
	}

	jQuery( document ).ready(function(){
		loadCommentsEdit();
	});

	jQuery('#invoice-form').on('submit', function(e){
					
		var stamping = jQuery('#stamping').val();
		if(stamping != 1 && {{count($invoices) }} <= 0 ){
			var con = confirm("Are you sure you want to proceed without a stamp duty?");
			if(con == false){
				e.preventDefault();
			}
		}
		
		
	});

	jQuery('.create-invoice').on('click', function(e){
					
					var stamping = jQuery('#stamping').val();
					if(stamping != 1 ){
						var con = confirm("Are you sure you want to proceed without a stamp duty?");
						if(con == false){
							e.preventDefault();
						}
					}
					
					
				});

	
</script>
@endsection
