@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-10">
					<h4 style="margin-bottom:0px;">Payroll: {{ $user->name }}</h4>
				</div>
				<div class="col-xs-2 ">
					<div class="dropdown text-right">
						<button class="btn btn-primary dropdown-toggle" style="background-color: #86726A;" type="button" data-toggle="dropdown">Convert
						<span class="caret"></span></button>
						<ul class="dropdown-menu convert" style="    right: 0;    width: 150px;    left: 100px;">
						<li><a href="#" class="us">US Dollar</a></li>
						<li><a href="#" class="eu">Euro</a></li>
						<li><a href="#" class="za">Rand</a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">	
				<div class="nest-buildings-result-wrapper">	
					@for ($year = $curyear; $year > 2018; $year--)
						@if ($year < $curyear)
							@php
								$curmonth = 12;
							@endphp
						@endif
						
						@for ($month = $curmonth; $month > 0; $month--)
							<div class="row 
							@if ($month%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>{{ $months[$month] }} {{ $year }}</b>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (isset($salaries[$year][$month]))
										<b>Payroll ID:</b> {{ $salaries[$year][$month]->id }}
									@else
										-
									@endif
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (isset($salaries[$year][$month]))
										<b>Net Amount (excl. MPF):</b> 
											HK$ {{ number_format($salaries[$year][$month]->fixedfees+$salaries[$year][$month]->commissionfees+$salaries[$year][$month]->otherfees-$salaries[$year][$month]->mpfemployee-$salaries[$year][$month]->otherdeductions, 2) }}
											<span class="us-c"><br />USD: {{ number_format($salaries[$year][$month]['total_USD'], 2) }}</span>
											<span class="eu-c"><br />EUR: {{ number_format($salaries[$year][$month]['total_EUR'], 2) }}</span>
											<span class="za-c"><br />ZAR: {{ number_format($salaries[$year][$month]['total_ZAR'], 2) }}</span>									

										@if (!empty($salaries[$year][$month]->paymentdate))
											<br /><b>Payment Date:</b> {{ \NestDate::nest_date_format($salaries[$year][$month]->paymentdate, 'Y-m-d') }}
										@endif

									@else
										-
									@endif
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (isset($salaries[$year][$month]))
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/editsalary/'.$user->id.'/'.$year.'/'.$month) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
									@else
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/createsalary/'.$user->id.'/'.$year.'/'.$month) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Create
										</a>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							
						@endfor
					@endfor
					
					
						
					
				</div>
			</div>
		</div>
    </div>
</div>

<style>
.us-c,
.eu-c,
.za-c{display: none;}
</style>


<script>
jQuery(document).ready(function(){
	jQuery('.convert a').click(function(){
		jQuery(".us-c, .eu-c, .za-c").hide();
		jQuery("."+jQuery(this).attr('class')+"-c").show();
	});
});
</script>
@endsection
