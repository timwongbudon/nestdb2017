@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4 style="margin-bottom:0px;">Expenses {{ $user->name }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">	
				<div class="nest-buildings-result-wrapper">	
					@for ($year = $curyear; $year > 2018; $year--)
						@if ($year < $curyear)
							@php
								$curmonth = 12;
							@endphp
						@endif
						
						@for ($month = $curmonth; $month > 0; $month--)
							<div class="row 
							@if ($month%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>{{ $months[$month] }} {{ $year }}</b>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (isset($expenses[$year][$month]))
										<b>Expense ID:</b> {{ $expenses[$year][$month]->id }}<br />
										<b>Expensify ID:</b> {{ $expenses[$year][$month]->expensifyid }}
									@else
										-
									@endif
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (isset($expenses[$year][$month]))
										<b>Amount:</b> HK$ {{ number_format($expenses[$year][$month]->amount, 2) }}
										@if (!empty($expenses[$year][$month]->paymentdate))
											<br /><b>Payment Date:</b> {{ \NestDate::nest_date_format($expenses[$year][$month]->paymentdate, 'Y-m-d') }}
										@endif
									@else
										-
									@endif
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (isset($expenses[$year][$month]))
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/editexpenses/'.$user->id.'/'.$year.'/'.$month) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
									@else
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/createexpenses/'.$user->id.'/'.$year.'/'.$month) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Create
										</a>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							
						@endfor
					@endfor
					
					
						
					
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
