@extends('layouts.app')

@section('content')

            <!-- Current Clients -->
            @if (count($invoices) > 0)
				<div>
					<div class="nest-buildings-result-wrapper">
						@foreach ($invoices as $index => $inv)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-2 col-md-2 col-sm-6">
									<a href="/commission/invoice/{{ $inv->id }}" target="_blanke"><b>#{{ (20000+$inv->id) }}</b></a>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6">
									<b>Consultant:</b> {{ $consultants[$inv->consultantid]->name }}<br />
									<b>For:</b>
									<a href="/commission/invoiceedit/{{ $inv->commissionid }}" target="_blank">
										@if ($inv->invtype == 1)
											@if ($inv->whofor == 2)
												Tenant
											@else
												Landlord
											@endif
										@else
											@if ($inv->whofor == 2)
												Purchaser
											@else
												Vendor
											@endif
										@endif
									</a>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6">
									<b>Status:</b> 
									@if ($inv->status == 0)
										Newly Created
									@else
										Changed
									@endif
									<br />
									<b>Amount:</b> HK${{ number_format(($inv->price+$inv->addcost), 2) }}
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6">
									<b>Date Created:</b> 
									{{date("d/m/Y H:i:s", strtotime($inv->created_at))}}
									
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6">
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/editinvoice/'.$inv->id) }}">
											<i class="fa fa-btn fa-pencil"></i>Edit
										</a>
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/approve/'.$inv->id) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Approve
										</a>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
				</div>
            @endif
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection
