<html>
<head>

<title>Salary</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}
td{
	border:1px solid #000000;
	padding:4px 8px !important;
}
.innertable td{
	padding:0px !important;
}

.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-sl-top-left{
	padding-top:0px;
	padding-bottom:0px;
	height:200px;
}
.nest-invoice-main-table{
	padding-top:20px;
	height:480px;
}
.nest-invoice-top-center{
	font-size:22px;
	text-align:center;
	color:#9e9586;
	line-height:1;
	font-family: MyriadBold;
	padding-top:0px;
}
.nest-invoice-nest-address{
	text-align:center;
	color:#9e9586;
	font-family: MyriadBold;
	font-size:16px;
	padding-bottom:10px;
	border-bottom:1px dashed #000000;
}
.nest-invoice-thankyou{
	text-align:center;
	color:#9e9586;
	font-family: MyriadBold;
	font-size:20px;
	padding-top:10px;
	padding-bottom:10px;
}
</style>


</head>
<body>
	<div class="nest-pdf-container">
		<div style="text-align:center;padding-top:180px;padding-bottom:30px;font-size:18px;font-family:MyriadBold;">
			<div style="padding-bottom:5px;"><span style="border-bottom:1px solid #222222;">Strictly Private & Confidential</span></div>
			PAY-RECORD
		</div>
		<div style="padding-bottom:20px;font-size:18px;font-family:MyriadBold;">
			Nest Property Limited
		</div>
		
		<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #000000;margin-bottom:30px;">
			<tr>
				<td width="50%">
					<div style="font-family:MyriadBold;">
						STAFF DETAILS
					</div>
				</td>
				<td width="50%">
					<div style="font-family:MyriadBold;">
						SALARY ADVICE FOR THE MONTH OF {{ strtoupper($months[$month]) }} {{ $year }}
					</div>
				</td>
			</tr>
			<tr>
				<td width="50%" valign="top">
					<div style="font-family:MyriadBold;">
						{{ $salary->employeename }} 
						@if (trim($salary->hkid) != '')
							(HKID No. {{ trim($salary->hkid) }})
						@endif
					</div>
					<div style="font-family:MyriadBold;">
						Employee ID: {{ $salary->userid }}
					</div>
					<div style="">
						{!! nl2br($salary->employeeaddress) !!}
					</div>
				</td>
				<td width="50%" valign="top">
					<div style="">
						<table class="innertable" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="padding-top:0px;">
									<div style="font-family:MyriadBold;padding-right:20px;">Department:</div>
								</td>
								<td>
									Residential Property
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">Pay Basis:</div>
								</td>
								<td>
									Monthly
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">Pay Period:</div>
								</td>
								<td>
									@if (!empty($salary->payperiodfrom) && !empty($salary->payperiodto))
										{{ \NestDate::nest_date_format($salary->payperiodfrom, 'Y-m-d') }}-{{ \NestDate::nest_date_format($salary->payperiodto, 'Y-m-d') }}
									@endif
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">Pay Mode:</div>
								</td>
								<td>
									{{ $salary->paymode }}
								</td>
							</tr>
							@if (trim($salary->chequenumber) != '' && $salary->chequenumber != '0')
								<tr>
									<td>
										<div style="font-family:MyriadBold;padding-right:20px;">
											@if ($salary->paymode == 'Cheque')
												Cheque Number:
											@else
												Reference Number:
											@endif
										</div>
									</td>
									<td>
										{{ $salary->chequenumber }}
									</td>
								</tr>
							@endif
						</table>
					</div>
				</td>
			</tr>
		</table>
		
		
		<table width="100%" cellpadding="0" cellspacing="0" style="border:0px solid #000000;margin-bottom:30px;">
			<tr>
				<td width="50%" colspan="2" style="">
					<div style="font-family:MyriadBold;text-align:center;">EARNINGS</div>
				</td>
				<td width="50%" colspan="2" style="">
					<div style="font-family:MyriadBold;text-align:center;">DEDUCTIBLES</div>
				</td>
			</tr>
			<tr>
				<td width="35%" style="">
					<div style="font-family:MyriadBold;">DESCRIPTION</div>
				</td>
				<td width="15%">
					<div style="font-family:MyriadBold;text-align:center;">AMOUNT</div>
				</td>
				<td width="35%">
					<div style="font-family:MyriadBold;">DESCRIPTION</div>
				</td>
				<td width="15%" style="">
					<div style="font-family:MyriadBold;text-align:center;">AMOUNT</div>
				</td>
			</tr>
			<tr>
				<td style="height:150px;" valign="top">
					@if ($salary->fixedfees > 0)
						Basic Salary:<br />
					@endif
					@if ($salary->commissionfees > 0)
						Commission:<br />
					@endif
					@if ($salary->otherfees > 0)
						{{ $salary->otherfeestext }}:<br />
					@endif
					@if ($leadingCommissionFees > 0)
						Sales Team Leader Bonus:<br>
					@endif
					@if ($io_commission > 0)
						Information Officer Commission:<br>
					@endif
				</td>
				<td style="text-align:right;" valign="top">
					@if ($salary->fixedfees > 0)
						HK$ {{ number_format($salary->fixedfees, 2) }}&nbsp;&nbsp;<br />
					@endif
					@if ($salary->commissionfees > 0)
						HK$ {{ number_format($salary->commissionfees, 2) }}&nbsp;&nbsp;<br />
					@endif
					@if ($salary->otherfees > 0)
						HK$ {{ number_format($salary->otherfees, 2) }}&nbsp;&nbsp;<br />
					@endif
					@if ($leadingCommissionFees > 0)
						HK$ {{ number_format($leadingCommissionFees, 2) }}&nbsp;&nbsp;<br />
					@endif
					@if ($io_commission > 0)
						HK$ {{ number_format($io_commission, 2) }}&nbsp;&nbsp;<br />
					@endif
				</td>
				<td valign="top">
					@if ($salary->mpfemployee > 0)
						Employee MPF (Mandatory):<br />
					@endif
					@if ($salary->otherdeductions > 0)
						{{ $salary->otherdeductionstext }}:<br />
					@endif
				</td>
				<td style="text-align:right;" valign="top">
					@if ($salary->mpfemployee > 0)
						HK$ {{ number_format($salary->mpfemployee, 2) }}&nbsp;&nbsp;<br />
					@endif
					@if ($salary->otherdeductions > 0)
						HK$ {{ number_format($salary->otherdeductions, 2) }}&nbsp;&nbsp;<br />
					@endif
				</td>
			</tr>
			<tr>
				<td valign="top" style="">
					<div style="font-family:MyriadBold;text-align:right;">Total Additions</div>
				</td>
				<td style="text-align:right;" valign="top">
					<div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format(($salary->fixedfees+$salary->commissionfees+$salary->otherfees+$leadingCommissionFees+$io_commission), 2) }}&nbsp;&nbsp;</div>
				</td>
				<td valign="top">
					<div style="font-family:MyriadBold;text-align:right;">Total Deductions</div>
				</td>
				<td style="text-align:right;" valign="top">
					<div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format(($salary->mpfemployee+$salary->otherdeductions), 2) }}&nbsp;&nbsp;</div>
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="3" style="border-left:0px !important;border-bottom:0px !important;border-right:0px !important;border-top:2px solid #000000;">
					<div style="font-family:MyriadBold;text-align:right;padding-top:5px;">NET SALARY:</div>
				</td>
				<td style="text-align:right;border-left:0px !important;border-bottom:0px !important;border-right:0px !important;border-top:2px solid #000000;" valign="top">
					<div style="font-family:MyriadBold;text-align:right;padding-top:5px;">HK$ {{ number_format(( ($salary->fixedfees+$salary->commissionfees+$salary->otherfees+$leadingCommissionFees+$io_commission)-$salary->mpfemployee-$salary->otherdeductions), 2) }}&nbsp;&nbsp;</div>
				</td>
			</tr>
		</table>
		
		@if ($salary->mpfsixty == 1)
			MPF contributions are not applicable for employees who have not reached 60 days of employment
		@else
			<table width="50%" cellpadding="0" cellspacing="0" style="border:1px solid #000000;">
				<tr>
					<td width="60%">
						<div style="font-family:MyriadBold;">Employer MPF (Mandatory)</div>
					</td>
					<td width="40%">
						<div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format($salary->mpfemployer, 2) }}&nbsp;&nbsp;</div>
					</td>
				</tr>
			</table>
		@endif
		
		<br /><br /><br /><br />
		<div style="font-family:MyriadBold;text-align:center;padding-bottom:20px;">
			This is a computer-generated payslip. No signature is required.
		</div>
		<div style="text-align:center;">
			Suite 1301, 13/F, Hollywood Centre, 233 Hollywood Road, Sheung Wan, Hong Kong
		</div>
	</div>
</body>
</html>

















