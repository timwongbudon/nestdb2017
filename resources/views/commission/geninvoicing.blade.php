@extends('layouts.app')

@section('content')

	<div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
			<a class="nest-button btn btn-primary" href="{{ url('commission/creategeninvoices') }}">
				<i class="fa fa-btn fa-plus"></i> Create Generic Invoice
			</a>
		</div>
	</div>
	
	
	<!-- Current Clients -->
	@if (count($invoices) > 0)
		<div>
			<div class="nest-buildings-result-wrapper">
				@foreach ($invoices as $index => $inv)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Invoice No.:</b> #{{ $inv->id }}<br />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Name:</b> {{ $inv->buyername }}<br />
							<b>Amount:</b> HK$ {{ number_format(($inv->price+$inv->addcost), 2) }}<br />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Invoice Date:</b> {{ NestDate::nest_date_format($inv->invoicedate, 'Y-m-d') }}<br />
							<b>Due Date:</b> {{ NestDate::nest_date_format($inv->duedate, 'Y-m-d') }}<br />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/editgeninvoices/'.$inv->id) }}" target="_blank">
								<i class="fa fa-btn fa-pencil-square-o"></i> Edit
							</a>
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/geninvoice/'.$inv->id) }}" target="_blank">
								<i class="fa fa-btn fa-pencil-square-o"></i> Download
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
			</div>
			@if ($invoices->lastPage() > 1)
				<div class="panel-footer nest-buildings-pagination">
					{{ $invoices->links() }}
				</div>
			@endif
		</div>
	@endif
	<script>
		jQuery('form').on('focus', 'input[type=number]', function (e) {
			jQuery(this).on('mousewheel.disableScroll', function (e) {
				e.preventDefault();
			});
		});
		jQuery('form').on('blur', 'input[type=number]', function (e) {
			jQuery(this).off('mousewheel.disableScroll');
		});
	</script>
@endsection
