@extends('layouts.app')

@section('content')

            <!-- Client Search -->
            <div class="nest-buildings-search-panel panel panel-default">
                <div class="panel-body" style="padding-bottom:0px;">
					<!-- New Property Form -->
                    <form action="{{ url('/commission/invoicing') }}" method="GET" class="form-horizontal">
                        <div class="nest-buildings-search-wrapper row">
							@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->getUserRights()['Director'])
								<div class="col-sm-4">
									{!! Form::select('user_id', $user_ids, $curuser, ['id'=>'invoicing-user_id', 'class'=>'form-control', 'placeholder' => 'All consultant invoices']); !!}
								</div>
								<div class="col-sm-4">
									<input type="text" name="client_name" placeholder="Client" id="client-name" class="form-control" value="{{ old('client_name', $input->client_name) }}">
								</div>
								<div class="col-sm-4">
									<input type="text" name="property_name" placeholder="Property" class="form-control" value="{{ old('property_name', $input->property_name) }}" >
								</div>
								<div class="col-sm-4">
									{!! Form::select('status', $statuslist, $input->status, ['id'=>'invoicing-status', 'class'=>'form-control', 'placeholder' => 'Status']); !!}
								</div>
								<div class="col-sm-4">
									<input type="text" name="invoice_number" placeholder="Invoice Number" class="form-control" value="{{ old('invoice_number', $input->invoice_number) }}" >
								</div>
								<div class="col-sm-4">
									<input type="number" name="invoice_amount" placeholder="Invoice Amount" class="form-control" value="{{ old('invoice_amount', $input->invoice_amount) }}" step=".01" >
								</div>
							@else
								<input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
								<div class="col-sm-3">
									<input type="text" name="client_name" placeholder="Client" id="client-name" class="form-control" value="{{ old('client_name', $input->client_name) }}">
								</div>
								<div class="col-sm-3">
									<input type="text" name="property_name" placeholder="Property" class="form-control" value="{{ old('property_name', $input->property_name) }}" >
								</div>
								<div class="col-sm-3">
									{!! Form::select('status', $statuslist, $input->status, ['id'=>'invoicing-status', 'class'=>'form-control', 'placeholder' => 'Status']); !!}
								</div>
								<div class="col-sm-3">
									<input type="text" name="invoice_number" placeholder="Invoice Number" class="form-control" value="{{ old('invoice_number', $input->invoice_number) }}" >
								</div>
							@endif
							<div class="clearfix"></div>
                        </div>
                        <div class="nest-buildings-search-buttons-wrapper nest-buildings-search-wrapper row">
							<div class="col-sm-4">
								<select name="orderby" id="orderby" class="form-control">
									<option value="statusasc">Order by Status (New to Finished)</option>
									<option value="statusdesc" 
									@if ($input->orderby == 'statusdesc')
										selected
									@endif
									>Order by Status (Finished to New)</option>
									<option value="invnrasc" 
									@if ($input->orderby == 'invnrasc')
										selected
									@endif
									>Order by Invoice Number (Low to High)</option>
									<option value="invnrdesc" 
									@if ($input->orderby == 'invnrdesc')
										selected
									@endif
									>Order by Invoice Number (High to Low)</option>
									<option value="offerdateasc" 
									@if ($input->orderby == 'offerdateasc')
										selected
									@endif
									>Order by Offer Date (Low to High)</option>
									<option value="offerdatedesc" 
									@if ($input->orderby == 'offerdatedesc')
										selected
									@endif
									>Order by Offer Date (High to Low)</option>
									<option value="invoicedateasc" 
									@if ($input->orderby == 'invoicedateasc')
										selected
									@endif
									>Order by Invoice Date (Low to High)</option>
									<option value="invoicedatedesc" 
									@if ($input->orderby == 'invoicedatedesc')
										selected
									@endif
									>Order by Invoice Date (High to Low)</option>
								</select>
							</div>
                            <div class="col-sm-8">
                                <button type="submit" name="action" value="search" class="nest-button nest-right-button btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search / Update
								</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
            <!-- Current Clients -->
            @if (count($commissions) > 0)
				<div>
					<div class="nest-buildings-result-wrapper">
						@foreach ($commissions as $index => $comm)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if ($comm->status == 1)
										<span class="badge badge-danger">New</span>&nbsp; 
									@elseif ($comm->status == 2)
										<span class="badge badge-danger">Created</span>&nbsp; 
									@elseif ($comm->status < 10 && $comm->invoicedate != null && $comm->invoicedate != '0000-00-00' && $comm->invoicedate <= date('Y-m-d', time() - 14*24*60*60))
										<span class="badge badge-danger">Late</span>&nbsp; 
									@endif
									<b>Property:</b>
									
									@if($comm->property->deleted_at === null)									
										<a href="javascript:;"  data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $comm->propertyid }}, '{{ str_replace("'", "\'", $comm->property->name) }}', '')">{{ $comm->property->name }}</a>
									@else								
										<a href="{{url('/property/edit/'.$comm->propertyid)}}"> {{\App\Property::where('id', $comm->propertyid)->withTrashed()->first()->name }} (deleted) </a>
									@endif

									<br />
									<b>Client:</b> <a href="{{ url('client/show/'.$comm->client->id) }}" target="_blank">{{ $comm->client->name() }}</a><br />
									<b>Transaction</b>: 
									@if ($comm->shortlist->typeid == 2)
										Sale
									@else
										Lease
									@endif
									<br />
									<b>Status:</b> {{ $comm->getStatus() }}
									@if ($comm->status == 7)
										<i class="fa fa-times" style="color:#cc0000;"></i>
									@endif
									<br />
									<b>Invoice No.:</b> {{ $comm->invoicenumbers }}<br />
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Consultant:</b> {{ $consultants[$comm->consultantid]->name }}<br />
									<b>Finished on:</b> {{ NestDate::nest_date_format($comm->finishdate, 'Y-m-d') }}<br />
									<b>Invoice Date:</b> {{ NestDate::nest_date_format($comm->invoicedate, 'Y-m-d') }}<br />
									<b>Offer Signing Date:</b> {{ NestDate::nest_date_format($comm->commissiondate, 'Y-m-d') }}<br />
									<b>Type:</b> {{ ucfirst($comm->type) }}<br />
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Amount Landlord:</b> HK$ {{ number_format($comm->landlordamount, 2) }}
									@if ($comm->landlordamount > 0)
										@if (trim($comm->landlordpaiddate) == '' || trim($comm->landlordpaiddate) == '0000-00-00')
											<i class="fa fa-times" style="color:#cc0000;"></i>
										@else
											<i class="fa fa-check" style="color:#00cc00;"></i>
										@endif
									@endif
									<br />
									<b>Payment Landlord:</b> {{ NestDate::nest_date_format($comm->landlordpaiddate, 'Y-m-d') }}<br />
									<b>Amount Tenant:</b> HK$ {{ number_format($comm->tenantamount, 2) }}
									@if ($comm->tenantamount > 0)
										@if (trim($comm->tenantpaiddate) == '' || trim($comm->tenantpaiddate) == '0000-00-00')
											<i class="fa fa-times" style="color:#cc0000;"></i>
										@else
											<i class="fa fa-check" style="color:#00cc00;"></i>
										@endif
									@endif
									<br />
									<b>Payment Tenant:</b> {{ NestDate::nest_date_format($comm->tenantpaiddate, 'Y-m-d') }}<br />
									@if ($comm->sd_amount > 0)
										<b>Stamp Duty:</b> HK$ {{ number_format($comm->sd_amount, 2) }}<br />
									@endif
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
										@if (isset($payouts[$comm->id]) && count($payouts[$comm->id]) > 0)
											@foreach ($payouts[$comm->id] as $cc)
												<b>Commission to {{ $consultants[$cc->consultantid]->name }}:</b> HK$ {{ number_format($cc->payout, 2) }}<br />
											@endforeach
										@endif
									@endif
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/invoiceedit/'.$comm->id) }}" target="_blank">
										<i class="fa fa-btn fa-pencil-square-o"></i> View
									</a>
									@if ($comm->shortlist->typeid == 2)
										<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('/documents/index-sale/'.$comm->shortlistid) }}">
											<i class="fa fa-btn fa-book"></i> Documents
										</a>
									@else
										<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('/documents/index-lease/'.$comm->shortlistid) }}">
											<i class="fa fa-btn fa-book"></i> Documents
										</a>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
					@if ($commissions->lastPage() > 1)
						<div class="panel-footer nest-buildings-pagination">
							{{ $commissions->appends($querystringArray)->links() }}
						</div>
					@endif
				</div>
            @endif
            <div class="nest-buildings-search-panel panel panel-default" style="margin-top:15px;">
                <div class="panel-body">
					<b>Important:</b> If the number of deals on the dashboard is not the same as your number of invoices, this is very likely due to cases of shared commission 
					where you helped another consultant with a deal. These deals count towards your total deal count but don't show up here. This section only shows your own deals.
				</div>
			</div>
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection
