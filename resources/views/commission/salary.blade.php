@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
			<div class="nest-property-edit-wrapper">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Salary {{ $months[$month] }} {{ $year }} {{ $user->name }}</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}

							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Period From</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="payperiodfrom" id="payperiodfrom" class="form-control" value="{{ $salary->payperiodfrom }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Period To</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="payperiodto" id="payperiodto" class="form-control" value="{{ $salary->payperiodto }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Mode</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<select name="paymode" id="paymode" class="form-control">
										<option value="Cheque">Cheque</option>
										<option value="Bank Transfer"
										@if ($salary->paymode == 'Bank Transfer')
											selected
										@endif
										>Bank Transfer</option>
										<option value="Cash"
										@if ($salary->paymode == 'Cash')
											selected
										@endif
										>Cash</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Cheque/Transfer Number</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="chequenumber" id="chequenumber" class="form-control" value="{{ $salary->chequenumber }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Basic Salary</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="fixedfees" id="fixedfees" class="form-control" value="{{ $salary->fixedfees }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Other Earnings Text (Bonus, ...)</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="otherfeestext" id="otherfeestext" class="form-control" value="{{ $salary->otherfeestext }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Other Earnings Amount</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="otherfees" id="otherfees" class="form-control" value="{{ $salary->otherfees }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">MPF Employee</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="mpfemployee" id="mpfemployee" class="form-control" value="{{ $salary->mpfemployee }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">MPF Employer</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="mpfemployer" id="mpfemployer" class="form-control" value="{{ $salary->mpfemployer }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">MPF First 60 Days</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="checkbox" name="mpfsixty" id="mpfsixty" style="margin-top:8px;"
									@if ($salary->mpfsixty == 1)
										checked
									@endif
									>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Other Deductables Text (Unpaid Leave, ...)</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="otherdeductionstext" id="otherdeductionstext" class="form-control" value="{{ $salary->otherdeductionstext }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Other Deductables Amount</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="otherdeductions" id="otherdeductions" class="form-control" value="{{ $salary->otherdeductions }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Employee Name</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="employeename" id="employeename" class="form-control" value="{{ $salary->employeename }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">HKID No.</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="hkid" id="hkid" class="form-control" value="{{ $salary->hkid }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Employee Address</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<textarea name="employeeaddress" id="employeeaddress" class="form-control">{{ $salary->employeeaddress }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Payment Date</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="paymentdate" id="paymentdate" class="form-control" value="{{ $salary->paymentdate }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Update All Data</button>
								<a href="javascript:;" class="nest-button nest-right-button btn btn-default" onClick="recalcMPF()"><i class="fa fa-refresh"></i>&nbsp; MPF</a>
								<a href="{{ url('/commission/salaries/'.$user->id) }}" class="nest-button nest-right-button btn btn-default">Back</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
						   <h4>Commission</h4>
						</div>

						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							@if (count($invoiceoutput) > 0)
								@foreach ($invoiceoutput as $index => $io)
									<div class="row
									@if ($index%2 == 1)
										nest-striped
									@endif
									">
										<div class="col-lg-5 col-md-5 col-sm-12">
											<b>Property:</b> <a href="javascript:;"  data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $io['invoice']->propertyid }}, '{{ str_replace("'", "\'", $io['invoice']->property->name) }}', '')">{{ $io['invoice']->property->name }}</a><br />
											<b>Client:</b> <a href="{{ url('client/show/'.$io['invoice']->client->id) }}" target="_blank">{{ $io['invoice']->client->name() }}</a>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-6">
											<b>Invoice ID:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->id }}</a><br />
											<b>Invoice No.:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->invoicenumbers }}</a><br />
											<b>Who Paid:</b> {{ $io['type'] }}
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 text-right">
											HK$ {{ number_format($io['amount'], 2) }}
										</div>
									<div class="clearfix"></div>
								</div>
								@endforeach
								<div class="row">
									<div class="col-lg-5 col-md-5 col-sm-12"></div>
									<div class="col-lg-4 col-md-4 col-sm-6"></div>
									<div class="col-lg-3 col-md-3 col-sm-6 text-right">
										<b>HK$ {{ number_format($commissionfees, 2) }}</b>
										<input type="hidden" name="commissionfees" id="commissionfees" value="{{ $commissionfees }}" />
									</div>
									<div class="clearfix"></div>
								</div>

							@else
								<br /><div class="text-center">No commission for this month</div><br /><br />
							<input type="hidden" name="commissionfees" id="commissionfees" value="0" />
							@endif
						</div>
					</div>
					@if (count($managing) > 0)
						<div class="panel panel-default">
							<div class="panel-heading">
							   <h4>Sales Team Manager Bonus</h4>
							</div>

							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
								@if (count($managingbonus) > 0)
									@foreach ($managingbonus as $index => $io)
										<div class="row
										@if ($index%2 == 1)
											nest-striped
										@endif
										">
											<div class="col-lg-5 col-md-5 col-sm-12">
												<b>Property:</b> <a href="javascript:;"  data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $io['invoice']->propertyid }}, '{{ str_replace("'", "\'", $io['invoice']->property->name) }}', '')">{{ $io['invoice']->property->name }}</a><br />
												<b>Client:</b> <a href="{{ url('client/show/'.$io['invoice']->client->id) }}" target="_blank">{{ $io['invoice']->client->name() }}</a>
												<br><b>Consultant:</b> {{ $io['invoice']->consultant->name }}<br> <br>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6">
												<b>Invoice ID:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->id }}</a><br />
												<b>Invoice No.:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->invoicenumbers }}</a><br />
												<b>Who Paid:</b> {{ $io['type'] }}
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 text-right">
												HK$ {{ number_format($io['amount'], 2) }}
											</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-12"></div>
										<div class="col-lg-4 col-md-4 col-sm-6"></div>
										<div class="col-lg-3 col-md-3 col-sm-6 text-right">
											<b>HK$ {{ number_format($managingcommissionfees, 2) }}</b>
										</div>
										<div class="clearfix"></div>
									</div>

								@else
									<br /><div class="text-center">No bonus for this month</div><br /><br />
								@endif
							</div>
						</div>
					@endif
					@if (count($leading) > 0)
						<div class="panel panel-default">
							<div class="panel-heading">
							   <h4>Sales Team Leader Bonus</h4>
							</div>

							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
								@if (count($leadingBonus) > 0)
									@foreach ($leadingBonus as $index => $io)
										<div class="row
										@if ($index%2 == 1)
											nest-striped
										@endif
										">
											<div class="col-lg-5 col-md-5 col-sm-12">
												<b>Property:</b> <a href="javascript:;"  data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $io['invoice']->propertyid }}, '{{ str_replace("'", "\'", $io['invoice']->property->name) }}', '')">{{ $io['invoice']->property->name }}</a><br />
												<b>Client:</b> <a href="{{ url('client/show/'.$io['invoice']->client->id) }}" target="_blank">{{ $io['invoice']->client->name() }}</a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6">
												<b>Invoice ID:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->id }}</a><br />
												<b>Invoice No.:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->invoicenumbers }}</a><br />
												<b>Who Paid:</b> {{ $io['type'] }}
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 text-right">
												HK$ {{ number_format($io['amount'], 2) }}
											</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-12"></div>
										<div class="col-lg-4 col-md-4 col-sm-6"></div>
										<div class="col-lg-3 col-md-3 col-sm-6 text-right">
											<b>HK$ {{ number_format($leadingCommissionFees, 2) }}</b>
										</div>
										<div class="clearfix"></div>
									</div>

								@else
									<br /><div class="text-center">No bonus for this month</div><br /><br />
								@endif
							</div>
						</div>
					@endif
					@if (count($io_commission) > 0)
						<div class="panel panel-default">
							<div class="panel-heading">
							   <h4>Information Officer Commission</h4>
							</div>
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
								@if (count($io_commission) > 0)

								@foreach ($io_commission as $index => $inv)
									<div class="row 
									@if ($index%2 == 1)
										nest-striped
									@endif
									">
										<div class="col-lg-3 col-md-3 col-sm-6">
										<strong>Property:</strong> {{ $inv->name }}
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6">
										<strong>Client Name:</strong> {{ $inv->client_firstname }} {{ $inv->client_lastname }}
										</div>	
										<div class="col-lg-3 col-md-3 col-sm-6">
										<strong>Invoice ID:</strong> <a href="{{url('/commission/invoiceedit/'.$inv->invoiceId) }}">{{ $inv->invoiceId }}</a>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6">
										HK$ {{ $inv->commission }}
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							</div>
								@else
									<br /><div class="text-center">No commission for this month</div><br /><br />
								@endif							
						</div>
					@endif
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								@if ( $salary->commissionfees > 0 && round($commissionfees) != round($salary->commissionfees))
									<div class="text-center">Please click "Update All Data" to be able to download the PDF</div>
								@else
									<div class="text-center" id="pdfbuttontext" style="display:none;">Please click "Update All Data" to be able to download the PDF</div>
									<div id="pdfbutton">
										<a href="{{ url('/commission/pdfsalary/'.$user->id.'/'.$year.'/'.$month) }}" class="nest-button nest-right-button btn btn-default" target="_blank">PDF Download</a>
										<a href="{{ url('/commission/pdfcommissionlist/'.$user->id.'/'.$year.'/'.$month) }}" class="nest-button nest-right-button btn btn-default" target="_blank">Commission Breakdown</a>
									</div>
								@endif
							</div>
						</div>
					</div>

				</div>
				<div class="clearfix"></div>
				<br /><br /><br /><br /><br />
			</div>
		</form>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('change', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('#pdfbuttontext').css('display', 'block');
	});

	function recalcMPF(){
		var commissionfees = Number(jQuery('#commissionfees').val())+Number(jQuery('#fixedfees').val())+Number(jQuery('#otherfees').val());

		if (commissionfees < 7100){
			jQuery('#mpfemployee').val(0);
			jQuery('#mpfemployer').val(Math.round(commissionfees*0.05*100)/100);
		}else if (commissionfees > 30000){
			jQuery('#mpfemployee').val(1500);
			jQuery('#mpfemployer').val(1500);
		}else{
			jQuery('#mpfemployee').val(Math.round(commissionfees*0.05*100)/100);
			jQuery('#mpfemployer').val(Math.round(commissionfees*0.05*100)/100);
		}

		jQuery('#pdfbutton').css('display', 'none');
		jQuery('#pdfbuttontext').css('display', 'block');

	}
</script>
@endsection
