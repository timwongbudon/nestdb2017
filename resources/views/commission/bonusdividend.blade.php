@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
			<div class="nest-property-edit-wrapper">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Bonus/Dividend</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Type</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<select name="type" id="type" class="form-control">
										<option value="0">Bonus Payment</option>
										<option value="1" 
										@if ($record->type == 1)
											selected
										@endif
										>Dividend</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row" id="useridwrapper">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Employee</div>
								</div>
								<div class="col-sm-6 col-xs-8" id="userselection_bonus" <?php if ($record->type == 1) echo 'style="display:none;"'; ?>>
									<select name="userid_bonus" id="userid" class="form-control">
										@foreach ($consultants as $u)
											<option value="{{ $u->id }}" data-role="{{$u->getUserRightsString()}}"
											@if ($u->id == $record->userid)
												selected
											@endif
											>{{ $u->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-sm-6 col-xs-8" id="userselection_dividend" <?php if ($record->type == 0) echo 'style="display:none;"'; ?>>
									<select name="userid_dividend" id="userid" class="form-control">
										@foreach ($directors as $u)
											<option value="{{ $u->id }}" data-role="{{$u->getUserRightsString()}}"
											@if ($u->id == $record->userid)
												selected
											@endif
											>{{ $u->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Pay Mode</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<select name="paymode" id="paymode" class="form-control">
										<option value="Cheque">Cheque</option>
										<option value="Bank Transfer" 
										@if ($record->paymode == 'Bank Transfer')
											selected
										@endif
										>Bank Transfer</option>
										<option value="Cash" 
										@if ($record->paymode == 'Cash')
											selected
										@endif
										>Cash</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Cheque/Transfer Number</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="chequenumber" id="chequenumber" class="form-control" value="{{ $record->chequenumber }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row" id="otherfeestextwrapper" <?php if ($record->type == 1) echo 'style="display:none;"'; ?>>
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Earnings Text (Bonus, ...)</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="otherfeestext" id="otherfeestext" class="form-control" value="{{ $record->otherfeestext }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Earnings/Dividend Amount</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="otherfees" id="otherfees" class="form-control" value="{{ $record->otherfees }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row" id="otherdeductionstextwrapper" <?php if ($record->type == 1) echo 'style="display:none;"'; ?>>
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Deductables Text (Unpaid Leave, ...)</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="otherdeductionstext" id="otherdeductionstext" class="form-control" value="{{ $record->otherdeductionstext }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row" id="otherdeductionswrapper" <?php if ($record->type == 1) echo 'style="display:none;"'; ?>>
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Deductables Amount</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="number" name="otherdeductions" id="otherdeductions" class="form-control" value="{{ $record->otherdeductions }}" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
						
							<input type="hidden" name="employeename" id="employeename" class="form-control" value="{{ $record->employeename }}">
							<div class="nest-property-edit-row" id="hkidwrapper" <?php if ($record->type == 1) echo 'style="display:none;"'; ?>>
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">HKID No.</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="text" name="hkid" id="hkid" class="form-control" value="{{ $record->hkid }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row" id="employeeaddresswrapper" <?php if ($record->type == 1) echo 'style="display:none;"'; ?>>
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Employee Address</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<textarea name="employeeaddress" id="employeeaddress" class="form-control">{{ $record->employeeaddress }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Payment Date</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<input type="date" name="paymentdate" id="paymentdate" class="form-control" value="{{ $record->paymentdate }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-sm-6 col-xs-4">
									 <div class="nest-property-edit-label">Comments</div>
								</div>
								<div class="col-sm-6 col-xs-8">
									<textarea rows="5" class="form-control" name="comments">{{$record->comments}}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Update/Save</button>
								<a href="{{ url('/commission/bonusdividends') }}" class="nest-button nest-right-button btn btn-default">Back</a>
							</div>
						</div>
					</div>
				</div>
				@if ($record->id > 0)
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:5px;">
								<div class="col-xs-12" style="margin-top:10px;">
									<div class="text-center" id="pdfbuttontext" style="display:none;">Please click "Update/Save" to be able to download the PDF</div>
									<div id="pdfbutton">
										<a href="{{ url('/commission/bonusdividendpdf/'.$record->id) }}" class="nest-button nest-right-button btn btn-default" target="_blank">PDF Download</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
				<div class="clearfix"></div>
				<br /><br /><br /><br /><br />
			</div>
		</form>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('change', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('#pdfbuttontext').css('display', 'block');
	});

	jQuery('select[name="userid"]').on('change', function(e){
		var selectedName = jQuery('select[name="userid"] :selected').html();
		jQuery('input[name="employeename"]').val(selectedName);
	});

	function employeeDividend (type) {
		jQuery('select[name="userid"]').find('option').each(function(){
			if (type == 1 && jQuery(this).attr('data-role').indexOf('Director') < 0 ) {
				jQuery(this).attr('hidden', true);
			} else {
				jQuery(this).removeAttr('hidden');
			}
		});
	}
	
	jQuery('#type').on('change', function(){
		var val = jQuery('#type').val();
		employeeDividend(val);
		if (val == 0){
			jQuery('#useridwrapper').css('display', 'block');
			jQuery('#otherfeestextwrapper').css('display', 'block');
			jQuery('#otherdeductionstextwrapper').css('display', 'block');
			jQuery('#otherdeductionswrapper').css('display', 'block');
			jQuery('#employeenamewrapper').css('display', 'block');
			jQuery('#hkidwrapper').css('display', 'block');
			jQuery('#employeeaddresswrapper').css('display', 'block');
			jQuery('#userselection_bonus').css('display', 'block');
			jQuery('#userselection_dividend').css('display', 'none');
		}else{
			// jQuery('#useridwrapper').css('display', 'none');
			jQuery('#otherfeestextwrapper').css('display', 'none');
			jQuery('#otherdeductionstextwrapper').css('display', 'none');
			jQuery('#otherdeductionswrapper').css('display', 'none');
			jQuery('#employeenamewrapper').css('display', 'none');
			jQuery('#hkidwrapper').css('display', 'none');
			jQuery('#employeeaddresswrapper').css('display', 'none');
			jQuery('#userselection_bonus').css('display', 'none');
			jQuery('#userselection_dividend').css('display', 'block');
		}
	});
	
</script>
@endsection
