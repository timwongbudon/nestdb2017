<html>
<head>

<title>Expenses</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}
td{
	border:1px solid #000000;
	padding:4px 8px !important;
}
.innertable td{
	padding:0px !important;
}

.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-sl-top-left{
	padding-top:0px;
	padding-bottom:0px;
	height:200px;
}
.nest-invoice-main-table{
	padding-top:20px;
	height:480px;
}
.nest-invoice-top-center{
	font-size:22px;
	text-align:center;
	color:#9e9586;
	line-height:1;
	font-family: MyriadBold;
	padding-top:0px;
}
.nest-invoice-nest-address{
	text-align:center;
	color:#9e9586;
	font-family: MyriadBold;
	font-size:16px;
	padding-bottom:10px;
	border-bottom:1px dashed #000000;
}
.nest-invoice-thankyou{
	text-align:center;
	color:#9e9586;
	font-family: MyriadBold;
	font-size:20px;
	padding-top:10px;
	padding-bottom:10px;
}
</style>


</head>
<body>
	<div class="nest-pdf-container">
		<div style="text-align:center;padding-top:180px;padding-bottom:30px;font-size:18px;font-family:MyriadBold;">
			<div style="padding-bottom:5px;"><span style="border-bottom:1px solid #222222;">Strictly Private & Confidential</span></div>
			EXPENSES
		</div>
		<div style="padding-bottom:20px;font-size:18px;font-family:MyriadBold;">
			Nest Property Limited
		</div>
		
		<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #000000;margin-bottom:30px;">
			<tr>
				<td width="50%">
					<div style="font-family:MyriadBold;">
						STAFF DETAILS
					</div>
				</td>
				<td width="50%">
					<div style="font-family:MyriadBold;">
						EXPENSES FOR THE MONTH OF {{ strtoupper($months[$month]) }} {{ $year }}
					</div>
				</td>
			</tr>
			<tr>
				<td width="50%" valign="top">
					<div style="font-family:MyriadBold;">
						{{ $expenses->employeename }}
					</div>
					<div style="font-family:MyriadBold;">
						Employee ID: {{ $expenses->userid }}
					</div>
					@if ($expenses->expensifyid > 0)
						<div style="font-family:MyriadBold;">
							Expensify ID: {{ $expenses->expensifyid }}
						</div>
					@endif
					<div style="">
						{!! nl2br($expenses->employeeaddress) !!}
					</div>
				</td>
				<td width="50%" valign="top">
					<div style="">
						<table class="innertable" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="padding-top:0px;">
									<div style="font-family:MyriadBold;padding-right:20px;">Department:</div>
								</td>
								<td>
									Residential Property
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">Refund Basis:</div>
								</td>
								<td>
									Monthly
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">Refund Period:</div>
								</td>
								<td>
									@if (!empty($expenses->payperiodfrom) && !empty($expenses->payperiodto))
										{{ \NestDate::nest_date_format($expenses->payperiodfrom, 'Y-m-d') }}-{{ \NestDate::nest_date_format($expenses->payperiodto, 'Y-m-d') }}
									@endif
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">Payout Mode:</div>
								</td>
								<td>
									{{ $expenses->paymode }}
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-family:MyriadBold;padding-right:20px;">@if ($expenses->paymode == 'Cheque') Cheque @else Transaction Ref No @endif:</div>
								</td>
								<td>
									{{ $expenses->chequenumber }}
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
		
		<table width="50%" cellpadding="0" cellspacing="0" style="border:1px solid #000000;">
			<tr>
				<td width="60%">
					<div style="font-family:MyriadBold;">Expense Amount</div>
				</td>
				<td width="40%">
					<div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format($expenses->amount, 2) }}&nbsp;&nbsp;</div>
				</td>
			</tr>
		</table>
		
		
		<br /><br /><br /><br />
		<div style="font-family:MyriadBold;text-align:center;padding-bottom:20px;">
			This is a computer-generated document. No signature is required.
		</div>
		<div style="text-align:center;">
			Suite 1301, 13/F, Hollywood Centre, 233 Hollywood Road, Sheung Wan, Hong Kong
		</div>
	</div>
</body>
</html>

















