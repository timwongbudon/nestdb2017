@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<style>
	.select2-container .select2-choices .select2-search-field input, .select2-container .select2-choice, .select2-container .select2-choices{
    background-color: #ffffff;	
}
.select2-container-multi.select2-container-disabled .select2-choices .select2-search-choice{
		background-color: #a0a0a0;	
	}
</style>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2({
		placeholder: "Select",
	});
});
</script>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('commission/readyforinvoicing') }}" id="invoice-form" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Create Invoice Record</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							<input type="hidden" name="status" value="1" />
							<input type="hidden" name="shortlistid" value="{{ $shortlist->id }}" />
							<input type="hidden" name="clientid" value="{{ $shortlist['client']->id }}" />
							<input type="hidden" name="finishdate" value="{{ date('Y-m-d') }}" />
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Consultant</div>
								</div>
								<div class="col-xs-8">
									<select name="consultantid" id="consultantid" class="form-control">
										@foreach ($consultants as $c)
											@if ($c->isConsultant())
												@if ($c->id == $shortlist->user_id)
													<option value="{{ $c->id }}" selected>{{ $c->name }}</option>
												@else
													<option value="{{ $c->id }}">{{ $c->name }}</option>
												@endif
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Other Consultant</div>
								</div>
								<div class="col-xs-8">
									<!-- consultantid -->
									<select name="shared_with[]" class="form-control js-example-basic-multiple" disabled="disabled" style="    background: #FFF;
    border: 1px solid;
    border-radius: 0;" multiple="">
										@foreach ($consultants as $u)
											@if ($u->isConsultant() && $u->status !== 1)												
												<option value="{{ $u->id }}" 
												@if ( !empty($lead->shared_with) && in_array($u->id, json_decode($lead->shared_with)) )												
													selected
												@endif
												>
												{{ $u->name }}
												</option>
												
											@endif
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>


							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Property</div>
								</div>
								<div class="col-xs-8">
									<select name="propertyid" id="propertyid" class="form-control">
										@foreach ($properties as $p)
											<option value="{{ $p->id }}">{{ $p->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Lease/Sale</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<select name="salelease" id="salelease" class="form-control">
										@if ($shortlist->typeid == 2)
											<option value="1">Lease</option>
											<option value="2" selected>Sale</option>
										@else
											<option value="1">Lease</option>
											<option value="2">Sale</option>
										@endif
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Nest managing Stamp Duty?</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<select name="stamping" id="stamping" class="form-control">
										<option value="0">N/A</option>
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									@if ($shortlist->typeid == 2)
										<div class="nest-property-edit-label">Full Sale Price (HK$)</div>
									@else
										<div class="nest-property-edit-label">Full Lease per Month (HK$)</div>
									@endif
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="fullamount" id="fullamount" class="form-control" value="" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Amount to Invoice Landlord/Vendor (HK$)</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="landlordamount" id="landlordamount" class="form-control" value="" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Amount to Invoice Tenant/Purchaser (HK$)</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="tenantamount" id="tenantamount" class="form-control" value="" step="0.01">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Invoice Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="invoicedate" id="invoicedate" class="form-control" value="">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Offer Signing Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="commissiondate" id="commissiondate" class="form-control" value="">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Type of Deal</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<select name="type" id="type" class="form-control">
										<option value="direct">Direct</option>
										<option value="coop">Co-operation</option>
										<option value="reduced">Reduced commission</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							@if (Auth::user()->commstruct == 2)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Source of Client</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<select name="source" id="source" class="form-control">
											<option value="0">Nest</option>
											<option value="1">Previous Client / Direct</option>
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
							@else
								<input type="hidden" value="0" name="source" />
							@endif
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Comments</div>
								</div>
								<div class="col-xs-8">
									Please add any information that is needed for the creation of the invoices in the field below. 
									If there is not uploaded signed Tenancy Agreement or Provisional Sale & Purchase Agreement available yet, please state the necessary addresses in the field below 
									for the Office Manager to use.
									<textarea name="comment" id="comment" class="form-control" style="height:100px;"></textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							<div id="stampdutywrapper" style="display:none;">
								<br />
								<h4>Stamp Duty</h4>
								
								<div id="salestampdutywrapper"
								@if ($shortlist->typeid != 2)
									style="display:none;"
								@endif
								>
									Stamp duty isn't handled by Nest. If it is, please add a comment with the calculated stamp duty.
								</div>
								<div id="leasestampdutywrapper"
								@if ($shortlist->typeid == 2)
									style="display:none;"
								@endif
								>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Term</div>
										</div>
										<div class="col-lg-5 col-xs-8">
											<select name="sd_term" id="sd_term" class="form-control">
												<option value="0">Not Defined / Uncertain</option>
												<option value="1">Less than 1 Year</option>
												<option value="2">Between 1 and 3 Uears</option>
												<option value="3">More than 3 Years</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label" id="sd_rent_label">
												Average Yearly Rent
											</div>
										</div>
										<div class="col-lg-5 col-xs-8">
											<input type="number" name="sd_rent" id="sd_rent" class="form-control" value="0" step="0.01">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Key Money, Construction Fees, etc.</div>
										</div>
										<div class="col-lg-5 col-xs-8">
											<input type="number" name="sd_keymoney" id="sd_keymoney" class="form-control" value="0" step="0.01">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Stamp Duty</div>
										</div>
										<div class="col-lg-5 col-xs-8" style="padding-top:8px;">
											<input type="hidden" name="sd_amount" id="sd_amount" class="form-control" value="0" step="1">
											<div id="stampdutyamount"></div>
											<div id="stampdutycalculation" style="font-size:80%;padding-top:5px;"></div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							
							<script>
								function calculateStampDuty(){
									var term = jQuery('#sd_term').val();
									var rent = jQuery('#sd_rent').val();
									var keymoney = jQuery('#sd_keymoney').val();
									
									//5 dollars duplication fee
									var stampduty = 5;
									var out = '5 + ';
									
									//round up yearly average rent to the nearest 100
									if (rent%100 > 0){
										rent = Math.floor(rent/100)*100+100;
									}
									
									if (term <= 1){
										stampduty = stampduty + rent*0.0025;
										out = out + rent + ' x 0.0025';
									}else if (term == 2){
										stampduty = stampduty + rent*0.005;
										out = out + rent + ' x 0.005';
									}else{
										stampduty = stampduty + rent*0.01;
										out = out + rent + ' x 0.01';
									}
									
									if (keymoney > 0){
										stampduty = stampduty + keymoney*0.0425;
										out = out + ' + ' + keymoney + ' x 0.0425';
									}
									
									//round up to full number
									if (Math.floor(stampduty) < stampduty){
										stampduty = Math.floor(stampduty) + 1;
									}
									
									jQuery('#sd_amount').val(stampduty);
									if (stampduty > 1000){
										var nrout = 'HK$ '+Math.floor(stampduty/1000)+',';
										if (stampduty%1000 < 10){
											nrout = nrout + '00' + stampduty%1000;
										}else if (stampduty%1000 < 100){
											nrout = nrout + '0' + stampduty%1000;
										}else{
											nrout = nrout + '' + stampduty%1000;
										}
										jQuery('#stampdutyamount').html(nrout+'.00');
									}else{
										jQuery('#stampdutyamount').html('HK$ '+stampduty+'.00');
									}
									jQuery('#stampdutycalculation').html('('+out+')');
								}
								
								jQuery('#stamping').on('change', function(){
									if (jQuery('#stamping').val() == 1){
										jQuery('#stampdutywrapper').css('display', 'block');
										
										if (jQuery('#salelease').val() == 1){
											jQuery('#salestampdutywrapper').css('display', 'none');
											jQuery('#leasestampdutywrapper').css('display', 'block');
										}else{
											jQuery('#salestampdutywrapper').css('display', 'block');
											jQuery('#leasestampdutywrapper').css('display', 'none');
										}
									}else{
										jQuery('#stampdutywrapper').css('display', 'none');
									}
								});
								jQuery('#salelease').on('change', function(){
									if (jQuery('#stamping').val() == 1){
										jQuery('#stampdutywrapper').css('display', 'block');
										
										if (jQuery('#salelease').val() == 1){
											jQuery('#salestampdutywrapper').css('display', 'none');
											jQuery('#leasestampdutywrapper').css('display', 'block');
										}else{
											jQuery('#salestampdutywrapper').css('display', 'block');
											jQuery('#leasestampdutywrapper').css('display', 'none');
										}
									}else{
										jQuery('#stampdutywrapper').css('display', 'none');
									}
								});
								jQuery('#sd_term').on('change', function(){
									if (jQuery('#sd_term').val() == 1){
										jQuery('#sd_rent_label').html('Total Rent');
									}else{
										jQuery('#sd_rent_label').html('Average Yearly Rent');
									}
									calculateStampDuty();
								});
								jQuery('#sd_rent').on('change', function(){
									calculateStampDuty();
								});
								jQuery('#sd_keymoney').on('change', function(){
									calculateStampDuty();
								});

								jQuery('#invoice-form').on('submit', function(e){
									
									var stamping = jQuery('#stamping').val();
									if(stamping != 1){
										var con = confirm("Are you sure you want to proceed without a stamp duty?");
										if(con == false){
											e.preventDefault();
										}
									}
									
									
								});


							</script>
							
							
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Submit for Invoicing</button>
							</div>
						</div>
					</div>
				</div>
			</form>
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('change', function(){
		jQuery('#pdfbutton').css('display', 'none');
	});
</script>
@endsection
