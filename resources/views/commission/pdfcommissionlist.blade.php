<html>
<head>

<title>Salary</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}
td{
	border:1px solid #000000;
	padding:4px 8px !important;
}
.innertable td{
	padding:0px !important;
}

.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-sl-top-left{
	padding-top:0px;
	padding-bottom:0px;
	height:200px;
}
.nest-invoice-main-table{
	padding-top:20px;
	height:480px;
}
.nest-invoice-top-center{
	font-size:22px;
	text-align:center;
	color:#9e9586;
	line-height:1;
	font-family: MyriadBold;
	padding-top:0px;
}
.nest-invoice-nest-address{
	text-align:center;
	color:#9e9586;
	font-family: MyriadBold;
	font-size:16px;
	padding-bottom:10px;
	border-bottom:1px dashed #000000;
}
.nest-invoice-thankyou{
	text-align:center;
	color:#9e9586;
	font-family: MyriadBold;
	font-size:20px;
	padding-top:10px;
	padding-bottom:10px;
}
</style>


</head>
<body>
	<div class="nest-pdf-container">
		<div style="padding-bottom:30px;padding-top:120px;font-size:14px;font-family:MyriadBold;">
			{{ $salary->employeename }}
		</div>
		<div style="text-align:center;padding-bottom:40px;font-size:18px;font-family:MyriadBold;">
			<span style="border-bottom:1px solid #222222;">Strictly Private & Confidential</span>
		</div>
		<div style="padding-bottom:25px;font-size:18px;font-family:MyriadBold;text-align:center;">
			Commission Breakdown: {{ $months[$month] }} {{ $year }}
		</div>
		
		<table width="100%" cellpadding="0" cellspacing="0" style="border:0px;">
			<tr>
				<!-- Invoice No -->
				<td width="10%"><div style="font-family:MyriadBold;text-align:left;">Inv. No.</div></td>
				<td width="30%"><div style="font-family:MyriadBold;text-align:left;">Property Details</div></td>
				<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Client</div></td>
				<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Received</div></td>
				<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Commission</div></td>
			</tr>
			@if (count($invoiceoutput) > 0)
				@foreach ($invoiceoutput as $index => $io)
					<tr>
						<td>{{ $io['invoice']->invoicenumbers }}</td>
						<td>{{ $io['invoice']->property->unit }}, {{ $io['invoice']->property->name }}</td>
						<td>{{ $io['invoice']->client->name() }}</td>
						<td>{{ $io['type'] }}</td>
						<td style="text-align:right;">HK$ {{ number_format($io['amount'], 2) }}</td>
					</tr>
				@endforeach
			@endif							

			<tr>
				<td colspan="4" style="border-top:2px solid #000000;padding-top:10px !important;padding-bottom:10px !important;border-right:0px;border-bottom:0px !important;border-left:0px !important;"><div style="font-family:MyriadBold;text-align:right;">TOTAL: </div></td>
				<td style="border-top:2px solid #000000;padding-top:10px !important;padding-bottom:10px !important;border-left:0px;border-bottom:0px !important;border-right:0px !important;"><div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format($commissionfees, 2) }}</div></td>
			</tr>
		</table>

		@if (!empty($leadingBonus))
			<br><br>
			<div style="padding-bottom:25px;font-size:18px;font-family:MyriadBold;text-align:center;">
				Sales Team Leader Bonus: {{ $months[$month] }} {{ $year }}
			</div>
			<table width="100%" cellpadding="0" cellspacing="0" style="border:0px;">
				<tr>
					<!-- Invoice No -->
					<td width="10%"><div style="font-family:MyriadBold;text-align:left;">Inv. No.</div></td>
					<td width="30%"><div style="font-family:MyriadBold;text-align:left;">Billing Consultant</div></td>
					@if (request()->get('property') == 1)
						<td width="30%"><div style="font-family:MyriadBold;text-align:left;">Property Details</div></td>	
					@endif
					<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Client</div></td>
					<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Received</div></td>
					<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Commission</div></td>
				</tr>
				@if (count($leadingBonus) > 0)
					@foreach ($leadingBonus as $index => $io)
						<tr>
							<td>{{ $io['invoice']->invoicenumbers }}</td>
							<td>{{ $io['invoice']->consultant->name }}</td>
							@if (request()->get('property') == 1)
								<td>{{ $io['invoice']->property->unit }}, {{ $io['invoice']->property->name }}</td>
							@endif
							<td>{{ $io['invoice']->client->name() }}</td>
							<td>{{ $io['type'] }}</td>
							<td style="text-align:right;">HK$ {{ number_format($io['amount'], 2) }}</td>
						</tr>
					@endforeach
				@endif		

				<tr>
					<td colspan="{{ (request()->get('property') == 1) ? 5 : 4 }}" style="border-top:2px solid #000000;padding-top:10px !important;padding-bottom:10px !important;border-right:0px;border-bottom:0px !important;border-left:0px !important;"><div style="font-family:MyriadBold;text-align:right;">TOTAL: </div></td>
					<td style="border-top:2px solid #000000;padding-top:10px !important;padding-bottom:10px !important;border-left:0px;border-bottom:0px !important;border-right:0px !important;"><div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format($leadingCommissionFees, 2) }}</div></td>
				</tr>
			</table>	
		@endif

		@if (count($io_commission) > 0)
			<br><br>
			<div style="padding-bottom:25px;font-size:18px;font-family:MyriadBold;text-align:center;">
				Information Officer Commission: {{ $months[$month] }} {{ $year }}
			</div>
			<table width="100%" cellpadding="0" cellspacing="0" style="border:0px;">
				<tr>
					<!-- Invoice No -->
					<td width="10%"><div style="font-family:MyriadBold;text-align:left;">Inv. No.</div></td>
					<td width="30%"><div style="font-family:MyriadBold;text-align:left;">Property Details</div></td>
					<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Client</div></td>
					<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Received</div></td>
					<td width="20%"><div style="font-family:MyriadBold;text-align:left;">Commission</div></td>
				</tr>
				@foreach ($io_commission as $index => $io)
						<tr>
							<td>{{ $io->invoiceId }}</td>						
							<td>{{ $io->name }}</td>						
							<td>{{$io->client_firstname}} {{$io->client_lastname}}</td>						
							<td>-</td>						
							<td>{{ $io->commission }}</td>							
						</tr>
					@endforeach
					<tr>
				<td colspan="4" style="border-top:2px solid #000000;padding-top:10px !important;padding-bottom:10px !important;border-right:0px;border-bottom:0px !important;border-left:0px !important;"><div style="font-family:MyriadBold;text-align:right;">TOTAL: </div></td>
				<td style="border-top:2px solid #000000;padding-top:10px !important;padding-bottom:10px !important;border-left:0px;border-bottom:0px !important;border-right:0px !important;"><div style="font-family:MyriadBold;text-align:right;">HK$ {{ number_format($ioComTotal, 2) }}</div></td>
			</tr>
			</table>
		@endif	
		
		<br /><br /><br />
		<div style="padding-bottom:30px;font-size:14px;font-family:MyriadBold;text-align:right;">
			Current Month Commission: HK$ {{ number_format( ($commissionfees+$leadingCommissionFees+$ioComTotal), 2) }}
		</div>
		
	</div>
</body>
</html>

















