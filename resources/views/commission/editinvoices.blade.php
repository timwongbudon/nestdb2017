@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="" method="POST" class="form-horizontal editinvoice" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Basic Information</h4>
						</div>
						
						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')
							
							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Invoice Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="invoicedate" id="invoicedate" class="form-control" value="{{ $inv['invoicedate'] }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($inv['invtype'] == 1)
											Tenant Name
										@else
											Purchaser Name
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="buyername" id="buyername" class="form-control">{{ $inv['buyername'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							@if ($inv['invtype'] == 1)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">
											Tenant Work Title
										</div>
									</div>
									<div class="col-xs-8">
										<textarea name="work_title" id="work_title" class="form-control">{{ $inv['work_title'] }}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">
											Tenant Work Place
										</div>
									</div>
									<div class="col-xs-8">
										<textarea name="work_place" id="work_place" class="form-control">{{ $inv['work_place'] }}</textarea>
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($inv['invtype'] == 1)
											Tenant Address
										@else
											Purchaser Address
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="buyeraddress" id="buyeraddress" class="form-control">{{ $inv['buyeraddress'] }}</textarea>
									<input type="button" id="buyeraddressformat" style="float:right; margin:5px 0;" value="Format Address">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($inv['invtype'] == 1)
											Landlord Name
										@else
											Vendor Name
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="sellername" id="sellername" class="form-control">{{ $inv['sellername'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($inv['invtype'] == 1)
											Landlord Address
										@else
											Vendor Address
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="selleraddress" id="selleraddress" class="form-control">{{ $inv['selleraddress'] }}</textarea>
									<input type="button" id="addressformat" style="float:right; margin:5px 0;" value="Format Address">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Property Address</div>
								</div>
								<div class="col-xs-8">
									<textarea name="propertyaddress" id="propertyaddress" class="form-control">{{ $inv['propertyaddress'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($inv['invtype'] == 1)
											Rental
										@else
											Purchase Price
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="purchasetext" id="purchasetext" class="form-control"
										@if ($inv['invtype'] == 1)
											placeholder="e.g. HK$35,000.00 per month inclusive of Management Fees, Government Rates, Government Rent and Property Tax"
										@else
											placeholder="e.g. HK$10,000,000"
										@endif
									>{{ $inv['purchasetext'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										@if ($inv['invtype'] == 1)
											Term
										@else
											Completion Date
										@endif
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="terms" id="terms" class="form-control" 
										@if ($inv['invtype'] == 1)
											placeholder="e.g. 8th August 2019 – 7th August 2021 (both days incl.) [Two Year Lease, 12+2 month Break Clause]"
										@else
											placeholder="e.g. 16th September 2019"
										@endif
									>{{ $inv['terms'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>


							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Start Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="start_date" id="start_date" class="form-control" value="{{ $inv['start_date'] }}">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										Lease Term
									</div>
								</div>
								<div class="col-xs-8">
									<select name="lease_term" class="lease_term">
										<option value="">Please Select</option>
										@foreach($lease_term as $key => $lt)
											<option value="{{$key}}" 
												@if($inv['lease_term'] == $key)
												selected="selected"
												@endif
											>{{$lt}}</option>											
										@endforeach
										
									</select>
								</div>
								<div class="clearfix"></div>
							</div>

							<div 
								@if($inv['month_break_clause'] >= 3) 
									class="nest-property-edit-row month_break_clause" 
								@else
									class="nest-property-edit-row month_break_clause" style="display: none;"
								@endif
							>
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										Month Break Clause
									</div>
								</div>
								<div class="col-xs-8">
									<select name="month_break_clause" class="" >
										<option value="">Please Select</option>
										@foreach($month_break_clause as $key => $mbc)
											<option value="{{$key}}"
												@if($inv['month_break_clause'] == $key)
												selected="selected"
												@endif
											>{{$mbc}}</option>											
										@endforeach										
									</select>
								</div>
								<div class="clearfix"></div>
							</div>


						</div>
					</div>
				</div>
				<div class="col-sm-6">
					@if ($inv['whofor'] == 2)
						<div class="panel panel-default">
							<div class="panel-heading">
								@if ($inv['invtype'] == 1)
									<h4>Tenant Invoice Information</h4>
								@else
									<h4>Purchaser Invoice Information</h4>
								@endif
							</div>
							
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Fee Calculation Text</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="feecalculation" id="feecalculation" class="form-control" value="{{ $inv['feecalculation'] }}" />
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Invoice Amount</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="number" name="price" id="price" class="form-control" value="{{ $inv['price'] }}" step=".01">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Additional Costs Text (e.g. Stamp Duty)</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="addcosttext" id="addcosttext" class="form-control" placeholder="e.g. 50% of Government Stamp Duty Fee [HK$10,000 ÷ 2]" value="{{ $inv['addcosttext'] }}" />
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Additional Costs Amount (e.g. Stamp Duty)</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="number" name="addcost" id="addcost" class="form-control" value="{{ $inv['addcost'] }}" step=".01">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Due Date</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="date" name="duedate" id="duedate" class="form-control" value="{{ $inv['duedate'] }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					@else
						<div class="panel panel-default">
							<div class="panel-heading">
								@if ($inv['invtype'] == 1)
									<h4>Landlord Invoice Information</h4>
								@else
									<h4>Vendor Invoice Information</h4>
								@endif
							</div>
							
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Fee Calculation Text</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="feecalculation" id="feecalculation" class="form-control" value="{{ $inv['feecalculation'] }}" />
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Invoice Amount</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="number" name="price" id="price" class="form-control" value="{{ $inv['price'] }}" step=".01">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Additional Costs Text (e.g. Stamp Duty)</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="addcosttext" id="addcosttext" class="form-control" placeholder="e.g. 50% of Government Stamp Duty Fee [HK$10,000 ÷ 2]" value="{{ $inv['addcosttext'] }}" />
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Additional Costs Amount (e.g. Stamp Duty)</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="number" name="addcost" id="addcost" class="form-control" value="{{ $inv['addcost'] }}" step=".01">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Due Date</div>
									</div>
									<div class="col-lg-5 col-xs-8">
										<input type="date" name="duedate" id="duedate" class="form-control" value="{{ $inv['duedate'] }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					@endif
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Resubmit for Approval</button>
							</div>
						</div>
					</div>

					</form>


					<div class="panel panel-default">
						<div class="panel-heading">							
							<h4>Comments</h4>								
						</div>

						<div class="panel-body">

							<div class="nest-new-comments-wrapper">
								<form action="/commission/editinvoicecomment" id="invoicecomment" method="POST">
									{{ csrf_field() }}
									<textarea id="comments" name="comments" class="form-control" rows="3"></textarea>
									<input type="hidden" name="id" value="{{ $inv['id']}}">
									<button class="nest-button nest-right-button btn btn-default">Add Comment</button>
								</form>
								<div class="clearfix"></div>
							</div>

							@foreach($comments as $comment)
							<div class="media media-comment">
									<div class="media-left">								
									<img src="/{{$comment['user_pic']}}" class="img-circle media-object" width="60px" style="border:1px solid #cccccc;" />
									</div>
									<div class="media-body message text-left">
										<u>{{$comment['user_name']}}</u> <small class="text-muted">{{$comment['date']}}</small>								
										<br />
										{{$comment['comment']}}
									</div>
							</div>
							<div class="clearfix"></div>
							@endforeach

						</div>

					</div>


				</div>
			</form>
			<div class="clearfix"></div>
			<br /><br /><br /><br /><br />
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});

	jQuery('#invoicecomment').on('submit', function (e) {
		if(jQuery('#comments').val() == ''){
			
			e.preventDefault();
		}
	});
	jQuery('#addressformat').on('click',function (e) {
		
		var text  = jQuery("#selleraddress").val();				
		var newtext = text.replace(/\s*,\s*/g, ',\n'); // Or \n, depending on your needs
		jQuery("#selleraddress").val(newtext);	
	});

	jQuery('#buyeraddressformat').click(function (e) {
		
		var text  = jQuery("#buyeraddress").val();			
		var newtext = text.replace(/\s*,\s*/g, ',\n'); // Or \n, depending on your needs		 	
		jQuery("#buyeraddress").val(newtext);	
	});


	jQuery('.lease_term').on('change',function (e) {
		var val = 	jQuery(this).val();
		if(val >= 3){
			jQuery('.month_break_clause').show();
		} else {
			jQuery('.month_break_clause').hide();
		}
	});


	jQuery('.editinvoice').on('submit',function (e) {

	if(jQuery('.lease_term').val() == ""){
		e.preventDefault();
		alert("Please select Lease Term");
	} else if(jQuery('#start_date').val() == ""){
		e.preventDefault();
		alert("Please select Start Date");
	}

	});


	
	
		
</script>
@endsection
