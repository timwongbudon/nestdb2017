@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4 style="margin-bottom:0px;">Salaries CSV Download</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">	
				<div class="nest-buildings-result-wrapper">	
					@for ($year = $curyear; $year > 2018; $year--)
						@if ($year < $curyear)
							@php
								$curmonth = 12;
							@endphp
						@endif
						
						@for ($month = $curmonth; $month > 0; $month--)
							<div class="row 
							@if ($month%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-sm-6">
									<b>{{ $months[$month] }} {{ $year }}</b>
								</div>
								<div class="col-sm-6">
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/csvsalarydownload/'.$year.'/'.$month) }}">
										<i class="fa fa-btn fa-pencil-square-o"></i>Download
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
							
						@endfor
					@endfor
					
					
						
					
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
