
@if (count($comments) == 0)
	No comments found
@else
	@foreach ($comments as $comment)
		<div class="media media-comment">
			<div class="media-left">
				<img src="{{ url($comment['pic']) }}" class="img-circle media-object" width="60px" style="border:1px solid #cccccc;" />
			</div>
			<div class="media-body message text-left">
				<u>{{ $comment['name'] }}</u> <small class="text-muted">{{ $comment['datum'] }}</small>
				@if ($user->id == $comment['userid'])
					<a href="javascript:;" class="nest-comment-remove" onClick="removeComment({{ $comment['datetime'] }});"><i class="fa fa-trash"></i></a>
				@endif
				<br />
				{{ $comment['content'] }}
			</div>
		</div>
		<div class="clearfix"></div>
	@endforeach
@endif