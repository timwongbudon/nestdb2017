@extends('layouts.app')

@section('content')

	<div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
			<a class="nest-button btn btn-primary" href="{{ url('commission/createbonusdividend') }}">
				<i class="fa fa-btn fa-plus"></i> Create Bonus/Dividend
			</a>
		</div>
	</div>
	
	
	<!-- Current Clients -->
	@if (count($records) > 0)
		<div>
			<div class="nest-buildings-result-wrapper">
				@foreach ($records as $index => $rec)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b> @if ($rec->type == 1) Dividend @else Bonus @endif No.: </b> #{{ $rec->id }}<br />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Name:</b> {{ $rec->user->name }}<br />
							<b>Amount:</b> HK$ {{ number_format(($rec->netincome), 2) }}<br />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<b>Payment Date:</b> {{ NestDate::nest_date_format($rec->paymentdate, 'Y-m-d') }}<br />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/editbonusdividend/'.$rec->id) }}">
								<i class="fa fa-btn fa-pencil-square-o"></i> Edit
							</a>
							<a class="nest-button nest-right-button btn btn-primary" href="{{ url('commission/bonusdividendpdf/'.$rec->id) }}" target="_blank">
								<i class="fa fa-btn fa-pencil-square-o"></i> Download
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endforeach
			</div>
			@if ($records->lastPage() > 1)
				<div class="panel-footer nest-buildings-pagination">
					{{ $records->links() }}
				</div>
			@endif
		</div>
	@endif
	<script>
		jQuery('form').on('focus', 'input[type=number]', function (e) {
			jQuery(this).on('mousewheel.disableScroll', function (e) {
				e.preventDefault();
			});
		});
		jQuery('form').on('blur', 'input[type=number]', function (e) {
			jQuery(this).off('mousewheel.disableScroll');
		});
	</script>
@endsection
