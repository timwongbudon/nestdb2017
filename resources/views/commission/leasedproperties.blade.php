@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>
	

 	$( function() {
    	var dateFormat = "mm/dd/yy",
      from = $( "#property-available_date_from" )
        .datepicker({
         // defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#property-available_date_to" ).datepicker({
        //defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
		function getDate( element ) {
		var date;
		try {
			date = $.datepicker.parseDate( dateFormat, element.value );
		} catch( error ) {
			date = null;
		}
	
		return date;
		}



	} );
    
</script>

	<div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
			<h2>Property Available Date</h2>
		</div>
	</div>

    <div class="nest-buildings-search-panel panel panel-default">
		<div class="panel-body">
		
			
		<form action="" method="get">


			<div class="row">
				<div class="col-sm-3">
					<div class="form-group">
						<div class="nest-new ">							
							
								<label for="show_type_1">Expiration</label>
								<select name="filter" style="padding: 5px 8px;">
									<option value=""> Please Select</option>
									<option value="0" <?php echo isset($filter) && $filter == 0 ?  "selected" : ''; ?> >6 months before expiry</option>
									<option value="1" <?php echo isset($filter) && $filter == 1 ?  "selected" : ''; ?> >3 months before expiry</option>
									<option value="2" <?php echo isset($filter) && $filter == 2 ?  "selected" : ''; ?> >Expired</option>
								</select>

						</div>
					</div>
				</div>

				
			</div>


			<div class="row">
				<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
						<input type="text" name="available_date_from" placeholder="Expiration Date From" autocomplete="off" id="property-available_date_from" class="form-control datepicker" value="{{$from}}" min="0">                                  
				</div>

				<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">                  
						<input type="text" name="available_date_to" placeholder="Expiration Date To" autocomplete="off" id="property-available_date_to" class="form-control datepicker"value="{{$to}}"  min="0">
					
				</div>

				<div class="clearfix"></div>

				<div>
					<input type="submit" class="nest-button btn btn-primary" value="Filter">
					<a href="{{url('/commission/propertyavailabledate')}}" class="nest-button btn btn-primary" >Reset</a>
				</div>
			</div>

			</form>

			
		</div>
	</div>


    <div>
			<div class="nest-buildings-result-wrapper">

			<div class="row 
				
						nest-striped
				
					">
						<div class="col-lg-3 col-md-2 col-sm-6">
						<b>Client</b>
						</div>
						<div class="col-lg-3 col-md-2 col-sm-6">
						<b>Property</b>
						</div>      
                        
                        <div class="col-lg-3 col-md-2 col-sm-6">
						<b>Expiration</b>
						</div>
						
												
						<div class="col-lg-2 col-md-2 col-sm-6">
						<b>	Action</b>
						</div>
						<div class="clearfix"></div>
					</div>


				@foreach ($list as $index => $inv)
					<div class="row 
					@if ($index%2 == 1)
						nest-striped
					@endif
					">
						<div class="col-lg-3 col-md-2 col-sm-6">
                            {{ $inv->buyername }} 							
						</div>                      
                      
						<div class="col-lg-3 col-md-2 col-sm-6">						
						    <a href="{{url('property/edit/'.$inv->propertyid)}}">{{ $inv->name }}</a>
						</div>	 
                        
                        <div class="col-lg-3 col-md-2 col-sm-6">
                            {{ $inv->expiration_date }} 							
						</div>                    


                        <div class="col-lg-2 col-md-2 col-sm-6">                        
                           <a href="/commission/invoiceedit/{{$inv->commissionid}}" class="nest-button btn btn-primary">View Invoicing</a>								
						</div>	

                 	
						<div class="clearfix"></div>
					</div>
				@endforeach

				<div class="nest-new ">
			
				</div>
			</div>
			
        </div>



@endsection