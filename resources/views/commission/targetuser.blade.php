@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4 style="margin-bottom:0px;">Targets {{ $user->name }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">	
				<div class="nest-buildings-result-wrapper">	
					@php
						$indexplus = 0;
					@endphp
					@if ($highestyear != $curyear)					
						<div class="row">
							<div class="col-xs-6">
								<b>{{ $curyear }}</b>
							</div>
							<div class="col-xs-6">
								<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/edittargets/'.$user->id.'/'.$curyear) }}">
									<i class="fa fa-btn fa-plus"></i>Create
								</a>
							</div>
							<div class="clearfix"></div>
						</div>	
						@php
							$indexplus = 1;
						@endphp
					@endif			
					@if (count($targets) > 0)
						@foreach ($targets as $index => $target)
							<div class="row 
								@if (($indexplus + $index)%2 == 1)
									nest-striped
								@endif
								">
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>{{ $target->year }}</b>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Lease Target:</b> HK$ {{ number_format($target->currentlease, 2, '.', ',') }} / month<br />
										<b>Lease Deals:</b> {{ $target->currentleasedeals }} / month</b> 
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Sale Target:</b> HK$ {{ number_format($target->currentsale, 2, '.', ',') }} / year<br />
										<b>Sale Deals:</b> {{ $target->currentsaledeals }} / year</b> 
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/commission/edittargets/'.$user->id.'/'.$target->year) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
						@endforeach
					@endif	
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
