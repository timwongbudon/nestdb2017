@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Basic Information</h4>
						</div>
						
						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')
							
							{{ csrf_field() }}
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Invoice Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="invoicedate" id="invoicedate" class="form-control" value="{{ $inv['invoicedate'] }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										Name
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="buyername" id="buyername" class="form-control">{{ $inv['buyername'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">
										Address
									</div>
								</div>
								<div class="col-xs-8">
									<textarea name="buyeraddress" id="buyeraddress" class="form-control">{{ $inv['buyeraddress'] }}</textarea>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Text & Amounts</h4>
						</div>
						
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Line 1</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="feecalculation" id="feecalculation" class="form-control" value="{{ $inv['feecalculation'] }}" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Amount 1</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="price" id="price" class="form-control" value="{{ $inv['price'] }}" step=".01">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Line 2</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="addcosttext" id="addcosttext" class="form-control" value="{{ $inv['addcosttext'] }}" />
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Amount 2</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="number" name="addcost" id="addcost" class="form-control" value="{{ $inv['addcost'] }}" step=".01">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									<div class="nest-property-edit-label">Due Date</div>
								</div>
								<div class="col-lg-5 col-xs-8">
									<input type="date" name="duedate" id="duedate" class="form-control" value="{{ $inv['duedate'] }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;padding-top:0px;">
							<div class="col-xs-12" style="margin-top:10px;">
								<button class="nest-button nest-right-button btn btn-default" type="submit">Create</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="clearfix"></div>
			<br /><br /><br /><br /><br />
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	
		
</script>
@endsection
