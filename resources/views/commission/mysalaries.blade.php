@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 style="margin-bottom:0px;">Salaries {{ $user->name }}</h4>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">	
				<div class="nest-buildings-result-wrapper">	
					@if (count($invoiceoutput) > 0)
						<p><b><u>Commission Payable {{ $months[$month] }} {{ $year }}</u></b></p>
						@foreach ($invoiceoutput as $index => $io)
							<div class="row 
							@if ($index%2 == 0)
								nest-striped
							@endif
							">
								<div class="col-lg-5 col-md-5 col-sm-12">
									<b>Property:</b> <a href="javascript:;"  data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $io['invoice']->propertyid }}, '{{ str_replace("'", "\'", $io['invoice']->property->name) }}', '')">{{ $io['invoice']->property->name }}</a><br />
									<b>Client:</b> <a href="{{ url('client/show/'.$io['invoice']->client->id) }}" target="_blank">{{ $io['invoice']->client->name() }}</a>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<b>Invoice ID:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->id }}</a><br />
									<b>Invoice No.:</b> <a href="/commission/invoiceedit/{{ $io['invoice']->id }}" target="_blank">{{ $io['invoice']->invoicenumbers }}</a><br />
									<b>Who Paid:</b> {{ $io['type'] }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 text-right">
									HK$ {{ number_format($io['amount'], 2) }}
								</div>
							<div class="clearfix"></div>
						</div>
						@endforeach
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12"></div>
							<div class="col-lg-4 col-md-4 col-sm-6"></div>
							<div class="col-lg-3 col-md-3 col-sm-6 text-right">
								<b>HK$ {{ number_format($commissionfees, 2) }}</b>
								<input type="hidden" name="commissionfees" id="commissionfees" value="{{ $commissionfees }}" />
							</div>
							<div class="clearfix"></div>
						</div>
						<br />
						
						<p><b><u>Past Months</u></b></p>
					@endif
					
					@foreach ($salaries as $index => $salary)
						<div class="row 
						@if ($index%2 == 0)
							nest-striped
						@endif
						">
							<div class="col-lg-4 col-md-4 col-sm-6">
								<b>{{ $months[$salary->month] }} {{ $salary->year }}</b>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6">
								<b>Net Amount (excl. MPF):</b> HK$ {{ number_format($salary->fixedfees+$salary->commissionfees+$salary->otherfees-$salary->mpfemployee-$salary->otherdeductions, 2) }}
								<br /><b>Payment Date:</b> {{ \NestDate::nest_date_format($salary->paymentdate, 'Y-m-d') }}
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6">
								<a href="{{ url('/commission/pdfcommissionlist/'.$user->id.'/'.$salary->year.'/'.$salary->month) }}" class="nest-button nest-right-button btn btn-default" target="_blank">Commission Breakdown</a>
								<a href="{{ url('/commission/pdfsalary/'.$user->id.'/'.$salary->year.'/'.$salary->month) }}" class="nest-button nest-right-button btn btn-default" target="_blank">Salary PDF</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
