@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4 style="margin-bottom:0px;">Expenses {{ $user->name }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">	
				<div class="nest-buildings-result-wrapper">	
					@foreach ($expenses as $index => $expense)
						<div class="row 
						@if ($index%2 == 0)
							nest-striped
						@endif
						">
							<div class="col-lg-4 col-md-4 col-sm-6">
								<b>{{ $months[$expense->month] }} {{ $expense->year }}</b>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6">
								<b>Amount:</b> HK$ {{ number_format($expense->amount, 2) }}
								@if (!empty($expense->paymentdate))
									<br /><b>Payment Date:</b> {{ \NestDate::nest_date_format($expense->paymentdate, 'Y-m-d') }}
								@endif
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6">
								<a href="{{ url('/commission/pdfexpenses/'.$user->id.'/'.$expense->year.'/'.$expense->month) }}" class="nest-button nest-right-button btn btn-default" target="_blank">Expenses PDF</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endforeach
					
					
						
					
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
