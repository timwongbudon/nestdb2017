@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>Edit Targets {{ $user->name }} for {{ $year }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
    </div>
	<div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="" method="POST" class="form-horizontal">
				{{ csrf_field() }}
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="currentlease-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Monthly Lease Commission</div>
								</div>
								<div class="col-xs-6">
									<input type="number" step="1" name="currentlease" id="currentlease" class="form-control" value="{{ old('currentlease', $target->currentlease) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<script>
						jQuery('#currentlease').on('change', function(){
							var val = jQuery('#currentlease').val();
							var curmonth = {{ date('m') }};
							
							for (var i = curmonth; i <= 11; i++){
								jQuery('#monthlylease'+i).val(val);
							}
						});
					</script>
					<div class="panel panel-default">
						<div class="panel-body">
							@foreach ($months as $index => $month)
								<div id="monthlylease{{ $index }}-section" class="nest-property-edit-row">
									<div class="col-xs-6">
										<div class="nest-property-edit-label">{{ $month }}</div>
									</div>
									<div class="col-xs-6">
										<input type="number" step="1" name="monthlylease{{ $index }}" id="monthlylease{{ $index }}" class="form-control" value="{{ $monthlylease[$index] }}" min="0"> 
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="currentleasedeals-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Monthly Lease Deals</div>
								</div>
								<div class="col-xs-6">
									<input type="number" step="1" name="currentleasedeals" id="currentleasedeals" class="form-control" value="{{ old('currentleasedeals', $target->currentleasedeals) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<script>
						jQuery('#currentleasedeals').on('change', function(){
							var val = jQuery('#currentleasedeals').val();
							var curmonth = {{ date('m') }};
							
							for (var i = curmonth; i <= 11; i++){
								jQuery('#monthlyleasedeals'+i).val(val);
							}
						});
					</script>
					<div class="panel panel-default">
						<div class="panel-body">
							@foreach ($months as $index => $month)
								<div id="monthlyleasedeals{{ $index }}-section" class="nest-property-edit-row">
									<div class="col-xs-6">
										<div class="nest-property-edit-label">{{ $month }}</div>
									</div>
									<div class="col-xs-6">
										<input type="number" step="1" name="monthlyleasedeals{{ $index }}" id="monthlyleasedeals{{ $index }}" class="form-control" value="{{ $monthlyleasedeals[$index] }}" min="0"> 
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="currentsale-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Yearly Sales Commission</div>
								</div>
								<div class="col-xs-6">
									<input type="number" step="1" name="currentsale" id="currentsale" class="form-control" value="{{ old('currentsale', $target->currentsale) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
							<div id="currentsaledeals-section" class="nest-property-edit-row">
								<div class="col-xs-6">
									<div class="nest-property-edit-label">Yearly Sales Deals</div>
								</div>
								<div class="col-xs-6">
									<input type="number" step="1" name="currentsaledeals" id="currentsaledeals" class="form-control" value="{{ old('currentsaledeals', $target->currentsaledeals) }}" min="0"> 
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-xs-12">
								<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection




