<html>
<head>

<title>Invoice</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 3.2cm 1.5cm 0.5cm 1.5cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-sl-top-left{
	padding-top:0px;
    padding-bottom:0px;
    margin-top: -30px;
    z-index: 1000;
    display: block;
}
.nest-invoice-main-table{
	padding-top:10px;
    /* height:480px; */    
    margin-bottom: 20px;
}
.nest-invoice-top-center{
	font-size:22px;
	text-align:center;
	color:#222222;
	line-height:1;
	font-family: MyriadBold;
	padding-top:5px;
}
.nest-invoice-nest-address{
	text-align:center;
	color:#222222;
	font-family: MyriadBold;
	font-size:16px;
	padding-bottom:10px;
	border-bottom:1px dashed #000000;
}
.nest-invoice-thankyou{
	text-align:center;
	color:#222222;
	font-family: MyriadBold;
	font-size:20px;
	padding-top:10px;
	padding-bottom:10px;
}


#header,
#footer{
	position: fixed;
	left: 0;
	right: 0;
	color: #000000;
	font-size: 75%;
}

#header {
	top: -75px;
}

.nest-hide-first-header{
    background: #fff;
	z-index:10;
	text-align:right;
    padding-bottom: 15px; 
    margin-top: -120px;
}
.nest-hide-first-header img{
	width:160px;
}
#footer {
	bottom: 0;
	border-top: 0.1pt solid #000000;
	padding-top:0;
}
#footer{
    text-align: center;
    line-height: 0px;
    width: 100%;
    height: 10px;
    margin: 0 -15cm;
    margin-bottom: -3px !important;
}
#footer a {
    text-decoration: none;
    color: black;
    padding-top: 17px;
    display: block;
	background-color:#C5C2C0 !important;
	width: inherit;
	height: inherit;
}
.page-number {
	text-align: right;
	color: black; 
	position: absolute;
	display: block;
	width: 70%;
	height: 10px;
	top: 17px;
}
.page-number:before {
  content: counter(page);
  margin-right: 20px;
  font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
}
.nest-pdf-container {
    position: relative;
    z-index: 100;
}

</style>


</head>
<body>
    <div id="header" style="padding-top:0px;">
        <div style="text-align:right;">
            <img src="{{ url('/documents/images/nest-letterhead-logo-header.png') }}" style="width:90px;" />
        </div>
    </div>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>
    <div class="nest-hide-first-header">
        <div style="width: 49%; display: inline-block; text-align: left !important">
            <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
        </div>
        <div style="width: 50%; display: inline-block; background: #fff">
            <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
        </div>
	</div>
	<div class="nest-pdf-container">
        <div class="nest-pdf-sl-top-left">
            {{ \NestDate::nest_contract_datetime_format($inv->invoicedate) }} <br/><br>
            @if ($inv->whofor == 1)
                {{ $inv->sellername }}<br />
                {!! nl2br($inv->selleraddress) !!}<br />
            @else
                {{ $inv->buyername }}<br />
                {{ $inv->work_place }}
                @if($inv->work_place)
                    <br/>
                @endif
                {!! nl2br($inv->buyeraddress) !!}<br />
            @endif
            <div style="clear:both;"></div>
        </div>
		<div class="nest-invoice-top-center">
			INVOICE: NO.{{ ($inv->id) }}<br />
			Attention: 
			@if ($inv->whofor == 1)
				{{ $inv->sellername }}
			@else
				{{ $inv->buyername }}
			@endif
		</div>
		<div class="nest-invoice-main-table">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr style="">
					<td width="75%" style="background:#ffffff;color:#222222;font-family: MyriadBold;padding:8px 12px;font-size:17px;border-bottom:2px solid #222222;">Description</td>
					<td width="25%" style="background:#ffffff;color:#222222;font-family: MyriadBold;text-align:right;padding:5px 10px;font-size:17px;border-bottom:2px solid #222222;">Amount HK$</td>
				</tr>
				<tr>
					<td style="color:#222222;font-family: MyriadBold;padding:10px 12px 6px;font-size:15px;">
						@if ($inv->invtype == 1)
							Lease Information:
						@else
							Sale and Purchase Information:
						@endif
					</td>
					<td></td>
				</tr>
				<tr>
					<td style="padding:3px 12px 20px;">
						<table cellpadding="0" border="0" cellspacing="0">
							<tr>
								<td valign="top" style="padding-bottom:3px;">Address:&nbsp;&nbsp;&nbsp;</td>
								<td style="padding-bottom:3px;">{{ $inv->propertyaddress }}</td>
							</tr>
							<tr>
								<td valign="top" style="padding-bottom:3px;">
									@if ($inv->invtype == 1)
										Landlord:&nbsp;&nbsp;&nbsp;
									@else
										Vendor:&nbsp;&nbsp;&nbsp;
									@endif
								</td>
								<td style="padding-bottom:3px;">{{ $inv->sellername }}</td>
							</tr>
							<tr>
								<td valign="top" style="padding-bottom:3px;">
									@if ($inv->invtype == 1)
										Tenant:&nbsp;&nbsp;&nbsp;
									@else
										Purchaser:&nbsp;&nbsp;&nbsp;
									@endif
								</td>
								<td style="padding-bottom:3px;">{{ $inv->buyername }}</td>
							</tr>
							<tr>
								<td valign="top" style="padding-bottom:3px;">
									@if ($inv->invtype == 1)
										Rental:&nbsp;&nbsp;&nbsp;
									@else
										Purchase Price:&nbsp;&nbsp;&nbsp;
									@endif
								</td>
								<td style="padding-bottom:3px;">{{ $inv->purchasetext }}</td>
							</tr>
							<tr>
								<td valign="top" style="padding-bottom:3px;">
									@if ($inv->invtype == 1)
										Term:&nbsp;&nbsp;&nbsp;
									@else
										Completion Date:&nbsp;&nbsp;&nbsp;
									@endif
								</td>
								<td style="padding-bottom:3px;">{!! nl2br($inv->terms) !!}</td>
							</tr>
						</table>
					</td>
					<td></td>
				</tr>
				<tr>
					<td style="padding-bottom:20px;padding-left:12px;">
						<div style="color:#222222;font-family: MyriadBold;padding-bottom:6px;font-size:15px;">Agency Fee Calculation Details:</div>
						{{ $inv->feecalculation }}
					</td>
					<td style="text-align:right;padding-bottom:20px;padding-right:12px;" valign="bottom">{{ number_format($inv->price, 2) }}</td>
				</tr>
				@if ($inv->addcost != 0)
					<tr>
						<td style="padding-bottom:20px;padding-left:12px;">
							<div style="color:#222222;font-family: MyriadBold;padding-bottom:6px;font-size:15px;">Additional Costs:</div>
							{{ $inv->addcosttext }}
						</td>
						<td style="text-align:right;padding-bottom:20px;padding-right:12px;" valign="bottom">{{ number_format($inv->addcost, 2) }}</td>
					</tr>
				@endif
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td style="text-align:right;color:#222222;font-family: MyriadBold;font-size:17px;border-top:2px solid #222222;">
						Total Due:&nbsp;&nbsp;
					</td>
					<td style="text-align:right;background:#ffffff;color:#222222;font-family: MyriadBold;font-size:17px;padding:5px 12px;border-top:2px solid #222222;" valign="bottom">{{ number_format(($inv->price+$inv->addcost), 2) }}</td>
				</tr>
			</table>
        </div>
		<div class="nest-invoice-nest-address">
			Suite 1301, Hollywood Centre, No.233 Hollywood Road, Sheung Wan, Hong Kong
		</div>
		<div class="nest-invoice-thankyou">
			Thank you for using Nest Property Limited!
		</div>
		<div class="nest-invoice-table-bottom">
			<table cellpadding="0" cellspacing="0" style="border:1px solid #000000;" width="100%">
				<tr>
					<td rowspan="3" width="37%">
						<div style="font-size:12px;padding:12px;padding-rigth:5px;">
							<div style="color:#222222;font-family: MyriadBold;">Remittance Advice</div>
							<br />
							Please post this advice slip together with a cheque to our office address below:<br />
							<br />
							Nest Property Limited<br />
							Suite 1301, Hollywood Centre,<br />
							233 Hollywood Road,<br />
							Sheung Wan,<br />
							Hong Kong
						</div>
					</td>
					<td rowspan="3" width="42%" style="border-left:1px solid #000000;border-right:1px solid #000000;">
						<div style="font-size:12px;padding:12px;">
							<div style="color:#222222;font-family: MyriadBold;">Electronic Bank Transfer Advice</div>
							<br />
							Please use the below bank details together with the invoice number for our reference:<br />
							<br />
							<span style="display:inline-block;width:92px;">Email:</span>accounts@nest-property.com<br />
							<span style="display:inline-block;width:92px;">Account Name:</span>Nest Property Limited<br />
							<span style="display:inline-block;width:92px;">Account No:</span>848-120929-001<br />
							<span style="display:inline-block;width:92px;">Bank:</span>HSBC<br />
							<span style="display:inline-block;width:92px;">Swift Code:</span>HSBC HKH HHKH
						</div>
					
					</td>
					<td width="21%" style="color:#222222;font-family: MyriadBold;font-size:14px;text-align:center;border-bottom:1px solid #000000;padding-bottom:3px;">
						Invoice No: {{ ($inv->id) }}
					</td>
				</tr>
				<tr>
					<td style="color:#222222;font-family: MyriadBold;font-size:14px;text-align:center;border-bottom:1px solid #000000;padding-bottom:3px;">
						Due Date:<br />
						{{ \NestDate::nest_contract_datetime_format($inv->duedate) }}
					</td>
				</tr>
				<tr>
					<td style="color:#222222;font-family: MyriadBold;font-size:14px;text-align:center;padding-bottom:3px;">
						Total Due:<br />
						HK${{ number_format(($inv->price+$inv->addcost), 2) }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>