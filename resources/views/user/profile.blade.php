@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>User Profile</h4>
				</div>
			</div>
			<div class="panel-body">					
				@if (!empty($saved) && $saved)
					<div class="col-xs-12">
						<div class="nest-message-banner bg-success">
							The changes have been saved successfully!
						</div>
					</div>
				@endif
				
				<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
					{{ csrf_field() }}
					
					<!-- Picture -->
					<div class="col-md-4">
						<div class="col-xs-12">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label class="col-xs-12 control-label text-left">Picture</label>
								<div class="nest-profile-picture-wrapper">
									@if ($user->picpath == "")
										<img src="{{ url('/showimage/users/dummy.jpg') }}" />
									@else
										<img src="{{ url($user->picpath) }}" />
									@endif
								</div>
								
							</div>
							<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
								<label for="picture" class="col-xs-12 control-label text-left">New picture (square - e.g. 600x600)</label>

								<div class="col-xs-12">
									{!! Form::file('picture', ['class'=>'form-control']) !!}

									@if ($errors->has('picture'))
										<span class="help-block">{{ $errors->first('picture') }}</span>
									@endif
								</div>
							</div>
							
						</div>
					</div>
					
					<!-- Information -->
					<div class="col-md-8">
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-xs-12 control-label text-left">Name (for internal use)</label>

								<div class="col-xs-12">
									{!! Form::text('name', $user->name, ['class'=>'form-control']) !!}

									@if ($errors->has('name'))
										<span class="help-block">{{ $errors->first('name') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
								<label for="firstname" class="col-xs-12 control-label text-left">First name (for documents)</label>

								<div class="col-xs-12">
									{!! Form::text('firstname', $user->firstname, ['class'=>'form-control']) !!}

									@if ($errors->has('firstname'))
										<span class="help-block">{{ $errors->first('firstname') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
								<label for="lastname" class="col-xs-12 control-label text-left">Last name (for documents)</label>

								<div class="col-xs-12">
									{!! Form::text('lastname', $user->lastname, ['class'=>'form-control']) !!}

									@if ($errors->has('lastname'))
										<span class="help-block">{{ $errors->first('lastname') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('hkid') ? ' has-error' : '' }}">
								<label for="hkid" class="col-xs-12 control-label text-left">HKID No.</label>

								<div class="col-xs-12">
									{!! Form::text('hkid', $user->hkid, ['class'=>'form-control']) !!}

									@if ($errors->has('hkid'))
										<span class="help-block">{{ $errors->first('hkid') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('expensifyid') ? ' has-error' : '' }}">
								<label for="expensifyid" class="col-xs-12 control-label text-left">Expensify ID No.</label>

								<div class="col-xs-12">
									{!! Form::text('expensifyid', $user->expensifyid, ['class'=>'form-control']) !!}

									@if ($errors->has('expensifyid'))
										<span class="help-block">{{ $errors->first('expensifyid') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
								<label for="gender" class="col-xs-12 control-label text-left">Gender</label>

								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-4">
											<input type="radio" name="gender" value="1" {{ $user->gender == 1 ? "checked " : "" }}/>&nbsp;female
										</div>
										<div class="col-xs-4">
											<input type="radio" name="gender" value="2" {{ $user->gender == 2 ? "checked " : "" }}/>&nbsp;male
										</div>
										<div class="col-xs-4">
											<input type="radio" name="gender" value="0" {{ $user->gender == 0 ? "checked " : "" }}/>&nbsp;N/A
										</div>
									</div>

									@if ($errors->has('gender'))
										<span class="help-block">{{ $errors->first('gender') }}</span>
									@endif
								</div>
							</div>

							<div class="form-group">	
								@if(!is_null($user->vacation_tie) && $user->vacation_tier !='Freelancer')
									@if(!is_null( $user->allowed_vacation))
										<div class="col-xs-12">
											Allowed Vacation:<strong>{!!  $user->allowed_vacation!!} days</strong>
										</div>
									@endif

								@endif
								
									<div class="col-xs-12">
									Total number of days used: <strong>{{$vacation_used}} days</strong><br>
									@if($user->allowed_vacation != 0 || $user->allowed_vacation != null)
										Remaining days left: <strong>{{$user->allowed_vacation - $vacation_used}} days</strong>
										@endif
									</div>
								
							</div>



						</div>
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email" class="col-xs-12 control-label text-left">Email (not editable)</label>

								<div class="col-xs-12">
									{!! Form::text('email', $user->email, ['class'=>'form-control', 'readonly']) !!}

									@if ($errors->has('email'))
										<span class="help-block">{{ $errors->first('email') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
								<label for="phone" class="col-xs-12 control-label text-left">Phone number (e.g. 1234 5678)</label>

								<div class="col-xs-12">
									{!! Form::text('phone', $user->phone, ['class'=>'form-control']) !!}

									@if ($errors->has('phone'))
										<span class="help-block">{{ $errors->first('phone') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('agentid') ? ' has-error' : '' }}">
								<label for="agentid" class="col-xs-12 control-label text-left">License Number (x-xxxxxx)</label>

								<div class="col-xs-12">
									{!! Form::text('agentid', $user->agentid, ['class'=>'form-control']) !!}

									@if ($errors->has('agentid'))
										<span class="help-block">{{ $errors->first('agentid') }}</span>
									@endif
								</div>
							</div>
							@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
								<div class="form-group{{ $errors->has('offtitle') ? ' has-error' : '' }}">
									<label for="offtitle" class="col-xs-12 control-label text-left">Title (e.g. Senior Consultant)</label>

									<div class="col-xs-12">
										{!! Form::text('offtitle', $user->offtitle, ['class'=>'form-control']) !!}

										@if ($errors->has('offtitle'))
											<span class="help-block">{{ $errors->first('offtitle') }}</span>
										@endif
									</div>
								</div>
							@else
								<input type="hidden" value="{{ $user->offtitle }}" name="offtitle" />
							@endif
							<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
								<label for="address" class="col-xs-12 control-label text-left">Address</label>

								<div class="col-xs-12">
									{!! Form::textarea('address', $user->address, ['class'=>'form-control', 'rows' => 3]) !!}

									@if ($errors->has('address'))
										<span class="help-block">{{ $errors->first('address') }}</span>
									@endif
								</div>
							</div>
							@if ($user->sigpath != "")
								<div class="user-profile-signature">
									<img src="{{ url($user->sigpath) }}" />
								</div>
							@endif
							<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
								<label for="picture" class="col-xs-12 control-label text-left">New Signature</label>

								<div class="col-xs-12">
									{!! Form::file('signature', ['class'=>'form-control']) !!}

									@if ($errors->has('signature'))
										<span class="help-block">{{ $errors->first('signature') }}</span>
									@endif
								</div>
							</div>
							
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-xs-12 text-center nest-form-button-wrapper" style="padding-right:15px;">
						<button class="nest-button nest-right-button btn btn-default" type="submit">Save</button>
					</div>
					
				</form>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>Email Signature</h4>
				</div>
			</div>

			@php
			if($user->id == 4){
				$whatsApp = "https://api.whatsapp.com/send?phone=+447854500679";
			} else {
				$whatsApp = "https://api.whatsapp.com/send?phone=".str_replace(' ', '', $user->phone);
			}
			@endphp
			<div class="panel-body">		
				<div class="col-xs-12">
					A tutorial on how to set up signatures for Mac can be found <a href="https://www.nicereply.com/blog/create-html-signature-in-apple-mail/" target="_blank">here</a>
					<br /><br />


					Option 1:
					<textarea class="form-control" rows="20">
<body>
	<div style="font-size:11px;">
		<b>{{ $user->firstname }} {{ $user->lastname }}</b><br />
		{{ $user->offtitle }} | Nest Property<br />
		Residential Search & Investment<br />
		<br />
		Suite 1301 | Hollywood Centre | 233 Hollywood Road | Sheung Wan<br />
		<b>T</b> | <a href="tel:+85236897523">(+852) 3689 7523</a> &amp;nbsp; <b>M</b> | <a href="tel:{{ str_replace(' ', '', $user->phone) }}">{{ $user->phone }}</a> &amp;nbsp; <b>W</b> |  <a href="https://www.nest-property.com" target="_blank">www.nest-property.com</a><br /><br />
	</div>	
	<table style="max-width:100%;" width="650" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="650px" style="background:#ffffff;" valign="top">
				<a href="https://www.nest-property.com" target="_blank"><img src="https://www.nest-property.com/signatures/1_{{ $user->firstname }}_{{ $user->lastname }}.png" style="width:650px;" /></a>
			</td>			
		</tr>		
	</table>		
	<br style="clear:both" />
	<div style="font-size:10px;">
		A standard agency fee of fifty percent (50%) of one month's total rental is payable to Nest Property by both the landlord and the tenant upon signing of a tenancy agreement. The agency fee payable to Nest Property on a sales transaction is 1% of the total purchase price payable by both the vendor and the purchaser on completion.
		<br /><br />
		Nest Property has prepared these property particulars as a guide only. They are not intended to constitute part of an offer or contract. Information relating to properties (including but not limited to price, management fees, government rates, general property descriptions, property area, address and floor area) are subject to change without notice and should not be relied upon. All details should be confirmed by your solicitor prior to entering into any formal agreement. EA License C-048625. Nest Property, Suite 1301, Hollywood Centre, 233 Hollywood Road, Sheung Wan. 
	</div>
	<br /><br /><br /><br />
</body>

					</textarea>
					<br />
					Option 2:
					<textarea class="form-control" rows="20">
<body>
	<div style="font-size:11px;">
		<b>{{ $user->firstname }} {{ $user->lastname }}</b><br />
		{{ $user->offtitle }} | Nest Property<br />
		Residential Search & Investment<br />
		<br />
		Suite 1301 | Hollywood Centre | 233 Hollywood Road | Sheung Wan<br />
		<b>T</b> | <a href="tel:+85236897523">(+852) 3689 7523</a> &amp;nbsp; <b>M</b> | <a href="tel:{{ str_replace(' ', '', $user->phone) }}">{{ $user->phone }}</a> &amp;nbsp; <b>W</b> |  <a href="https://www.nest-property.com" target="_blank">www.nest-property.com</a><br /><br />
	</div>	
	<table style="max-width:100%;" width="650" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="650px" style="background:#ffffff;" valign="top">
				<a href="https://www.nest-property.com" target="_blank"><img src="https://www.nest-property.com/signatures/2_{{ $user->firstname }}_{{ $user->lastname }}.png" style="width:650px;" /></a>
			</td>
		</tr>		
	</table>				
	<br style="clear:both" />
	<div style="font-size:10px;">
		A standard agency fee of fifty percent (50%) of one month's total rental is payable to Nest Property by both the landlord and the tenant upon signing of a tenancy agreement. The agency fee payable to Nest Property on a sales transaction is 1% of the total purchase price payable by both the vendor and the purchaser on completion.
		<br /><br />
		Nest Property has prepared these property particulars as a guide only. They are not intended to constitute part of an offer or contract. Information relating to properties (including but not limited to price, management fees, government rates, general property descriptions, property area, address and floor area) are subject to change without notice and should not be relied upon. All details should be confirmed by your solicitor prior to entering into any formal agreement. EA License C-048625. Nest Property, Suite 1301, Hollywood Centre, 233 Hollywood Road, Sheung Wan. 
	</div>
	<br /><br /><br /><br />
</body>
					</textarea>


					<br />
					Option 3:

					<textarea class="form-control" rows="20">
<body>
	<div style="font-size:11px;">
		<b>{{ $user->firstname }} {{ $user->lastname }}</b><br />
		{{ $user->offtitle }} | Nest Property<br />
		Residential Search & Investment<br />
		<br />
		Suite 1301 | Hollywood Centre | 233 Hollywood Road | Sheung Wan<br />
		<b>T</b> | <a href="tel:+85236897523">(+852) 3689 7523</a> &amp;nbsp; <b>M</b> | <a href="tel:+852{{ str_replace(' ', '', $user->phone) }}">(+852) {{ $user->phone }}</a> &amp;nbsp; <b>W</b> |  <a href="https://www.nest-property.com" target="_blank">www.nest-property.com</a><br /><br />
	</div>
	<div style="font-size:10px;">
		A standard agency fee of fifty percent (50%) of one month's total rental is payable to Nest Property by both the landlord and the tenant upon signing of a tenancy agreement. The agency fee payable to Nest Property on a sales transaction is 1% of the total purchase price payable by both the vendor and the purchaser on completion.
		<br /><br />
		Nest Property has prepared these property particulars as a guide only. They are not intended to constitute part of an offer or contract. Information relating to properties (including but not limited to price, management fees, government rates, general property descriptions, property area, address and floor area) are subject to change without notice and should not be relied upon. All details should be confirmed by your solicitor prior to entering into any formal agreement. EA License C-048625. Nest Property, Suite 1301, Hollywood Centre, 233 Hollywood Road, Sheung Wan. 
	</div>
	<br /><br /><br /><br />
</body>
					</textarea>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
