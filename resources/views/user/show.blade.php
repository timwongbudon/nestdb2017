@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="nest-user-changes-wrapper row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="col-xs-12">
						<h4>{{ $user->name }}</h4>
					</div>
				</div>
				<div class="panel-body">
					<!-- Picture -->
					<div class="col-md-6">
						<div class="col-xs-12">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<div class="nest-profile-picture-wrapper-public nest-profile-picture-wrapper">
									@if ($user->picpath == "")
										<img src="{{ url('/showimage/users/dummy.jpg') }}" />
									@else
										<img src="{{ url($user->picpath) }}" />
									@endif
								</div>
							</div>
						</div>
					</div>

					<!-- Information -->
					<div class="col-md-6">
						<label>Name: </label> {{ $user->name }}<br />
						<label>First name: </label> {{ $user->firstname }}<br />
						<label>Last name: </label> {{ $user->lastname }}<br />
						<label>Email: </label> {{ $user->email }}<br />
						<label>Phone number: </label> {{ $user->phone }}<br />
						<label>License Number: </label> {{ $user->agentid }}<br />
					</div>
					<div class="clearfix"></div>



				</div>
			</div>
		</div>
		@if (Auth::user()->id == $user->id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="col-xs-12">
							<h4 style="padding-bottom:0px;">User Documents</h4>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="panel-body" style="padding-bottom:15px;padding-top:0px;">
						<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
							<div class="nest-propety-docs-uploaded col-xs-12">
								@if (count($docuploads) > 0)
									@foreach ($docuploads as $d)

										@if ($d->doctype == 'contract')
											Contract
										@elseif ($d->doctype == 'hkid')
											HKID
										@elseif ($d->doctype == 'eaa')
											EAA License
										@elseif ($d->doctype == 'dismissal')
										    @if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
											Letter of Dismissal
											@endif
										@elseif ($d->doctype == 'resignation')
										    @if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
											Resignation Letter
											@endif
										@endif

										@if (($d->doctype !== 'dismissal' && $d->doctype !== 'resignation') || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
										from
										{{ $d->created_at_nice() }}
										@if (isset($consultants[$d->userid]))
											by {{ $consultants[$d->userid]->name }}
										@endif
										<a href="{{ url('/user/showdoc/'.$d->id.'') }}" target="_blank">download</a>
										@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
											<a href="{{ url('/user/removedoc/'.$d->id.'/'.$d->docuserid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a>
										@endif
										<br />

										@endif
									@endforeach
								@endif
							</div>
							<div class="clearfix"></div>
							<div class="nest-propety-prop-docs-upload" style="padding-top:12px;">
								<form action="{{ url('/user/docupload/'.$user->id.'') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									
									<div class="col-xs-6">
										<select name="doctype" class="form-control" style="font-weight:normal;">
											<option value="contract">Contract</option>
											<option value="hkid">HKID</option>
											<option value="eaa">EAA License</option>
											@if ( Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
											<option value="dismissal">Letter of Dismissal</option>
											<option value="resignation">Resignation Letter</option>
											@endif

										</select>
									</div>
									<div class="col-xs-6">
										<button class="nest-button nest-right-button btn btn-default" type="submit">Upload Document</button>
									</div>
									<div class="clearfix"></div>
									<div class="draganddropup col-xs-12">
										{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
									</div>

									
								</form>
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="col-xs-12">
							<h4 style="padding-bottom:0px;">Time at Nest</h4>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="panel-body" style="padding-bottom:15px;padding-top:0px;">
						<div class="row">
							<form action="" method="post">
								{{ csrf_field() }}
								<div class="col-sm-4" style="padding-left:0px;">
									<div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
										<label for="startdate" class="col-xs-12 control-label text-left">Start Date</label>

										<div class="col-xs-12">
											{!! Form::date('startdate', $user->startdate, ['class'=>'form-control']) !!}

											@if ($errors->has('startdate'))
												<span class="help-block">{{ $errors->first('startdate') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="col-sm-4" style="padding-left:0px;">
									<div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
										<label for="enddate" class="col-xs-12 control-label text-left">End Date</label>

										<div class="col-xs-12">
											{!! Form::date('enddate', $user->enddate, ['class'=>'form-control']) !!}

											@if ($errors->has('enddate'))
												<span class="help-block">{{ $errors->first('enddate') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="col-sm-4" style="padding-left:0px;padding-right:0px;padding-top:31px;">
									<input type="submit" class="nest-button btn btn-default" value="Update Dates" style="width:100%;" />
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="clearfix"></div>
    </div>
	@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
		<div class="nest-user-changes-wrapper row">
			@if (isset($rights['Consultant']))
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>Leases & Sales (Last 6 Months)</h4>
							</div>
						</div>
						<div class="panel-body">
							<div class="nest-user-changes-wrapper-inner col-xs-12">
								@php
									$curdate = '';
								@endphp
								@foreach ($invoices as $invoice)
									@php
										$fdate = Carbon\Carbon::createFromFormat('Y-m-d', $invoice->commissiondate)->format('d/m/Y (l)');
									@endphp
									@if ($curdate != $fdate)
										<h5>{{ $fdate }}</h5>
										@php
											$curdate = $fdate;
										@endphp
									@endif
									<div class="">
										<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $invoice->propertyid }}, '{{ $propertynames[$invoice->propertyid]->name }}', '')">{{ $propertynames[$invoice->propertyid]->name }}</a><br />
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>New Properties</h4>
							</div>
						</div>
						<div class="panel-body" id="newproperties">
						@include('user.paginationnewproperties')
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>Major Property Updates</h4>
							</div>
						</div>
						<div class="panel-body" id="majorchanges">
							@include('user.paginationmajorupdates')
						</div>
					</div>
				</div>

			@else
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>New Properties</h4>
							</div>
						</div>
						<div class="panel-body">
							<div class="nest-user-changes-wrapper-inner col-xs-12">
								@php
									$curdate = '';
								@endphp
								@foreach ($changelist as $list)
									@php
										$first = $list[0];
										$last = null;
										if (isset($list[1])){
											$last = $list[1];
											for ($i = 2; $i < count($list); $i++){
												if ($list[$i]->logtime > $d){
													$last = $list[$i];
												}
											}
										}
									@endphp
									@if ($last == null || ($first->created_at == $last->updated_at))
										@php
											$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
											$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('H:i');
										@endphp
										@if ($curdate != $fdate)
											<h5>{{ $fdate }}</h5>
											@php
												$curdate = $fdate;
											@endphp
										@endif
										<div class="">
											{{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $first->id }}, '{{ $first->name }}', '')">{{ $first->name }}</a><br />
										</div>
									@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="col-xs-12">
								<h4>Major Property Updates</h4>
							</div>
						</div>
						<div class="panel-body">
							<div class="nest-user-changes-wrapper-inner col-xs-12">
								@php
									$curdate = '';
								@endphp
								@foreach ($changelist as $list)
									@php
										$first = $list[0];
										$last = null;
										if (isset($list[1])){
											$last = $list[1];
											for ($i = 2; $i < count($list); $i++){
												if ($list[$i]->logtime > $d){
													$last = $list[$i];
												}
											}
										}
									@endphp
									@if ($last == null || $first->available_date != $last->available_date || $first->pictst != $last->pictst || $first->owner_id != $last->owner_id || $first->agent_id != $last->agent_id || $first->rep_ids != $last->rep_ids || $first->agent_ids != $last->agent_ids || $first->asking_rent != $last->asking_rent || $first->asking_sale != $last->asking_sale)
										@php
											//$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
											//$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('H:i');

											$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
											$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('H:i');
										@endphp
										@if ($curdate != $fdate)
											<h5>{{ $fdate }}</h5>
											@php
												$curdate = $fdate;
											@endphp
										@endif
										<div class="">
											{{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $first->id }}, '{{ $first->name }}', '')">{{ $first->name }}</a>
											@if ($last == null)
												 [New]
											@endif
											@if ($first->available_date != $last->available_date)
												 [Availability]
											@endif
											@if ($first->pictst != $last->pictst)
												 [Pictures]
											@endif
											@if ($first->owner_id != $last->owner_id || $first->agent_id != $last->agent_id || $first->rep_ids != $last->rep_ids || $first->agent_ids != $last->agent_ids)
												 [Landlord]
											@endif
											@if ($first->asking_rent != $last->asking_rent || $first->asking_sale != $last->asking_sale)
												 [Price]
											@endif
											<br />
										</div>
									@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endif
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="col-xs-12">
							<h4>All Changes in last 15 Days</h4>
						</div>
					</div>
					<div class="panel-body" id="allchanges">
						@include('user.paginationallchanges')
					</div>
					<script>
						$(document).ready(function(){

							$(document).on('click', '#allchanges .pagination a', function(event){
								event.preventDefault(); 
								
								var page = $(this).attr('href').split('page=')[1];
								$('#allchanges').html("Loading...");
								allchanges(page);
							});

							function allchanges(page)
							{
								$.ajax({
								url:"/user/pagination/allchanges/{{$user->id}}?page="+page,
								success:function(data)
								{									
									$('#allchanges').html(data);
								},
								error:function(res){
									$('#allchanges').html("Error");
								}
								});
							}

							$(document).on('click', '#newproperties .pagination a', function(event){
								event.preventDefault(); 
								
								var page = $(this).attr('href').split('page=')[1];
								$('#newproperties').html("Loading...");
								majorchanges(page,'#newproperties');
							});

							$(document).on('click', '#majorchanges .pagination a', function(event){
								event.preventDefault(); 
								
								var page = $(this).attr('href').split('page=')[1];
								$('#majorchanges').html("Loading...");
								majorchanges(page,'#majorchanges');
							});

							function majorchanges(page,dom)
							{
								if(dom=='#majorchanges'){
									var url = "/user/pagination/majorchanges/{{$user->id}}?page="+page;
								} else {
									var url = "/user/pagination/newproperties/{{$user->id}}?page="+page;
								}
								$.ajax({
								url: url,
								success:function(data)
								{			
									console.log(data);					
									$(dom).html(data);
								},
								error:function(res){
									$(dom).html("Error");
								}
								});
							}
							
						});
					</script>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
