@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 panel panel-default">
				<div class="panel-heading">
					<div class="col-xs-12">
						<h4>New User</h4>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
						{{ csrf_field() }}
						<!-- Information -->
						<div class="col-xs-12">
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email" class="col-xs-12 control-label text-left">Email address</label>

								<div class="col-xs-12">
									{!! Form::text('email', $user['email'], ['class'=>'form-control']) !!}

									@if ($errors->has('email'))
										<span class="help-block">{{ $errors->first('email') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-xs-12 control-label text-left">Name (for internal use)</label>

								<div class="col-xs-12">
									{!! Form::text('name', $user['name'], ['class'=>'form-control']) !!}

									@if ($errors->has('name'))
										<span class="help-block">{{ $errors->first('name') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
								<label for="firstname" class="col-xs-12 control-label text-left">First name (for documents)</label>

								<div class="col-xs-12">
									{!! Form::text('firstname', $user['firstname'], ['class'=>'form-control']) !!}

									@if ($errors->has('firstname'))
										<span class="help-block">{{ $errors->first('firstname') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
								<label for="lastname" class="col-xs-12 control-label text-left">Last name (for documents)</label>

								<div class="col-xs-12">
									{!! Form::text('lastname', $user['lastname'], ['class'=>'form-control']) !!}

									@if ($errors->has('lastname'))
										<span class="help-block">{{ $errors->first('lastname') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
								<label for="gender" class="col-xs-12 control-label text-left">Gender</label>

								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-4">
											<input type="radio" name="gender" value="1" {{ $user['gender'] == 1 ? "checked " : "" }}/>&nbsp;female
										</div>
										<div class="col-xs-4">
											<input type="radio" name="gender" value="2" {{ $user['gender'] == 2 ? "checked " : "" }}/>&nbsp;male
										</div>
										<div class="col-xs-4">
											<input type="radio" name="gender" value="0" {{ $user['gender'] == 0 ? "checked " : "" }}/>&nbsp;N/A
										</div>
									</div>

									@if ($errors->has('gender'))
										<span class="help-block">{{ $errors->first('gender') }}</span>
									@endif
								</div>
							</div>
						</div>
						<label class="col-xs-12 control-label text-left">Password</label>
						<div class="col-xs-12">
							New passwords need to:
							<ul>
								<li>be at least 8 characters long</li>
								<li>contain at least 1 lower case character (a, b, c, ...)</li>
								<li>contain at least 1 upper case character (A, B, C, ...)</li>
								<li>contain at least 1 number (1, 2, 3, ...)</li>
							</ul>
							<br />
						</div>
						<div class="col-xs-12">
							<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
								<label for="new_password" class="col-xs-12 control-label text-left">New Password</label>

								<div class="col-xs-12">
									{!! Form::password('new_password', ['class'=>'form-control']) !!}

									@if ($errors->has('new_password'))
										<span class="help-block">{{ $errors->first('new_password') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<label for="password_confirmation" class="col-xs-12 control-label text-left">New Password (again)</label>

								<div class="col-xs-12">
									{!! Form::password('password_confirmation', ['class'=>'form-control']) !!}

									@if ($errors->has('password_confirmation'))
										<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-12 text-center nest-form-button-wrapper">
							<button class="nest-button nest-right-button btn btn-default" type="submit">Create</button>
						</div>
					</form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
