<div class="nest-user-changes-wrapper-inner col-xs-12">
							@php
								$curdate = '';
							@endphp
							@foreach ($changesfull as $change)
								@php
									//$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
									//$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('H:i');

									$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
									$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $change->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('H:i');
								@endphp
								@if ($curdate != $fdate)
									<h5>{{ $fdate }}</h5>
									@php
										$curdate = $fdate;
									@endphp
								@endif
								<div class="">
									{{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $change->id }}, '{{ $change->name }}', '')">{{ $change->name }}</a><br />
								</div>
							@endforeach
						</div>
						{!! $changesfull->links() !!}