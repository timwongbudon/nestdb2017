@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="col-xs-12">
					<h4>User Profile {{ $user->name }}</h4>
				</div>
			</div>
			<div class="panel-body">					
				@if (!empty($saved) && $saved)
					<div class="col-xs-12">
						<div class="nest-message-banner bg-success">
							The changes have been saved successfully!
						</div>
					</div>
				@endif					
				@if (!empty($added) && $added)
					<div class="col-xs-12">
						<div class="nest-message-banner bg-success">
							The user has been created successfully!
						</div>
					</div>
				@endif
				
				<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
					{{ csrf_field() }}
					{!! Form::hidden('userid', $user->id) !!}
					
					<!-- Picture -->
					<div class="col-md-4">
						<div class="col-xs-12">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label class="col-xs-12 control-label text-left">Picture</label>
								<div class="nest-profile-picture-wrapper">
									@if ($user->picpath == "")
										<img src="{{ url('/showimage/users/dummy.jpg') }}" />
									@else
										<img src="{{ url($user->picpath) }}" />
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
								<label for="picture" class="col-xs-12 control-label text-left">New picture (square - e.g. 600x600)</label>

								<div class="col-xs-12">
									{!! Form::file('picture', ['class'=>'form-control']) !!}

									@if ($errors->has('picture'))
										<span class="help-block">{{ $errors->first('picture') }}</span>
									@endif
								</div>
							</div>
							
						</div>
					</div>
					
					<!-- Information -->
					<div class="col-md-8">
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-xs-12 control-label text-left">Name (for internal use)</label>

								<div class="col-xs-12">
									{!! Form::text('name', $user->name, ['class'=>'form-control']) !!}

									@if ($errors->has('name'))
										<span class="help-block">{{ $errors->first('name') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
								<label for="firstname" class="col-xs-12 control-label text-left">First Name (for documents)</label>

								<div class="col-xs-12">
									{!! Form::text('firstname', $user->firstname, ['class'=>'form-control']) !!}

									@if ($errors->has('firstname'))
										<span class="help-block">{{ $errors->first('firstname') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
								<label for="lastname" class="col-xs-12 control-label text-left">Last Name (for documents)</label>

								<div class="col-xs-12">
									{!! Form::text('lastname', $user->lastname, ['class'=>'form-control']) !!}

									@if ($errors->has('lastname'))
										<span class="help-block">{{ $errors->first('lastname') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('hkid') ? ' has-error' : '' }}">
								<label for="hkid" class="col-xs-12 control-label text-left">HKID No.</label>

								<div class="col-xs-12">
									{!! Form::text('hkid', $user->hkid, ['class'=>'form-control']) !!}

									@if ($errors->has('hkid'))
										<span class="help-block">{{ $errors->first('hkid') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('expensifyid') ? ' has-error' : '' }}">
								<label for="expensifyid" class="col-xs-12 control-label text-left">Expensify ID No.</label>

								<div class="col-xs-12">
									{!! Form::text('expensifyid', $user->expensifyid, ['class'=>'form-control']) !!}

									@if ($errors->has('expensifyid'))
										<span class="help-block">{{ $errors->first('expensifyid') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
								<label for="gender" class="col-xs-12 control-label text-left">Gender</label>

								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-4">
											<input type="radio" name="gender" value="1" {{ $user->gender == 1 ? "checked " : "" }}/>&nbsp;female
										</div>
										<div class="col-xs-4">
											<input type="radio" name="gender" value="2" {{ $user->gender == 2 ? "checked " : "" }}/>&nbsp;male
										</div>
										<div class="col-xs-4">
											<input type="radio" name="gender" value="0" {{ $user->gender == 0 ? "checked " : "" }}/>&nbsp;N/A
										</div>
									</div>

									@if ($errors->has('gender'))
										<span class="help-block">{{ $errors->first('gender') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
								<label for="startdate" class="col-xs-12 control-label text-left">Start Date</label>

								<div class="col-xs-12">
									{!! Form::date('startdate', $user->startdate, ['class'=>'form-control']) !!}

									@if ($errors->has('startdate'))
										<span class="help-block">{{ $errors->first('startdate') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
								<label for="enddate" class="col-xs-12 control-label text-left">End Date</label>

								<div class="col-xs-12">
									{!! Form::date('enddate', $user->enddate, ['class'=>'form-control']) !!}

									@if ($errors->has('enddate'))
										<span class="help-block">{{ $errors->first('enddate') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('commstruct') ? ' has-error' : '' }}">
								<label for="commstruct" class="col-xs-12 control-label text-left">Commission Structure..</label>

								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-6">
											<input type="radio" name="commstruct" value="0" {{ $user->commstruct == 0 ? "checked " : "" }}/>&nbsp;None
										</div>
										<div class="col-xs-6">
											<input type="radio" name="commstruct" value="1" {{ $user->commstruct == 1 ? "checked " : "" }}/>&nbsp;Salary Plus Commission
										</div>
										<div class="col-xs-6">
											<input type="radio" name="commstruct" value="2" {{ $user->commstruct == 2 ? "checked " : "" }}/>&nbsp;Commission Only
										</div>
										<div class="col-xs-6">
											<input type="radio" name="commstruct" value="3" {{ $user->commstruct == 3 ? "checked " : "" }}/>&nbsp;Will Budden
										</div>
                                                                                <div id="commstructItemsMainDiv" class="col-xs-12" style="box-shadow: 0px 1px 4px -1px rgba(0,0,0,0.83);padding-bottom: 5px;">
											
                                                                                    <input type="radio" name="commstruct" value="4" {{ $user->commstruct == 4 ? "checked " : "" }}/>&nbsp;Custom
                                                                                    <div style="clear:both;"></div>
                                                                                    <?php 
                                                                                    
                                                                                   $commstruct_custom= json_decode($user->commstruct_custom, true);
                                                                                   if(empty($commstruct_custom)){
                                                                                       $commstruct_custom['type']='';
                                                                                       $commstruct_custom['currency']='';
                                                                                       $commstruct_custom['amount']='';
                                                                                   }
                                                                                   
                                                                                   if(!(empty($commstruct_customArray))){//saved values from function changeadmuser in usercontroller
                                                                                       $commstruct_custom['type']=$commstruct_customArray['type'];
                                                                                       $commstruct_custom['currency']=$commstruct_customArray['currency'];
                                                                                       $commstruct_custom['amount']=$commstruct_customArray['amount'];
                                                                                   }
                                                                                   // print_r($commstruct_customArray);
                                                                                    ?>
                                                                                    <div style="float:left;width:33%;" class="commstructItems">
                                                                                        <select id="commstruct_custom_type" class="form-control" name="commstruct_custom_type">
                                                                                            <option >Type...</option>
                                                                                            <option value="Salary" <?php if($commstruct_custom['type']=='Salary'){echo 'selected';}?> >Salary</option>
                                                                                            <option value="Commission" <?php if($commstruct_custom['type']=='Commission'){echo 'selected';}?>>Commission</option>
                                                                                            <option value="Platinum" <?php if($commstruct_custom['type']=='Platinum'){echo 'selected';}?>>Platinum</option>
                                                                                        </select>

                                                                                    </div>
                                                                                    
                                                                                    <div style="float:left;width:91px;padding-left: 5px;padding-right: 5px;" class="commstructItems">
                                                                                        <select id="commstruct_custom_currency" class="form-control" name="commstruct_custom_currency" style="padding:0px;">
                                                                                            <option >Currency...</option>
                                                                                            <option value="HKD" <?php if($commstruct_custom['currency']=='HKD'){echo 'selected';}?>>HKD - Hong Kong Dollar</option>
                                                                                            <option value="ZAR" <?php if($commstruct_custom['currency']=='ZAR'){echo 'selected';}?>>ZAR - South African Rand</option>
                                                                                        </select>
                                                                                        
                                                                                        <div style="clear:both;"></div>
                                                                                    </div>
                                                                                    
                                                                                    <div style="float:left;width:33%;" class="commstructItems">
                                                                                        
                                                                                        <input id="commstruct_custom_amount" class="form-control" name="commstruct_custom_amount" type="number" placeholder="Amount..." value="<?php if($commstruct_custom['amount']>0){echo $commstruct_custom['amount'];}?>">

                                                                                    </div>
                                                                                    
                                                                                    <div style="clear:both;"></div>
									       </div>
									</div>

									@if ($errors->has('commstruct'))
										<span class="help-block">{{ $errors->first('commstruct') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('commstruct') ? ' has-error' : '' }}">
								<label for="commstruct" class="col-xs-12 control-label text-left">User Documents</label><br />
								<div class="col-xs-12">
									User documents can be up/downloaded <a href="/user/show/{{ $user->id }}">here</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email" class="col-xs-12 control-label text-left">Email (not editable)</label>

								<div class="col-xs-12">
									{!! Form::text('email', $user->email, ['class'=>'form-control', 'readonly']) !!}

									@if ($errors->has('email'))
										<span class="help-block">{{ $errors->first('email') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
								<label for="phone" class="col-xs-12 control-label text-left">Phone number</label>

								<div class="col-xs-12">
									{!! Form::text('phone', $user->phone, ['class'=>'form-control']) !!}

									@if ($errors->has('phone'))
										<span class="help-block">{{ $errors->first('phone') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('agentid') ? ' has-error' : '' }}">
								<label for="agentid" class="col-xs-12 control-label text-left">License Number (x-xxxxxx)</label>

								<div class="col-xs-12">
									{!! Form::text('agentid', $user->agentid, ['class'=>'form-control']) !!}

									@if ($errors->has('agentid'))
										<span class="help-block">{{ $errors->first('agentid') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('offtitle') ? ' has-error' : '' }}">
								<label for="offtitle" class="col-xs-12 control-label text-left">Title (e.g. Senior Consultant)</label>

								<div class="col-xs-12">
									{!! Form::text('offtitle', $user->offtitle, ['class'=>'form-control']) !!}

									@if ($errors->has('offtitle'))
										<span class="help-block">{{ $errors->first('offtitle') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
								<label for="address" class="col-xs-12 control-label text-left">Address</label>

								<div class="col-xs-12">
									{!! Form::textarea('address', $user->address, ['class'=>'form-control', 'rows' => 3]) !!}

									@if ($errors->has('address'))
										<span class="help-block">{{ $errors->first('address') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('offtitle') ? ' has-error' : '' }}">
								<label for="offtitle" class="col-xs-12 control-label text-left">Color (for diagram)</label>

								<div class="col-xs-12">
									{!! Form::text('color', $user->color, ['class'=>'form-control']) !!}

									@if ($errors->has('color'))
										<span class="help-block">{{ $errors->first('color') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
								<label for="status" class="col-xs-12 control-label text-left">Status</label>

								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-6">
											<input type="radio" name="status" value="0" {{ $user->status == 0 ? "checked " : "" }}/>&nbsp;active
										</div>
										<div class="col-xs-6">
											<input type="radio" name="status" value="1" {{ $user->status == 1 ? "checked " : "" }}/>&nbsp;inactive
										</div>
									</div>

									@if ($errors->has('status'))
										<span class="help-block">{{ $errors->first('status') }}</span>
									@endif
								</div>
							</div>


							<div class="form-group">
								<label for="status" class="col-xs-12 control-label text-left">Allowed Vacation</label>

																
									<div class="col-xs-8">
										{!! Form::number('allowed_vacation', $user->allowed_vacation, ['class'=>'form-control allowed_vacation']) !!}
									</div>
								
									
							

								
									<div class="col-xs-12">
										Total number of days used: <strong>{{$vacation_used}} days</strong><br>
										@if($user->allowed_vacation != 0 || $user->allowed_vacation != null)
											Remaining days left: <strong>{{$user->allowed_vacation - $vacation_used}} days</strong>
										@endif
									</div>
							
							</div>

							<div class="form-group">
								<label for="vacation_tier" class="col-xs-12 control-label text-left">Vacation Tier</label>

								<div class="col-xs-12">
									<div class="row">										
										<div class="col-xs-6">
											<input type="radio" name="vacation_tier" class="vacation_tier" value="Salary Specific" {{ $user->vacation_tier == 'Salary Specific' ? "checked " : "" }}/>&nbsp;Salary Specific
										</div>
										<div class="col-xs-6">
											<input type="radio" name="vacation_tier" class="vacation_tier" value="Freelancer" {{ $user->vacation_tier == 'Freelancer' ? "checked " : "" }}/>&nbsp;Freelancer
										</div>										
									</div>
								
								</div>

								<div class="col-xs-4 nest-right-button ">
										<button id="updated_vacation"  class="nest-button nest-right-button btn btn-default " data-user="{{ $user->id }}" type="button">Update Vacation</button>
									</div>

									<script>
									$('#updated_vacation').click(function(){
										

										var user = $(this).data('user');
										var allowed_vacation = $('.allowed_vacation').val();
										var vacation_tier = $('.vacation_tier:checked').val();
									
									
										jQuery.ajax({
											url: "/user/update_vacation/"+user+"/"+allowed_vacation+"/"+vacation_tier,
											dataType: 'json',
											type: 'GET',
											contentType: 'application/json',
											success: function(res){
										
											location.reload(true); 	
											}
										});

										setTimeout(location.reload(true), 1000);

									});

									$('.vacation_tier').click(function(){
										
										

										if($(this).val()=="Freelancer"){
											$('.allowed_vacation').val(0);
										} else {
											$('.allowed_vacation').val(14);
										}
										
									});
									</script>
							</div>
							
						</div>
					</div>
					<div class="clearfix"></div>
					<hr />
					<div class="col-md-4">
						<label for="rights" class="col-xs-12 control-label text-left">Roles</label>
						<div class="row">
							@foreach ($roles as $role)
								<div class="col-md-6">
									@if (isset($rights[$role]) && $rights[$role])
										<input type="checkbox" name="rights[]" value="{{ $role }}" checked /> {{ $rolenames[$role] }}
									@else
										<input type="checkbox" name="rights[]" value="{{ $role }}" /> {{ $rolenames[$role] }}
									@endif
								</div>
							@endforeach
						</div>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6">
								<label class="col-xs-12 control-label text-left">Password</label>
								<div class="col-xs-12">
									New passwords need to:
									<ul>
										<li>be at least 8 characters long</li>
										<li>contain at least 1 lower case character (a, b, c, ...)</li>
										<li>contain at least 1 upper case character (A, B, C, ...)</li>
										<li>contain at least 1 number (1, 2, 3, ...)</li>
									</ul>
									<br />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
									<label for="new_password" class="col-xs-12 control-label text-left">New Password</label>

									<div class="col-xs-12">
										{!! Form::password('new_password', ['class'=>'form-control']) !!}

										@if ($errors->has('new_password'))
											<span class="help-block">{{ $errors->first('new_password') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
									<label for="password_confirmation" class="col-xs-12 control-label text-left">New Password (again)</label>

									<div class="col-xs-12">
										{!! Form::password('password_confirmation', ['class'=>'form-control']) !!}

										@if ($errors->has('password_confirmation'))
											<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<hr />
					
					
					<div class="col-xs-12">
						<label for="managing" class="col-xs-12 control-label text-left">Sales Team Manager</label>
						<div class="row">
							@foreach ($users as $u)
								@if ($u['status'] == 0)
									<div class="col-md-3 col-xs-6">
										@if (isset($managing[$u['id']]))
											<input type="checkbox" name="managing[]" value="{{ $u['id'] }}" checked /> {{ $u['name'] }}
										@else
											<input type="checkbox" name="managing[]" value="{{ $u['id'] }}" /> {{ $u['name'] }}
										@endif
									</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="clearfix"></div>
					<hr />

					<div class="col-xs-12">
						<label for="managing" class="col-xs-12 control-label text-left">Sales Team Leader</label>
						<div class="row">
							@foreach ($users as $u)
								@if ($u['status'] == 0)
									<div class="col-md-3 col-xs-6">
										@if ( isset($salesLeaders[$u['id']]) )
											<input type="checkbox" name="sales_leaders[]" value="{{ $u['id'] }}" checked /> {{ $u['name'] }}
										@else
											<input type="checkbox" name="sales_leaders[]" value="{{ $u['id'] }}" /> {{ $u['name'] }}
										@endif
									</div>
								@endif
							@endforeach
						</div>
					</div>
					
					<div class="col-xs-12 text-center nest-form-button-wrapper" style="padding-right:15px;">
						<button class="nest-button nest-right-button btn btn-default" type="submit">Save</button>
					</div>
					
				</form>
			</div>
		</div>
    </div>
</div>

<script>
    $(document).on("change", "input[name='commstruct']", function () {
        
        if($("input[name='commstruct']:checked").val()==='4'){
            
            $(".commstructItems").show();
            $("#commstructItemsMainDiv").css("box-shadow", "0px 1px 4px -1px rgba(0,0,0,0.83)");
            
        }else{
            $(".commstructItems").hide();
            $("#commstructItemsMainDiv").css("box-shadow", "");
        }
        
 });
 
 //ini after page load
     setTimeout(function() { 
      
        if($("input[name='commstruct']:checked").val()==='4'){
            
            $(".commstructItems").show();
            $("#commstructItemsMainDiv").css("box-shadow", "0px 1px 4px -1px rgba(0,0,0,0.83)");
            
        }else{
            $(".commstructItems").hide();
            $("#commstructItemsMainDiv").css("box-shadow", "");
        }
      
    }, 500);
 
</script>
@endsection
