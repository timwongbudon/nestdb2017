<div class="nest-user-changes-wrapper-inner col-xs-12">
								@php
									$curdate = '';
								@endphp
								@foreach ($newpropertieslogs as $list)
									@php
										$first = $list[0];
										$last = null;
										if (isset($list[1])){
											$last = $list[1];
											for ($i = 2; $i < count($list); $i++){
												if ($list[$i]->logtime > $d){
													$last = $list[$i];
												}
											}
										}
									@endphp
									@if ($last == null || ($first->created_at == $last->updated_at))
										@php
											$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
											$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('H:i');
										@endphp
										@if ($curdate != $fdate)
											<h5>{{ $fdate }}</h5>
											@php
												$curdate = $fdate;
											@endphp
										@endif
										<div class="">
											{{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $first->id }}, '{{ $first->name }}', '')">{{ $first->name }}</a><br />
										</div>
									@endif
								@endforeach

                                {!! $newpropertieslogs->links() !!}
							</div>