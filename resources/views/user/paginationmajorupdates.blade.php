<div class="nest-user-changes-wrapper-inner col-xs-12">
								@php
                                    $curdate = '';
                                     count($changelist).'ds';
                                  $c =0;
	
								@endphp
								@foreach ($changelist as $list)
                                    @php
                                
										$first = $list[0];
										$last = null;
										if (isset($list[1])){
											$last = $list[1];
											/*if(isset($list[2])){
												$last = $list[2];
											}*/
											for ($i = 2; $i < count($list); $i++){
												if ($list[$i]->logtime > $d){
													$last = $list[$i];
													
												}
											}
                                        }
                                     
									@endphp
                
                                        @php
                                        	//$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
											//$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'America/Los_Angeles')->setTimezone('Asia/Hong_Kong')->format('H:i');

											$fdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('d/m/Y (l)');
											$ftime = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $first->logtime, 'Europe/London')->setTimezone('Asia/Hong_Kong')->format('H:i');
										@endphp
										@if ($curdate != $fdate)
											<h5>{{ $fdate }}</h5>
											@php
												$curdate = $fdate;
											@endphp
										@endif
										<div class="">
                                        
                                            {{ $ftime }} <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onclick="showproperty({{ $first->id }}, '{{ $first->name }}', '')">{{ $first->name }}</a>
											 @if ($last == null)
												 [New]
                                            @endif
                                            @if ($first['available_date'] != $last['available_date'])
												 [Availability]
                                            @endif
                                            
                                            @if ($first['pictst'] != $last['pictst'])
												 [Pictures]
											@endif
											@if ($first['owner_id'] != $last['owner_id'] || $first['agent_id'] != $last['agent_id'] || $first['rep_ids'] != $last['rep_ids'] || $first['agent_ids'] != $last['agent_ids'])
												 [Landlord]
											@endif
											@if ($first['asking_rent'] != $last['asking_rent'] || $first['asking_sale'] != $last['asking_sale'])
												 [Price]
											@endif
											
											
										</div>							
								
                                @endforeach
                                
                                {!! $changelist->links() !!}
								
							</div>
