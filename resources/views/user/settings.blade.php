@extends('layouts.app')

@section('content')
<div class="nest-new">
    <div class="row">
        <div class="row">
            <div class="panel panel-default col-md-8 col-md-offset-2">
                <div class="panel-heading">
					<div class="col-xs-12">
						<h4>User Settings</h4>
					</div>
				</div>
                <div class="panel-body">
					
					<div class="col-xs-12">
						New passwords need to:
						<ul>
							<li>be at least 8 characters long</li>
							<li>contain at least 1 lower case character (a, b, c, ...)</li>
							<li>contain at least 1 upper case character (A, B, C, ...)</li>
							<li>contain at least 1 number (1, 2, 3, ...)</li>
						</ul>
						<br />
					</div>
					
                    <form class="form-horizontal" role="form" method="POST" action="">
                        {{ csrf_field() }}
						
						@if (!empty($saved) && $saved)
							<div class="col-xs-12">
								<div class="nest-message-banner bg-success">
									The changes have been saved successfully!
								</div>
							</div>
						@endif
						
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
								<label for="old_password" class="col-xs-12 control-label text-left">Old Password</label>

								<div class="col-xs-12">
									{!! Form::password('old_password', ['class'=>'form-control']) !!}

									@if ($errors->has('old_password'))
										<span class="help-block">{{ $errors->first('old_password') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
								<label for="new_password" class="col-xs-12 control-label text-left">New Password</label>

								<div class="col-xs-12">
									{!! Form::password('new_password', ['class'=>'form-control']) !!}

									@if ($errors->has('new_password'))
										<span class="help-block">{{ $errors->first('new_password') }}</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<label for="password_confirmation" class="col-xs-12 control-label text-left">New Password (again)</label>

								<div class="col-xs-12">
									{!! Form::password('password_confirmation', ['class'=>'form-control']) !!}

									@if ($errors->has('password_confirmation'))
										<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-xs-12 text-center nest-form-button-wrapper">
							<button class="nest-button nest-right-button btn btn-default" type="submit">Save</button>
						</div>

						
						
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
