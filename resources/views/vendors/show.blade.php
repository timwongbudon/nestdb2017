@extends('layouts.app')

@section('content')

<div class="nest-new container">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			@if ($vendor->type_id == 3 || $vendor->type_id == 1)
				<div class="col-sm-4">
			@else
				<div class="col-sm-6">
			@endif
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Vendor Information</h4>
					</div>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>ID</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $vendor->id }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								<b>Type</b>
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $vendor->type() }}
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 col-xs-6">
								@if ($vendor->type_id > 2)
									<b>Agency</b>
								@else
									<b>Company</b>
								@endif
							</div>
							<div class="col-md-8 col-xs-6">
								{{ $vendor->company() }}
							</div>
						</div>
						
						@if ($vendor->type_id != 3)
							@if ($vendor->v_title > 0)
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>Title</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->getVTitle() }}
									</div>
								</div>
							@endif
							<div class="row">
								<div class="col-md-4 col-xs-6">
									<b>First Name</b>
								</div>
								<div class="col-md-8 col-xs-6">
									{{ $vendor->firstname }}
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4 col-xs-6">
									<b>Last Name</b>
								</div>
								<div class="col-md-8 col-xs-6">
									{{ $vendor->lastname }}
								</div>
							</div>
						@endif
						
						@if (trim($vendor->tel) != '')
							<div class="row">
								<div class="col-md-4 col-xs-6">
									<b>Tel</b>
								</div>
								<div class="col-md-8 col-xs-6">
									{!! $vendor->telLinked() !!}
								</div>
							</div>
						@endif
						
						@if (trim($vendor->email) != '')
							<div class="row">
								<div class="col-md-4 col-xs-6">
									<b>Email</b>
								</div>
								<div class="col-md-8 col-xs-6">
									{{ $vendor->email }}
								</div>
							</div>
						@endif
					</div>
				</div>
				@if ($vendor->type_id == 1)
					<div class="panel panel-default">
						<div class="panel-body">
							@if (trim($vendor->address1) != '')
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>Address</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->address1 }}
									</div>
								</div>
							@endif
							
							@if (trim($vendor->district()) != '')
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>District</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->district() }}
									</div>
								</div>
							@endif
							
							@if (trim($vendor->get_nice_country()) != '')
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>Country</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->get_nice_country() }}
									</div>
								</div>
							@endif
							
							@if (trim($vendor->bri) != '')
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>BRI</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->bri }}
									</div>
								</div>
							@endif
							
							@if (trim($vendor->hkid) != '')
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>HKID</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->hkid }}
									</div>
								</div>
							@endif
							
							@if (trim($vendor->agency_fee) != '')
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<b>Agency Fee</b>
									</div>
									<div class="col-md-8 col-xs-6">
										{{ $vendor->agency_fee }}
									</div>
								</div>
							@endif
							
							
						
						</div>
					</div>
					@if (trim($vendor->firstname2) != '' || trim($vendor->lastname2) != '' || trim($vendor->tel2) != '' || trim($vendor->email2) != '')
						<div class="panel panel-default">
							<div class="panel-body">
								@if ($vendor->v_title2 > 0)
									<div class="row">
										<div class="col-md-4 col-xs-6">
											<b>Title</b>
										</div>
										<div class="col-md-8 col-xs-6">
											{{ $vendor->getVTitle2() }}
										</div>
									</div>
								@endif
								@if (trim($vendor->firstname2) != '')
									<div class="row">
										<div class="col-md-4 col-xs-6">
											<b>First Name (2)</b>
										</div>
										<div class="col-md-8 col-xs-6">
											{{ $vendor->firstname2 }}
										</div>
									</div>
								@endif
								
								@if (trim($vendor->lastname2) != '')
									<div class="row">
										<div class="col-md-4 col-xs-6">
											<b>Last Name (2)</b>
										</div>
										<div class="col-md-8 col-xs-6">
											{{ $vendor->lastname2 }}
										</div>
									</div>
								@endif
								
								@if (trim($vendor->tel2) != '')
									<div class="row">
										<div class="col-md-4 col-xs-6">
											<b>Tel (2)</b>
										</div>
										<div class="col-md-8 col-xs-6">
											{!! $vendor->tel2Linked() !!}
										</div>
									</div>
								@endif
								
								@if (trim($vendor->email2) != '')
									<div class="row">
										<div class="col-md-4 col-xs-6">
											<b>Email (2)</b>
										</div>
										<div class="col-md-8 col-xs-6">
											{{ $vendor->email2 }}
										</div>
									</div>
								@endif
							</div>
						</div>
					@endif
				@endif
				
				@if ($vendor->type_id == 2 || $vendor->type_id == 4)
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4 col-xs-6">
									@if ($vendor->type_id > 2)
										<b>Agency (Parent)</b>
									@else
										<b>Owner (Parent)</b>
									@endif
								</div>
								<div class="col-md-8 col-xs-6">
									@if (!empty($vendor['parent_id']))
										<a href="{{ url('/vendor/show/'.$vendor['parent']->id) }}">{{ $vendor['parent']->company }}</a>
									@endif
								</div>
							</div>
						</div>
					</div>
				@endif
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-xs-12">
							<a href="{{ url('/vendor/edit/'.$vendor->id) }}" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Edit</a>
						</div>
					</div>
				</div>
			</div>
			@if ($vendor->type_id == 3 || $vendor->type_id == 1)
				<div class="col-sm-4">
			@else
				<div class="col-sm-6">
			@endif
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					<div class="panel-body">
						<div class="nest-comments" id="existingCommentsVendor"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				
				<script>			
					function loadCommentsVendor(){
						jQuery.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/vendor/comments/".$vendor->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsVendor').html(data);
							},
							error: function (data) {
								jQuery.ajax({
									type: 'GET',
									url: '{{ url("/vendor/comments/".$vendor->id) }}',
									cache: false,
									success: function (data) {
										jQuery('#existingCommentsVendor').html(data);
									},
									error: function (data) {										
										jQuery.ajax({
											type: 'GET',
											url: '{{ url("/vendor/comments/".$vendor->id) }}',
											cache: false,
											success: function (data) {
												jQuery('#existingCommentsVendor').html(data);
											},
											error: function (data) {
												jQuery('#existingCommentsVendor').html('An error occurred '+data);
											}
										});
									}
								});
							}
						});
					}
					
					jQuery( document ).ready(function(){
						loadCommentsVendor();
					});
				</script>
			</div>
			@if ($vendor->type_id == 3 || $vendor->type_id == 1)
				<div class="col-sm-4">
			@endif	
				@if ($vendor->type_id == 1)
					@include('partials.vendor.create-rep', ['parent' => $vendor])
				@endif
				@if ($vendor->type_id == 3)
					@include('partials.vendor.create-agent', ['parent' => $vendor])
				@endif
			@if ($vendor->type_id == 3 || $vendor->type_id == 1)
				</div>
			@endif
        </div>
    </div>
	<div class="row">
		<div class="nest-property-edit-wrapper" style="padding-top:0px;">
			<div class="col-xs-12">
				@if (($vendor->type_id == 1 || $vendor->type_id == 3) && count($vendor['children']) > 0)
					<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
						@if (count($vendor['children']) > 0) 
							@foreach ($vendor['children'] as $index => $v)
								<div class="row 
								@if ($index%2 == 1)
									nest-striped
								@endif
								">
									<div class="col-lg-4 col-md-4 col-sm-6">
										<b>Name:</b> <a href="{{ url('vendor/show/'.$v->id) }}">
											{{ $v->name() }} 
											@if($v->firstname2)
												/ {{ $v->name2()}}
											@endif</a><br />
										<b>Company:</b> <a href="{{ url('vendor/show/'.$v->id) }}">{{ $v->company }}</a>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>ID:</b> {{ $v->id }}<br />
										<b>Type:</b> {{ $v->type() }}
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Tel:</b> {!! $v->telLinked() !!} 
											@if($v->tel2)
												/ {!! $v->tel2Linked() !!}
											@endif<br />
										<b>Email:</b> {{ $v->email() }}
									</div>
									<div class="col-lg-2 col-md-2 col-sm-6">
										<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('vendor/edit/'.$v->id) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
										<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('vendor/show/'.$v->id) }}">
											<i class="fa fa-btn fa-info-circle"></i>View
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						@endif
					</div>
				@endif
				@if (count($properties) > 0)
					<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
						@foreach ($properties as $index => $p)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">{{ $p->name }}</a><br />
									<b>ID:</b> {{ $p->id }}
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<b>Address:</b> {{ $p->address1 }}<br />
									<b>Unit:</b> {{ $p->unit }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Available Date:</b> {{ $p->available_date() }}<br />
									<b>Asking Price:</b> {!! $p->price_db_searchresults_new() !!}
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6">
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('property/edit/'.$p->id) }}">
										<i class="fa fa-btn fa-pencil-square-o"></i>Edit
									</a>
									<a class="nest-button nest-right-button btn btn-primary" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">
										<i class="fa fa-btn fa-info-circle"></i>View
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>	
				@endif
				
				<div class="panel panel-default">
					<div class="panel-body" style="padding-bottom:15px;">
						@if ((empty($properties) || (count($properties) == 0)) && (count($vendor['children']) == 0))
							<form action="{{ url('vendor/remove') }}" method="post" class="form-horizontal" onsubmit="return confirm('Do you really want to delete this vendor?');">
								{{ csrf_field() }}
								{{ Form::hidden('id', $vendor->id) }}
								<div class="nest-delete-row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-danger"><i class="fa fa-btn fa-trash"></i> Delete Vendor</button>
									</div>
								</div>
							</form>
						@else
							<b>NOTE:</b> A vendor can only be deleted, if there are no Reps or Agents that have that vendor as parent vendor and if there are no properties related to that vendor.
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer-script')
@include('partials.vendor.parent-modal-control')
@include('partials.vendor.field-display-control')
@endsection







