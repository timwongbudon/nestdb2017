@extends('layouts.app')

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('vendor/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>New Vendor</h4>
						</div>
						<div class="panel-body">
							@include('common.errors')
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Type</div>
								</div>
								<div class="col-xs-8">
									<label for="vendor-type-1" class="control-label">{!! Form::radio('type_id', 1, '', ['id'=>'vendor-type-1']) !!} Owner</label>
									<label for="vendor-type-2" class="control-label">{!! Form::radio('type_id', 2, '', ['id'=>'vendor-type-2']) !!} Rep</label>
									<label for="vendor-type-3" class="control-label">{!! Form::radio('type_id', 3, '', ['id'=>'vendor-type-3']) !!} Agency</label>
									<label for="vendor-type-4" class="control-label">{!! Form::radio('type_id', 4, '', ['id'=>'vendor-type-4']) !!} Agent</label>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row" id="vendor-company-info-section">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Company</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="company" id="vendor-company" class="form-control" value="{{ old('company') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div id="vendor-fullname-info-section">
								<!-- <div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Title</div>
									</div>
									<div class="col-xs-8">
										<label for="vendor-title-1" class="control-label">{!! Form::radio('v_title', 1, '', ['id'=>'vendor-title-1']) !!} Ms.</label>&nbsp;
										<label for="vendor-title-2" class="control-label">{!! Form::radio('v_title', 2, '', ['id'=>'vendor-title-2']) !!} Mrs.</label>&nbsp;
										<label for="vendor-title-3" class="control-label">{!! Form::radio('v_title', 3, '', ['id'=>'vendor-title-3']) !!} Mr.</label>&nbsp;
										<label for="vendor-title-4" class="control-label">{!! Form::radio('v_title', 0, '', ['id'=>'vendor-title-4']) !!} N/A</label>
									</div>
									<div class="clearfix"></div>
								</div> -->
								<input type="hidden" name="v_title" value="0" />
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">First Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="firstname" id="vendor-firstname" class="form-control" value="{{ old('firstname') }}">
									</div>
									<div class="clearfix"></div>
								</div>
								
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Last Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="lastname" id="vendor-lastname" class="form-control" value="{{ old('lastname') }}">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel" id="vendor-tel" class="form-control" value="{{ old('tel') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="email" id="vendor-email" class="form-control" value="{{ old('email') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Add Email to EDM List</div>
								</div>
								<div class="col-xs-4">
									<label for="vendor-edm-0" class="control-label">{!! Form::radio('edm', 0, true, ['id'=>'vendor-edm-0']) !!} No</label>
								</div>
								<div class="col-xs-4">
									<label for="vendor-edm-1" class="control-label">{!! Form::radio('edm', 1, false, ['id'=>'vendor-edm-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Corporate Landlord</div>
								</div>
								<div class="col-xs-4">
									<label for="vendor-corporatelandlord-0" class="control-label">{!! Form::radio('corporatelandlord', 0, true, ['id'=>'vendor-corporatelandlord-0']) !!} No</label>
								</div>
								<div class="col-xs-4">
									<label for="vendor-corporatelandlord-1" class="control-label">{!! Form::radio('corporatelandlord', 1, false, ['id'=>'vendor-corporatelandlord-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4" id="vendor-second-contact-info-section">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Second Contact</h4>
						</div>
						<div class="panel-body">
							<!-- <div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Title</div>
								</div>
								<div class="col-xs-8">
									<label for="vendor-title2-1" class="control-label">{!! Form::radio('v_title2', 1, '', ['id'=>'vendor-title2-1']) !!} Ms.</label>&nbsp;
									<label for="vendor-title2-2" class="control-label">{!! Form::radio('v_title2', 2, '', ['id'=>'vendor-title2-2']) !!} Mrs.</label>&nbsp;
									<label for="vendor-title2-3" class="control-label">{!! Form::radio('v_title2', 3, '', ['id'=>'vendor-title2-3']) !!} Mr.</label>&nbsp;
									<label for="vendor-title2-4" class="control-label">{!! Form::radio('v_title2', 0, '', ['id'=>'vendor-title2-4']) !!} N/A</label>
								</div>
								<div class="clearfix"></div>
							</div> -->
							<input type="hidden" name="v_title2" value="0" />
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">First Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="firstname2" id="vendor-firstname2" class="form-control" value="{{ old('firstname2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Last Name</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="lastname2" id="vendor-lastname2" class="form-control" value="{{ old('lastname2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel2" id="vendor-tel2" class="form-control" value="{{ old('tel2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="email2" id="vendor-email2" class="form-control" value="{{ old('email2') }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default" id="vendor-owner-info-section">
						<div class="panel-body">	
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Address</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="address1" id="vendor-address1" class="form-control" value="{{ old('address1') }}" >
									<input type="hidden" name="address2" id="vendor-address2" class="form-control" value="" >
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">District</div>
								</div>
								<div class="col-xs-8">
									{!! Form::select('district_id', $district_ids, '', ['id'=>'vendor-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Country</div>
								</div>
								<div class="col-xs-8">
									{!! Form::select('country_code', $country_ids, 'hk', ['id'=>'vendor-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">BRI</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="bri" id="vendor-bri" class="form-control" value="{{ old('bri') }}" >
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">HKID</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="hkid" id="vendor-hkid" class="form-control" value="{{ old('hkid') }}" >
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Agency Fee</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="agency_fee" id="vendor-agency_fee" class="form-control" value="{{ old('agency_fee') }}" >
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default" id="vendor-parent-info-section">
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Parent</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="parent" id="vendor-parent" class="form-control" value="{{ old('parent') }}">
									<span id="vendor-parent-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the owner name"><i class="fa fa-btn fa-close"></i></button></span>
									<input type="hidden" name="parent_id" id="vendor-parent_id" class="form-control" value="{{ old('parent_id') }}">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Comments</div>
								</div>
								<div class="col-xs-8">
									{!! Form::textarea('comments', '', ['class'=>'form-control', 'placeholder' => 'Type your comments here', 'rows' => '4']) !!}
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Submit </button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
</script>
@endsection

@section('footer-script')
@include('partials.vendor.parent-modal-control')
@include('partials.vendor.field-display-control')
@endsection