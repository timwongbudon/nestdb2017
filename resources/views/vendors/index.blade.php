@extends('layouts.app')

@section('content')
            <!-- Vendor Search -->
            <div class="nest-buildings-search-panel panel panel-default">
                <div class="panel-body">
                    <!-- New Property Form -->
                    <form action="{{ url('vendors') }}" method="GET" class="form-horizontal">
                        <div class="nest-buildings-search-wrapper row">
                            <div class="col-sm-3">
                                <input type="text" name="sstring" placeholder="First name, Last name" class="form-control" value="{{ old('sstring', $input->sstring) }}">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="company" placeholder="Company, Email, Address" class="form-control" value="{{ old('company', $input->company) }}">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="tel" placeholder="Phone number" id="vendor-tel" class="form-control" value="{{ old('tel', $input->tel) }}">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="id" placeholder="Vendor ID" id="vendor-id" class="form-control" value="{{ old('id', $input->id) }}">
                            </div>
							<div class="clearfix"></div>
                        </div>
    
						<div class="nest-buildings-search-buttons-wrapper row">
							<div class="col-xs-6">
								<a class="nest-button btn btn-primary" href="{{ url('vendor/create') }}">
									<i class="fa fa-btn fa-plus"></i>Add Vendor
								</a>
							</div>
                            <div class="col-xs-6">
                                <button type="submit" name="action" value="search" class="nest-button nest-right-button btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search </button>
                            </div>
						</div>
                    </form>
                </div>
            </div>

            
            <!-- Current Properties -->
            @if (count($vendors) > 0)
				<div>
					<div class="nest-buildings-result-wrapper">
						@foreach ($vendors as $index => $vendor)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Name:</b> <a href="{{ url('vendor/show/'.$vendor->id) }}">
										{{ $vendor->name() }} 
										@if($vendor->firstname2 || $vendor->lastname2)
											/ {{ $vendor->name2()}}
										@endif</a><br />
									<b>Company:</b> <a href="{{ url('vendor/show/'.$vendor->id) }}">{{ $vendor->company() }}</a>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>ID/Type:</b> {{ $vendor->id }} / {{ $vendor->type() }}<br />
									<b>Parent:</b> {!! $vendor['parent'] ? '<a href="'.url('vendor/show/'.$vendor['parent']->id).'">'.$vendor['parent']->id.'</a>' : '-' !!}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Tel:</b> {!! $vendor->telLinked() !!} 
										@if($vendor->tel2)
											/ {!! $vendor->tel2Linked() !!}
										@endif<br />
									<b>Email:</b> {{ $vendor->email() }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('vendor/edit/'.$vendor->id) }}">
										<i class="fa fa-btn fa-pencil-square-o"></i>Edit
									</a>
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('vendor/show/'.$vendor->id) }}">
										<i class="fa fa-btn fa-info-circle"></i>View
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
				</div>
				
				@if ($vendors->lastPage() > 1)
					<div class="panel-footer nest-buildings-pagination">
						<!-- <div style="margin: 10px 0;">Page {{ $vendors->currentPage() }} of {{ $vendors->lastPage() }} (Total: {{ $vendors->total() }} items)</div> -->
						{{ $vendors->links() }}
					</div>
				@endif
            @endif

           <!--  <div class="panel panel-default">
                <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <a class="btn btn-primary" href="{{ url('vendor/create') }}">
                                    <i class="fa fa-btn fa-plus"></i>Add Vendor
                                </a>
                            </div>
                        </div>
                </div>
            </div> -->
	<script>
		jQuery('form').on('focus', 'input[type=number]', function (e) {
			jQuery(this).on('mousewheel.disableScroll', function (e) {
				e.preventDefault();
			});
		});
		jQuery('form').on('blur', 'input[type=number]', function (e) {
			jQuery(this).off('mousewheel.disableScroll');
		});
	</script>
@endsection
