@extends('layouts.app')

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('vendor/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@if ($vendor->type_id != 3)
					<div class="col-sm-4">
				@else
					<div class="col-sm-6">
				@endif
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Edit Vendor</h4>
						</div>

						<div class="panel-body">
							@include('common.errors')

							{{ csrf_field() }}
							
							@if ($vendor->type_id==1 || $vendor->type_id==3)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Type</div>
									</div>
									<div class="col-xs-8">
										<label for="vendor-type-1" class="control-label">{!! Form::radio('type_id', 1, $vendor->type_id==1, ['id'=>'vendor-type-1']) !!} Owner</label>
										<label for="vendor-type-3" class="control-label">{!! Form::radio('type_id', 3, $vendor->type_id==3, ['id'=>'vendor-type-3']) !!} Agency</label>
									</div>
									<div class="clearfix"></div>
								</div>
							@else
								<input type="hidden" name="type_id" value="{{ $vendor->type_id }}" />
							@endif
							
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									@if ($vendor->type_id > 2)
										<div class="nest-property-edit-label">Agency</div>
									@else
										<div class="nest-property-edit-label">Company</div>
									@endif
								</div>
								<div class="col-xs-8">
									<input type="text" name="company" id="vendor-company" class="form-control" value="{{ old('company', $vendor->company) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							@if ($vendor->type_id != 3)
								<!-- <div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Title</div>
									</div>
									<div class="col-xs-8">
										<label for="vendor-title-1" class="control-label">{!! Form::radio('v_title', 1, ($vendor->v_title == 1), ['id'=>'vendor-title-1']) !!} Ms.</label>&nbsp;
										<label for="vendor-title-2" class="control-label">{!! Form::radio('v_title', 2, ($vendor->v_title == 2), ['id'=>'vendor-title-2']) !!} Mrs.</label>&nbsp;
										<label for="vendor-title-3" class="control-label">{!! Form::radio('v_title', 3, ($vendor->v_title == 3), ['id'=>'vendor-title-3']) !!} Mr.</label>&nbsp;
										<label for="vendor-title-4" class="control-label">{!! Form::radio('v_title', 0, ($vendor->v_title == 0), ['id'=>'vendor-title-4']) !!} N/A</label>
									</div>
									<div class="clearfix"></div>
								</div>--> 
								<input type="hidden" name="v_title" value="{{ $vendor->v_title }}" />
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">First Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="firstname" id="vendor-firstname" class="form-control" value="{{ old('firstname', $vendor->firstname) }}">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Last Name</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="lastname" id="vendor-lastname" class="form-control" value="{{ old('lastname', $vendor->lastname) }}">
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Tel</div>
								</div>
								<div class="col-xs-8">
									<input type="text" name="tel" id="vendor-tel" class="form-control" value="{{ old('tel', $vendor->tel) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Email</div>
								</div>
								<div class="col-xs-8">
									<input type="hidden" name="emailold" value="{{ $vendor->email }}">
									<input type="text" name="email" id="vendor-email" class="form-control" value="{{ old('email', $vendor->email) }}">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="nest-property-edit-row">
								<div class="col-xs-4">
									 <div class="nest-property-edit-label">Add Email to EDM List</div>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="edmold" value="{{ $vendor->edm }}">
									<label for="vendor-edm-0" class="control-label">{!! Form::radio('edm', 0, $vendor->edm == 0, ['id'=>'vendor-edm-0']) !!} No</label>
								</div>
								<div class="col-xs-4">
									<label for="vendor-edm-1" class="control-label">{!! Form::radio('edm', 1, $vendor->edm == 1, ['id'=>'vendor-edm-1']) !!} Yes</label>
								</div>
								<div class="clearfix"></div>
							</div>
							@if ($vendor->type_id == 1 || $vendor->type_id == 3)
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Corporate Landlord</div>
									</div>
									<div class="col-xs-4">
										<label for="vendor-corporatelandlord-0" class="control-label">{!! Form::radio('corporatelandlord', 0, $vendor->corporatelandlord == 0, ['id'=>'vendor-corporatelandlord-0']) !!} No</label>
									</div>
									<div class="col-xs-4">
										<label for="vendor-corporatelandlord-1" class="control-label">{!! Form::radio('corporatelandlord', 1, $vendor->corporatelandlord == 1, ['id'=>'vendor-corporatelandlord-1']) !!} Yes</label>
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
						</div>
					</div>
					
					@if ($vendor->type_id == 1)
						<div id="vendor-owner-info-section" class="panel panel-default">
							<div class="panel-body">
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Address</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="address1" id="vendor-address1" class="form-control" value="{{ old('address1', $vendor->address1) }}" >
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">District</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('district_id', $district_ids, $vendor->district_id, ['id'=>'vendor-district_id', 'class'=>'form-control', 'placeholder' => 'Choose a district...']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Country</div>
									</div>
									<div class="col-xs-8">
										{!! Form::select('country_code', $country_ids, $vendor->country_code, ['id'=>'vendor-country_code', 'class'=>'form-control', 'placeholder' => 'Choose a country...']); !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">BRI</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="bri" id="vendor-bri" class="form-control" value="{{ old('bri', $vendor->bri) }}" >
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">HKID</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="hkid" id="vendor-hkid" class="form-control" value="{{ old('hkid', $vendor->hkid) }}" >
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										 <div class="nest-property-edit-label">Agency Fee</div>
									</div>
									<div class="col-xs-8">
										<input type="text" name="agency_fee" id="vendor-agency_fee" class="form-control" value="{{ old('agency_fee', $vendor->agency_fee) }}" >
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					@endif
					@if ($vendor->type_id == 3)
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="col-xs-12">
									{{ Form::hidden('previous_url', $previous_url) }}
									{{ Form::hidden('id', $vendor->id) }}
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								</div>
							</div>
						</div>
					@endif
				</div>
				@if ($vendor->type_id != 3)
					<div class="col-sm-4">
						@if ($vendor->type_id == 1)
							<div id="vendor-second-contact-info-section" class="panel panel-default">
								<div class="panel-body">
									<!-- <div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Title</div>
										</div>
										<div class="col-xs-8">
											<label for="vendor-title2-1" class="control-label">{!! Form::radio('v_title2', 1, ($vendor->v_title2 == 1), ['id'=>'vendor-title2-1']) !!} Ms.</label>&nbsp;
											<label for="vendor-title2-2" class="control-label">{!! Form::radio('v_title2', 2, ($vendor->v_title2 == 2), ['id'=>'vendor-title2-2']) !!} Mrs.</label>&nbsp;
											<label for="vendor-title2-3" class="control-label">{!! Form::radio('v_title2', 3, ($vendor->v_title2 == 3), ['id'=>'vendor-title2-3']) !!} Mr.</label>&nbsp;
											<label for="vendor-title2-4" class="control-label">{!! Form::radio('v_title2', 0, ($vendor->v_title2 == 0), ['id'=>'vendor-title2-4']) !!} N/A</label>
										</div>
										<div class="clearfix"></div>
									</div> -->
									<input type="hidden" name="v_title2" value="{{ $vendor->v_title2 }}" />
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">First Name (2)</div>
										</div>
										<div class="col-xs-8">
											<input type="text" name="firstname2" id="vendor-firstname2" class="form-control" value="{{ old('firstname2', $vendor->firstname2) }}">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Last Name (2)</div>
										</div>
										<div class="col-xs-8">
											<input type="text" name="lastname2" id="vendor-lastname2" class="form-control" value="{{ old('lastname2', $vendor->lastname2) }}">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Tel (2)</div>
										</div>
										<div class="col-xs-8">
											<input type="text" name="tel2" id="vendor-tel2" class="form-control" value="{{ old('tel2', $vendor->tel2) }}">
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											 <div class="nest-property-edit-label">Email (2)</div>
										</div>
										<div class="col-xs-8">
											<input type="text" name="email2" id="vendor-email2" class="form-control" value="{{ old('email2', $vendor->email2) }}">
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						@endif
						
						@if ($vendor->type_id == 2 || $vendor->type_id == 4)
							<div id="vendor-parent-info-section" class="panel panel-default">
								<div class="panel-body">
									<div class="nest-property-edit-row">
										<div class="col-xs-4">
											@if ($vendor->type_id > 2)
												<div class="nest-property-edit-label">Agency (Parent)</div>
											@else
												<div class="nest-property-edit-label">Owner (Parent)</div>
											@endif
										</div>
										<div class="col-xs-8">
											<input type="text" name="parent" id="vendor-parent" class="form-control" value="{{ old('parent', !empty($vendor['parent_id'])?$vendor['parent']->company:'') }}">
											<span id="vendor-parent-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the owner name"><i class="fa fa-btn fa-close"></i></button></span>
										</div>
										<div class="clearfix"></div>
									</div>
									
									<input type="hidden" name="parent_id" id="vendor-parent_id" class="form-control" value="{{ old('parent_id', $vendor->parent_id) }}">
								</div>
							</div>
						@endif

						<div class="panel panel-default">
							<div class="panel-body">
								<div class="col-xs-12">
									{{ Form::hidden('previous_url', $previous_url) }}
									{{ Form::hidden('id', $vendor->id) }}
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								</div>
							</div>
						</div>
					</div>
				@endif
			</form>
			@if ($vendor->type_id != 3)
				<div class="col-sm-4">
			@else
				<div class="col-sm-6">
			@endif
				<div class="panel panel-default">
					<div class="panel-heading">
					   <h4>Comments</h4>
					</div>
					<div class="panel-body">
						<div class="nest-new-comments-wrapper">
							<textarea id="newcommentVendor" class="form-control" rows="3"></textarea>
							<button class="nest-button nest-right-button btn btn-default" onClick="addCommentVendor();">Add Comment</button>
							<div class="clearfix"></div>
						</div>
						<div class="nest-comments" id="existingCommentsVendor"><img src="{{ url('images/tools/loader.gif') }}" /></div>
					</div>
				</div>
				
				<script>			
					function loadCommentsVendor(){
						jQuery.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/vendor/comments/".$vendor->id) }}',
							cache: false,
							success: function (data) {
								jQuery('#existingCommentsVendor').html(data);
							},
							error: function (data) {
								jQuery.ajax({
									type: 'GET',
									url: '{{ url("/vendor/comments/".$vendor->id) }}',
									cache: false,
									success: function (data) {
										jQuery('#existingCommentsVendor').html(data);
									},
									error: function (data) {										
										jQuery.ajax({
											type: 'GET',
											url: '{{ url("/vendor/comments/".$vendor->id) }}',
											cache: false,
											success: function (data) {
												jQuery('#existingCommentsVendor').html(data);
											},
											error: function (data) {
												jQuery('#existingCommentsVendor').html('An error occurred '+data);
											}
										});
									}
								});
							}
						});
					}
					
					function addCommentVendor(){
						var comment = jQuery('#newcommentVendor').val().trim();
						if (comment.length > 0){
							jQuery.ajaxSetup({
								headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								}
							});
							jQuery.ajax({
								type: 'POST',
								url: '{{ url("/vendor/commentadd/".$vendor->id) }}',
								data: {comment: comment},
								dataType: 'text',
								cache: false,
								async: false,
								success: function (data) {
									jQuery('#newcommentVendor').val('');
									loadCommentsVendor();
								},
								error: function (data) {
									console.log(data);
									alert('Something went wrong, please try again!');
								}
							});
						}else{
							alert('Please enter a comment');
						}
					}
					
					$('#newcommentVendor').keydown(function( event ) {
						if ( event.keyCode == 13 ) {
							event.preventDefault();
							addCommentVendor();
						}				
					});
					
					function removeCommentVendor(t){
						jQuery.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});
						jQuery.ajax({
							type: 'POST',
							url: '{{ url("/vendor/commentremove/".$vendor->id) }}',
							data: {t: t},
							dataType: 'text',
							cache: false,
							async: false,
							success: function (data) {
								loadCommentsVendor();
							},
							error: function (data) {
								alert('Something went wrong, please try again!');
							}
						});
					}
					
					jQuery( document ).ready(function(){
						loadCommentsVendor();
					});
				</script>
				
			</div>
        </div>
	</div>
	<div class="row">
		<div class="nest-property-edit-wrapper" style="padding-top:0px;">
			<div class="col-xs-12">
				@if (($vendor->type_id == 1 || $vendor->type_id == 3) && count($vendor['children']) > 0)
					<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
						@if (count($vendor['children']) > 0) 
							@foreach ($vendor['children'] as $index => $v)
								<div class="row 
								@if ($index%2 == 1)
									nest-striped
								@endif
								">
									<div class="col-lg-4 col-md-4 col-sm-6">
										<b>Name:</b> <a href="{{ url('vendor/show/'.$v->id) }}">
											{{ $v->name() }} 
											@if($v->firstname2)
												/ {{ $v->name2()}}
											@endif</a><br />
										<b>Company:</b> <a href="{{ url('vendor/show/'.$v->id) }}">{{ $v->company }}</a>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>ID:</b> {{ $v->id }}<br />
										<b>Type:</b> {{ $v->type() }}
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
										<b>Tel:</b> {{ $v->tel() }} 
											@if($v->tel2)
												/ {{ $v->tel2()}}
											@endif<br />
										<b>Email:</b> {{ $v->email() }}
									</div>
									<div class="col-lg-2 col-md-2 col-sm-6">
										<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('vendor/edit/'.$v->id) }}">
											<i class="fa fa-btn fa-pencil-square-o"></i>Edit
										</a>
										<a class="nest-button nest-right-button btn btn-primary" target="_blank" href="{{ url('vendor/show/'.$v->id) }}">
											<i class="fa fa-btn fa-info-circle"></i>View
										</a>
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						@endif
					</div>
				@endif
				@if (count($properties) > 0)
					<div class="nest-buildings-result-wrapper" style="margin-bottom:15px;">
						@foreach ($properties as $index => $p)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Name:</b> <a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">{{ $p->name }}</a><br />
									<b>ID:</b> {{ $p->id }}
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<b>Address:</b> {{ $p->address1 }}<br />
									<b>Unit:</b> {{ $p->unit }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Available Date:</b> {{ $p->available_date() }}<br />
									<b>Asking Price:</b> {!! $p->price_db_searchresults_new() !!}
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6">
									<a class="nest-button nest-right-button btn btn-primary" href="{{ url('property/edit/'.$p->id) }}">
										<i class="fa fa-btn fa-pencil-square-o"></i>Edit
									</a>
									<a class="nest-button nest-right-button btn btn-primary" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $p->id }}, '{{ str_replace("'", "\'", $p->name) }}', '')">
										<i class="fa fa-btn fa-info-circle"></i>View
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>	
				@endif
				
				<div class="row">
					<div class="nest-property-edit-wrapper" style="padding-top:0px;">
						<div class="col-sm-4">
							<div class="panel panel-default">
								<div class="panel-body" style="padding-bottom:15px;">
									@if ((empty($properties) || (count($properties) == 0)) && (count($vendor['children']) == 0))
										<form action="{{ url('vendor/remove') }}" method="post" class="form-horizontal" onsubmit="return confirm('Do you really want to delete this vendor?');">
											{{ csrf_field() }}
											{{ Form::hidden('id', $vendor->id) }}
											<div class="nest-delete-row">
												<div class="col-xs-12">
													<button type="submit" class="btn btn-danger"><i class="fa fa-btn fa-trash"></i> Delete Vendor</button>
												</div>
											</div>
										</form>
									@else
										<b>NOTE:</b> A vendor can only be deleted, if there are no Reps or Agents that have that vendor as parent vendor and if there are no properties related to that vendor.
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection

@section('footer-script')
@include('partials.vendor.parent-modal-control')
@endsection