@extends('layouts.app')

@section('content')

<div class="panel panel-default nest-dashboardnew-wrapper">
    <div class="panel-heading">
      
        <h2>Xero Logs</h2>
     
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        <table class="table ">
            <tr>	
                <th>User</th>
                <th>Invoice ID</th>
                <th>Message</th>
                <th>Date/Time</th>
            </tr>

            @if(count($logs) > 0 )

                @foreach($logs as $log)
                    <tr>
                        <td>{{$log->name}}</td>
                        <td>{{$log->invoiceid}}</td>
                        <td>{{$log->message}}</td>
                        <td>{{date('d.m.Y H:i:s',strtotime($log->created_at)) }}</td>
                    </tr>
                @endforeach
            @else   

                <tr>
                    <td colspan="4">No data available.</td>                    
                </tr> 

            @endif


        </table>

        <div class="panel nest-new panel-default ">
            {{ $logs->links() }}
        </div>

        </div>
    </div>
</div>


@endsection