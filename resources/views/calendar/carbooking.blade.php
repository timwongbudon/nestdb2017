@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<?php
	function paintTime($date, $from, $to, $id, $color){
		$d = new \DateTime($date);
		$dd = $d->format('N');
		
		$top = 28+(($dd-1)*50);
		
		if ($from != null && $to != null){
			$ef = explode(':', $from);
			$et = explode(':', $to);
			
			if (count($ef) > 1 && $et > 1){
				$hf = $ef[0];
				$ht = $et[0];
				$mf = intval(str_replace('pm', '', str_replace('am', '', $ef[1])));
				$mt = intval(str_replace('pm', '', str_replace('am', '', $et[1])));
				
				if (strstr($ef[1], 'pm') && $hf < 12){
					$hf += 12;
				}
				if (strstr($et[1], 'pm') && $ht < 12){
					$ht += 12;
				}
				
				$tf = $hf + $mf/60;
				$tt = $ht + $mt/60 - $tf;
				
				return '<div onmouseout="hideDetails(\''.$id.'\');" onmouseover="showDetails(\''.$id.'\');" class="carbooking-timebar" style="position:absolute;top:'.$top.'px;height:20px;left:'.floor(100+$tf*40).'px;width:'.floor($tt*40).'px;background:#'.$color.';"></div>';
			}
		}
		
		return '';
	}
	
	function hoverPosition($date, $from, $to){
		$d = new \DateTime($date);
		$dd = $d->format('N');
		
		$top = 45+(($dd-1)*50);
		
		if ($from != null && $to != null){
			$ef = explode(':', $from);
			$et = explode(':', $to);
			
			if (count($ef) > 1 && $et > 1){
				$hf = $ef[0];
				$ht = $et[0];
				$mf = intval(str_replace('pm', '', str_replace('am', '', $ef[1])));
				$mt = intval(str_replace('pm', '', str_replace('am', '', $et[1])));
				
				if (strstr($ef[1], 'pm') && $hf < 12){
					$hf += 12;
				}
				if (strstr($et[1], 'pm') && $ht < 12){
					$ht += 12;
				}
				
				$tf = $hf + $mf/60;
				$tt = $ht + $mt/60 - $tf;
				
				return 'left:'.floor(100+$tf*40-25).'px;top:'.$top.'px;';
			}
		}
		
		return '';
	}
?>
<style>
	.carbooking-timebar:hover{
		opacity:0.8;
	}
</style>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Car Bookings</h4>
						<div style="padding-top:0px;">
							{{ Carbon\Carbon::createFromFormat('Y-m-d', $monday)->format('d/m/Y') }} to {{ Carbon\Carbon::createFromFormat('Y-m-d', $saturday)->format('d/m/Y') }}
						</div>
					</div>
					<div class="panel-body">
						<div style="position:relative;width:1060px;height:400px;margin:10px auto 0px;">
							<div style="position:absolute;top:30px;left:0px;width:80px;text-align:right;">Monday</div>
							<div style="position:absolute;top:80px;left:0px;width:80px;text-align:right;">Tuesday</div>
							<div style="position:absolute;top:130px;left:0px;width:80px;text-align:right;">Wednesday</div>
							<div style="position:absolute;top:180px;left:0px;width:80px;text-align:right;">Thursday</div>
							<div style="position:absolute;top:230px;left:0px;width:80px;text-align:right;">Friday</div>
							<div style="position:absolute;top:280px;left:0px;width:80px;text-align:right;">Saturday</div>
							
							<div style="position:absolute;top:38px;left:100px;width:960px;height:1px;background:#cccccc;"></div>
							<div style="position:absolute;top:88px;left:100px;width:960px;height:1px;background:#cccccc;"></div>
							<div style="position:absolute;top:138px;left:100px;width:960px;height:1px;background:#cccccc;"></div>
							<div style="position:absolute;top:188px;left:100px;width:960px;height:1px;background:#cccccc;"></div>
							<div style="position:absolute;top:238px;left:100px;width:960px;height:1px;background:#cccccc;"></div>
							<div style="position:absolute;top:288px;left:100px;width:960px;height:1px;background:#cccccc;"></div>
							
							<?php
								for ($i = 0; $i <= 24; $i++){
									echo '<div style="position:absolute;top:320px;left:'.(90+$i*40).'px;width:20px;text-align:center">'.$i.':00</div>';
									echo '<div style="position:absolute;top:10px;left:'.(100+$i*40).'px;width:1px;height:300px;background:#cccccc;"></div>';
								}
							?>
							
							@foreach ($varr as $index => $v)
								<?php echo paintTime($v->v_date, $v->timefrom, $v->timeto, 'v'.$index, $users[$v['shortlist']->user_id]->color); ?>
								
								<div id="v{{ $index }}" class="calendar-entry-viewing calendar-entry" style="position:absolute;display:none;z-index:99;width:140px;background:#ffffff;text-align:center;<?php echo hoverPosition($v->v_date, $v->timefrom, $v->timeto); ?>">
									<div class="calendar-entry-top">
										@if (trim($users[$v['shortlist']->user_id]->picpath) != '')
											<img src="{{ url($users[$v['shortlist']->user_id]->picpath) }}" /> 
										@else
											<img src="/showimage/users/dummy.jpg" />
										@endif
										Viewing<br />{{ $users[$v['shortlist']->user_id]->name }}
									</div>
									@if (!Auth::user()->isOnlyDriver() && !Auth::user()->isOnlyPhotographer())
										<div class="calendar-entry-time">{{ $v->getNiceTime() }}</div>
										<div class="calendar-entry-client">with {{ $v['client']->name() }}</div>
									@else
										<div class="calendar-entry-time">{{ $v->getNiceTime() }}</div>
									@endif
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
										<div class="calendar-entry-comment">
											Created:<br />{{ $v->created_at() }}<br />Last Change:<br />{{ $v->updated_at() }}
										</div>
									@endif
								</div>
							@endforeach
							
							@foreach ($parr as $index => $v)
								<?php echo paintTime($v->v_date, $v->timefrom, $v->timeto, 'p'.$index, 'cc0000'); ?>
								
								<div id="p{{ $index }}" class="calendar-entry-viewing calendar-entry" style="position:absolute;display:none;z-index:99;width:140px;background:#ffffff;text-align:center;<?php echo hoverPosition($v->v_date, $v->timefrom, $v->timeto); ?>">
									<div class="calendar-entry-top">
										<img src="/showimage/users/dummy.jpg" />
										Photoshoot
									</div>
									<div class="calendar-entry-time">{{ $v->getNiceTime() }}</div>
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
										<div class="calendar-entry-comment">
											Created:<br />{{ $v->created_at() }}<br />Last Change:<br />{{ $v->updated_at() }}
										</div>
									@endif
								</div>
							@endforeach
							
							@foreach ($carr as $index => $c)
								<?php echo paintTime($c->datefrom, $c->timefrom, $c->timeto, 'c'.$index, $users[$c->user_id]->color); ?>
								
								@if ($c->type == 1)
									<div id="c{{ $index }}" class="calendar-entry-handover calendar-entry" style="position:absolute;display:none;z-index:99;width:140px;background:#ffffff;text-align:center;<?php echo hoverPosition($c->datefrom, $c->timefrom, $c->timeto); ?>">
										<div class="calendar-entry-top">
											@if (trim($users[$c->user_id]->picpath) != '')
												<img src="{{ url($users[$c->user_id]->picpath) }}" /> 
											@else
												<img src="/showimage/users/dummy.jpg" /> 
											@endif
											Handover<br />
											{{ $users[$c->user_id]->name }} 
										</div>
										@if (isset($leads[$c->leadid]) && !Auth::user()->isOnlyDriver() && !Auth::user()->isOnlyPhotographer())
											<div class="calendar-entry-time">{{ $c->timefrom }}</div>
											<div class="calendar-entry-client">with {{ $leads[$c->leadid]['client']->name() }}</div>
										@else
											<div class="calendar-entry-time">{{ $c->timefrom }}</div>
										@endif
										@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
											<div class="calendar-entry-comment">
												Created:<br />{{ $c->created_at() }}<br />Last Change:<br />{{ $c->updated_at() }}
											</div>
										@endif
									</div>
								@elseif ($c->type == 2)
									<div id="c{{ $index }}" class="calendar-entry-handover calendar-entry" style="position:absolute;display:none;z-index:99;width:140px;background:#ffffff;text-align:center;<?php echo hoverPosition($c->datefrom, $c->timefrom, $c->timeto); ?>">
										<div class="calendar-entry-top">
											@if (trim($users[$c->user_id]->picpath) != '')
												<img src="{{ url($users[$c->user_id]->picpath) }}" /> 
											@else
												<img src="/showimage/users/dummy.jpg" /> 
											@endif
											Driver<br />
											{{ $users[$c->user_id]->name }}
										</div>
										<div class="calendar-entry-time">{{ $c->timefrom }}-{{ $c->timeto }}</div>
										@if (trim($c->driveroptions) != '')
											<div class="calendar-entry-client">{{ $c->driveroptions }}</div>
										@endif
										@if (trim($c->comments) != '')
											<div class="calendar-entry-comment">{{ $c->comments }}</div><br />
										@endif
										@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
											<div class="calendar-entry-comment">
												Created:<br />{{ $c->created_at() }}<br />Last Change:<br />{{ $c->updated_at() }}
											</div>
										@endif
									</div>
								@elseif ($c->type == 4)
									<div id="c{{ $index }}" class="calendar-entry-handover calendar-entry" style="position:absolute;display:none;z-index:99;width:140px;background:#ffffff;text-align:center;<?php echo hoverPosition($c->datefrom, $c->timefrom, $c->timeto); ?>">
										<div class="calendar-entry-top">
											@if (trim($users[$c->user_id]->picpath) != '')
												<img src="{{ url($users[$c->user_id]->picpath) }}" /> 
											@else
												<img src="/showimage/users/dummy.jpg" /> 
											@endif
											{{ $c->driveroptions }}<br />
											{{ $users[$c->user_id]->name }}
										</div>
										<div class="calendar-entry-time">{{ $c->timefrom }}-{{ $c->timeto }}</div>
										@if (trim($c->comments) != '')
											<div class="calendar-entry-comment">{{ $c->comments }}</div>
										@endif
										@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
											<div class="calendar-entry-comment">
												Created:<br />{{ $c->created_at() }}<br />Last Change:<br />{{ $c->updated_at() }}
											</div>
										@endif
									</div>
								@endif
							@endforeach
							
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function showDetails(id){
		jQuery('#'+id).css('display', 'block');
	}
	function hideDetails(id){
		jQuery('#'+id).css('display', 'none');
	}
</script>



@endsection

@section('footer-script')
@endsection