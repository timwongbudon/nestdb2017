@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-lg-3 col-sm-4">
				<div class="panel panel-default">
					<div class="nest-calendar-heading panel-body text-center">
						<h4>{{ $months[$curmonth] }} {{ $curyear }}</h4>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-4">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<a href="/calendar/{{ $prevmonth }}/{{ $prevyear }}"> << </a>
						
						<select id="month" onChange="shownewmonth()" class="calendar-top-month form-control">
							@for ($i = 1; $i <= 12; $i++)
								@if ($curmonth == $i)
									<option value="{{ $i }}" selected>{{ $months[$i] }}</option>
								@else
									<option value="{{ $i }}">{{ $months[$i] }}</option>
								@endif
							@endfor
						</select>
						<select id="year" onChange="shownewmonth()" class="calendar-top-month form-control">
							@for ($i = 2018; $i < ($nextyear+5); $i++)
								@if ($curyear == $i)
									<option value="{{ $i }}" selected>{{ $i }}</option>
								@else
									<option value="{{ $i }}">{{ $i }}</option>
								@endif
							@endfor
						</select>
						<a href="/calendar/{{ $nextmonth }}/{{ $nextyear }}"> >> </a>
						
						<script>
							function shownewmonth(){
								var year = jQuery('#year').val();
								var month = jQuery('#month').val();
								window.location = '/calendar/'+month+'/'+year;
							}
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						@if ($curmonth == intval(date('m')) && $curyear == intval(date('Y')))
							<a href="javascript:;" id="showpastlink" style="float:right;display:block;padding:10px;" onClick="showPast();">
								Show Past Entries
							</a>
						@endif
						<h4>Calendar</h4>
					</div>
					<div class="panel-body text-center">
						<div class="table-responsive">
							<table class="table">
								<tr>
									<th style="text-align:center;width:14.3%">Monday</th>
									<th style="text-align:center;width:14.3%;">Tuesday</th>
									<th style="text-align:center;width:14.3%;">Wednesday</th>
									<th style="text-align:center;width:14.3%;">Thursday</th>
									<th style="text-align:center;width:14.3%;">Friday</th>
									<th style="text-align:center;width:14.3%;">Saturday</th>
									<th style="text-align:center;width:14.2%;">Sunday</th>
								</tr>
								@php
									$index = 0;
								@endphp
								<tr>
									@for ($i = 0; $i < $firstday; $i++)
										<td>&nbsp;</td>
										@php
											$index++;
										@endphp
									@endfor
									@for ($i = 0; $i < $days; $i++)
										<td style="vertical-align:top !important;">
											@if ($curmonth == intval(date('m')) && $curyear == intval(date('Y')) && ($i+1) == intval(date('d')))
												<div style="font-weight:bold;color:#cc0000;"><u>{{ ($i+1) }}</u></div>
											@else
												<div class=""><u>{{ ($i+1) }}</u></div>
											@endif
											@if ($curmonth == intval(date('m')) && $curyear == intval(date('Y')) && ($i+1) < intval(date('d')))
												<div class="show-hide-past">...</div>
												<div class="hide-past" style="display:none;">
											@else
												<div>
											@endif
												@foreach ($times as $t)
													@if (isset($varr[($i+1)]))
														@foreach ($varr[($i+1)] as $v)
														
															@php
															$removeTimeSpage = str_replace(" ","",$t);
															@endphp	
																												

															@if (str_replace(" ","",$v->getNiceTimeFrom()) == $removeTimeSpage)
																<div class="calendar-entry-viewing calendar-entry">
																	<div class="calendar-entry-top">
																		@if (trim($users[$v['shortlist']->user_id]->picpath) != '')
																			<img src="{{ url($users[$v['shortlist']->user_id]->picpath) }}" 
																			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				title="Created: {{ $v->created_at() }}, Last updated: {{ $v->updated_at() }}" 
																			@endif
																			/> 
																		@else
																			<img src="/showimage/users/dummy.jpg" 
																			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				title="Created: {{ $v->created_at() }}, Last updated: {{ $v->updated_at() }}" 
																			@endif
																			/>
																		@endif
																		Viewing<br />{{ $users[$v['shortlist']->user_id]->name }}
																	</div>
																	@if (!Auth::user()->isOnlyDriver() && !Auth::user()->isOnlyPhotographer())
																		<div class="calendar-entry-time"><a href="/viewing/show/{{ $v->id }}">{{ $v->getNiceTime() }}</a></div>
																		<div class="calendar-entry-client">with <a href="/client/show/{{ $v['client']->id }}">{{ $v['client']->name() }}</a></div>
																	@else
																		<div class="calendar-entry-time">{{ $v->getNiceTime() }}</div>
																	@endif
																	@if ($v->driver == 0)
																		<div class="calendar-entry-client">No Driver</div>
																	@else
																		<div class="calendar-entry-client">Driver</div>
																		<div class="calendar-entry-car">
																			<img src="{{$v->getUsedCar()}}" /> 
																		</div>
																	@endif
																</div>
															@endif
														@endforeach
													@endif	
													@if (isset($parr[($i+1)]))
														@foreach ($parr[($i+1)] as $v)

															@php
																$removeTimeSpage = str_replace(" ","",$t);
															@endphp	
																	
															@if (str_replace(" ","",$v->getNiceTimeFrom()) == $removeTimeSpage)
																<div class="calendar-entry-viewing calendar-entry">
																	<div class="calendar-entry-top">
																		<img src="/showimage/users/dummy.jpg" 
																			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				title="Created: {{ $v->created_at() }}, Last updated: {{ $v->updated_at() }}" 
																			@endif
																		/>
																		Photoshoot
																	</div>
																	@if (!Auth::user()->isOnlyDriver() && !Auth::user()->isOnlyPhotographer())
																		<div class="calendar-entry-time"><a href="/photoshoot/show/{{ $v->id }}">{{ $v->getNiceTime() }}</a></div>
																	@else
																		<div class="calendar-entry-time">{{ $v->getNiceTime() }}</div>
																	@endif
																	@if ($v->driver == 0)
																		<div class="calendar-entry-client">No Driver</div>
																	@else
																		<div class="calendar-entry-client">Driver</div>
																		<div class="calendar-entry-car">
																			<img src="{{$v->getUsedCar()}}" /> 
																		</div>
																	@endif
																</div>
															@endif
														@endforeach
													@endif		
													@if (isset($carr[($i+1)]))
														@foreach ($carr[($i+1)] as $c)
															@php
																$removeTimeSpage = str_replace(" ","",$t);																
															@endphp	
															
															@if (str_replace(" ","",$c->timefrom) == $removeTimeSpage)
																@if ($c->type == 1)
																	<div class="calendar-entry-handover calendar-entry">
																		<div class="calendar-entry-top">
																			@if (trim($users[$c->user_id]->picpath) != '')
																				<img src="{{ url($users[$c->user_id]->picpath) }}" 
																				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																					title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																				@endif
																				/> 
																			@else
																				<img src="/showimage/users/dummy.jpg" 
																				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																					title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																				@endif
																				/> 
																			@endif
																			Handover
																			@if (Auth::user()->id == $c->user_id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				<a href="/calendar/edit/{{ $c->id }}/0"><i class="fa fa-pencil"></i></a> 
																				<a href="/calendar/remove/{{ $c->id }}"><i class="fa fa-trash-o"></i></a> <br />
																			@endif
																			{{ $users[$c->user_id]->name }} 
																		</div>
																		@if (isset($leads[$c->leadid]) && !Auth::user()->isOnlyDriver() && !Auth::user()->isOnlyPhotographer())
																			<div class="calendar-entry-time"><a href="/lead/edit/{{ $c->leadid }}">{{ $c->timefrom }}</a></div>
																			<div class="calendar-entry-client">with <a href="/client/show/{{ $leads[$c->leadid]['client']->id }}">{{ $leads[$c->leadid]['client']->name() }}</a></div>
																		@else
																			<div class="calendar-entry-time">{{ $c->timefrom }}</div>
																		@endif
																	</div>
																@elseif ($c->type == 2)
																	<div class="calendar-entry-handover calendar-entry">
																		<div class="calendar-entry-top">
																			@if (trim($users[$c->user_id]->picpath) != '')
																				<img src="{{ url($users[$c->user_id]->picpath) }}" 
																				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																					title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																				@endif
																				/> 
																			@else
																				<img src="/showimage/users/dummy.jpg" 
																				@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																					title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																				@endif
																				/> 
																			@endif
																			Driver
																			@if (Auth::user()->id == $c->user_id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				<a href="/calendar/edit/{{ $c->id }}/0"><i class="fa fa-pencil"></i></a> 
																				<a href="/calendar/remove/{{ $c->id }}"><i class="fa fa-trash-o"></i></a> <br />
																			@endif
																			{{ $users[$c->user_id]->name }}
																		</div>
																		<div class="calendar-entry-time">{{ $c->timefrom }}-{{ $c->timeto }}</div>
																		@if (trim($c->driveroptions) != '')
																			<div class="calendar-entry-client">{{ $c->driveroptions }}</div>
																		@endif
																		@if (trim($c->comments) != '')
																			<div class="calendar-entry-comment">{{ $c->comments }}</div><br />
																		@endif
																		<div class="calendar-entry-car">
																			<img src="{{$c->getUsedCar()}}" /> 
																		</div>
																	</div>
																@elseif ($c->type == 4)
																	@if ($c->directoronly == 0)
																		<div class="calendar-entry-handover calendar-entry">
																			<div class="calendar-entry-top">
																				@if (trim($users[$c->user_id]->picpath) != '')
																					<img src="{{ url($users[$c->user_id]->picpath) }}" 
																					@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																						title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																					@endif
																					/> 
																				@else
																					<img src="/showimage/users/dummy.jpg" 
																					@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																						title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																					@endif
																					/> 
																				@endif
																				{{ $c->driveroptions }}
																				@if (Auth::user()->id == $c->user_id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																					<a href="/calendar/edit/{{ $c->id }}/0"><i class="fa fa-pencil"></i></a> 
																					<a href="/calendar/remove/{{ $c->id }}"><i class="fa fa-trash-o"></i></a> <br />
																				@endif
																				{{ $users[$c->user_id]->name }}
																			</div>
																			<div class="calendar-entry-time">{{ $c->timefrom }}-{{ $c->timeto }}</div>
																			@if (trim($c->comments) != '')
																				<div class="calendar-entry-comment">{{ $c->comments }}</div>
																			@endif
																		</div>
																	@endif
																@endif
															@endif
														@endforeach
													@endif	
												@endforeach
												@if (isset($carr[($i+1)]))
													@foreach ($carr[($i+1)] as $c)
														@if ($c->type == 3)
															@if (Auth::user()->id == $c->user_id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
																<div class="calendar-entry-handover calendar-entry">
																	<div class="calendar-entry-top">
																		@if (trim($users[$c->user_id]->picpath) != '')
																			<img src="{{ url($users[$c->user_id]->picpath) }}" 
																			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																			@endif
																			/> 
																		@else
																			<img src="/showimage/users/dummy.jpg" 
																			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																				title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																			@endif
																			/> 
																		@endif
																		Vacation
																		@if (Auth::user()->id == $c->user_id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																			<a href="/calendar/edit/{{ $c->id }}/0"><i class="fa fa-pencil"></i></a> 
																			<a href="/calendar/remove/{{ $c->id }}"><i class="fa fa-trash-o"></i></a> <br />
																		@endif
																		{{ $users[$c->user_id]->name }}
																	</div>
																	<div class="calendar-entry-time">
																		@if ($c->accepted == 0)
																			Not Approved Yet
																		@else
																			Approved
																		@endif
																	</div>
																</div>
															@endif
														@endif
													@endforeach
												@endif	
											</div>
										</td>
										@php
											$index++;
										@endphp
										@if ($index >= 7)
											</tr><tr>
											@php
												$index=0;
											@endphp
										@endif
									@endfor
									@for ($i = $index; $i < 7; $i++)
										<td>&nbsp;</td>
									@endfor
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="nest-property-edit-wrapper">
			@if (!Auth::user()->isOnlyDriver())
				<div class="calendar-add-edit-wrapper col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Driver Booking</h4>
						</div>
						<div class="panel-body">
							<form action="" method="post" id="addViewing">
								{{ csrf_field() }}
								<input type="hidden" name="type" value="2" />
								<select name="driveroptions" class="calendar-top-time form-control">
									@foreach ($driveroptions as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
								<input type="text" name="comments" class="calendar-comments-field form-control" placeholder="Comment" />
								<div class="clearfix"></div><br>
								<input type="date" name="datefrom" id="viewingdate" class="calendar-top-date form-control" required />
								<div class="clearfix"></div>
								<select name="timefrom" id="timefrom" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
								<div class="calendar-top-text">-</div>
								<select name="timeto" id="timeto" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
								<div class="clearfix"></div>
								<select name="use_car" class="calendar-top-time form-control">
									<option value="1">Alphard</option>
									<option value="2">Tesla 1</option>
                                                                        <option value="3">Tesla 2</option>
								</select>
								<div class="clearfix"></div>
								<input type="submit" value="Add" class="calendar-top-button nest-button btn btn-default" />
							</form>
						</div>
					</div>
				</div>


				<script>


					async function checkDriverBooking(){
						var d = jQuery('#viewingdate').val();

						$timefrom = $('#timefrom').val();
						$timeto = $('#timeto').val();
						$car = jQuery('select[name="use_car"]').val();
					

						result = await $.ajax({
							url: "/calendar/carbooking_check/"+d+"/0/"+$timefrom+"/"+$timeto+"/"+$car,
							dataType: 'json',
							type: 'GET',
							contentType: 'application/json',
							//success: callback
						});
						return result;
						
					}

					async function checkViewDuration(){
						var d = jQuery('#viewingdate').val();

						$timefrom = $('#timefrom').val();
						$timeto = $('#timeto').val();

						//var numOfProp = $('.property-option:checked').length;
						var numOfProp = 100;
						
						result = await $.ajax({
							url: "/calendar/viewing_duration_check/"+d+"/"+numOfProp+"/"+$timefrom+"/"+$timeto,
							dataType: 'json',
							type: 'GET',
							contentType: 'application/json',
							//success: callback2
						});

						return result;
					
					}

				jQuery('#addViewing').on('submit', function(e){
						
						
						var numOfProp = $('.property-option:checked').length;

						if( $('#viewingdate').val() == '' ){
							alert("Please Add date");
							return false;
						}
						
						var error = false;
						
						
					//	if($('.driveropt:checked').val() == 1){
							e.preventDefault();


							
							$car = jQuery('select[name="use_car"]').val();
							if( $car == "" ){
								e.preventDefault();
								alert("Please select a car");
								error = true;							
								return false;
							}
						

							/*if( numOfProp == 0){
								e.preventDefault();
								alert("Please select property to view");
								error = true;							
								return false;
							}*/

							checkDriverBooking().then( (result) => {
								
								
								if(result[0]==true){
									e.preventDefault();
									alert("The car is not available");
									error = true;
									return false;
								} else {
									checkViewDuration().then((result2) => {
										if(result2[0] !== 0){
											alert(result2);
											error = true;	
											return false;
										} else {
											if(	error == false	){
												
												$(this).unbind('submit').submit()
											}
										}
									})
								}
							} )

													
						//}

						e.preventDefault();

						
						
					});
					
				</script>
						
			@endif
			<div class="calendar-add-edit-wrapper col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Calendar Entry Without Driver</h4>
					</div>
					<div class="panel-body">
						<form action="" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="type" value="4" />
							<select name="driveroptions" class="calendar-top-time form-control">
								@foreach ($nondriveroptions as $t)
									<option value="{{ $t }}">{{ $t }}</option>
								@endforeach
							</select>
							<input type="text" name="comments" class="calendar-comments-field form-control" placeholder="Comment" />
							<div class="clearfix"></div>
							<input type="date" name="datefrom" class="calendar-top-date form-control" required />
							<select name="timefrom" class="calendar-top-time form-control">
								@foreach ($times as $t)
									<option value="{{ $t }}">{{ $t }}</option>
								@endforeach
							</select>
							<div class="calendar-top-text">-</div>
							<select name="timeto" class="calendar-top-time form-control">
								@foreach ($times as $t)
									<option value="{{ $t }}">{{ $t }}</option>
								@endforeach
							</select>
							<div class="clearfix"></div>
							<input type="submit" value="Add" class="calendar-top-button nest-button btn btn-default" />
						</form>
					</div>
				</div>
			</div>
			<div class="calendar-add-edit-wrapper col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Vacation</h4>
					</div>
					<div class="panel-body">
						<form action="" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="type" value="3" />
							<input type="date" name="datefrom" class="calendar-top-date form-control" required />
							<div class="calendar-top-text">-</div>
							<input type="date" name="dateto" class="calendar-top-date form-control" required />
							<input type="submit" value="Add" class="calendar-top-button nest-button btn btn-default" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function showPast(){
		jQuery('.show-hide-past').css('display', 'none');
		jQuery('.hide-past').css('display', 'block');
		jQuery('#showpastlink').css('display', 'none');
	}
</script>



@endsection

@section('footer-script')
@endsection