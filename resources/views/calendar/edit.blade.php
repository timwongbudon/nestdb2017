@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="calendar-add-edit-wrapper col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Edit Calendar Entry</h4>
					</div>
					<div class="panel-body">
						<form action="" method="post" id="addViewing">
							{{ csrf_field() }}
							<input type="hidden" name="mode" value="{{ $mode }}" />
							<input type="hidden" name="id" value="{{ $cal->id }}" />
							@if ($cal->type == 2)
								<select name="driveroptions" class="calendar-top-time form-control">
									@foreach ($driveroptions as $t)
										<option value="{{ $t }}"
										@if ($t == $cal->driveroptions)
											selected
										@endif
										>{{ $t }}</option>
									@endforeach
								</select>
							@endif
							@if ($cal->type == 4)
								<select name="driveroptions" class="calendar-top-time form-control">
									@foreach ($nondriveroptions as $t)
										<option value="{{ $t }}"
										@if ($t == $cal->driveroptions)
											selected
										@endif
										>{{ $t }}</option>
									@endforeach
								</select>
							@endif
							<input type="date" name="datefrom" id="viewingdate" value="{{ $cal->datefrom }}" class="calendar-top-date form-control" required />
							@if ($cal->type == 3)
								<input type="date" name="dateto" value="{{ $cal->dateto }}" class="calendar-top-date form-control" required />
							@endif
							@if ($cal->type != 3)
								<select name="timefrom" id="timefrom" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}" 
										@if ($t == $cal->timefrom)
											selected
										@endif
										>{{ $t }}</option>
									@endforeach
								</select>
							@endif
							@if ($cal->type == 2 || $cal->type == 4)
								<div class="calendar-top-text">-</div>
								<select name="timeto" id="timeto" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}" 
										@if ($t == $cal->timeto)
											selected
										@endif
										>{{ $t }}</option>
									@endforeach
								</select>
							@endif
							@if ($cal->type == 2)
								<div class="calendar-top-text"></div>
								<select name="use_car" class="calendar-top-time form-control">
									<option value="1" @if ($cal->use_car == 1) selected @endif>Alphard</option>
									<option value="2" @if ($cal->use_car == 2) selected @endif>Audi A8</option>
								</select>
							@endif
							@if ($cal->type == 2 || $cal->type == 4)
								<div class="calendar-top-text"> </div>
								<input type="text" class="calendar-comments-field form-control" name="comments" value="{{ $cal->comments }}" placeholder="Comments" /> &nbsp; 
							@elseif ($cal->type == 3 && (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director']))
								<div class="calendar-top-text"> </div>
								<input type="text" class="calendar-comments-field form-control" name="comments" value="{{ $cal->comments }}" placeholder="Comments" /> &nbsp; 
							@endif
							<input type="submit" value="Save" class="calendar-top-button nest-button btn btn-default" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@if( $cal->type == 2)  
<script>


					async function checkDriverBooking(){
						var d = jQuery('#viewingdate').val();

						$timefrom = $('#timefrom').val();
						$timeto = $('#timeto').val();
						$car = jQuery('select[name="use_car"]').val();
					

						result = await $.ajax({
							url: "/calendar/carbooking_check/"+d+"/{{$cal->id}}/"+$timefrom+"/"+$timeto+"/"+$car,
							dataType: 'json',
							type: 'GET',
							contentType: 'application/json',
							//success: callback
						});
						return result;
						
					}

					async function checkViewDuration(){
						var d = jQuery('#viewingdate').val();

						$timefrom = $('#timefrom').val();
						$timeto = $('#timeto').val();

						//var numOfProp = $('.property-option:checked').length;
						var numOfProp = 100;
						
						result = await $.ajax({
							url: "/calendar/viewing_duration_check/"+d+"/"+numOfProp+"/"+$timefrom+"/"+$timeto,
							dataType: 'json',
							type: 'GET',
							contentType: 'application/json',
							//success: callback2
						});

						return result;
					
					}

				jQuery('#addViewing').on('submit', function(e){
				
						
						//var numOfProp = $('.property-option:checked').length;

						if( $('#viewingdate').val() == '' ){
							alert("Please Add date");
							return false;
						}
						
						var error = false;
						
						
					//	if($('.driveropt:checked').val() == 1){
							e.preventDefault();


							
							$car = jQuery('select[name="use_car"]').val();
							if( $car == "" ){
								e.preventDefault();
								alert("Please select a car");
								error = true;							
								return false;
							}
						

							/*if( numOfProp == 0){
								e.preventDefault();
								alert("Please select property to view");
								error = true;							
								return false;
							}*/

							checkDriverBooking().then( (result) => {
								
								
								if(result[0]==true){
									e.preventDefault();
									alert("The car is not available");
									error = true;
									return false;
								} else {
									checkViewDuration().then((result2) => {
										if(result2[0] !== 0){
											alert(result2);
											error = true;	
											return false;
										} else {
											if(	error == false	){
												
												$(this).unbind('submit').submit()
											}
										}
									})
								}
							} )

													
						//}

						e.preventDefault();

						
						
					});
					
				</script>

@endif


@endsection

@section('footer-script')
@endsection