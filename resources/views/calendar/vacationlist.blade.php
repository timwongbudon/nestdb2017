@extends('layouts.app')

@section('content')

            <!-- Current Clients -->
			@php
				
				
				
			@endphp
            @if (count($calendars) > 0)
				<div>
					<div class="nest-buildings-result-wrapper">
						@foreach ($calendars as $index => $vac)
							<div class="row 
							@if ($index%2 == 1)
								nest-striped
							@endif
							">

							@php		
							$thisVacation="";

								if(isset($vacation_remaining[$vac->user_id]) && !empty($users[$vac->user_id]->allowed_vacation) ){
									$thisVacation = current($vacation_remaining[$vac->user_id]);
									array_shift($vacation_remaining[$vac->user_id]);  
								
								}
								$rem="";
								$used = "";
								if(isset($userRemaining[$vac->id][$vac->user_id])  && !empty($users[$vac->user_id]->allowed_vacation) ){
									$rem = $userRemaining[$vac->id][$vac->user_id]['remaining_days'];
									
								}
								
								if(isset($userRemaining[$vac->id][$vac->user_id]['used_days'])){
									$used = $userRemaining[$vac->id][$vac->user_id]['used_days'];
								}
								
							@endphp

								<div class="col-lg-1 col-md-1 col-sm-6">
									<b>{{ $users[$vac->user_id]->name }} <!--{{$vac->id}} - {{$thisVacation}}   {{$rem}}-->  </b>
								</div>

								<div class="col-lg-3 col-md-3 col-sm-6">
									<!--{!! $vac->vacation_text !!}-->
									@if($used !=="")
										Total number of days used: <strong>{{$used}} days.</strong><br>
									@endif

									@if($rem !== "")
									
									Remaining days left: <strong>{{$rem}} days. </strong>
									@endif
								</div>
								<div class="col-lg-1 col-md-1 col-sm-6">
									<b>From:</b> {{ $vac->niceDateFrom() }}<br />
									<b>To:</b> {{ $vac->niceDateTo() }}
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<b>Status:</b> 
									@if ($vac->accepted == 0)
										Not Approved Yet
									@else
										Approved
									@endif
									<br />
									@if (trim($vac->comments) != '')
										<b>Comments:</b> {{ trim($vac->comments) }}
									@endif
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->id == $vac->user_id)
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('calendar/remove/'.$vac->id.'/1') }}">
											<i class="fa fa-btn fa-trash-o"></i>Delete
										</a>
										@if ($vac->accepted == 0 && (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director']))
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('calendar/approve/'.$vac->id) }}">
												<i class="fa fa-btn fa-pencil-square-o"></i>Approve
											</a>
										@endif
									@endif
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->id == $vac->user_id)
										<a class="nest-button nest-right-button btn btn-primary" href="{{ url('calendar/edit/'.$vac->id.'/1') }}">
											<i class="fa fa-btn fa-pencil"></i>Edit
										</a>
									@endif
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'])
										@if ($vac->accepted == 1)
											<a class="nest-button nest-right-button btn btn-primary" href="{{ url('calendar/noted/'.$vac->id) }}">
												<i class="fa fa-btn fa-check"></i>Noted
											</a>
										@endif
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
					@if ($calendars->lastPage() > 1)
						<div class="panel-footer nest-buildings-pagination">
							{{ $calendars->links() }}
						</div>
					@endif
				</div>			
            @endif
		<script>
			jQuery('form').on('focus', 'input[type=number]', function (e) {
				jQuery(this).on('mousewheel.disableScroll', function (e) {
					e.preventDefault();
				});
			});
			jQuery('form').on('blur', 'input[type=number]', function (e) {
				jQuery(this).off('mousewheel.disableScroll');
			});
		</script>
@endsection
