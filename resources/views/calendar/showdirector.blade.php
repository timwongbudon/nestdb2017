@extends('layouts.app')

@section('content')

<style>
h4{
	padding-bottom:0px !important;
}
.nest-property-edit-row{
	margin-left:-7.5px;
	margin-right:-7.5px;
}
</style>

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-lg-3 col-sm-4">
				<div class="panel panel-default">
					<div class="nest-calendar-heading panel-body text-center">
						<h4>{{ $months[$curmonth] }} {{ $curyear }}</h4>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-4">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<a href="/directorcalendar/{{ $prevmonth }}/{{ $prevyear }}"> << </a>
						
						<select id="month" onChange="shownewmonth()" class="calendar-top-month form-control">
							@for ($i = 1; $i <= 12; $i++)
								@if ($curmonth == $i)
									<option value="{{ $i }}" selected>{{ $months[$i] }}</option>
								@else
									<option value="{{ $i }}">{{ $months[$i] }}</option>
								@endif
							@endfor
						</select>
						<select id="year" onChange="shownewmonth()" class="calendar-top-month form-control">
							@for ($i = 2018; $i < ($nextyear+5); $i++)
								@if ($curyear == $i)
									<option value="{{ $i }}" selected>{{ $i }}</option>
								@else
									<option value="{{ $i }}">{{ $i }}</option>
								@endif
							@endfor
						</select>
						<a href="/directorcalendar/{{ $nextmonth }}/{{ $nextyear }}"> >> </a>
						
						<script>
							function shownewmonth(){
								var year = jQuery('#year').val();
								var month = jQuery('#month').val();
								window.location = '/directorcalendar/'+month+'/'+year;
							}
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						@if ($curmonth == intval(date('m')) && $curyear == intval(date('Y')))
							<a href="javascript:;" id="showpastlink" style="float:right;display:block;padding:10px;" onClick="showPast();">
								Show Past Entries
							</a>
						@endif
						<h4>Director Calendar</h4>
					</div>
					<div class="panel-body text-center">
						<div class="table-responsive">
							<table class="table">
								<tr>
									<th style="text-align:center;width:14.3%">Monday</th>
									<th style="text-align:center;width:14.3%;">Tuesday</th>
									<th style="text-align:center;width:14.3%;">Wednesday</th>
									<th style="text-align:center;width:14.3%;">Thursday</th>
									<th style="text-align:center;width:14.3%;">Friday</th>
									<th style="text-align:center;width:14.3%;">Saturday</th>
									<th style="text-align:center;width:14.2%;">Sunday</th>
								</tr>
								@php
									$index = 0;
								@endphp
								<tr>
									@for ($i = 0; $i < $firstday; $i++)
										<td>&nbsp;</td>
										@php
											$index++;
										@endphp
									@endfor
									@for ($i = 0; $i < $days; $i++)
										<td style="vertical-align:top !important;">
											@if ($curmonth == intval(date('m')) && $curyear == intval(date('Y')) && ($i+1) == intval(date('d')))
												<div style="font-weight:bold;color:#cc0000;"><u>{{ ($i+1) }}</u></div>
											@else
												<div class=""><u>{{ ($i+1) }}</u></div>
											@endif
											@if ($curmonth == intval(date('m')) && $curyear == intval(date('Y')) && ($i+1) < intval(date('d')))
												<div class="show-hide-past">...</div>
												<div class="hide-past" style="display:none;">
											@else
												<div>
											@endif
												@foreach ($times as $t)
													@if (isset($carr[($i+1)]))
														@foreach ($carr[($i+1)] as $c)
															@if ($c->timefrom == $t)
																@if ($c->type == 4)
																	@if ($c->directoronly == 1 && (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer']))
																		<div class="calendar-entry-handover calendar-entry">
																			<div class="calendar-entry-top">
																				@if (trim($users[$c->user_id]->picpath) != '')
																					<img src="{{ url($users[$c->user_id]->picpath) }}" 
																					@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																						title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																					@endif
																					/> 
																				@else
																					<img src="/showimage/users/dummy.jpg" 
																					@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																						title="Created: {{ $c->created_at() }}, Last updated: {{ $c->updated_at() }}" 
																					@endif
																					/> 
																				@endif
																				{{ $c->driveroptions }}
																				@if (Auth::user()->id == $c->user_id || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
																					<a href="/calendar/edit/{{ $c->id }}/0"><i class="fa fa-pencil"></i></a> 
																					<a href="/calendar/remove/{{ $c->id }}"><i class="fa fa-trash-o"></i></a> <br />
																				@endif
																				{{ $users[$c->user_id]->name }}
																			</div>
																			<div class="calendar-entry-time">{{ $c->timefrom }}-{{ $c->timeto }}</div>
																			@if (trim($c->comments) != '')
																				<div class="calendar-entry-comment">{{ $c->comments }}</div>
																			@endif
																		</div>
																	@endif
																@endif
															@endif
														@endforeach
													@endif	
												@endforeach
											</div>
										</td>
										@php
											$index++;
										@endphp
										@if ($index >= 7)
											</tr><tr>
											@php
												$index=0;
											@endphp
										@endif
									@endfor
									@for ($i = $index; $i < 7; $i++)
										<td>&nbsp;</td>
									@endfor
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="nest-property-edit-wrapper">
			@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer'])
				<div class="calendar-add-edit-wrapper col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Director Only Entry</h4>
						</div>
						<div class="panel-body">
							<form action="" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="type" value="4" />
								<input type="hidden" name="directoronly" value="1" />
								Entry for &nbsp; 
								<select name="userid" style="padding-left:5px;padding-right:5px;">
									@foreach ($users as $u)
										@if (($u->getUserRights()['Director'] || $u->getUserRights()['Admin']) && $u->status == 0)
											<option value="{{ $u->id }}">{{ $u->name }}</option>
										@endif
									@endforeach
								</select>
								<div class="clearfix"></div>
								<input type="text" name="driveroptions" class="calendar-comments-field form-control" placeholder="Title" style="margin-right:8px;margin-bottom:8px;" />
								<input type="text" name="comments" class="calendar-comments-field form-control" placeholder="Comment" />
								<div class="clearfix"></div>
								<input type="date" name="datefrom" class="calendar-top-date form-control" required />
								<select name="timefrom" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
								<div class="calendar-top-text">-</div>
								<select name="timeto" class="calendar-top-time form-control">
									@foreach ($times as $t)
										<option value="{{ $t }}">{{ $t }}</option>
									@endforeach
								</select>
								<div class="clearfix"></div>
								<input type="submit" value="Add" class="calendar-top-button nest-button btn btn-default" />
							</form>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>

<script>
	function showPast(){
		jQuery('.show-hide-past').css('display', 'none');
		jQuery('.hide-past').css('display', 'block');
		jQuery('#showpastlink').css('display', 'none');
	}
</script>



@endsection

@section('footer-script')
@endsection