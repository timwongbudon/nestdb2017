<!DOCTYPE html>
<html class="st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l2" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Nest Property Database</title>

    <!-- Fonts -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous"> -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700"> -->

    <!-- Styles -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> -->
    <link href="{{ env('HOST_ROOT') }}css/app.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ env('HOST_ROOT') }}css/vendor/all.css">
	<link href="{{ env('HOST_ROOT') }}css/app/app.css" rel="stylesheet">
	<link href="{{ env('HOST_ROOT') }}css/slick.css" rel="stylesheet">
	<link href="{{ env('HOST_ROOT') }}css/slick-theme.css" rel="stylesheet">
	<link href="{{ env('HOST_ROOT') }}css/chosen.min.css" rel="stylesheet">
	<link href="{{ env('HOST_ROOT') }}css/custom.css?v=57" rel="stylesheet">
	<link href="{{ env('HOST_ROOT') }}css/customnewdesign.css?v=1" rel="stylesheet">
	@yield('header-scripts')
	
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "admin",
      skins: {
        "default": {
          "primary-color": "#3498db"
        }
      }
    };
  </script>

	<script src="{{ env('HOST_ROOT') }}js/vendor/all.js?v=2"></script>
	<script src="{{ env('HOST_ROOT') }}js/slick.min.js"></script>
	<script src="{{ env('HOST_ROOT') }}js/chosen.jquery.min.js"></script>
	<script src="{{ env('HOST_ROOT') }}js/html5sortable.min.js"></script>
	<script src="{{ env('HOST_ROOT') }}js/chartjs.min.js"></script>
	<script src="{{ env('HOST_ROOT') }}js/chartjsannotations.min.js"></script>
</head>
<body>
	<script src="{{ env('HOST_ROOT') }}js/app/app.js?v=1"></script>
	
	<div class="st-container">
	
		<!-- Fixed navbar -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		  
		  <div class="container-fluid" @if(config('app.env') == 'localwg') style="background-color: #00FFFF !important;" @endif>
			<div class="navbar-header">
			  <a href="#sidebar-menu" data-toggle="sidebar-menu" data-effect="st-effect-3" class="toggle pull-left visible-xs"><i class="fa fa-bars"></i></a>

			  <!-- <a href="#sidebar-chat" data-toggle="sidebar-menu" class="toggle pull-right"><i class="fa fa-comments"></i></a> -->

			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a href="/" class="navbar-brand hidden-xs navbar-brand-primary" @if(config('app.env') == 'localwg') style="background-color: #00FFFF !important;" @endif>Nest Property</a>
			</div>
			@if (!Auth::guest() && !Auth::user()->isOnlyDriver())
					<form id="navbar-search-form" action="{{ url('/listsearch') }}" method="get" class="navbar-form navbar-left " role="search">
						<div class="search-2 nest-header-search-area">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Property Name, Property ID, Phone Number, Address" name="s" id="search-navbar" />
								<span class="input-group-btn">
									<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</div>
					</form>
				@endif

			<div class="navbar-collapse collapse" id="collapse">
				
				<ul class="nav navbar-nav navbar-right">
				<!-- notifications -->
				<!-- <li class="dropdown notifications updates hidden-xs hidden-sm">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-bell-o"></i>
					<span class="badge badge-primary">4</span>
				  </a>
				  <ul class="dropdown-menu" role="notification">
					<li class="dropdown-header">Notifications</li>
					<li class="media">
					  <div class="pull-right">
						<span class="label label-success">New</span>
					  </div>
					  <div class="media-left">
						<img src="images/people/50/guy-2.jpg" alt="people" class="img-circle" width="30">
					  </div>
					  <div class="media-body">
						<a href="#">Adrian D.</a> posted <a href="#">a photo</a> on his timeline.
						<br/>
						<span class="text-caption text-muted">5 mins ago</span>
					  </div>
					</li>
					<li class="media">
					  <div class="pull-right">
						<span class="label label-success">New</span>
					  </div>
					  <div class="media-left">
						<img src="images/people/50/guy-6.jpg" alt="people" class="img-circle" width="30">
					  </div>
					  <div class="media-body">
						<a href="#">Bill</a> posted <a href="#">a comment</a> on Adrian's recent <a href="#">post</a>.
						<br/>
						<span class="text-caption text-muted">3 hrs ago</span>
					  </div>
					</li>
					<li class="media">
					  <div class="media-left">
						<span class="icon-block s30 bg-grey-200"><i class="fa fa-plus"></i></span>
					  </div>
					  <div class="media-body">
						<a href="#">Mary D.</a> and <a href="#">Michelle</a> are now friends.
						<p>
						  <span class="text-caption text-muted">1 day ago</span>
						</p>
						<a href="">
						  <img class="width-30 img-circle" src="images/people/50/woman-6.jpg" alt="people">
						</a>
						<a href="">
						  <img class="width-30 img-circle" src="images/people/50/woman-3.jpg" alt="people">
						</a>
					  </div>
					</li>
				  </ul>
				</li> -->
				<!-- // END notifications -->
				<!-- messages -->
				<!-- <li class="dropdown notifications hidden-xs hidden-sm">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-envelope-o"></i>

					<span class="badge floating badge-danger">12</span>

				  </a>
				  <ul class="dropdown-menu">
					<li class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object thumb" src="images/people/50/guy-2.jpg" alt="people">
						</a>
					  </div>
					  <div class="media-body">
						<div class="pull-right">
						  <span class="label label-default">5 min</span>
						</div>
						<h5 class="media-heading">Adrian D.</h5>

						<p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					  </div>
					</li>
					<li class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object thumb" src="images/people/50/woman-7.jpg" alt="people">
						</a>
					  </div>

					  <div class="media-body">
						<div class="pull-right">
						  <span class="label label-default">2 days</span>
						</div>
						<h5 class="media-heading">Jane B.</h5>
						<p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					  </div>
					</li>
					<li class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object thumb" src="images/people/50/guy-8.jpg" alt="people">
						</a>
					  </div>

					  <div class="media-body">
						<div class="pull-right">
						  <span class="label label-default">3 days</span>
						</div>
						<h5 class="media-heading">Andrew M.</h5>
						<p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					  </div>
					</li>
				  </ul>
				</li> -->
				<!-- // END messages -->
				<!-- user -->
				
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
				@else
					@if (!Auth::user()->isOnlyDriver())
						@if (!empty($headerdata) && isset($headerdata['invoicenotes']) && $headerdata['invoicenotes'] > 0)
							<li class="notifications">
								<a href="/commission/invoicenote">
									<i class="fa fa-money"></i> <span class="badge floating badge-danger">{{ $headerdata['invoicenotes'] }}</span>
								</a>
							</li>
						@endif
						@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
							@if (!empty($headerdata) && isset($headerdata['invoices']) && $headerdata['invoices'] > 0)
								<li class="notifications">
									<a href="/commission/approval">
										<i class="fa fa-money"></i> <span class="badge floating badge-danger">{{ $headerdata['invoices'] }}</span>
									</a>
								</li>
							@endif
						@endif
						@if (!empty($headerdata) && isset($headerdata['warnings']) && $headerdata['warnings'] > 0)
							<li class="notifications">
								<a href="/">
									<i class="fa fa-user"></i> <span class="badge floating badge-danger">{{ $headerdata['warnings'] }}</span>
								</a>
							</li>
						@endif
						@if (!empty($headerdata) && isset($headerdata['new']) && $headerdata['new'] > 0)
							<li class="notifications">
								<a href="/">
									<i class="fa fa-bell-o"></i> <span class="badge floating badge-danger">{{ $headerdata['new'] }}</span>
								</a>
							</li>
						@endif
						@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
							@if (!empty($headerdata) && isset($headerdata['websiteform']) && $headerdata['websiteform'] > 0)
								<li class="notifications">
									<a href="/websitenquiries">
										<i class="fa fa-envelope"></i> <span class="badge floating badge-danger">{{ $headerdata['websiteform'] }}</span>
									</a>
								</li>
							@endif
						@elseif (Auth::user()->getUserRights()['Informationofficer'])
							@if (!empty($headerdata) && isset($headerdata['websiteform']) && $headerdata['websiteform'] > 0)
								<li class="notifications">
									<a href="/websitenquiriesinfo">
										<i class="fa fa-envelope"></i> <span class="badge floating badge-danger">{{ $headerdata['websiteform'] }}</span>
									</a>
								</li>
							@endif
						@endif
						@if (!empty($headerdata) && isset($headerdata['messages']) && $headerdata['messages'] > 0)
							<li class="notifications">
								<a href="/websitenquiriesforwarded">
									<i class="fa fa-envelope"></i> <span class="badge floating badge-danger">{{ $headerdata['messages'] }}</span>
								</a>
							</li>
						@endif
						@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
							@if (!empty($headerdata) && isset($headerdata['vacation']) && $headerdata['vacation'] > 0)
								<li class="notifications">
									<a href="/vacationlist">
										<i class="fa fa-plane"></i> <span class="badge floating badge-danger">{{ $headerdata['vacation'] }}</span>
									</a>
								</li>
							@endif
						@endif
						@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'])
							@if (!empty($headerdata) && isset($headerdata['invoice']) && $headerdata['invoice'] > 0)
								<li class="notifications">
									<a href="/commission/invoicing">
										<i class="fa fa-dollar"></i> <span class="badge floating badge-danger">{{ $headerdata['invoice'] }}</span>
									</a>
								</li>
							@endif
						@endif
					@endif
					<li class="dropdown user">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						@if (Auth::user()->picpath == "")
							<img src="{{ url('/showimage/users/dummy.jpg') }}" alt="" class="img-circle" />
						@else
							<img src="{{ url(Auth::user()->picpath) }}" alt="" class="img-circle" />
						@endif
						&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu" role="menu">
						@if (Auth::user()->status == 0)
							<li><a href="{{ url('/user/profile') }}"><i class="fa fa-user"></i>Profile</a></li>
							<li><a href="{{ url('/user/settings') }}"><i class="fa fa-wrench"></i>Settings</a></li>
							@if ((Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director']) && !(Auth::user()->getUserRights()['Admin']))
								<li><a href="{{ url('/user/admusers') }}"><i class="fa fa-users"></i>Users</a></li>
								<li><a href="{{ url('/commission/targetusers') }}"><i class="fa fa-bullseye"></i>Targets</a></li>
							@endif
						@endif
						<li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i>Logout</a></li>
					  </ul>
					</li>
				@endif
				
				<!-- // END user -->
				<!-- country flags -->
				<!-- // END country flags -->
			  </ul>
			</div>
		  </div>
		</div>
		
		
		
		
		
		
		
		
		
		
	<div class="st-pusher">
	
		<div class="sidebar left sidebar-size-2 sidebar-offset-0 sidebar-skin-blue sidebar-visible-desktop" id="sidebar-menu" data-type="collapse">
        <div class="split-vertical">
          <div class="split-vertical-body">
            <div class="split-vertical-cell">
              <div class="tab-content">
                <div class="tab-pane active" id="sidebar-tabs-menu">
                    <ul class="sidebar-menu sm-icons-right sm-icons-block">
					
					@if (Auth::guest() || Auth::user()->status == 0)
						<style>
							a.current-{{ str_replace('/', '-', Route::getCurrentRoute()->getPath()) }}{
								text-decoration:underline;
							}
						</style>
						@if (Auth::guest())
							<li><a href="{{ url('/login') }}"><i class="fa fa-book"></i> <span>Login</span></a></li>
						@else
							@if (Auth::user()->isOnlyDriver())
								<li><a href="{{ url('/calendar') }}" class="current-calendar"><i class="fa fa-bullseye"></i> <span>Calendar</span></a></li>
								<li><a href="{{ url('/vacationlists') }}" class="current-vacationlist" style="font-size:12px;line-height:2;">Vacations</a></li>
							@else
								@if ((Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant']) && !(Auth::user()->getUserRights()['Admin']))
									<li><a href="{{ url('/companyoverview') }}" class="current-companyoverview" style="line-height:1.5;padding-top:10px;"><i class="fa fa-dollar"></i> <span>Company</span></a></li>
								@endif
								@if ((Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director']) && !(Auth::user()->getUserRights()['Admin']))
									<li><a href="{{ url('/companydbusage') }}" class="current-companydbusage" style="font-size:12px;line-height:2;"><span>Database Usage</span></a></li>
									<li><a href="{{ url('/companydbtime') }}" class="current-companydbtime" style="font-size:12px;line-height:2;"><span>Time on Database</span></a></li>									
								@endif
								@if (!(Auth::user()->getUserRights()['Admin']))
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->getUserRights()['Director'])
										<li><a href="{{ url('/commission/invoicing') }}" class="current-commission-invoicing" style="line-height:1.5;padding-top:10px;"><i class="fa fa-money"></i> <span>Invoices</span></a></li>
										<li><a href="{{ url('/commission/geninvoicing') }}" class="current-geninvoicing" style="font-size:12px;line-height:2;">Generic Invoices</a></li>
										<li><a href="{{ url('/commission/invoicescsv') }}" class="current-invoicescsv" style="font-size:12px;line-height:2;">Invoice CSV</a></li>
										<li><a href="{{ url('/commission/salaryusers') }}" class="current-salaryusers" style="font-size:12px;line-height:2;">Salary</a></li>
										<li><a href="{{ url('/commission/salariescsv') }}" class="current-salariescsv" style="font-size:12px;line-height:2;">Salary CSV</a></li>
										<li><a href="{{ url('/commission/bonusdividends') }}" class="current-bonusdividends" style="font-size:12px;line-height:2;">Bonus/Dividend</a></li>
										<li><a href="{{ url('/commission/expensesusers') }}" class="current-expensesusers" style="font-size:12px;line-height:2;">Expenses	</a></li>
										<li><a href="{{ url('/informationofficer/commission') }}" class="current-informationofficercommission" style="font-size:12px;line-height:2;">Information Officer Commissions</a></li>
									@endif
								@endif
								@if ( Auth::user()->getUserRights()['Consultant'] && Auth::user()->getUserRights()['Informationofficer']  )
									<li><a href="{{ url('/dashboardnew') }}" class="current-dashboardnew" style="line-height:1.5;padding-top:10px;"><i class="fa fa-bullseye"></i> <span>Dashboard</span></a></li>
								@elseif (Auth::user()->getUserRights()['Informationofficer'])
									<li><a href="{{ url('/calendar') }}" class="current-dashboardnew" style="line-height:1.5;padding-top:10px;"><i class="fa fa-bullseye"></i> <span>Calendar</span></a></li>
								@else
									<li><a href="{{ url('/dashboardnew') }}" class="current-dashboardnew" style="line-height:1.5;padding-top:10px;"><i class="fa fa-bullseye"></i> <span>Dashboard</span></a></li>
									<li><a href="{{ url('/calendar') }}" class="current-calendar" style="font-size:12px;line-height:2;">Calendar</a></li>
								@endif
								@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Informationofficer'])
									<li><a href="{{ url('/directorcalendar') }}" class="current-directorcalendar" style="font-size:12px;line-height:2;">Director Calendar</a></li>
								@endif
								<li><a href="{{ url('/carbooking') }}" class="current-carbooking" style="font-size:12px;line-height:2;">Car Bookings (Current Week)</a></li>
								<!-- @if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Accountant'])
									<li><a href="{{ url('/vacationlist') }}" class="current-vacationlist" style="font-size:12px;line-height:2;">Vacations</a></li>
								@endif -->
								<li><a href="{{ url('/vacationlists') }}" class="current-vacationlist" style="font-size:12px;line-height:2;">Vacations</a></li>
								<li><a href="{{ url('/websitenquiriesforwarded') }}" class="current-websitenquiriesforwarded" style="font-size:12px;line-height:2;">Messages</a></li>
								<li><a href="{{ url('/iopropertycreate') }}" class="current-iopropertycreate" style="font-size:12px;line-height:2;">Info Officer</a></li>

								@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Admin'])
									<li><a href="{{ url('/market-insight') }}" class="current-companydbtime" style="font-size:12px;line-height:2;"><span>Market Insight</span></a></li>
								@endif

								@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
									<li><a href="{{ url('/websitenquiries') }}" class="current-websitenquiries"><i class="fa fa-globe"></i> <span>Website Enquiries</span></a></li>
								@elseif (Auth::user()->getUserRights()['Informationofficer'])
									<li><a href="{{ url('/websitenquiriesinfo') }}" class="current-websitenquiries"><i class="fa fa-globe"></i> <span>Landlord Enquiries</span></a></li>
								@endif
								@if (Auth::user()->getUserRights()['Informationofficer'])
									<li><a href="{{ url('/list?advanced=1') }}" class="current-list" style="line-height:1.5;padding-top:10px;"><i class="fa fa-home"></i> <span>Properties</span></a></li>
								@else
									<li><a href="{{ url('/list') }}" class="current-list" style="line-height:1.5;padding-top:10px;"><i class="fa fa-home"></i> <span>Properties</span></a></li>
								@endif
								<!-- <li><a href="{{ url('/hotproperties') }}" style="font-size:12px;line-height:2;">Hot Properties</a></li> -->
								<li><a href="{{ url('/newproperties') }}" class="current-newproperties" style="font-size:12px;line-height:2;">Available Listings</a></li>
								<li><a href="{{ url('/newcreatedprops') }}" class="current-newcreatedprops" style="font-size:12px;line-height:2;">New Listings</a></li>
								<li><a href="{{ url('/properties/new') }}" class="current-properties-new" style="font-size:12px;line-height:2;">Property Updates</a></li>
								<li><a href="{{ url('/upcomingleases') }}" class="current-upcomingleases" style="font-size:12px;line-height:2;">Leases Coming Up</a></li>
								<li><a href="{{ url('/bonuscomm') }}" class="current-bonuscomm" style="font-size:12px;line-height:2;">Bonus Commission</a></li>
								<li><a href="{{ url('/property/photoshoot') }}" class="current-photoshoot" style="font-size:12px;line-height:2;">Photoshoot</a></li>
								@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Admin'])
								<li><a href="{{ url('/property/tobedeleted') }}" class="current-photoshoot" style="font-size:12px;line-height:2;">To be Deleted Properties</a></li>
								@endif
								
								@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'] || Auth::user()->getUserRights()['Admin'] || Auth::user()->getUserRights()['Informationofficer'])
								<li><a href="{{ url('/property/deletedproperties') }}" class="current-photoshoot" style="font-size:12px;line-height:2;">List of Deleted Properties</a></li>
								<li><a href="{{ url('/commission/propertyavailabledate') }}" class="current-leasedproperties" style="font-size:12px;line-height:2;">Property Available Date</a></li>
								@endif

								<li><a href="{{ url('/buildings') }}" class="current-buildings"><i class="fa fa-building"></i> <span>Building</span></a></li>
								<li><a href="{{ url('/vendors') }}" class="current-vendors"><i class="fa fa-arrows-alt"></i> <span>Vendor</span></a></li>
								@if (!Auth::user()->isOnlyPhotographer())
									<li><a href="{{ url('/clients') }}" class="current-clients"><i class="fa fa-user"></i> <span>Client</span></a></li>
									<li><a href="{{ url('/shortlists') }}" class="current-shortlists" style="line-height:1.5;padding-top:10px;"><i class="fa fa-list"></i> <span>Shortlist</span></a></li>
									<li><a href="{{ url('/allshortlists') }}" class="current-allshortlists" style="font-size:12px;line-height:2;">Open & Closed Shortlists</a></li>
								@endif
								@if (!(Auth::user()->getUserRights()['Admin']))
									@if (Auth::user()->getUserRights()['Consultant'])
										<li><a href="{{ url('/commission/invoicing') }}" class="current-commission-invoicing"><i class="fa fa-money"></i> <span>Invoices</span></a></li>
									@endif
								@endif
								<li><a href="{{ url('/user/list') }}" class="current-user-list" style="line-height:1.5;padding-top:10px;"><i class="fa fa-users"></i> <span>Staff Members</span></a></li>
								<li><a href="{{ url('/commission/mysalaries') }}" class="current-mysalary" style="font-size:12px;line-height:2;">My Salary</a></li>
								<li><a href="{{ url('/commission/myexpenses') }}" class="current-myexpenses" style="font-size:12px;line-height:2;">My Expenses</a></li>
								
								@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Accountant'] || Auth::user()->getUserRights()['Informationofficer'] || Auth::user()->getUserRights()['Director'])
									<li><a href="{{ url('/resources') }}" class="current-resources" style="line-height:1.5;padding-top:10px;"><i class="fa fa-book"></i> <span>Resources</span></a></li>
									<li><a href="{{ url('/basedocuments') }}" class="current-basedocuments" style="font-size:12px;line-height:2;">Documents</a></li>
								@else
									<li><a href="{{ url('/resources') }}" class="current-resources"><i class="fa fa-book"></i> <span>Resources</span></a></li>
								@endif
								@if (!(Auth::user()->getUserRights()['Admin']))
									@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Informationofficer'] || Auth::user()->getUserRights()['Director'])
										<li><a href="{{ url('/csvindex') }}" class="current-csvindex"><i class="fa fa-arrow-up"></i> <span>CSV Import</span></a></li>
									@endif
									@if (Auth::user()->getUserRights()['Superadmin'])
										<li><hr /></li>
										@if (Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Informationofficer'])
											<li><a href="#" style="line-height:2;padding-top:10px;"><u>Tools</u></a></li>
											<li><a href="{{ url('/trash/properties') }}" style="font-size:12px;line-height:2;">Trash Properties Search</a></li>
										@endif
										@if (Auth::user()->getUserRights()['Superadmin'])
											<li><a href="{{ url('/incomplete/properties') }}" style="font-size:12px;line-height:2;">Incomplete Properties Search</a></li>
											<li><a href="{{ url('/imports') }}" style="font-size:12px;line-height:2;">Bulk Import with CSV file</a></li>
											<li><a href="{{ url('/districts') }}" style="font-size:12px;line-height:2;">Website Districts (For Programmer)</a></li>
											<li><a href="{{ url('/xerologs') }}" style="font-size:12px;line-height:2;">Xero Logs</a></li>
										@endif
										<li><a href="#" style="line-height:2;padding-top:10px;"><u>Old</u></a></li>
										<li><a href="{{ url('/properties') }}" style="font-size:12px;line-height:2;">Properties</a></li>
										<li><a href="{{ url('/frontend/properties') }}" style="font-size:12px;line-height:2;">Website Prop.</a></li>
										<li><a href="{{ url('/whatsnew') }}" style="font-size:12px;line-height:2;">What's New</a></li>
									@endif
								@endif
							@endif
						@endif
					@else
						<li><a href="{{ url('/logout') }}"><i class="fa fa-book"></i> <span>Logout</span></a></li>
					@endif
					  
                    </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
	  
	  
	  
      <div class="st-content" id="content">
        <!-- extra div for emulating position:fixed of the menu -->
        <div class="st-content-inner-custom">

          <div class="container-fluid">

			<div id="client_shortlist">
				@include('partials.shortlist')
			</div>
			@if (Auth::guest() || Auth::user()->status == 0)
				@yield('content')
			@else
				<div class="nest-new admusers">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="col-xs-12">
									<h4>Account Suspended</h4>
								</div>
							</div>
							<div class="panel-body">
								Your account has been suspended. Please contact directors or IT to solve this problem.
							</div>
						</div>
					</div>
				</div>
			@endif
	  
	      </div>
	    </div>
	  </div>
	</div>
		
	</div>
	
<script type="text/javascript">
  $(window).load(function(){
    $("#nest-search-panel, #nest-search-action").show();  
	//$('.st-content-inner-custom').niceScroll();
  });
</script>

@include('partials.include-easy-autocomplete')
@include('partials.property.building-modal-control')
@yield('footer-script')

@include('partialsnew.showpropertymodal')

</body>
</html>
