@extends('layouts.app')

@section('content')

            <!-- Building Search -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    District Search
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <a href="{{ url('districts') }}" class="btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>All Districts
                            </a>
                            <a href="{{ url('districts?web=1') }}" class="btn btn-default">
                                <i class="fa fa-btn fa-search"></i>Districts for Web
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Current Districts -->
            @if (count($districts) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Districts
                    </div>

                    <div class="panel-body">
                    <form id="select_frontend_district" action="{{ url('districts/select') }}" method="post">
                        {{ csrf_field() }}
                        <table class="table table-striped district-table">
                            <thead>
                                <th style="width: 5%">&nbsp;</th>
                                <th style="width: 5%">ID</th>
                                <th class="col-sm-4">District</th>
                                <th>Web</th>
                                <!-- <th style="width: 8%">&nbsp;</th>
                                <th style="width: 8%">&nbsp;</th> -->
                            </thead>
                            <tbody>
                                @foreach ($districts as $district)
                                    <tr>
                                        <td class="table-text"><div>{!! Form::checkbox('options_ids[]', $district->id, null, ['class'=>'district-option', 'id'=>'district-option_id-checkbox-' . $district->id]) !!}</div></td>
                                        <td class="table-text"><div>{{ $district->id }}</div></td>
                                        <td class="table-text"><div>{{ $district->name }}</div></td>
                                        <td class="table-text"><div>{{ $district->web }}</div></td>

                                        <!-- District Delete Button -->
                                        <!-- <td>
                                            <a class="btn btn-primary" href="{{ url('building/show/'.$district->id) }}">
                                                <i class="fa fa-btn fa-info-circle"></i>View
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('building/edit/'.$district->id) }}">
                                                <i class="fa fa-btn fa-pencil-square-o"></i>Edit
                                            </a>
                                        </td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ Form::hidden('action', 'select') }}
                    </form>
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $districts->currentPage() }} of {{ $districts->lastPage() }} (Total: {{ $districts->total() }} items)</div>
                        {{ $districts->links() }}
                    </div>
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button id="add_frontend_district" class="btn btn-primary">
                                    <i class="fa fa-btn fa-plus"></i>Add Districts to Front End Website
                                </button>
                                <button id="remove_frontend_district" class="btn btn-primary">
                                    <i class="fa fa-btn fa-minus"></i>Remove Districts to Front End Website
                                </button>
                            </div>
                        </div>
                </div>
            </div>

@endsection

@section('footer-script')
<script type="text/javascript">
$(document).ready(function(){
    $('button#add_frontend_district.btn').click(function(event){
        event.preventDefault();
        submit_action('select');
        return false;
    });
    $('button#remove_frontend_district.btn').click(function(event){
        event.preventDefault();
        submit_action('deselect');
        return false;
    });
    function submit_action(action) {
        if ($('form#select_frontend_district input.district-option:checked').length > 0) {
            if (action == 'deselect') {
                $('form#select_frontend_district input[name=action]').val('deselect');
            }
            $('form#select_frontend_district').submit();
        } else {
            alert('Please choose district to put into frontend district list.');
        }
    }
});
</script>
@endsection