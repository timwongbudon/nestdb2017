@extends('layouts.app')

@section('content')

            <!-- Building Search -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    District Search
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <a href="{{ url('districts') }}" class="btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>All Districts
                            </a>
                            <a href="{{ url('districts?web=1') }}" class="btn btn-default">
                                <i class="fa fa-btn fa-search"></i>Districts for Web
                            </a>
                        </div>
                    </div>
                </div>
            </div>

<form action="{{ url('districts/option/reorder') }}" method="POST" class="form-horizontal">{{ csrf_field() }}
            <!-- Current Districts -->
            @if (count($districts) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Districts <?php if(isset($_GET['web']) && $_GET['web']==1)echo 'for web';?>
                    </div>

                    <div class="panel-body">
<ul id="sortable-1" class="container sortable-listing">
@foreach ($districts as $district)
    <li id="{{ $district->id }}" class="default col-sm-12">
    {{ Form::hidden('options_ids[]', $district->id) }}
    <span class="col-sm-1">{{ $district->id }}</span>
    <span class="col-sm-4">{{ $district->name }}</span>
    </li>
@endforeach
</ul>
                        
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $districts->currentPage() }} of {{ $districts->lastPage() }} (Total: {{ $districts->total() }} items)</div>
                        {{ $districts->links() }}
                    </div>
                </div>
            @endif
            
            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                    <button id="reorder_option" class="btn btn-primary">
                                        <i class="fa fa-btn fa-arrows"></i>Re-order
                                    </button>

                                    <a href="{{url('districts/export')}}" target="_blank" class="btn btn-primary">
                                        <i class="fa fa-btn fa-download"></i>Export Code
                                    </a>
                            </div>
                        </div>
                </div>
            </div>
</form>

@endsection

@section('footer-script')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
#sortable-1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 100%;
}

#sortable-1 li {
    padding: 0.4em;
    padding-left: 1.5em;
    height: auto;
    cursor: move;
}

.default {
    background: #fff;
    border: 1px solid #DDDDDD;
    color: #333333;
}
</style>
<script>
$(document).ready(function(){
    $( "#sortable-1" ).sortable();
});
$(document).ready(function(){
    $("#reorder_option").click(function(){
        event.preventDefault();
        $('form').submit();
    });
});
</script>
@endsection