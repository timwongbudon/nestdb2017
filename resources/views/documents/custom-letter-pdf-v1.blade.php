<html>
<head>

<title>Custom Letter</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-sl-top-left{
	padding-top:180px;
	padding-bottom:0px;
}
.nest-pdf-sl-bold{
	font-family:MyriadBold;
}
.nest-pdf-sl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-sl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-sl-content{
	text-align:justify;
}
.nest-pdf-sl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-100px;
	text-align:right;
	padding-top:60px;
}
.nest-hide-first-header img{
	width:140px;
	padding-right:20px;
}
.nest-pdf-sl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-sl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-sl-faithfully{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-1{
	padding-top:17px;
}
.nest-pdf-lfl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-lfl-faithfully{
	
}
.nest-pdf-lfl-signature{
	padding:20px 0px 10px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
				<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		@elseif (!empty($logo) && $logo == 'nonest')
			<div class="nest-pdf-letterhead" style="width:152px;">
		@else
			<div class="nest-pdf-letterhead" style="width:152px;">
				<br /><br /><br /><br /><br /><br /><br /><br />
		@endif
		@if (!empty($logo) && $logo == 'nonest')
			<div class="nest-pdf-letterhead-text">
				{!! nl2br($data['fieldcontents']['f13']) !!}
			</div>
		@else
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com
			</div>
		@endif
		</div>
		<div class="nest-pdf-sl-top-left">
			To: {{ $data['fieldcontents']['f2'] }}<br />
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br /><br />
			@endif
			<br /><br /><br /><br /><br /><br />
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			Dear {{ $data['fieldcontents']['f4'] }},
		</div>
		<div class="nest-pdf-sl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			{!! nl2br($data['fieldcontents']['f6']) !!}
		</div>
		@if (trim($data['fieldcontents']['f7']) == 'Consultant')
			<table cellpadding="0" cellspacing="0" border="0"><tr><td>
				<div class="nest-pdf-lfl-bottom-1">
					Yours sincerely, 	
				</div>
				<div class="nest-pdf-lfl-signature">
					@if ($consultant->sigpath != '')
						<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" />
					@else
						<br /><br /><br />
					@endif
				</div>
				<div class="nest-pdf-lfl-consultant">
					{{ $data['fieldcontents']['f8'] }}<br />
					{{ $data['fieldcontents']['f9'] }}<br />
					Residential Search & Investment
				</div>
				<div class="nest-pdf-lfl-licensnr">
					Individual Licence No: {{ $data['fieldcontents']['f10'] }}
				</div>
			</td></tr></table>
		@elseif (trim($data['fieldcontents']['f7']) == '1 Party')
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%"></td>
				</tr>
				<tr>
					<td>
					@if (isset($data['fieldcontents']['f11']) && trim($data['fieldcontents']['f11']) != '')
						{{ trim($data['fieldcontents']['f11']) }}
					@endif
					</td>
					<td></td>
					<td>
					</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		@elseif (trim($data['fieldcontents']['f7']) == '2 Parties')
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
				</tr>
				<tr>
					<td>
					@if (isset($data['fieldcontents']['f11']) && trim($data['fieldcontents']['f11']) != '')
						{{ trim($data['fieldcontents']['f11']) }}
					@endif
					</td>
					<td></td>
					<td>
					@if (isset($data['fieldcontents']['f12']) && trim($data['fieldcontents']['f12']) != '')
						{{ trim($data['fieldcontents']['f12']) }}
					@endif
					</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td></td>
					<td>Date:</td>
				</tr>
			</table>
		@endif
	</div>
</body>
</html>

















