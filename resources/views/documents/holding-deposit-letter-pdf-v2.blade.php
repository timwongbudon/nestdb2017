<html>
<head>

<title>Holding Deposit Letter</title>


<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 5cm 1.5cm 0.5cm 1.5cm;
}
#footer {
	height: 10px !important;
	margin-bottom: -3px !important;
}
#footer a {
	background-color:#C5C2C0 !important;
}
#header {
	top: -153px;
	padding-bottom:5px;
	z-index:10;
}
.nest-hide-first-header{
	margin-top:-238px;
}
</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com
			</div>
        </div>
        @if (empty($logo) || $logo == 'yes')
            <br><br><br>
        @else
        <br><br><br><br><br><br><br><br><br><br><br><br>
        @endif
		<div class="nest-pdf-hdl-top-left">
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx
				<br /><br />
			@endif
			{{ $data['fieldcontents']['f2'] }}<br />
			@php
				$e = explode("\n", trim($data['fieldcontents']['f3']));
				echo nl2br(trim($data['fieldcontents']['f3']));
				for ($i = count($e); $i <= 5; $i++){
					//echo '<br />';
				}
			@endphp
			<br /><br /><br /><br />
			Dear {{ $data['fieldcontents']['f4'] }},
		</div>
		<div class="nest-pdf-hdl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-hdl-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else
				@if (is_numeric($data['fieldcontents']['f6']))
					We herewith enclose a cheque for the sum of HK${{ number_format($data['fieldcontents']['f6'], 0, '', ',') }} from the Tenant (cheque no:______________________) being the initial deposit for the above-mentioned premises.
				@else
					We herewith enclose a cheque for the sum of HK$xxxxxx from the Tenant (cheque no:______________________) being the initial deposit for the above-mentioned premises.
				@endif
				<br /><br />
				As stipulated in our offer letter, this holding deposit shall be refunded to the Tenant in full should either party fail to sign the Tenancy Agreement.
			@endif
		</div>
		
		<table><tr><td>
			<div class="nest-pdf-hdl-bottom-1">
				<br />
				Please acknowledge receipt by countersigning a copy of the attached letter.<br /><br />
				Yours sincerely, 	
			</div>
			<div class="nest-pdf-hdl-signature">
				@if ($consultant->sigpath != '')
					<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}"  style="max-height:80px;max-width:300px;" />
				@else
					<br /><br /><br />
				@endif
			</div>
			<div class="nest-pdf-hdl-consultant">
				{{ $data['fieldcontents']['f7'] }}<br />
				{{ $data['fieldcontents']['f8'] }}<br />
				Residential Search & Investment
			</div>
			<div class="nest-pdf-hdl-licensnr">
				Individual Licence No: {{ $data['fieldcontents']['f9'] }}
			</div>
			
		</td></tr></table>
	</div>
</body>
</html>