<html>
<head>

<title>Letter of Undertaking</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">

<style>
.nest-pdf-lou-bold{
	font-family:myriadbold;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-lou-paragraph{
	padding-bottom:20px;
}
.nest-pdf-lou-content{
	padding-bottom:40px;
	text-align:justify;
	line-height: 1.2;
}
.nest-pdf-lou-top-left {
	margin-top: 8px !important;
}
</style>

</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com
			</div>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br>
		<div class="nest-pdf-lou-top-left">
			<div class="nest-pdf-lou-bold">LETTER OF UNDERTAKING:</div><br />
			<div class="nest-pdf-lou-bold">Re: {{ $data['fieldcontents']['f2'] }}
		</div>
		<br /><br />
		<div class="nest-pdf-lou-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else
				The Landlord hereby declares that the Said Premises has been mortgaged to secure for repayment of certain advancement made by the Mortgage Bank or Financial Institution.  The Tenant hereby agrees that the Landlord shall not be obliged to, and the Tenant hereby waives the requirement that the Landlord shall, obtain the consent of the mortgagee of the Premises (“the Mortgagee”) to the signing of this Agreement.  In the event that the Mortgagee shall commence a mortgagee action against the Landlord to re-possess the Said Premises or shall otherwise exert its title to the Said Premises against the Tenant, the Tenant shall be entitled to terminate this Agreement forthwith and recover the Deposit.  In such event, the Landlord agrees to indemnify the Tenant against any loss or damage incurred by the Tenant in connection with this Agreement.  The Landlord will be responsible for the Tenant’s relocation expenses including but not limited to agency fees, legal fees, stamp duty etc.
			@endif
		</div>
		<div class="nest-pdf-lou-paragraph">
			@if (trim($data['fieldcontents']['f1']) != '')
				Dated {{ \NestDate::nest_dayofmonth_datetime_format($data['fieldcontents']['f1']) }}
			@else
				Dated xxxxxx
			@endif
		</div>
		<br />
		<div class="nest-pdf-lou-paragraph">
			Confirmed the receipt of the above by:
		</div>
		
		<table width="100%" style="padding-top:80px;">
			<tr>
				<td width="40%" style="border-bottom:1px solid #000000;"></td>
				<td width="20%"></td>
				<td width="40%" style="border-bottom:1px solid #000000;"></td>
			</tr>
			<tr>
				<td>
				@if (isset($data['fieldcontents']['f3']) && trim($data['fieldcontents']['f3']) != '')
					{{ trim($data['fieldcontents']['f3']) }}
				@endif
				</td>
				<td></td>
				<td>
				@if (isset($data['fieldcontents']['f4']) && trim($data['fieldcontents']['f4']) != '')
					{{ trim($data['fieldcontents']['f4']) }}
				@endif
				</td>
			</tr>
			<tr>
				<td>Landlord</td>
				<td></td>
				<td>Tenant</td>
			</tr>
		</table>
	</div>
</body>
</html>