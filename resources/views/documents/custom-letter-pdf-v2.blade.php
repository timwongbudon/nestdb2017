<html>
<head>

<title>Custom Letter</title>

<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 5cm 1.5cm 0.5cm 1.5cm;
}
#footer {
	height: 10px !important;
	margin-bottom: -3px !important;
}
#footer a {
	background-color:#C5C2C0 !important;
}
#header {
	top: -153px;
	padding-bottom:5px;
	z-index:10;
}
.nest-hide-first-header{
	margin-top:-238px;
}
</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>    
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com
			</div>
        </div>
        @if (empty($logo) || $logo == 'yes')
            <br><br><br>
        @else
        <br><br><br><br><br><br><br><br><br><br><br><br>
        @endif
        
		<div class="nest-pdf-sl-top-left">
			To: {{ $data['fieldcontents']['f2'] }}<br />
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br /><br />
            @endif
            
            <br />    
            
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			<br />
			Dear {{ $data['fieldcontents']['f4'] }},
            
            
			
		</div>
		<div class="nest-pdf-sl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-sl-content">
			{!! nl2br($data['fieldcontents']['f6']) !!}
		</div>
		@if (trim($data['fieldcontents']['f7']) == 'Consultant')
			<table cellpadding="0" cellspacing="0" border="0"><tr><td>
				<div class="nest-pdf-lfl-bottom-1">
					Yours sincerely, 	
				</div>
				<div class="nest-pdf-lfl-signature">
					@if ($consultant->sigpath != '')
						<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" style="max-height:80px;max-width:300px;" />
					@else
						<br /><br /><br />
					@endif
				</div>
				<div class="nest-pdf-lfl-consultant">
					{{ $data['fieldcontents']['f8'] }}<br />
					{{ $data['fieldcontents']['f9'] }}<br />
					Residential Search & Investment
				</div>
				<div class="nest-pdf-lfl-licensnr">
					Individual Licence No: {{ $data['fieldcontents']['f10'] }}
				</div>
			</td></tr></table>
		@elseif (trim($data['fieldcontents']['f7']) == '1 Party')
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%"></td>
				</tr>
				<tr>
					<td>
					@if (isset($data['fieldcontents']['f11']) && trim($data['fieldcontents']['f11']) != '')
						{{ trim($data['fieldcontents']['f11']) }}
					@endif
					</td>
					<td></td>
					<td>
					</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		@elseif (trim($data['fieldcontents']['f7']) == '2 Parties')
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-top:80px;">
				<tr>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
					<td width="20%"></td>
					<td width="40%" style="border-bottom:1px solid #000000;"></td>
				</tr>
				<tr>
					<td>
					@if (isset($data['fieldcontents']['f11']) && trim($data['fieldcontents']['f11']) != '')
						{{ trim($data['fieldcontents']['f11']) }}
					@endif
					</td>
					<td></td>
					<td>
					@if (isset($data['fieldcontents']['f12']) && trim($data['fieldcontents']['f12']) != '')
						{{ trim($data['fieldcontents']['f12']) }}
					@endif
					</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td></td>
					<td>Date:</td>
				</tr>
			</table>
		@endif
	</div>
</body>
</html>

















