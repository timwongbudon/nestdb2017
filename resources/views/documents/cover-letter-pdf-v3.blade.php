<html>
<head>

<title>Cover Letter</title>

<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 5cm 1.5cm 0.5cm 1.5cm;
}
#footer {
	height: 10px !important;
	margin-bottom: -3px !important;
}
#footer a {
	background-color:#C5C2C0 !important;
}
#header {
	top: -153px;
	padding-bottom:5px;
	z-index:10;
}
.nest-hide-first-header{
	margin-top:-238px;
}


.nest-pdf-cl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}


.nest-pdf-cl-doclist td{
	line-height:1;
	padding:0px;
}
.nest-pdf-cl-doclist{
	padding-top:10px;
	padding-bottom:10px;
	padding-left:20px;
}
td.nest-pdf-cl-doclist-nr{
	padding-right:10px !important;
}

.nest-pdf-cl-content{
	padding-bottom:20px;
	text-align:justify;
}

.nest-pdf-cl-faithfully{
	padding-top:30px;
}

.nest-pdf-cl-signature{
	padding:20px 0px 10px;
}
.nest-pdf-cl-signature img{
	max-height:80px;
	max-width:300px;
}

.nest-pdf-cl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}
</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>    
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com
			</div>
        </div>
        @if (empty($logo) || $logo == 'yes')
            <br><br><br>
        @else
        <br><br><br><br><br><br><br><br><br><br><br><br>
        @endif
		<div class="nest-pdf-cl-top-left">
            @if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />		
			@endif
			To: {{ $data['fieldcontents']['f2'] }}<br />
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br /><br />
			@endif
			<br /><br /><br /><br />
		
			Dear {{ $data['fieldcontents']['f4'] }},
		</div>
		<div class="nest-pdf-cl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-cl-introsentence">
			@if (isset($data['replace']['introsentence']) && trim($data['replace']['introsentence']) != '')
				{!! nl2br($data['replace']['introsentence']) !!}
			@else
				Please find enclosed the following documents for your attention:
			@endif
		</div>
		<div class="nest-pdf-cl-doclist">
			<table>
				@php
					$a = 1;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f6']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td class="nest-pdf-cl-doclist-nr">{{ $a }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
			</table>
		</div>
		<div class="nest-pdf-cl-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else
				Kindly sign / counter-sign and return all the above documents to us. Or in the interest of time, you may inform either myself, or someone in our office, once all documents are ready for collection, and we can send a courier to pick them up from your home.
			@endif
		</div>
		<div class="nest-pdf-cl-finalsentence">
			@if (isset($data['replace']['finalsentence']) && trim($data['replace']['finalsentence']) != '')
				{!! nl2br($data['replace']['finalsentence']) !!}
			@else
				Should you should have any questions regarding the above, feel free to call me on {{ $data['fieldcontents']['f7'] }}.
			@endif
		</div>
		
		<div class="nest-pdf-cl-faithfully">
			Yours faithfully<br />
			As agent for<br />
			NEST PROPERTY LIMITED
		</div>
		<div class="nest-pdf-cl-signature">
			@if ($consultant->sigpath != '')
				<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" />
			@else
				<br /><br /><br />
			@endif
		</div>
		<div class="nest-pdf-cl-consultant">
			{{ $data['fieldcontents']['f8'] }}<br />
			{{ $data['fieldcontents']['f9'] }}<br />
			Residential Search & Investment
		</div>
		<div class="nest-pdf-cl-licensnr">
			Individual Licence No: {{ $data['fieldcontents']['f10'] }}
		</div>
			
	</div>
</body>
</html>

















