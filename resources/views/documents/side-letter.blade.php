@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('documents/sideletter') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Side Letter Fields</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							{{ Form::hidden('id', $data['id']) }}
							{{ Form::hidden('docversion', $data['docversion']) }}
							{{ Form::hidden('shortlistid', $data['shortlistid']) }}
							{{ Form::hidden('propertyid', $data['propertyid']) }}
							{{ Form::hidden('consultantid', $data['consultantid']) }}
							{{ Form::hidden('clientid', $data['clientid']) }}
							
							@foreach ($fields as $field)
								@if (count($field) >= 3)
									@if ($field[1] == 'date')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<input type="date" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'hidden')
										{{ Form::hidden('f'.$field[0], old('f'.$field[0], $data['fieldcontents']['f'.$field[0]])) }}
									@elseif ($field[1] == 'text')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												@if (isset($field[3]))
													<input type="text" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}" placeholder="{{ $field[3] }}">
												@else
													<input type="text" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
												@endif
											</div>
											<div class="clearfix"></div>
										</div>	
									@elseif ($field[1] == 'number')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<input type="number" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>		
									@elseif ($field[1] == 'radio')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											@php
												$rbs = explode('#', $field[3]);
											@endphp
											<div class="col-xs-8">
												@foreach ($rbs as $index => $rb)
													@if (isset($data['fieldcontents']['f'.$field[0]]))
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, $data['fieldcontents']['f'.$field[0]] == $rb, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@else
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, false, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@endif
												@endforeach
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'textarea')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">{{ $field[2] }}</div>
											</div>
											<div class="col-xs-8">
												<textarea name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" rows="4">{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}</textarea>
												<p style="padding-top:5px;">
													<u>Example for this section:</u><br />
													We the Landlord agree that the premises shall be handed over to the Tenant on or before 1st June 2018. Rental, Management fees and Government rates for the period from 1st June 2018 to 30th June 2018 and 1st May 2020 to 31st May shall be waived. However, the Tenant should take up all tenant’s responsibilities from the date of handover, and agrees to pay all utility charges during the rent-free periods and to take up all responsibility for maintaining the property, swimming pool and grounds from the date of handover. 
												</p>
											</div>
											<div class="clearfix"></div>
										</div>		
									@endif
								@endif
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Side Letter Content</h4>
						</div>

						<div class="panel-body">
							<b>Tick the boxes for the parts you don't want to show up in the contract:</b><br />
							<div class="nest-form-builder-hide-wrapper">
								@foreach ($hides as $field)
									@if (count($field) >= 4)
										@if ($field[1] == 0)
											@if ($field[2] == 1)
												<div class="nest-form-builder-hide-section">
													{{ $field[3] }}
												</div>
												<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
													{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} Hide this section
												</label>
											@else
												<div class="nest-form-builder-hide-section">
													{{ $field[3] }} (tick what to hide)
												</div>
											@endif
										@else
											<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
												{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} {{ $field[3] }}
											</label>
										@endif
									@endif
								@endforeach
								@foreach ($hiddenhides as $hh)
									{{ Form::hidden('hidevals[]', $hh) }}
								@endforeach
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							@foreach ($replaceFields as $key => $name)
								<div class="nest-property-edit-row">
									<div class="nest-property-edit-label">Replace Text for Section "{{ $name }}"
									@if (!isset($data['replace'][$key]) || trim($data['replace'][$key]) == '')
										<a href="javascript:;" onClick="showReplace('{{ $key }}');">show</a>
										<a href="javascript:;" id="replacedesclink{{ $key }}" onClick="replaceOriginalText('{{ $key }}')" style="display:none;">replace with original text</a>
									@else
										<a href="javascript:;" onClick="replaceOriginalText('{{ $key }}')">replace with original text</a>
									@endif
									</div>
									@if (isset($replaceFieldsDesc[$key]))
										<div id="replacedesc{{ $key }}" style="display:none;">
											{{ $replaceFieldsDesc[$key] }}
										</div>
									@endif
									@if (isset($data['replace'][$key]) && trim($data['replace'][$key]) != '')
										<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
									@else
										<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2" style="display:none;">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
									@endif
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-xs-12">
								@if ($data['id'] > 0)
									<a id="pdfbutton" href="{{ url('/documents/'.$data['shortlistid'].'/sideletterpdf/'.$data['propertyid'].'/no') }}" target="_blank" class="pdfbuttonhide nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download Letterhead PDF </a>
									<a id="pdfbutton" href="{{ url('/documents/'.$data['shortlistid'].'/sideletterpdf/'.$data['propertyid']) }}" target="_blank" class="pdfbuttonhide nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download PDF </a>
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								@else
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Save </button>
								@endif
								@if ($shortlist->type_id == 2)
									<a href="{{ url('/documents/index-sale/'.$shortlist->id) }}" class="nest-button nest-right-button btn btn-default">
										<i class="fa fa-btn fa-arrow-left"></i>Back to Documents
									</a>
								@else
									<a href="{{ url('/documents/index-lease/'.$shortlist->id) }}" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-arrow-left"></i>Back to Documents
									</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				
			</form>
        </div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('.pdfbuttonhide').css('display', 'none');
	});
	jQuery('textarea').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
		jQuery('.pdfbuttonhide').css('display', 'none');
	});
	function showReplace(id){
		jQuery('#replace-'+id).css('display', 'block');
		jQuery('#showlink'+id).css('display', 'none');
		jQuery('#replacedesclink'+id).css('display', 'inline');
	}
	function replaceOriginalText(id){
		var content = jQuery('#replacedesc'+id).html();
		jQuery('#replace-'+id).text(content.trim());
	}
</script>
@endsection
