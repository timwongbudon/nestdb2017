<html>
<head>

<title>Landlord Fee Letter</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
	line-height:1;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-cl-top-left{
	padding-top:180px;
	padding-bottom:0px;
}
.nest-pdf-cl-bold{
	font-family:MyriadBold;
}
.nest-pdf-cl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-cl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-cl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-cl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-cl-content{
	padding-bottom:20px;
	text-align:justify;
}
.nest-pdf-cl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-100px;
	text-align:right;
	padding-top:60px;
}
.nest-hide-first-header img{
	width:140px;
	padding-right:20px;
}
.nest-pdf-cl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-cl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-cl-faithfully{
	padding-top:30px;
}
.nest-pdf-cl-signature{
	padding:20px 0px 10px;
}
.nest-pdf-cl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-cl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

.nest-pdf-cl-doclist td{
	line-height:1;
	padding:0px;
}
.nest-pdf-cl-doclist{
	padding-top:10px;
	padding-bottom:10px;
	padding-left:20px;
}
td.nest-pdf-cl-doclist-nr{
	padding-right:10px !important;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
				<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		@else
			<div class="nest-pdf-letterhead" style="width:152px;">
				<br /><br /><br /><br /><br /><br /><br /><br />
		@endif
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-cl-top-left">
			To: {{ $data['fieldcontents']['f2'] }}<br />
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br /><br />
			@endif
			<br /><br /><br /><br /><br /><br />
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			Dear {{ $data['fieldcontents']['f4'] }},
		</div>
		<div class="nest-pdf-cl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-cl-introsentence">
			@if (isset($data['replace']['introsentence']) && trim($data['replace']['introsentence']) != '')
				{!! nl2br($data['replace']['introsentence']) !!}
			@else
				Please find enclosed the following documents for your attention:
			@endif
		</div>
		<div class="nest-pdf-cl-doclist">
			<table>
				@php
					$a = 1;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f6']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td class="nest-pdf-cl-doclist-nr">{{ $a }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
			</table>
		</div>
		<div class="nest-pdf-cl-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else
				Kindly sign / counter-sign and return all the above documents to us. Or in the interest of time, you may inform either myself, or someone in our office, once all documents are ready for collection, and we can send a courier to pick them up from your home.
			@endif
		</div>
		<div class="nest-pdf-cl-finalsentence">
			@if (isset($data['replace']['finalsentence']) && trim($data['replace']['finalsentence']) != '')
				{!! nl2br($data['replace']['finalsentence']) !!}
			@else
				Should you should have any questions regarding the above, feel free to call me on {{ $data['fieldcontents']['f7'] }}.
			@endif
		</div>
		
		<div class="nest-pdf-cl-faithfully">
			Yours faithfully<br />
			As agent for<br />
			NEST PROPERTY LIMITED
		</div>
		<div class="nest-pdf-cl-signature">
			@if ($consultant->sigpath != '')
				<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" />
			@else
				<br /><br /><br />
			@endif
		</div>
		<div class="nest-pdf-cl-consultant">
			{{ $data['fieldcontents']['f8'] }}<br />
			{{ $data['fieldcontents']['f9'] }}<br />
			Residential Search & Investment
		</div>
		<div class="nest-pdf-cl-licensnr">
			Individual Licence No: {{ $data['fieldcontents']['f10'] }}
		</div>
			
	</div>
</body>
</html>

















