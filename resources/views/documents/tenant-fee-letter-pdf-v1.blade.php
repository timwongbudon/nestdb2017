<html>
<head>

<title>Tenant Fee Letter</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-lfl-top-left{
	padding-top:120px;
	padding-bottom:0px;
}
.nest-pdf-lfl-bold{
	font-family:MyriadBold;
}
.nest-pdf-lfl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-lfl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-lfl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-lfl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-lfl-content{
	padding-bottom:20px;
	text-align:justify;
	line-height:1 !important;
}
.nest-pdf-lfl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-100px;
	text-align:right;
	padding-top:60px;
}
.nest-hide-first-header img{
	width:140px;
	padding-right:20px;
}
.nest-pdf-lfl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-lfl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-lfl-faithfully{
	
}
.nest-pdf-lfl-signature{
	padding:20px 0px 10px;
}
.nest-pdf-lfl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-lfl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
	line-height:10px !important;
}
.nest-pdf-lfl-consultant{
	line-height:1 !important;
}
.nest-pdf-lfl-signature-names{
	line-height:1 !important;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
				<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		@else
			<div class="nest-pdf-letterhead" style="width:152px;">
				<br /><br /><br /><br /><br /><br /><br /><br />
		@endif
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-lfl-top-left">
			<div style="padding-bottom:10px;">To: {{ $data['fieldcontents']['f2'] }}</div>
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br />
			@endif
			<div class="nest-pdf-lfl-bold">{{ $data['fieldcontents']['f4'] }}</div>
			@php
				$e = explode("\n", trim($data['fieldcontents']['f5']));
				echo nl2br(trim($data['fieldcontents']['f5']));
				for ($i = count($e); $i <= 4; $i++){
					echo '<br />';
				}
			@endphp
			<br /><br />
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx<br /><br />
			@endif
			Dear {{ $data['fieldcontents']['f6'] }},
		</div>
		<div class="nest-pdf-lfl-top-premises">
			Re: {{ $data['fieldcontents']['f7'] }}
		</div>
		<div class="nest-pdf-lfl-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else				
				In respect of the above premises, we would like to reconfirm that the Tenant shall pay an agency fee to Nest Property Limited equal to 
				{{ $data['fieldcontents']['f8'] }}% of one month’s rent{{ $data['fieldcontents']['f9'] }} upon the successful signing of the Tenancy Agreement. 
				Cheques should be made payable to “NEST PROPERTY LIMITED”. 
				@if (trim($data['fieldcontents']['f10']) != '')
					For the avoidance of doubt the agency fee for the above-mentioned property, should the fee stated in the Offer Letter be accepted, 
					totals HK${{ number_format($data['fieldcontents']['f10'], 0, '', ',') }}.
				@else
					For the avoidance of doubt the agency fee for the above-mentioned property, should the fee stated in the Offer Letter be accepted, 
					totals HK$xxxxxx.
				@endif
				
				
				
			@endif
		</div>
		
		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
			<div class="nest-pdf-lfl-bottom-1">
				Please acknowledge receipt by countersigning this letter.<br /><br />
				Yours sincerely, 	
			</div>
			<div class="nest-pdf-lfl-signature">
				@if ($consultant->sigpath != '')
					<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" />
				@else
					<br /><br /><br />
				@endif
			</div>
			<div class="nest-pdf-lfl-consultant">
				{{ $data['fieldcontents']['f11'] }}<br />
				{{ $data['fieldcontents']['f12'] }}<br />
				Residential Search & Investment
			</div>
			<div class="nest-pdf-lfl-licensnr">
				Individual Licence No: {{ $data['fieldcontents']['f13'] }}
			</div>
			
			<div class="nest-pdf-lfl-signature-names">
				We hereby confirm and agree<br />
				to the above terms and conditions
			</div>
			<br /><br /><br />
			
			__________________________________<br />
			<div class="nest-pdf-lfl-signature-names">
				Name: {{ $data['fieldcontents']['f4'] }}<br />
				Date:
			</div>
			
			
		</td></tr></table>
	</div>
</body>
</html>

















