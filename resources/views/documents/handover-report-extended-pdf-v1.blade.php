<html>
<head>

<title>Handover Report</title>

<style>
body{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size:12px;
	line-height:1.4;
}
@page {
	margin: 3.6cm 2cm 1.5cm;
}


.nest-pdf-letterhead{
	text-align:right;
	padding-bottom:10px;
}
.nest-pdf-letterhead img{
	width:100px;
}
.nest-pdf-hr-top{
	padding:0px 0px 0px;
	text-align:center;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	font-size:20px;
	color:#000000;
}
.nest-pdf-hr-handover-accepted{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	color:#000000;
	padding-bottom:15px;
	padding-left:3px;
	
}
.nest-pdf-hr-basics{
	padding-top:5px;
}
.nest-pdf-first-big .nest-pdf-hr-basics{
	padding-top:15px;
}
.nest-pdf-hr-basics table{
	width:100%;
	border:1px solid #cdc6b7;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-hr-basics-left{
	color:#000000;
	background:#cdc6b7;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	width:25%;
	padding:0px 10px 1px;
	text-align:right;
}
.nest-pdf-first-big .nest-pdf-hr-basics-left{
	padding:2px 10px 3px;
}
.nest-pdf-hr-basics-right{
	border:1px solid #cdc6b7;
	width:75%;
	padding:0px 10px 1px;
}
.nest-pdf-first-big .nest-pdf-hr-basics-right{
	padding:2px 10px 3px;
}
.nest-pdf-hr-handover{
	padding-top:18px;
}
.nest-pdf-hr-handover-utility{
	padding-top:40px;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first {
	padding-top:20px;
}
.nest-pdf-hr-handover table{
	width:100%;
	border:1px solid #cdc6b7;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-hr-handover table.outer-table{
	border:0px solid #cdc6b7;
	
}
.nest-pdf-hr-handover-top{
	color:#000000;
	background:#cdc6b7;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	text-align:left;
	padding:2px 10px 1px;
	font-weight:normal;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first .nest-pdf-hr-handover-top{
	padding:4px 10px 3px;
}
.nest-pdf-hr-handover-left{
	width:20%;
	padding:3px 10px 0px;
	border-right:1px solid #cdc6b7;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	text-align:right;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first .nest-pdf-hr-handover-left{
	padding:6px 10px 1px;
}
.nest-pdf-hr-handover-right{
	border:1px solid #cdc6b7;
	width:60%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first .nest-pdf-hr-handover-right{
	padding:5px 10px 2px;
}
.nest-pdf-hr-handover-right-third{
	border:1px solid #cdc6b7;
	width:20%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-hr-row{
	border:1px solid #cdc6b7;
	width:100%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-hr-handover-utility-left{
	width:25%;
	padding:3px 10px 0px;
	border-right:1px solid #cdc6b7;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	text-align:right;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-hr-handover-utility-right{
	border:1px solid #cdc6b7;
	width:25%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#000000;
	vertical-align:top;
	text-align:center;
}


#header,
#footer{
	position: fixed;
	left: 0;
	right: 0;
	color: #000000;
	font-size: 75%;
}
#footer{
	opacity:0.5;
}

#header {
	top: -95px;
	padding-bottom:5px;
	z-index:10;
	text-align:right;
}

#footer {
	bottom: 0;
	border-top: 0.1pt solid #000000;
	padding-top:5px;
}
#header table,
#footer table {
	width: 100%;
	border-collapse: collapse;
	border: none;
}

#footer td {
	padding: 0;
	width: 50%;
}

.page-number {
  text-align: right;
  font-size:75%;
}

.page-number:before {
  content: counter(page);
}

.nest-pdf-hr-check-square{
	float:none;
	display:inline-block;
	height:8px;
	width:8px;
	border:1px solid #cdc6b7;
	margin-top:2px;
	margin-right:8px;
}
.nest-pdf-hr-extra-box{
	color:#000000;
}
.nest-pdf-hr-handover-signature-bottom{
	padding-top:20px;
}
.nest-pdf-hr-handover-signature-bottom .nest-pdf-hr-extra-box{
	color:#000000;
}
.nest-pdf-hr-handover-signature-bottom .nest-pdf-hr-check-square{
	border:1px solid #000000;
}
.nest-pdf-hr-extra-box .nest-pdf-hr-check-square{
	margin-top:4px;
}
.nest-pdf-dark-text{
	color:#000000;
}

</style>


</head>
<body>
	<div id="header" style="padding-top:0px;">
		<img src="{{ url('/images/tools/nest-letterhead-logo-header.png') }}" style="padding-top:10px;width:80px;" />
	</div>
	<div id="footer" style="border-top:0px !important;">
		<table><tr>
			<td><div class="page-number" style="text-align:center !important;"></div></td>
		</tr></table>
	</div>
	@if (isset($data['fieldcontents']['f32']) && $data['fieldcontents']['f32'] == 'small')
		<div class="nest-pdf-container">
	@else
		<div class="nest-pdf-container nest-pdf-first-big">
	@endif
		<div class="nest-pdf-hr-top">
			Handover Report
		</div>
		<div class="nest-pdf-hr-basics">
			<table>
				<tr>
					<td class="nest-pdf-hr-basics-left">Address</td>
					<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f1'] }}</td>
				</tr>
				@if (trim($data['fieldcontents']['f2']) != '')
					<tr>
						<td class="nest-pdf-hr-basics-left">Car Park No.</td>
						<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f2'] }}</td>
					</tr>
				@endif
				@if (trim($data['fieldcontents']['f2']) != '' && trim($data['fieldcontents']['f3']) != '')
					<tr>
						<td class="nest-pdf-hr-basics-left">Vehicle Reg. No.</td>
						<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f3'] }}</td>
					</tr>
				@endif
				<tr>
					<td class="nest-pdf-hr-basics-left">Taken By:</td>
					<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f4'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-basics-left">Handover Date:</td>
					<td class="nest-pdf-hr-basics-right">
						@if (trim($data['fieldcontents']['f5']) != '')
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f5']) }}
						@endif
					</td>
				</tr>
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Lease Term</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Commencement Date</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f6']) != '')
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f6']) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Expiry Date</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f7']) != '')
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f7']) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Monthly Rental</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f8']) == '')
							NA
						@else
							@if (is_numeric($data['fieldcontents']['f8']))
								HK${{ number_format($data['fieldcontents']['f8'], 0, '', ',') }}.00
							@else
								NA
							@endif
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Management Fee</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f9']) == '')
							Inclusive
						@else
							@if (is_numeric($data['fieldcontents']['f9']))
								HK${{ number_format($data['fieldcontents']['f9'], 0, '', ',') }}.00
							@else
								Inclusive
							@endif
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Government Rates</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f10']) == '')
							Inclusive
						@else
							@if (is_numeric($data['fieldcontents']['f10']))
								HK${{ number_format($data['fieldcontents']['f10'], 0, '', ',') }}.00
							@else
								Inclusive
							@endif
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Government Rent</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f11']) == '')
							Inclusive
						@else
							@if (is_numeric($data['fieldcontents']['f11']))
								HK${{ number_format($data['fieldcontents']['f11'], 0, '', ',') }}.00
							@else
								Inclusive
							@endif
						@endif
					</td>
				</tr>
				@if (isset($data['fieldcontents']['f29']) && trim($data['fieldcontents']['f29']) == 'show')
					<tr>
						<td class="nest-pdf-hr-handover-left">Air-Conditioner Maintenance Charges</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
							@if (trim($data['fieldcontents']['f12']) == '')
								Inclusive
							@else
								@if (is_numeric($data['fieldcontents']['f12']))
									HK${{ number_format($data['fieldcontents']['f12'], 0, '', ',') }}.00
								@else
									Inclusive
								@endif
							@endif
						</td>
					</tr>
				@endif
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Rental Payment</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Name of Bank</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f13'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Bank A/C No.</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f14'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">A/C Name</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f15'] }}</td>
				</tr>
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Tenant</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">
						@if (isset($data['fieldcontents']['f30']) && $data['fieldcontents']['f30'] == 'company')
							Company Name
						@else
							Name
						@endif
					</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f16'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Address</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f17'] }}</td>
				</tr>
				@if (trim($data['fieldcontents']['f18']) != '')
					<tr>
						<td class="nest-pdf-hr-handover-left">Occupant</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f18'] }}</td>
					</tr>
				@endif
				@if (trim($data['fieldcontents']['f21']) != '')
					<tr>
						<td class="nest-pdf-hr-handover-left">Contact Person</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f21'] }}</td>
					</tr>
				@endif
				<tr>
					<td class="nest-pdf-hr-handover-left">Tel</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f19'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Email</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f20'] }}</td>
				</tr>
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Landlord</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">
						@if (isset($data['fieldcontents']['f31']) && $data['fieldcontents']['f31'] == 'company')
							Company Name
						@else
							Name
						@endif
					</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f23'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Address</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f24'] }}</td>
				</tr>
				@if (trim($data['fieldcontents']['f27']) != '')
					<tr>
						<td class="nest-pdf-hr-handover-left">Contact Person</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f27'] }}</td>
					</tr>
				@endif
				<tr>
					<td class="nest-pdf-hr-handover-left">Tel</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f25'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Email</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f28'] }}</td>
				</tr>
			</table>
		</div>
		<div style="page-break-after:always;"></div>
		
		@php
			$tickbox = '<div class="nest-pdf-hr-check-square"></div>';
		@endphp
		
		
		
		@if (count($rooms) > 0)
			@foreach ($rooms as $index => $room)
				@if ($room['roomtype'] == 'living')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Air-Conditioner</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Remote Control:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Window:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Split:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Central:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Balcony</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'hallway')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Air-Conditioner</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Remote Control:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Window:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Split:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Central:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets / Wardrobe</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'kitchen')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cupboards</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Gas</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Electric Cooker</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cooker Hood</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Microwave Oven</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Oven</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Refrigerator</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Dishwasher</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Washing Machine</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Tumble Dryer</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Exhaust Fan</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Boiler</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Water Heater</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Brand:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Electricity:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Towngas:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Security Pane / Maid Call Panel</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'bedroom')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Air-Conditioner</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Remote Control:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Window:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Split:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Central:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets / Wardrobe</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
					
				@elseif ($room['roomtype'] == 'bathroom')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Fittings</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Exhaust Fan</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Shower Cubicle</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Bathtub</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Showerhead/Overhead Shower</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Tap</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Basin + Plug</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Toilet</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Flushing</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Water Heater</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Brand:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Electricity:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Towngas:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'masterbedroom')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Air-Conditioner</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Remote Control:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Window:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Split:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Central:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets / Wardrobe</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'ensuitebathroom')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Fittings</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Exhaust Fan</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Shower Cubicle</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Bathtub</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Showerhead/Overhead Shower</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Tap</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Basin + Plug</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Toilet</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Flushing</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Water Heater</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Brand:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Electricity:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Towngas:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'study')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Air-Conditioner</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Remote Control:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"></td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Window:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Split:</td>
								<td class="nest-pdf-hr-handover-right-third">{!! $tickbox !!}Central:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Cabinets</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@elseif ($room['roomtype'] == 'utility')
					@if ($index == 0)
						<div class="nest-pdf-hr-handover" style="padding-top:0px;">
					@else
						<div class="nest-pdf-hr-handover">
					@endif
						<table class="outer-table"><tr><td>
						<table>
							<tr>
								<th colspan="4" class="nest-pdf-hr-handover-top">{{ $room['roomname'] }}</th>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Lights</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Shelves / Cupboards</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Washing Machine</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Clothes Dryer</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}Brand:</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left">Others</td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
							<tr>
								<td class="nest-pdf-hr-handover-left"> &nbsp; </td>
								<td class="nest-pdf-hr-handover-right" colspan="3">{!! $tickbox !!}</td>
							</tr>
						</table>
						</td></tr></table>
					</div>
				@endif
			@endforeach
		@endif
		
		
		
		
		@if (!in_array('16_0', $data['sectionshidden']))
			<div class="nest-pdf-hr-handover">
				<table class="outer-table"><tr><td>
				<table>
					<tr>
						<th class="nest-pdf-hr-handover-top">Defects / Remarks:</th>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-row">&nbsp;</td>
					</tr>
				</table>
				</td></tr></table>
			</div>
		@endif
		<br />
		<div class="nest-pdf-hr-extra-box" style="padding-left:3px;">
			{!! $tickbox !!}Digital photos taken
			<div class="clearfix"></div>
		</div>
		<div class="nest-pdf-hr-extra-box" style="padding-left:3px;">
			{!! $tickbox !!}Thorough cleaning of the flat throughout
			<div class="clearfix"></div>
		</div>
		<div style="page-break-after:always;"></div>
		<div class="nest-pdf-hr-handover" style="padding-top:0px;">
			<table>
				<tr>
					<th class="nest-pdf-hr-handover-top">Key List</th>
				</tr>
			</table>
		</div>
		<div style="padding:10px 3px;text-align:justify;">
			The Tenant acknowledges receipt of the following keys for above mentioned premises upon handover of tenancy: -
		</div>
		<div class="nest-pdf-hr-handover-keylist">
			<table width="100%">
				<tr>
					<td width="18%">Front door</td>
					<td width="17%">- Upper lock</td>
					<td width="15%">_____ units</td>
					<td width="35%">Front Gate</td>
					<td width="15%">_____ units</td>
				</tr>
				<tr>
					<td></td>
					<td>- Lower lock</td>
					<td>_____ units</td>
					<td>Garden Gate</td>
					<td>_____ units</td>
				</tr>
				<tr>
					<td>Back door</td>
					<td>- Upper lock</td>
					<td>_____ units</td>
					<td>Roof Door</td>
					<td>_____ units</td>
				</tr>
				<tr>
					<td></td>
					<td>- Lower lock</td>
					<td>_____ units</td>
					<td>Security</td>
					<td>_____ units</td>
				</tr>
				<tr>
					<td colspan="2">Mail Box</td>
					<td>_____ units</td>
					<td>Remote Control (Air-Con)</td>
					<td>_____ units</td>
				</tr>
				<tr>
					<td colspan="2">Residence Card</td>
					<td>_____ units</td>
					<td>User Manual</td>
					<td>_____ units</td>
				</tr>
				
				
				@php
					$unitsstart = true;
				@endphp
				
				@if (count($rooms) > 0)
					@foreach ($rooms as $index => $room)
						@if ($unitsstart)
							<tr>
								<td colspan="2">{{ $room['roomname'] }}</td>
								<td>_____ units</td>
							@php
								$unitsstart = false;
							@endphp
						@else
								<td>{{ $room['roomname'] }}</td>
								<td>_____ units</td>
							</tr>
							@php
								$unitsstart = true;
							@endphp
						@endif
					@endforeach
				@endif
				
				@if (!$unitsstart)
						<td>Door Code</td>
						<td>_________</td>
					</tr>
					<tr>
						<td colspan="2">Others:</td>
						<td>_____ units</td>
						<td>Others:</td>
						<td>_____ units</td>
					</tr>
					<tr>
						<td colspan="2">Others:</td>
						<td>_____ units</td>
						<td></td>
						<td></td>
					</tr>
				@else
					<tr>
						<td colspan="2">Others:</td>
						<td>_____ units</td>
						<td>Door Code</td>
						<td>_________</td>
					</tr>
					<tr>
						<td colspan="2">Others:</td>
						<td>_____ units</td>
						<td></td>
						<td></td>
					</tr>
					
				@endif
			</table>
		</div>
		@if (!in_array('21_0', $data['sectionshidden']))
			<br />
			<div class="nest-pdf-hr-handover-utility nest-pdf-hr-handover">
				<table>
					<tr>
						<th colspan="4" class="nest-pdf-hr-handover-top">Utilities</th>
					</tr>
					<tr>
						<td class="nest-pdf-hr-handover-utility-left"></td>
						<td class="nest-pdf-hr-handover-utility-right">Meter No.</td>
						<td class="nest-pdf-hr-handover-utility-right">Meter Reading</td>
						<td class="nest-pdf-hr-handover-utility-right">Date Read</td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-handover-utility-left">Gas</td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-handover-utility-left">Water</td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
					</tr>
					<tr>
						<td class="nest-pdf-hr-handover-utility-left">Electricity</td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
						<td class="nest-pdf-hr-handover-utility-right"></td>
					</tr>
				</table>
			</div> 
			<!-- <div style="page-break-after:always;"></div> -->
			<br /><br /><br />
			<div class="nest-pdf-hr-handover-accepted">
				Accepted for and on behalf of
			</div>
			<div class="nest-pdf-hr-handover-signature">
				<table width="100%">
					<tr>
						<td width="20%">Signature:</td>
						<td width="25%" style="border-bottom:1px solid #000000;"></td>
						<td width="10%"></td>
						<td width="20%">Signature:</td>
						<td width="25%" style="border-bottom:1px solid #000000;"></td>
					</tr>
					<tr>
						<td>Name:</td>
						<td style="border-bottom:1px solid #000000;"></td>
						<td></td>
						<td>Name:</td>
						<td style="border-bottom:1px solid #000000;"></td>
					</tr>
					<tr>
						<td>Date:</td>
						<td style="border-bottom:1px solid #000000;"></td>
						<td></td>
						<td>Date:</td>
						<td style="border-bottom:1px solid #000000;"></td>
					</tr>
					<tr>
						<td>Contact No.:</td>
						<td style="border-bottom:1px solid #000000;"></td>
						<td></td>
						<td>Contact No.:</td>
						<td style="border-bottom:1px solid #000000;"></td>
					</tr>
				</table>
			</div>
			<div class="nest-pdf-hr-handover-signature-bottom">
				<table width="100%">
					<tr>
						<td width="0%">&nbsp;</td>
						<td width="45%">
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Landlord
								<div class="clearfix"></div>
							</div>
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Landlord’s Agent
								<div class="clearfix"></div>
							</div>
						</td>
						<td width="10%">&nbsp;</td>
						<td width="45%">
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Tenant
								<div class="clearfix"></div>
							</div>
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Tenant’s representative
								<div class="clearfix"></div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		@else
			<br /><br /><br />
			<div class="nest-pdf-hr-handover-accepted">
				Accepted for and on behalf of
			</div>
			<div class="nest-pdf-hr-handover-signature">
				<table width="100%">
					<tr>
						<td width="20%">Signature:</td>
						<td width="25%" style="border-bottom:1px solid #000000;"></td>
						<td width="10%"></td>
						<td width="20%">Signature:</td>
						<td width="25%" style="border-bottom:1px solid #000000;"></td>
					</tr>
					<tr>
						<td>Name:</td>
						<td style="border-bottom:1px solid #000000;"></td>
						<td></td>
						<td>Name:</td>
						<td style="border-bottom:1px solid #000000;"></td>
					</tr>
					<tr>
						<td>Date:</td>
						<td style="border-bottom:1px solid #000000;"></td>
						<td></td>
						<td>Date:</td>
						<td style="border-bottom:1px solid #000000;"></td>
					</tr>
					<tr>
						<td>Contact No.:</td>
						<td style="border-bottom:1px solid #000000;"></td>
						<td></td>
						<td>Contact No.:</td>
						<td style="border-bottom:1px solid #000000;"></td>
					</tr>
				</table>
			</div>
			<div class="nest-pdf-hr-handover-signature-bottom">
				<table width="100%">
					<tr>
						<td width="0%">&nbsp;</td>
						<td width="45%">
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Landlord
								<div class="clearfix"></div>
							</div>
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Landlord’s Agent
								<div class="clearfix"></div>
							</div>
						</td>
						<td width="10%">&nbsp;</td>
						<td width="45%">
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Tenant
								<div class="clearfix"></div>
							</div>
							<div class="nest-pdf-hr-extra-box">
								{!! $tickbox !!}Tenant’s Representative
								<div class="clearfix"></div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		@endif
		
	</div>
</body>
</html>

















