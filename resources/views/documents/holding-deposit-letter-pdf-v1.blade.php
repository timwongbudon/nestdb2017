<html>
<head>

<title>Holding Deposit Letter</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:Myriad;
	font-size:14px;
}
@page {
	margin: 1.2cm 1.7cm 1cm;
}


.nest-pdf-letterhead{
	float:right;
	width:160px;
	text-align:left;
	line-height:1.1;
	z-index:999;
	background:#ffffff;
}
.nest-pdf-letterhead img{
	width:160px;
}
.nest-pdf-letterhead-text{
	font-size:11px;
	padding:5px 3px;
}




.nest-pdf-hdl-top-left{
	padding-top:140px;
	padding-bottom:0px;
}
.nest-pdf-hdl-bold{
	font-family:MyriadBold;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-hdl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-hdl-top-premises{
	padding-bottom:30px;
	padding-top:30px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-hdl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-hdl-top-premises-header{
	padding-top:2px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-hdl-content{
	padding-bottom:30px;
	text-align:justify;
}
.nest-pdf-hdl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-hide-first-header{
	background:#ffffff;
	z-index:99;
	margin-top:-100px;
	text-align:right;
	padding-top:60px;
}
.nest-hide-first-header img{
	width:140px;
	padding-right:20px;
}
.nest-pdf-hdl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-hdl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-hdl-faithfully{
	
}
.nest-pdf-hdl-signature{
	padding:20px 0px 10px;
}
.nest-pdf-hdl-signature img{
	max-height:80px;
	max-width:300px;
}
.nest-pdf-hdl-licensnr{
	font-size:12px;
	padding-top:5px;
	padding-bottom:30px;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		@if (empty($logo) || $logo == 'yes')
			<div class="nest-pdf-letterhead">
				<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		@else
			<div class="nest-pdf-letterhead" style="width:152px;">
				<br /><br /><br /><br /><br /><br /><br /><br />
		@endif
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-hdl-top-left">
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				xxxxxx
				<br /><br />
			@endif
			{{ $data['fieldcontents']['f2'] }}<br />
			@php
				$e = explode("\n", trim($data['fieldcontents']['f3']));
				echo nl2br(trim($data['fieldcontents']['f3']));
				for ($i = count($e); $i <= 5; $i++){
					echo '<br />';
				}
			@endphp
			<br /><br />
			Dear {{ $data['fieldcontents']['f4'] }},
		</div>
		<div class="nest-pdf-hdl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-hdl-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else
				@if (is_numeric($data['fieldcontents']['f6']))
					We herewith enclose a cheque for the sum of HK${{ number_format($data['fieldcontents']['f6'], 0, '', ',') }} from the Tenant (cheque no:______________________) being the initial deposit for the above-mentioned premises.
				@else
					We herewith enclose a cheque for the sum of HK$xxxxxx from the Tenant (cheque no:______________________) being the initial deposit for the above-mentioned premises.
				@endif
				<br /><br />
				As stipulated in our offer letter, this holding deposit shall be refunded to the Tenant in full should either party fail to sign the Tenancy Agreement.
			@endif
		</div>
		
		<table><tr><td>
			<div class="nest-pdf-hdl-bottom-1">
				Please acknowledge receipt by countersigning a copy of the attached letter.<br /><br />
				Yours sincerely, 	
			</div>
			<div class="nest-pdf-hdl-signature">
				@if ($consultant->sigpath != '')
					<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" />
				@else
					<br /><br /><br />
				@endif
			</div>
			<div class="nest-pdf-hdl-consultant">
				{{ $data['fieldcontents']['f7'] }}<br />
				{{ $data['fieldcontents']['f8'] }}<br />
				Residential Search & Investment
			</div>
			<div class="nest-pdf-hdl-licensnr">
				Individual Licence No: {{ $data['fieldcontents']['f9'] }}
			</div>
			
		</td></tr></table>
	</div>
</body>
</html>

















