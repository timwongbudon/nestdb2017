<html>
<head>

<title>Letter of Undertaking</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size:10px;
}
@page {
	margin: 1cm 1.3cm 1cm;
}


.nest-pdf-letterhead{
	text-align:right;
	padding-bottom:20px;
}
.nest-pdf-letterhead img{
	width:120px;
}
.nest-pdf-ua-top{
	padding:8px 0px 8px;
	text-align:center;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	font-size:16px;
	color:#000000;
	background:#cdc6b7;
}
.nest-pdf-ua-basics{
	padding-top:20px;
}
.nest-pdf-ua-basics table{
	width:100%;
	border:0.6px solid #cdc6b7;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-ua-basics-left{
	color:#000000;
	background:#cdc6b7;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	width:25%;
	padding:1px 8px 2px;
}
.nest-pdf-ua-basics-right{
	border:0.6px solid #cdc6b7;
	width:75%;
	padding:1px 8px 1px;
}
.nest-pdf-ua-utility{
	padding-top:20px;
}
.nest-pdf-ua-utility table{
	width:100%;
	border:0.6px solid #cdc6b7;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-ua-utility-top{
	color:#000000;
	background:#cdc6b7;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	text-align:left;
	padding:3px 8px 3px;
	font-weight:normal;
}
.nest-pdf-ua-utility-left{
	width:20%;
	padding:1px 8px 1px;
	border-right:0.6px solid #cdc6b7;
	font-weight:400;
	line-height:1;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
}
.nest-pdf-ua-utility-middle{
	width:20%;
	padding:2px 8px 1px;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-ua-utility-right{
	border:0.6px solid #cdc6b7;
	width:60%;
	padding:1px 8px 1px;
}

</style>


</head>
<body>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<img src="{{ url('/images/tools/nest-letterhead-logo.png') }}" />
		</div>
		<div class="nest-pdf-ua-top">
			Utilities Accounts Summary
		</div>
		<div class="nest-pdf-ua-basics">
			<table>
				<tr>
					<td class="nest-pdf-ua-basics-left">{{ $data['fieldcontents']['f21'] }}</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f1'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">{{ $data['fieldcontents']['f22'] }}</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f2'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">Property Address</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f3'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">Lease Term</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f4'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">Contact Tel</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f5'] }}</td>
				</tr>
			</table>
		</div>
		@if (!in_array('1_0', $data['sectionshidden']))
			<div class="nest-pdf-ua-utility">
				<table>
					<tr>
						<th colspan="3" class="nest-pdf-ua-utility-top">Water</th>
					</tr>
					<tr>
						<th class="nest-pdf-ua-utility-left" rowspan="5">
							<img src="{{ url('/images/tools/water.png') }}" style="padding-bottom:10px;width:54px;" /><br />
							Tel: 2824 5000<br />
							Fax: 2802 7333
						</td>
						<td class="nest-pdf-ua-utility-middle">Account Name</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f6'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Billing Address</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f7'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Account No.</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f8'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Effective Date</td>
						<td class="nest-pdf-ua-utility-right">
							@if (strstr($data['fieldcontents']['f9'], '-'))
								{{ \NestDate::nest_date_format($data['fieldcontents']['f9'], "Y-m-d") }}
							@else
								{{ $data['fieldcontents']['f9'] }}
							@endif
						</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Deposit</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f10'] }}</td>
					</tr>
				</table>
			</div>
		@endif
		@if (!in_array('2_0', $data['sectionshidden']))
			<div class="nest-pdf-ua-utility">
				<table>
					<tr>
						<th colspan="3" class="nest-pdf-ua-utility-top">Electricity</th>
					</tr>
					<tr>
						<th class="nest-pdf-ua-utility-left" rowspan="5">
							<img src="{{ url('/images/tools/electricity.png') }}" style="padding-bottom:0px;width:40px;" /><br />
							Tel: 2887 3411<br />
							Fax: 2510 7667
						</td>
						<td class="nest-pdf-ua-utility-middle">Account Name</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f11'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Billing Address</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f12'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Account No.</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f13'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Effective Date</td>
						<td class="nest-pdf-ua-utility-right">
							@if (strstr($data['fieldcontents']['f14'], '-'))
								{{ \NestDate::nest_date_format($data['fieldcontents']['f14'], "Y-m-d") }}
							@else
								{{ $data['fieldcontents']['f14'] }}
							@endif
						</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Deposit</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f15'] }}</td>
					</tr>
				</table>
			</div>
		@endif
		@if (!in_array('3_0', $data['sectionshidden']))
			<div class="nest-pdf-ua-utility">
				<table>
					<tr>
						<th colspan="3" class="nest-pdf-ua-utility-top">Gas</th>
					</tr>
					<tr>
						<th class="nest-pdf-ua-utility-left" rowspan="5">
							<img src="{{ url('/images/tools/towngas.png') }}" style="padding-bottom:0px;width:60px;" /><br />
							Tel: 2880 6988<br />
							Fax: 2590 7886
						</td>
						<td class="nest-pdf-ua-utility-middle">Account Name</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f16'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Billing Address</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f17'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Account No.</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f18'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Effective Date</td>
						<td class="nest-pdf-ua-utility-right">
							@if (strstr($data['fieldcontents']['f19'], '-'))
								{{ \NestDate::nest_date_format($data['fieldcontents']['f19'], "Y-m-d") }}
							@else
								{{ $data['fieldcontents']['f19'] }}
							@endif
						</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Deposit</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f20'] }}</td>
					</tr>
				</table>
			</div>
		@endif
	</div>
</body>
</html>

















