@extends('layouts.app')

<?php
	$xxx = '';
?>

@section('content')

<div class="nest-new">
    <div class="row">
		<div class="nest-property-edit-wrapper">
			<form action="{{ url('documents/tenancyagreement') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Tenancy Agreement Fields</h4>
						</div>

						<div class="panel-body nest-form-field-wrapper">
							@include('common.errors')

							{{ csrf_field() }}
							
							{{ Form::hidden('id', $data['id']) }}
							{{ Form::hidden('docversion', $data['docversion']) }}
							{{ Form::hidden('shortlistid', $data['shortlistid']) }}
							{{ Form::hidden('propertyid', $data['propertyid']) }}
							{{ Form::hidden('consultantid', $data['consultantid']) }}
							{{ Form::hidden('clientid', $data['clientid']) }}
							
							@foreach ($fields as $field)
								@if (count($field) >= 3)
									@if ($field[1] == 'date')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">
													{{ $field[2] }} 
													@if (isset($helppics['fields'][$field[0]]) && $helppics['fields'][$field[0]] != '')
														<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['fields'][$field[0]] }}')"><i class="fa fa-info-circle"></i></a>
													@endif
												</div>
											</div>
											<div class="col-xs-8">
												<input type="date" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'hidden')
										{{ Form::hidden('f'.$field[0], old('f'.$field[0], $data['fieldcontents']['f'.$field[0]])) }}
									@elseif ($field[1] == 'text')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">
													{{ $field[2] }} 
													@if (isset($helppics['fields'][$field[0]]) && $helppics['fields'][$field[0]] != '')
														<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['fields'][$field[0]] }}')"><i class="fa fa-info-circle"></i></a>
													@endif
												</div>
											</div>
											<div class="col-xs-8">
												@if (isset($field[3]))
													<input type="text" aa name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}" placeholder="{{ $field[3] }}">
												@else

													@if( $field[0] == "41")
														<select id="f41-1" style="margin-bottom: 5px; padding-left: 8px;">
															<option value="">Select</option>
															<option value="Early Possession">Early Possession</option>
															<option value="Rent Free">Rent Free</option>
														</select>	
													@endif

													<input type="text"  name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
													
												@endif
											</div>
											<div class="clearfix"></div>
										</div>	
									@elseif ($field[1] == 'number')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">
													{{ $field[2] }} 
													@if (isset($helppics['fields'][$field[0]]) && $helppics['fields'][$field[0]] != '')
														<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['fields'][$field[0]] }}')"><i class="fa fa-info-circle"></i></a>
													@endif
												</div>
											</div>
											<div class="col-xs-8">
												<input type="number" name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" value="{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}">
											</div>
											<div class="clearfix"></div>
										</div>		
									@elseif ($field[1] == 'radio')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">
													{{ $field[2] }} 
													@if (isset($helppics['fields'][$field[0]]) && $helppics['fields'][$field[0]] != '')
														<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['fields'][$field[0]] }}')"><i class="fa fa-info-circle"></i></a>
													@endif
												</div>
											</div>
											@php
												$rbs = explode('#', $field[3]);
											@endphp
											<div class="col-xs-8">
												@foreach ($rbs as $index => $rb)
													@if (isset($data['fieldcontents']['f'.$field[0]]))
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, $data['fieldcontents']['f'.$field[0]] == $rb, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@else
														<label for="radio-{{ $field[0] }}-{{ $index }}" class="control-label">{!! Form::radio('f'.$field[0], $rb, false, ['id'=>'radio-'.$field[0].'-'.$index]) !!} {{ $rb }} &nbsp;</label>
													@endif
												@endforeach
											</div>
											<div class="clearfix"></div>
										</div>
									@elseif ($field[1] == 'textarea')
										<div class="nest-property-edit-row">
											<div class="col-xs-4">
												 <div class="nest-property-edit-label">
													{{ $field[2] }} 
													@if (isset($helppics['fields'][$field[0]]) && $helppics['fields'][$field[0]] != '')
														<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['fields'][$field[0]] }}')"><i class="fa fa-info-circle"></i></a>
													@endif
												</div>
											</div>
											<div class="col-xs-8">
												@if( $field[0] == "42" && $data['fieldcontents']['f'.$field[0]]=="")	
													<textarea name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" rows="4">The premises shall be handed over to the Tenant on XXXX. Rental, Management Fees and Government Rates for the period from XXXX to XXXX shall be waived. However, the Tenant should take up all tenant’s responsibilities from the date of handover.</textarea>
												@else
													<textarea name="f{{ $field[0] }}" id="f{{ $field[0] }}" class="form-control" rows="4">{{ old('f'.$field[0], $data['fieldcontents']['f'.$field[0]]) }}</textarea>
												@endif
											</div>
											<div class="clearfix"></div>
										</div>							
									@endif
								@endif
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Tenancy Agreement Content</h4>
						</div>

						<div class="panel-body">
							<b>Tick the boxes for the parts you don't want to show up in the contract:</b><br />
							<div class="nest-form-builder-hide-wrapper">
								@foreach ($hides as $field)
									@if (count($field) >= 4)
										@if ($field[1] == 0)
											@if ($field[2] == 1)
												<div class="nest-form-builder-hide-section">
													@if (isset($helppics['showhide'][$field[0].'_'.$field[1]]) && $helppics['showhide'][$field[0].'_'.$field[1]] != '')
														<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['showhide'][$field[0].'_'.$field[1]] }}')"><i class="fa fa-info-circle"></i></a> 
													@endif
													{{ $field[3] }}
												</div>
												<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
													{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} Hide this section
												</label>
											@else
												<div class="nest-form-builder-hide-section">
													{{ $field[3] }} (tick what to hide)
												</div>
											@endif
										@else
											<label for="{{ 'hides_'.$field[0].'_'.$field[1] }}" class="control-label">
												{!! Form::checkbox('hidevals[]', $field[0].'_'.$field[1], in_array($field[0].'_'.$field[1], $data['sectionshidden']), ['id'=>'hides_'.$field[0].'_'.$field[1]]) !!} {{ $field[3] }}
											</label>
										@endif
									@endif
								@endforeach
								@foreach ($hiddenhides as $hh)
									{{ Form::hidden('hidevals[]', $hh) }}
								@endforeach
							</div>
						</div>
					</div>
					@if (count($replaceFields) > 0)
						<div class="panel panel-default">
							<div class="panel-body">
								@foreach ($replaceFields as $key => $name)
									<div class="nest-property-edit-row">
										<div class="nest-property-edit-label">
											@if (isset($helppics['replace'][$key]) && $helppics['replace'][$key] != '')
												<a href="javascript:;" data-toggle="modal" data-target="#helpModal"  onClick="showHelp('{{ $helppics['replace'][$key] }}')"><i class="fa fa-info-circle"></i></a> 
											@endif
											Replace Text for Section "{{ $name }}"
											@if (!isset($data['replace'][$key]) || trim($data['replace'][$key]) == '')
												<a href="javascript:;" id="showlink{{ $key }}" onClick="showReplace('{{ $key }}');">show</a>
												<a href="javascript:;" id="replacedesclink{{ $key }}" onClick="replaceOriginalText('{{ $key }}')" style="display:none;">replace with original text</a>
											@else
												<a href="javascript:;" onClick="replaceOriginalText('{{ $key }}')">replace with original text</a>
											@endif
										</div>
										@if (isset($replaceFieldsDesc[$key]))
											<div id="replacedesc{{ $key }}" style="display:none;">
												{{ $replaceFieldsDesc[$key] }}
											</div>
										@endif
										@if (isset($data['replace'][$key]) && trim($data['replace'][$key]) != '')
											<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
										@else
											<textarea name="replace[{{ $key }}]" id="replace-{{ $key }}" class="form-control nest-replace-field" rows="2" style="display:none;">{{ old('replace[$key]', $data['replace'][$key]) }}</textarea>
										@endif
									</div>
								@endforeach
							</div>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-xs-12">
								@if ($data['id'] > 0)
									<a id="pdfbutton" href="{{ url('/documents/'.$data['shortlistid'].'/tenancyagreementpdf/'.$data['propertyid']) }}" target="_blank" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-download"></i>Download PDF </a>
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Update </button>
								@else
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-pencil"></i>Save </button>
								@endif
								@if ($shortlist->type_id == 2)
									<a href="{{ url('/documents/index-sale/'.$shortlist->id) }}" class="nest-button nest-right-button btn btn-default">
										<i class="fa fa-btn fa-arrow-left"></i>Back to Documents
									</a>
								@else
									<a href="{{ url('/documents/index-lease/'.$shortlist->id) }}" class="nest-button btn btn-default">
										<i class="fa fa-btn fa-arrow-left"></i>Back to Documents
									</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				
			</form>
        </div>
	</div>
</div>
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalTitle">
	<div class="modal-dialog nest-property-list-dialog" role="document" style="max-width:800px;">
		<div class="modal-content" id="helpcontent">
			
		</div>
	</div>
</div>
<script>
	jQuery('form').on('focus', 'input[type=number]', function (e) {
		jQuery(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	jQuery('form').on('blur', 'input[type=number]', function (e) {
		jQuery(this).off('mousewheel.disableScroll');
	});
	jQuery('input').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
	});
	jQuery('textarea').on('focus', function(){
		jQuery('#pdfbutton').css('display', 'none');
	});
	function showReplace(id){
		jQuery('#replace-'+id).css('display', 'block');
		jQuery('#showlink'+id).css('display', 'none');
		jQuery('#replacedesclink'+id).css('display', 'inline');
	}
	function replaceOriginalText(id){
		var content = jQuery('#replacedesc'+id).html();
		jQuery('#replace-'+id).text(content.trim());
	}
	function showHelp(src){
		jQuery('#helpcontent').html('<img src="'+src+'" width="100%" />');
	}

	jQuery('#f41-1').on('change', function(){
		jQuery('#f41').val($(this).val());
	});
</script>
@endsection
