@extends('layouts.app')

@section('content')
<div class="nest-new container">
	<div class="nest-property-edit-wrapper row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="">Documents {{ $shortlist['client']->name() }} (Lease)</h4>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Properties</u>
								</div>
								<div class="clearfix"></div>
							</div>
							@if (count($properties) > 0)
								<div style="padding-bottom:10px;">
									@foreach ($properties as $p)
										<div class="col-xs-12">
											{{ $p->shorten_building_name() }} <a href="{{ url('/documents/'.$shortlist->id.'/docdelproperty/'.$p->id) }}">del</a>
										</div>
									@endforeach
									<div class="clearfix"></div>
								</div>
							@endif
							<div class="col-xs-12">
								To add properties, please go to the <a href="{{ url('/shortlist/show/'.$shortlist->id) }}">shortlist</a> and pick properties to create documents for.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Comments</u>
								</div>
								<div class="clearfix"></div>
							</div>
							<form action="{{ url('/documents/'.$shortlist->id.'/docsavecomments') }}" method="POST">
								{{ csrf_field() }}
								<div class="nest-property-edit-row">
									<div class="col-xs-12">
										{!! Form::textarea('remarks', $docinfo->remarks, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-xs-12" style="padding-top:15px;">
									<button type="submit" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-pencil-square-o"></i>Save Comments</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Documents Client</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($clientdocs) > 0)
										@foreach ($clientdocs as $d)
											@if ($d->doctype == 'businesscard')
												Business Card
											@elseif ($d->doctype == 'idcard')
												ID Card
											@elseif ($d->doctype == 'passport')
												Passport
											@endif
											from 
											{{ $d->created_at() }}
											@if (isset($consultants[$d->consultantid]))
												by {{ $consultants[$d->consultantid]->name }}
											@endif
											<a href="{{ url('/client/showdoc/'.$d->id.'') }}" target="_blank">download</a><br />
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<a href="" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-repeat"></i>Reload</a>
								<a href="/client/edit/{{ $shortlist['client']->id }}" class="nest-button nest-right-button btn btn-primary" target="_blank"><i class="fa fa-btn fa-upload"></i>Upload / Remove</a>
							</div>
						</div>
					</div>
					@foreach ($properties as $p)
						<div class="panel panel-default">
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
								<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
									<div class="col-xs-12">
										<u>Documents {{ $p->name }}</u>
									</div>
									<div class="nest-propety-docs-uploaded col-xs-12">
										@if (isset($propdocs[$p->id]) && count($propdocs[$p->id]) > 0)
											@foreach ($propdocs[$p->id] as $d)
												@if ($d->doctype == 'landsearch')
													Landsearch
												@endif
												from 
												{{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/property/showdoc/'.$d->id.'') }}" target="_blank">download</a><br />
											@endforeach
										@endif
									</div>
									<div class="clearfix"></div>
									<a href="" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-repeat"></i>Reload</a>
									<a href="/property/edit/{{ $p->id }}#docupload" class="nest-button nest-right-button btn btn-primary" target="_blank"><i class="fa fa-btn fa-upload"></i>Upload / Remove</a>
								</div>
							</div>
						</div>
					@endforeach
					
					
					
					
					
					
					
					
					
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Offer Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['offerletter']))
											@foreach ($docs['offerletter'] as $d)
												@if (isset($offerletters[$d->propertyid]))
													@foreach ($offerletters[$d->propertyid] as $dd)
														<tr>
															<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }} [Version {{ $dd->revision }}]</td>
															<td class="nest-documents-table-buttons">
																<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/offerletterpdf/'.$d->propertyid).'/'.$dd->revision }}" target="_blank">
																	<i class="fa fa-btn fa-download"></i>Download PDF
																</a>
																<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/offerletter/'.$d->propertyid).'/'.$dd->revision }}">
																	<i class="fa fa-btn fa-pencil"></i>Edit
																</a>
															</td>
														</tr>
													@endforeach
														<tr>
															<td></td>
															<td class="nest-documents-table-buttons">
																<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/offerletternewversion/'.$d->propertyid) }}">
																	<i class="fa fa-btn fa-plus"></i>New Version
																</a>
															</td>
														</tr>
													
												@endif
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['offerletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/offerletter/'.$p->id.'/1') }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Offer Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'offerletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="offerletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Letter of Undertaking</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['letterofundertaking']))
											@foreach ($docs['letterofundertaking'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/letterofundertakingpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/letterofundertaking/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['letterofundertaking'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/letterofundertaking/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Letter of Undertaking</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'letterofundertaking')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="letterofundertaking" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Holding Deposit Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['holdingdepositletter']))
											@foreach ($docs['holdingdepositletter'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/holdingdepositletterpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/holdingdepositletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['holdingdepositletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/holdingdepositletter/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Holding Deposit Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'holdingdepositletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="holdingdepositletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Tenant Fee Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['tenantfeeletter']))
											@foreach ($docs['tenantfeeletter'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenantfeeletterpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenantfeeletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['tenantfeeletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenantfeeletter/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Tenant Fee Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'tenantfeeletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="tenantfeeletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Landlord Fee Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['landlordfeeletter']))
											@foreach ($docs['landlordfeeletter'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/landlordfeeletterpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/landlordfeeletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['landlordfeeletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/landlordfeeletter/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Landlord Fee Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'landlordfeeletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="landlordfeeletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Tenancy Agreement</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['tenancyagreement']))
											@foreach ($docs['tenancyagreement'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenancyagreementpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenancyagreement/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['tenancyagreement'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/tenancyagreement/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Tenancy Agreement</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@php
										$hastenancyagreement = false;
									@endphp
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'tenancyagreement')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
												@php
													$hastenancyagreement = true;
												@endphp
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="tenancyagreement" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
								
								
							</div>
							
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Stamp Duty Document</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@php
										$hastenancyagreement = false;
									@endphp
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'stampduty')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
												@php
													$hastenancyagreement = true;
												@endphp
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="stampduty" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
								
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Side Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['sideletter']))
											@foreach ($docs['sideletter'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/sideletterpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/sideletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['sideletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/sideletter/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Side Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'sideletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="sideletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Cover Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['coverletter']))
											@foreach ($docs['coverletter'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/coverletterpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/coverletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['coverletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/coverletter/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Cover Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'coverletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="coverletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Custom Letter</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['customletter']))
											@foreach ($docs['customletter'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/customletterpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/customletter/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['customletter'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/customletter/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Custom Letter</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'customletter')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="customletter" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Handover Report</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['handoverreport']))
											@foreach ($docs['handoverreport'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreportpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreport/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['handoverreport'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreport/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Handover Report</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'handoverreport')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="handoverreport" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Handover Report (Custom)</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['handoverreportext']))
											@foreach ($docs['handoverreportext'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreportextpdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreportext/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['handoverreportext'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/handoverreportext/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Utility Accounts</u>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12">
									<table class="nest-documents-table">
										@if (isset($docs['utilityaccounts']))
											@foreach ($docs['utilityaccounts'] as $d)
													<tr>
														<td>{{ $docsprops[$d->propertyid]->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/utilityaccountspdf/'.$d->propertyid) }}" target="_blank">
																<i class="fa fa-btn fa-download"></i>Download PDF
															</a>
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/utilityaccounts/'.$d->propertyid) }}">
																<i class="fa fa-btn fa-pencil"></i>Edit
															</a>
														</td>
													</tr>
												
											@endforeach
										@endif
										@if (count($properties) > 0)
											@foreach ($properties as $p)
												@if (!isset($docs['utilityaccounts'][$p->id]))
													<tr>
														<td>{{ $p->shorten_building_name() }}</td>
														<td class="nest-documents-table-buttons">
															<a class="nest-button nest-right-button btn btn-primary" href="{{ url('/documents/'.$shortlist->id.'/utilityaccounts/'.$p->id) }}">
																<i class="fa fa-btn fa-plus"></i>Create
															</a>
														</td>
													</tr>
												@endif
											@endforeach
										@endif
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Filled Utility Accounts</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'utilityaccounts')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedoclease/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="utilityaccounts" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
