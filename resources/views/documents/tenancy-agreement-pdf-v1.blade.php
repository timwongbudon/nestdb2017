<html>
<head>

<title>Tenancy Agreement</title>

<style>
@font-face {
    font-family: Myriad;
	src: url('{{asset('fonts/myriad-pro-regular.ttf')}}'); 
}
@font-face {
    font-family: MyriadBold;
	src: url('{{asset('fonts/myriad-pro-bold.ttf')}}'); 
}
body{
}
@page {
	margin: 1.2cm 1.5cm 1.5cm;
}

#header,
#footer{
	position: fixed;
	left: 0;
	right: 0;
	color: #000000;
	font-size: 75%;
	opacity:0.5;
}

#header {
	top: -85px;
	padding-bottom:5px;
	z-index:10;
}

#footer {
	bottom: 0;
	border-top: 0.1pt solid #000000;
	padding-top:5px;
}

#footer table {
	width: 100%;
	border-collapse: collapse;
	border: none;
}

#footer td {
	padding: 0;
	width: 50%;
}

.page-number {
  text-align: right;
  font-size:75%;
}

.page-number:before {
	/*content: "Page " counter(page) " of " <script type="text/php">$PAGE_NUM</script>;*/
}


.nest-pdf-ta-section-header{
	text-align:center;
	text-decoration:underline;
	text-transform:uppercase;
	padding-bottom:5px;
}
.nest-pdf-ta-section-subheader{
	text-align:center;
	text-transform:uppercase;	
	padding-bottom:20px;
}
.nest-pdf-ta-paragraph{
	padding-bottom:50px;
	text-align:justify;
}
.nest-pdf-ta-paragraph td{
	vertical-align:top;
	text-align:justify;
}
.nest-pdf-ta-paragraph th{
	font-weight:normal;
	text-align:justify;
}
.nest-pdf-ta-section-bold-header{
	font-weight:bold;
	text-align:center;
	padding-bottom:10px;
}
.nest-pdf-ta-bold{
	font-weight:bold;
}

</style>


</head>
<body>
	<div id="footer" style="border-top:0px !important;">
		<table><tr>
			<td><div class="page-number" style="text-align:center !important;"></div></td>
		</tr></table>
	</div>

	<script type="text/php">
    if (isset($pdf))
    {
        $x = 285;
        $y = 805;
        $text = "Page {PAGE_NUM} of {PAGE_COUNT}";
        $font = "";
        $size = 8;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
		
	<div class="nest-pdf-container"
		<div class="nest-pdf-ta-paragraph" style="padding-bottom:100px;text-align:center;padding-top:100px;">
			@if (trim($data['fieldcontents']['f1']) != '')
				<u>Dated {{ \NestDate::nest_dayofmonth_datetime_format($data['fieldcontents']['f1']) }}</u>
			@else
				<u>xxxxxx</u>
			@endif
		</div>
		<div class="nest-pdf-ta-paragraph" style="text-align:center;">
			<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f5'] }}</div>
			@if ($data['fieldcontents']['f23'] == 'BRI')
				BRI No.: 
			@elseif ($data['fieldcontents']['f23'] == 'HKID')
				HKID No.: 
			@elseif ($data['fieldcontents']['f23'] == 'Passport')
				Passport No.: 
			@endif
			{{ $data['fieldcontents']['f17'] }}<br />
			<br /><br /><br />
			AND<br />
			<br /><br /><br />
			<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f7'] }}</div>
			@if ($data['fieldcontents']['f24'] == 'Passport')
				Passport No.: 
			@elseif ($data['fieldcontents']['f24'] == 'HKID')
				HKID No.: 
			@elseif ($data['fieldcontents']['f24'] == 'BRI')
				BRI No.: 
			@endif
			{{ $data['fieldcontents']['f18'] }}<br />
			<br /><br /><br />
			TENANCY AGREEMENT<br />
			<br />
			of<br />
			<br />
			{{ $data['fieldcontents']['f9'] }}<br />
			<br /><br />
			Commencement Date: {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f19']) }}<br />
			Expiry Date: {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f20']) }}
		</div>
		<div style="page-break-after:always;"></div>
		
		<div class="nest-pdf-ta-section-header">
			Section I
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph">
			@if (trim($data['fieldcontents']['f1']) != '')
				THIS AGREEMENT is made {{ \NestDate::nest_dayofmonth_datetime_format($data['fieldcontents']['f1']) }}
			@else
				THIS AGREEMENT is made xxxxxx
			@endif
			<br /><br />
			BETWEEN the Landlord and the Tenant as particularly described in Part I of the First Schedule.
			<br /><br />
			NOW IT IS AGREED as follows:-
			<br /><br />
			@if (isset($data['replace']['section-i-bottom']) && trim($data['replace']['section-i-bottom']) != '')
				{!! nl2br($data['replace']['section-i-bottom']) !!}
			@else
				The Landlord shall let and the Tenant shall take ALL the Premises (“the Premises”) forming part of the Building (“the Building”) as set out in Part II of the First Schedule TOGETHER with furniture, fixtures, fittings, and appliances (collectively called “the Furniture”), including those set out in the Third Schedule, all on an “as is, where is” basis, AND TOGETHER with the use in common with the Landlord and all others having the like right of the common facilities in the Building and for the Term as set out in Part III of the First Schedule (the “Term”) AND PAYING throughout the Term the rent (the “Rent”) as set out in the Second Schedule and other charges as provided herein. 
			@endif
		</div>
		
		<div class="nest-pdf-ta-section-header">
			Section II
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Rent and other charges
		</div>
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="50px">(1)</td>
					<td>
						<u>Rent</u><br />
						@if (isset($data['replace']['section-ii-1']) && trim($data['replace']['section-ii-1']) != '')
							{!! nl2br($data['replace']['section-ii-1']) !!}
						@else
							The Tenant shall pay the Rent without deductions, monthly in advance, on or before the 
							@if (isset($data['fieldcontents']['f21']) && trim($data['fieldcontents']['f21']) != '')
								{{ trim($data['fieldcontents']['f21']) }} 
							@else
								twelfth 
							@endif
							day of each and every calendar month by bank auto-pay to the Landlord’s specified bank account, as set out in the Second Schedule. 
							@if (!isset($data['fieldcontents']['f21']) || trim($data['fieldcontents']['f21']) != 'first')
								The first and last of such payments shall be apportioned according to the number of days in the month included in the said Term. 
							@endif
							If the 
							@if (isset($data['fieldcontents']['f21']) && trim($data['fieldcontents']['f21']) != '')
								{{ trim($data['fieldcontents']['f21']) }} 
							@else
								twelfth 
							@endif
							day of the month falls on a Sunday or public holiday of Hong Kong, the Tenant shall pay the said Rent on the business day prior to the 
							@if (isset($data['fieldcontents']['f21']) && trim($data['fieldcontents']['f21']) != '')
								{{ trim($data['fieldcontents']['f21']) }} 
							@else
								twelfth 
							@endif
							day of the month concerned.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(2)</td>
					<td><br />
						<u>Default in Payment of Rent</u><br />
						@if (isset($data['replace']['section-ii-2']) && trim($data['replace']['section-ii-2']) != '')
							{!! nl2br($data['replace']['section-ii-2']) !!}
						@else
							If any rent or any part thereof shall be 15 days in arrear (whether legally demanded or not) or if the Tenant shall fail to observe or perform any stipulation herein contained and on his part to be observed or performed or if the Tenant shall become bankrupt or, being a limited company, shall go into liquidation or shall become insolvent, or make any composition or arrangement with creditors, or shall suffer any execution to be levied on the said premises, or otherwise on the Tenant's goods, it shall be lawful for the Landlord to re-enter upon the said premises or upon any part thereof in the name of the whole, and this Tenancy shall thereupon absolutely cease, and determine but without prejudice, to any claim which the Landlord may have against the Tenant, in respect of any breach of the stipulations on the part of the Tenant herein contained, or to the Landlord's right to deduct all monetary loss thereby incurred from the deposit paid by the Tenant.  
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(3)</td>
					<td><br />
						<u>Utility Charges and Deposits</u><br />
						@if (isset($data['replace']['section-ii-3']) && trim($data['replace']['section-ii-3']) != '')
							{!! nl2br($data['replace']['section-ii-3']) !!}
						@else
							The Tenant shall pay all deposits and charges in respect of any gas, water, electricity, telephone, internet, broadband, cable TV and any other utilities consumed on or in the Premises.
						@endif
					</td>
				</tr>
			</table>
		</div>
		@if (!in_array('3_0', $data['sectionshidden']))
			<div style="page-break-after:always;"></div>
		@endif
		
		
		<div class="nest-pdf-ta-section-header">
			Section III
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Deposit
		</div>
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="50px">(1)</td>
					<th colspan="2">
						<u>Deposit</u>
					</th>
				</tr>
				<tr>
					<td></td>
					<td width="50px">(i)</td>
					<td>
						@if (isset($data['replace']['section-iii-1-i']) && trim($data['replace']['section-iii-1-i']) != '')
							{!! nl2br($data['replace']['section-iii-1-i']) !!}
						@else
							Upon the signing of this Agreement, the Tenant shall pay a deposit to the Landlord as specified in Part IV of the First Schedule (the “Deposit”), Provided Always that the Deposit shall be equivalent to twice the amount of the Rent as security for the due payment of all sums payable herein and the due observance by the Tenant of the terms and conditions of this Agreement.  
						@endif
					</td>
				</tr>
				<tr>
					<td></td>
					<td><br />(ii)</td>
					<td><br />
						@if (isset($data['replace']['section-iii-1-ii']) && trim($data['replace']['section-iii-1-ii']) != '')
							{!! nl2br($data['replace']['section-iii-1-ii']) !!}
						@else
							The Landlord may deduct from the Deposit the amount of Rent and other charges, expenses, loss or damage sustained by the Landlord as the result of any breach of this Agreement by the Tenant. In the event of any deduction being made by the Landlord from the Deposit, the Tenant shall on demand by the Landlord make a further deposit equal to the amount so deducted and failure by the Tenant to do so shall entitle the Landlord to forthwith terminate this Agreement but without prejudice to any right of action by the Landlord in respect of any outstanding breach of this Agreement by the Tenant.
						@endif
					</td>
				</tr>
				<tr>
					<td></td>
					<td><br />(iii)</td>
					<td><br />
						@if (isset($data['replace']['section-iii-1-iii']) && trim($data['replace']['section-iii-1-iii']) != '')
							{!! nl2br($data['replace']['section-iii-1-iii']) !!}
						@else
							In the event and upon the Landlord selling the said premises during the term of this tenancy, the Landlord shall be entitled to transfer the said deposit or the balance thereof to the new owner subject to the Landlord obtaining an undertaking from the new owner given in favour of the Landlord and the Tenant to hold and refund the said deposit to the Tenant in accordance with the terms and conditions of this Agreement.
						@endif
					</td>
				</tr>
				<tr>
					<td></td>
					<td><br />(iv)</td>
					<td><br />
						@if (isset($data['replace']['section-iii-1-iv']) && trim($data['replace']['section-iii-1-iv']) != '')
							{!! nl2br($data['replace']['section-iii-1-iv']) !!}
						@else
							The Tenant shall not apply the Deposit as payment of Rent or payment in lieu of notice.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(2)</td>
					<th colspan="2"><br />
						<u>Repayment of Deposit</u><br />
						@if (isset($data['replace']['section-iii-2']) && trim($data['replace']['section-iii-2']) != '')
							{!! nl2br($data['replace']['section-iii-2']) !!}
						@else
							Subject to the above, the Deposit shall be refunded to the Tenant without interest within {{ $data['fieldcontents']['f2'] }} days after the expiration or sooner determination of this Agreement and delivery of vacant possession of the Premises to the Landlord, and after settlement of all claims by the Landlord against the Tenant in respect of any breach, non-observance or non-performance of any of the agreements, stipulations, terms or conditions herein contained, and on the part of the Tenant to be observed and performed whichever is the later. 
						@endif
					</th>
				</tr>
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-header">
			Section IV
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Tenant's Obligations
		</div>
		<div class="nest-pdf-ta-paragraph">
			The Tenant also agrees with the Landlord as follows:
			@php $iIndex = 1 @endphp
			<table>
				<tr>
					<td width="50px"><br />({{$iIndex++}})</td>
					<td><br />
						<u>User</u><br />
						@if (isset($data['replace']['section-iv-1']) && trim($data['replace']['section-iv-1']) != '')
							{!! nl2br($data['replace']['section-iv-1']) !!}
						@else
							To use the Premises for domestic purpose only. The Premises shall only be occupied by the Tenant and his immediate family. 
						@endif
					</td>
				</tr>
				@if (!in_array('8_0', $data['sectionshidden']))
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Compliance with Ordinances</u><br />
						@if (isset($data['replace']['section-iv-2']) && trim($data['replace']['section-iv-2']) != '')
							{!! nl2br($data['replace']['section-iv-2']) !!}
						@else
							To comply with and to indemnify the Landlord against the breach of any ordinances of Hong Kong and the Deed of Mutual Covenants of the Building, and any other act, nonfeasance, or negligence by the Tenant, the designated occupier, or any contractors, employees, invitees, visitors, agents, licensees of the Tenant or family of the designated occupier of the Tenant (collectively the “Tenant’s Agents”).
						@endif
					</td>
				</tr>
				@endif
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Fitting Out Interior</u><br />
						@if (isset($data['replace']['section-iv-3']) && trim($data['replace']['section-iv-3']) != '')
							{!! nl2br($data['replace']['section-iv-3']) !!}
						@else
							To maintain the interior of the Premises throughout the Term in good, clean and tenantable condition and repair at the Tenant’s cost and expense (fair wear and tear excepted); and except for hanging a wall-mounted television and small paintings on the walls, the Tenant shall seek the Landlord’s prior approval in writing of all plans and specifications in the event that the Tenant wants to decorate, renovate and/or fit out the interior of the Premises.  
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Good Repair</u><br />
						@if (isset($data['replace']['section-iv-4']) && trim($data['replace']['section-iv-4']) != '')
							{!! nl2br($data['replace']['section-iv-4']) !!}
						@else
							To keep all the Furniture and the interior of the Premises, windows, glass, electrical wiring, drains, pipes, sanitary or plumbing apparatus and all additions thereto, in good repair and condition (fair wear and tear excepted).
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Indemnity against Loss / Damage from Interior Defects</u><br />
						@if (isset($data['replace']['section-iv-5']) && trim($data['replace']['section-iv-5']) != '')
							{!! nl2br($data['replace']['section-iv-5']) !!}
						@else
							To be responsible for any loss, damage or injury caused to any person directly or indirectly through whatever cause, including the defective or damaged condition of any part of the interior of the Premises or any Furniture for the repair of which the Tenant is responsible.  The Tenant shall indemnify the Landlord against all such costs, losses and claims as a result therefrom.  As the occupier of the Premises, the Tenant is obligated under Hong Kong law to effect and maintain during the Term adequate insurance coverage against occupiers liability.  
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Landlord to Enter and View</u><br />
						@if (isset($data['replace']['section-iv-6']) && trim($data['replace']['section-iv-6']) != '')
							{!! nl2br($data['replace']['section-iv-6']) !!}
						@else
							To permit the Landlord to enter upon the Premises, with prior appointment to be made with the Tenant (consent shall not be unreasonably withheld), to view the condition and take inventories of the Furniture and to carry out any repair required to be done, provided that in the event of an emergency, the Landlord or its authorized agents may enter without notice and forcibly if necessary.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Yield Up Premises and Reinstatement</u><br />
						@if (isset($data['replace']['section-iv-7']) && trim($data['replace']['section-iv-7']) != '')
							{!! nl2br($data['replace']['section-iv-7']) !!}
						@else
							To yield up the Premises and the Furniture and additions thereto at the expiration or sooner termination of the Term in good, clean and tenantable condition and repair (fair wear and tear excepted). Where the Tenant has made any alterations or renovations, with or without the Landlord’s consent and/or any damage caused, the Tenant shall at its cost and expense reinstate the Premises and the Furniture in its original condition before handing over the Premises to the Landlord. 
						@endif
					</td>
				</tr>
				@if ( !empty($data['fieldcontents']['tenantOtherConditions']) && !in_array('9_0', $data['sectionshidden']) )
				<tr>
					<td><br />({{$iIndex++}})</td>
					<td><br />
						<u>Other Conditions: </u><br />
						Prior to the handover of the premises / commencement of the tenancy, the TENANT agrees to carry out the following:
						@php 
							if ( !empty( trim( $data['replace']['tenantOtherConditions'] ) ) ) {
								$tenantConditions = explode("\n", $data['replace']['tenantOtherConditions']); 
							} else {
								$tenantConditions = explode("\n", $data['fieldcontents']['tenantOtherConditions']); 
							}
						@endphp
						<br>
						<ul type="a">
							@foreach ($tenantConditions as $item)
								@if ( !empty( trim($item) )  )
									<li>{{$item}}</li>			
								@endif
							@endforeach
						</ul>
					</td>
				</tr>
				@endif
			</table>
		</div>
		
		@if (!in_array('4_0', $data['sectionshidden']))
			<div style="page-break-after:always;"></div>
		@endif
		
		
		<div class="nest-pdf-ta-section-header">
			Section V
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Landlord's obligations
		</div>
		<div class="nest-pdf-ta-paragraph">
			The Landlord agrees with the Tenant as follows:-
			<table>
				<tr>
					<td width="50px"><br />(1)</td>
					<th colspan="2"><br />
						<u>Premises and Furniture</u><br />
						@if (isset($data['replace']['section-v-1']) && trim($data['replace']['section-v-1']) != '')
							{!! nl2br($data['replace']['section-v-1']) !!}
						@else
							To convey to the Tenant at the start of the Term the Premises and the Furniture in good, clean and tenantable condition and repair, with all of the Furniture in good working order.  Further, to maintain Premises and the Furniture in good working order and repair at the Landlord’s expense, except where, pursuant to Section IV, the Tenant is responsible for the expense of repairs.
						@endif
					</th>
				</tr>
				<tr>
					<td><br />(2)</td>
					<th colspan="2"><br />
						<u>Quiet Enjoyment</u>
					</th>
				</tr>
				<tr>
					<td></td>
					<td width="50px">(i)</td>
					<td>
						@if (isset($data['replace']['section-v-2-i']) && trim($data['replace']['section-v-2-i']) != '')
							{!! nl2br($data['replace']['section-v-2-i']) !!}
						@else
							Subject to the Tenant duly paying the Rent and other charges and observing the terms and conditions of this Agreement, to permit Tenant to have quiet possession and enjoyment of the Premises during the Term without any interruption by the Landlord.
						@endif
					</td>
				</tr>
				<tr>
					<td></td>
					<td><br />(ii)</td>
					<td><br />
						@if (isset($data['replace']['section-v-2-ii']) && trim($data['replace']['section-v-2-ii']) != '')
							{!! nl2br($data['replace']['section-v-2-ii']) !!}
						@else
							The Landlord declares that he has not received and does not have knowledge of existence of any notice relating to the renovation or refurbishment of the common parts or external wall of the building of which the said premises form part.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(3)</td>
					<th colspan="2"><br />
						<u>Payment of Fees</u><br />
						@if (isset($data['replace']['section-v-3']) && trim($data['replace']['section-v-3']) != '')
							{!! nl2br($data['replace']['section-v-3']) !!}
						@else
							To pay the Management Fees, Government Rates and Rent, and Property Tax. 
						@endif
					</th>
				</tr>
				<tr>
					<td><br />(4)</td>
					<th colspan="2"><br />
						<u>Main Structure, Structural Repair and Latent Defects</u><br />
						@if (isset($data['replace']['section-v-4']) && trim($data['replace']['section-v-4']) != '')
							{!! nl2br($data['replace']['section-v-4']) !!}
						@else
							To keep the main structure of the Premises in proper and tenantable repair and condition, and to be responsible for structural repairs and latent defects whenever necessary, except those caused by the act, negligence or nonfeasance by the Tenant or the Tenant’s Agents. 
						@endif
					</th>
				</tr>
				@if ( !empty($data['fieldcontents']['landLordOtherConditions']) && !in_array('10_0', $data['sectionshidden']) )
				<tr>
					<td><br />(5)</td>
					<th colspan="2"><br />
						<u>Other Conditions: </u><br />
						Prior to the handover of the premises / commencement of the tenancy, the LANDLORD agrees to carry out the following:
						<br>
						@php
							if ( !empty ( trim($data['replace']['landLordOtherConditions'] ) ) ) {
								$landLordConditions = explode("\n", $data['replace']['landLordOtherConditions']); 
							} else {
								$landLordConditions = explode("\n", $data['fieldcontents']['landLordOtherConditions']); 
							}
						@endphp
						<ul type="a">
							@foreach ($landLordConditions as $item)
								@if ( !empty( trim($item) )  )
									<li>{{$item}}</li>			
								@endif
							@endforeach
						</ul>
					</th>
				</tr>
				@endif
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-header">
			Section VI
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Restrictions and Prohibitions
		</div>
		<div class="nest-pdf-ta-paragraph">
			The Tenant hereby further agrees with the Landlord as follows:-
			<table>
				<tr>
					<td width="50px"><br />(1)</td>
					<th colspan="2"><br />
						<u>Installation and Alterations</u>
					</th>
				</tr>
				<tr>
					<td></td>
					<td width="50px">(a)</td>
					<td>
						@if (isset($data['replace']['section-vi-1-a']) && trim($data['replace']['section-vi-1-a']) != '')
							{!! nl2br($data['replace']['section-vi-1-a']) !!}
						@else
							Not without the prior written consent of the Landlord, to install or alter any structure, fixtures, partitioning, installation, any electrical wiring, air condition ducting or lighting fixtures on and in the Premises, or any equipment or machinery including any safe which imposes a weight on the flooring in excess of its design.
						@endif
					</td>
				</tr>
				<tr>
					<td></td>
					<td><br />(b)</td>
					<td><br />
						@if (isset($data['replace']['section-vi-1-b']) && trim($data['replace']['section-vi-1-b']) != '')
							{!! nl2br($data['replace']['section-vi-1-b']) !!}
						@else
							Not to damage, remove or alter any doors, windows, walls, or any part of the fabric of the Premises nor any of the plumbing or sanitary apparatus or installations. 
						@endif
					</td>
				</tr>
				<tr>
					<td></td>
					<td><br />(c)</td>
					<td><br />
						@if (isset($data['replace']['section-vi-1-c']) && trim($data['replace']['section-vi-1-c']) != '')
							{!! nl2br($data['replace']['section-vi-1-c']) !!}
						@else
							Not to affix anything or paint or make any alteration to the exterior of the Premises.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(2)</td>
					<th colspan="2"><br />
						<u>Locks</u><br />
						@if (isset($data['replace']['section-vi-2']) && trim($data['replace']['section-vi-2']) != '')
							{!! nl2br($data['replace']['section-vi-2']) !!}
						@else
							Not without the prior written consent of the Landlord to install additional locks, bolts or other fittings to the entrance doors or grille gate to the Premises, and in the event that consent is given under this Clause, to immediately deposit with the Landlord duplicate keys to all such additional locks, or other fittings as may be approved for installation. All keys are to be returned to the Landlord pursuant to the Tenant’s obligations under Section IV clause (7) of this Agreement.
						@endif
					</th>
				</tr>
				<tr>
					<td><br />(3)</td>
					<th colspan="2"><br />
						<u>Illegal Use or Nuisance</u><br />
						@if (isset($data['replace']['section-vi-3']) && trim($data['replace']['section-vi-3']) != '')
							{!! nl2br($data['replace']['section-vi-3']) !!}
						@else
							Not to use the Premises for any illegal, immoral or improper purposes, nor to cause disturbance or nuisance to the occupiers of the neighbouring premises.  Not to place or leave or (so far as may be in his power) suffer or permit to be placed or left any boxes furniture articles or rubbish in the entrance of the Premises or the entrance of or any of the staircases passages or landings of the said building used in common with other occupiers of the said building or otherwise encumber the same.
						@endif
					</th>
				</tr>
				@if (!in_array('1_0', $data['sectionshidden']))
					<tr>
						<td><br />(4)</td>
						<th colspan="2"><br />
							<u>Pets and Infestation</u><br />
							@if (isset($data['replace']['section-vi-4']) && trim($data['replace']['section-vi-4']) != '')
								{!! nl2br($data['replace']['section-vi-4']) !!}
							@else
								Not to keep any pets or animals in the said premises which would cause nuisance or annoyance or threat to other tenants or occupiers of the building of which the said premises forms part, and to take all precautions to prevent the Premises from becoming infested by termites, rats, mice, cockroaches or any other pests or vermin. 
							@endif
						</th>
					</tr>
				@endif
				<tr>
					@if (!in_array('1_0', $data['sectionshidden']))
						<td><br />(5)</td>
					@else
						<td><br />(4)</td>
					@endif
					<th colspan="2"><br />
						<u>Subletting, Assigning</u><br />
						@if (isset($data['replace']['section-vi-5']) && trim($data['replace']['section-vi-5']) != '')
							{!! nl2br($data['replace']['section-vi-5']) !!}
						@else
							Not to assign, underlet, share, lend or otherwise part with the possession of the Premises whereby any third person obtains the use or possession of the Premises.  In such event, this Agreement shall terminate forthwith. The Tenant shall forthwith vacate the Premises on Landlord’s notice.   This tenancy shall be personal to the Tenant only.
						@endif
					</th>
				</tr>
				<tr>
					@if (!in_array('1_0', $data['sectionshidden']))
						<td><br />(6)</td>
					@else
						<td><br />(5)</td>
					@endif
					<th colspan="2"><br />
						<u>Dangerous Articles</u><br />
						@if (isset($data['replace']['section-vi-6']) && trim($data['replace']['section-vi-6']) != '')
							{!! nl2br($data['replace']['section-vi-6']) !!}
						@else
							Not to store on or in the Premises any combustible or hazardous goods.
						@endif
					</th>
				</tr>
			</table>
		</div>
		
		@if (!in_array('5_0', $data['sectionshidden']))
			<div style="page-break-after:always;"></div>
		@endif
		
		
		<div class="nest-pdf-ta-section-header">
			Section VII
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Exclusions
		</div>
		<div class="nest-pdf-ta-paragraph">
			IT IS HEREBY FURTHER EXPRESSLY AGREED that the Landlord shall not in any circumstances be liable to the Tenant or any other person whomsoever:-
			<table>
				<tr>
					<td width="50px"><br />(1)</td>
					<td><br />
						<u>Lifts and Utilities, Fire and Overflow of Water</u><br />
						@if (isset($data['replace']['section-vii-1']) && trim($data['replace']['section-vii-1']) != '')
							{!! nl2br($data['replace']['section-vii-1']) !!}
						@else
							In respect of any loss or damage to person or property sustained by the Tenant owing to any defect in or breakdown of the lifts, electric power and water supplies, or any other service provided in the Premises or the Building, or owing to the escape of fumes, smoke, fire or any other substance or thing or the overflow of water from anywhere within the Premises or the Building, or
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(2)</td>
					<td><br />
						<u>Security</u><br />
						@if (isset($data['replace']['section-vii-2']) && trim($data['replace']['section-vii-2']) != '')
							{!! nl2br($data['replace']['section-vii-2']) !!}
						@else
							For the security or safekeeping of the Premises or any contents therein and the provision by the Building Management of watchmen and caretakers shall not create any obligation on the part of the Landlord, nor shall the rent and other charges herein provided abate or cease to be payable on account of any security breach. The responsibility for the safety of the Premises and the contents thereof shall at all times rest with the Tenant.
						@endif
					</td>
				</tr>
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-header">
			Section VIII
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Abatement of Rent
		</div>
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="50px"><br />(1)</td>
					<td><br />
						@if (isset($data['replace']['section-viii-1']) && trim($data['replace']['section-viii-1']) != '')
							{!! nl2br($data['replace']['section-viii-1']) !!}
						@else
							If the Premises or the Building shall at any time during the Term be condemned as a dangerous structure, or be subject to a demolition order or closing order, or be destroyed or damaged or inaccessible owing to any calamity or material circumstances beyond the control of the Landlord and not attributable to the negligence or fault of the Tenant or the Tenant’s Agents, and the policy of insurance on the Premises shall not have been vitiated or payment of the policy moneys refused in consequence of any act or default of the Tenant or the Tenant’s Agents, then, the Rent or a fair proportion thereof according to the nature of the damage sustained or order made shall, after the expiration of the then current month, be suspended until the Premises shall again be fit for use.  
						@endif
					</td>
				</tr>
				<tr>
					<td><br />(2)</td>
					<td><br />
						@if (isset($data['replace']['section-viii-2']) && trim($data['replace']['section-viii-2']) != '')
							{!! nl2br($data['replace']['section-viii-2']) !!}
						@else
							Should the Premises or the Building not have been reinstated in the meantime, either the Landlord or the Tenant may at any time after 3 months from the occurrence of such damage or destruction or order, give to the other party 1 month’s notice to terminate this present tenancy and thereupon the same shall cease as from the date of the occurrence of such destruction or damage or order or the Premises becoming inaccessible but without prejudice to the rights of the Landlord in respect of the Rent payable hereunder prior to the coming into effect of the suspension.
						@endif
					</td>
				</tr>
			</table>
		</div>
		
		@if (!in_array('6_0', $data['sectionshidden']))
			<div style="page-break-after:always;"></div>
		@endif
		
		<div class="nest-pdf-ta-section-header">
			Section IX
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Default and Termination
		</div>
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="50px">(1)</td>
					<th colspan="2">
						@if (isset($data['replace']['section-ix-1']) && trim($data['replace']['section-ix-1']) != '')
							{!! nl2br($data['replace']['section-ix-1']) !!}
						@else
							If the Rent or any part thereof is in arrears for 15 days or if the Tenant shall persistently delay in paying the Rent or use the premises for immoral or illegal purposes; or cause nuisance and/or make unauthorized structural alteration to the Premises; or if there shall be any breach of a material term herein contained, in any such case it shall be lawful for the Landlord to forfeit this tenancy and terminate this Agreement forthwith at any time thereafter.
						@endif
					</th>
				</tr>
				<tr>
					<td><br />(2)</td>
					<th colspan="2"><br />
						@if (isset($data['replace']['section-ix-2']) && trim($data['replace']['section-ix-2']) != '')
							{!! nl2br($data['replace']['section-ix-2']) !!}
						@else
							The parties agree it shall be lawful for the Landlord at any time thereafter to re-enter on the Premises; or, at the sole discretion of the Landlord, to serve a notice to quit (the “Notice”) to the Tenant and the Tenant agrees to move out from the Premises within the time specified in the Notice.  The Deposit shall be forfeited to the Landlord without prejudice to the Landlord’s other rights and entitlements.   All costs and expenses incurred by the Landlord in demanding payment of the Rent and other charges shall be paid by the Tenant and shall be recoverable from the Tenant as a debt.
						@endif
					</th>
				</tr>
				@if (!in_array('11_0', $data['sectionshidden']))
					<tr>
						<td><br />(3)</td>
						<th colspan="2"><br />
							<u>Early Termination</u><br />
							
							@if (isset($data['replace']['section-ix-3']) && trim($data['replace']['section-ix-3']) != '')
								{!! nl2br($data['replace']['section-ix-3']) !!}
							@else
								@if (isset($data['fieldcontents']['f22']) && $data['fieldcontents']['f22'] == 'Landlord')
									Notwithstanding anything herein contained to the contrary, it is hereby agreed by the parties hereto that the Landlord shall 
									be at liberty to earlier terminate this Agreement at any time 
									after the first {{ $data['fieldcontents']['f3'] }} of the Term subject to the following conditions:-
								@elseif (isset($data['fieldcontents']['f22']) && $data['fieldcontents']['f22'] == 'Both')
									Notwithstanding anything herein contained to the contrary, it is hereby agreed by the parties hereto that either the Landlord or the Tenant shall 
									be at liberty to earlier terminate this Agreement at any time 
									after the first {{ $data['fieldcontents']['f3'] }} of the Term subject to the following conditions:-
								@else
									Notwithstanding anything herein contained to the contrary, it is hereby agreed by the parties hereto that the Tenant shall 
									be at liberty to earlier terminate this Agreement at any time 
									after the first {{ $data['fieldcontents']['f3'] }} of the Term subject to the following conditions:-
								@endif
							@endif
						</th>
					</tr>
				
					<tr>
						<td></td>
						<td width="50px"><br />(a)</td>
						<td><br />
							@if (isset($data['replace']['section-ix-3-a']) && trim($data['replace']['section-ix-3-a']) != '')
								{!! nl2br($data['replace']['section-ix-3-a']) !!}
							@else
								@if (trim($data['fieldcontents']['f4']) == 'one (1)')
									By giving not less than one (1) calendar month prior notice in writing of such intention to terminate this Agreement 
									or in lieu of such notice pay an amount equivalent to one (1) month of the agreed Rent. 
									The Tenant shall not use the Deposit as Rent in lieu of notice for this purpose.							
								@else
									By giving not less than {{ $data['fieldcontents']['f4'] }} calendar months' prior notice in writing of such intention to terminate this Agreement 
									or in lieu of such notice pay an amount equivalent to {{ $data['fieldcontents']['f4'] }} months of the agreed Rent. 
									The Tenant shall not use the Deposit as Rent in lieu of notice for this purpose.
								@endif
							@endif
						</td>
					</tr>
					<tr>
						<td></td>
						<td><br />(b)</td>
						<td><br />
							@if (isset($data['replace']['section-ix-3-b']) && trim($data['replace']['section-ix-3-b']) != '')
								{!! nl2br($data['replace']['section-ix-3-b']) !!}
							@else
								The Tenant shall duly observe, perform and comply with all agreements, conditions, provisions, terms and stipulations on the Tenant's part herein contained up to the date of such earlier determination.
							@endif
						</td>
					</tr>
					<tr>
						<td></td>
						<td><br />(c)</td>
						<td><br />
							@if (isset($data['replace']['section-ix-3-c']) && trim($data['replace']['section-ix-3-c']) != '')
								{!! nl2br($data['replace']['section-ix-3-c']) !!}
							@else
								The Tenant shall upon the expiration of such notice of earlier termination deliver up vacant possession of the Premises to the Landlord in accordance with Section IV Clause (7) hereof.
							@endif
						</td>
					</tr>
					<tr>
						<td></td>
						<td><br />(d)</td>
						<td><br />
							@if (isset($data['replace']['section-ix-3-d']) && trim($data['replace']['section-ix-3-d']) != '')
								{!! nl2br($data['replace']['section-ix-3-d']) !!}
							@else
								Such earlier termination shall not prejudice the rights of either party against the other in respect of any antecedent claim for breach of agreement and shall not affect the right of the Tenant to recover the Deposit from the Landlord, after due deductions (if any).
							@endif
						</td>
					</tr>
				@endif
			</table>
		</div>
		
		@if (!in_array('7_0', $data['sectionshidden']))
			<div style="page-break-after:always;"></div>
		@endif
		
		<div class="nest-pdf-ta-section-header">
			Section X
		</div>
		<div class="nest-pdf-ta-section-subheader">
			Interpretation and Miscellaneous
			@php $imIndex = 1 @endphp
		</div>
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="50px">({{$imIndex++}})</td>
					<td>
						<u>Condonation not a Waiver</u><br />
						@if (isset($data['replace']['section-x-1']) && trim($data['replace']['section-x-1']) != '')
							{!! nl2br($data['replace']['section-x-1']) !!}
						@else
							No condoning, or overlooking by the Landlord of any default or breach of this Agreement by the Tenant shall operate as a waiver of the Landlord’s rights or so as to affect in any way the rights and remedies of the Landlord hereunder and no waiver by the Landlord shall be implied by anything done or omitted by the Landlord.  Any consent given by the Landlord shall operate as a consent only for the particular matter to which it relates and shall not be construed as dispensing with the necessity of obtaining the specific written consent of the Landlord in the future.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$imIndex++}})</td>
					<td><br />
						<u>Letting Notices</u><br />
						@if (isset($data['replace']['section-x-2']) && trim($data['replace']['section-x-2']) != '')
							{!! nl2br($data['replace']['section-x-2']) !!}
						@else
							Notwithstanding anything herein contained, during the 1 month immediately before the expiration or sooner termination of the Term, the Tenant shall permit Landlord’s authorized agent, with prior appointment to be made with the Tenant (consent shall not be unreasonably withheld), to enter and view the Premises at all reasonable times and the Landlord shall during such period be at liberty to make known to the public that the Premises are to be let and other relevant information.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$imIndex++}})</td>
					<td><br />
						<u>Service of Notice</u><br />
						@if (isset($data['replace']['section-x-3']) && trim($data['replace']['section-x-3']) != '')
							{!! nl2br($data['replace']['section-x-3']) !!}
						@else
							Any notice required to be served on either the Tenant or the Landlord shall be deemed to have been validly served if delivered by ordinary or registered post or left at the Premises or at the last known address of the Tenant or the Landlord.   A notice sent by ordinary or registered post shall be deemed to be given next business day after posting.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$imIndex++}})</td>
					<td><br />
						<u>Stamp Duty and Costs</u><br />
						@if (isset($data['replace']['section-x-4']) && trim($data['replace']['section-x-4']) != '')
							{!! nl2br($data['replace']['section-x-4']) !!}
						@else
							Each party shall bear its own costs of the preparation of this Agreement and the stamp duty thereon shall be borne by the Landlord and the Tenant in equal shares.
						@endif
					</td>
				</tr>
				<tr>
					<td><br />({{$imIndex++}})</td>
					<td><br />
						<u>No Premium Paid</u><br />
						@if (isset($data['replace']['section-x-5']) && trim($data['replace']['section-x-5']) != '')
							{!! nl2br($data['replace']['section-x-5']) !!}
						@else
							The Tenant hereby declares that he has not paid any premium, construction money or other consideration for the tenancy.
						@endif
					</td>
				</tr>
				@if (!in_array('12_0', $data['sectionshidden']))
				<tr>
					<td><br />({{$imIndex++}})</td>
					<td><br />
						<u>Counterparts</u><br />
						@if (isset($data['replace']['section-x-6']) && trim($data['replace']['section-x-6']) != '')
							{!! nl2br($data['replace']['section-x-6']) !!}
						@else
							This agreement may be executed by each party, in two or more counterparts. These said counterparts, all of which together, shall constitute one and the same instrumental and shall be deemed to be a full and complete contract between the parties hereto. While there may be duplicate originals of this Agreement, including counterparts, only one need be produced as evidence of terms thereof.
						@endif
					</td>
				</tr>
				@endif
				@if (isset($data['fieldcontents']['f32']) && trim($data['fieldcontents']['f32']) != '')
					<tr>
						<td><br />({{$imIndex++}})</td>
						<td><br />
							<u>{{ trim($data['fieldcontents']['f31']) }}</u><br />
							{{ trim($data['fieldcontents']['f32']) }}
						</td>
					</tr>
				@endif
				@if (isset($data['fieldcontents']['f34']) && trim($data['fieldcontents']['f34']) != '')
					<tr>
						<td><br />({{$imIndex++}})</td>
						<td><br />
							<u>{{ trim($data['fieldcontents']['f33']) }}</u><br />
							{{ trim($data['fieldcontents']['f34']) }}
						</td>
					</tr>
				@endif
				@if (isset($data['fieldcontents']['f36']) && trim($data['fieldcontents']['f36']) != '')
					<tr>
						<td><br />({{$imIndex++}})</td>
						<td><br />
							<u>{{ trim($data['fieldcontents']['f35']) }}</u><br />
							{{ trim($data['fieldcontents']['f36']) }}
						</td>
					</tr>
				@endif
				@if (isset($data['fieldcontents']['f38']) && trim($data['fieldcontents']['f38']) != '')
					<tr>
						<td><br />({{$imIndex++}})</td>
						<td><br />
							<u>{{ trim($data['fieldcontents']['f37']) }}</u><br />
							{{ trim($data['fieldcontents']['f38']) }}
						</td>
					</tr>
				@endif
				@if (isset($data['fieldcontents']['f40']) && trim($data['fieldcontents']['f40']) != '')
					<tr>
						<td><br />({{$imIndex++}})</td>
						<td><br />
							<u>{{ trim($data['fieldcontents']['f39']) }}</u><br />
							{{ trim($data['fieldcontents']['f40']) }}
						</td>
					</tr>
				@endif
				
			</table>
		</div>
		<div style="page-break-after:always;"></div>
		
		
		
		<div class="nest-pdf-ta-section-bold-header">
			<u>THE FIRST SCHEDULE ABOVE REFERRED TO</u>
		</div>
		<div class="nest-pdf-ta-section-header" style="padding-bottom:5px;">
			Part I
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="120px">Landlord:</td>
					<td>
						<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f5'] }}</div>
						@if ($data['fieldcontents']['f23'] == 'BRI')
							BRI No.: 
						@elseif ($data['fieldcontents']['f23'] == 'HKID')
							HKID No.: 
						@elseif ($data['fieldcontents']['f23'] == 'Passport')
							Passport No.: 
						@endif
						{{ $data['fieldcontents']['f17'] }}<br />
						{!! nl2br(trim($data['fieldcontents']['f6'])) !!}
						<br /><br />
					</td>
				</tr>
				<tr>
					<td width="120px">Tenant:</td>
					<td>
						<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f7'] }}</div>
						@if ($data['fieldcontents']['f24'] == 'Passport')
							Passport No.: 
						@elseif ($data['fieldcontents']['f24'] == 'HKID')
							HKID No.:  
						@elseif ($data['fieldcontents']['f24'] == 'BRI')
							BRI No.: 
						@endif
						{{ $data['fieldcontents']['f18'] }}<br />
						{!! nl2br(trim($data['fieldcontents']['f8'])) !!}
						@if (isset($data['fieldcontents']['f25']) && trim($data['fieldcontents']['f25']))
							<br /><br />
						@endif
					</td>
				</tr>
				@if (isset($data['fieldcontents']['f25']) && trim($data['fieldcontents']['f25']) != '' && isset($data['fieldcontents']['f28']) && trim($data['fieldcontents']['f28']) != '')
					<tr>
						<td width="120px">Occupant 1:</td>
						<td>
							<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f25'] }}</div>
							@if ($data['fieldcontents']['f27'] == 'Passport')
								Passport No.: 
							@elseif ($data['fieldcontents']['f27'] == 'HKID')
								HKID No.:  
							@elseif ($data['fieldcontents']['f27'] == 'BRI')
								BRI No.: 
							@endif
							{{ $data['fieldcontents']['f26'] }}
							<br /><br />
						</td>
					</tr>
					<tr>
						<td width="120px">Occupant 2:</td>
						<td>
							<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f28'] }}</div>
							@if ($data['fieldcontents']['f30'] == 'Passport')
								Passport No.: 
							@elseif ($data['fieldcontents']['f30'] == 'HKID')
								HKID No.:  
							@elseif ($data['fieldcontents']['f30'] == 'BRI')
								BRI No.: 
							@endif
							{{ $data['fieldcontents']['f29'] }}
						</td>
					</tr>
				@elseif (isset($data['fieldcontents']['f25']) && trim($data['fieldcontents']['f25']))
					<tr>
						<td width="120px">Occupant:</td>
						<td>
							<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f25'] }}</div>
							@if ($data['fieldcontents']['f27'] == 'Passport')
								Passport No.: 
							@elseif ($data['fieldcontents']['f27'] == 'HKID')
								HKID No.:  
							@elseif ($data['fieldcontents']['f27'] == 'BRI')
								BRI No.: 
							@endif
							{{ $data['fieldcontents']['f26'] }}
						</td>
					</tr>
				@endif
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-header" style="padding-bottom:5px;">
			Part II
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="120px">Premises:</td>
					<td>
						{!! nl2br($data['fieldcontents']['f9']) !!}
					</td>
				</tr>
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-header" style="padding-bottom:5px;">
			Part III
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="120px">Term:</td>
					<td>
						@if (isset($data['replace']['part-iii-terms']) && trim($data['replace']['part-iii-terms']) != '')
							{!! nl2br($data['replace']['part-iii-terms']) !!}
						@else
							{!! nl2br($data['fieldcontents']['f10']) !!} commencing on 
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f19']) }} and expiring on
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f20']) }} (both days inclusive)
						@endif
						
					</td>
				</tr>
				@if (!in_array('2_0', $data['sectionshidden']))
					<tr>
						<td width="120px"><br />Break Clause:</td>
						<td><br />
							@if (isset($data['replace']['break']) && trim($data['replace']['break']) != '')
								{!! nl2br($data['replace']['break']) !!}
							@else
								@if (isset($data['fieldcontents']['f22']) && $data['fieldcontents']['f22'] == 'Landlord')
									The Landlord shall have the right to terminate the lease (by giving two (2) months written notice to the Tenant) after a minimum 
									period of twelve (12) months occupancy.
								@elseif (isset($data['fieldcontents']['f22']) && $data['fieldcontents']['f22'] == 'Both')
									The Tenant and the Landlord shall have the right to terminate the lease (by giving two (2) months written notice to the other Party) 
									after a minimum period of twelve (12) months occupancy.
								@else
									The Tenant shall have the right to terminate the lease (by giving two (2) months written notice to the Landlord) after a minimum 
									period of twelve (12) months occupancy.
								@endif
							@endif
						</td>
					</tr>
				@endif
				@if ( !empty($data['fieldcontents']['f41']) && !empty($data['fieldcontents']['f42']) && !in_array('13_0', $data['sectionshidden']) )
					<tr>
						<td width="120px"><br />{{$data['fieldcontents']['f41']}}:</td>
						<td><br />
							{{$data['fieldcontents']['f42']}}
						</td>
					</tr>
				@endif
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-header" style="padding-bottom:5px;">
			Part IV
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="120px">Prepayment:</td>
					<td>
						Hong Kong Dollars {{ $data['fieldcontents']['f11'] }}
					</td>
				</tr>
				<tr>
					<td width="120px"><br />Deposit:</td>
					<td><br />
						Hong Kong Dollars {{ $data['fieldcontents']['f12'] }}
					</td>
				</tr>
			</table>
		</div>
		<div style="page-break-after:always;"></div>
		
		<div class="nest-pdf-ta-section-bold-header" style="padding-bottom:50px;">
			<u>THE SECOND SCHEDULE ABOVE REFERRED TO</u>
		</div>
		<div class="nest-pdf-ta-section-bold-header">
			PARTICULARS OF RENT
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph" style="padding-bottom:100px;">
			<table>
				<tr>
					<td width="120px">Monthly Rent:</td>
					<td>
						HONG KONG DOLLARS {{ $data['fieldcontents']['f13'] }} per calendar month
					</td>
				</tr>
			</table>
			<br />
			<table>
				<tr><td>
					The Tenant shall pay the Rent by bank auto-pay payable in advance to Landlord’s bank account as from the signing of this Agreement, and thereafter on or before the 
					@if (isset($data['fieldcontents']['f21']) && trim($data['fieldcontents']['f21']) != '')
						{{ trim($data['fieldcontents']['f21']) }}
					@else
						twelfth
					@endif
					day of each month.   
				</td></tr>
			</table>
		</div>
		
		<div class="nest-pdf-ta-section-bold-header">
			PARTICULARS OF LANDLORD’S BANK ACCOUNT
		</div>
		<br />
		<div class="nest-pdf-ta-paragraph">
			<table>
				<tr>
					<td width="220px">Beneficiary Bank:</td>
					<td>
						{{ $data['fieldcontents']['f14'] }}
					</td>
				</tr>
				<tr>
					<td>Beneficiary Name:</td>
					<td>
						{{ $data['fieldcontents']['f15'] }}
					</td>
				</tr>
				<tr>
					<td>Beneficiary Account No.:</td>
					<td>
						{{ $data['fieldcontents']['f16'] }}
					</td>
				</tr>
			</table>
		</div>
		<div style="page-break-after:always;"></div>
		
		<div class="nest-pdf-ta-paragraph">
			AS WITNESS the hands of the parties hereto the day and year first above written.
		</div>
		<div class="nest-pdf-ta-paragraph">
			<table width="100%" style="margin-bottom:50px;">
				<tr>
					<td width="55%" style="text-align:left;">
						SIGNED by the Landlord<br />
						<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f5'] }}</div>
						@if ($data['fieldcontents']['f23'] == 'BRI')
							BRI No.: 
						@elseif ($data['fieldcontents']['f23'] == 'HKID')
							HKID No.: 
						@elseif ($data['fieldcontents']['f23'] == 'Passport')
							Passport No.: 
						@endif
						{{ $data['fieldcontents']['f17'] }}<br />
						<br />
						in the presence	 of:-
					</td>
					<td>
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
					</td>
					<td></td>
				</tr>
			</table>
			<table width="100%" style="margin-bottom:50px;">
				<tr>
					<td width="55%" style="text-align:left;">
						SIGNED by the Tenant<br />
						<div class="nest-pdf-ta-bold">{{ $data['fieldcontents']['f7'] }}</div>
						@if ($data['fieldcontents']['f24'] == 'Passport')
							Passport No.: 
						@elseif ($data['fieldcontents']['f24'] == 'HKID')
							HKID No.: 
						@elseif ($data['fieldcontents']['f24'] == 'BRI')
							BRI No.: 
						@endif
						{{ $data['fieldcontents']['f18'] }}<br />
						<br />
						in the presence	 of:-
					</td>
					<td>
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
					</td>
					<td></td>
				</tr>
			</table>
			<table width="100%" style="margin-bottom:10px;">
				<tr>
					<td width="55%" style="text-align:left;">
						RECEIVED the day and year first above written from<br />
						the Tenant the sum of HONG KONG DOLLARS<br />
						{{ $data['fieldcontents']['f12'] }}<br />
						being the Deposit above mentioned<br />
						paid by the Tenant to the Landlord.
					</td>
					<td>
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
						)<br />
					</td>
					<td></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td width="55%"></td>
					<td>The Landlord</td>
				</tr>
			</table>
		</div>
		
		
		
		
		
		
		
	</div>
</body>
</html>

















