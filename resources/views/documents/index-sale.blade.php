@extends('layouts.app')

@section('content')
<div class="nest-new container">
	<div class="nest-property-edit-wrapper row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="">Documents {{ $shortlist['client']->name() }} (Sale)</h4>
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Properties</u>
								</div>
								<div class="clearfix"></div>
							</div>
							@if (count($properties) > 0)
								<div style="padding-bottom:10px;">
									@foreach ($properties as $p)
										<div class="col-xs-12">
											{{ $p->shorten_building_name() }} <a href="{{ url('/documents/'.$shortlist->id.'/docdelproperty/'.$p->id) }}">del</a>
										</div>
									@endforeach
									<div class="clearfix"></div>
								</div>
							@endif
							<div class="col-xs-12">
								To add properties, please go to the <a href="{{ url('/shortlist/show/'.$shortlist->id) }}">shortlist</a> and pick properties to create documents for.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Comments</u>
								</div>
								<div class="clearfix"></div>
							</div>
							<form action="{{ url('/documents/'.$shortlist->id.'/docsavecomments') }}" method="POST">
								{{ csrf_field() }}
								<div class="nest-property-edit-row">
									<div class="col-xs-12">
										{!! Form::textarea('remarks', $docinfo->remarks, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-xs-12" style="padding-top:15px;">
									<button type="submit" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-pencil-square-o"></i>Save Comments</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Documents Client</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($clientdocs) > 0)
										@foreach ($clientdocs as $d)
											@if ($d->doctype == 'businesscard')
												Business Card
											@elseif ($d->doctype == 'idcard')
												ID Card
											@elseif ($d->doctype == 'passport')
												Passport
											@endif
											from 
											{{ $d->created_at() }}
											@if (isset($consultants[$d->consultantid]))
												by {{ $consultants[$d->consultantid]->name }}
											@endif
											<a href="{{ url('/client/showdoc/'.$d->id.'') }}" target="_blank">download</a><br />
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<a href="" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-repeat"></i>Reload</a>
								<a href="/client/edit/{{ $shortlist['client']->id }}" class="nest-button nest-right-button btn btn-primary" target="_blank"><i class="fa fa-btn fa-upload"></i>Upload / Remove</a>
							</div>
						</div>
					</div>
					@foreach ($properties as $p)
						<div class="panel panel-default">
							<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
								<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
									<div class="col-xs-12">
										<u>Documents {{ $p->name }}</u>
									</div>
									<div class="nest-propety-docs-uploaded col-xs-12">
										@if (isset($propdocs[$p->id]) && count($propdocs[$p->id]) > 0)
											@foreach ($propdocs[$p->id] as $d)
												@if ($d->doctype == 'landsearch')
													Landsearch
												@endif
												from 
												{{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/property/showdoc/'.$d->id.'') }}" target="_blank">download</a><br />
											@endforeach
										@endif
									</div>
									<div class="clearfix"></div>
									<a href="" class="nest-button nest-right-button btn btn-primary"><i class="fa fa-btn fa-repeat"></i>Reload</a>
									<a href="/property/edit/{{ $p->id }}#docupload" class="nest-button nest-right-button btn btn-primary" target="_blank"><i class="fa fa-btn fa-upload"></i>Upload / Remove</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Provisional Sale & Purchase Agreement</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'provisionalsale')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedocsale/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="provisionalsale" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Signed Sale & Purchase Agreement</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'salepurchase')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedocsale/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="salepurchase" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="nest-property-edit-wrapper row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Riders</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'salerider')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedocsale/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="salerider" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">
							<div class="nest-property-edit-label-features" style="padding-bottom:10px;">
								<div class="col-xs-12">
									<u>Other Documents</u>
								</div>
								<div class="nest-propety-docs-uploaded col-xs-12">
									@if (count($docuploads) > 0)
										@foreach ($docuploads as $d)
											@if ($d->doctype == 'saleothers')
												Upload {{ $d->created_at() }}
												@if (isset($consultants[$d->consultantid]))
													by {{ $consultants[$d->consultantid]->name }}
												@endif
												<a href="{{ url('/documents/showdoc/'.$d->id.'') }}" target="_blank">download</a>
												@if (Auth::user()->id == $d->consultantid || Auth::user()->getUserRights()['Superadmin'] || Auth::user()->getUserRights()['Director'])
													<a href="{{ url('/documents/removedocsale/'.$d->id.'/'.$d->shortlistid) }}" onclick="return confirm('Are you sure you want to remove this document?')">remove</a> 
												@endif
												<br />
											@endif
										@endforeach
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="nest-propety-docs-upload">
									<form action="{{ url('/documents/'.$shortlist->id.'/docstoreupload') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="draganddropupdocument col-xs-8">
											<input type="hidden" value="saleothers" name="doctype" />
											{!! Form::file('upfile', ['id'=>'upfile', 'class'=>'form-control', 'style'=>'visibility:visible;', 'placeholder'=>'Try drag and drop']) !!}
										</div>
										<div class="col-xs-4">
											<button class="nest-button nest-right-button btn btn-primary" type="submit">Upload PDF</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
