<html>
<head>
	<title>Landlord Fee Letter</title>
	<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
   </div>
    <div class="nest-hide-first-header">
        @if (empty($logo) || $logo == 'yes')
            <div style="width: 49%; display: inline-block; text-align: left !important">
                <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
            </div>
            <div style="width: 50%; display: inline-block;">
                <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
            </div>
        @endif
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com
			</div>
        </div>
        @if (empty($logo) || $logo == 'yes')
            
        @else
        <br><br><br><br><br><br><br><br><br><br><br><br>
        @endif
		<div class="nest-pdf-lfl-top-left">
			<div style="padding-bottom:10px;">To: {{ $data['fieldcontents']['f2'] }}</div>
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br />
			@endif
			<div class="nest-pdf-lfl-bold">{{ $data['fieldcontents']['f4'] }}</div>
			<div style="display: block; margin-bottom: 13px;">
				@php
					$e = explode("\n", trim($data['fieldcontents']['f5']));
					if (empty($data['fieldcontents']['f5'])) {
						echo '<span style="color: #fff !important;">Hidden Text</span>'; //added a white text since pdf ignore the styling of empty section
					} else {
						echo nl2br(trim($data['fieldcontents']['f5']));
					}
				@endphp
			</div>
			@if (trim($data['fieldcontents']['f1']) != '')
				<div style="display: block;">
					{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}
				</div>
			@else
				<div style="display: block;">
					xxxxxx
				</div>
			@endif
			<br /><br />
			Dear {{ $data['fieldcontents']['f6'] }},
		</div>
		<div class="nest-pdf-lfl-top-premises">
			Re: {{ $data['fieldcontents']['f7'] }}
		</div>
		<div class="nest-pdf-lfl-content">
			@if (isset($data['replace']['maintext']) && trim($data['replace']['maintext']) != '')
				{!! nl2br($data['replace']['maintext']) !!}
			@else				
				In respect of the above premises, we would like to reconfirm that the Landlord shall pay an agency fee to Nest Property Limited equal to 
				{{ $data['fieldcontents']['f8'] }}% of one month’s rent{{ $data['fieldcontents']['f9'] }} upon the successful signing of the Tenancy Agreement. 
				Cheques should be made payable to “NEST PROPERTY LIMITED”. 
				@if (trim($data['fieldcontents']['f10']) != '')
					For the avoidance of doubt the agency fee for the above-mentioned property, should the fee stated in the Offer Letter be accepted, 
					totals HK${{ number_format($data['fieldcontents']['f10'], 0, '', ',') }}.
				@else
					For the avoidance of doubt the agency fee for the above-mentioned property, should the fee stated in the Offer Letter be accepted, 
					totals HK$xxxxxx.
				@endif
			@endif
		</div>
		
		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
			<div class="nest-pdf-lfl-bottom-1">
				Please acknowledge receipt by countersigning this letter.<br /><br />
				Yours sincerely, 	
			</div>
			<div class="nest-pdf-lfl-signature">
				@if ($consultant->sigpath != '')
					<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" style="max-height:80px;max-width:300px;" />
				@else
					<br /><br /><br />
				@endif
			</div>
			<div class="nest-pdf-lfl-consultant">
				{{ $data['fieldcontents']['f11'] }}<br />
				{{ $data['fieldcontents']['f12'] }}<br />
				Residential Search & Investment
			</div>
			<div class="nest-pdf-lfl-licensnr">
				Individual Licence No: {{ $data['fieldcontents']['f13'] }}
			</div>
			
			<div class="nest-pdf-lfl-signature-names">
				We hereby confirm and agree<br />
				to the above terms and conditions
			</div>
			<br /><br /><br />
			
			__________________________________<br />
			<div class="nest-pdf-lfl-signature-names">
				Name: {{ $data['fieldcontents']['f4'] }}<br />
				Date:
			</div>
			
		</td></tr></table>
	</div>
</body>
</html>

















