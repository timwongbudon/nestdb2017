<html>
<head>

<title>Offer Letter</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 5cm 1.5cm 0.5cm 1.5cm;
}
#footer {
	height: 10px !important;
	margin-bottom: -3px !important;
}
#footer a {
	background-color:#C5C2C0 !important;
}
#header {
	top: -153px;
	padding-bottom:5px;
	z-index:10;
}
.nest-hide-first-header{
	margin-top:-238px;
}
</style>

</head>
<body>
    <div id="header" style="padding-top:0px;padding-left:5px;">
        <div style="text-align:right;">
            <img src="{{ url('/documents/images/nest-letterhead-logo-header.png') }}" style="padding-top:10px;width:90px; margin-bottom: 2px;" />	
            <div class="nest-pdf-ol-top-subject-header">
                {{ $data['fieldcontents']['f5'] }}
            </div>
        </div>
		
		<div>
			<div class="nest-pdf-ol-top-premises-header">
				Re: {{ $data['fieldcontents']['f6'] }}
			</div>
		</div>
	</div>
	<div id="footer" style="border-top:0px !important;">
		 <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
		 <div class="page-number"></div>
	</div>
	<div class="nest-hide-first-header">
        <div style="width: 49%; display: inline-block; text-align: left !important">
            <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
        </div>
        <div style="width: 50%; display: inline-block;">
            <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
        </div>
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
		</div>
		<div class="nest-pdf-ol-top-left">
			@if (trim($data['fieldcontents']['f1']) != '')
				{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
			@else
				<br /><br />
			@endif
			To: {{ $data['fieldcontents']['f2'] }}<br />
			@if (trim($data['fieldcontents']['f3']) != '')
				Attn: {{ $data['fieldcontents']['f3'] }}<br />
			@else
				<br />
			@endif
			<br />
			By Email<br /><br /><br />
			Dear {{ $data['fieldcontents']['f4'] }},
		</div>
		<div class="nest-pdf-ol-top-subject">
			{{ $data['fieldcontents']['f5'] }}
		</div>
		<div class="nest-pdf-ol-top-premises">
			Re: {{ $data['fieldcontents']['f6'] }}
		</div>
		<div class="nest-pdf-ol-top-intro">
			We write to submit an offer, on behalf of our Client, to lease the above mentioned property on the following terms and conditions:				
		</div>
		@php
			$i = 1;
		@endphp
		
		<table class="nest-pdf-ol-table">
			<tr>
				<td width="26px">
					{{ $i }}.
					@php
						$i++;
					@endphp				
				</td>
				<td width="130px">A) Tenant</td>
				<td width="26px">:</td>
				<td style="padding-bottom:15px;">
					@if (trim($data['fieldcontents']['f7']) != '')
						{{ $data['fieldcontents']['f7'] }}<br />
					@endif
					@if (trim($data['fieldcontents']['f8']) != '')
						{{ $data['fieldcontents']['f8'] }}<br />
					@endif
					@if (trim($data['fieldcontents']['f9']) != '')
						{{ $data['fieldcontents']['f9'] }}<br />
					@endif
					@if (trim($data['fieldcontents']['f10']) != '')
						{{ $data['fieldcontents']['f10'] }}<br />
					@endif
					@if (trim($data['fieldcontents']['f11']) != '')
						{{ $data['fieldcontents']['f11'] }}<br />
					@endif
					@if (trim($data['fieldcontents']['f12']) != '')
						<div style="padding-top:10px;">{{ $data['fieldcontents']['f58'] }} No.: {{ $data['fieldcontents']['f12'] }}</div>
					@endif
					
					@if (trim($data['fieldcontents']['f42']) == 'show')
						<br />
						@if (trim($data['fieldcontents']['f43']) != '')
							{{ $data['fieldcontents']['f43'] }}<br />
						@endif
						@if (trim($data['fieldcontents']['f44']) != '')
							{{ $data['fieldcontents']['f44'] }}<br />
						@endif
						@if (trim($data['fieldcontents']['f45']) != '')
							{{ $data['fieldcontents']['f45'] }}<br />
						@endif
						@if (trim($data['fieldcontents']['f46']) != '')
							{{ $data['fieldcontents']['f46'] }}<br />
						@endif
						@if (trim($data['fieldcontents']['f47']) != '')
							{{ $data['fieldcontents']['f47'] }}<br />
						@endif
						@if (trim($data['fieldcontents']['f48']) != '')
							<div style="padding-top:10px;">{{ $data['fieldcontents']['f59'] }} No.: {{ $data['fieldcontents']['f48'] }}</div>
						@endif
					@endif
				</td>
			</tr>
			@if (isset($data['fieldcontents']['f37']) && $data['fieldcontents']['f37'] == 'show')
				<tr>
					<td></td>
					<td>B) Occupant</td>
					<td>:</td>
					<td>
						{!! nl2br($data['fieldcontents']['f38']) !!}
					</td>
				</tr>
				<tr>
					<td></td>
					<td>C) Landlord</td>
					<td>:</td>
					<td>
						{{ $data['fieldcontents']['f36'] }}
						@if (trim($data['fieldcontents']['f50']) != '')
							<br />{{ $data['fieldcontents']['f50'] }}
						@endif
						@if (trim($data['fieldcontents']['f51']) != '')
							<br />{{ $data['fieldcontents']['f51'] }}
						@endif
						@if (trim($data['fieldcontents']['f52']) != '')
							<br />{{ $data['fieldcontents']['f52'] }}
						@endif
					</td>
				</tr>
			@else
				<tr>
					<td></td>
					<td>B) Landlord</td>
					<td>:</td>
					<td>
						{{ $data['fieldcontents']['f36'] }}
						@if (trim($data['fieldcontents']['f50']) != '')
							<br />{{ $data['fieldcontents']['f50'] }}
						@endif
						@if (trim($data['fieldcontents']['f51']) != '')
							<br />{{ $data['fieldcontents']['f51'] }}
						@endif
						@if (trim($data['fieldcontents']['f52']) != '')
							<br />{{ $data['fieldcontents']['f52'] }}
						@endif
					</td>
				</tr>
			@endif
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Rental</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['rental']) && trim($data['replace']['rental']) != '')
						{!! nl2br($data['replace']['rental']) !!}
					@elseif (is_numeric($data['fieldcontents']['f13']))
						HK${{ number_format($data['fieldcontents']['f13'], 0, '', ',') }}.00 per month, payable monthly in advance, {{ $data['fieldcontents']['f35'] }} of Management Fees, Government Rates and Government Rent.
					@else
						HK$xxxxxx.00 per month, payable monthly in advance, {{ $data['fieldcontents']['f35'] }} of Management Fees, Government Rates and Government Rent.
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@if (!in_array('1_0', $data['sectionshidden']))
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Car Park</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['carpark']) && trim($data['replace']['carpark']) != '')
							{!! nl2br($data['replace']['carpark']) !!}
						@else
							@if ($data['fieldcontents']['f14'] == 0)
								No 
							@elseif ($data['fieldcontents']['f14'] == 1)
								One 
							@elseif ($data['fieldcontents']['f14'] == 2)
								Two 
							@elseif ($data['fieldcontents']['f14'] == 3)
								Three 
							@elseif ($data['fieldcontents']['f14'] == 4)
								Four 
							@elseif ($data['fieldcontents']['f14'] == 5)
								Five 
							@elseif ($data['fieldcontents']['f14'] == 6)
								Six 
							@elseif ($data['fieldcontents']['f14'] == 7)
								Seven 
							@elseif ($data['fieldcontents']['f14'] == 8)
								Eight 
							@elseif ($data['fieldcontents']['f14'] == 9)
								Nine 
							@elseif ($data['fieldcontents']['f14'] == 10)
								ten 
							@endif
							@if ($data['fieldcontents']['f14'] > 1)
								covered car parks 
							@else
								covered car park 
							@endif
							@if ($data['fieldcontents']['f14'] == 0)
								to be provided.
							@else
								to be provided 
								@if ($data['fieldcontents']['f15'] == 0)
									at no additional cost.
								@elseif (is_numeric($data['fieldcontents']['f15']))
									for the account of the Tenant at HK${{ number_format($data['fieldcontents']['f15'], 0, '', ',') }}.00 per month.
								@else
									for the account of the Tenant at HK$xxxxxx.00 per month.
								@endif
							@endif
						@endif
					</td>
				</tr>
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Terms</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['terms']) && trim($data['replace']['terms']) != '')
						{!! nl2br($data['replace']['terms']) !!}
					@elseif (trim($data['fieldcontents']['f16']) != '')
						{{ trim($data['fieldcontents']['f53']) }}, lease commencement from {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f16']) }}.
					@else
						{{ trim($data['fieldcontents']['f53']) }}, lease commencement from XXXXXX.
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@if ($data['fieldcontents']['f17'] != 'Hide')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Management Fees</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['mngfees']) && trim($data['replace']['mngfees']) != '')
							{!! nl2br($data['replace']['mngfees']) !!}
						@elseif (is_numeric($data['fieldcontents']['f18']))
							For the account of the {{ $data['fieldcontents']['f17'] }}, presently set at HK${{ number_format($data['fieldcontents']['f18'], 0, '', ',') }}.00 per month but subject to revision.
						@else
							For the account of the {{ $data['fieldcontents']['f17'] }}, presently set at HK$xxxxxx.00 per month but subject to revision.
						@endif
					</td>
				</tr>
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			@if ($data['fieldcontents']['f19'] != 'Hide')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Government Rates</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['govrates']) && trim($data['replace']['govrates']) != '')
							{!! nl2br($data['replace']['govrates']) !!}
						@elseif (is_numeric($data['fieldcontents']['f20']))
							For the account of the {{ $data['fieldcontents']['f19'] }}, presently set at HK${{ number_format($data['fieldcontents']['f20'], 0, '', ',') }}.00 per month but subject to revision.
						@else
							For the account of the {{ $data['fieldcontents']['f19'] }}, presently set at HK$xxxxxx.00 per month but subject to revision.
						@endif
					</td>
				</tr>
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			@if ($data['fieldcontents']['f21'] != 'Hide')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Government Rent</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['govrent']) && trim($data['replace']['govrent']) != '')
							{!! nl2br($data['replace']['govrent']) !!}
						@elseif (is_numeric($data['fieldcontents']['f22']))
							For the account of the {{ $data['fieldcontents']['f21'] }}, presently set at HK${{ number_format($data['fieldcontents']['f22'], 0, '', ',') }}.00 per month but subject to revision.
						@else
							For the account of the {{ $data['fieldcontents']['f21'] }}, presently set at HK$xxxxxx.00 per month but subject to revision.
						@endif
					</td>
				</tr>
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			@if (!in_array('5_0', $data['sectionshidden']))
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Rent-free Period</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['rentfree']) && trim($data['replace']['rentfree']) != '')
							{!! nl2br($data['replace']['rentfree']) !!}
						@elseif (trim($data['fieldcontents']['f23']) != '' && trim($data['fieldcontents']['f24']) != '')
							The Landlord agrees to grant to the Tenant a rent-free period from {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f23']) }} to {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f24']) }} (both dates inclusive).  
							The Tenant agrees to pay all utility charges during the rent-free period.
						@else
							The Landlord agrees to grant to the Tenant a rent-free period from XXXXXX to XXXXXX (both dates inclusive).  
							The Tenant agrees to pay all utility charges during the rent-free period.
						@endif
					</td>
				</tr>
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			@if (!in_array('2_0', $data['sectionshidden']))
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Early Possession</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['early']) && trim($data['replace']['early']) != '')
							{!! nl2br($data['replace']['early']) !!}
						@elseif (trim($data['fieldcontents']['f23']) != '' && trim($data['fieldcontents']['f24']) != '' && trim($data['fieldcontents']['f25']) != '')
							The premises shall be handed over to the Tenant on {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f25']) }} subject to signing of the Tenancy Agreement.  
							Rental, Management Fees and Government Rates for the period from {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f23']) }} to {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f24']) }} shall be waived.  
							However, the Tenant should take up all tenant’s responsibilities from the date of handover. 
						@else
							The premises shall be handed over to the Tenant on XXXXXX subject to signing of the Tenancy Agreement.  
							Rental, Management Fees and Government Rates for the period from XXXXXX to XXXXXX shall be waived.  
							However, the Tenant should take up all tenant’s responsibilities from the date of handover.
						@endif
					</td>
				</tr>
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Break Clause</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['break']) && trim($data['replace']['break']) != '')
						{!! nl2br($data['replace']['break']) !!}
					@else
						@if (isset($data['fieldcontents']['f49']) && $data['fieldcontents']['f49'] == 'Landlord')
							The Landlord shall have the right to terminate the lease (by giving {{ $data['fieldcontents']['f55'] }} to the Tenant) after a minimum 
							period of {{ $data['fieldcontents']['f26'] }}' occupancy.
						@elseif (isset($data['fieldcontents']['f49']) && $data['fieldcontents']['f49'] == 'Both')
							The Tenant & the Landlord shall have the right to terminate the lease (by giving {{ $data['fieldcontents']['f55'] }} to the Landlord or 
							the Tenant) after a minimum period of {{ $data['fieldcontents']['f26'] }}' occupancy.
						@else
							The Tenant shall have the right to terminate the lease (by giving {{ $data['fieldcontents']['f55'] }} to the Landlord) after a minimum 
							period of {{ $data['fieldcontents']['f26'] }}' occupancy.
						@endif
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Holding Deposit</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['holding']) && trim($data['replace']['holding']) != '')
						{!! nl2br($data['replace']['holding']) !!}
					@elseif (is_numeric($data['fieldcontents']['f13']))
						A deposit of HK${{ number_format($data['fieldcontents']['f13'], 0, '', ',') }}.00, representing one (1) month's rental is payable to the Landlord as a holding deposit immediately upon acceptance of this offer letter. The said deposit shall become the first month's rental upon signing of the formal Tenancy Agreement.
						@if ($data['fieldcontents']['f27'] == 'Yes')
							<br /><br />Should either party fail to sign the Tenancy Agreement, this holding deposit will be refunded to the Tenant in full.
						@else
							<br /><br />
							Should either party fail to sign the Tenancy Agreement (a copy of which is attached herewith) on or before 
							@if (isset($data['fieldcontents']['f56']) && trim($data['fieldcontents']['f56']) != '')
								{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f56']) }},
							@else
								XX/XX/XXXX,
							@endif
							the defaulting party shall be liable to a penalty equivalent to 
							@if (isset($data['fieldcontents']['f56']) && trim($data['fieldcontents']['f57']) != '')
								HK${{ number_format($data['fieldcontents']['f57'], 0, '', ',') }}.00.
							@else
								HK$xxx.00.
							@endif
							In this instance, should the Tenant fail to sign the Tenancy Agreement, 
							then the initial deposit paid herein shall be forfeited or should the Landlord fail to sign the Tenancy Agreement, then they shall 
							at once return the initial deposit and compensate the Tenant with an equivalent amount. 
						@endif
					@else
						A deposit of HK$xxxxxx.00, representing one (1) month's rental is payable to the Landlord as a holding deposit immediately upon acceptance of this offer letter. The said deposit shall become the first month's rental upon signing of the formal Tenancy Agreement.
						@if ($data['fieldcontents']['f27'] == 'Yes')
							<br />Should either party fail to sign the Tenancy Agreement, this holding deposit will be refunded to the Tenant in full.
						@else
							<br /><br />
							Should either party fail to sign the Tenancy Agreement (a copy of which is attached herewith) on or before 
							@if (isset($data['fieldcontents']['f56']) && trim($data['fieldcontents']['f56']) != '')
								{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f56']) }},
							@else
								XX/XX/XXXX,
							@endif
							the defaulting party shall be liable to a penalty equivalent to 
							@if (isset($data['fieldcontents']['f56']) && trim($data['fieldcontents']['f57']) != '')
								HK${{ number_format($data['fieldcontents']['f57'], 0, '', ',') }}.00.
							@else
								HK$xxx.00.
							@endif
							In this instance, should the Tenant fail to sign the Tenancy Agreement, 
							then the initial deposit paid herein shall be forfeited or should the Landlord fail to sign the Tenancy Agreement, then they shall 
							at once return the initial deposit and compensate the Tenant with an equivalent amount. 
						@endif
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Security Deposit</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['security']) && trim($data['replace']['security']) != '')
						{!! nl2br($data['replace']['security']) !!}
					@elseif (is_numeric($data['fieldcontents']['f13']))
						A deposit of HK${{ number_format($data['fieldcontents']['f13']*2, 0, '', ',') }}.00, representing two (2) months’ rental is payable to the Landlord upon the signing of the formal Tenancy Agreement.  This security deposit will be held by the Landlord for the term of the tenancy and shall be returned to the Tenant upon the expiry of term, without interest, and on the terms and conditions as set out in the Tenancy Agreement.
					@else
						A deposit of HK$xxxxxx.00, representing two (2) months’ rental is payable to the Landlord upon the signing of the formal Tenancy Agreement.  This security deposit will be held by the Landlord for the term of the tenancy and shall be returned to the Tenant upon the expiry of term, without interest, and on the terms and conditions as set out in the Tenancy Agreement.
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Stamp Duty</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['stamp']) && trim($data['replace']['stamp']) != '')
						{!! nl2br($data['replace']['stamp']) !!}
					@else
						To be shared equally by both parties.
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			<tr>
				<td>
					{{ $i }}.
					@php
						$i++;
					@endphp
				</td>
				<td>Legal Fees</td>
				<td>:</td>
				<td>
					@if (isset($data['replace']['legal']) && trim($data['replace']['legal']) != '')
						{!! nl2br($data['replace']['legal']) !!}
					@else
						Each party to bear its own legal costs. (if any)
					@endif
				</td>
			</tr>
			<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@if (isset($data['fieldcontents']['f40']) && isset($data['fieldcontents']['f34']) && trim($data['fieldcontents']['f40']) != '' && trim($data['fieldcontents']['f34']) != '')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Other Conditions</td>
					<td>:</td>
					<td>
						Prior to the handover of the premises / commencement of the tenancy, the LANDLORD agrees to carry out the following:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f40']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr>
					<td>
					</td>
					<td></td>
					<td></td>
					<td>
						<br />Prior to the handover of the premises / commencement of the tenancy, the TENANT agrees to carry out the following:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f34']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@elseif (isset($data['fieldcontents']['f40']) && trim($data['fieldcontents']['f40']) != '')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Other Conditions</td>
					<td>:</td>
					<td>
						Prior to the handover of the premises / commencement of the tenancy, the LANDLORD agrees to carry out the following:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f40']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@elseif (isset($data['fieldcontents']['f34']) && trim($data['fieldcontents']['f34']) != '')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Other Conditions</td>
					<td>:</td>
					<td>
						Prior to the handover of the premises / commencement of the tenancy, the TENANT agrees to carry out the following:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f34']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			
			@if (isset($data['fieldcontents']['f39']) && isset($data['fieldcontents']['f41']) && trim($data['fieldcontents']['f39']) != '' && trim($data['fieldcontents']['f41']) != '')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Special Conditions</td>
					<td>:</td>
					<td>
						For the entirety of the lease, the LANDLORD agrees:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f41']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<br />For the entirety of the lease, the TENANT agrees:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f39']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@elseif (isset($data['fieldcontents']['f41']) && trim($data['fieldcontents']['f41']) != '')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Special Conditions</td>
					<td>:</td>
					<td>
						For the entirety of the lease, the LANDLORD agrees:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f41']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@elseif (isset($data['fieldcontents']['f39']) && trim($data['fieldcontents']['f39']) != '')
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Special Conditions</td>
					<td>:</td>
					<td>
						For the entirety of the lease, the TENANT agrees:<br /><br />
					</td>
				</tr>
				@php
					$a = 0;
				@endphp
				@php
					$econd = explode("\n", $data['fieldcontents']['f39']);
				@endphp
				@foreach ($econd as $ec)
					@if (trim($ec) != '')
						<tr>
							<td></td>
							<td></td>
							<td>{{ $abc[$a] }}.</td>
							<td>
								{{ $ec }}
							</td>
							@php
								$a++;
							@endphp
						</tr>
					@endif
				@endforeach
				<tr><td colspan="4" class="nest-pdf-ol-table-gap">&nbsp;</td></tr>
			@endif
			@if (!in_array('4_0', $data['sectionshidden']))
				<tr>
					<td>
						{{ $i }}.
						@php
							$i++;
						@endphp
					</td>
					<td>Bank Consent</td>
					<td>:</td>
					<td>
						@if (isset($data['replace']['bank']) && trim($data['replace']['bank']) != '')
							{!! nl2br($data['replace']['bank']) !!}
						@else
							The Landlord shall provide to the Tenant a copy of the Written Consent for leasing purposes from the existing mortgagee, prior to the signing of the Tenancy Agreement for the leasing of the above property OR sign a Letter of Undertaking.
						@endif
					</td>
				</tr>
			@endif
		</table>
		
		@if ($data['fieldcontents']['f54'] == 'Yes')
			<div style="page-break-after:always;"></div>
			<table><tr><td>
		@else
			<br />
		@endif
			@if (isset($data['replace']['toplast']) && trim($data['replace']['toplast']) != '')
				<div class="nest-pdf-ol-bottom-1">
					{!! nl2br($data['replace']['toplast']) !!}
				</div>
			@else
				<div class="nest-pdf-ol-bottom-1">
					Should you agree to the above terms and conditions, kindly sign, scan and return a copy of this letter to us at 
					{{ $data['fieldcontents']['f28'] }} at your earliest convenience. 
					@if ($data['fieldcontents']['f33'] != 'No Email Sentence')
						{{ $data['fieldcontents']['f33'] }}
					@endif
				</div>
				<div class="nest-pdf-ol-bottom-2">
					Alternatively, please do not hesitate to contact me on {{ $data['fieldcontents']['f29'] }}, should you have any questions regarding the above.						
				</div>
			@endif

			<div class="nest-pdf-ol-faithfully">
				Yours faithfully<br />
				As agent for<br />
				NEST PROPERTY LIMITED
			</div>
			<div class="nest-pdf-ol-signature">
				@if ($consultant->sigpath != '')
					<img src="{{ str_replace('showsig', '../storage/signatures', $consultant->sigpath) }}" />
				@else
					<br /><br /><br />
				@endif
			</div>
			<div class="nest-pdf-ol-consultant">
				{{ $data['fieldcontents']['f30'] }}<br />
				{{ $data['fieldcontents']['f31'] }}<br />
				Residential Search & Investment
			</div>
			<div class="nest-pdf-ol-licensnr">
				Individual Licence No: {{ $data['fieldcontents']['f32'] }}
			</div>
			
			<table class="nest-pdf-ol-table-signatures">
				<tr>
					<td width="46%">
						We hereby confirm and agree to the above terms and conditions
					</td>
					<td width="8%"></td>
					<td width="46%">
						We hereby confirm and agree to the above terms and conditions
					</td>
				</tr>
				<tr>
					<td style="padding-top:80px;border-bottom:1px solid #000000;">
						
					</td>
					<td></td>
					<td style="padding-top:80px;border-bottom:1px solid #000000;">
						
					</td>
				</tr>
				<tr>
					<td valign="top">
						@if (trim($data['fieldcontents']['f42']) == 'show')
							Name: {{ $data['fieldcontents']['f7'] }} & {{ $data['fieldcontents']['f43'] }}<br />
							Date: 
						@else
							Name: {{ $data['fieldcontents']['f7'] }}<br />
							Date: 
						@endif
					</td>
					<td></td>
					<td valign="top">
						Name: {{ $data['fieldcontents']['f36'] }}<br />
						Date: 
					</td>
				</tr>
			</table>
			<div class="nest-pdf-ol-final-legal-note">
				@if (isset($data['replace']['final']) && trim($data['replace']['final']) != '')
					{!! nl2br($data['replace']['final']) !!}
				@else
					SUBJECT TO CONTRACT:  This document is prepared by Nest Property Limited for information only.  Whilst reasonable care has been exercised in preparing this document, it is subject to change and Nest Property Limited makes no representation to its truth, accuracy or completeness, and accordingly cannot be held responsible for any liability whatsoever for any loss howsoever arising from or in reliance upon the whole or any part of the contents of this document.
				@endif
			</div>
		@if ($data['fieldcontents']['f54'] == 'Yes')
			</td></tr></table>
		@endif
	</div>
</body>
</html>

















