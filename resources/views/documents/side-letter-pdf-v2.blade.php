<html>
<head>

<title>Side Letter</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">
<style>
@page {
	margin: 1.2cm 1.5cm 0.5cm 1.5cm;
}

.nest-pdf-sl-bold{
	font-family:MyriadBold;
}
.nest-pdf-sl-top-subject{
	font-family:MyriadBold;
	text-align:right;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises{
	padding-bottom:20px;
	padding-top:20px;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	line-height:1.2 !important;
	font-size:14px;
	text-decoration:underline;
	text-align:justify;
	padding-right:5px;
}
.nest-pdf-sl-top-subject-header{
	font-family:MyriadBold;
	text-align:left;
	text-transform:uppercase;
}
.nest-pdf-sl-top-premises-header{
	padding-top:2px;
	font-family:MyriadBold;
	line-height:1.2;
}
.nest-pdf-sl-introsentence{
	text-align:justify;
}
.nest-pdf-sl-finalsentence{
	text-align:justify;
}
.nest-pdf-sl-content{
	padding-top:17px;
	padding-bottom:17px;
	text-align:justify;
}
.nest-pdf-sl-top-intro{
	padding-bottom:10px;
	text-align:justify;
}

.nest-pdf-sl-bottom-1{
	padding-bottom:0px;
}
.nest-pdf-sl-bottom-2{
	padding-bottom:20px;
}
.nest-pdf-sl-faithfully{
	padding-top:17px;
}

.nest-pdf-sl-landlord{
	padding-top:70px;
}
.nest-pdf-sl-tenant-top{
	float:right;
	width:35%;
	margin-right:20px;
	padding-bottom:70px;
}
.nest-pdf-sl-tenant-bottom{
	float:right;
	width:35%;
	margin-right:20px;	
	border-top:1px solid #000000;
}

#footer{
	height: 10px !important;
	margin-bottom: -3px !important;
	font-family:Myriad;
}

#footer a {
	background-color:#C5C2C0;
}

.nest-hide-first-header{
	margin-top:-95px;
}

.nest-pdf-sl-top-left{
	padding-top:40px;
	padding-bottom:0px;
}
</style>


</head>
<body>
    <div id="footer" style="border-top:0px !important;">
        <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
        <div class="page-number"></div>
    </div>
    <div class="nest-hide-first-header">
        <div style="width: 49%; display: inline-block; text-align: left !important">
            <img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:50px;width:150px;" />
        </div>
        <div style="width: 50%; display: inline-block;">
            <img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" />
        </div>
	</div>
	<div class="nest-pdf-container">
        <div class="nest-pdf-letterhead">
			<div class="nest-pdf-letterhead-text">
				Nest Property Limited<br />
				Suite 1301, Hollywood Centre,<br />
				No.233 Hollywood Road,<br />
				Sheung Wan, Hong Kong<br />
				<br />				 
				Company License No: C-048625<br />
				<br />
				Tel:  	+852 3689 7523<br />
				Fax:  	+852 3568 2976<br />
				Email: 	info@nest-property.com

			</div>
        </div>
        @if ( request()->get('version') == 2 ) 
            <div class="nest-pdf-sl-top-left">
                @if (trim($data['fieldcontents']['f1']) != '')
                    {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br />
                @else
                    xxxxxx<br /><br />
                @endif
                To: {{ $data['fieldcontents']['f2'] }}<br />
                @if (trim($data['fieldcontents']['f3']) != '')
                    Attn: {{ $data['fieldcontents']['f3'] }}<br />
                @else
                    <br /><br />
                @endif
                <br /><br /><br /><br /><br /><br />
                Dear {{ $data['fieldcontents']['f4'] }},
            </div>
        @else
            <div class="nest-pdf-sl-top-left">
                To: {{ $data['fieldcontents']['f2'] }}<br />
                @if (trim($data['fieldcontents']['f3']) != '')
                    Attn: {{ $data['fieldcontents']['f3'] }}<br />
                @else
                    <br /><br />
                @endif
                <br />
                @if (trim($data['fieldcontents']['f1']) != '')
                    {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f1']) }}<br /><br /><br />
                @else
                    xxxxxx<br /><br />
                @endif
                <br />
                Dear {{ $data['fieldcontents']['f4'] }},
            </div>
        @endif
  
		<div class="nest-pdf-sl-top-premises">
			Re: {{ $data['fieldcontents']['f5'] }}
		</div>
		@if (!in_array('1_0', $data['sectionshidden']))
			<div class="nest-pdf-sl-introsentence">
				@if (isset($data['replace']['introsentence']) && trim($data['replace']['introsentence']) != '')
					{!! nl2br($data['replace']['introsentence']) !!}
				@else
					We refer to the tenancy agreement dated {{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f6']) }} made between us as Landlord and your good self as Tenant in respect of the Property (“the Tenancy Agreement”). 
				@endif
			</div>
		@endif
		<div class="nest-pdf-sl-content">
			{!! nl2br($data['fieldcontents']['f7']) !!}
		</div>
		@if (!in_array('2_0', $data['sectionshidden']))
			<div class="nest-pdf-sl-finalsentence">
				@if (isset($data['replace']['finalsentence']) && trim($data['replace']['finalsentence']) != '')
					{!! nl2br($data['replace']['finalsentence']) !!}
				@else
					This Side Letter is supplemental to the Tenancy Agreement. 
					<br /><br />
					Save for the above variation or modification, the terms and provisions of the Tenancy Agreement shall remain unchanged and continue be in full force and effect. 
				@endif
			</div>
		@endif
		
		<div class="nest-pdf-sl-faithfully">
			<br />
			Yours faithfully<br />
		</div>
		<div class="nest-pdf-sl-landlord">
			{{ $data['fieldcontents']['f8'] }}
		</div>
		<div class="nest-pdf-sl-tenant-top">
			Agreed and accepted the terms and 
			conditions set out in this Side Letter:-
		</div>
		<div style="clear:both;"></div>
		<div class="nest-pdf-sl-tenant-bottom">
			{{ $data['fieldcontents']['f9'] }} <br />
			Date:
		</div>
			
	</div>
</body>
</html>
