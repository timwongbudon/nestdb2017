@extends('layouts.app')

@section('content')
<div class="nest-new container">
	<div class="nest-property-edit-wrapper row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="">Documents</h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body" style="padding-bottom:15px;">
					<a href="{{ url('/documents/371/offerletter/16472') }}">Test Offer Letter Edit</a><br />
					<a href="{{ url('/documents/371/tenancyagreement/16472') }}">Test Tenancy Agreement Edit</a><br />
					<a href="{{ url('/documents/371/landlordfeeletter/16472') }}">Test Landlord Fee Letter Edit</a><br />
					<a href="{{ url('/documents/371/letterofundertaking/16472') }}">Test Letter of Undertaking Edit</a><br />
					<a href="{{ url('/documents/371/holdingdepositletter/16472') }}">Test Holding Deposit Letter Edit</a><br />
					<a href="{{ url('/documents/371/utilityaccounts/16472') }}">Test Utility Accounts Edit</a><br />
					<a href="{{ url('/documents/371/handoverreport/16472') }}">Test Handover Report Edit</a><br />
					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
