<html>
<head>

<title>Handover Report</title>

<style>
body{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size:12px;
	line-height:1.4;
}
@page {
	margin: 3.6cm 2cm 1.5cm;
}


.nest-pdf-letterhead{
	text-align:right;
	padding-bottom:10px;
}
.nest-pdf-letterhead img{
	width:100px;
}
.nest-pdf-hr-top{
	padding:0px 0px 0px;
	text-align:center;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	font-size:20px;
	color:#000000;
}
.nest-pdf-hr-handover-accepted{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	color:#cdc6b7;
	padding-bottom:15px;
	padding-left:3px;
	
}
.nest-pdf-hr-basics{
	padding-top:5px;
}
.nest-pdf-first-big .nest-pdf-hr-basics{
	padding-top:15px;
}
.nest-pdf-hr-basics table{
	width:100%;
	border:1px solid #cdc6b7;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-hr-basics-left{
	color:#000000;
	background:#cdc6b7;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	width:25%;
	padding:0px 10px 1px;
	text-align:right;
}
.nest-pdf-first-big .nest-pdf-hr-basics-left{
	padding:2px 10px 3px;
}
.nest-pdf-hr-basics-right{
	border:1px solid #cdc6b7;
	width:75%;
	padding:0px 10px 1px;
}
.nest-pdf-first-big .nest-pdf-hr-basics-right{
	padding:2px 10px 3px;
}
.nest-pdf-hr-handover{
	padding-top:18px;
}
.nest-pdf-hr-handover-utility{
	padding-top:40px;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first {
	padding-top:20px;
}
.nest-pdf-hr-handover table{
	width:100%;
	border:1px solid #cdc6b7;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-hr-handover table.outer-table{
	border:0px solid #cdc6b7;
	
}
.nest-pdf-hr-handover-top{
	color:#000000;
	background:#cdc6b7;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	text-align:left;
	padding:2px 10px 1px;
	font-weight:normal;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first .nest-pdf-hr-handover-top{
	padding:4px 10px 3px;
}
.nest-pdf-hr-handover-left{
	width:20%;
	padding:3px 10px 0px;
	border-right:1px solid #cdc6b7;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	text-align:right;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first .nest-pdf-hr-handover-left{
	padding:6px 10px 1px;
}
.nest-pdf-hr-handover-right{
	border:1px solid #cdc6b7;
	width:60%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#cdc6b7;
	vertical-align:top;
}
.nest-pdf-first-big .nest-pdf-hr-handover-first .nest-pdf-hr-handover-right{
	padding:5px 10px 2px;
}
.nest-pdf-hr-handover-right-third{
	border:1px solid #cdc6b7;
	width:20%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#cdc6b7;
	vertical-align:top;
}
.nest-pdf-hr-row{
	border:1px solid #cdc6b7;
	width:100%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#cdc6b7;
	vertical-align:top;
}
.nest-pdf-hr-handover-utility-left{
	width:25%;
	padding:3px 10px 0px;
	border-right:1px solid #cdc6b7;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	text-align:right;
	color:#cdc6b7;
	vertical-align:top;
}
.nest-pdf-hr-handover-utility-right{
	border:1px solid #cdc6b7;
	width:25%;
	padding:3px 10px 0px;
	font-weight:400;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#cdc6b7;
	vertical-align:top;
	text-align:center;
}


#header,
#footer{
	position: fixed;
	left: 0;
	right: 0;
	color: #000000;
	font-size: 75%;
}
#footer{
	opacity:0.5;
}

#header {
	top: -95px;
	padding-bottom:5px;
	z-index:10;
	text-align:right;
}

#footer {
	bottom: 0;
	border-top: 0.1pt solid #000000;
	padding-top:5px;
}
#header table,
#footer table {
	width: 100%;
	border-collapse: collapse;
	border: none;
}

#footer td {
	padding: 0;
	width: 50%;
}

.page-number {
  text-align: right;
  font-size:75%;
}

.page-number:before {
  content: counter(page);
}

.nest-pdf-hr-check-square{
	float:none;
	display:inline-block;
	height:8px;
	width:8px;
	border:1px solid #cdc6b7;
	margin-top:2px;
	margin-right:8px;
}
.nest-pdf-hr-extra-box{
	color:#cdc6b7;
}
.nest-pdf-hr-handover-signature-bottom{
	padding-top:20px;
}
.nest-pdf-hr-handover-signature-bottom .nest-pdf-hr-extra-box{
	color:#000000;
}
.nest-pdf-hr-handover-signature-bottom .nest-pdf-hr-check-square{
	border:1px solid #000000;
}
.nest-pdf-hr-extra-box .nest-pdf-hr-check-square{
	margin-top:4px;
}
.nest-pdf-dark-text{
	color:#000000;
}

</style>


</head>
<body>
	<div id="header" style="padding-top:0px;">
		<img src="{{ url('/images/tools/nest-letterhead-logo-header.png') }}" style="padding-top:10px;width:80px;" />
	</div>
	@if (isset($data['fieldcontents']['f32']) && $data['fieldcontents']['f32'] == 'small')
		<div class="nest-pdf-container">
	@else
		<div class="nest-pdf-container nest-pdf-first-big">
	@endif
		<div class="nest-pdf-hr-top">
			Lease Details
		</div>
		<div class="nest-pdf-hr-basics">
			<table>
				<tr>
					<td class="nest-pdf-hr-basics-left">Address</td>
					<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f1'] }}</td>
				</tr>
				@if (trim($data['fieldcontents']['f2']) != '')
					<tr>
						<td class="nest-pdf-hr-basics-left">Car Park No.</td>
						<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f2'] }}</td>
					</tr>
				@endif
				@if (trim($data['fieldcontents']['f2']) != '' && trim($data['fieldcontents']['f3']) != '')
					<tr>
						<td class="nest-pdf-hr-basics-left">Vehicle Reg. No.</td>
						<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f3'] }}</td>
					</tr>
				@endif
				<tr>
					<td class="nest-pdf-hr-basics-left">Taken By:</td>
					<td class="nest-pdf-hr-basics-right">{{ $data['fieldcontents']['f4'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-basics-left">Handover Date:</td>
					<td class="nest-pdf-hr-basics-right">
						@if (trim($data['fieldcontents']['f5']) != '')
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f5']) }}
						@endif
					</td>
				</tr>
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Lease Term</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Commencement Date</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f6']) != '')
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f6']) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Expiry Date</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f7']) != '')
							{{ \NestDate::nest_contract_datetime_format($data['fieldcontents']['f7']) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Monthly Rental</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f8']) == '')
							NA
						@else
							@if (is_numeric($data['fieldcontents']['f8']))
								HK${{ number_format($data['fieldcontents']['f8'], 0, '', ',') }}.00
							@else
								NA
							@endif
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Management Fee</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f9']) == '')
							Inclusive
						@else
							@if (is_numeric($data['fieldcontents']['f9']))
								HK${{ number_format($data['fieldcontents']['f9'], 0, '', ',') }}.00
							@else
								Inclusive
							@endif
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Government Rates</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f10']) == '')
							Inclusive
						@else
							@if (is_numeric($data['fieldcontents']['f10']))
								HK${{ number_format($data['fieldcontents']['f10'], 0, '', ',') }}.00
							@else
								Inclusive
							@endif
						@endif
					</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Government Rent</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
						@if (trim($data['fieldcontents']['f11']) == '')
							Inclusive
						@else
							@if (is_numeric($data['fieldcontents']['f11']))
								HK${{ number_format($data['fieldcontents']['f11'], 0, '', ',') }}.00
							@else
								Inclusive
							@endif
						@endif
					</td>
				</tr>
				@if (isset($data['fieldcontents']['f29']) && trim($data['fieldcontents']['f29']) == 'show')
					<tr>
						<td class="nest-pdf-hr-handover-left">Air-Conditioner Maintenance Charges</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">
							@if (trim($data['fieldcontents']['f12']) == '')
								Inclusive
							@else
								@if (is_numeric($data['fieldcontents']['f12']))
									HK${{ number_format($data['fieldcontents']['f12'], 0, '', ',') }}.00
								@else
									Inclusive
								@endif
							@endif
						</td>
					</tr>
				@endif
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Rental Payment</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Name of Bank</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f13'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Bank A/C No.</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f14'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">A/C Name</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f15'] }}</td>
				</tr>
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Tenant</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">
						@if (isset($data['fieldcontents']['f30']) && $data['fieldcontents']['f30'] == 'company')
							Company Name
						@else
							Name
						@endif
					</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f16'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Address</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f17'] }}</td>
				</tr>
				@if (trim($data['fieldcontents']['f18']) != '')
					<tr>
						<td class="nest-pdf-hr-handover-left">Occupant</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f18'] }}</td>
					</tr>
				@endif
				@if (trim($data['fieldcontents']['f21']) != '')
					<tr>
						<td class="nest-pdf-hr-handover-left">Contact Person</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f21'] }}</td>
					</tr>
				@endif
				<tr>
					<td class="nest-pdf-hr-handover-left">Tel</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f19'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Email</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f20'] }}</td>
				</tr>
			</table>
		</div>
		<div class="nest-pdf-hr-handover-first nest-pdf-hr-handover">
			<table>
				<tr>
					<th colspan="4" class="nest-pdf-hr-handover-top">Landlord</th>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">
						@if (isset($data['fieldcontents']['f31']) && $data['fieldcontents']['f31'] == 'company')
							Company Name
						@else
							Name
						@endif
					</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f23'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Address</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f24'] }}</td>
				</tr>
				@if (trim($data['fieldcontents']['f27']) != '')
					<tr>
						<td class="nest-pdf-hr-handover-left">Contact Person</td>
						<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f27'] }}</td>
					</tr>
				@endif
				<tr>
					<td class="nest-pdf-hr-handover-left">Tel</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f25'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-hr-handover-left">Email</td>
					<td class="nest-pdf-dark-text nest-pdf-hr-handover-right" colspan="3">{{ $data['fieldcontents']['f28'] }}</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>

















