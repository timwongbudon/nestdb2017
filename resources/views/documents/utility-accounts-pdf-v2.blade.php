<html>
<head>

<title>Letter of Undertaking</title>
<link href="{{asset('css/pdf-template.css')}}" rel="stylesheet">

@if (request()->get('page_margin') != '')
	@php $pageMargin = str_replace('-', 'cm ', request()->get('page_margin')) @endphp
	<style> @page { margin: {{$pageMargin.'cm'}}; } </style>
@else
	<style> @page {margin: 2.3cm 1.05cm 0.5cm 1.05cm;} </style>
@endif

<style>

body{
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size:10px;
}
.nest-pdf-ua-top{
	padding:8px 0px 8px;
	text-align:center;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	font-size:16px;
	color:#000000;
	background:#C5C2C0;
}
.nest-pdf-ua-basics{
	padding-top:20px;
}
.nest-pdf-ua-basics table{
	width:100%;
	border:0.6px solid #C5C2C0;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-ua-basics-left{
	color:#000000;
	background:#C5C2C0;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	width:25%;
	padding:1px 8px 2px;
}
.nest-pdf-ua-basics-right{
	border:0.6px solid #C5C2C0;
	width:75%;
	padding:1px 8px 1px;
}
.nest-pdf-ua-utility{
	padding-top:20px;
	display: block;
}
.nest-pdf-ua-utility table{
	width:100%;
	border:0.6px solid #C5C2C0;
	border-spacing:0px;
	border-collapse: collapse;
}
.nest-pdf-ua-utility-top{
	color:#000000;
	background:#C5C2C0;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:bold;
	text-align:left;
	padding:4px 8px 4px;
}
.nest-pdf-ua-utility-left{
	width:20%;
	padding:1px 8px 1px;
	border-right:0.6px solid #C5C2C0;
	font-weight:400;
	line-height:1;
	font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
}
.nest-pdf-ua-utility-middle{
	width:20%;
	padding:2px 8px 1px;
	color:#000000;
	vertical-align:top;
}
.nest-pdf-ua-utility-right{
	border:0.6px solid #C5C2C0;
	width:60%;
	padding:1px 8px 1px;
}
#footer{
    height: 10px;
    margin: 0 -10.5cm -8px -10.5cm;
}
#footer a {
	padding-top: 14px;
    font-size: 8px !important;
	background-color: #C5C2C0 !important;
}
.page-number:before {
	font-size: 8px !important;
}
.page-number {
	top: 14px;
}
.nest-pdf-letterhead{
	width:135px;
	line-height:1.3;
}
.nest-hide-first-header{
	margin-top:-138px;
	padding-bottom: 8px; 
}
.nest-pdf-letterhead-text{
	font-size: 8px !important;
}
#header {
	top: -60px;
}
.nest-pdf-container {
	margin: 0 2px;
}

</style>


</head>
<body>
	<div id="header" style="padding-top:0px;padding-left:5px;">
		<div style="text-align:right;">
			<img src="{{ url('/documents/images/nest-letterhead-logo-header.png') }}" style="padding-top:10px;width:80px;" />
		</div>
	</div>
	<div id="footer" style="border-top:0px !important;">
		 <a href="https://nest-property.com/">NEST-PROPERTY.COM</a>
		 <div class="page-number"></div>
	</div>
	<div class="nest-hide-first-header">
		<div style="width: 49%; display: inline-block; text-align: left !important">
			@php $asianWidth = '80'; @endphp
			@if (request()->get('asian_width') != '')
				@php $asianWidth = request()->get('asian_width'); @endphp
			@endif
			<img src="{{ url('/documents/images/asia_pacific_award.png') }}" style="padding-top:38px;width:{{$asianWidth}};" />
		</div>
		<div style="width: 50%; display: inline-block;">
			@php $letterHeadWidth = '85'; @endphp
			@if (request()->get('letter_head_width') != '')
				@php $letterHeadWidth = request()->get('letter_head_width'); @endphp
			@endif
			<img src="{{ url('/documents/images/nest-letterhead-logo.png') }}" style="width:{{$letterHeadWidth}};"/>
		</div>
	</div>
	<div class="nest-pdf-container">
		<div class="nest-pdf-ua-top" style="margin-top: 15px; padding: 10px 10px 9px 10px;">
			Utilities Accounts Summary
		</div>
		<div class="nest-pdf-ua-basics">
			<table>
				<tr>
					<td class="nest-pdf-ua-basics-left">{{ $data['fieldcontents']['f21'] }}</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f1'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">{{ $data['fieldcontents']['f22'] }}</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f2'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">Property Address</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f3'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">Lease Term</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f4'] }}</td>
				</tr>
				<tr>
					<td class="nest-pdf-ua-basics-left">Contact Tel</td>
					<td class="nest-pdf-ua-basics-right">{{ $data['fieldcontents']['f5'] }}</td>
				</tr>
			</table>
		</div>
		@php $displayed = 0; @endphp
		@if (!in_array('1_0', $data['sectionshidden']))
			@php $displayed++; @endphp
			<div class="nest-pdf-ua-utility">
				<table>
					<tr>
						<th colspan="3" class="nest-pdf-ua-utility-top">WATER</th>
					</tr>
					<tr>
						<th class="nest-pdf-ua-utility-left" rowspan="5">
							<img src="{{ url('/images/tools/water.png') }}" style="padding-bottom:10px;width:54px;" /><br />
							Tel: 2824 5000<br />
							Fax: 2802 7333
						</td>
						<td class="nest-pdf-ua-utility-middle">Account Name</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f6'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Billing Address</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f7'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Account No.</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f8'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Effective Date</td>
						<td class="nest-pdf-ua-utility-right">
							@if (strstr($data['fieldcontents']['f9'], '-'))
								{{ \NestDate::nest_date_format($data['fieldcontents']['f9'], "Y-m-d") }}
							@else
								{{ $data['fieldcontents']['f9'] }}
							@endif
						</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Deposit</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f10'] }}</td>
					</tr>
				</table>
			</div>
		@endif
		@if (!in_array('2_0', $data['sectionshidden']))
			@php $displayed++; @endphp
			<div class="nest-pdf-ua-utility">
				<table>
					<tr>
						<th colspan="3" class="nest-pdf-ua-utility-top">ELECTRICITY</th>
					</tr>
					<tr>
						<th class="nest-pdf-ua-utility-left" rowspan="5">
							<img src="{{ url('/images/tools/electricity.png') }}" style="padding-bottom:0px;width:40px;" /><br />
							Tel: 2887 3411<br />
							Fax: 2510 7667
						</td>
						<td class="nest-pdf-ua-utility-middle">Account Name</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f11'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Billing Address</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f12'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Account No.</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f13'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Effective Date</td>
						<td class="nest-pdf-ua-utility-right">
							@if (strstr($data['fieldcontents']['f14'], '-'))
								{{ \NestDate::nest_date_format($data['fieldcontents']['f14'], "Y-m-d") }}
							@else
								{{ $data['fieldcontents']['f14'] }}
							@endif
						</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Deposit</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f15'] }}</td>
					</tr>
				</table>
			</div>
		@endif
		@if (!in_array('3_0', $data['sectionshidden']))
			<div class="nest-pdf-ua-utility">
				<table>
					<tr>
						<th colspan="3" class="nest-pdf-ua-utility-top">GAS</th>
					</tr>
					<tr>
						<th class="nest-pdf-ua-utility-left" rowspan="5">
							<img src="{{ url('/images/tools/towngas.png') }}" style="padding-bottom:0px;width:60px;" /><br />
							Tel: 2880 6988<br />
							Fax: 2590 7886
						</td>
						<td class="nest-pdf-ua-utility-middle">Account Name</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f16'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Billing Address</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f17'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Account No.</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f18'] }}</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Effective Date</td>
						<td class="nest-pdf-ua-utility-right">
							@if (strstr($data['fieldcontents']['f19'], '-'))
								{{ \NestDate::nest_date_format($data['fieldcontents']['f19'], "Y-m-d") }}
							@else
								{{ $data['fieldcontents']['f19'] }}
							@endif
						</td>
					</tr>
					<tr>
						<td class="nest-pdf-ua-utility-middle">Deposit</td>
						<td class="nest-pdf-ua-utility-right">{{ $data['fieldcontents']['f20'] }}</td>
					</tr>
				</table>
			</div>
		@endif
	</div>
</body>
</html>