@extends('layouts.app')

@section('content')
<div class="container" style="min-height: 600px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">What's New (New Feature / Improvement / Fix)</div>
                <div class="panel-body">
                    <ul>
                        <li>Tracking user activities - Shortlist search (2017.03.10)</li>
                        <li>Hot properties (2017.03.08)
                            <ul>
                                <li>Click properties on properties page then click set or unset</li>
                                <li>Add hot properties option filter on property search form</li>
                            </ul>
                        </li>
                        <li>Delete/restore properties (2017.03.08)
                            <ul>
                                <li>Delete button at the bottom of property edit page</li>
                                <li>Restore button in Admin Tools > trash properties search page</li>
                            </ul>
                        </li>
                        <li>Bulk import (2017.03.03)
                            <ul>
                                <li>Import csv/excel file - Admin Tools > Bulk import</li>
                            </ul>
                        </li>
                        <li>Add remove property image features (2017.03.03)</li>
                        <li>Add property display name to simplify prpoperty name for web (2017.03.01)<br/>i.e. from 'Robinson Road 80' to 'Robinson Road' </li>
                        <li>Add districts list, filter, re-order, export code for website to use, for admin only (2017.02.28)</li>
                        <li>Improve property image re-order UI(2017.02.28)</li>
                        <li>Add country list for international, web (2017.02.27)</li>
                        <li>Add more property features for web (2017.02.24)</li>
                        <li>Add another property photo re-order field for web (2017.02.24)</li>
                        <li>Add display building Name - to override very long building name on listing card (2017.02.24)</li>
                        <li>Fix - Building Coordinates (2017.02.24)</li>
                        <li>Add Property Listing Sorting (2017.02.23)</li>
                        <li>Add New Property Field - 'Leased' (2017.02.22)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-warning">
                <div class="panel-heading">Todo</div>
                <div class="panel-body">
                    <strong>For Website</strong>
                    <ul>
                        <li>Fix - Building IDs</li>
                    </ul>
                    <strong>For Database Improvement</strong>
                    <ul>
                        <li>Load time for printing very slow</li>
                        <li>Search properties by vendor name</li>
                        <li>Add user icons / initials for agent to highlight who created shortlists</li>
                        <li>Add option to delete shortlists</li>
                        <li>Image ordering not consistent</li>
                        <li>Currently oliged to add a company name when adding a vendor</li>
                        <li>Add feature so that you can 'Add Vendor' whilst editing a property within the same page</li>
                        <li>Add function to allow you to search for shortlists by name</li>
                        <li>Add quick link to 'Edit Shortlist'</li>
                        <li>Search for client > Add latest shortlist</li>
                        <li>Merge duplicate properties</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
