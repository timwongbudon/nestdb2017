@foreach ($properties as $property)
<?php //dex($property->toArray());?>
@endforeach
<html>
<head>

@if( !empty($viewing['client']) )
<title>{{ $viewing['client']->name() }}'s viewing</title>
@else
<title>Property Listing</title>
@endif
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
<style>


@page { margin: 0px; padding:0px; background-color: green}
body { 
	font-family: 'TrajanPro';
	margin: 0px; 
	/*background-image: url('/images/property-listing.jpg'); // todo fix.*/
	background-repeat: no-repeat;
}

.page-break {
    page-break-after: always;
}

@font-face { 
   font-family: 'Champagne'; 
   src: url('{{asset('fonts/Champagne_Limousines.ttf')}}'); 
} 

@font-face {
	font-family: 'TrajanPro';
	src: url('{{asset('fonts/TrajanPro-Regular.otf')}}');
}

img {
	margin: auto; 
    max-height: 100%;
    max-width: 100%;
}

.image1 {
	position: absolute; 
	top: 13px; 
	left: 13px; 
	overflow: hidden; 
	width: 510px; 
	height: 340px;
}

.image2{
	position: absolute; 
	top: 13px; 
	left: 530px; 
	overflow: hidden; 
	width: 250px; 
	height: 166px;
}

.image3{
	position: absolute; 
	top: 186px; 
	left: 530px; 
	overflow: hidden; 
	width: 250px; 
	height: 166px;
}

div.number {
	position: absolute; 
	top: 483px; 
	width: 35px; 
	height: 35px;
	text-align: center;
}

p.number {
	font-family: 'Champagne';
	font-size: 25px; 
	padding-top: -29px; 
	padding-left: 11px; 
	color: #B39C8C;
}

div.property-name {
	position: absolute; 
	top: 382px; 
	left: 13px; 
	width: 526px; 
	height: 52px;
	font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif; 
	text-transform: uppercase;
}

p.property-name {
	font-size: 27px; 
	color: #fff; 
	padding-top: -24px; 
	padding-left: 15px;
}

div.price {
	position: absolute; 
	top: 366px; 
	left: 548px; 
	width: 92px; 
	height: 74px;
}

div.lease {
	position: absolute; 
	top: 382px; 
	left: 649px; 
	width: 132px; 
	height: 52px;
	font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif; 
	text-transform: uppercase;
}

p.size, p.price {
	font-family: 'Champagne';
}

p.price {
	font-size: 27px; 
	text-align: center; 
	line-height: 0.1; 
	color: #B39C8C;
}

</style>


<style>
/*new css*/
	.boxsection {
		position: absolute; 
		top: 376px;
		height: 54px;
		background-color: #cfc2b2;
	}
	.boxsection-street {
		left: 13px;
		width: 525px;
	}
	.boxsection-price {
		top: 365px;
		left: 549px;
		width: 92px;
		height: 76px;
		background-color: #ece5df;
	}
	.boxsection-type {
		left: 649px;
		width: 130px;
	}
	.icontext {
		text-align: center; 
		font-family: 'Champagne'; 
		position: absolute; 
		top: 522px; 
		width: 96px; 
		color: #B39C8C;
		background-color: #ece5df;
	}
	.iconbox {
		position: absolute;
		top: 452px;
		width: 96px;
		height: 96px;
		background-color: #ece5df;
	}
	.iconimg {
		position: absolute;
		top: 0px;
		left: 0px;
		width: 96px;
		height: 96px;
	}
	.numbox-outer {
		position: absolute;
		top: 28px;
		left: 76px;
		width: 40px;
		height: 40px;
		background-color: #fff;
		border-radius: 20px;
	}
	.numbox-inner {
		position: relative;
		top: 3px;
		left: 3px;
		width: 34px;
		height: 34px;
		background-color: #ece5df;
		border-radius: 17px;
	}
	.icontext-bedrooms {
		left: 13px;
	}
	.icontext-bathrooms {
		left: 133px;
	}
	.icontext-maidroom {
		left: 252px;
	}
	.icontext-outdoor {
		left: 371px;
	}
	.icontext-carpark {
		left: 490px;
	}
	.price-exclusive{
		position:absolute;
		top:59px;
		right:5px;
		font-size:10px;
		color: #B39C8C;
		font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif; 
	}
</style>

</head>
	<body>
@foreach ($properties as $index => $property)
@if($index!=0)<div class="page-break"></div>@endif

<?php $print_type = 'lease';?>
@if(!empty($viewing) && $property->type_id==2)
	<?php $print_type = 'sale';?>
@elseif(!empty($viewing) && $property->type_id!=3)
	<?php $option = $viewing->getOption($property->id); ?>
	<?php $print_type = $option->print_type();?>
@elseif(!empty($viewing))
	<?php $option = $viewing->getOption($property->id); ?>
	<?php $print_type = $option->print_type();?>
@endif
	<!-- 794x560 -->
		<div style="padding: 13px;">
			
			<div class="boxsection boxsection-street"></div>
			<div class="boxsection boxsection-price"></div>
			<div class="boxsection boxsection-type"></div>

			<div class="iconbox icontext-bedrooms">
				<div class="numbox-outer"><div class="numbox-inner"></div></div>
				<div class="iconimg">
					<img src="{{ asset('system-images/icons/icon-bedrooms.png')}}" alt="" width="96px">
				</div>
			</div>
			<div class="iconbox icontext-bathrooms">
				<div class="numbox-outer"><div class="numbox-inner"></div></div>
				<div class="iconimg">
					<img src="{{ asset('system-images/icons/icon-bathrooms.png')}}" alt="" width="96px">
				</div>
			</div>
			<div class="iconbox icontext-maidroom">
				<div class="numbox-outer"><div class="numbox-inner"></div></div>
				<div class="iconimg">
					<img src="{{ asset('system-images/icons/icon-maidroom.png')}}" alt="" width="96px">
				</div>
			</div>
			<div class="iconbox icontext-outdoor">
				<div class="numbox-outer"><div class="numbox-inner"></div></div>
				<div class="iconimg">
					<img src="{{ asset('system-images/icons/icon-outdoor.png')}}" alt="" width="96px">
				</div>
			</div>
			<div class="iconbox icontext-carpark">
				<div class="iconimg">
					<img src="{{ asset('system-images/icons/icon-carpark.png')}}" alt="" width="96px">
				</div>
				<div class="numbox-outer"><div class="numbox-inner"></div></div>
			</div>

@if(!$property->is_old_property())
			<div class="image1">
				<img src="{{ \NestImage::fixOldUrl($property->first_print_image())}}" width="510">
			</div>

			<div class="image2">
				<img src="{{ \NestImage::fixOldUrl($property->second_print_image()) }}" width="250">
			</div>

			<div class="image3">
				<img src="{{ \NestImage::fixOldUrl($property->third_print_image()) }}" width="250">
			</div>
@else
			<!-- the order here used to be third_print_image / first_print_image / second_print_image which caused some problems down the road when changing old imported properties, 
			no idea why that was implemented like this -->
			<div class="image1">
				<img src="{{ \NestImage::fixOldUrl($property->first_print_image())}}" width="510">
			</div>

			<div class="image2">
				<img src="{{ \NestImage::fixOldUrl($property->second_print_image()) }}" width="250">
			</div>

			<div class="image3">
				<img src="{{ \NestImage::fixOldUrl($property->third_print_image()) }}" width="250">
			</div>
@endif
			<div class="property-name">
				<p class="property-name">{{ $property->shorten_building_name() }}</p>
			</div>
			<div class="price">
				@if ($property->inclusive == 0 && !(strtolower($print_type) == 'sale'))
					<div class="price-exclusive">EX</div>
				@endif
				<p class="price">HK$</p>
@if( strtolower($print_type) == 'sale')
				<p class="price">{{ $property->shorten_asking_sale() }}</p>
@else
				<p class="price">{{ $property->shorten_asking_rent() }}</p>
@endif
			</div>
			<div class="lease">
			<p style="font-size: 20px; text-align: center; color: #fff; padding-top: -12px; text-transform: uppercase;">FOR {{ $print_type }}</p>
			</div>
			<div class="number" style="left: 85px;">
				<p class="number">{{ $property->bedroom_count() }}</p>
			</div>
			<div class="number" style="left: 205px;">
				<p class="number">{{ $property->bathroom_count() }}</p>
			</div>
			<div class="number" style="left: 325px;">
				<p class="number">{{ $property->maidroom_count() }}</p>
			</div>
			<div class="number" style="left: 445px;">
				<p class="number">{{ $property->outdoor_count() }}</p>
			</div>			
			<div class="number" style="left: 563px;">
				<p class="number">{{ $property->carpark_count() }}</p>
			</div>	
			<div style="position: absolute; top: 454px; right: 15px; width: 150px; height: 50px;">
				<p class="size" style="font-size: 22px; padding-top: -31px; text-align: right; color: #B39C8C;">{{ $property->property_grosssize() }} sq. ft.</p>
				<p class="size" style="font-size: 17px; padding-top: -31px; text-align: right; color: #B39C8C;">({{ $property->property_saleablesize() }} net)</p>
			</div>	
			<div style="position: absolute; top: 540px; right: 15px; width: 200px; height: 50px;">
				<p class="size" style="font-size: 15px; padding-top: -45px; text-align: right; color: #B39C8C;line-height:1;">
					{{ $property->print_address() }}<br />
					{{ $property->district() }}
				</p>
			</div>

			<div class="icontext icontext-bedrooms">
				Bedrooms
			</div>
			<div class="icontext icontext-bathrooms">
				Bathrooms
			</div>
			<div class="icontext icontext-maidroom">
				Maid's Room
			</div>
			<div class="icontext icontext-outdoor">
				Outdoor
			</div>
			<div class="icontext icontext-carpark">
				Car Park
			</div>
		</div>
@endforeach
	</body>
</html>