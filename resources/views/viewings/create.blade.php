@extends('layouts.app')

@section('content')
<div class="nest-new container">
	<div class="nest-property-edit-wrapper row">
		<div class=" col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="">New Viewing</h4>
				</div>
			</div>

			
			<form action="{{ url('/viewing/store') }}" id="addViewing" method="POST">


			<!--form -->
			<div class="panel panel-default">
					<div class="panel-body" style="padding-bottom:15px;padding-left:7.5px;padding-right:7.5px;">

						

							<div class="col-md-6">

								{{ csrf_field() }}
								
								<input type="hidden" name="shortlist_id" value="{{ $shortlist->id }}" />
								<input type="hidden" name="client_id" value="{{ $shortlist->client->id }}" />
								<input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
								<input type="hidden" name="status" value="0" />
								
								<div class="nest-property-edit-row">
									<div class="col-sm-4 col-xs-6">
										<div class="nest-property-edit-label">Date</div>
									</div>
									<div class="col-sm-4 col-xs-6">
										{{ Form::date('v_date', old('v_date'), ['class'=>'form-control', 'id'=>'viewingdate']) }}
									</div>
									<div class="clearfix"></div>
								</div>
								<!-- <div class="nest-property-edit-row">
									<div class="col-sm-4 col-xs-6">
										<div class="nest-property-edit-label">Time</div>
									</div>
									<div class="col-sm-2 col-xs-2">
										{{ Form::number('hour', old('hour'), ['class'=>'form-control', 'placeholder'=>'Hour']) }}
									</div>
									<div class="col-sm-2 col-xs-2">
										{{ Form::number('minute', old('minute'), ['class'=>'form-control', 'placeholder'=>'Minute']) }}
									</div>
									<div class="col-sm-2 col-xs-2" style="padding-top:5px;">
										<label for="viewing-ampm-0" class="control-label">{!! Form::radio('ampm', 0, false, ['id'=>'viewing-ampm-0']) !!} am</label> &nbsp;
										<label for="viewing-ampm-1" class="control-label">{!! Form::radio('ampm', 1, true, ['id'=>'viewing-ampm-1']) !!} pm</label>
									</div>
									<div class="clearfix"></div>
								</div> -->
								<div class="nest-property-edit-row">
									<div class="col-sm-4 col-xs-6">
										<div class="nest-property-edit-label">Time</div>
									</div>
									<div class="col-sm-6 col-xs-6" style="padding: 0">
										<div class="col-sm-4 col-xs-4">
											<select name="timefrom" id="timefrom" class="calendar-top-time form-control">
												@foreach ($times as $t)
													<option value="{{ $t }}">{{ $t }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-sm-6 col-xs-6">
											<select name="timeto" id="timeto"  class="calendar-top-time form-control">
												@foreach ($times as $t)
													<option value="{{ $t }}">{{ $t }}</option>
												@endforeach
											</select>
										</div>
										<div class="clearfix"></div>
										<span style="padding: 5px; display: block;">Note: Maximum allowed time per property: 60 minutes on the 1st property and 35 mins for every following properties.</span>
									</div>
									<div class="clearfix"></div>
									
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Driver</div>
									</div>
									<div class="col-xs-4">
										<label for="viewing-driver-0" class="control-label ">{!! Form::radio('driver', 0, true, ['id'=>'viewing-driver-0','class'=>'driveropt']) !!} No</label>
									</div>
									<div class="col-xs-4">
										<label for="viewing-driver-1" class="control-label">{!! Form::radio('driver', 1, false, ['id'=>'viewing-driver-10','class'=>'driveropt']) !!} Yes</label>
									</div>

									<div class="clearfix"></div>
									
								</div>
								<div class="nest-property-edit-row" id="useCardHolder">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Select Car</div>
								</div>
								<div class="col-sm-8 col-xs-6">
										<select name="use_car" class="calendar-top-time form-control" disabled>
											<option value=""></option>
											<option value="1">Alphard</option>
											<option value="2">Audi A8</option>
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<b>Usage of Car that Day</b>
									</div>
									<div class="col-sm-8 col-xs-6" id="usageofcar">
										Please enter a date first
									</div>
									<div class="clearfix"></div>
								</div>

							</div>

							<div class="col-md-6">

								<div class="nest-property-edit-row">
									<div class="col-xs-4">
										<div class="nest-property-edit-label">Comments</div>
									</div>
									<div class="col-xs-8">
										{!! Form::textarea('comments', $shortlist->comments, ['class'=>'form-control', 'placeholder' => '', 'rows' => '4']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-xs-12" style="padding-top:15px;">
									<button type="submit" class="nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-plus"></i>Create </button>
								</div>

							</div>

							<div class="clearfix"></div>
							
							
						
					</div>
				</div>
				<!--end form -->

	

@if (count($properties) > 0)
					@php
						$i = 0;
					@endphp
					
				
					<div class="nest-property-list-wrapper">
						
						
						<div class="row">
							<div style="padding-left: 10px; font-weight: bold; font-size: 16px"> Select property to view</div>
							
								{{ csrf_field() }}
								
								@php
									$curdistrict = -1;
									$viewingcnt = 1;
									$shown = array();
								@endphp
								
								@foreach ($vps as $vp)
									<div class="clearfix"></div>
									<h3 class="nest-property-shortlist-district-h3">Viewing {{ $viewingcnt }}</h3>
								
									@php
										$i = 0;
									@endphp
									
									@foreach ($properties as $property)
										@if (isset($vp[$property->id]))
											<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 nest-property-list-item">
												<div class="panel panel-default">
													<div class="nest-property-list-item-top">
														<div class="nest-property-list-item-top-checkbox">
															@if (!isset($shown[$property->id]))
																{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
															@else
																<input type="checkbox" disabled />
															@endif
														</div>
														<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
															{{ $property->shorten_building_name(0) }}
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="nest-property-list-item-image-wrapper">
														<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
															@if(!empty($property->featured_photo_url))
																<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
															@else
																<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
															@endif
															
															<div class="nest-property-list-item-price-wrapper">
																<div class="nest-property-list-item-price">
																	{!! $property->price_db_searchresults_new() !!}
																</div>
															</div>
														</a>
														<div class="nest-property-list-item-image-top-bar-left">
															@if (count($property->nest_photos) > 0)
																<div class="nest-property-list-item-nestpics-wrapper">
																	<div class="nest-property-list-item-nestpics">
																		<img src="/images/camera2.png" />
																	</div>
																</div>
															@endif
														</div>
														<div class="nest-property-list-item-image-top-bar">
															@if($property->bonuscomm == 1)
																<div class="nest-property-list-item-bonus-wrapper">
																	<div class="nest-property-list-item-bonus">
																		$$$
																	</div>
																</div>
															@endif
															@if($property->hot == 1)
																<div class="nest-property-list-item-hot-wrapper">
																	<div class="nest-property-list-item-hot">
																		HOT
																	</div>
																</div>
															@endif
															@if($property->web)
																<div class="nest-property-list-item-web-wrapper">
																	<div class="nest-property-list-item-web">
																		<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																			WEB
																		</a>
																	</div>
																</div>
															@endif
															@if ($property->owner_id > 0)
																@if ($property->getOwner() && $property->getOwner()->count() > 0)
																	@foreach ($property->getOwner() as $owner)
																		@if ($owner->corporatelandlord == 1)
																			<div class="nest-property-list-item-cp-wrapper">
																				<div class="nest-property-list-item-cp">
																					<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																						CL
																					</a>
																				</div>
																			</div>
																		@endif
																	@endforeach
																@endif
															@elseif ($property->poc_id == 3 || $property->poc_id == 4)
																@if ($property->getAgency() && $property->getAgency()->count() > 0)
																	@foreach ($property->getAgency() as $agent)
																		@if ($agent->corporatelandlord == 1)
																			<div class="nest-property-list-item-cp-wrapper">
																				<div class="nest-property-list-item-cp">
																					<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																						CL
																					</a>
																				</div>
																			</div>
																		@endif
																	@endforeach
																@endif
															@endif
														</div>
													</div>
													<div class="nest-property-list-item-bottom-address-wrapper">
														<div class="nest-property-list-item-bottom-address">
															{{ $property->fix_address1() }}
														</div>
														<div class="nest-property-list-item-bottom-unit">
															{{ $property->unit }}
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="nest-property-list-item-bottom-rooms-wrapper">
														<div class="nest-property-list-item-bottom-rooms-left">
															Room(s)
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@if ($property->bedroom_count() == 1)
																{{ $property->bedroom_count() }} bed, 
															@else
																{{ $property->bedroom_count() }} beds, 
															@endif
															@if ($property->bathroom_count() == 1)
																{{ $property->bathroom_count() }} bath
															@else
																{{ $property->bathroom_count() }} baths
															@endif
															
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="nest-property-list-item-bottom-rooms-wrapper">
														<div class="nest-property-list-item-bottom-rooms-left">
															Size
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															{{ $property->property_netsize() }} / {{ $property->property_size() }}
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="nest-property-list-item-bottom-rooms-wrapper">
														<div class="nest-property-list-item-bottom-rooms-left">
															@if ($property->poc_id ==  1)
																Landlord
															@elseif ($property->poc_id ==  2)
																Rep
															@elseif ($property->poc_id ==  3)
																Agency
															@else
																Contact
															@endif
														</div>
														<div class="nest-property-list-item-bottom-rooms-right">
															@php
																$vendorshown = false;
															@endphp
															@if ($property->poc_id ==  1)
																@if ($property->getOwner() && $property->getOwner()->count() > 0)
																	@foreach ($property->getOwner() as $owner)
																		{!! $owner->basic_info_property_search_linked() !!}
																	@endforeach		
																	@php
																		$vendorshown = true;
																	@endphp
																@endif
															@elseif ($property->poc_id ==  2)
																@if ($property->getRep() && $property->getRep()->count() > 0)
																	@foreach ($property->getRep() as $rep)
																		{!! $rep->basic_info_property_search_linked() !!}
																	@endforeach
																	@php
																		$vendorshown = true;
																	@endphp
																@endif
															@elseif ($property->poc_id ==  3)
																@if ($property->getAgency() && $property->getAgency()->count() > 0)
																	@foreach ($property->getAgency() as $agent)
																		{!! $agent->basic_info_property_search_linked() !!}
																	@endforeach
																	@php
																		$vendorshown = true;
																	@endphp
																@endif
															@endif
															@if (!$vendorshown)
																@if ($property->getOwner() && $property->getOwner()->count() > 0)
																	@foreach ($property->getOwner() as $owner)
																		{!! $owner->basic_info_property_search_linked() !!}
																	@endforeach														
																	@if ($property->getRep() && $property->getRep()->count() > 0)
																		@foreach ($property->getRep() as $rep)
																			{!! $rep->basic_info_property_search_linked() !!}
																		@endforeach
																	@endif
																@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
																	@foreach ($property->getAgency() as $agent)
																		{!! $agent->basic_info_property_search_linked() !!}
																	@endforeach
																@else
																	-
																@endif
															@endif
														</div>
														<div class="clearfix"></div>
														@if ($property->poc_id == 1 && $vendorshown)
															@if ($property->getRep() && $property->getRep()->count() > 0)
																<div class="nest-property-list-item-bottom-rooms-left">
																	Rep
																</div>
																<div class="nest-property-list-item-bottom-rooms-right">
																	@foreach ($property->getRep() as $rep)
																		{!! $rep->basic_info_property_search_linked() !!}
																	@endforeach
																</div>
															@endif
														@endif
														@if ($property->poc_id == 2 && $vendorshown)
															@if ($property->getOwner() && $property->getOwner()->count() > 0)
																<div class="nest-property-list-item-bottom-rooms-left">
																	Owner
																</div>
																<div class="nest-property-list-item-bottom-rooms-right">
																	@foreach ($property->getOwner() as $owner)
																		{!! $owner->basic_info_property_search_linked() !!}
																	@endforeach
																</div>
															@endif
														@endif
														@if ($property->poc_id == 3 && $vendorshown)
															@if ($property->getAgent() && $property->getAgent()->count() > 0)
																<div class="nest-property-list-item-bottom-rooms-left">
																	Agent
																</div>
																<div class="nest-property-list-item-bottom-rooms-right">
																	@foreach ($property->getAgent() as $agent)
																		{!! $agent->basic_info_property_search_linked() !!}
																	@endforeach
																</div>
															@endif	
														@endif
														<div class="clearfix"></div>
													</div>
													<div class="nest-property-list-item-bottom-features-wrapper">
														<div class="col-xs-4">
															<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
															{{ $property->maidroom_count() }}
														</div>
														<div class="col-xs-4">
															<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
															{{ $property->backend_search_outdoor() }}
														</div>
														<div class="col-xs-4">
															<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
															{{ $property->carpark_count() }}
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="nest-property-list-item-bottom-buttons-wrapper">
														<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
															<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
																<i class="fa fa-btn fa-pencil-square-o"></i>Edit
															</a>
														</div>
														
														@if($property->type_id == 3)
															<div class="col-xs-6" style="padding-left:5px;padding-right:0px;padding-top:0px;">
																<?php $option = $property->shortlist_option->where('shortlist_id', $shortlist->id)->where('property_id', $property->id)->first();?>
																<input type="hidden" name="property_id" value="{{ $property->id }}">
																<button id="shortlist-option-{{ $shortlist->id }}" class="nest-button btn btn-default shortlist-option-button" style="width:100%;border:0px;">
																	<i class="fa fa-btn fa-toggle-on"></i>{{ $option->type() }}
																</button>
															</div>
														@else
															<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
																<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')" style="width:100%;border:0px;">
																	<i class="fa fa-btn fa-dot-circle-o"></i>Show
																</a>
															</div>
														@endif
														
														<div class="clearfix"></div>
														<div class="col-xs-12" style="padding-left:0px;padding-top:10px;padding-right:0px;">
															<a class="nest-button btn btn-default" href="{{ url('/documents/'.$shortlist->id.'/docaddproperty/'.$property->id) }}" style="width:100%;border:0px;">
																<i class="fa fa-btn fa-book"></i>Create / Edit Documents
															</a>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
											@php
												$i++;
												if ($i%3 == 0){
													echo '<div class="hidden-xs hidden-sm hidden-lg clearfix"></div>';
												}
												if ($i%2 == 0){
													echo '<div class="hidden-lg hidden-md clearfix"></div>';
												}
												if ($i%4 == 0){
													echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
												}
												$shown[$property->id] = 1;
											@endphp
										@endif
									@endforeach
									
									@php
										$viewingcnt++;
									@endphp
									
								@endforeach
								
								<div class="clearfix"></div>
								<h3 class="nest-property-shortlist-district-h3">Not Viewed Yet</h3>
							
								@php
									$i = 0;
								@endphp
								
								@foreach ($properties as $property)
									@if (!isset($shown[$property->id]))
										<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 nest-property-list-item">
											<div class="panel panel-default">
												<div class="nest-property-list-item-top">
													<div class="nest-property-list-item-top-checkbox">
														{!! Form::checkbox('options_ids[]', $property->id, null, ['class'=>'property-option', 'id'=>'property-option_id-checkbox-' . $property->id]) !!}
													</div>
													<div class="nest-property-list-item-top-building-name" title="{{ $property->shorten_building_name(0) }}" alt="{{ $property->shorten_building_name(0) }}">
														{{ $property->shorten_building_name(0) }}
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="nest-property-list-item-image-wrapper">
													<a href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')">
														@if(!empty($property->featured_photo_url))
															<img src="{{ \NestImage::fixOldUrl($property->featured_photo_url) }}?t={{ $property->pictst }}" alt="" width="100%" />
														@else
															<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
														@endif
														
														<div class="nest-property-list-item-price-wrapper">
															<div class="nest-property-list-item-price">
																{!! $property->price_db_searchresults_new() !!}
															</div>
														</div>
													</a>
													<div class="nest-property-list-item-image-top-bar-left">
														@if (count($property->nest_photos) > 0)
															<div class="nest-property-list-item-nestpics-wrapper">
																<div class="nest-property-list-item-nestpics">
																	<img src="/images/camera2.png" />
																</div>
															</div>
														@endif
													</div>
													<div class="nest-property-list-item-image-top-bar">
														@if($property->bonuscomm == 1)
															<div class="nest-property-list-item-bonus-wrapper">
																<div class="nest-property-list-item-bonus">
																	$$$
																</div>
															</div>
														@endif
														@if($property->hot == 1)
															<div class="nest-property-list-item-hot-wrapper">
																<div class="nest-property-list-item-hot">
																	HOT
																</div>
															</div>
														@endif
														@if($property->web)
															<div class="nest-property-list-item-web-wrapper">
																<div class="nest-property-list-item-web">
																	<a href="http://nest-property.com/{{ $property->get_property_api_path() }}" target="_blank">
																		WEB
																	</a>
																</div>
															</div>
														@endif
														@if ($property->owner_id > 0)
															@if ($property->getOwner() && $property->getOwner()->count() > 0)
																@foreach ($property->getOwner() as $owner)
																	@if ($owner->corporatelandlord == 1)
																		<div class="nest-property-list-item-cp-wrapper">
																			<div class="nest-property-list-item-cp">
																				<a href="/clpropertiesowner/{{ $owner->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																					CL
																				</a>
																			</div>
																		</div>
																	@endif
																@endforeach
															@endif
														@elseif ($property->poc_id == 3 || $property->poc_id == 4)
															@if ($property->getAgency() && $property->getAgency()->count() > 0)
																@foreach ($property->getAgency() as $agent)
																	@if ($agent->corporatelandlord == 1)
																		<div class="nest-property-list-item-cp-wrapper">
																			<div class="nest-property-list-item-cp">
																				<a href="/clpropertiesagency/{{ $agent->id }}/{{ $property->building_id }}/" target="_blank" title="Corporate Landlord">
																					CL
																				</a>
																			</div>
																		</div>
																	@endif
																@endforeach
															@endif
														@endif
													</div>
												</div>
												<div class="nest-property-list-item-bottom-address-wrapper">
													<div class="nest-property-list-item-bottom-address">
														{{ $property->fix_address1() }}<!-- <br />{{ $property->district() }} --> 
													</div>
													<div class="nest-property-list-item-bottom-unit">
														{{ $property->unit }}
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="nest-property-list-item-bottom-rooms-wrapper">
													<div class="nest-property-list-item-bottom-rooms-left">
														Room(s)
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@if ($property->bedroom_count() == 1)
															{{ $property->bedroom_count() }} bed, 
														@else
															{{ $property->bedroom_count() }} beds, 
														@endif
														@if ($property->bathroom_count() == 1)
															{{ $property->bathroom_count() }} bath
														@else
															{{ $property->bathroom_count() }} baths
														@endif
														
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="nest-property-list-item-bottom-rooms-wrapper">
													<div class="nest-property-list-item-bottom-rooms-left">
														Size
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														{{ $property->property_netsize() }} / {{ $property->property_size() }}
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="nest-property-list-item-bottom-rooms-wrapper">
													<div class="nest-property-list-item-bottom-rooms-left">
														@if ($property->poc_id ==  1)
															Landlord
														@elseif ($property->poc_id ==  2)
															Rep
														@elseif ($property->poc_id ==  3)
															Agency
														@else
															Contact
														@endif
													</div>
													<div class="nest-property-list-item-bottom-rooms-right">
														@php
															$vendorshown = false;
														@endphp
														@if ($property->poc_id ==  1)
															@if ($property->getOwner() && $property->getOwner()->count() > 0)
																@foreach ($property->getOwner() as $owner)
																	{!! $owner->basic_info_property_search_linked() !!}
																@endforeach		
																@php
																	$vendorshown = true;
																@endphp
															@endif
														@elseif ($property->poc_id ==  2)
															@if ($property->getRep() && $property->getRep()->count() > 0)
																@foreach ($property->getRep() as $rep)
																	{!! $rep->basic_info_property_search_linked() !!}
																@endforeach
																@php
																	$vendorshown = true;
																@endphp
															@endif
														@elseif ($property->poc_id ==  3)
															@if ($property->getAgency() && $property->getAgency()->count() > 0)
																@foreach ($property->getAgency() as $agent)
																	{!! $agent->basic_info_property_search_linked() !!}
																@endforeach
																@php
																	$vendorshown = true;
																@endphp
															@endif
														@endif
														@if (!$vendorshown)
															@if ($property->getOwner() && $property->getOwner()->count() > 0)
																@foreach ($property->getOwner() as $owner)
																	{!! $owner->basic_info_property_search_linked() !!}
																@endforeach														
																@if ($property->getRep() && $property->getRep()->count() > 0)
																	@foreach ($property->getRep() as $rep)
																		{!! $rep->basic_info_property_search_linked() !!}
																	@endforeach
																@endif
															@elseif ($property->getAgency() && $property->getAgency()->count() > 0)
																@foreach ($property->getAgency() as $agent)
																	{!! $agent->basic_info_property_search_linked() !!}
																@endforeach
															@else
																-
															@endif
														@endif
													</div>
													<div class="clearfix"></div>
													@if ($property->poc_id == 1 && $vendorshown)
														@if ($property->getRep() && $property->getRep()->count() > 0)
															<div class="nest-property-list-item-bottom-rooms-left">
																Rep
															</div>
															<div class="nest-property-list-item-bottom-rooms-right">
																@foreach ($property->getRep() as $rep)
																	{!! $rep->basic_info_property_search_linked() !!}
																@endforeach
															</div>
														@endif
													@endif
													@if ($property->poc_id == 2 && $vendorshown)
														@if ($property->getOwner() && $property->getOwner()->count() > 0)
															<div class="nest-property-list-item-bottom-rooms-left">
																Owner
															</div>
															<div class="nest-property-list-item-bottom-rooms-right">
																@foreach ($property->getOwner() as $owner)
																	{!! $owner->basic_info_property_search_linked() !!}
																@endforeach
															</div>
														@endif
													@endif
													@if ($property->poc_id == 3 && $vendorshown)
														@if ($property->getAgent() && $property->getAgent()->count() > 0)
															<div class="nest-property-list-item-bottom-rooms-left">
																Agent
															</div>
															<div class="nest-property-list-item-bottom-rooms-right">
																@foreach ($property->getAgent() as $agent)
																	{!! $agent->basic_info_property_search_linked() !!}
																@endforeach
															</div>
														@endif	
													@endif
													<div class="clearfix"></div>
												</div>
												<div class="nest-property-list-item-bottom-features-wrapper">
													<div class="col-xs-4">
														<img src="{{ url('/images/tools/icon-maid.jpg') }}" alt="" /><br />
														{{ $property->maidroom_count() }}
													</div>
													<div class="col-xs-4">
														<img src="{{ url('/images/tools/icon-outdoor.jpg') }}" alt="" /><br />
														{{ $property->backend_search_outdoor() }}
													</div>
													<div class="col-xs-4">
														<img src="{{ url('/images/tools/icon-car.jpg') }}" alt="" /><br />
														{{ $property->carpark_count() }}
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="nest-property-list-item-bottom-buttons-wrapper">
													<div class="col-xs-6" style="padding-left:0px;padding-right:5px;padding-top:0px;">
														<a class="nest-button btn btn-default" href="{{ url('property/edit/'.$property->id) }}" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-pencil-square-o"></i>Edit
														</a>
													</div>
													
													@if($property->type_id == 3)
														<div class="col-xs-6" style="padding-left:5px;padding-right:0px;padding-top:0px;">
															<?php $option = $property->shortlist_option->where('shortlist_id', $shortlist->id)->where('property_id', $property->id)->first();?>
															<input type="hidden" name="property_id" value="{{ $property->id }}">
															<button id="shortlist-option-{{ $shortlist->id }}" class="nest-button btn btn-default shortlist-option-button" style="width:100%;border:0px;">
																<i class="fa fa-btn fa-toggle-on"></i>{{ $option->type() }}
															</button>
														</div>
													@else
														<div class="col-xs-6" style="padding-right:0px;padding-left:5px;padding-top:0px;">
															<a class="nest-button btn btn-default" href="javascript:;" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $property->id }}, '{{ str_replace("'", "\'", $property->name) }}', '')" style="width:100%;border:0px;">
																<i class="fa fa-btn fa-dot-circle-o"></i>Show
															</a>
														</div>
													@endif
													
													<div class="clearfix"></div>
													<div class="col-xs-12" style="padding-left:0px;padding-top:10px;padding-right:0px;">
														<a class="nest-button btn btn-default" href="{{ url('/documents/'.$shortlist->id.'/docaddproperty/'.$property->id) }}" style="width:100%;border:0px;">
															<i class="fa fa-btn fa-book"></i>Create / Edit Documents
														</a>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
										@php
											$i++;
											if ($i%3 == 0){
												echo '<div class="hidden-xs hidden-sm hidden-lg clearfix"></div>';
											}
											if ($i%2 == 0){
												echo '<div class="hidden-lg hidden-md clearfix"></div>';
											}
											if ($i%4 == 0){
												echo '<div class="hidden-sm hidden-md hidden-xs clearfix"></div>';
											}
											$shown[$property->id] = 1;
										@endphp
									@endif
								@endforeach
									
								
								{{ Form::hidden('default_type', '') }}
								{{ Form::hidden('action', 'select') }}
								{{ Form::hidden('shortlist_id', $shortlist->id) }}
						
						</div>
					</div>
					
					
				@endif


				

				</form>

			</div>
	
	</div>
</div>
	

				<script>
					function checkCarDate(){
						var d = jQuery('#viewingdate').val();
						
						jQuery('#usageofcar').html('...');
						
						jQuery.get("/calendar/carbooking/"+d+"/0", function(data) {
							jQuery('#usageofcar').html(data);
						});

						
					}

					async function checkDriverBooking(){
						var d = jQuery('#viewingdate').val();

						$timefrom = $('#timefrom').val();
						$timeto = $('#timeto').val();
						$car = jQuery('select[name="use_car"]').val();

						//jQuery.get("/calendar/carbooking_check/"+d+"/0/"+$timefrom+"/"+$timeto+"/"+$car, function(data) {
							
						//	if(data == "true"){								
								//alert("Car not available");	
						//		return true;
						//	} else {
						//		return false;
						//	}
							
						///});*/

						result = await $.ajax({
							url: "/calendar/carbooking_check/"+d+"/0/"+$timefrom+"/"+$timeto+"/"+$car,
							dataType: 'json',
							type: 'GET',
							contentType: 'application/json',
							//success: callback
						});
						return result;
						
					}

					async function checkViewDuration(){
						var d = jQuery('#viewingdate').val();

						$timefrom = $('#timefrom').val();
						$timeto = $('#timeto').val();

						var numOfProp = $('.property-option:checked').length;
						
						result = await $.ajax({
							url: "/calendar/viewing_duration_check/"+d+"/"+numOfProp+"/"+$timefrom+"/"+$timeto,
							dataType: 'json',
							type: 'GET',
							contentType: 'application/json',
							//success: callback2
						});

						return result;
					
					}

					jQuery('input[name="driver"]').on('change', function(){
						// console.log();
						if (jQuery(this).val() == 1) {
							jQuery('select[name="use_car"]').attr('disabled', false);
						} else {
							jQuery('select[name="use_car"]').val('');
							jQuery('select[name="use_car"]').attr('disabled', true);
						}
					});
					
					jQuery('#viewingdate').on('blur', function(){
						checkCarDate();
					});

					jQuery('#addViewing').on('submit', function(e){
						
						
						var numOfProp = $('.property-option:checked').length;

						if( $('#viewingdate').val() == '' ){
							alert("Please Add date");
							return false;
						}
						
						var error = false;
						
						
						if($('.driveropt:checked').val() == 1){
							e.preventDefault();


							
							$car = jQuery('select[name="use_car"]').val();
							if( $car == "" ){
								e.preventDefault();
								alert("Please select a car");
								error = true;							
								return false;
							}

							if( numOfProp == 0){
								e.preventDefault();
								alert("Please select property to view");
								error = true;							
								return false;
							}

							checkDriverBooking().then( (result) => {
								
								
								if(result[0]==true){
									e.preventDefault();
									alert("The car is not available");
									error = true;
									return false;
								} else {
									checkViewDuration().then((result2) => {
										if(result2[0] !== 0){
											alert(result2);
											error = true;	
											return false;
										} else {
											if(	error == false	){
												
												$(this).unbind('submit').submit()
											}
										}
									})
								}
							} )


							/*checkDriverBooking(result => {
								
								if(result[0]==true){
									e.preventDefault();
									alert("Car not available");
									error = true;
									return false;
								}
							})

							checkViewDuration(result2 => {			
								if(result2[0] !== 0){
									e.preventDefault();
									alert(result2);
									error = true;
									return false;
								}			
							
							})*/

						
							
							
							/*var timefrom = $('#timefrom').val().split(":");
							var timefromHour = timefrom[0];
							var timefrom2 = timefrom[1].split(" ");
							var timefromMin = timefrom2[0];
							var timefromMeridian = timefrom2[1];

							var timeto = $('#timeto').val().split(":");
							var timetoHour = timeto[0];
							var timeto2 = timeto[1].split(" ");
							var timetoMin = timeto2[0];
							var timetoMeridian = timeto2[1];

							var viewingdate = $('#viewingdate').val().split("-");;

							if(numOfProp > 1 ) {
								var allowedTotalMin = ((numOfProp - 1) * 35 ) + 60;
							} else {
								var allowedTotalMin = 60;
							}
											

							var add_minutes =  function (dt, minutes) {
								var newDate = new Date(dt.getTime() + minutes*60000).toString();
								var addedTime = newDate.split(" ");
								return addedTime = addedTime[4];							
							}
							var upToTime = add_minutes(new Date(viewingdate[0],viewingdate[1],viewingdate[2], timefromHour,timefromMin), allowedTotalMin);
							var upToTimeSplit = upToTime.split(":");

							var upToTimeHr = upToTimeSplit[0];
							var upToTimeMin = upToTimeSplit[1];

							

							
							

							if(timefromMeridian == 'am' && timetoMeridian == 'pm'){
								if(timetoHour != parseInt(12) ){
									timetoHour = parseInt(timetoHour) + 12;
									console.log(timetoHour);
								}
							}

							//alert(upToTimeHr+"-"+timetoHour);

							if(parseInt(upToTimeHr) < timetoHour){
								alert("Usage of time is over the allowed maximum time per property viewing. Select up to "+upToTimeHr+":"+upToTimeMin + " only." );
								//console.log("Up to "+upToTimeHr+":"+upToTimeMin);
								//return false;
							} else {
								if(parseInt(upToTimeMin) < timetoMin){
									alert("Usage of time is over the allowed maximum time per property viewing. Select up to "+upToTimeHr+":"+upToTimeMin + " only." );
									//console.log("Up to "+upToTimeHr+":"+upToTimeMin);
								//	return false;
								}
							}*/

							
							
							
							

							
						
						}

						
					

						
						
					});
					
				</script>
						
						
@endsection
