@extends('layouts.app')

@section('content')
    
            <!-- Current Properties -->
            <div class="panel panel-default">
<form action="{{ url('viewing/option/reorder') }}" method="POST" class="form-horizontal">{{ csrf_field() }}
                    <div class="panel-body">
    <ul id="sortable-1" class="container sortable-listing" style="">
    @if (count($properties) > 0)
    @foreach ($properties as $property)
        <li id="{{ $property->id }}" class="default col-sm-3" style="padding:15px !important;background:#ffffff;">
            @if(!empty($property->featured_photo_url))
				<img src="{{ $property->featured_photo_url }}" alt="" width="100%" />
			@else
				<img src="{{ url('/images/tools/dummy-featured-small.jpg') }}" alt="" width="100%" />
            @endif
            {{ Form::hidden('options_ids[]', $property->id) }}
			<div style="font-size:16px;font-family:'Gill Sans', Calibri;padding:10px 0px;text-transform:uppercase;">
				{{ $property->name }} (ID {{$property->id}})
			</div>
			<?php /*
            <span>
                <strong>{{ $property->name }} {{$property->id}}</strong><br/>
                                            <?php $rep_ids = $property->rep_ids?explode(',',$property->rep_ids):array(); ?>
                                            {{ $property->fix_address1() }}<br/><hr style="margin: 5px 0;">

                                            @if($property->unit)
                                                Unit: {{ $property->unit }}<br/>
                                            @endif
                                            
                                            Asking Price: {{ $property->price() }}<br/>

                                            @if($property->bedroom_count())
                                                Bedrooms: {{ $property->bedroom_count() }}<br/>
                                            @endif

                                            @if($property->bathroom_count())
                                                Bathrooms: {{ $property->bathroom_count() }}<br/>
                                            @endif

                                            @if($property->show_size())
                                                Size: {{ $property->show_size() }}<br/>
                                            @endif
                                            
                                            @if($property->size)
                                                Size: {{ $property->property_size() }}<br/>
                                            @endif

                                            @if($property->poc_id)
                                                Point of contact: {{ $property->poc() }}<br/>
                                            @endif
                                            
                                            @if(!empty($property['building']->tel))
                                                Mgmt Office tel: {{$property['building']->tel}}<br/>
                                            @endif
                                            
                                            @if(!empty($property['owner']))
                                                Owner: {{$property['owner']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['owner']) && !empty($property['reps']))
                                                @foreach($property['reps'] as $i => $rep)
                                                Rep {{$i+1}}: {{$rep->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property['agent']))
                                                Agency: {{$property['agent']->basic_info()}}<br/>
                                            @endif

                                            @if(!empty($property['agent']) && !empty($property['agents']))
                                                @foreach($property['agents'] as $i => $agent)
                                                Agent {{$i+1}}: {{$agent->basic_info()}}<br/>
                                                @endforeach
                                            @endif

                                            @if(!empty($property->comments) && $property->comments != '[]')
                                                Last Comment: {{$property->show_last_comment()}}
                                            @endif

                                            @if(!empty($property['agent']->contact_info))
                                                Contact Info: {{$property['agent']->contact_info}}<br/>
                                            @endif

                                            @if(!empty($property->available_date()))
                                            Available Date: {{ $property->available_date() }}
                                            @endif

            </span> */ ?>
        </li>
    @endforeach
    @endif
    </ul>
                    </div>
            </div>


            <div class="panel panel-default nest-property-list-buttons">
                <div class="panel-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-offset-6 col-sm-3 col-xs-6">
								<a href="{{ url('/viewing/show/'.$viewing->id) }}" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-arrow-left"></i>Back
								</a>
                            </div>
							<div class="col-sm-3 col-xs-6">
								{{ Form::hidden('viewing_id', $viewing->id) }}
								<button id="reorder_option" class="nest-button btn btn-default">
									<i class="fa fa-btn fa-arrows"></i>Re-order
								</button>
							</div>
                        </div>
					</div>
                </div>
            </div>
</form>
@endsection

@section('footer-script')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
#sortable-1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 100%;
}

#sortable-1 li {
    padding: 0.4em;
    padding-left: 1.5em;
    font-size: 17px;
    height: auto;
    min-height: 280px;
    cursor: move;
}

.default {
    background: #f5f5f5;
    border: 1px solid #DDDDDD;
    color: #333333;
}
</style>
<script>
$(document).ready(function(){
    $( "#sortable-1" ).sortable();
});
$(document).ready(function(){
    $("#reorder_option").click(function(){
        event.preventDefault();
        $('form').submit();
    });
    $("#backto_option").click(function(){
        event.preventDefault();
        location.href = "{{ url( 'viewing/show/' . $viewing->id) }}";
    });
});

</script>
@endsection