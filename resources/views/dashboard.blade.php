@extends('layouts.app')

@section('content')
<div class="nest-new dashboard">
    <div class="row">
				
			<div class="row" data-toggle="isotope">
				<div class="item col-md-4 col-xs-12 nest-dashboard-button-wrapper">
					<div class="panel panel-default" style="background:url('{{ url('/images/dashboard/dashboard01.jpg') }}');">
						<a href="{{ url('/list') }}">
							<div class="nest-dashboard-button"><strong>Property search</strong></div>
						</a> 
					</div>
				</div>
				<div class="item col-md-4 col-xs-12 nest-dashboard-button-wrapper">
					<div class="panel panel-default" style="background:url('{{ url('/images/dashboard/dashboard02.jpg') }}');">
						<a href="{{ url('/clients') }}">
							<div class="nest-dashboard-button"><strong>Clients</strong></div>
						</a>
					</div>
				</div>
				<div class="item col-md-4 col-xs-12 nest-dashboard-button-wrapper">
					<div class="panel panel-default" style="background:url('{{ url('/images/dashboard/dashboard03.jpg') }}');">
						<a href="{{ url('/shortlists') }}">
							<div class="nest-dashboard-button"><strong>Shortlists</strong></div>
						</a>
					</div>
				</div>
			</div>



            <!-- <h2 class="page-section-heading">Tables &amp; lists</h2> -->

            <div class="row prototype-dummy-appointment" data-toggle="isotope">
              <!-- block 1 -->
              <div class="item col-md-4 col-xs-12">
                <div class="panel panel-default">
                  <div class="gdl-header-wrapper">
                      <h3 class="gdl-header-title">
                          Hot Properties
                      </h3>
                      <div class="gdl-header-gimmick-wrapper">
                          <div class="gdl-header-right">
                              <a class="view-all-projects" href="{{ url('/hotproperties') }}">
                                  View all hot properties
                              </a>
                          </div>
                          <div class="gdl-header-gimmick">
                          </div>
                      </div>
                  </div>
                  <table class="table table-striped margin-none">
                    <thead>
                      <tr>
                        <th class="text-left width-200">Property</th>
                        <th class="text-left">Date</th>
                        <th class="text-left">Type</th>
                        <th class="text-left">Price</th>
                      </tr>
                    </thead>
                    <tbody>
						@foreach ($hotprops as $prop)
							<tr>
								<td>
									<a href="javascript:;" class="text-primary" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $prop->id }}, '{{ str_replace("'", "\'", $prop->name) }}', '')">{{ $prop->shorten_building_name(0) }}</a>
								</td>
								<td class="text-left">
									{{ $prop->available_date() }}
								</td>
								<td class="text-left">
									{{ $prop->type() }}
								</td> 
								<td class="text-left">
									{{ $prop->price_db_searchresults() }}
								</td>
							</tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>

              <!-- block 1 -->
              <div class="item col-md-4 col-xs-12">
                <div class="panel panel-default">
                  <div class="gdl-header-wrapper">
                      <h3 class="gdl-header-title">
                          Latest Properties
                      </h3>
                      <div class="gdl-header-gimmick-wrapper">
                          <div class="gdl-header-right">
                              <a class="view-all-projects" href="{{ url('/list') }}">
                                  View all properties
                              </a>
                          </div>
                          <div class="gdl-header-gimmick">
                          </div>
                      </div>
                  </div>
                  <table class="table table-striped margin-none">
                    <thead>
                      <tr>
                        <th class="text-left width-200">Property</th>
                        <th class="text-left">Date</th>
                        <th class="text-left">Type</th>
                        <th class="text-left">Price</th>
                      </tr>
                    </thead>
                    <tbody>
						@foreach ($availableprops as $prop)
							<tr>
								<td>
									<a href="javascript:;" class="text-primary" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $prop->id }}, '{{ str_replace("'", "\'", $prop->name) }}', '')">{{ $prop->shorten_building_name(0) }}</a>
								</td>
								<td class="text-left">
									{{ $prop->available_date() }}
								</td>
								<td class="text-left">
									{{ $prop->type() }}
								</td> 
								<td class="text-left">
									{{ $prop->price_db_searchresults() }}
								</td>
							</tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>
			  
              <div class="item col-md-4 col-xs-12">
                <div class="panel panel-default">
                  <div class="gdl-header-wrapper">
                      <h3 class="gdl-header-title">
                          Leases Coming Up
                      </h3>
                      <div class="gdl-header-gimmick-wrapper">
                          <div class="gdl-header-right">
                              <a class="view-all-projects" href="{{ url('/upcomingleases') }}">
                                  View all upcoming leases
                              </a>
                          </div>
                          <div class="gdl-header-gimmick">
                          </div>
                      </div>
                  </div>
                  <table class="table table-striped margin-none">
                    <thead>
                      <tr>
                        <th class="text-left">Property</th>
                        <th class="text-left">Date</th>
                        <th class="text-left">Owner</th>
                      </tr>
                    </thead>
                    <tbody>
						@foreach ($leasedprops as $prop)
							<tr>
								<td>
									<a href="javascript:;" class="text-primary" data-toggle="modal" data-target="#propertyModal" onClick="showproperty({{ $prop->id }}, '{{ str_replace("'", "\'", $prop->name) }}', '')">{{ $prop->shorten_building_name(0) }}</a>
								</td>
								<td class="text-left">
									{{ $prop->available_date() }}
								</td>
								<td class="text-left">
									@if(!empty($prop['owner']))
										{{$prop['owner']->basic_info()}}<br/>
									@endif
								</td> 
							</tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>

				
				
				
				
				
				
				
				
				
				
				
				
				
				
    </div>
</div>
@endsection
