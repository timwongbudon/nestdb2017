{{-- plupload control script--}}

@if (isset($inc_lib))
<!-- <script type="text/javascript" src="http://www.plupload.com/plupload/js/plupload.full.min.js" charset="UTF-8"></script> -->
<script type="text/javascript" src="{{ url('/js/plupload.full.min.js') }}" charset="UTF-8"></script>
@endif
<script type="text/javascript">

var {{ $id }}_uploader = new plupload.Uploader({
	runtimes : 'html5,flash,silverlight,html4',
     
    browse_button : '{{ $id }}_pickfiles', // you can pass in id...
    container: document.getElementById('{{ $id }}_container'), // ... or DOM Element itself
    

    url : "{{ $url }}",
     
    filters : {
        max_file_size : '20mb',
        mime_types: [
@if (in_array('image', $types))
			{title : "Image files", extensions : "jpg,gif,png,jpeg"},
@endif
@if (in_array('pdf', $types))
			{title : "PDF files", extensions : "pdf"},
@endif
@if (in_array('zip', $types))
			{title : "Zip files", extensions : "zip"}
@endif
        ]
    },
 
    // Flash settings
    flash_swf_url : '/plupload/js/Moxie.swf',
 
    // Silverlight settings
    silverlight_xap_url : '/plupload/js/Moxie.xap',
     
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },

@if (isset($vars) && !empty($vars))
    multipart_params: {
@foreach ($vars as $key => $value)
        "{{ $key }}" : "{{ $value }}"@if ($value !== end($vars)),@endif
@endforeach
    },
@endif
 
    init: {
        PostInit: function() {
            document.getElementById('{{ $id }}_filelist').innerHTML = '';
            document.getElementById('{{ $id }}_uploadfiles').onclick = function() {
                {{ $id }}_uploader.start();
                return false;
            };
            {{ $id }}_updater();
        },
 
        FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
                document.getElementById('{{ $id }}_filelist').innerHTML += '<div id="{{ $id }}_' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            });
        },
 
        UploadProgress: function(up, file) {
			document.getElementById('{{ $id }}_console').style.display = 'block';
            document.getElementById('{{ $id }}_'+file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
        },
 
        Error: function(up, err) {
			document.getElementById('{{ $id }}_console').style.display = 'block';
            document.getElementById('{{ $id }}_console').innerHTML += "Error #" + err.code + ": " + err.message + "\n";
        },

        UploadComplete: function(up, files) {
			document.getElementById('{{ $id }}_console').style.display = 'block';
            document.getElementById('{{ $id }}_console').innerHTML += 'upload completed.' + "\n";
            {{ $id }}_updater();
        },

    }
});

function {{ $id }}_updater() {
    if("{{$vars['group']}}" != 'nest-doc') {
		$.get("{{ url('/propertyimages/'.$vars['id'].'/'.$vars['group'].'/'.$vars['type'].'/') }}", function(data) {
			$("#{{ $id }}_files").html('');
			$.each(data, function(index, file){
				img = $('<image>').attr('src', file.url).attr('width', '200').attr('style', 'margin-bottom: 10px');
				$("#{{ $id }}_files").append( img.prop('outerHTML') );
			});
		});
	}else{
		$.get("{{ url('/property/'.$vars['id'].'/'.$vars['group'].'/'.$vars['type'].'/') }}", function(data) {
			$("#{{ $id }}_files").html('');
			$.each(data, function(index, file){
				alink = $('<a target="_blank">').attr('href', file.url).html(file.filename);
				$("#{{ $id }}_files").append( alink.prop('outerHTML') );
			});
		});
	}
}
 
{{ $id }}_uploader.init();

</script>