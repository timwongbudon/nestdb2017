<div class="modal fade bs-example-modal-lg" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="propertyModalTitle">
	<div class="modal-dialog modal-lg nest-property-list-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="propertyModalTitle">Modal title</h4>
			</div>
			<div id="show-property-modal-pics" class="nest-property-show-picture-wrapper modal-body">
				<div class="col-xs-12">
					<div id="show-pics-slider-wrapper">
						<div id="propertyModalPics"></div>
						<!-- <div style="text-align:center;"><a id="downloadpics" href="javascript:;" onClick="downloadPics();">Download Pictures</a></div> -->
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="show-property-modal-body" class="modal-body">
				<div class="col-md-3 col-sm-6">
					<h5>Details</h5>
					<div id="propertyModalContent"></div>
				</div>
				<div class="col-md-3 col-sm-6">
					<h5>Information</h5>
					<div id="propertyModalInformation"></div>
				</div>
				<div class="hidden-md hidden-lg clearfix"></div>
				<div class="col-md-3 col-sm-6">
					<h5>Contact</h5>
					<div id="propertyModalContact"></div>
					<h5>Interested Clients</h5>
					<div id="propertyModalClients"></div>
				</div>
				<div class="col-md-3 col-sm-6">
					<h5>Comments</h5>
					<div id="propertyModalComments"></div>
					<br />
					<h5>Leases & Sales</h5>
					<div id="propertyModalLeaseSales"></div>
					<br />
					<h5>Change Log</h5>
					<div id="propertyModalChanges"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-primary" style="border:0px;" data-dismiss="modal">Close</a>
				<a id="propertyModalEdit" href="" class="btn btn-primary" class="btn btn-primary" style="border:0px;">Edit</a>
				<a id="show-pics-slider-big-image" href="javascript:;" onclick="hidebigimage();" class="btn btn-primary" style="display:none;border:0px;">Close Picture Slider</a>
			</div>
		</div>
	</div>
</div>

<script>
	var curpropid = 0;
	
	function showproperty(pid, name, searchterm){
		curpropid = pid;
		
		jQuery('#propertyModalTitle').html(name+' (ID: <a href="/listsearch?s='+pid+'" style="color:#212121;">'+pid+'</a>)');
		jQuery('#propertyModalEdit').attr('href', "{{ url('property/edit/') }}/"+pid+"?searchterm="+searchterm);
		
		var loading = '<div class="nest-property-list-dialog-loading"><img src="{{ url("images/tools/loader3.gif") }}" /></div>';
		
		jQuery('#propertyModalPics').html(loading);
		jQuery('#show-property-modal-pics').css('display', 'block');
		jQuery('#propertyModalContent').html(loading);
		jQuery('#propertyModalInformation').html(loading);
		jQuery('#propertyModalContact').html(loading);
		jQuery('#propertyModalComments').html(loading);
		jQuery('#propertyModalClients').html(loading);
		jQuery('#propertyModalChanges').html(loading);
		jQuery('#propertyModalLeaseSales').html(loading);
		
		
		loadComments(pid);
		loadChanges(pid);
		loadContent(pid);
		loadInformation(pid);
		loadClients(pid);
		loadContact(pid);
		loadPics(pid);
		loadLeaseSales(pid);
	}
	
	function loadLeaseSales(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/property/leasesales/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalLeaseSales').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/property/leasesales/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalLeaseSales').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/property/leasesales/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalLeaseSales').html(data);
							},
							error: function (data) {
								jQuery('#propertyModalLeaseSales').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function loadComments(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/comments/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalComments').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/comments/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalComments').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/comments/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalComments').html(data);
							},
							error: function (data) {
								console.log(data);
								jQuery('#propertyModalComments').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function loadChanges(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/property/changes/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalChanges').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/property/changes/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalChanges').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/property/changes/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalChanges').html(data);
							},
							error: function (data) {
								jQuery('#propertyModalChanges').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function loadPics(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			}
		});
		
		var width = jQuery('#propertyModal').width();
		var bwidth = jQuery(document).width();
		if (bwidth < 768){
			width = bwidth - 100;
		}else{
			width = bwidth - 200;
		}
		jQuery('#show-pics-slider-wrapper').css('max-width', width);
		
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/showpics/") }}/'+pid,
			cache: false,
			success: function (data) {
				if (data == 0){
					jQuery('#show-property-modal-pics').css('display', 'none');
					jQuery('#downloadpics').css('display', 'none');
				}else{
					jQuery('#downloadpics').css('display', 'inline-block');
					jQuery('#propertyModalPics').html(data);
					
					jQuery('#show-pics-slider').slick({
						dots: false,
						infinite: false,
						speed: 300,
						slidesToShow: 1,
						centerMode: false,
						variableWidth: true
					});
					$('#show-pics-slider').get(0).slick.setPosition();
					
					jQuery('#show-pics-slider-big').slick({
						dots: true,
						infinite: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						centerMode: true,
						variableWidth: true
					});
				}
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/showpics/") }}/'+pid,
					cache: false,
					success: function (data) {
						if (data == 0){
							jQuery('#show-property-modal-pics').css('display', 'none');
							jQuery('#downloadpics').css('display', 'none');
						}else{
							jQuery('#downloadpics').css('display', 'inline-block');
							jQuery('#propertyModalPics').html(data);
							
							jQuery('#show-pics-slider').slick({
								dots: false,
								infinite: false,
								speed: 300,
								slidesToShow: 1,
								centerMode: false,
								variableWidth: true
							});
							$('#show-pics-slider').get(0).slick.setPosition();
							
							jQuery('#show-pics-slider-big').slick({
								dots: true,
								infinite: true,
								slidesToShow: 1,
								slidesToScroll: 1,
								centerMode: true,
								variableWidth: true
							});
						}
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/showpics/") }}/'+pid,
							cache: false,
							success: function (data) {
								if (data == 0){
									jQuery('#show-property-modal-pics').css('display', 'none');
									jQuery('#downloadpics').css('display', 'none');
								}else{
									jQuery('#downloadpics').css('display', 'inline-block');
									jQuery('#propertyModalPics').html(data);
									
									jQuery('#show-pics-slider').slick({
										dots: false,
										infinite: false,
										speed: 300,
										slidesToShow: 1,
										centerMode: false,
										variableWidth: true
									});
									$('#show-pics-slider').get(0).slick.setPosition();
									
									jQuery('#show-pics-slider-big').slick({
										dots: true,
										infinite: true,
										slidesToShow: 1,
										slidesToScroll: 1,
										centerMode: true,
										variableWidth: true
									});
								}
							},
							error: function (data) {
								console.log(data);
								jQuery('#propertyModalPics').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
		
	function loadClients(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/showpropertyclients/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalClients').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/showpropertyclients/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalClients').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/showpropertyclients/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalClients').html(data);
							},
							error: function (data) {
								console.log(data);
								jQuery('#propertyModalClients').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function loadContent(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/showcontent/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalContent').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/showcontent/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalContent').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/showcontent/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalContent').html(data);
							},
							error: function (data) {
								console.log(data);
								jQuery('#propertyModalContent').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function loadInformation(pid){
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/showinformation/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalInformation').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/showinformation/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalInformation').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/showinformation/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalInformation').html(data);
							},
							error: function (data) {
								console.log(data);
								jQuery('#propertyModalInformation').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function loadContact(pid){
		hidebigimage();
		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.ajax({
			type: 'GET',
			url: '{{ url("/showcontact/") }}/'+pid,
			cache: false,
			success: function (data) {
				jQuery('#propertyModalContact').html(data);
			},
			error: function (data) {
				jQuery.ajax({
					type: 'GET',
					url: '{{ url("/showcontact/") }}/'+pid,
					cache: false,
					success: function (data) {
						jQuery('#propertyModalContact').html(data);
					},
					error: function (data) {
						jQuery.ajax({
							type: 'GET',
							url: '{{ url("/showcontact/") }}/'+pid,
							cache: false,
							success: function (data) {
								jQuery('#propertyModalContact').html(data);
							},
							error: function (data) {
								console.log(data);
								jQuery('#propertyModalContact').html('An error occurred '+data);
							}
						});
					}
				});
			}
		});
	}
	
	function showbigimage(id){
		jQuery('#show-pics-slider-big').css('display', 'block');
		jQuery('#show-pics-slider-big-image').css('display', 'inline-block');
		jQuery('.nest-property-pic-slider-bigimg-wrapper').slick('slickGoTo', id);
		jQuery('#show-pics-slider').css('display', 'none');
		jQuery('#show-property-modal-body').css('display', 'none');
	}
	
	function hidebigimage(){
		jQuery('#show-pics-slider-big').css('display', 'none');
		jQuery('#show-pics-slider-big-image').css('display', 'none');
		jQuery('#show-pics-slider').css('display', 'block');
		jQuery('#show-property-modal-body').css('display', 'block');
	}
	
	
</script>