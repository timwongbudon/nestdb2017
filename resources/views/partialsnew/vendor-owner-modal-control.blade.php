<script type="text/javascript">
var owner_input_field = "property-owner";
var owner_input_value = "";
var owner_modal_id = "property-vendor-owner-modal";
var options = {
    url: function(phrase) {return "{{ url('/vendors/api/owner/index?keywords=') }}" + phrase;}, 
    getValue: function(element) {return element.company + ', ' + element.firstname + ' ' + element.lastname;},
    template: {
        type: "custom",
        method: function(value, item) {
            if (item.id!='') {
                return item.company + ' ' + item.firstname + ' ' + item.lastname;
            } else {
                return item.name;
            }
        }
    },
    list: {
        maxNumberOfElements: 12,
        onShowListEvent: function() {
            owner_input_value = $("#"+owner_input_field).val(); 
        },
        onChooseEvent: function(element) {
            var action = $("#"+owner_input_field).getSelectedItemData().action;
            switch(action) {
                case "create-new-item":
                    $("#"+owner_modal_id).modal();
                    break;
                case "manage-item":
                    $("#"+owner_input_field).val(owner_input_value);
                    var win = window.open('{{ url('/vendors/') }}', '_vendor');
                    break;
                default:
                    var content;
                    content = $("#"+owner_input_field).getSelectedItemData();
                    $.get("{{ url('/vendors/api/child/') }}" + '/' + content.id, function(data) {
                        if (!data.error) {
                            $("#"+owner_input_field).val(content.company + ' ' + content.firstname + ' ' + content.lastname);
                            $("#"+owner_input_field+'_id').val(content.id);
                            content = data.content;
                            set_property_owner(content);
                        }
                    }).fail(function(){
                    });
                    break;
            }
        }
    },
    highlightPhrase: false
};
$("#"+owner_input_field).easyAutocomplete(options);

$(document).ready(function(){
    $("#"+owner_input_field+"-display button").click(function(event){
        event.preventDefault();
        unset_property_owner();
    });
    set_property_owner_display();
});

function set_property_owner(content) {
    var vendor_rep_section= $("#property-vendor-rep-section .nest-property-edit-rep-wrapper");
    unset_property_reps();
    $.each(content, function(index, rep){
        var elem = $('#property-vendor-rep-checkbox-template').clone();
        var elem_id = 'property-rep_id-checkbox-'+index;
        elem.find('label').attr('for', elem_id);
        elem.find('input').attr('id', elem_id).val(rep.id);
        // console.info(rep);
        var elem_tel = '';
        if(rep.tel) elem_tel = ' (' + rep.tel + ')';
        elem.find('label').append(rep.firstname + ' ' + rep.lastname + elem_tel);
        vendor_rep_section.append(elem.html() + '<br />');
    });
    vendor_rep_section.find('span').remove();
    if (content.length==0) {
        vendor_rep_section.append("<span>"+"-"+"</span>");
    }
    $('#property-vendor-rep-section').show();
    set_property_owner_display();
}

function set_property_owner_display() {
    var owner_id = $("#"+owner_input_field+"_id").val();
    if (owner_id) {
        $("#"+owner_input_field+"-display").show()
            .find('a').text($("#"+owner_input_field).hide().val())
            .attr('target', '_vendor')
            .attr('href', get_property_owner_url(owner_id));
    }
}

function unset_property_owner() {
    unset_property_reps();
    $("#property-owner_id").val(''); 
    $("#"+owner_input_field).val('').show().focus();
    $("#"+owner_input_field+"-display").hide().find('a').text(''); 
}

function unset_property_reps() {
    var vendor_rep_section= $("#property-vendor-rep-section .nest-property-edit-rep-wrapper");
    vendor_rep_section.find('label').each(function(index, item){
        if (index>0) {$(this).remove();}
    });
    vendor_rep_section.find('span').remove();
    vendor_rep_section.append("<span>"+"-"+"</span>");
}

function get_property_owner_url(vendor_id) {
    return "{{ url('/vendor/show/') }}" + '/' + vendor_id; 
}

</script>