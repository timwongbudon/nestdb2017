
<script>
	function setTypeID(id){
		jQuery('#type_id').val(id);
		jQuery('#typeIdTab1').removeClass('nest-property-search-rent-active');
		jQuery('#typeIdTab2').removeClass('nest-property-search-rent-active');
		jQuery('#typeIdTab0').removeClass('nest-property-search-rent-active');
		jQuery('#typeIdTab'+id).addClass('nest-property-search-rent-active');
	}
	jQuery(document).ready(function(){
		var typeid = jQuery('#type_id').val();
		if (typeid == ''){
			jQuery('#type_id').val(1);
			jQuery('#typeIdTab1').removeClass('nest-property-search-rent-active');
			jQuery('#typeIdTab2').removeClass('nest-property-search-rent-active');
			jQuery('#typeIdTab0').removeClass('nest-property-search-rent-active');
			jQuery('#typeIdTab1').addClass('nest-property-search-rent-active');
		}
	});
</script>

<div class="nest-property-search-rent-wrapper">
	@php
		if (!$showadvsearch){
			$input->type_id = 0;
		}
	@endphp
	<a href="javascript:;" id="typeIdTab1" onClick="setTypeID(1);"
	@if ($input->type_id == 1)
		class="nest-property-search-rent-active"
	@endif
	>
		Rent
	</a>
	<a href="javascript:;" id="typeIdTab2" onClick="setTypeID(2);"
	@if ($input->type_id == 2)
		class="nest-property-search-rent-active"
	@endif
	>
		Sale
	</a>
	<a href="javascript:;" id="typeIdTab0" onClick="setTypeID(0);"
	@if ($input->type_id == 0)
		class="nest-property-search-rent-active"
	@endif
	>
		Rent & Sale
	</a>
	
</div>
<div class="nest-property-new-search panel panel-default">
	<!-- New Property Form -->
	<!-- <form action="{{ !empty($template_type)?url($template_type.'/properties'):url('properties') }}" method="GET" class="form-horizontal"> -->
	<form action="{{ url('/listadvsearch') }}" method="get" class="form-horizontal" id="propertyserachform">
		{{ csrf_field() }}
		<input type="hidden" id="type_id" name="type_id" value="{{ old('type_id', $input->type_id) }}" />
		<input type="hidden" id="sort_by_id" name="sort_by_id" value="{{ old('sort_by_id', $input->sort_by_id) }}" />
		<input type="hidden" id="show_type_id" name="show_type_id" value="{{ old('show_type_id', $input->show_type_id) }}" />
		<div class="row">
			<div class="nest-property-new-search-col col-md-4 col-sm-6 col-xs-12">
				{!! Form::select('district_id[]', $district_ids, $input->district_id, ['multiple'=>'multiple', 'id'=>'district_id', 'class'=>'nest-property-new-search-multiple form-control', 'data-placeholder' => 'All Locations']); !!}
			</div>
			<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
				<input type="number" name="bedroom_min" placeholder="Bedrooms from" id="property-bedroom_min" class="form-control" value="{{ old('bedroom_min', $input->bedroom_min) }}" min="0">
			</div>
			<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
				<input type="number" name="bedroom_max" placeholder="Bedrooms to" id="property-bedroom_max" class="form-control" value="{{ old('bedroom_max', $input->bedroom_max) }}" min="0">
			</div>
			<div class="clearfix hidden-lg hidden-md hidden-xs"></div>	
			<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
				<input type="number" name="size_min" placeholder="Size from" id="property-size_min" class="form-control" value="{{ old('size_min', $input->size_min) }}" min="0">
			</div>
			<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
				<input type="number" name="size_max" placeholder="Size to" id="property-size_max" class="form-control" value="{{ old('size_max', $input->size_max) }}" min="0">
			</div>
			<div class="clearfix hidden-sm"></div>
			
			<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
				<input type="number" name="price_min" placeholder="Price from" id="property-price_min" class="form-control" value="{{ old('price_min', $input->price_min) }}" min="0">
			</div>
			<div class="nest-property-new-search-col col-md-2 col-sm-3 col-xs-6">
				<input type="number" name="price_max" placeholder="Price to" id="property-price_max" class="form-control" value="{{ old('price_max', $input->price_max) }}" min="0">
			</div>
			<div class="clearfix hidden-lg hidden-md hidden-sm"></div>			
			<div class="nest-property-new-search-col col-md-4 col-sm-6 col-xs-12">
				{!! Form::select('features[]', $feature_ids, $input->features, ['multiple'=>'multiple', 'id'=>'features', 'class'=>'nest-property-new-search-multiple form-control', 'data-placeholder' => 'Features / Facilities']); !!}
			</div>
			<div class="clearfix hidden-lg hidden-md hidden-sm"></div>		
			<div class="nest-property-new-search-col col-md-4 col-sm-6 col-xs-12">
				@if (!$showadvsearch)
					<input type="text" name="name" placeholder="Building Name" id="property-name" class="form-control" value="{{ old('name', $searchterm) }}">
					<span id="property-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
				@else
					<input type="text" name="name" placeholder="Building Name" id="property-name" class="form-control" value="{{ old('name', $input->name) }}">
					<span id="property-name-display" style="display: none"><a></a><button type="button" class="btn btn-link" title="Change the building name"><i class="fa fa-btn fa-close"></i></button></span>
				@endif
			</div>
		</div>
		
		
		<div class="row" id="advanced-search" <?php
			if ((!isset($_GET['advanced']) || $_GET['advanced'] != 1) && empty($address)){
				echo 'style="display:none;"';
			}
		?>>	
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				@if (!empty($address))
					<input type="text" name="address1" placeholder="Address" id="property-address1_exception" class="form-control" value="{{ old('name', $address) }}">
				@else
					<input type="text" name="address1" placeholder="Address" id="property-address1_exception" class="form-control" value="{{ old('name', $input->address1) }}">
				@endif
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				@if (!empty($propid))
					<input type="text" name="id" placeholder="Property ID" id="property-id" class="form-control" value="{{ old('id', $propid) }}" >
				@else
					<input type="text" name="id" placeholder="Property ID" id="property-id" class="form-control" value="{{ old('id', $input->id) }}" >
				@endif
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				<input type="text" name="unit" placeholder="Unit" id="property-unit" class="form-control" value="{{ old('unit', $input->unit) }}">
			</div>	
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				<input type="text" name="comments" placeholder="Comments" id="property-comments" class="form-control" value="{{ old('comments', $input->comments) }}" >
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				<input type="text" name="description" placeholder="Description" id="property-description" class="form-control" value="{{ old('description', $input->description) }}" >
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				{!! Form::select('hot_option', $hot_option_ids, $input->hot_option, ['id'=>'property-hot_option', 'class'=>'form-control']); !!}
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				<input type="number" name="bathroom_min" placeholder="Bathrooms from" id="property-bathroom_min" class="form-control" value="{{ old('bathroom_min', $input->bathroom_min) }}" min="0">
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				<input type="number" name="bathroom_max" placeholder="Bathrooms to" id="property-bathroom_max" class="form-control" value="{{ old('bathroom_max', $input->bathroom_max) }}" min="0">
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				{!! Form::select('photo_option', $photo_option_ids, $input->photo_option, ['id'=>'property-photo_option', 'class'=>'form-control']); !!}
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				{!! Form::select('web_option', $web_option_ids, $input->web_option, ['id'=>'property-web_option', 'class'=>'form-control']); !!}
			</div>
			<div class="nest-property-new-search-col col-md-3 col-sm-4 col-xs-6">
				<label>
					<input type="checkbox" value="1" name="without_building_id" {{isset($_GET['without_building_id']) ? 'checked' : ''}} style="margin-top:10px;"> Without Building ID 
				</label>
				&nbsp;&nbsp;
				<label>
					<input type="checkbox" value="1" name="no_available_date" {{isset($_GET['no_available_date']) ? 'checked' : ''}} style="margin-top:10px;"> No Available Date  
				</label>
			</div>
		</div>
		
		<!-- Add Submit Button -->
		<div class="form-group">
			<style>
				#advanced-search-link, #advanced-search-link-hide{
					padding-top:8px;
				}
				@media (max-width: 768px){
					#advanced-search-link, #advanced-search-link-hide{
						padding-top:0px;
						padding-bottom:15px;
					}
				}
			</style>
			<div class="text-center col-sm-push-4 col-sm-4 " style="padding-top:5px;">		
				<a href="javascript:;" id="advanced-search-link" onClick="showAdvancedSearch();" style="<?php
					if (!isset($_GET['advanced']) || $_GET['advanced'] != 1){
						echo 'display:inline-block;';
					}else{
						echo 'display:none;';
					}
				?>"><i class="fa fa-angle-down"></i> Advanced Search</a>
				<a href="javascript:;" id="advanced-search-link-hide" onClick="hideAdvancedSearch();" style="<?php
					if (!isset($_GET['advanced']) || $_GET['advanced'] != 1){
						echo 'display:none;';
					}else{
						echo 'display:inline-block;';
					}
				?>"><i class="fa fa-angle-up"></i> Hide Advanced Search</a>
			</div>
			<div class="col-sm-4 col-sm-pull-4 col-xs-6">
				<a class="nest-button btn btn-default nest-property-list-search-button" href="{{ url('property/create') }}">
					<i class="fa fa-btn fa-plus"></i>Add Property
				</a>
			</div>
			<div class="col-sm-4 col-xs-6">
				<button type="submit" name="action" value="search" class="nest-property-list-search-button nest-button nest-right-button btn btn-default"><i class="fa fa-btn fa-search"></i>Search </button>
			</div>
			
			<input type="hidden" id="advanced" name="advanced" value="<?php
					if (!isset($_GET['advanced']) || $_GET['advanced'] != 1){
						echo '0';
					}else{
						echo '1';
					}
				?>" />
		</div>
		
		
		
	<script>
		function showAdvancedSearch(){
			jQuery('#advanced').val(1);
			jQuery('#advanced-search').css('display', 'block');
			jQuery('#advanced-search-link').css('display', 'none');
			jQuery('#advanced-search-link-hide').css('display', 'inline-block');
		}
		function hideAdvancedSearch(){
			jQuery('#advanced').val(0);
			jQuery('#advanced-search').css('display', 'none');
			jQuery('#advanced-search-link').css('display', 'inline-block');
			jQuery('#advanced-search-link-hide').css('display', 'none');
		}
		
		jQuery(document).ready(function(){
			/*jQuery('#district_id').customSelect({
				placeholder: 'Neighbourhoods'
			});*/
			jQuery('#district_id').chosen();
			jQuery('#features').chosen();
			/*jQuery('#features').customSelect({
				placeholder: 'Features & Facilities'
			});*/
		});
		
		
	</script>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	</form>
</div>