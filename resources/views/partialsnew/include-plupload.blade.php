{{-- plupload form --}}

<div id="{{ $id }}" class="col-xs-12" style="padding-left:0px;padding-right:0px;">
	<div class="nest-property-edit-upload-label">{{ $label }}</div>
	<div class="nest-property-edit-upload-images">
		<div id="{{ $id }}_files"></div>
		<div id="{{ $id }}_filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
		<div id="{{ $id }}_container">
			<a id="{{ $id }}_pickfiles" href="javascript:;">[Select files]</a>
			<a id="{{ $id }}_uploadfiles" href="javascript:;">[Upload files]</a>
			@if (preg_match("/photo/", $id))<a id="{{ $id }}_reorderfiles" target="_reorder_files" href="{{ url('property/media/edit-reorder/' . $vars['id'] . '/' . $vars['group'] . '/' . $vars['type']) }}">[Re-order files]</a><a id="{{ $id }}_removefiles" target="_remove_files" href="{{ url('property/media/edit-remove/' . $vars['id'] . '/' . $vars['group'] . '/' . $vars['type']) }}">[Remove files]</a>@endif
		</div>
		<br />
		<pre id="{{ $id }}_console" class="nest-property-edit-upload-output" style="display:none;"></pre>
	</div>
</div>