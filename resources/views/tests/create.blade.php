@extends('layouts.app')
<!-- depreciated -->
@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Test
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Test Form -->
                    <form action="{{ url('test/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- Test Name -->
                        <div class="form-group">
                            <label for="test-name" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="test-name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Textarea</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('break_clause', '', ['class'=>'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Hidden</label>
                            <div class="col-sm-6">
                                {!! Form::hidden('hiddentext') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Checkbox</label>
                            <div class="col-sm-6">
                                 <label for="test-checkbox-1" class="control-label">{!! Form::checkbox('checkboxfield', 1, null, ['id'=>'test-checkbox-1']) !!} Agree</label>
                                 <label for="test-checkbox-2" class="control-label">{!! Form::checkbox('checkboxfield', 0, null, ['id'=>'test-checkbox-2']) !!} Disagree</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Radio</label>
                            <div class="col-sm-6">
                                 <label for="test-radio-1" class="control-label">{!! Form::radio('radiofield', 1, null, ['id'=>'test-radio-1']) !!} Agree</label>
                                 <label for="test-radio-2" class="control-label">{!! Form::radio('radiofield', 0, null, ['id'=>'test-radio-2']) !!} Disagree</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-6">
                                {!! Form::password('password', array('class' => 'form-control')); !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Number</label>
                            <div class="col-sm-6">
                                {!! Form::number('number', '999', ['class'=>'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Date</label>
                            <div class="col-sm-6">
                                {!! Form::date('datefield', '2016-08-25') !!}
                                {!! Form::date('datefield', \Carbon\Carbon::now()) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">File</label>
                            <div class="col-sm-6">
                                {!! Form::file('image', ['class'=>'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Button</label>
                            <div class="col-sm-6">
                                {!! Form::submit('Click Me!') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-break_clause" class="col-sm-3 control-label">Selection</label>
                            <div class="col-sm-6">
                                {!! Form::select('size', array('L' => 'Large', 'S' => 'Small'), 'S', ['class'=>'form-control', 'placeholder' => 'Pick a size...']); !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="test-name" class="col-sm-3 control-label">Amount</label>
                            <div class="col-sm-6">
                                <input type="text" name="amount" id="test-amount" class="form-control" value="{{ old('amount') }}">
                            </div>
                        </div>

                        <!-- Add Test Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Test
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
