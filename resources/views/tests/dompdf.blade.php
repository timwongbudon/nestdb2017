<html>
<head>
<style>


@page { margin: 0px; padding:0px; background-color: green}
body { 
	font-family: 'TrajanPro';
	margin: 0px; 
	background-image: url('/test-images/property-listing.jpg');
	background-repeat: no-repeat;
}

.page-break {
    page-break-after: always;
}

@font-face { 
   font-family: 'Champagne'; 
   src: url('{{storage_path('/fonts')}}/Champagne_Limousines.ttf'); 
} 

@font-face {
	font-family: 'TrajanPro';
	src: url('{{storage_path('/fonts')}}/TrajanPro-Regular.otf');
}

img {
	margin: auto; 
    max-height: 100%;
    max-width: 100%;
}

.image1 {
	position: absolute; 
	top: 13px; 
	left: 13px; 
	overflow: hidden; 
	width: 510px; 
	height: 340px;
}

.image2{
	position: absolute; 
	top: 13px; 
	left: 530px; 
	overflow: hidden; 
	width: 250px; 
	height: 166px;
}

.image3{
	position: absolute; 
	top: 186px; 
	left: 530px; 
	overflow: hidden; 
	width: 250px; 
	height: 166px;
}

div.number {
	position: absolute; 
	top: 483px; 
	width: 35px; 
	height: 35px;
}

p.number {
	font-family: 'Champagne';
	font-size: 25px; 
	padding-top: -29px; 
	padding-left: 11px; 
	color: #B39C8C;
}

div.property-name {
	position: absolute; 
	top: 377px; 
	left: 13px; 
	width: 526px; 
	height: 52px;
}

p.property-name {
	font-size: 27px; 
	color: #fff; 
	padding-top: -24px; 
	padding-left: 15px;
}

div.price {
	position: absolute; 
	top: 366px; 
	left: 548px; 
	width: 92px; 
	height: 74px;
}

div.lease {
	position: absolute; 
	top: 377px; 
	left: 649px; 
	width: 132px; 
	height: 52px;
}

p.size, p.price {
	font-family: 'Champagne';
}

p.price {
	font-size: 27px; 
	text-align: center; 
	line-height: 0.1; 
	color: #B39C8C;
}

</style>
</head>
	<body>
	<!-- 794x560 -->
		<div style="padding: 13px;">
			<div class="image1">
				<img src="http://www.collectspace.com/images/news-012016a.jpg" width="510">
			</div>

			<div class="image2">
				{{-- <img src="{{ url('/test-images/image2.jpg') }}"> --}}
				<img src="http://www.collectspace.com/images/news-012016a.jpg" width="250">
			</div>

			<div class="image3"">
				<img src="{{ url('/test-images/image3.jpg') }}">
			</div>
			<div class="property-name">
				<p class="property-name">STEWART TERRACE</p>
			</div>
			<div class="price">
				<p class="price">HK$</p>
				<p class="price">35K</p>
			</div>
			<div class="lease">
				<p style="font-size: 20px; text-align: center; color: #fff; padding-top: -12px;">FOR LEASE</p>
			</div>
			<div class="number" style="left: 90px;">
				<p class="number">1</p>
			</div>
			<div class="number" style="left: 210px;">
				<p class="number">2</p>
			</div>
			<div class="number" style="left: 330px;">
				<p class="number">3</p>
			</div>
			<div class="number" style="left: 450px;">
				<p class="number">4</p>
			</div>			
			<div class="number" style="left: 570px;">
				<p class="number">5</p>
			</div>	
			<div style="position: absolute; top: 454px; right: 15px; width: 150px; height: 50px;">
				<p class="size" style="font-size: 22px; padding-top: -31px; text-align: right; color: #B39C8C;">3,507 sq. ft.</p>
				<p class="size" style="font-size: 17px; padding-top: -31px; text-align: right; color: #B39C8C;">(2,690 net)</p>
			</div>	
			<div style="position: absolute; top: 540px; right: 15px; width: 150px; height: 50px;">
				<p class="size" style="font-size: 15px; padding-top: -31px; text-align: right; color: #B39C8C;">81-95 Peak Road</p>
			</div>							
{{-- <div class="page-break"></div> --}}				
		</div>
	</body>
</html>