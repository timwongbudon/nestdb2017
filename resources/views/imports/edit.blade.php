@extends('layouts.app')

<?php 
$sample = current($results);
?>

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Import
                </div>
                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Test Form -->
                    <form action="{{ url('import/update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="import-name" class="col-sm-3 control-label">Source</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" id="import-name" class="form-control" value="{{ old('name', $import->name) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-comments" class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('comments', old('comments', $import->comments), ['class'=>'form-control', 'placeholder' => '', 'rows'=>'5']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-comments" class="col-sm-3 control-label">For web</label>
                            <div class="col-sm-6">
                                {!! Form::select('web', array('0'=>'No', '1'=>'Yes'), old('web', $import->web), ['class'=>'form-control']); !!}
                            </div>
                        </div>
                        <hr/>
<?php foreach($fields as $key => $field):?>
<?php 
    $value = !empty($preference)?$preference[$key]:'';
    if ($value=='' && preg_match("/^image\d+/", $fields[$key])) {
        $value = 'nest_photos[]';
    }
?>
                        <div class="form-group">
                            <label for="property-fields" class="col-sm-3 control-label">{{ $field }}</label>
                            <div class="col-sm-3">
                                {!! Form::select("fields[$key]", $options, old('field[1]', $value), ['id'=>'property-fields', 'class'=>'form-control', 'placeholder' => '', 'style'=>'width: 300px;']); !!}
                                [<i>{{ $sample->{$field} }}</i>]
                            </div>
                        </div>
<?php endforeach;?>
                        <div class="form-group">
                            <label for="import-submit" class="col-sm-3 control-label"></label>
                            <div class="col-sm-6">
                                {{ Form::hidden('previous_url', $previous_url) }}
                                {{ Form::hidden('id', $import->id) }}
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-pencil"></i>Import data
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Bulk Import Excel Data</h4>
      </div>
      <div class="modal-body">
        <p>Loading... </p>
        <p>Please do not reload or click 'back' button.</p>
        <p>It wil return to index page when it is done.</p>
      </div>
    </div>

  </div>
</div>
@endsection

@section('footer-script')
<script type="text/javascript">
$(document).ready(function(){
    $("button[type=submit]").click(function(event){
        event.preventDefault();
        $('#myModal').modal('show');
        $('form').submit();
    });
});
</script>
@endsection