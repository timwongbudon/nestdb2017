@extends('layouts.app')

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Import
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Test Form -->
                    <form action="{{ url('import/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="import-file" class="col-sm-3 control-label">File</label>
                            <div class="col-sm-6">
                                {!! Form::file('file', ['class'=>'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-name" class="col-sm-3 control-label">Source</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" id="import-name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-comments" class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('comments', '', ['class'=>'form-control', 'placeholder' => '', 'rows'=>'3']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-comments" class="col-sm-3 control-label">Format</label>
                            <div class="col-sm-6">
                                {!! Form::select('format', $formats, '', ['class'=>'form-control', 'placeholder' => 'Choose a saved format']); !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-comments" class="col-sm-3 control-label">For web</label>
                            <div class="col-sm-6">
                                {!! Form::select('web', array('0'=>'No', '1'=>'Yes'), '0', ['class'=>'form-control']); !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-submit" class="col-sm-3 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Submit 
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <!-- Current imports -->
            @if (count($imports) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Import History
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped import-table">
                            <thead>
                                <th style="width: 5%">ID</th>
                                <th>Source</th>
                                <th>Comments</th>
                                <th>File</th>
                                <th style="width: 8%">&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($imports as $import)
                                    <tr>
                                        <td class="table-text"><div>{{ $import->id }}</div></td>
                                        <td class="table-text"><div>{{ $import->name }}</div></td>
                                        <td class="table-text"><div>{{ $import->comments }}</div></td>
                                        <td class="table-text"><div>{{ $import->filename }}</div></td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('import/edit/'.$import->id) }}">
                                                <i class="fa fa-btn fa-pencil-square-o"></i>Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div style="margin: 10px 0;">Page {{ $imports->currentPage() }} of {{ $imports->lastPage() }} (Total: {{ $imports->total() }} items)</div>
                        {{ $imports->links() }}
                    </div>
                </div>
            @endif
@endsection
