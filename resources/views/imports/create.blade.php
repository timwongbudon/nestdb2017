@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Import
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Test Form -->
                    <form action="{{ url('import/store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="import-file" class="col-sm-3 control-label">File</label>
                            <div class="col-sm-6">
                                {!! Form::file('file', ['class'=>'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-name" class="col-sm-3 control-label">Source</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" id="import-name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-comments" class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('comments', '', ['class'=>'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import-submit" class="col-sm-3 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Submit 
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
