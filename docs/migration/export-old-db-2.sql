SELECT `ID`, `post_title` as `title`, `post_content` as `content`,  
-- `wp_postmeta1`.`meta_value` as `unit` , 
-- `wp_postmeta2`.`meta_value` as `address` , 
-- `wp_postmeta3`.`meta_value` as `location` , 
-- `wp_postmeta4`.`meta_value` as `contact information` , 
-- `wp_postmeta5`.`meta_value` as `comment` , 
-- `wp_postmeta6`.`meta_value` as `type` , 
`wp_postmeta7`.`meta_value` as `price` , 
`wp_postmeta8`.`meta_value` as `bedroom` , 
`wp_postmeta9`.`meta_value` as `bathroom` , 
`wp_postmeta12`.`meta_value` as `maidroom` , 
`wp_postmeta10`.`meta_value` as `gross_area` , 
`wp_postmeta11`.`meta_value` as `saleable_area` , 
`post_date` as `created` 
FROM `wp_posts` 
-- LEFT JOIN `wp_postmeta` as `wp_postmeta1` ON `wp_posts`.ID = `wp_postmeta1`.`post_id` 
-- LEFT JOIN `wp_postmeta` as `wp_postmeta2` ON `wp_posts`.ID = `wp_postmeta2`.`post_id` 
-- LEFT JOIN `wp_postmeta` as `wp_postmeta3` ON `wp_posts`.ID = `wp_postmeta3`.`post_id` 
-- LEFT JOIN `wp_postmeta` as `wp_postmeta4` ON `wp_posts`.ID = `wp_postmeta4`.`post_id` 
-- LEFT JOIN `wp_postmeta` as `wp_postmeta5` ON `wp_posts`.ID = `wp_postmeta5`.`post_id` 
-- LEFT JOIN `wp_postmeta` as `wp_postmeta6` ON `wp_posts`.ID = `wp_postmeta6`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta7` ON `wp_posts`.ID = `wp_postmeta7`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta8` ON `wp_posts`.ID = `wp_postmeta8`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta9` ON `wp_posts`.ID = `wp_postmeta9`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta10` ON `wp_posts`.ID = `wp_postmeta10`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta11` ON `wp_posts`.ID = `wp_postmeta11`.`post_id`
LEFT JOIN `wp_postmeta` as `wp_postmeta12` ON `wp_posts`.ID = `wp_postmeta11`.`post_id`
WHERE 
-- `wp_postmeta1`.`meta_key` = 'dropbox' and 
-- `wp_postmeta2`.`meta_key` = 'address' and 
-- `wp_postmeta3`.`meta_key` = 'deposit' and 
-- `wp_postmeta4`.`meta_key` = 'contact_information' and 
-- `wp_postmeta5`.`meta_key` = 'comments' and 
-- `wp_postmeta6`.`meta_key` = 'rent_or_sale' and 
`wp_postmeta7`.`meta_key` = 'price' and 
`wp_postmeta8`.`meta_key` = 'bedrooms' and 
`wp_postmeta9`.`meta_key` = 'bathrooms' and 
`wp_postmeta10`.`meta_key` = 'checkbox' and 
`wp_postmeta11`.`meta_key` = 'net' and 
`wp_postmeta12`.`meta_key` = 'helpers_quarter' and 
`post_type` = 'property' and `post_status` = 'publish' ORDER BY ID ASC
