SELECT `ID`, `post_title` as `title`, `post_content` as `content`,  
`wp_postmeta12`.`meta_value` as `inclusive` , 
`wp_postmeta13`.`meta_value` as `management_fee` , 
`wp_postmeta14`.`meta_value` as `government_rate` , 
`wp_postmeta15`.`meta_value` as `availlable_date` , 
`post_date` as `created` 
FROM `wp_posts` 
LEFT JOIN `wp_postmeta` as `wp_postmeta12` ON `wp_posts`.ID = `wp_postmeta12`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta13` ON `wp_posts`.ID = `wp_postmeta13`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta14` ON `wp_posts`.ID = `wp_postmeta14`.`post_id` 
LEFT JOIN `wp_postmeta` as `wp_postmeta15` ON `wp_posts`.ID = `wp_postmeta14`.`post_id` 
WHERE 
`wp_postmeta12`.`meta_key` = 'incusive' and 
`wp_postmeta13`.`meta_key` = 'management_fee' and 
`wp_postmeta14`.`meta_key` = 'government_rates' and 
`wp_postmeta15`.`meta_key` = 'availlable_date' and 
`post_type` = 'property' and `post_status` = 'publish' ORDER BY ID ASC LIMIT 10
